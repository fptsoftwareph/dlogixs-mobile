package com.dlogixs.neurobeat;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.RNFetchBlob.RNFetchBlobPackage;
import io.github.douglasjunior.ReactNativeEasyBluetooth.classic.ClassicPackage;
import com.cinder92.musicfiles.RNReactNativeGetMusicFilesPackage;
import com.theweflex.react.WeChatPackage;
import com.auth0.react.A0Auth0Package;
import com.sunyrora.kakaosignin.RNKaKaoSigninPackage;
import com.github.wuxudong.rncharts.MPAndroidChartPackage;
import com.devstepbcn.wifi.AndroidWifiPackage;
import com.beefe.picker.PickerViewPackage;
import it.innove.BleManagerPackage;

import me.fraserxu.rncouchbaselite.ReactCBLiteManager;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.dlogixs.MyPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.oblador.vectoricons.VectorIconsPackage;
import com.goldenowl.twittersignin.TwitterSigninPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.facebook.stetho.Stetho;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.appevents.AppEventsLogger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;


public class MainApplication extends MultiDexApplication implements ReactApplication {

  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();
  
  protected static CallbackManager getCallbackManager() {
      return mCallbackManager;
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RNFetchBlobPackage(),
            new ClassicPackage(),
            new RNReactNativeGetMusicFilesPackage(),
            new WeChatPackage(),
            new A0Auth0Package(),
            new RNKaKaoSigninPackage(),
            new MPAndroidChartPackage(),
            new AndroidWifiPackage(),
            new PickerViewPackage(),
            new RNI18nPackage(),
            new BleManagerPackage(),
            new ReactCBLiteManager(),
            new MyPackage(),
            new RNDeviceInfo(),
            new VectorIconsPackage(),
            new TwitterSigninPackage(),
            new RNGoogleSigninPackage(),
            new FBSDKPackage(mCallbackManager)
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Stetho.initializeWithDefaults(this); //visit 'chrome://inspect'
    SoLoader.init(this, /* native exopackage */ false);
    FacebookSdk.sdkInitialize(getApplicationContext());
    // If you want to use AppEventsLogger to log events.
    AppEventsLogger.activateApp(this);

    // Add code to print out the key hash
    try {
      PackageInfo info = getPackageManager().getPackageInfo(
              "com.dlogixs.neurobeat",
              PackageManager.GET_SIGNATURES);
      for (Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
      }
    } catch (PackageManager.NameNotFoundException e) {

    } catch (NoSuchAlgorithmException e) {

    }
  }
}
