package com.bestmafen.easeblelib.scanner;

import com.bestmafen.easeblelib.entity.EaseDevice;

/**
 * Created by fptsoftware on 25/10/2017.
 */

public class SimpleEaseScanCallback implements EaseScanCallback {

    @Override
    public void onDeviceFound(EaseDevice device) {

    }

    @Override
    public void onBluetoothDisabled() {

    }

    @Override
    public void onScanStart(boolean start) {

    }
}