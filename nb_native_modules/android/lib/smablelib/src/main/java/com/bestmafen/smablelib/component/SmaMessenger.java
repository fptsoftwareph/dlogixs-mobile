package com.bestmafen.smablelib.component;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

import com.bestmafen.easeblelib.util.EaseUtils;
import com.bestmafen.easeblelib.util.L;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * This class is used to communication with Bluetooth device.
 */
public class SmaMessenger {
    /**
     * The queue holds normal messages,such as reading a characteristic,writing a characteristic and enabling or disabling a
     * notification.
     */
    private LinkedBlockingQueue<SmaMessage> mMessages = new LinkedBlockingQueue<>();

    /**
     * The semaphore to keep every small package of data sent to device is processed sequentially.
     */
    private Semaphore mPackageSemaphore = new Semaphore(1);

    /**
     * The semaphore to keep every whole command sent to device is processed sequentially.
     */
    private Semaphore mReceiveACKSemaphore = new Semaphore(1);

    /**
     * The queue holds messages of ACK,see {@link SmaManager#isNeedReturnACK(byte, byte)}
     */
    private LinkedBlockingQueue<SmaMessage> mACKs = new LinkedBlockingQueue<>();

    /**
     * The semaphore to keep every ACK sent to device is processed sequentially.
     */
    private Semaphore mReturnACKSemaphore = new Semaphore(1);

    volatile boolean isReplacingWatchFace = false;
    private volatile boolean isExit = false;

    /**
     * The type of messages.
     */
    public enum MessageType {
        /**
         * to write a characteristic
         */
        WRITE,

        /**
         * to read a characteristic
         */
        READ,

        /**
         * to enable or disable a notification
         */
        NOTIFY
    }

    SmaMessenger() {
        /**
         * The thread used to process normal messages,it will be blocking when {@link SmaMessenger#mMessages} gets empty.
         */
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    while (!isExit) {
                        SmaMessage message = mMessages.take();
                        if (message.mGatt == null || message.mCharacteristic == null) continue;

                        switch (message.mType) {
                            case READ:
                                L.v("SmaMessenger acquire mPackageSemaphore -> " + mPackageSemaphore.availablePermits() + " " +
                                        ", " + message.toString());
                                mPackageSemaphore.acquire();
                                message.mGatt.readCharacteristic(message.mCharacteristic);
                                break;
                            case NOTIFY:
                                BluetoothGattDescriptor descriptor = message.mCharacteristic.getDescriptor(UUID.fromString
                                        ("00002902-0000-1000-8000-00805f9b34fb"));
                                if (descriptor == null) continue;

                                L.v("SmaMessenger acquire mPackageSemaphore -> " + mPackageSemaphore.availablePermits() +
                                        " , " + message.toString());
                                mPackageSemaphore.acquire();
                                message.mGatt.setCharacteristicNotification(message.mCharacteristic, message.mData != null);
                                descriptor.setValue(message.mData != null ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                                        : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                                message.mGatt.writeDescriptor(descriptor);
                                break;
                            case WRITE:
                                if (message.isNeedReturnACK) {
                                    L.v("SmaMessenger acquire mReturnACKSemaphore -> " + mReturnACKSemaphore.availablePermits());
                                    mReturnACKSemaphore.acquire();
                                }

                                if ((message.mData[0] & 0xFF) == 0xAB && !isReplacingWatchFace) {// 如果是一条命令的第一个packet
                                    if (!(message.mData.length == 8 && (message.mData[1] == 0x10 || message.mData[1] == 0x30))) {
                                        L.v("SmaMessenger acquire mReceiveACKSemaphore -> " + mReceiveACKSemaphore
                                                .availablePermits());
                                        mReceiveACKSemaphore.acquire();
                                    }
                                }
                                L.v("SmaMessenger acquire mPackageSemaphore -> " + mPackageSemaphore.availablePermits() + " , " +
                                        message.toString());
                                mPackageSemaphore.acquire();
                                message.mCharacteristic.setValue(message.mData);
                                message.mGatt.writeCharacteristic(message.mCharacteristic);
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        /**
         * The thread used to write ACK ,it will be blocking when {@link SmaMessenger#mACKs} gets empty.
         */
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    while (!isExit) {
                        SmaMessage message = mACKs.take();
                        if (message.mGatt == null || message.mCharacteristic == null) continue;

                        mPackageSemaphore.acquire();
                        message.mCharacteristic.setValue(message.mData);
                        message.mGatt.writeCharacteristic(message.mCharacteristic);
                        L.v("SmaMessenger acquire mPackageSemaphore -> " + mPackageSemaphore.availablePermits() + " , " + message
                                .toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Add a message to the queue {@link SmaMessenger#mMessages} or {@link SmaMessenger#mACKs}
     *
     * @param message the message to be added
     */
    synchronized void addMessage(SmaMessage message) {
        try {
            L.v("SmaMessenger -> addMessage >>>> " + message.toString());
            if (message.isACK) {
                mACKs.put(message);
                return;
            }

            switch (message.mType) {
                case READ:
                case NOTIFY:
                    mMessages.put(message);
                    break;
                case WRITE:
                    byte[] data = message.mData;
                    if (data == null || data.length < 1) return;

                    int count = data.length % 20 == 0 ? data.length / 20 : data.length / 20 + 1;
                    for (int i = 0; i < count; i++) { //拆分
                        byte[] packet;
                        if (i == count - 1) {
                            packet = Arrays.copyOfRange(data, i * 20, data.length);
                        } else {
                            packet = Arrays.copyOfRange(data, i * 20, (i + 1) * 20);
                        }

                        SmaMessage msg = new SmaMessage(message.mGatt, message.mCharacteristic, packet, SmaMessenger.MessageType
                                .WRITE);
                        if (i == 0 && message.isNeedReturnACK) {
                            msg.isNeedReturnACK = true;
                        }
                        mMessages.put(msg);
                    }
                    break;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove all tasks in task queue.
     */
    synchronized void clearAllTask() {
        isReplacingWatchFace = false;
        mMessages.clear();
        mACKs.clear();
        L.v("SmaMessenger clearAllTask 之前 -> " + mPackageSemaphore.availablePermits() + "," + mReceiveACKSemaphore.availablePermits
                () + "," + mReturnACKSemaphore.availablePermits());
        if (mPackageSemaphore.availablePermits() == 0) {
            mPackageSemaphore.release();
        }
        if (mReceiveACKSemaphore.availablePermits() == 0) {
            mReceiveACKSemaphore.release();
        }
        if (mReturnACKSemaphore.availablePermits() == 0) {
            mReturnACKSemaphore.release();
        }
        L.v("SmaMessenger clearAllTask 之后 -> " + mPackageSemaphore.availablePermits() + "," + mReceiveACKSemaphore.availablePermits
                () + "," + mReturnACKSemaphore.availablePermits());
    }

    void releasePackageSemaphore() {
        if (mPackageSemaphore.availablePermits() > 0) return;

        mPackageSemaphore.release();
        L.v("SmaMessenger -> release mPackageSemaphore " + mPackageSemaphore.availablePermits());
    }

    void releaseReceiveSemaphore() {
        if (mReceiveACKSemaphore.availablePermits() > 0) return;

        mReceiveACKSemaphore.release();
        L.v("SmaMessenger -> release mReceiveACKSemaphore " + mReceiveACKSemaphore.availablePermits());
    }

    void releaseReturnSemaphore() {
        if (mReturnACKSemaphore.availablePermits() > 0) return;

        mReturnACKSemaphore.release();
        L.v("SmaMessenger -> release mReturnACKSemaphore " + mReturnACKSemaphore.availablePermits());
    }

    void exit() {
        isExit = true;
    }

    static class SmaMessage {
        /**
         * GATT
         */
        BluetoothGatt mGatt;

        /**
         * characteristic
         */
        BluetoothGattCharacteristic mCharacteristic;

        /**
         * if type of the message is {@link MessageType#WRITE},this field is the data will be sent to device;
         * if type of the message is {@link MessageType#NOTIFY},enable if this is not null,disable if is null.
         * if type of the message is {@link MessageType#READ},this field is useless.
         */
        byte[] mData;

        MessageType mType;

        /**
         * see {@link SmaManager#isNeedReturnACK(byte, byte)}
         */
        boolean isNeedReturnACK;
        boolean isACK;

        SmaMessage(BluetoothGatt gatt, BluetoothGattCharacteristic bc, byte[] data, MessageType type, boolean isNeedReturnACK,
                   boolean isACK) {
            this.mGatt = gatt;
            this.mCharacteristic = bc;
            this.mData = data;
            this.mType = type;
            this.isNeedReturnACK = isNeedReturnACK;
            this.isACK = isACK;
        }

        SmaMessage(BluetoothGatt gatt, BluetoothGattCharacteristic bc, byte[] data, MessageType type, boolean isNeedReturnACK) {
            this(gatt, bc, data, type, isNeedReturnACK, false);
        }

        SmaMessage(BluetoothGatt gatt, BluetoothGattCharacteristic bc, byte[] data, MessageType type) {
            this(gatt, bc, data, type, false);
        }

        @Override
        public String toString() {
            return "SmaMessage{" +
                    "mType=" + mType +
                    ", isNeedReturnACK=" + isNeedReturnACK +
                    ", isACK=" + isACK +
                    ", mData=" + EaseUtils.byteArray2HexString(mData) +
                    '}';
        }
    }
}
