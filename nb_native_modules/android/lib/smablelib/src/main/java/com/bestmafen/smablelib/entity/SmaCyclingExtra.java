package com.bestmafen.smablelib.entity;

/**
 * Created by Administrator on 2017/12/1.
 */

public class SmaCyclingExtra implements ISmaCmd {
    public int   speed;
    public int   altitude;
    public float distance;

    @Override
    public byte[] toByteArray() {
        byte[] extra = new byte[5];
        extra[0] = (byte) speed;
        extra[1] = (byte) (altitude >>> 8);
        extra[2] = (byte) (altitude & 0xff);
        int d = (int) (distance * 10);
        extra[3] = (byte) (d >>> 8);
        extra[4] = (byte) (d & 0xff);
        return extra;
    }
}
