package com.bestmafen.smablelib.util;

import android.text.TextUtils;

import com.bestmafen.easeblelib.util.L;

import java.util.Locale;
import java.util.TimeZone;

public class SmaBleUtils {
    public static TimeZone getDefaultTimeZone() {
        return TimeZone.getTimeZone("GMT+0");
    }

    public static byte getLanguageCode() {
        byte code = 0x01;
        String language = Locale.getDefault().getLanguage();
        if (language.equalsIgnoreCase("zh")) {
            code = 0x00;
        } else if (language.equalsIgnoreCase("tr")) {
            code = 0x02;
        } else if (language.equalsIgnoreCase("ru")) {
            code = 0x04;
        } else if (language.equalsIgnoreCase("es")) {
            code = 0x05;
        } else if (language.equalsIgnoreCase("it")) {
            code = 0x06;
        } else if (language.equalsIgnoreCase("ko")) {
            code = 0x07;
        } else if (language.equalsIgnoreCase("pt")) {
            code = 0x08;
        } else if (language.equalsIgnoreCase("de")) {
            code = 0x09;
        } else if (language.equalsIgnoreCase("fr")) {
            code = 0x0A;
        } else if (language.equalsIgnoreCase("nl")) {
            code = 0xB;
        } else if (language.equalsIgnoreCase("pl")) {
            code = 0x0C;
        } else if(language.equalsIgnoreCase("cs")) {
            code = 0x0D;
        }
        L.d(language + "," + code);
        return code;
    }

    public static String addressPlus1(String address) {
        if (TextUtils.isEmpty(address)) {
            return "";
        }

        String s = address.replace(":", "");
        long value = Long.parseLong(s, 16) + 1;
        s = Long.toHexString(value);

        int l = s.length() / 2;
        StringBuilder sb = new StringBuilder(s);
        for (int i = 1; i < l; i++) {
            sb.insert(i * 2 + (i - 1), ":");
        }

        return sb.toString().toUpperCase();
    }

    /**
     * Get distance by height and steps
     *
     * @param height height(cm)
     * @param steps  steps
     * @return distance(km)
     */
    public static float getDistance(float height, int steps) {
        return height * steps * 45 / 10000 / 1000;
    }

    /**
     * Get calorie by weight ,steps and gender
     *
     * @param weight weight(kg)
     * @param steps  steps
     * @param gender 0 female;1 male
     * @return calorie(Kcal)
     */
    public static int getCalorie(float weight, int steps, int gender) {
        return gender == 0 ? (int) Math.floor(0.46f * weight * steps / 1000) : (int) Math.floor(0.55f * weight * steps /
                1000);
    }
}