package com.bestmafen.smablelib.entity;

/**
 * 天气预报
 */

public class SmaForecast implements ISmaCmd {
    public int temH;
    public int temL;
    public int weatherCode;
    public int ultraviolet;

    @Override
    public byte[] toByteArray() {
        byte[] extra = new byte[4];
        extra[0] = (byte) temH;
        extra[1] = (byte) temL;
        extra[2] = (byte) weatherCode;
        extra[3] = (byte) ultraviolet;
        return extra;
    }
}
