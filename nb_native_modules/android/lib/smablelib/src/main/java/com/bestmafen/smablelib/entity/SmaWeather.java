package com.bestmafen.smablelib.entity;

import com.bestmafen.easeblelib.util.EaseUtils;

/**
 * 实时天气
 */

public class SmaWeather implements ISmaCmd {
    public SmaTime time;
    public int     tem;
    public int     weatherCode;
    public int     precipitation;
    public int     visibility;
    public int     windSpeed;
    public int     humidity;

    @Override
    public byte[] toByteArray() {
        byte[] extra = new byte[7];
        extra[0] = (byte) tem;
        extra[1] = (byte) weatherCode;
        extra[2] = (byte) (precipitation >>> 8);
        extra[3] = (byte) (precipitation & 0xff);
        extra[4] = (byte) visibility;
        extra[5] = (byte) windSpeed;
        extra[6] = (byte) humidity;

        if (time != null) {
            extra = EaseUtils.concat(time.toByteArray(), extra);
        } else {
            extra = EaseUtils.concat(new byte[4], extra);
        }
        return extra;
    }
}
