package com.bestmafen.smablelib.component;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.bestmafen.easeblelib.connector.EaseConnector;
import com.bestmafen.easeblelib.util.EaseUtils;
import com.bestmafen.easeblelib.util.L;
import com.bestmafen.smablelib.entity.ISmaCmd;
import com.bestmafen.smablelib.entity.SmaAlarm;
import com.bestmafen.smablelib.entity.SmaBloodPressure;
import com.bestmafen.smablelib.entity.SmaCycling;
import com.bestmafen.smablelib.entity.SmaExercise;
import com.bestmafen.smablelib.entity.SmaHeartRate;
import com.bestmafen.smablelib.entity.SmaSedentarinessSettings;
import com.bestmafen.smablelib.entity.SmaSleep;
import com.bestmafen.smablelib.entity.SmaSport;
import com.bestmafen.smablelib.entity.SmaTime;
import com.bestmafen.smablelib.entity.SmaTracker;
import com.bestmafen.smablelib.util.SmaBleHelper;
import com.bestmafen.smablelib.util.SmaBleUtils;
import com.bestmafen.smablelib.util.SmaConsts;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * This is a core class which we used to manager a Bluetooth device.
 */
public class SmaManager {
    /**
     * UUID of the main {@link BluetoothGattService}.
     */
    public static final String UUID_SERVICE_MAIN = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";

    /**
     * UUID of the {@link BluetoothGattCharacteristic} for writing.
     */
    public static final String UUID_CHARACTER_WRITE = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";

    /**
     * UUID of the {@link BluetoothGattCharacteristic} for reading.
     */
    public static final String UUID_CHARACTER_READ = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";

    /**
     * UUID of the main {@link BluetoothGattService} for round watch.
     */
    public static final String UUID_SERVICE_MAIN_ROUND = "0000ff60-0000-1000-8000-00805f9b34fb";

    /**
     * UUID of the {@link BluetoothGattCharacteristic} for writing to round watch.
     */
    public static final String UUID_CHARACTER_WRITE_ROUND = "0000ff61-0000-1000-8000-00805f9b34fb";

    /**
     * UUID of the {@link BluetoothGattCharacteristic} for reading to round watch.
     */
    public static final String UUID_CHARACTER_READ_ROUND = "0000ff62-0000-1000-8000-00805f9b34fb";

    /**
     * UUID of the {@link BluetoothGattService} for reading firmware flag.
     */
    public static final String UUID_SERVICE_FIRM_FLAG = "0000180a-0000-1000-8000-00805f9b34fb";

    /**
     * UUID of the {@link BluetoothGattCharacteristic} for reading firmware flag.
     */
    public static final String UUID_CHARACTER_FIRM_FLAG = "00002a29-0000-1000-8000-00805f9b34fb";

    public static final String UUID_CHARACTER_CLASSIC_ADDRESS = "00002a23-0000-1000-8000-00805f9b34fb";

    /**
     * The key to save a remote Bluetooth device's name.
     */
    public static final String SP_DEVICE_NAME = "sp_device_name";

    /**
     * The key to save a remote Bluetooth device's address.
     */
    public static final String SP_DEVICE_ADDRESS = "sp_device_address";

    /**
     * The key to save a remote classic Bluetooth device's address.
     */
    public static final String SP_CLASSIC_ADDRESS = "sp_classic_address";

    /**
     * The key to save a remote Bluetooth device's type.
     */
    public static final String SP_DEVICE_TYPE = "sp_device_type";

    /**
     * The key to save a remote Bluetooth device's firmware version.
     */
    public static final String SP_FIRMWARE = "sp_firmware";

    /**
     * The key to save a remote Bluetooth device's firmware flag.
     */
    public static final String SP_FIRMWARE_FLAG = "sp_firmware_flag";

    public static final String SP_PHONE_FLAG = "sp_phone_flag";

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}
     */
    public static final class Cmd {
        public static final byte NONE    = 0x00;
        public static final byte UPDATE  = 0x01;
        public static final byte SET     = 0x02;
        public static final byte CONNECT = 0x03;
        public static final byte NOTICE  = 0x04;
        public static final byte DATA    = 0x05;
        public static final byte CONTROL = 0x07;
    }

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}
     */
    public static final class Key {
        public static final byte NONE                           = 0x00;
        //UPDATE
        public static final byte OTA                            = 0x01;
        static final        byte OTA_BACK                       = 0x02;
        public static final byte INTO_REPLACE_WATCH_FACE        = 0x21;
        public static final byte INTO_REPLACE_WATCH_FACE_BACK   = 0x22;
        public static final byte RECEIVE_REPLACE_ID             = 0x43;
        public static final byte GET_WATCH_FACES                = 0x31;
        public static final byte GET_WATCH_FACES_BACK           = 0x32;
        public static final byte REPLACING_WATCH_FACE           = 0x06;
        public static final byte REPLACING_WATCH_FACE_COMPLETED = 0x04;
        public static final byte EXIT_REPLACING_WATCH_FACE      = 'q';

        //SET
        public static final byte SYNC_TIME_2_DEVICE = 0x01;
        public static final byte SYNC_TIME          = 0x46;
        public static final byte SET_USER_INFO      = 0x10;
        public static final byte READ_ALARM         = 0x2D;
        public static final byte READ_ALARM_BACK    = 0x2E;
        public static final byte SET_GOAL           = 0x05;
        public static final byte READ_BATTERY       = 0x08;
        static final        byte READ_BATTERY_BACK  = 0x09;
        public static final byte READ_VERSION       = 0x0A;
        static final        byte READ_VERSION_BACK  = 0x0B;

        public static final byte ENABLE_ANTI_LOST        = 0x20;
        public static final byte ENABLE_NO_DISTURB       = 0x2B;
        public static final byte ENABLE_CALL             = 0x26;
        public static final byte ENABLE_NOTIFICATION     = 0x27;
        public static final byte ENABLE_RAISE_ON         = 0x35;
        public static final byte ENABLE_DISPLAY_VERTICAL = 0x36;
        public static final byte ENABLE_DETECT_SLEEP     = 0x39;
        public static final byte INTO_TAKE_PHOTO         = 0x42;

        public static final byte SET_SEDENTARINESS = 0x2A;
        public static final byte SET_ALARMS        = 0x2C;
        public static final byte SET_HEART_RATE    = 0x44;
        public static final byte SET_UNIT          = 0x45;
        static final        byte SET_VIBRATION     = 0x2F;
        public static final byte SET_BACK_LIGHT    = 0x29;
        public static final byte SET_LANGUAGE      = 0x34;
        public static final byte SET_24HOUR        = 0x3B;
        public static final byte SET_NAME_GROUP    = 0x11;//IMED

        public static final byte ALARMS_CHANGE           = 0x60;
        public static final byte GOAL_CHANGE             = 0x61;
        public static final byte READ_GOAL               = 0x62;
        public static final byte READ_GOAL_BACK          = 0x63;
        public static final byte SEDENTARINESS_CHANGE    = 0x64;
        public static final byte READ_SEDENTARINESS      = 0x65;
        public static final byte READ_SEDENTARINESS_BACK = 0x66;
        public static final byte CAMERA_PRESSED          = 0x67;
        public static final byte SET_SYSTEM              = 0x23;
        public static final byte REQUEST_PAIR            = 0x47;
        public static final byte SET_ANTI_LOST_TEL       = 0x48;
        public static final byte SET_WEATHER             = 0x49;
        public static final byte SET_CYCLING_EXTRA       = 0x4A;
        public static final byte SET_FORECAST            = 0x4B;

        //CONNECT
        public static final byte BIND       = 0x01;
        static final        byte BIND_BACK  = 0x02;
        static final        byte LOGIN      = 0x03;
        static final        byte LOGIN_BACK = 0x04;
        public static final byte UNBIND     = 0x05;

        // NOTICE
        public static final byte CALL_INCOMING = 0x01;
        public static final byte CALL_OFF_HOOK = 0x02;
        public static final byte CALL_IDLE     = 0x03;
        public static final byte MESSAGE       = 0x51;
        public static final byte MESSAGE_v2    = 0x52;
        public static final byte FIND_PHONE    = 0x60;
        public static final byte FIND_DEVICE   = 0x61;
        public static final byte ARRIVE_AT     = 0x53;

        // DATA
        public static final byte EXERCISE            = 0x34;
        public static final byte EXERCISE2           = 0x35;
        public static final byte EXERCISE2_BACK      = 0x36;
        public static final byte SPORT               = 0x41;
        static final        byte SPORT_BACK          = 0x42;
        public static final byte RATE                = 0x43;
        static final        byte RATE_BACK           = 0x44;
        public static final byte SLEEP               = 0x45;
        static final        byte SLEEP_BACK          = 0x46;
        public static final byte TRACKER             = 0x47;
        static final        byte TRACKER_BACK        = 0x48;
        public static final byte BLOOD_PRESSURE      = 0x49;
        static final        byte BLOOD_PRESSURE_BACK = 0x4A;
        public static final byte CYCLING             = 0x4C;
        public static final byte CYCLING_BACK        = 0x4D;
        public static final byte SWIM                = 0x4E;
        public static final byte SWIM_BACK           = 0x4F;
    }

    public static final byte VIBRATION_TYPE_ALARM = 0x5;

    /**
     * Amount of millisecond between 1970-01-01~2000-01-01
     */
    private static final Long MS = 946684800000L;

    public           EaseConnector   mEaseConnector;
    private          Context         mContext;
    private          BluetoothSocket mBluetoothSocket;
    /**
     * Whether the device is logged in.
     */
    private volatile boolean         isLoggedIn;

    private static BluetoothAdapter sBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private static Handler          sHandler          = new Handler(Looper.getMainLooper());
    private        Runnable         mRunnable         = new Runnable() {

        @Override
        public void run() {
            for (SmaCallback connectorCallback : mSmaCallbacks) {
                connectorCallback.onReadDataFinished(false);
            }
        }
    };

    /**
     * The data you want read
     */
    private byte[] mDataKes;

    /**
     * The UUID of main {@link BluetoothGattService},{@link SmaManager#UUID_SERVICE_MAIN} or
     * {@link SmaManager#UUID_SERVICE_MAIN_ROUND}
     */
    private String mUUIDMainService;

    /**
     * UUID of the {@link BluetoothGattCharacteristic} for writing,{@link SmaManager#UUID_CHARACTER_WRITE} or
     * {@link SmaManager#UUID_CHARACTER_WRITE_ROUND}
     */
    private String mUUIDWrite;

    /**
     * UUID of the {@link BluetoothGattCharacteristic} for reading,{@link SmaManager#UUID_CHARACTER_READ} or
     * {@link SmaManager#UUID_CHARACTER_READ_ROUND}
     */
    private String mUUIDRead;

    private SmaMessenger mSmaMessenger;
    private List<SmaCallback> mSmaCallbacks = new ArrayList<>();

    private DateFormat               mDateFormat;
    private SharedPreferences        mPreferences;
    private SharedPreferences.Editor mEditor;
    private boolean           isExit    = false;
    /**
     * Receiver to receive broadcast with action {@link BluetoothAdapter#ACTION_STATE_CHANGED},so that you can start
     * connecting
     * when Bluetooth adapter get enabled when a remote device has been bond.
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(), BluetoothAdapter.ACTION_STATE_CHANGED)) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                L.d("SmaManager -> BluetoothAdapter.ACTION_STATE_CHANGED , state = " + state);
                switch (state) {
                    case BluetoothAdapter.STATE_ON:
                        if (isBond()) mEaseConnector.setAddress(getSavedAddress()).connect(true);
                        break;

                    case BluetoothAdapter.STATE_TURNING_OFF:
//                        isLoggedIn = false;
//                        isOta = false;
//                        mEaseConnector.connect(false);
                        break;
                }
            }
        }
    };

    public static synchronized SmaManager getInstance() {
        return SingletonHolder.sInstance;
    }

    private static class SingletonHolder {
        private static SmaManager sInstance = new SmaManager();
    }

    private SmaManager() {
        /**
         * Keep the timezone of {@link SmaManager#mDateFormat} as same as devices's.
         */
        mDateFormat = new SimpleDateFormat(SmaConsts.DATE_FORMAT_yyyy_MM_dd_HH_mm_ss, Locale.getDefault());
        mDateFormat.setTimeZone(SmaBleUtils.getDefaultTimeZone());
    }

    public SmaManager init(Context context) {
        isExit = false;
        mContext = context.getApplicationContext();
        mContext.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        mSmaMessenger = new SmaMessenger();

        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mEditor = mPreferences.edit();

        mEaseConnector = new EaseConnector(mContext).setGattCallback(new BluetoothGattCallback() {

            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                L.d("SmaManager onConnectionStateChange -> status = " + status + ",newState = " + newState);
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        L.d("SmaManager STATE_CONNECTED");
                        gatt.discoverServices();
                        clearAllTask();
                    }
                }

                if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    L.d("SmaManager STATE_DISCONNECTED");
                    mEaseConnector.closeConnect(!sBluetoothAdapter.isEnabled());
                    mEaseConnector.connect(sBluetoothAdapter.isEnabled() && !isExit);

                    if (isLoggedIn) {
                        for (SmaCallback bc : mSmaCallbacks) {
                            bc.onLogin(false);
                        }
                    }

                    isLoggedIn = false;
                    clearAllTask();
                }
            }

            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                L.d("SmaManager onServicesDiscovered");
                selectUUID(gatt);
                setNotify(mUUIDMainService, mUUIDRead, true);

                if (isBond()) {
                    login();
                } else {
                    bind(true);
                }
                for (SmaCallback bc : mSmaCallbacks) {
                    bc.onDeviceConnected(gatt.getDevice());
                }
            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                L.d("SmaManager onDescriptorWrite");
                mSmaMessenger.releasePackageSemaphore();
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                L.d("SmaManager onCharacteristicRead " + EaseUtils.byteArray2HexString(characteristic.getValue()));
                mSmaMessenger.releasePackageSemaphore();
                byte[] value = characteristic.getValue();
                if (value == null || value.length < 1) return;

                try {
                    if (characteristic.getUuid().equals(UUID.fromString(UUID_CHARACTER_FIRM_FLAG))) {//读到厂商号
                        String flag = new String(value, "UTF-8");
                        L.d("FIRMWARE_FLAG " + flag);
                        for (SmaCallback callback : mSmaCallbacks) {
                            callback.onReadFlag(flag);
                        }
                    } else if (characteristic.getUuid().equals(UUID.fromString(UUID_CHARACTER_CLASSIC_ADDRESS))) {
                        if (value.length != 6) return;

                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < 6; i++) {
                            String item = Integer.toHexString(value[i] & 0xff);
                            if (item.length() == 1) {
                                item = "0" + item;
                            }
                            if (i == 0) {
                                sb.append(item);
                            } else {
                                sb.append(":").append(item);
                            }
                        }
                        //读到圆表的经典蓝牙地址，通过地址直接连到经典蓝牙
                        final String address = sb.toString().toUpperCase();
                        saveClassicAddress(address);
                        L.d("CLASSIC_ADDRESS " + address);

                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                                BluetoothDevice device = sBluetoothAdapter.getRemoteDevice(address);
                                try {
                                    if (mBluetoothSocket != null) {
                                        mBluetoothSocket.close();
                                        mBluetoothSocket = null;
                                    }
                                    mBluetoothSocket = device.createRfcommSocketToServiceRecord(UUID.fromString
                                            ("00001101-0000-1000-8000-00805F9B34FB"));
                                    mBluetoothSocket.connect();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    if (mBluetoothSocket != null) {
                                        try {
                                            mBluetoothSocket.close();
                                        } catch (IOException e1) {
                                            e1.printStackTrace();
                                        } finally {
                                            mBluetoothSocket = null;
                                        }
                                    }
                                }
                            }
                        }).run();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                byte[] data = characteristic.getValue();
                L.d("SmaManager onCharacteristicWrite " + EaseUtils.byteArray2HexString(data));
                mSmaMessenger.releasePackageSemaphore();
                if (characteristic.getUuid().equals(UUID.fromString(mUUIDWrite))) {
                    if (data.length == 8 && ((data[0] & 0xff) == 0xAB)) {
                        if (((data[1] & 0xff) == 0x10 || (data[1] & 0xff) == 0x30)) {
                            mSmaMessenger.releaseReturnSemaphore();
                        }
                    }
                }
            }

            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                if (characteristic.getUuid().equals(UUID.fromString(mUUIDRead))) {
                    byte[] data = characteristic.getValue();
                    L.d("SmaManager onCharacteristicChanged " + EaseUtils.byteArray2HexString(data));
                    if (mSmaMessenger.isReplacingWatchFace && data.length == 1) {
                        handleReplaceWatchFace(data);
                    } else {
                        receiveData(data);
                    }
                }
            }
        });

        return this;
    }

    /**
     * See {@link EaseConnector#connect(boolean)}
     *
     * @param connect
     */
    public void connect(boolean connect) {
        L.d("SmaManager -> connect " + connect);
        if (connect) {
            if (!isBond() || !sBluetoothAdapter.isEnabled() || isLoggedIn) return;

            mEaseConnector.setAddress(getSavedAddress()).connect(true);
        } else {
            mEaseConnector.connect(false);
        }
    }

    /**
     * See {@link EaseConnector#closeConnect(boolean)}
     *
     * @param stopReconnect
     */
    public void close(boolean stopReconnect) {
        isLoggedIn = false;
        mEaseConnector.closeConnect(stopReconnect);
    }

    /**
     * Exit and release resource.
     */
    public void exit() {
        L.d("SmaManager -> exit");
        isExit = true;
        mContext.unregisterReceiver(mReceiver);
        mSmaMessenger.exit();
        mEaseConnector.exit();
        isLoggedIn = false;
    }

    /**
     * See {@link EaseConnector#setDevice(BluetoothDevice)}
     *
     * @param device
     */
    public void bindWithDevice(BluetoothDevice device) {
        L.d("SmaManager -> bindWithDevice " + device.getName() + " " + device.getAddress());
        mEaseConnector.setDevice(device);
        mEaseConnector.connect(true);
    }

    /**
     * Write data to device to bind with it,or unbind with the bond device and clear all its information saved in local.
     *
     * @param bind bind if true,else unbind
     */
    public void bind(boolean bind) {
        L.d("SmaManager -> bind " + bind);
        if (bind) {
            byte[] extra = new byte[32];
            extra[0] = (byte) -1;
            write(Cmd.CONNECT, Key.BIND, extra);
        } else {
            if (mBluetoothSocket != null) {
                try {
                    mBluetoothSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    mBluetoothSocket = null;
                }
            }
            removeDevice(getClassicAddress());
            saveClassicAddress("");

            if (isLoggedIn()) {
                write(Cmd.CONNECT, Key.UNBIND, new byte[]{0});
            }

            mEaseConnector.setAddress("");
            mEaseConnector.setDevice(null);
            isLoggedIn = false;

            final String address = getSavedAddress();
            saveNameAndAddress("", "");
            saveFirmwareVersion("");
            saveFirmwareFlag("");

            sHandler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    if (!TextUtils.isEmpty(address)) {
                        mEaseConnector.closeConnect(true);
                        removeDevice(address);
                    }
                }
            }, 1600);
        }
    }

    /**
     * Write data to the remote device to login it.
     */
    public void login() {
        L.d("SmaManager -> login");
        byte[] extra = new byte[32];
        extra[0] = (byte) -1;
        write(Cmd.CONNECT, Key.LOGIN, extra);
    }

    private boolean isReceiveId = false;

    /**
     * Process the replacing of watch face.
     *
     * @param data the data received
     */
    private void handleReplaceWatchFace(byte[] data) {
        int key = data[0] & 0xff;
        switch (key) {
            case Key.RECEIVE_REPLACE_ID:
                L.d("RECEIVE_REPLACE_ID " + mSmaMessenger.isReplacingWatchFace);
                if (!isReceiveId) {
                    isReceiveId = true;
                    for (SmaCallback callback : mSmaCallbacks) {
                        callback.onReceiveReplaceId();
                    }
                }
                break;
            case Key.REPLACING_WATCH_FACE:
                L.d("REPLACING_WATCH_FACE");
                if (isReceiveId) {
                    isReceiveId = false;
                }
                for (SmaCallback callback : mSmaCallbacks) {
                    callback.onReplacingWatchFace();
                }
                break;
        }
    }

    private byte[] mData   = new byte[0];
    private int    mLength = 0;

    /**
     * Process the data received.
     *
     * @param back the data sent by the bond device
     */
    private void receiveData(byte[] back) {
        if (back == null || back.length < 1) return;

        if ((back[0] & 0xFF) == 0xAB && back.length == 8 && mLength == 0) {//以0xAB开头，且长度为8
            if (((back[1] & 0xff) == 0x10 || (back[1] & 0xff) == 0x30)) {//如果是ACK或NACK
                //L.d("收到ACK或NACK" + Arrays.toString(back));
                mSmaMessenger.releaseReceiveSemaphore();
            } else {//不是ACK或NACK
                // L.d("receiveData>>>>返回的不是ACK和NACK = " +Arrays.toString(back));
                mData = new byte[0];
                mLength = (back[2] & 0xff) << 8;
                mLength |= back[3] & 0xff;
                mLength += 8;
            }
        }

        if (back.length <= mLength && mData.length < mLength) {
            mData = EaseUtils.concat(mData, back);
            if (mData.length == mLength) {
                mLength = 0;
                parseData();
            }
        }
    }

    /**
     * Parse the data received.
     */
    private synchronized void parseData() {
        L.d("parseData " + EaseUtils.byteArray2HexString(mData));
        int size = mData.length;
        byte[] crc = new byte[size];
        System.arraycopy(mData, 0, crc, 0, size);
        crc[4] = 0;
        crc[5] = 0;
        byte[] crcBytes = SmaBleHelper.cmdCRC(crc);

        String crcRt = EaseUtils.bytesArray2HexStringWithout0x(crcBytes);
        String dataRt = EaseUtils.bytesArray2HexStringWithout0x(mData);
        if (TextUtils.equals(crcRt, dataRt)) {
            returnACK(mData);
        }

        byte cmd = (byte) (mData[8] & 0xff);
        byte key = (byte) (mData[10] & 0xff);
        switch (cmd) {
            case Cmd.UPDATE:
                if (key == Key.OTA_BACK) {
                    boolean ok = mData[13] == 0;// 0 成功,1 失败
                    L.d("OTA_BACK " + ok);
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onOTA(ok);
                    }
                } else if (key == Key.INTO_REPLACE_WATCH_FACE_BACK) {
                    boolean ok = mData[13] == 0;
                    mSmaMessenger.isReplacingWatchFace = ok;
                    isReceiveId = false;
                    L.d("INTO_REPLACE_WATCH_FACE_BACK " + ok);
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onIntoReplaceWatch(ok);
                    }
                } else if (key == Key.GET_WATCH_FACES_BACK) {
                    int count = mData[13];
                    int[] ids = new int[count];
                    for (int i = 0; i < count; i++) {
                        ids[i] = (mData[14 + i * 4] & 0xFF) << 24
                                | (mData[15 + i * 4] & 0xFF) << 16
                                | (mData[16 + i * 4] & 0xFF) << 8 | mData[17 + i * 4];
                    }
                    L.d("GET_WATCH_FACES_BACK " + Arrays.toString(ids));
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onReadWatchFaces(ids);
                    }
                }
                break;

            case Cmd.SET:
                if (key == Key.READ_ALARM_BACK) {
                    int count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 23;
                    if (count > 0) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeZone(SmaBleUtils.getDefaultTimeZone());
                        List<SmaAlarm> alarms = new ArrayList<>();
                        byte[] b;
                        for (int i = 0; i < count; i++) {
                            SmaAlarm alarm = new SmaAlarm();
                            b = Arrays.copyOfRange(mData, 23 * i + 13, 23 * i + 36);
                            calendar.set(((b[0] >> 2) + 2000), (((b[0] & 0b11) << 2) | ((b[1] >> 6) & 0b11)) - 1,
                                    (b[1] >> 1) & 0b11111, ((b[1] & 0b1) << 4) | (((b[2] >> 4) & 0b1111)), ((b[2] &
                                            0b1111) << 2) | ((b[3] & 0xff) >> 6));
                            alarm.setTime(calendar.getTimeInMillis());
                            alarm.setId((b[3] & 0b111111) >> 3);
//                            alarm.setEnabled((b[3] & 0b111) >> 2);
                            alarm.setEnabled((b[4] & 0xff) >> 7);
                            alarm.setRepeat(b[4] & 0b1111111);
                            try {
                                String tag = new String(Arrays.copyOfRange(b, 5, 24), "UTF-8");
                                int index = tag.indexOf('\u0000');
                                alarm.setTag(tag.substring(0, index));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            alarms.add(alarm);
                        }
                        L.d("READ_ALARM_BACK " + alarms.toString());
                        for (SmaCallback bc : mSmaCallbacks) {
                            bc.onReadAlarm(alarms);
                        }
                    } else {
                        L.d("READ_ALARM_BACK " + 0);
                        for (SmaCallback bc : mSmaCallbacks) {
                            bc.onReadAlarm(null);
                        }
                    }
                } else if (key == Key.ALARMS_CHANGE) {
                    L.d("ALARMS_CHANGE");
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onAlarmsChange();
                    }
                } else if (key == Key.READ_VERSION_BACK) {
                    int a = mData[13] & 0xff;
                    int b = mData[14] & 0xff;
                    int c = mData[15] & 0xff;
                    String version = a + "." + b + "." + c;
                    L.d("READ_VERSION_BACK " + version);
                    saveFirmwareVersion(version);

                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onReadVersion(version, version);
                    }
                } else if (key == Key.READ_BATTERY_BACK) {
                    int battery = mData[13] & 0xff;
                    L.d("READ_BATTERY_BACK" + battery);
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onReadBattery(battery);
                    }
                } else if (key == Key.GOAL_CHANGE) {
                    L.d("GOAL_CHANGE");
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onGoalChange();
                    }
                } else if (key == Key.READ_GOAL_BACK) {
                    byte[] bytes = Arrays.copyOfRange(mData, 13, 17);
                    int goal = EaseUtils.bytesToInt(bytes);
                    L.d("READ_GOAL_BACK -> " + goal);
                    if (goal == 0) return;

                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onReadGoal(goal);
                    }
                } else if (key == Key.SEDENTARINESS_CHANGE) {
                    L.d("SEDENTARINESS_CHANGE");
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onSedentarinessChange();
                    }
                } else if (key == Key.READ_SEDENTARINESS_BACK) {
                    byte[] b = Arrays.copyOfRange(mData, 13, 21);
                    SmaSedentarinessSettings ss = new SmaSedentarinessSettings();
                    ss.setRepeat(b[0]);
                    ss.setEnd2((b[1] & 0xff) >> 3);
                    ss.setStart2(((b[1] & 0b111) << 2) | ((b[2] & 0xff) >> 6));
                    ss.setEnd1((b[2] & 0b111111) >> 1);
                    ss.setStart1(((b[2] & 0b1) << 4) | ((b[3] & 0xff) >> 4));
                    ss.setInterval(((b[3] & 0b1111) << 4) | ((b[4] & 0xff) >> 4));
                    L.d("READ_SEDENTARINESS_BACK -> " + ss.toString());
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onReadSedentariness(ss);
                    }
                } else if (key == Key.CAMERA_PRESSED) {
                    L.d("CAMERA_PRESSED ->");
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onTakePhoto();
                    }
                }
                break;

            case Cmd.CONNECT:
                boolean result = mData[13] == 0;
                if (key == Key.BIND_BACK) {
                    L.d("BIND_BACK " + result);
                    if (result) {
                        saveNameAndAddress(mEaseConnector.mBluetoothDevice.getName(), mEaseConnector.mBluetoothDevice
                                .getAddress());
                        mEaseConnector.setAddress(mEaseConnector.mBluetoothDevice.getAddress());
                        saveFirmwareVersion("");
                        saveFirmwareFlag("");

                        login();
                    }
                } else if (key == Key.LOGIN_BACK) {
                    L.d("LOGIN_BACK " + result);
                    if (result) {
                        isLoggedIn = true;
                        mEaseConnector.connect(false);

                        write(Cmd.SET, Key.SET_24HOUR, !android.text.format.DateFormat.is24HourFormat(mContext)); //设置小时制

                        SmaTime smaTime = new SmaTime();

                        write(Cmd.SET, Key.SYNC_TIME_2_DEVICE, smaTime);//同步手机上的时间到设备

                        write(Cmd.SET, Key.SET_LANGUAGE, SmaBleUtils.getLanguageCode(), 1);//设置语言

                        read(UUID_SERVICE_FIRM_FLAG, UUID_CHARACTER_FIRM_FLAG);//读取厂商号

                        read(UUID_SERVICE_FIRM_FLAG, UUID_CHARACTER_CLASSIC_ADDRESS);//圆表读经典蓝牙的地址

                        write(Cmd.SET, Key.READ_VERSION);//读版本号

                        write(Cmd.SET, Key.READ_BATTERY);//读电量
                    }

                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onLogin(result);
                    }
                }
                break;

            case Cmd.NOTICE:
                if (key == Key.FIND_PHONE) {
                    L.d("FIND_PHONE");
                    for (SmaCallback bc : mSmaCallbacks) {
                        bc.onFindPhone(mData[13] == 1);
                    }
                }
                break;

            case Cmd.DATA:
                int count = 0;
                if (key == Key.EXERCISE) {
                    count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 8;
                    if (count > 0) {
                        byte[] b;
                        for (int i = 0; i < count; i++) {
                            b = Arrays.copyOfRange(mData, 8 * i + 13, 8 * i + 21);
                            int mode = b[4] & 0xff;
//                            L.d(SmaBleUtils.byteArray2HexString(b));
                            if (mode == SmaSport.Mode.START) {
                                for (SmaCallback bc : mSmaCallbacks) {
                                    bc.onStartExercise(true);
                                }
                            } else if (mode == SmaSport.Mode.GOING) {
                                for (SmaCallback bc : mSmaCallbacks) {
                                    bc.onStartExercise(true);
                                }
                            }
                        }
                    }
                } else {
                    if (key == Key.SPORT_BACK) {
                        count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 8;
                        if (count > 0) {
                            List<SmaSport> list = new ArrayList<>();
                            byte[] b;
                            for (int i = 0; i < count; i++) {
                                SmaSport sport = new SmaSport();

                                b = Arrays.copyOfRange(mData, 8 * i + 13, 8 * i + 21);
                                long seconds = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        0, 4)), 16);
                                int step = Integer.parseInt(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b, 5,
                                        8)), 16);
                                sport.mode = b[4] & 0xff;
                                sport.time = MS + seconds * 1000;
                                sport.date = mDateFormat.format(new Date(sport.time));
                                sport.step = step;
                                list.add(sport);

                                if (sport.mode == SmaSport.Mode.START) {
                                    for (SmaCallback bc : mSmaCallbacks) {
                                        bc.onStartExercise(true);
                                    }
                                } else if (sport.mode == SmaSport.Mode.END) {
                                    for (SmaCallback bc : mSmaCallbacks) {
                                        bc.onStartExercise(false);
                                    }
                                }
                            }
                            System.out.println("SMA SPORT: " + list);
                            for (SmaCallback bc : mSmaCallbacks) {
                                bc.onReadSportData(list);
                            }
                        }
                    } else if (key == Key.SLEEP_BACK) {
                        count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 7;
                        if (count > 0) {
                            List<SmaSleep> list = new ArrayList<>();
                            byte[] b;
                            for (int i = 0; i < count; i++) {
                                SmaSleep sleep = new SmaSleep();

                                b = Arrays.copyOfRange(mData, 7 * i + 13, 7 * i + 20);
                                long seconds = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        0, 4)), 16);
                                int mode = b[4] & 0xff;
                                int soft = b[5] & 0xff;
                                int strong = b[6] & 0xff;

                                sleep.time = MS + seconds * 1000;
                                sleep.date = mDateFormat.format(new Date(sleep.time));
                                sleep.soft = soft;
                                sleep.mode = mode;
                                sleep.strong = strong;
                                list.add(sleep);
                            }

                            for (SmaCallback bc : mSmaCallbacks) {
                                bc.onReadSleepData(list);
                            }
                        }
                    } else if (key == Key.RATE_BACK) {
                        count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 5;
                        if (count > 0) {
                            List<SmaHeartRate> list = new ArrayList<>();
                            byte[] b;
                            for (int i = 0; i < count; i++) {
                                SmaHeartRate rate = new SmaHeartRate();

                                b = Arrays.copyOfRange(mData, 5 * i + 13, 5 * i + 18);
                                long seconds = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        0, 4)), 16);
                                if ((seconds % 60) == 1) {//如果秒数为1，为锻炼模式的心率
                                    rate.type = SmaHeartRate.Type.EXERCISE;
                                } else if ((seconds % 60) == 2) {//如果秒数为2，为i-MED锻炼模式的心率
                                    rate.type = SmaHeartRate.Type.EXERCISE_IMED;
                                }
                                int value = b[4] & 0xff;

                                rate.time = MS + seconds * 1000;
                                rate.date = mDateFormat.format(new Date(rate.time));
                                rate.value = value;
                                list.add(rate);
                            }

                            for (SmaCallback bc : mSmaCallbacks) {
                                bc.onReadHeartRateData(list);
                            }
                        }
                    } else if (key == Key.BLOOD_PRESSURE_BACK) {
                        count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 6;
                        if (count > 0) {
                            List<SmaBloodPressure> list = new ArrayList<>();
                            byte[] b;
                            for (int i = 0; i < count; i++) {
                                SmaBloodPressure pressure = new SmaBloodPressure();

                                b = Arrays.copyOfRange(mData, 6 * i + 13, 6 * i + 19);
                                long seconds = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        0, 4)), 16);
                                pressure.systolic = b[4] & 0xff;
                                pressure.diastolic = b[5] & 0xff;
                                pressure.time = MS + seconds * 1000;
                                pressure.date = mDateFormat.format(new Date(pressure.time));
                                list.add(pressure);
                            }

                            for (SmaCallback bc : mSmaCallbacks) {
                                bc.onReadBloodPressure(list);
                            }
                        }
                    } else if (key == Key.CYCLING_BACK || key == Key.SWIM_BACK) {
                        count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 7;
                        if (count > 0) {
                            List<SmaCycling> list = new ArrayList<>();
                            byte[] b;
                            for (int i = 0; i < count; i++) {
                                SmaCycling cycling = new SmaCycling();

                                b = Arrays.copyOfRange(mData, 7 * i + 13, 7 * i + 20);
                                long seconds = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        0, 4)), 16);
                                if (key == Key.CYCLING_BACK) {
                                    if (seconds == 0 || seconds == 0xFFFFFFFEL || seconds == 0xFFFFFFFFL) {//骑行
                                        int status;
                                        if (seconds == 0) {
                                            status = SmaCycling.START;
                                        } else if (seconds == 0xFFFFFFFEL) {
                                            status = SmaCycling.GOING;
                                        } else {
                                            status = SmaCycling.END;
                                        }
                                        for (SmaCallback bc : mSmaCallbacks) {
                                            bc.onCycle(status);
                                        }
                                        return;
                                    }
                                }
                                if (key == Key.CYCLING_BACK) {
                                    cycling.type = SmaCycling.TYPE_CYCLING;
                                } else if (key == Key.SWIM_BACK) {
                                    cycling.type = SmaCycling.TYPE_SWIM;
                                }
                                cycling.time = MS + seconds * 1000;
                                cycling.date = mDateFormat.format(new Date(cycling.time));

                                cycling.cal = ((b[4] & 0xff) << 8) | (b[5] & 0xff);
                                cycling.rate = b[6] & 0xff;
                                list.add(cycling);
                            }
                            System.out.println("SMA CALORIES CYL: " + list);
                            for (SmaCallback bc : mSmaCallbacks) {
                                bc.onReadCycling(list);
                            }
                        }
                    } else if (key == Key.EXERCISE2_BACK) {
                        count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 34;
                        if (count > 0) {
                            List<SmaExercise> list = new ArrayList<>();
                            byte[] b;
                            for (int i = 0; i < count; i++) {
                                SmaExercise exercise = new SmaExercise();
                                b = Arrays.copyOfRange(mData, 34 * i + 13, 34 * i + 47);
                                long start = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        0, 4)), 16);
                                exercise.start = MS + start * 1000;
                                exercise.date = mDateFormat.format(new Date(exercise.start));

                                long end = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        4, 8)), 16);
                                exercise.end = MS + end * 1000;
                                exercise.duration = Integer.parseInt(EaseUtils.bytesArray2HexStringWithout0x(Arrays
                                        .copyOfRange(b, 8, 12)), 16);
                                exercise.altitude = ((b[12] & 0xff) << 8) | (b[13] & 0xff);
                                exercise.airPressure = ((b[14] & 0xff) << 8) | (b[15] & 0xff);
                                exercise.spm = b[16] & 0xff;
                                exercise.type = b[17] & 0xff;
                                exercise.step = Integer.parseInt(EaseUtils.bytesArray2HexStringWithout0x(Arrays
                                        .copyOfRange(b, 18, 22)), 16);
                                exercise.distance = Integer.parseInt(EaseUtils.bytesArray2HexStringWithout0x(Arrays
                                        .copyOfRange(b, 22, 26)), 16);
                                exercise.cal = Integer.parseInt(EaseUtils.bytesArray2HexStringWithout0x(Arrays
                                        .copyOfRange(b, 26, 30)), 16);
                                exercise.speed = ((b[30] & 0xff) << 8) | (b[31] & 0xff);
                                exercise.pace = ((b[32] & 0xff) << 8) | (b[33] & 0xff);
                                list.add(exercise);
                            }
                            System.out.println("SMA CALORIES: " + list);
                            for (SmaCallback bc : mSmaCallbacks) {
                                bc.onReadExercise(list);
                            }
                        }
                    } else if (key == Key.TRACKER_BACK) {
                        count = (((mData[11] & 0xff) << 8) | mData[12] & 0xff) / 16;
                        if (count > 0) {
                            List<SmaTracker> list = new ArrayList<>();
                            byte[] b;
                            for (int i = 0; i < count; i++) {
                                SmaTracker tracker = new SmaTracker();
                                b = Arrays.copyOfRange(mData, 16 * i + 13, 16 * i + 29);
                                long start = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays.copyOfRange(b,
                                        0, 4)), 16);
                                tracker.start = MS + start * 1000;
                                long offset = ((b[4] & 0xff) << 8) | (b[5] & 0xff);
                                tracker.time = tracker.start + offset * 1000;
                                tracker.date = mDateFormat.format(new Date(tracker.time));
                                tracker.latitude = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays
                                        .copyOfRange(b, 8, 12)), 16) * 1.0 / 1000000;
                                tracker.longitude = Long.parseLong(EaseUtils.bytesArray2HexStringWithout0x(Arrays
                                        .copyOfRange(b, 12, 16)), 16) * 1.0 / 1000000;
                                list.add(tracker);
                            }
                            for (SmaCallback bc : mSmaCallbacks) {
                                bc.onReadTracker(list);
                            }
                        }
                    }

                    onDataRead((byte) (key - 1), count);
                }
                break;
            case Cmd.CONTROL:
                if (key == 0x01) {
                    L.d("CONTROL " + mData[13]);
                    for (SmaCallback connectorCallback : mSmaCallbacks) {
                        connectorCallback.onKeyDown(mData[13]);
                    }
                }
                break;
        }
    }

    /**
     * Return ACK to device when you receive data from it.
     *
     * @param rt
     */
    private void returnACK(byte[] rt) {
        byte[] ack = new byte[8];
        ack[0] = (byte) 0xAB;
        ack[1] = 0x10;
        ack[6] = rt[6];
        ack[7] = rt[7];
        L.d("SmaManager -> returnACK " + EaseUtils.byteArray2HexString(ack));
        mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector.getGattCharacteristic
                (mUUIDMainService, mUUIDWrite), ack, SmaMessenger.MessageType.WRITE, false, true));
    }

    /**
     * @return True if the bond device is logged in,else false,see {@link SmaCallback#onLogin(boolean)}
     */
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public boolean isCalling = false;

    /**
     * Save the name and address of the remote device.
     *
     * @param name    name of the remote device
     * @param address address of the remote device
     */
    public void saveNameAndAddress(String name, String address) {
        mEditor.putString(SP_DEVICE_NAME, name).apply();
        mEditor.putString(SP_DEVICE_ADDRESS, address).apply();
    }

    /**
     * Save the address of the remote classic Bluetooth device.
     *
     * @param address address of the remote classic Bluetooth device
     */
    private void saveClassicAddress(String address) {
        mEditor.putString(SP_CLASSIC_ADDRESS, address).apply();
    }

    /**
     * Return the name of the bond device.
     *
     * @return If a device has been bond,return its name,or a empty String.
     */
    public String getSavedName() {
        return mPreferences.getString(SP_DEVICE_NAME, "");
    }

    /**
     * Return the address of the bond device.
     *
     * @return If a device has been bond,return its address,or a empty String.
     */
    public String getSavedAddress() {
        return mPreferences.getString(SP_DEVICE_ADDRESS, "");
    }

    /**
     * Return the address of the bond classic device.
     *
     * @return If a classic device has been bond,return its address,or a empty String.
     */
    public String getClassicAddress() {
        return mPreferences.getString(SP_CLASSIC_ADDRESS, "");
    }

    /**
     * Whether any device has been bond.
     *
     * @return true if a device has been bond,else false.
     */
    public boolean isBond() {
        return !TextUtils.isEmpty(getSavedAddress());
    }

    /**
     * Save the device type of the remote device.
     *
     * @param deviceType the device type of the remote device
     */
    public void saveDeviceType(String deviceType) {
        mEditor.putString(SP_DEVICE_TYPE, deviceType).apply();
    }

    /**
     * Return the device type of the bond device.
     *
     * @return If a device has been bond,return its device type,or a empty String.
     */
    public String getDeviceType() {
        return mPreferences.getString(SP_DEVICE_TYPE, "");
    }

    /**
     * Save the firmware version of the remote device.
     *
     * @param version the firmware version of the remote device
     */
    public void saveFirmwareVersion(String version) {
        mEditor.putString(SP_FIRMWARE, version).apply();
    }

    /**
     * Return the firmware version of the bond device.
     *
     * @return If a device has been bond,return its firmware version,or a empty String.
     */
    public String getFirmwareVersion() {
        return mPreferences.getString(SP_FIRMWARE, "");
    }

    /**
     * Save the firmware flag of the remote device.
     *
     * @param flag the firmware flag of the remote device
     */
    public void saveFirmwareFlag(String flag) {
        mEditor.putString(SP_FIRMWARE_FLAG, flag).apply();
    }

    /**
     * Return the firmware flag of the bond device.
     *
     * @return If a device has been bond,return its firmware flag,or a empty String.
     */
    public String getFirmwareFlag() {
        return mPreferences.getString(SP_FIRMWARE_FLAG, "");
    }

    public void setPhoneFlag(int flag) {
        mEditor.putInt(SP_PHONE_FLAG, flag).apply();
    }

    public int getPhoneFlag() {
        return mPreferences.getInt(SP_PHONE_FLAG, 1);
    }

    /**
     * 利用反射
     * 将设备从手机蓝牙设置中的已配对设备中移除，防止三星J5008等手机解除绑定后，在手机蓝牙设置中没有清除已配对设备信息，导致再次连接时会被动断开。
     *
     * @param address 设备地址
     */
    private void removeDevice(String address) {
        try {
            BluetoothDevice device = sBluetoothAdapter.getRemoteDevice(address);
            Method m = device.getClass().getMethod("removeBond", (Class[]) null);
            boolean success = (Boolean) m.invoke(device, (Object[]) null);
            L.d("removeDevice -> " + success);
        } catch (Exception e) {
            L.w("removeDevice = " + e.getMessage());
        }
    }

    /**
     * Add a callback to the list of callbacks to receive the result of communication with Bluetooth device.
     *
     * @param cb the callback to be added
     * @return This SmaManager object.
     */
    public SmaManager addSmaCallback(SmaCallback cb) {
        mSmaCallbacks.add(cb);
        return this;
    }

    /**
     * Remove a callback from the list of callbacks.
     *
     * @param cb the callback to be removed
     */
    public void removeSmaCallback(SmaCallback cb) {
        mSmaCallbacks.remove(cb);
    }

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}
     *
     * @param cmd
     * @param key
     */
    public void write(byte cmd, byte key) {
        write(cmd, key, new byte[]{});
    }

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}.
     *
     * @param cmd
     * @param key
     * @param b
     */
    public void write(byte cmd, byte key, boolean b) {
        write(cmd, key, new byte[]{(byte) (b ? 1 : 0)});
    }

    /**
     * The data written to device depends on {@link Cmd},{@link Key} and extra data.It will generate a byte array by
     * invoking  {@link SmaBleHelper#getBytes(byte, byte, byte[])}.We write the result to the bond device by invoking
     * this method.If the length of the result is greater than 20,it will be split in several byte arrays those length
     * are less than 21.And then,these data will be written to device sequentially.
     *
     * @param cmd   {@link Cmd}
     * @param key   {@link Key}
     * @param extra the extra data
     */
    public void write(byte cmd, byte key, byte[] extra) {
//        L.d("write -> cmd = " + cmd + ",key = " + key + ",data = " + EaseUtils.byteArray2HexString(extra));
        if (cmd != Cmd.CONNECT && !isLoggedIn) return;

        if (cmd == Cmd.NONE && key == Key.NONE) {
            mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector.getGattCharacteristic
                    (mUUIDMainService, mUUIDWrite), extra, SmaMessenger.MessageType.WRITE));
        } else {
            if (key == Key.CALL_IDLE || key == Key.CALL_OFF_HOOK) {
                if (mSmaMessenger.isReplacingWatchFace) {
                    L.w("正在换表盘，不推送消息");
                    return;
                }
            }

            if (key == Key.ENABLE_NO_DISTURB) {
                byte[] tem = new byte[13];
                tem[0] = extra[0];
                if (extra[0] == 1) {
                    tem[3] = 23;
                    tem[4] = 59;
                }
                byte[] data = SmaBleHelper.getBytes(cmd, key, tem);
                mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector
                        .getGattCharacteristic
                                (mUUIDMainService, mUUIDWrite), data, SmaMessenger.MessageType.WRITE, isNeedReturnACK
                        (cmd, key)));
            } else {
                byte[] data = SmaBleHelper.getBytes(cmd, key, extra);
                mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector
                        .getGattCharacteristic
                                (mUUIDMainService, mUUIDWrite), data, SmaMessenger.MessageType.WRITE, isNeedReturnACK
                        (cmd, key)));
            }
        }
        if (cmd == Cmd.SET) {
            if (key == Key.ENABLE_ANTI_LOST || key == Key.ENABLE_NO_DISTURB || key == Key.ENABLE_CALL || key
                    == Key.ENABLE_NOTIFICATION || key == Key.ENABLE_DISPLAY_VERTICAL || key == Key
                    .ENABLE_DETECT_SLEEP || key == Key.ENABLE_RAISE_ON) {
                mEditor.putBoolean("enabled" + cmd + String.valueOf(key), extra[0] == 1).apply();
            }
            if (key == Key.SET_BACK_LIGHT) {
                mEditor.putInt(SmaConsts.SP_BACK_LIGHT, extra[0]).apply();
            }
        }
    }

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}.
     *
     * @param cmd   cmd
     * @param key   key
     * @param value value
     * @param digit sometimes the value you set may be out of range -128~127,at this case we should convert it to a byte
     *              array by
     *              invoking {@link EaseUtils#intToBytes(int)},so that we should know how many bytes can hold the value.
     */
    public void write(byte cmd, byte key, int value, int digit) {
        if (cmd == Cmd.NONE && key == Key.NONE) {
            if (value == Key.REPLACING_WATCH_FACE_COMPLETED) {
                mSmaMessenger.isReplacingWatchFace = false;
            }

            if (digit == 1) {
                mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector
                        .getGattCharacteristic
                                (mUUIDMainService, mUUIDWrite), new byte[]{(byte) value}, SmaMessenger.MessageType.WRITE));
            } else if (digit == 4) {
                byte[] extra = EaseUtils.intToBytes(value);
                mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector
                        .getGattCharacteristic
                                (mUUIDMainService, mUUIDWrite), extra, SmaMessenger.MessageType.WRITE));
            }
        } else {
            if (digit == 1) {
                write(cmd, key, new byte[]{(byte) value});
            } else if (digit == 4) {
                byte[] extra = EaseUtils.intToBytes(value);
                write(cmd, key, extra);
            }
        }
    }

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}.
     *
     * @param cmd
     * @param key
     * @param smaCmd
     */
    public void write(byte cmd, byte key, ISmaCmd smaCmd) {
        write(cmd, key, smaCmd.toByteArray());
    }

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}.
     *
     * @param cmd
     * @param key
     * @param list
     */
    public void write(byte cmd, byte key, List<? extends ISmaCmd> list) {
        byte[] data = new byte[0];
        for (ISmaCmd smaCmd : list) {
            data = EaseUtils.concat(data, smaCmd.toByteArray());
        }
        write(cmd, key, data);
    }

    /**
     * See {@link SmaManager#write(byte, byte, byte[])}.
     *
     * @param cmd
     * @param key
     * @param title
     * @param content
     */
    public void write(byte cmd, byte key, String title, String content) {
        if (mSmaMessenger.isReplacingWatchFace) {
            L.w("正在换表盘，不推送消息");
            return;
        }
        try {
            if (TextUtils.isEmpty(title)) {
                title = "";
            }
            if (TextUtils.isEmpty(content)) {
                content = "";
            }

            byte[] titles;
            byte[] contents;

            titles = title.getBytes("UTF-8");
            contents = content.getBytes("UTF-8");

            L.d("title = " + title + ",content = " + content);
            if (cmd == Cmd.SET && key == Key.SET_NAME_GROUP) {
                if (titles.length > 32) {
                    titles = Arrays.copyOf(titles, 32);
                }
                if (contents.length > 32) {
                    contents = Arrays.copyOf(contents, 32);
                }

                byte[] name = new byte[32];
                System.arraycopy(titles, 0, name, 0, titles.length);

                byte[] group = new byte[32];
                System.arraycopy(contents, 0, group, 0, contents.length);

                write(cmd, key, EaseUtils.concat(name, group));
                return;
            }

            byte[] extra = new byte[32 + (contents.length > 199 ? 199 : contents.length)];
            for (int i = 0, l = extra.length; i < l; i++) {
                if (i < 32) {
                    if (i < titles.length) {
                        extra[i] = titles[i];
                    }
                } else {
                    extra[i] = contents[i - 32];
                }
            }
            write(cmd, key, extra);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void readData(byte[] keys) {
        if (keys == null || keys.length < 1) return;

        this.mDataKes = keys;
        write(Cmd.DATA, keys[0]);
        sHandler.postDelayed(mRunnable, 8000);
    }

    private void onDataRead(byte key, int count) {
        sHandler.removeCallbacks(mRunnable);

        int max = getMaxItem(key);
        if (!isLastData(key)) {
            if (count == max) {
                write(Cmd.DATA, key);
            } else {
                int index = 0;
                for (byte cKey : mDataKes) {
                    index++;
                    if (cKey == key) break;
                }
                write(Cmd.DATA, mDataKes[index]);
            }
        } else {
            if (count == max) {
                write(Cmd.DATA, key);
            }
        }

        if (isLastData(key) && count < max) {
            for (SmaCallback callback : mSmaCallbacks) {
                callback.onReadDataFinished(true);
            }
        } else {
            sHandler.postDelayed(mRunnable, 8000);
        }
    }

    private boolean isLastData(byte key) {
        return key == mDataKes[mDataKes.length - 1];
    }

    private int getMaxItem(byte key) {
        if (key == Key.EXERCISE2) {
            return 5;
        }

        if (key == Key.TRACKER) {
            return 10;
        }

        return 20;
    }

    /**
     * Enable or disable notifications for given UUIDs.
     *
     * @param serviceUUID        UUID of {@link BluetoothGattService}
     * @param CharacteristicUUID UUID of {@link BluetoothGattCharacteristic}
     * @param enable             Set true to enable notifications,false to disable notifications
     */
    public void setNotify(String serviceUUID, String CharacteristicUUID, boolean enable) {
        mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector.getGattCharacteristic
                (serviceUUID, CharacteristicUUID), enable ? new byte[0] : null, SmaMessenger.MessageType.NOTIFY));
    }

    /**
     * Read requested the characteristic for given UUIDs.
     *
     * @param serviceUUID        UUID of {@link BluetoothGattService}
     * @param CharacteristicUUID UUID of {@link BluetoothGattCharacteristic}
     */
    public void read(String serviceUUID, String CharacteristicUUID) {
        mSmaMessenger.addMessage(new SmaMessenger.SmaMessage(mEaseConnector.mGatt, mEaseConnector.getGattCharacteristic
                (serviceUUID, CharacteristicUUID), null, SmaMessenger.MessageType.READ));
    }

    /**
     * Remove all tasks in task queue.We call this method when the device get connected or disconnected,even thought the
     * tasks
     * have not been processed at that moment.
     */
    public void clearAllTask() {
        L.d("SmaManager clearAllTask");
        mData = new byte[0];
        mLength = 0;
        mSmaMessenger.clearAllTask();
    }

    /**
     * Return whether the function is enabled
     *
     * @param cmd {@link Cmd}
     * @param key {@link Key}
     * @return True if enabled,else false.
     */
    public boolean getEnabled(byte cmd, byte key) {
        return mPreferences.getBoolean("enabled" + cmd + String.valueOf(key), false);
    }

    /**
     * Enable or disable notifications of an application,this method has nothing to do with device,it just saves a flag
     * in local.
     *
     * @param packageName package name of a application
     * @param enabled     Whether to enable notification of the given package name
     */
    public void putPackage(String packageName, boolean enabled) {
        L.d("putPackage " + packageName + "," + enabled);
        mEditor.putBoolean(packageName + "isPush", enabled).apply();
    }

    /**
     * Return whether notifications of an application is enabled
     *
     * @param packageName package name of a application
     * @return True if enabled,else false.
     */
    public boolean getPackage(String packageName) {
        boolean isPush = mPreferences.getBoolean(packageName + "isPush", false);
        L.d("getPackage " + packageName + "," + isPush);
        return isPush;
    }

    /**
     * Set the times of vibration.
     *
     * @param type  the type of vibration,there is just only a type {@link SmaManager#VIBRATION_TYPE_ALARM} we have used
     *              for the
     *              time being
     * @param times the times of vibration you want to set
     */
    public void setVibrationTimes(int type, int times) {
        L.d("setVibrationTimes " + type + " " + times);
        mEditor.putInt(SmaConsts.SP_VIBRATION + type, times).apply();

        byte[] extra = new byte[2];
        extra[0] = (byte) type;
        extra[1] = (byte) times;

        write(Cmd.SET, Key.SET_VIBRATION, extra);
    }

    /**
     * Get the times of vibration
     *
     * @param type the type of vibration,there is only one type {@link SmaManager#VIBRATION_TYPE_ALARM} we have used for the
     *             time being
     * @return Times of vibration for given type.
     */
    public int getVibrationTimes(int type) {
        int times = mPreferences.getInt(SmaConsts.SP_VIBRATION + type, 4);
        L.d("getVibrationTimes " + type + " " + times);
        return times;
    }

    /**
     * Get back light time of the device.
     *
     * @return Amount of seconds the back light holds.
     */
    public int getBackLight() {
        int time = mPreferences.getInt(SmaConsts.SP_BACK_LIGHT, 2);
        L.d("getBackLight " + time);
        return time;
    }

    /**
     * Set the flag {@link SmaMessenger#isReplacingWatchFace} to false.
     */
    public void stopReplaceWatchFace() {
        L.d("stopReplaceWatchFace");
        mSmaMessenger.isReplacingWatchFace = false;
    }

    public void setAntiLostTel(String nickName, String tel) {
        if (TextUtils.isEmpty(nickName) || TextUtils.isEmpty(tel)) return;

        try {
            byte[] nicknames = nickName.getBytes("UTF-8");
            byte[] tels = tel.getBytes("UTF-8");

            byte[] extra = new byte[22 + tels.length];
            for (int i = 0, l = extra.length; i < l; i++) {
                if (i < 22) {
                    if (i < nicknames.length) {
                        extra[i] = nicknames[i];
                    }
                } else {
                    extra[i] = tels[i - 22];
                }
            }
            write(Cmd.SET, Key.SET_ANTI_LOST_TEL, extra);
            L.d("setAntiLostTel " + nickName + "," + tel);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * The UUIDs of {@link BluetoothGattService} and {@link BluetoothGattCharacteristic} are not the same in different
     * device,we
     * should select UUIDs according whether they are offered by a device.
     *
     * @param gatt GATT
     */
    public void selectUUID(BluetoothGatt gatt) {
        if (gatt == null) return;

        BluetoothGattService gattService = gatt.getService(UUID.fromString(UUID_SERVICE_MAIN_ROUND));
        if (gattService != null) {
            mUUIDMainService = UUID_SERVICE_MAIN_ROUND;
            mUUIDRead = UUID_CHARACTER_READ_ROUND;
            mUUIDWrite = UUID_CHARACTER_WRITE_ROUND;
        } else {
            mUUIDMainService = UUID_SERVICE_MAIN;
            mUUIDRead = UUID_CHARACTER_READ;
            mUUIDWrite = UUID_CHARACTER_WRITE;
        }
    }

    /**
     * If you will receive data asynchronous after you write data to the associated remote device with given {@link Cmd} and
     * {@link Key},you should return a ACK to the device to tell it that you have received the data it sent.This method
     * return
     * whether you need to return ACK when you write data to the associated remote device with given {@link Cmd} and
     * {@link Key}.
     *
     * @param cmd {@link Cmd}
     * @param key {@link Key}
     * @return True if need,else false.
     */
    private boolean isNeedReturnACK(byte cmd, byte key) {
        if (cmd == Cmd.UPDATE) {
            return key == Key.OTA || key == Key.INTO_REPLACE_WATCH_FACE || key == Key.GET_WATCH_FACES;
        }
        if (cmd == Cmd.CONNECT) {
            return key == Key.LOGIN || key == Key.BIND;
        }
        if (cmd == Cmd.SET) {
            return key == Key.READ_ALARM || key == Key.READ_GOAL || key == Key.READ_BATTERY || key == Key.READ_VERSION ||
                    key == Key.READ_SEDENTARINESS;
        }
        if (cmd == Cmd.DATA) {
            return key == Key.SPORT || key == Key.RATE || key == Key.SLEEP || key == Key.BLOOD_PRESSURE || key == Key
                    .CYCLING || key == Key.EXERCISE2 || key == Key.TRACKER || key == Key.SWIM;
        }
        return false;
    }
}
