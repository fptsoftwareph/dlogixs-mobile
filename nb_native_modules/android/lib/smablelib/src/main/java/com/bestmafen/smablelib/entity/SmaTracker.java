package com.bestmafen.smablelib.entity;

/**
 * Created by Administrator on 2017/3/20.
 */

public class SmaTracker {
    public long   id;
    public String account;
    public String date;
    public long   start;
    public long   time;
    public double latitude;
    public double longitude;
    public int    altitude;//手机定位获取的海拔数据
    public int    synced;

    @Override
    public String toString() {
        return "SmaTracker{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", date='" + date + '\'' +
                ", start=" + start +
                ", time=" + time +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", altitude=" + altitude +
                ", synced=" + synced +
                '}';
    }
}
