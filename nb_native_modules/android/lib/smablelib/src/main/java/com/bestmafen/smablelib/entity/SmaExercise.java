package com.bestmafen.smablelib.entity;

/**
 * Created by Administrator on 2017/11/25.
 */

public class SmaExercise {
    /**
     * Q2，B2跑步
     */
    public static final int RUNNING = -1;

    /**
     * B2骑行
     */
    public static final int CYCLING_B = -2;

    public static final int INDOOR    = 0;
    public static final int OUTDOOR   = 1;
    public static final int CYCLING_M = 2;
    public static final int CLIMB     = 3;
    public static final int MARATHON  = 4;
    public static final int SWIMMING  = 5;

    public long   id;
    public long   start;
    public long   end;
    public int    duration;
    public String date;

    /**
     * 海拔 m
     */
    public int altitude;

    /**
     * 气压 KPa
     */
    public int airPressure;

    /**
     * 步频 step/min
     */
    public int spm;
    public int type;
    public int step;

    /**
     * 距离 m
     */
    public int distance;

    /**
     * 卡路里
     */
    public int cal;

    /**
     * 速度 km/h
     */
    public int speed;

    /**
     * min/km
     */
    public int    pace;
    public String account;
    public int    synced;

    @Override
    public String toString() {
        return "SmaExercise{" +
                "id=" + id +
                ", start=" + start +
                ", end=" + end +
                ", duration=" + duration +
                ", date='" + date + '\'' +
                ", altitude=" + altitude +
                ", airPressure=" + airPressure +
                ", spm=" + spm +
                ", type=" + type +
                ", step=" + step +
                ", distance=" + distance +
                ", cal=" + cal +
                ", speed=" + speed +
                ", pace=" + pace +
                ", account='" + account + '\'' +
                ", synced=" + synced +
                '}';
    }
}
