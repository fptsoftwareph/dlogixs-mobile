package com.bestmafen.smablelib.entity;

/**
 * Created by Administrator on 2017/9/23.
 */

public class SmaCycling {
    public static final int START        = 0;
    public static final int GOING        = 1;
    public static final int END          = 2;
    public static final int TYPE_CYCLING = 0;
    public static final int TYPE_SWIM    = 1;

    public long id;
    public int type = TYPE_CYCLING;
    public long   time;
    public String date;
    public long   start;
    public String dateStart;
    public long   end;
    public String dateEnd;
    public int    cal;
    public int    rate;
    public String account;
    public int    synced;

    @Override
    public String toString() {
        String title = "";
        if (type == TYPE_CYCLING) {
            title = "SmaCycling";
        } else if (type == TYPE_SWIM) {
            title = "SmaSwim";
        }
        return title + "{" +
                "id=" + id +
                ", type=" + type +
                ", time=" + time +
                ", date='" + date + '\'' +
                ", start=" + start +
                ", dateStart='" + dateStart + '\'' +
                ", end=" + end +
                ", dateEnd='" + dateEnd + '\'' +
                ", cal=" + cal +
                ", rate=" + rate +
                ", account=" + account + '\'' +
                ", synced=" + synced +
                '}';
    }
}
