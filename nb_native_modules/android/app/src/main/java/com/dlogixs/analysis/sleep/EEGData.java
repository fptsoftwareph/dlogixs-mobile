package com.dlogixs.analysis.sleep;

public class EEGData {
	private long[] time_acq = new long[1440];
	private double[] signal_min = new double[1440];
	private int[] signal_poor = new int[1440];
	private int duration_in_min = 0;
	
	public EEGData(long[] time_acq, double[] signal_min, int[] signal_poor, int duration_in_min) {
		super();
		this.time_acq = time_acq;
		this.signal_min = signal_min;
		this.signal_poor = signal_poor;
		this.duration_in_min = duration_in_min;
	}

	public long[] getTimeAcq() {
		return time_acq;
	}

	public double[] getSignalMin() {
		return signal_min;
	}

	public int[] getSignalPoor() {
		return signal_poor;
	}

	public int getDurationInMin() {
		return duration_in_min;
	}
	
}
