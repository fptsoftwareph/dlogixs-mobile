package com.dlogixs.analysis.curation;

import java.text.DecimalFormat;
import java.util.Random;

public class Record {

	public final static int ELEMENT_TST = 0;
	public final static int ELEMENT_GTBT = 1;
	public final static int ELEMENT_SUN = 2;
	public final static int ELEMENT_WALK = 3;
	public final static int ELEMENT_COFFEE = 4;
	public final static int ELEMENT_SMOKE = 5;
	public final static int ELEMENT_DRINK = 6;
	public final static int ELEMENT_BB = 7;

	public final static int INDEX_AWAKEN = 0;
	public final static int INDEX_CYCLE = 1;
	public final static int INDEX_STABLE = 2;
	public final static int INDEX_TOTAL = 3;
	public final static int INDEX_STANDARD = 4;

	public final static int BINAURAL_BEAT_3kHz = 0;
	public final static int BINAURAL_BEAT_6kHz = 1;
	public final static int BINAURAL_BEAT_9kHz = 2;
	public final static int BINAURAL_BEAT_12kHz = 3;

	private String date;
	private int[] element = new int[8];
	private int[] index = new int[5];
	private int[] element_binaural_beat = new int[4];
	private int index_binaural_beat;
	private int direction;
	private int difference;
	private boolean isUpdated;
	
	public Record(){
		this.date = getCurrentDate();
		
		this.element[ELEMENT_TST] = generateRandomInteger(320,470);
		this.element[ELEMENT_GTBT] = generateRandomInteger(-50,100);
		this.element[ELEMENT_SUN] = generateRandomInteger(60,240);
		this.element[ELEMENT_WALK] = generateRandomInteger(2000,10000);
		
		this.element[ELEMENT_COFFEE] = generateRandomInteger(0,5);
		this.element[ELEMENT_SMOKE] = generateRandomInteger(10,25);
		this.element[ELEMENT_DRINK] = generateRandomInteger(0,5);

		while((element_binaural_beat[BINAURAL_BEAT_3kHz]+element_binaural_beat[BINAURAL_BEAT_6kHz]+element_binaural_beat[BINAURAL_BEAT_9kHz]+element_binaural_beat[BINAURAL_BEAT_12kHz]) == 0){
			this.element_binaural_beat[BINAURAL_BEAT_3kHz] = generateRandomIntegerBB();
			if((element_binaural_beat[BINAURAL_BEAT_3kHz]) == 0)
				this.element_binaural_beat[BINAURAL_BEAT_6kHz] = generateRandomIntegerBB();
			else
				this.element_binaural_beat[BINAURAL_BEAT_6kHz] = 0;
			if((element_binaural_beat[BINAURAL_BEAT_3kHz]+element_binaural_beat[BINAURAL_BEAT_6kHz]) == 0)
				this.element_binaural_beat[BINAURAL_BEAT_9kHz] = generateRandomIntegerBB();
			else
				this.element_binaural_beat[BINAURAL_BEAT_9kHz] = 0;
			if((element_binaural_beat[BINAURAL_BEAT_3kHz]+element_binaural_beat[BINAURAL_BEAT_6kHz]+element_binaural_beat[BINAURAL_BEAT_9kHz]) == 0)
			this.element_binaural_beat[BINAURAL_BEAT_12kHz] = generateRandomIntegerBB();
			else
				this.element_binaural_beat[BINAURAL_BEAT_12kHz] = 0;
		}
		if(element_binaural_beat[BINAURAL_BEAT_3kHz] != 0)
			this.index_binaural_beat = BINAURAL_BEAT_3kHz;
		if(element_binaural_beat[BINAURAL_BEAT_6kHz] != 0)
			this.index_binaural_beat = BINAURAL_BEAT_6kHz;
		if(element_binaural_beat[BINAURAL_BEAT_9kHz] != 0)
			this.index_binaural_beat = BINAURAL_BEAT_9kHz;
		if(element_binaural_beat[BINAURAL_BEAT_12kHz] != 0)
			this.index_binaural_beat = BINAURAL_BEAT_12kHz;
		this.element[ELEMENT_BB] =	element_binaural_beat[BINAURAL_BEAT_3kHz]
								 +	element_binaural_beat[BINAURAL_BEAT_6kHz]
								 +	element_binaural_beat[BINAURAL_BEAT_9kHz]
								 +	element_binaural_beat[BINAURAL_BEAT_12kHz];
		
		this.index[INDEX_AWAKEN] = generateRandomInteger(65, 95);
		this.index[INDEX_CYCLE] = generateRandomInteger(20, 80);
		this.index[INDEX_STABLE] = generateRandomInteger(0, 100);
		this.index[INDEX_TOTAL] = (int) (this.index[INDEX_AWAKEN]*0.7 + this.index[INDEX_CYCLE]*0.2 + this.index[INDEX_STABLE]*0.1);
		this.index[INDEX_STANDARD] = (int) (this.index[INDEX_AWAKEN]*0.7 + this.index[INDEX_STABLE]*0.1);
		this.direction = 0;
		this.difference = 0;
		this.isUpdated = false;
	}

	public Record(String date){
		this();
		this.date = date;
	}
	
	public Record(String date,
			int element_tst,
			int element_gtbt,
			int element_sun,
			int element_walk,
			int element_coffee,
			int element_smoke,
			int element_drink,
			int element_bb,
			int index_awaken,
			int index_cycle,
			int index_stable,
			int index_binaural_beat
			){
		
		this.date = date;

		this.element[ELEMENT_TST] = element_tst;
		this.element[ELEMENT_GTBT] = element_gtbt;
		this.element[ELEMENT_SUN] = element_sun;
		this.element[ELEMENT_WALK] = element_walk;
		this.element[ELEMENT_COFFEE] = element_coffee;
		this.element[ELEMENT_SMOKE] = element_smoke;
		this.element[ELEMENT_DRINK] = element_drink;
		this.element[ELEMENT_BB] = element_bb;

		this.index[INDEX_AWAKEN] = index_awaken;
		this.index[INDEX_CYCLE] = index_cycle;
		this.index[INDEX_STABLE] = index_stable;
		this.index[INDEX_TOTAL] = (int) (this.index[INDEX_AWAKEN]*0.7 + this.index[INDEX_CYCLE]*0.2 + this.index[INDEX_STABLE]*0.1);
		this.index[INDEX_STANDARD] = (int) (this.index[INDEX_AWAKEN]*0.7 + this.index[INDEX_STABLE]*0.1);

		this.element_binaural_beat[index_binaural_beat] = element_bb;
		this.index_binaural_beat = index_binaural_beat;
		
		this.direction = 0;
		this.difference = 0;
		this.isUpdated = false;
	}
	
	private int generateRandomInteger(int min, int max){
		Random rand = new Random();
		return rand.nextInt(max - min + 1) + min;
	}
	
	private int generateRandomIntegerBB(){
		int min = 3;
		int max = 15;
		Random rand = new Random();
		int zero = (rand.nextDouble() > 0.66)? 1 : 0;
		return zero * (rand.nextInt(max - min + 1) + min);
	}
	
	private String getCurrentDate(){
		/* Today */
		/*
		DecimalFormat df = new DecimalFormat("00");
		Calendar currentCal = Calendar.getInstance();
		
		currentCal.add(Calendar.DATE, 0);

		String year = Integer.toString(currentCal.get(Calendar.YEAR));
		String month = df.format(currentCal.get(Calendar.MONTH)+1);
		String day= df.format(currentCal.get(Calendar.DAY_OF_MONTH));
		
		return year + "-" + month + "-" + day;
		*/
		
		/* Random Date */
		DecimalFormat df = new DecimalFormat("00");
		String year = "2018";
		String month = df.format(generateRandomInteger(1,12));
		String day= df.format(generateRandomInteger(1,30));
		return year + "-" + month + "-" + day;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getElement(int element_enum) {
		return element[element_enum];
	}

	public void setElement(int element_enum, int value) {
		this.element[element_enum] = value;
	}

	public int getIndex(int index_enum) {
		return index[index_enum];
	}

	public void setIndex(int index_enum, int value) {
		this.index[index_enum] = value;
	}

	public int getElementBinauralBeat(int binaural_beat_enum) {
		return element_binaural_beat[binaural_beat_enum];
	}

	public void setElementBinauralBeat(int binaural_beat_enum, int value) {
		this.element_binaural_beat[binaural_beat_enum] = value;
	}

	public int getIndexBinauralBeat() {
		return index_binaural_beat;
	}

	public void setIndexBinauralBeat(int index_binaural_beat) {
		this.index_binaural_beat = index_binaural_beat;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}
	
	public int getDifference() {
		return difference;
	}

	public void setDifference(int difference) {
		this.difference = Math.abs(difference);
	}

	public boolean isUpdated() {
		return isUpdated;
	}

	public void setUpdated(boolean isUpdated) {
		this.isUpdated = isUpdated;
	}

	public static Record makeClone(Record record){
		
		Record clone = new Record();
		
		clone.setDate(record.getDate());
		
		clone.setElement(ELEMENT_TST, record.getElement(ELEMENT_TST));
		clone.setElement(ELEMENT_GTBT, record.getElement(ELEMENT_GTBT));
		clone.setElement(ELEMENT_SUN, record.getElement(ELEMENT_SUN));
		clone.setElement(ELEMENT_WALK, record.getElement(ELEMENT_WALK));
		clone.setElement(ELEMENT_COFFEE, record.getElement(ELEMENT_COFFEE));
		clone.setElement(ELEMENT_SMOKE, record.getElement(ELEMENT_SMOKE));
		clone.setElement(ELEMENT_DRINK, record.getElement(ELEMENT_DRINK));
		clone.setElement(ELEMENT_BB, record.getElement(ELEMENT_BB));

		clone.setIndex(INDEX_AWAKEN, record.getIndex(INDEX_AWAKEN));
		clone.setIndex(INDEX_CYCLE, record.getIndex(INDEX_CYCLE));
		clone.setIndex(INDEX_STABLE, record.getIndex(INDEX_STABLE));
		clone.setIndex(INDEX_TOTAL, record.getIndex(INDEX_TOTAL));
		clone.setIndex(INDEX_STANDARD, record.getIndex(INDEX_STANDARD));

		clone.setElementBinauralBeat(BINAURAL_BEAT_3kHz, record.getElementBinauralBeat(BINAURAL_BEAT_3kHz));
		clone.setElementBinauralBeat(BINAURAL_BEAT_6kHz, record.getElementBinauralBeat(BINAURAL_BEAT_6kHz));
		clone.setElementBinauralBeat(BINAURAL_BEAT_9kHz, record.getElementBinauralBeat(BINAURAL_BEAT_9kHz));
		clone.setElementBinauralBeat(BINAURAL_BEAT_12kHz, record.getElementBinauralBeat(BINAURAL_BEAT_12kHz));
		
		clone.setIndexBinauralBeat(record.getIndexBinauralBeat());
		clone.setDirection(record.getDirection());
		clone.setDifference(record.getDifference());
		clone.setUpdated(record.isUpdated());
		
		return clone;
	}
	
	public boolean equal(Record another){
		if(this.date.equalsIgnoreCase(another.getDate())) return false;
		if(this.element[ELEMENT_TST] != another.getElement(ELEMENT_TST)) return false;
		if(this.element[ELEMENT_GTBT] != another.getElement(ELEMENT_GTBT)) return false;
		if(this.element[ELEMENT_SUN] != another.getElement(ELEMENT_SUN)) return false;
		if(this.element[ELEMENT_WALK] != another.getElement(ELEMENT_WALK)) return false;
		if(this.element[ELEMENT_COFFEE] != another.getElement(ELEMENT_COFFEE)) return false;
		if(this.element[ELEMENT_SMOKE] != another.getElement(ELEMENT_SMOKE)) return false;
		if(this.element[ELEMENT_DRINK] != another.getElement(ELEMENT_DRINK)) return false;
		if(this.element[ELEMENT_BB] != another.getElement(ELEMENT_BB)) return false;
		if(this.index[INDEX_AWAKEN] != another.getIndex(INDEX_AWAKEN)) return false;
		if(this.index[INDEX_CYCLE] != another.getIndex(INDEX_CYCLE)) return false;
		if(this.index[INDEX_STABLE] != another.getIndex(INDEX_STABLE)) return false;
		return true;
	}
	
	public String toString(){
		String text = "";
		text += (this.date + "\t");
		
		text += (this.element[ELEMENT_TST] + "\t");
		text += (this.element[ELEMENT_GTBT] + "\t");
		text += (this.element[ELEMENT_SUN] + "\t");
		text += (this.element[ELEMENT_WALK] + "\t");
		text += (this.element[ELEMENT_COFFEE] + "\t");
		text += (this.element[ELEMENT_SMOKE] + "\t");
		text += (this.element[ELEMENT_DRINK] + "\t");
		text += (this.element[ELEMENT_BB] + "\t");

		text += (this.element_binaural_beat[BINAURAL_BEAT_3kHz] + "\t");
		text += (this.element_binaural_beat[BINAURAL_BEAT_6kHz] + "\t");
		text += (this.element_binaural_beat[BINAURAL_BEAT_9kHz] + "\t");
		text += (this.element_binaural_beat[BINAURAL_BEAT_12kHz] + "\t");

		text += (this.index[INDEX_AWAKEN] + "\t");
		text += (this.index[INDEX_CYCLE] + "\t");
		text += (this.index[INDEX_STABLE] + "\t");
		text += (this.index[INDEX_TOTAL] + "\t");
		text += (this.index[INDEX_STANDARD]);
		
		return text;
	}
}
