package com.dlogixs.alarm;

import android.net.Uri;

import java.net.URI;

/**
 * Created by fptsoftware on 03/04/2018.
 */

public class AlarmRingtone {
    private Uri mUri;
    private String mTitle;

    public AlarmRingtone(Uri uri, String title) {
        mUri = uri;
        mTitle = title;
    }

    public Uri getURI() {
        return mUri;
    }

    public String getTitle() {
        return mTitle;
    }
}
