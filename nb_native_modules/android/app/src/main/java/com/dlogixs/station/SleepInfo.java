package com.dlogixs.station;

/**
 * Created by fptsoftware on 22/02/2018.
 */

public class SleepInfo {
    private String sleepDate;
    private float sleepSpectrum;
    private int sleepSignalPoor;

    public SleepInfo(String sleepDate, float sleepSpectrum, int sleepSignalPoor) {
        this.sleepDate = sleepDate;
        this.sleepSpectrum = sleepSpectrum;
        this.sleepSignalPoor = sleepSignalPoor;
    }

    public String getSleepDate() {
        return sleepDate;
    }

    public float getSleepSpectrum() {
        return sleepSpectrum;
    }

    public int getSleepSignalPoor() {
        return sleepSignalPoor;
    }
}
