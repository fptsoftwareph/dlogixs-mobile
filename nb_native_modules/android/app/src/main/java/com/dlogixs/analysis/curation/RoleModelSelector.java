package com.dlogixs.analysis.curation;

import java.util.ArrayList;
import java.util.List;

public class RoleModelSelector {
	
	private List<ElementData> database = new ArrayList<ElementData>();
	private ElementData data;
	public RoleModelSelector(List<ElementData> database, ElementData data) {
		super();
		this.database = database;
		this.data = data;
	}
	
	public ElementData getRoleModel(int element){
		switch(element){
		case ElementData.TST: return getRoleModelByLifeElementIndex(ElementData.TST);
		case ElementData.GTBT: return getRoleModelByLifeElementIndex(ElementData.GTBT);
		case ElementData.SUN: return getRoleModelByLifeElementIndex(ElementData.SUN);
		case ElementData.WALK: return getRoleModelByLifeElementIndex(ElementData.WALK);
		case ElementData.COFFEE: return getRoleModelByTasteElementIndex(ElementData.COFFEE);
		case ElementData.SMOKE: return getRoleModelByTasteElementIndex(ElementData.SMOKE);
		case ElementData.DRINK: return getRoleModelByTasteElementIndex(ElementData.DRINK);
		case ElementData.BB: return getRoleModelByLifeElementIndex(ElementData.BB);
		}
		return null;
	}

	private ElementData getRoleModelByLifeElementIndex(int element_index){
		/* Sorting */
		List<ElementData> tmpDatabase = new ArrayList<ElementData>();
		for(int i = 0; i < database.size(); i++) tmpDatabase.add(database.get(i));
		List<ElementData> sortedDatabase = new ArrayList<ElementData>();
		for(int i = 0; i < database.size(); i++){
			if(i > 4) break;
			int maxElement = 0;
			int maxIndex = 0;
			for(int j = 0; j < tmpDatabase.size(); j++){
				// Element Dependent
				if(tmpDatabase.get(j).getElement(element_index) > maxElement){
					maxElement = tmpDatabase.get(j).getElement(element_index);
					maxIndex = j;
				}
			}
			sortedDatabase.add(tmpDatabase.get(maxIndex));
			tmpDatabase.remove(maxIndex);
		}
		
		/* Retrieve */
		ElementData model = null;
		
		int maxRM = data.getSqiRM();
		for(int i = 0; i < sortedDatabase.size(); i++){
			// Element Dependent
			if(data.getElement(element_index) == sortedDatabase.get(i).getElement(element_index)){
				if(sortedDatabase.get(i).getSqiRM() > maxRM){
					model = sortedDatabase.get(i);
					maxRM = sortedDatabase.get(i).getSqiRM();
				}
			}
		}
		if(model != null) return model;
		
		int minDist = Integer.MAX_VALUE;
		for(int i = 0; i < sortedDatabase.size(); i++){
			// Element Dependent
			if(data.getElement(element_index) == sortedDatabase.get(i).getElement(element_index)) continue;
			if(data.getSqiRM() > sortedDatabase.get(i).getSqiRM()) continue;
			int tmpDist = Math.abs(data.getElement(element_index) - sortedDatabase.get(i).getElement(element_index));
			if(tmpDist < minDist){
				minDist = tmpDist;
				model = sortedDatabase.get(i);
			}
		}
		if(model == null){
			if(sortedDatabase.get(0).getElement(element_index) > data.getElement(element_index)){
				data.setIndexOfPosition(-1);
			}
			else if(sortedDatabase.get(0).getElement(element_index) < data.getElement(element_index)){
				data.setIndexOfPosition(1);
			}
			else{
				data.setIndexOfPosition(0);
			}
			return data;
		}
		return model;
	}
	
	private ElementData getRoleModelByTasteElementIndex(int element_index){
		/* Sorting */
		List<ElementData> tmpDatabase = new ArrayList<ElementData>();
		for(int i = 0; i < database.size(); i++) tmpDatabase.add(database.get(i));
		List<ElementData> sortedDatabase = new ArrayList<ElementData>();
		for(int i = 0; i < database.size(); i++){
			if(i > 4) break;
			int maxElement = 0;
			int maxIndex = 0;
			for(int j = 0; j < tmpDatabase.size(); j++){
				// Element Dependent
				if(tmpDatabase.get(j).getElement(element_index) > maxElement){
					maxElement = tmpDatabase.get(j).getElement(element_index);
					maxIndex = j;
				}
			}
			sortedDatabase.add(tmpDatabase.get(maxIndex));
			tmpDatabase.remove(maxIndex);
		}
		
		/* Retrieve */
		ElementData model = null;
		
		int maxRM = data.getSqiRM();
		for(int i = 0; i < sortedDatabase.size(); i++){
			// Element Dependent
			if(data.getElement(element_index) == sortedDatabase.get(i).getElement(element_index)){
				if(sortedDatabase.get(i).getSqiRM() > maxRM){
					model = sortedDatabase.get(i);
					maxRM = sortedDatabase.get(i).getSqiRM();
				}
			}
		}
		if(model != null) return model;
		
		int minDist = Integer.MAX_VALUE;
		for(int i = 0; i < sortedDatabase.size(); i++){
			// Element Dependent
			if(data.getElement(element_index) == sortedDatabase.get(i).getElement(element_index)) continue;
			if(data.getSqiRM() > sortedDatabase.get(i).getSqiRM()) continue;
			int tmpDist = Math.abs(data.getElement(element_index) - sortedDatabase.get(i).getElement(element_index));
			if(tmpDist < minDist){
				minDist = tmpDist;
				model = sortedDatabase.get(i);
			}
			if(data.getElement(element_index) < sortedDatabase.get(i).getElement(element_index)) break;
		}
		if(model == null){
			if(sortedDatabase.get(0).getElement(element_index) > data.getElement(element_index)){
				data.setIndexOfPosition(-1);
			}
			else if(sortedDatabase.get(0).getElement(element_index) < data.getElement(element_index)){
				data.setIndexOfPosition(1);
			}
			else{
				data.setIndexOfPosition(0);
			}
			return data;
		}
		return model;
	}
		
}
