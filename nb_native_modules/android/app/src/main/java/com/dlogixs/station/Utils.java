//
package com.dlogixs.station;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.dlogixs.R;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

public class Utils
{  
	private static String TAG = "Utils";

	public static JSONObject JsonParse(String w_Str)
	{
		JSONObject w_JsonObject = null;
		if(w_Str.length() > 0) {
			try {
				w_JsonObject = new JSONObject(w_Str);	
			} catch (JSONException e) {
			}
		}
		return(w_JsonObject);
	}
	
	public static JSONArray JsonGetArray(JSONObject a_JsonObj, String a_Name)
	{
	   	JSONArray w_JsonArr = null;
	   	if(a_JsonObj != null) {
			try {
				w_JsonArr = a_JsonObj.getJSONArray(a_Name);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
	   	}
		return(w_JsonArr);
	}
	
	public static JSONObject JsonGetObject(JSONArray a_JsonObj, int a_Index)
	{
		JSONObject w_JsonObject = null;
		if(a_JsonObj != null) {
			try {
				w_JsonObject = a_JsonObj.getJSONObject(a_Index);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		return(w_JsonObject);
	}
	
	public static JSONObject JsonGetObject(JSONObject a_JsonObj, String a_Name)
	{
		JSONObject w_JsonObject = null;
		if(a_JsonObj != null) {
			try {
				w_JsonObject = a_JsonObj.getJSONObject(a_Name);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		return(w_JsonObject);
	}
	
    public static String JsonGetString(JSONObject a_JsonObj, String a_Name)
    {
    	String w_Value = "";
    	if(a_JsonObj != null) {
			try {  
				w_Value = a_JsonObj.getString(a_Name);
			} catch (JSONException e) {
	
			}
    	}
		return(w_Value);
    }
    
	public static int JsonGetInteger(JSONObject a_JsonObj, String a_Name)
  	{
    	String w_Value = "";
    	
    	if(a_JsonObj != null) {
			try {  
				w_Value = a_JsonObj.getString(a_Name);
			} catch (JSONException e) {
			}
    	}
		return(toInt(w_Value));
    }
        
    public static int toInt(String a_Str)
    {
    	if(a_Str.length() == 0) {
    		return(0);
    	}
    	try {
	   		return(Integer.parseInt(a_Str));
	   	} catch (Exception e) {
	   		return(0);
	   	}	   	
    }
    public static String toString(int a_Int)
    {
    	String w_Str = String.format("%d", a_Int);
    	
    	return w_Str;
    }

	public static String toString(byte a_Int)
	{
		String w_Str = String.format("%d", byteToInt(a_Int));

		return w_Str;
	}

	public static int byteToInt(byte b) {
		return b & 0xFF;
	}

    public static String getDataDir(Context context) 
    {
    	String	w_Path="";
    	try {
    		w_Path = context.getPackageManager()
                .getPackageInfo(context.getPackageName(), 0)
                .applicationInfo.dataDir;
    	} catch (NameNotFoundException e) {
    	    Log.w("yourtag", "Error Package name not found ", e);
    	}
    	return w_Path;
    }
    
    public static int CopyAssertFile(Context context, String a_AssertFname, String a_To)
    {		
    	File fTo = new File(a_To);
    	if(fTo.exists() == false) { 
//    	if(true) {
    		try {				
    			InputStream is = context.getAssets().open(a_AssertFname);
    			int size = is.available();
    			byte[] buffer = new byte[size];
    			is.read(buffer);
    			is.close();				
		
    			FileOutputStream os = new FileOutputStream(fTo); 
    			os.write(buffer); 
    			os.close();				
    		} catch (IOException e) { 
    			Log.v(TAG, "CopyAssertFile error : " + e.toString());
    			return(0);
    		}
		} else {
			Log.v(TAG, "FILE EXIST");
		}
		return 1;
	}	
    
    public static String stringToHex(String s) {
        String result = "";

        for (int i = 0; i < s.length(); i++) {
          result += String.format("%02X ", (int) s.charAt(i));
        }

        return result;
    }

	public static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder();

		for(final byte b: a)
			sb.append(String.format("%02x ", b&0xff));
		return sb.toString();
	}

	public static String byteArrayToIntString(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++)
			sb.append(String.format("%02d", (bytes[i] & 0xFF)));
		return sb.toString();
	}

	public static float littleEndianToFloat(byte[] bytes) {
		int bits = ((bytes[0] & 0xFF) << 24) | ((bytes[1] & 0xFF) << 16) | ((bytes[2] & 0xFF) << 8) | (bytes[3] & 0xFF);
		return Float.intBitsToFloat(bits);
	}

	public static String byteArrayToHex(byte[] a_str, int a_len, boolean a_space) {
		int w_ii = 0;
		StringBuilder sb = new StringBuilder();

		for(w_ii=0;w_ii<a_len;w_ii++) {
			sb.append(String.format("%02x", a_str[w_ii]&0xff));
			if(a_space && ((w_ii % 2) == 1)) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
    
    public static void ShowMessage(Context context, String w_Msg, int a_flag)
    {
    	Toast t = Toast.makeText(context, w_Msg, (a_flag == 1) ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
    	t.show();
    }    
    
	public static void PrintCursor(String a_Tag, Cursor a_Cursor)
    {
    	int		w_ii;
    	String  w_Str ="";
    	int		w_Type;
    	
    	for(w_ii=0;w_ii<a_Cursor.getColumnCount();w_ii++) {
    		w_Str += a_Cursor.getColumnName(w_ii);
    		w_Str += ":(";
    		try {
    			w_Str += a_Cursor.getString(w_ii);
    		} catch(Exception e) {
    			w_Str += "null";
    		}
    		w_Str += "), ";
    	}
    	Log.d(a_Tag, w_Str);
    }

	public static String milliSecondsToTimer(long milliseconds){
		String finalTimerString = "";
		String secondsString = "";
		
		// Convert total duration into time
		   int hours = (int)( milliseconds / (1000*60*60));
		   int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
		   int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
		   // Add hours if there
		   if(hours > 0){
			   finalTimerString = hours + ":";
		   }
		   
		   // Prepending 0 to seconds if it is one digit
		   if(seconds < 10){ 
			   secondsString = "0" + seconds;
		   }else{
			   secondsString = "" + seconds;}
		   
		   finalTimerString = finalTimerString + minutes + ":" + secondsString;
		
		// return timer string
		return finalTimerString;
	}
	
	public static int getProgressPercentage(long currentDuration, long totalDuration){
		Double percentage = (double) 0;
		
		long currentSeconds = (int) (currentDuration / 1000);
		long totalSeconds = (int) (totalDuration / 1000);
		
		// calculating percentage
		percentage =(((double)currentSeconds)/totalSeconds)*100;
		
		// return percentage
		return percentage.intValue();
	}

	public static int progressToTimer(int progress, int totalDuration) {
		int currentDuration = 0;
		totalDuration = (int) (totalDuration / 1000);
		currentDuration = (int) ((((double)progress) / 100) * totalDuration);
		
		// return current duration in milliseconds
		return currentDuration * 1000;
	}	
	private static int sArmArchitecture = -1;

	public static int getArmArchitecture() {
		if (sArmArchitecture != -1)
			return sArmArchitecture;
		try {
			InputStream is = new FileInputStream("/proc/cpuinfo");
			InputStreamReader ir = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(ir);
			try {
				String name = "CPU architecture";
				while (true) {
					String line = br.readLine();
					String[] pair = line.split(":");
					if (pair.length != 2)
						continue;
					String key = pair[0].trim();
					String val = pair[1].trim();
					if (key.compareToIgnoreCase(name) == 0) {
						String n = val.substring(0, 1);
						sArmArchitecture = Integer.parseInt(n);
						break;
					}
				}
			} finally {
				br.close();
				ir.close();
				is.close();
				if (sArmArchitecture == -1)
					sArmArchitecture = 6;
			}
		} catch (Exception e) {
			sArmArchitecture = 6;
		}
		return sArmArchitecture;
	}

	public static int getSDKVersionCode() {
		// TODO: fix this
		return Build.VERSION.SDK_INT;
	}

	public static String getExternalStoragePath() {
		boolean exists = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
		if (exists)
			return Environment.getExternalStorageDirectory().getAbsolutePath();
		else
			return "/";
	}

	@SuppressWarnings("rawtypes")
	public static int getDrawableId(String name) {
		int result = -1;
		try {
			Class clz = Class.forName(R.drawable.class.getName());
			Field field = clz.getField(name);
			result = field.getInt(new R.drawable());
		} catch (Exception e) {
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	public static Object realloc(Object oldArray, int newSize) {
		int oldSize = java.lang.reflect.Array.getLength(oldArray);
		Class elementType = oldArray.getClass().getComponentType();
		Object newArray = java.lang.reflect.Array.newInstance(elementType,
				newSize);
		int preserveLength = Math.min(oldSize, newSize);
		if (preserveLength > 0)
			System.arraycopy(oldArray, 0, newArray, 0, preserveLength);
		return newArray;
	}

	public static String getTimeString(int msec) {
		if (msec < 0) {
			return String.format("--:--:--");
		}
		int total = msec / 1000;
		int hour = total / 3600;
		total = total % 3600;
		int minute = total / 60;
		int second = total % 60;
		return String.format("%02d:%02d:%02d", hour, minute, second);
	}

	protected static String sTempPath = "/data/local/tmp";

	public static String getTempPath() {
		return sTempPath;
	}

	public static boolean simpleHttpGet(String url, String file) {
		try {
			HttpGet request = new HttpGet(url);
			request.setHeader("Accept-Encoding", "gzip");
			DefaultHttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				return false;
			}
			HttpEntity entity = response.getEntity();
			Header contentEncoding = entity.getContentEncoding();
			InputStream is = entity.getContent();
			if (contentEncoding != null
					&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
				is = new GZIPInputStream(is);
			}
			OutputStream os = new FileOutputStream(file);
			byte[] buffer = new byte[4096];
			while (true) {
				int count = is.read(buffer);
				if (count <= 0)
					break;
				os.write(buffer, 0, count);
			}
			os.close();
			is.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public static int getStringHash(String s) {
		byte[] target = s.getBytes();
		int hash = 1315423911;
		for (int i = 0; i < target.length; i++) {
			byte val = target[i];
			hash ^= ((hash << 5) + val + (hash >> 2));
		}
		hash &= 0x7fffffff;
		return hash;
	}
	
	private final static String	mPrefName = "DLIPTV";
	public static  String  getConfig(Context aContext, String aItem, String aDefValue)
	{         
		SharedPreferences pref = aContext.getSharedPreferences(mPrefName, aContext.MODE_PRIVATE); 
		return(pref.getString(aItem, aDefValue)); 
	}
	
	public static void setConfig(Context aContext, String aItem, String aValue)
	{         
		SharedPreferences pref = aContext.getSharedPreferences(mPrefName, aContext.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(aItem, aValue);
		editor.commit();
	}
	
	public static  int  getConfig(Context aContext, String aItem, int aDefValue)
	{         
		SharedPreferences pref = aContext.getSharedPreferences(mPrefName, aContext.MODE_PRIVATE); 
		return(toInt(pref.getString(aItem, toString(aDefValue)))); 
	}
	
	public static void setConfig(Context aContext, String aItem, int aValue)
	{         
		SharedPreferences pref = aContext.getSharedPreferences(mPrefName, aContext.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(aItem, toString(aValue));
		editor.commit();
	}
	
	
	public static void delConfig(Context aContext, String aItem)
	{         
		SharedPreferences pref = aContext.getSharedPreferences(mPrefName, aContext.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.remove(aItem);
		editor.commit();     
	} 
	
	public static void HideTitleAndNavigation(Activity aActivity)
	{
		aActivity.requestWindowFeature(Window.FEATURE_NO_TITLE);  
		aActivity.getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION 
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LOW_PROFILE);	
	}
	
	
	public static void HideTitleAndNavigation(Dialog aDialog)
	{
		aDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);  
		aDialog.getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION 
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LOW_PROFILE);	
	}	
	
	public static void hideSoftKeyboard (Activity aActivity) 
	{
		aActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	public static void showSoftKeyboard (Activity aActivity) 
	{
		aActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	}	
	
	public static void hideSoftKeyboard (Dialog aDialog) 
	{
		aDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	public static void showSoftKeyboard (Dialog aDialog) 
	{
		aDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	}	
	
	public static void inputText (TextView a_TextView, String a_Str) 
	{
		String w_Str = (String) a_TextView.getText();
		
		w_Str += a_Str;
		a_TextView.setText(w_Str);
	}
	
	private static Pattern g_pattern;
    private static Matcher g_matcher;
 
    private static final String IPADDRESS_PATTERN = 
		"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
 
    public static void IPAddressValidator()
    {  
    	g_pattern = Pattern.compile(IPADDRESS_PATTERN);
    }

    public static boolean checkIP(final String ip){	
    	IPAddressValidator(); 
    	g_matcher = g_pattern.matcher(ip);
    	return g_matcher.matches();	    	    
    }
    
	public static void setMute(Activity a_Activity, boolean a_mute) 
	{
		AudioManager audioManager = null;  
		 
		audioManager = (AudioManager) a_Activity.getSystemService(Context.AUDIO_SERVICE);
		if(audioManager != null) {
			audioManager.setStreamMute(AudioManager.STREAM_MUSIC, a_mute);
		}
	} 
	
	public static int getVolume(Activity a_Activity) 
	{
		int w_Vol = 0;
		AudioManager audioManager = null;  
		
		audioManager = (AudioManager) a_Activity.getSystemService(Context.AUDIO_SERVICE);
		if(audioManager != null) {
			w_Vol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		}
		int w_MaxLevel = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		
		Log.v(TAG, "getVolume:" + w_Vol);
		if(w_MaxLevel > 0) {
			w_Vol = (w_Vol * 100)/w_MaxLevel;
		} else {
			w_Vol = (w_Vol * 100)/15;
		}
		return w_Vol;
	}
	
	
	public static void setVolume(Activity a_Activity, int a_vol) 
	{
		AudioManager audioManager = null;  
		
		audioManager = (AudioManager) a_Activity.getSystemService(Context.AUDIO_SERVICE);
		if(audioManager != null) {
			int w_Level;
			int w_MaxLevel = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			
			w_Level = (a_vol * w_MaxLevel) / 100;
			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, w_Level, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
		}
 		
//		getVolume(a_Activity);
	}
	
	public static void resetVolume(Activity a_Activity) 
	{
		AudioManager audioManager = null;   
		 
		audioManager = (AudioManager) a_Activity.getSystemService(Context.AUDIO_SERVICE);	
		if(audioManager != null) {
			int w_vol = (int)audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

			if(w_vol != 0) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, w_vol, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
			}
			audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
		}
	} 
	
	
	/*public static void powerOff(Activity a_Activity)
	{
		PowerManager pm = (PowerManager) a_Activity.getSystemService(Context.POWER_SERVICE);

		if(pm != null) {
			pm.goToSleep(SystemClock.uptimeMillis());
		}
	}*/
	

	// 이하 파일 관리 함수들...
	public static File makeDirectory(String dir_path)
	{
        File dir = new File(dir_path);
        if (!dir.exists())
        {
            dir.mkdirs();
            Log.i( TAG , "!dir.exists" );
        }else{
            Log.i( TAG , "dir.exists" );
        }
 
        return dir;
    }
    
	public static  File makeFile(File dir , String file_path)
	{
        File file = null;
        boolean isSuccess = false;
        if(dir.isDirectory()){
            file = new File(file_path);
            if(file!=null&&!file.exists()){
                Log.i( TAG , "!file.exists" );
                try {
                    isSuccess = file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally{
                    Log.i(TAG, "파일생성 여부 = " + isSuccess);
                }
            }else{
                Log.i( TAG , "file.exists" );
            }
        }
        return file;
    }
 
  	public static  String getAbsolutePath(File file)
  	{
        return ""+file.getAbsolutePath();
    }
 
	public static  boolean deleteFile(File file)
	{
        boolean result;
        if(file!=null&&file.exists()){
            file.delete();
            result = true;
        }else{
            result = false;
        }
        return result;
    }
 
	public static  boolean isFile(File file)
	{
        boolean result;
        if(file!=null&&file.exists()&&file.isFile()){
            result=true;
        }else{
            result=false;
        }
        return result;
    }
 
	public static  boolean isDirectory(File dir)
	{
        boolean result;
        if(dir!=null&&dir.isDirectory()){
            result=true;
        }else{
            result=false;
        }
        return result;
    }
 
	public static boolean isFileExist(File file)
	{
        boolean result;
        if(file!=null&&file.exists()){
            result=true;
        }else{
            result=false;
        }
        return result;
    }
  
    public static  boolean reNameFile(File file , File new_name)
    {
        boolean result;
        if(file!=null&&file.exists()&&file.renameTo(new_name)){
            result=true;
        }else{
            result=false;
        }
        return result;
    }
     
 
    public static  String[] getList(File dir)
    {
        if(dir!=null&&dir.exists())
            return dir.list();
        return null;
    }
 
    public static  boolean writeFile(File file , byte[] file_content)
    {
        boolean result;
        FileOutputStream fos;
        if(file != null && file.exists() && file_content!=null){
            try {
                fos = new FileOutputStream(file);
                try {
                    fos.write(file_content);
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            result = true;
        }else{
            result = false;
        }
        return result;
    }

    public static  void readFile(File file)
    {
        int readcount=0;
        if(file!=null&&file.exists()){
            try {
                FileInputStream fis = new FileInputStream(file);
                readcount = (int)file.length();
                byte[] buffer = new byte[readcount];
                fis.read(buffer);
                for(int i=0 ; i<file.length();i++){
                    Log.d(TAG, ""+buffer[i]);
                }
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static  boolean copyFile(File file , String save_file)
    {
        boolean result;
        if(file!=null&&file.exists()){
            try {
                FileInputStream fis = new FileInputStream(file);
                FileOutputStream newfos = new FileOutputStream(save_file);
                int readcount=0;
                byte[] buffer = new byte[1024];
                while((readcount = fis.read(buffer,0,1024))!= -1){
                    newfos.write(buffer,0,readcount);
                }
                newfos.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            result = true;
        }else{
            result = false;
        }
        return result;
    }
    
    public static boolean isMovie(String a_Fname)
	{
    	if(a_Fname.length() < 4) {
    		return false;
    	}
    	String wExt = a_Fname.substring(a_Fname.length()-4, a_Fname.length());
		if(	   wExt.equalsIgnoreCase(".avi")
			|| wExt.equalsIgnoreCase(".mp4")
		  ) {
			return true;
		}
		return false;	
	}
	
    public static boolean isImage(String a_Fname)
	{
    	if(a_Fname.length() < 4) {
    		return false;
    	}
    	String wExt = a_Fname.substring(a_Fname.length()-4, a_Fname.length());
		if(	   wExt.equalsIgnoreCase(".jpg")
			|| wExt.equalsIgnoreCase(".png")
		  ) {
			return true;
		}
		return false;	
	}	
	
}