package com.dlogixs;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dlogixs.analysis.sleep.EEGAnalysis;
import com.dlogixs.analysis.sleep.EEGData;
import com.dlogixs.analysis.sleep.SleepAnalysis;
import com.dlogixs.station.BluetoothSppService;
import com.dlogixs.station.EEGStreamParser;
import com.dlogixs.station.SleepInfo;
import com.dlogixs.station.SleepTimerManager;
import com.dlogixs.station.Utils;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.RCTNativeAppEventEmitter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class StationModule extends ReactContextBaseJavaModule implements EEGStreamParser.StreamParserListener, SleepTimerManager.SleepTimerListener {

    static final private String TAG = "StationModule";

    private static Context mContext = null;

    // Message types sent from the BluetoothSppService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_CONLOST = 5;
    public static final int MESSAGE_CONFAIL = 6;

    // Key names received from the BluetoothSppService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String CONMSG = "conmsg";

    private static final byte STATION_CMD_BATTERY     = (byte)(0xA4);
    private static final byte STATION_CMD_EEG         = (byte)(0xAD);
    private static final byte STATION_CMD_SLEEP_INFO  = (byte)(0xAE);
    private static final byte STATION_CMD_WIFI        = (byte)(0xB0);
    private static final byte STATION_CMD_VOLUME      = (byte)(0xA2);
    private static final byte STATION_CMD_LIGHT       = (byte)(0xA0);
    private static final byte STATION_CMD_DSP         = (byte)(0xA6);
    private static final byte STATION_CMD_AUDIOINPUT  = (byte)(0xAA);
    private static final byte STATION_CMD_PLAYERCTRL  = (byte)(0xAC);
    private static final byte STATION_CMD_HEADSET     = (byte)(0x16);
    private static final byte STATION_CMD_DEV_STATE   = (byte)(0xDC);
    private static final int WIFI_PACKET_LEN = 60;
    private Callback recvFromRFCommCallback = null;
    private EEGStreamParser mEEGParser = null;
    private SleepTimerManager mSleepTimerMgr = null;

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothSppService mSppService = null;

    private String mConnectedDeviceName = null, connectFromListener = "", disconnectFromListener = "";
    private boolean isFromConnectStation = false;
    private boolean isTriggeredFromApp = false;

    public StationModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public String getName() {
        return "StationModule";
    }

//////////////////////////////////////////////////////////////////////
// DATA PROCESS
//////////////////////////////////////////////////////////////////////

    public static final byte CMD_STX = (byte) (0x02);
    public static final byte CMD_ETX = (byte) (0x03);

    public byte CheckSum(byte[] a_Val, int a_Offset, int a_Len) {
        byte w_Sum = 0x00;

        int w_ii = 0;

        for (w_ii = 0; w_ii < a_Len; w_ii++) {
            w_Sum += (byte) a_Val[a_Offset + w_ii];
        }

        return ((byte) (0x100 - (int) (w_Sum & 0xff)));
    }

    @ReactMethod
    private void setLight(int a_Light, Callback callback) {
        byte[] w_data = new byte[9];
        int w_ii = 0;

        w_data[w_ii++] = CMD_STX;
        w_data[w_ii++] = STATION_CMD_LIGHT;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x01;    // length1
        w_data[w_ii++] = (byte) 0x00;    // length0
        w_data[w_ii++] = (byte) a_Light;  // light
        w_data[w_ii] = CheckSum(w_data, 1, w_ii - 1);
        w_ii++;
        w_data[w_ii++] = CMD_ETX;

        Log.v(TAG, "setLight : " + a_Light);
        Log.v(TAG, Utils.byteArrayToHex(w_data)); //

        if (mSppService != null) {
            mSppService.write(w_data, w_ii);
            callback.invoke(true);
        }
    }

    @ReactMethod
    private void setVolume(int a_Volume, Callback callback) {
        byte[] w_data = new byte[9];
        int w_ii = 0;

        w_data[w_ii++] = CMD_STX;
        w_data[w_ii++] = STATION_CMD_VOLUME;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x01;    // length1
        w_data[w_ii++] = (byte) 0x00;    // length0
        w_data[w_ii++] = (byte) a_Volume;  // a_Volume
        w_data[w_ii] = CheckSum(w_data, 1, w_ii - 1);
        w_ii++;
        w_data[w_ii++] = CMD_ETX;

        Log.v(TAG, "setVolume : " + a_Volume);
        Log.v(TAG, Utils.byteArrayToHex(w_data)); //

        if (mSppService != null) {
            mSppService.write(w_data, w_ii);
            callback.invoke(true);
        }
    }

    @ReactMethod
    private void queryStationStatus() {
        byte[] wData = new byte[7];
        int ndxCtr = 0;

        wData[ndxCtr++] = CMD_STX;
        wData[ndxCtr++] = STATION_CMD_DEV_STATE;
        wData[ndxCtr++] = (byte) 0x00;
        wData[ndxCtr++] = (byte) 0x00;
        wData[ndxCtr++] = (byte) 0x00;
        wData[ndxCtr] = CheckSum(wData, 1, ndxCtr - 1);
        ndxCtr++;
        wData[ndxCtr++] = CMD_ETX;

        Log.d(TAG, "sim123 equipmentRqst: " + Utils.byteArrayToHex(wData, ndxCtr, true));
        if (mSppService != null) {
            mSppService.write(wData, ndxCtr);
        }
    }

    @ReactMethod
    private void setBB(int a_BB, boolean hasTriggered, Callback callback) {
        isTriggeredFromApp = hasTriggered;
        byte[] w_data = new byte[10];
        int w_ii = 0;

        w_data[w_ii++] = CMD_STX;
        w_data[w_ii++] = STATION_CMD_DSP;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x02;    // length1
        w_data[w_ii++] = (byte) 0x00;    // length0
        w_data[w_ii++] = (byte) 0x00;    // bb type ==> always 0
        w_data[w_ii++] = (byte) a_BB;    // bb
        w_data[w_ii] = CheckSum(w_data, 1, w_ii - 1);
        w_ii++;
        w_data[w_ii++] = CMD_ETX;

        Log.v(TAG, "setBB value: " + a_BB);

        if (mSppService != null) {
            Log.v(TAG, "setBB write: " + Utils.byteArrayToHex(w_data));
            mSppService.write(w_data, w_ii);
            callback.invoke(true);
        }

    }

    @ReactMethod
    private void setAudioInput(int a_Input, Callback callback) {
        byte[] w_data = new byte[9];
        int w_ii = 0;

        w_data[w_ii++] = CMD_STX;
        w_data[w_ii++] = STATION_CMD_AUDIOINPUT;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x00;
        w_data[w_ii++] = (byte) 0x01;    // length1
        w_data[w_ii++] = (byte) 0x00;    // length0
        w_data[w_ii++] = (byte) a_Input;    // a_Input
        w_data[w_ii] = CheckSum(w_data, 1, w_ii - 1);
        w_ii++;
        w_data[w_ii++] = CMD_ETX;

        Log.v(TAG, "setAudioInput : " + a_Input);
        Log.v(TAG, Utils.byteArrayToHex(w_data)); //

        if (mSppService != null) {
            mSppService.write(w_data, w_ii);
            callback.invoke(true);
        }
    }

    @ReactMethod
    private void setWifi(String ssid, String password, Callback callback) {
        recvFromRFCommCallback = callback;
        byte[] wifiPacket = new byte[WIFI_PACKET_LEN];
        int wifiNdx, pwCtr;
        wifiNdx = pwCtr = 0;

        wifiPacket[wifiNdx++] = CMD_STX;
        wifiPacket[wifiNdx++] = STATION_CMD_WIFI;
        wifiPacket[wifiNdx++] = (byte) 0x00;
        wifiPacket[wifiNdx++] = (byte) 0x00;
        wifiPacket[wifiNdx++] = (byte) 0x52;
        wifiPacket[wifiNdx++] = (byte) 0x00;
        for (int i = 0; i < 52;) {
            if (i < 32) {
                for (int j = i; j < ssid.length(); j++, i++)
                    wifiPacket[wifiNdx++] = (byte) ssid.charAt(j);
                wifiPacket[wifiNdx++] = 0x00;
                i++;
            } else {
                if (pwCtr < password.length()) {
                    for (int k = 0; k < password.length(); k++, i++) {
                        wifiPacket[wifiNdx++] = (byte) password.charAt(k);
                        pwCtr++;
                    }
                }
                wifiPacket[wifiNdx++] = 0x00;
                i++;
            }
        }
        wifiPacket[wifiNdx] = CheckSum(wifiPacket, 1, wifiNdx-1);
        wifiNdx++;
        wifiPacket[wifiNdx++] = CMD_ETX;

        Log.v(TAG, "setWifi: SSID:" + ssid + " PW: " + password);
        Log.v(TAG, Utils.byteArrayToHex(wifiPacket)); //

        if (mSppService != null) {
            mSppService.write(wifiPacket, wifiNdx);
        }
    }

    @ReactMethod
    private void forgetWifi(Callback callback) {
        recvFromRFCommCallback = callback;
        byte[] wifiPacket = new byte[WIFI_PACKET_LEN];
        int wifiNdx = 0;
        wifiPacket[wifiNdx++] = CMD_STX;
        wifiPacket[wifiNdx++] = STATION_CMD_WIFI;
        wifiPacket[wifiNdx++] = (byte) 0x00;
        wifiPacket[wifiNdx++] = (byte) 0x00;
        wifiPacket[wifiNdx++] = (byte) 0x52;
        wifiPacket[wifiNdx++] = (byte) 0x00;
        for (int i = 0; i < 52; i++)
            wifiPacket[wifiNdx++] = 0x00;
        wifiPacket[wifiNdx] = CheckSum(wifiPacket, 1, wifiNdx-1);
        wifiNdx++;
        wifiPacket[wifiNdx++] = CMD_ETX;

        Log.v(TAG, "forgetWifi: ");
        Log.v(TAG, Utils.byteArrayToHex(wifiPacket)); //

        if (mSppService != null) {
            mSppService.write(wifiPacket, wifiNdx);
        }
    }

    @ReactMethod
    public void serviceInit(Callback callback) {
        if (mSppService == null) {
            //Initialize the BluetoothSppService to perform bluetooth connections
            mSppService = new BluetoothSppService(mContext, mSppHandler);
            mEEGParser = new EEGStreamParser(EEGStreamParser.PARSER_TYPE_PACKETS, this, null);
            mSleepTimerMgr = SleepTimerManager.getSharedInstance(this);
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mSppService.getState() == BluetoothSppService.STATE_NONE) {
                // Start the Bluetooth chat services
                Log.v(TAG, "onResume : mSppService.start");
                mSppService.start();
                callback.invoke(true);
             }
        }
    }

    public void serviceInit() {
        if (mSppService == null) {
            //Initialize the BluetoothSppService to perform bluetooth connections
            mSppService = new BluetoothSppService(mContext, mSppHandler);

            //Only if the state is STATE_NONE, do we know that we haven't started already
            if (mSppService.getState() == BluetoothSppService.STATE_NONE) {
                // Start the Bluetooth chat services
                Log.v(TAG, "onResume : mSppService.start");
                mSppService.start();
            }
        }
    }

    @ReactMethod
    protected void disconnectStation(String disconnectFrom, String deviceId) {
        isFromConnectStation = false;

        if (mSppService != null) {
            boolean isDisconnected = mSppService.stop(deviceId);
            // if(isDisconnected == true) {
            //     Log.d(TAG, "isDisconnected");
            //    // mSppService = null;
            // }

            if(disconnectFrom.equalsIgnoreCase("settings")) {
                disconnectFromListener = "StationConnectionLostListener";
            } else {
                disconnectFromListener = "StationConnectionLost2Listener";
            }
        }
    }

    @ReactMethod
    private void isSppServiceNull(Callback callback) {
        callback.invoke(mSppService == null);
    }

    @ReactMethod
    private void connectStation(String address, String connectFrom) {
        isFromConnectStation = true;

        serviceInit();
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mSppService.connect(device, false, address);

        if(connectFrom.equalsIgnoreCase("settings")) {
            connectFromListener = "StationConnectionStatusListener";
        } else {
            connectFromListener = "StationConnectionStatus2Listener";
        }
    }

    private void sendEvent(String eventName, @Nullable Object params) {
        getReactApplicationContext().getJSModule(RCTNativeAppEventEmitter.class).emit(eventName, params);
    }

    private void recvFromRFCommSocket(byte[] a_data, int aLen) {
        byte w_chksum = CheckSum(a_data, 1, aLen - 3);
        if (aLen <= 3) {
            Log.v(TAG, "invalid data format:" + Utils.byteArrayToHex(a_data));
            //recvFromRFCommCallback.invoke("invalid data format:" + Utils.byteArrayToHex(a_data));
        } else if (((a_data[0] & 0xff) != CMD_STX) ||
                ((a_data[aLen - 1] & 0xff) != CMD_ETX)) {
            Log.v(TAG, "STX,ETX Error");
            //recvFromRFCommCallback.invoke("STX,ETX Error");
        } else if ((a_data[aLen - 2] & 0xff) != (w_chksum & 0xff)) {
            Log.v(TAG, "Check sum error :" + (a_data[aLen - 2] & 0xff) + "<>" + (w_chksum & 0xff));
            //recvFromRFCommCallback.invoke("Check sum error :" + (a_data[aLen - 2] & 0xff) + "<>" + (w_chksum & 0xff));
        } else {
//            Log.v(TAG, "recv : " + Utils.byteArrayToHex(a_data) + " data_len: " + a_len);
//            Log.v(TAG, " data_len: " + aLen);
            byte wCmd = (byte) (a_data[1] & 0xff);
            switch (wCmd) {
                case STATION_CMD_EEG:
                    mSleepTimerMgr.initTimer();
                    mSleepTimerMgr.addBrainwaveInfo(w_chksum);
                    int byteLen = aLen - 3;
                    for (int i = 0; i < byteLen; i++) {
                        mEEGParser.parseByte(a_data[i + 6] & 0xff);
                    }
                    break;
                case STATION_CMD_SLEEP_INFO: {
                    SleepInfo sleepInfo = parseSleepInfoStream(a_data);
                    mSleepTimerMgr.addSleepInfo(sleepInfo);
                }
                break;
                case STATION_CMD_BATTERY:
                    int batteryInfo = parseBatteryDataStream(a_data, aLen);
                    System.out.println("handleResponseValue() BATTERYINFO: " + batteryInfo);
                    sendEvent("StationBatteryStatusListener", batteryInfo);
                    break;
                case STATION_CMD_WIFI:
                    int dataLen = a_data[4] & 0xFF;
                    int val = (dataLen != 0) ? a_data[6] & 0xff : 0 ;
                    System.out.println("handleResponseValue() WIFIINFO: " + val);
                    recvFromRFCommCallback.invoke(val);
                    break;
                case STATION_CMD_HEADSET:
                    int headsetStatus = parseVolLightHeadsetStream(a_data, aLen);
                    System.out.println("handleResponseValue() HEADSETINFO: " + headsetStatus);
                    sendEvent("HeadsetStatusListener", headsetStatus);
                    break;
                case STATION_CMD_VOLUME:
                    int volStatus = parseVolLightHeadsetStream(a_data, aLen);
                    sendEvent("StationVolumeStatusListener", volStatus);
                    break;
                case STATION_CMD_LIGHT:
                    int lightStatus = parseVolLightHeadsetStream(a_data, aLen);
                    sendEvent("StationLightStatusListener", lightStatus);
                    break;
                case STATION_CMD_DSP:
                    if (!isTriggeredFromApp) {
                        int binauralVal = parseBinauralStream(a_data, aLen);
                        sendEvent("StationBinauralStatusListener", binauralVal);
                    } else {
                        isTriggeredFromApp = false;
                    }
                    break;
                case STATION_CMD_DEV_STATE:
                    Log.d(TAG, "sim123 eqiupmentResp: " + Utils.byteArrayToHex(a_data, aLen, true));
                    sendEvent("StationLookupListener", parseEquipmentStatusStream(a_data, aLen));
                    break;
            }
        }
    }

    private SleepInfo parseSleepInfoStream(byte[] bytes) {
        System.out.println("sleepinfo() rawBytes: " + Utils.byteArrayToHex(bytes));
        byte[] timeAcqBuffer = new byte[5];
        byte[] signalMinBuffer = new byte[4];
        //time_acquired
        timeAcqBuffer[0] = bytes[6];
        timeAcqBuffer[1] = bytes[7];
        timeAcqBuffer[2] = bytes[8];
        timeAcqBuffer[3] = bytes[9];
        timeAcqBuffer[4] = bytes[10];
        //signal_minimun
        signalMinBuffer[0] = bytes[11];
        signalMinBuffer[1] = bytes[12];
        signalMinBuffer[2] = bytes[13];
        signalMinBuffer[3] = bytes[14];
        String sleepDateStr = Utils.byteArrayToIntString(timeAcqBuffer);
        float sleepEEGSpec = Utils.littleEndianToFloat(signalMinBuffer);
        int sleepSignalPoor = bytes[15] & 0xFF;
        return new SleepInfo(sleepDateStr, sleepEEGSpec, sleepSignalPoor);
    }

    private String getCurrentDate() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getDefault());
        return dateFormat.format(date);
    }

    private int parseBatteryDataStream(byte[] bytes, int aLen) {
        return bytes[aLen-3] & 0xFF;
    }

    private int parseBinauralStream(byte[] bytes, int aLen) {
        System.out.println("sim123 BinauralLen: " + aLen);
        return bytes[aLen-3] & 0xFF;
    }

    private int parseVolLightHeadsetStream(byte[] bytes, int aLen) {
        System.out.println("sim123 VolLightHeadsetLen: " + aLen);
        return bytes[aLen - 3] & 0xFF;
    }

    private WritableMap parseEquipmentStatusStream(byte[] bytes, int dLen) {
        WritableMap devMap = Arguments.createMap();
        int lightData = bytes[dLen - 10] & 0xFF;
        int volData = bytes[dLen - 9] & 0xFF;
        int binauralData = bytes[dLen - 6] & 0xFF;
        devMap.putInt("light", lightData);
        devMap.putInt("volume", volData);
        devMap.putInt("binaural", binauralData);
        return devMap;
    }

    private static WritableMap eegSpectrumDataToMap(float[] eegVal) {
        WritableMap eegMap = Arguments.createMap();
        eegMap.putDouble("delta", eegVal[0]);
        eegMap.putDouble("theta", eegVal[1]);
        eegMap.putDouble("alpha", eegVal[2]);
        eegMap.putDouble("beta", eegVal[3]);
        eegMap.putDouble("gamma", eegVal[4]);
        return eegMap;
    }

    private void validateInvalidPacket(byte[] ap_Buff, int a_Len) {
        byte[] w_recvdata = new byte[1024];
        int s_len = 0;
        int startFlag = 0;
        int endFlag = 0;
        int	sidx=0;
        int	eidx=0;

        int dLen;
        int k = 0;

        for (int i = 0; i<a_Len; i++) {
            if (((ap_Buff[i]) == CMD_STX) && startFlag == 0) {
                startFlag = 1;
                sidx = i;
                dLen = ap_Buff[sidx + 4];
                i = (sidx + 4) + 1 + 1 + dLen;
            }

            if (startFlag == 1 && endFlag == 0) {
                if ((i >= a_Len) || i < 0)
                    return;
                if (ap_Buff[i] == CMD_ETX) {
                    eidx = i;
                    s_len = eidx - sidx + 1;
                    for (int j = sidx; j < a_Len; j++) {
                        w_recvdata[k] = ap_Buff[j];
                        k++;
                    }
                    k = 0;
                    endFlag = 1;
                    if(startFlag + endFlag == 2) {
                        Log.d("ValidateInvalidPacket", "recvPacket: " + Utils.byteArrayToHex(w_recvdata));
                        recvFromRFCommSocket(w_recvdata, s_len);
                    }
                }
            }
        }
    }

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mSppHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                     switch (msg.arg1) {
                         case BluetoothSppService.STATE_CONNECTED:
                             Log.i(TAG, "Bluetooth SPP : Connected to " + mConnectedDeviceName);
                             sendEvent(connectFromListener, 3);
                             break;
                         case BluetoothSppService.STATE_CONNECTING:
                             Log.i(TAG, "Bluetooth SPP : Connecting");
                             sendEvent(connectFromListener, 1);
                             break;
                         case BluetoothSppService.STATE_NONE:
                             Log.i(TAG, "BlueTooth SPP : Not Connected");
                             sendEvent(connectFromListener, 0);
                             break;
                     }
                    break;
                case MESSAGE_READ:
//                    Log.i(TAG, "MESSAGE_READ");
                    byte[] readBuf = (byte[])msg.obj;
                    int buffLen = msg.arg1;
                    byte wCmd = (byte) (readBuf[1] & 0xff);
                    validateInvalidPacket(readBuf, buffLen);
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Log.i(TAG, "MESSAGE_DEVICE_NAME: Connected to" + mConnectedDeviceName);
                    sendEvent(connectFromListener, 2);
                    break;
                case MESSAGE_CONLOST:
                    Log.i(TAG, "MESSAGE_CONLOST: " + msg.getData().getString(CONMSG));
                    sendEvent(disconnectFromListener, 4);
                case MESSAGE_CONFAIL:
                    Log.i(TAG, "MESSAGE_CONFAIL: " + msg.getData().getString(CONMSG));
                    if (isFromConnectStation) {
                        sendEvent(connectFromListener, 5);
                    }
                    break;
            }
        }
    };

    private int parseGtbtFromDateString(String s) {
        StringBuilder sb = new StringBuilder();
        if (s.length() == 0 || s.length() < 10) {
            return 0;
        } else {
            sb.append(s.charAt(s.length() - 2));
            sb.append(s.charAt(s.length() - 1));
            return Integer.parseInt(sb.toString());
        }
    }

    @Override
    public void handleSleepInfoInterruption(List<SleepInfo> sleepData) {
        Log.d(TAG, "sleep123 didStop() size: " + sleepData.size());
        if (sleepData.size() > 29) {
            int L = 0;
            long[] timeAcq = new long[1440];
            double[] signalMin = new double[1440];
            int[] signalPoor = new int[1440];
            WritableArray hypnogramArr = Arguments.createArray();
            int goToBedTime = parseGtbtFromDateString(sleepData.get(0).getSleepDate());
            for (int i = 0; i < sleepData.size(); i++) {
                timeAcq[i] = Long.parseLong(sleepData.get(i).getSleepDate());
                signalMin[i] = sleepData.get(i).getSleepSpectrum();
                signalPoor[i] = sleepData.get(i).getSleepSignalPoor();
                L++;
            }
            EEGData eegData = new EEGData(timeAcq, signalMin, signalPoor, L);
            EEGAnalysis eegAnalysis = new EEGAnalysis(eegData);
            SleepAnalysis sleepAnalysis = new SleepAnalysis(eegData, eegAnalysis);
            for (int i = 0; i < eegAnalysis.getLength(); i++) {
                int hypnoVal = (eegAnalysis.getHypnogram()[i] < 0 || eegAnalysis.getHypnogram()[i] > 6) ? 0 : eegAnalysis.getHypnogram()[i];
                hypnogramArr.pushInt(hypnoVal);
            }
            WritableMap sleepMap = Arguments.createMap();
            sleepMap.putString("DATE", getCurrentDate());
            sleepMap.putInt(DataConstants.SLEEP_ANALYSIS_TOTAL, sleepAnalysis.getTotalSleepTime());
            sleepMap.putInt(DataConstants.SLEEP_ANALYSIS_LATENCY, sleepAnalysis.getSleepLatency());
            sleepMap.putInt(DataConstants.SLEEP_ANALYSIS_WAKE, sleepAnalysis.getWakeAfterSleepOnset());
            sleepMap.putDouble(DataConstants.SLEEP_ANALYSIS_POINTS, sleepAnalysis.getIndexTotal());
            sleepMap.putDouble(DataConstants.SLEEP_SQI_STABILITY, sleepAnalysis.getIndexStability());
            sleepMap.putDouble(DataConstants.SLEEP_SQI_AWAKEN, sleepAnalysis.getIndexAwaken());
            sleepMap.putDouble(DataConstants.SLEEP_SQI_CYCLE, sleepAnalysis.getIndexSleepCycle());
            sleepMap.putArray(DataConstants.SLEEP_HYPNOGRAM, hypnogramArr);
            sleepMap.putDouble(DataConstants.SLEEP_STABILITY_BRAINWAVE, sleepAnalysis.getStabilityBrainWave());
            sleepMap.putInt(DataConstants.SLEEP_GTBT, goToBedTime);
            System.out.println("sleepinfo() API: " + sleepMap);
            sendEvent("StationSleepInfoListener", sleepMap);
            sleepData.clear();
        } else {
            sleepData.clear();
        }
        sendEvent("StationEEGInterruptionListener", true);
    }

    @Override
    public void handleEEGDataValue(int extendedCodeLevel, int code, int numBytes, byte[] valueBytes, Object customData) {
//        System.out.println("handleEEGDataValue() codeInHex: " + String.format("%02x", code) + " codeInInteger: " + code + " extendedCodeLevel: " + extendedCodeLevel);
        if (extendedCodeLevel == 0) {
            switch (code) {
                case EEGStreamParser.PARSER_CODE_ATTENTION:
//                    System.out.println("ATTENTION: " + (valueBytes[0] & 0xFF));
                    sendEvent("EEGAttentionListener", valueBytes[0] & 0xFF);
                    break;
                case EEGStreamParser.PARSER_CODE_MEDITATION:
//                    System.out.println("MEDITATION: " + (valueBytes[0] & 0xFF));
                    sendEvent("EEGMeditationListener", (valueBytes[0] & 0xFF));
                    break;
                case EEGStreamParser.PARSER_CODE_ASIC_EEG_POWERS:
                    int delta = (valueBytes[0] & 0xFF) << 16 | (valueBytes[1] & 0xFF) << 8 | (valueBytes[2] & 0xFF);
                    int theta = (valueBytes[3] & 0xFF) << 16 | (valueBytes[4] & 0xFF) << 8 | (valueBytes[5] & 0xFF);
                    int lowAlpha = (valueBytes[6] & 0xFF) << 16 | (valueBytes[7] & 0xFF) << 8 | (valueBytes[8] & 0xFF);
                    int highAplha = (valueBytes[9] & 0xFF) << 16 | (valueBytes[10] & 0xFF) << 8 | (valueBytes[11] & 0xFF);
                    float alpha = (lowAlpha * 0.5f) + (highAplha * 0.5f);
                    int lowBeta = (valueBytes[12] & 0xFF) << 16 | (valueBytes[13] & 0xFF) << 8 | (valueBytes[14] & 0xFF);
                    int highBeta = (valueBytes[15] & 0xFF) << 16 | (valueBytes[16] & 0xFF) << 8 | (valueBytes[17] & 0xFF);
                    float beta = (lowBeta * 0.5f) + (highBeta * 0.5f);
                    int lowGamma = (valueBytes[18] & 0xFF) << 16 | (valueBytes[19] & 0xFF) << 8 | (valueBytes[20] & 0xFF);
                    int midGamma = (valueBytes[21] & 0xFF) << 16 | (valueBytes[22] & 0xFF) << 8 | (valueBytes[23] & 0xFF);
                    float gamma = (lowGamma * 0.1f) + (midGamma * 0.1f);
                    float[] eegSpectrum = new float[]{(delta * 0.1f), (theta * 0.1f), alpha, beta, gamma};
//                    System.out.println("DELTA: " + eegSpectrum[0] + " THETA: " + eegSpectrum[1] + " ALPHA: " + eegSpectrum[2] + " BETA: " + eegSpectrum[3] + " GAMMA: " + eegSpectrum[4]);
                    sendEvent("EEGSpectrumListener", eegSpectrumDataToMap(eegSpectrum));
                    break;
            }
        }
    }
}
