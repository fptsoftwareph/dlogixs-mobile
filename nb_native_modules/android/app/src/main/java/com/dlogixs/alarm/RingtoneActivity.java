package com.dlogixs.alarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import static com.dlogixs.alarm.RingtoneService.EXTRA_RINGING_OBJECT;

/**
 * Created by fptsoftware on 02/04/2018.
 */

public abstract class RingtoneActivity<T extends Parcelable> extends Activity {
    private T mRingingObject;

    protected abstract Parcelable.Creator<T> getParcelableCreator();
    protected abstract Class<? extends RingtoneService> getRingtoneServiceClass();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final byte[] bytes = getIntent().getByteArrayExtra(EXTRA_RINGING_OBJECT);
        if (bytes == null) {
            throw new IllegalStateException("Cannot start RingtoneActivity without a ringing object");
        }
        mRingingObject = ParcelableUtil.unmarshall(bytes, getParcelableCreator());
        System.out.println("RING_OBJECT:"+mRingingObject.toString());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        Intent intent = new Intent(this, getRingtoneServiceClass());
        intent.putExtra(RingtoneService.EXTRA_RINGING_OBJECT, ParcelableUtil.marshall(mRingingObject));
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected final T getRingingObject() {
        return mRingingObject;
    }

    protected final void stopAndFinish() {
        stopService(new Intent(this, getRingtoneServiceClass()));
        finish();
    }
}
