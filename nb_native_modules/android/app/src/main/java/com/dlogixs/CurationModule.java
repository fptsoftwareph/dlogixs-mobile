package com.dlogixs;

import android.widget.Toast;

import com.dlogixs.analysis.curation.ElementData;
import com.dlogixs.analysis.curation.Record;
import com.dlogixs.analysis.curation.ServiceCuration;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;

import com.dlogixs.analysis.curation.Curator;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CurationModule extends ReactContextBaseJavaModule {

    private Curator curator;
    private ServiceCuration curation;

    public CurationModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "CurationModule";
    }

    @ReactMethod
    public void alert(String message) {
        Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @ReactMethod
    public void getSampleText(Callback callback) {
        callback.invoke("Sample text");
    }
    
    @Override
    public Map<String, Object> getConstants() {
      final Map<String, Object> constants = new HashMap<>();
      constants.put(DataConstants.CURATION_IS_UPDATED, DataConstants.CURATION_IS_UPDATED);
      constants.put(DataConstants.CURATION_MODEL_DATE, DataConstants.CURATION_MODEL_DATE);
      constants.put(DataConstants.CURATION_DATA_DATE, DataConstants.CURATION_DATA_DATE);
      constants.put(DataConstants.CURATION_ELEMENT_MODEL, DataConstants.CURATION_ELEMENT_MODEL);
      constants.put(DataConstants.CURATION_ELEMENT_DATA, DataConstants.CURATION_ELEMENT_DATA);
      constants.put(DataConstants.CURATION_DIFFERENCE, DataConstants.CURATION_DIFFERENCE);
      constants.put(DataConstants.CURATION_DIRECTION, DataConstants.CURATION_DIRECTION);
      constants.put(DataConstants.CURATION_MESSAGE, DataConstants.CURATION_MESSAGE);
      return constants;
    }

    @ReactMethod
    public void initCurationAnalysis(ReadableArray database, ReadableMap data,
                                     Callback callback) {

        //List<ElementData> database2 = new ArrayList<>();
        List<Record> database2 = new ArrayList<>();

        for(int i=0; i<database.size(); i++){
            String date = database.getMap(i).getString("date");
            //int elementSBW = database.getMap(i).getInt("elementSBW");
            int elementTST = database.getMap(i).getInt("elementTST");
            int elementGTBT = database.getMap(i).getInt("elementGTBT");
            int elementSUN = database.getMap(i).getInt("elementSUN");
            int elementWALK = database.getMap(i).getInt("elementWALK");
            int elementCOFFEE = database.getMap(i).getInt("elementCOFFEE");
            int elementSMOKE = database.getMap(i).getInt("elementSMOKE");
            int elementDRINK = database.getMap(i).getInt("elementDRINK");

            int elementBB = 0;

            try {
                elementBB = database.getMap(i).getInt("elementBB");
            } catch (Exception e) {
                elementBB = 0;
                System.out.println("An exception occurred while trying to fetch the elementBB value " + i + ": " + e.toString());
            }

            /*int elementBB3Hz = database.getMap(i).getInt("elementBB3Hz");
            int elementBB6Hz = database.getMap(i).getInt("elementBB6Hz");
            int elementBB9Hz = database.getMap(i).getInt("elementBB9Hz");
            int elementBB12Hz = database.getMap(i).getInt("elementBB12Hz");*/
            int sqiAWAKEN = database.getMap(i).getInt("sqiAWAKEN");
            int sqiCYCLE = database.getMap(i).getInt("sqiCYCLE");
            int sqiSTABILITY = database.getMap(i).getInt("sqiSTABILITY");
            int sqiTOTAL = database.getMap(i).getInt("sqiTOTAL");
            int indexOfBb = database.getMap(i).getInt("indexOfBB");

             /*database2.add(new ElementData( date,  elementSBW,
             elementTST,  elementGTBT,  elementSUN,  elementWALK,
             elementCOFFEE,  elementSMOKE,  elementDRINK,
             elementBB3Hz,  elementBB6Hz,  elementBB9Hz,  elementBB12Hz,
             sqiAWAKEN,  sqiCYCLE,  sqiSTABILITY,  sqiTOTAL));*/

            database2.add(new Record(date, elementTST, elementGTBT, elementSUN, elementWALK, elementCOFFEE,
                    elementSMOKE, elementDRINK, elementBB, sqiAWAKEN, sqiCYCLE, sqiSTABILITY, indexOfBb));
        }

        String date = data.getString("date");
        //int elementSBW = data.getInt("elementSBW");
        int elementTST = data.getInt("elementTST");
        int elementGTBT = data.getInt("elementGTBT");
        int elementSUN = data.getInt("elementSUN");
        int elementWALK = data.getInt("elementWALK");
        int elementCOFFEE = data.getInt("elementCOFFEE");
        int elementSMOKE = 0;
        try {
            elementSMOKE = data.getInt("elementSMOKE");
        } catch (Exception e) {
            elementSMOKE = 0;
            System.out.println("An exception occurred while trying to fetch the elementSMOKE value: " + e.toString());
        }
        
        int elementDRINK = 0; 
        
         try {
            elementDRINK = data.getInt("elementDRINK");
        } catch (Exception e) {
            elementDRINK = 0;
            System.out.println("An exception occurred while trying to fetch the elementDRINK value: " + e.toString());
        }

        int elementBB = 0;

        try {
            elementBB = data.getInt("elementBB");
        } catch (Exception e) {
            elementBB = 0;
            System.out.println("An exception occurred while trying to fetch the elementBB value: " + e.toString());
        }

        /*int elementBB3Hz = data.getInt("elementBB3Hz");
        int elementBB6Hz = data.getInt("elementBB6Hz");
        int elementBB9Hz = data.getInt("elementBB9Hz");
        int elementBB12Hz = data.getInt("elementBB12Hz");*/
        int sqiAWAKEN = data.getInt("sqiAWAKEN");
        int sqiCYCLE = data.getInt("sqiCYCLE");
        int sqiSTABILITY = data.getInt("sqiSTABILITY");
        int sqiTOTAL = data.getInt("sqiTOTAL");
        int indexOfBb = data.getInt("indexOfBB");

        /*ElementData data2 = new ElementData(date,  elementSBW,
                elementTST,  elementGTBT,  elementSUN,  elementWALK,
                elementCOFFEE,  elementSMOKE,  elementDRINK,
                elementBB3Hz,  elementBB6Hz,  elementBB9Hz,  elementBB12Hz,
                sqiAWAKEN,  sqiCYCLE,  sqiSTABILITY,  sqiTOTAL);*/

        Record data2 = new Record(date, elementTST, elementGTBT, elementSUN, elementWALK, elementCOFFEE,
                elementSMOKE, elementDRINK, elementBB, sqiAWAKEN, sqiCYCLE, sqiSTABILITY, indexOfBb);

        //curator = new Curator(database2, data2);
        curation = new ServiceCuration(database2, data2);
        curation.selectModel();

        callback.invoke("CurationAnalysis data initialized");
    }

    @ReactMethod
    public void getCuratorAnalysisData(int element_index, String key, Callback callback) {

        switch(key) {

            case DataConstants.CURATION_IS_UPDATED:

                callback.invoke(curation.getModel(element_index).isUpdated());

                break;

            case DataConstants.CURATION_MODEL_DATE:

                //callback.invoke(curator.getCreatorByIndex(element_index).getModel().getDate());
                callback.invoke(curation.getModel(element_index).getDate());

                break;

            case DataConstants.CURATION_DATA_DATE:

                //callback.invoke(curator.getCreatorByIndex(element_index).getData().getDate());
                callback.invoke(curation.getData().getDate());

                break;

            case DataConstants.CURATION_ELEMENT_MODEL:

                //callback.invoke(curator.getCreatorByIndex(element_index).getElementOfModel());
                callback.invoke(curation.getModel(element_index).getElement(element_index));

                break;

            case DataConstants.CURATION_ELEMENT_DATA:

                //callback.invoke(curator.getCreatorByIndex(element_index).getElementOfData());
                callback.invoke(curation.getData().getElement(element_index));

                break;

            case DataConstants.CURATION_DIFFERENCE:

                //callback.invoke(curator.getCreatorByIndex(element_index).getDifference());
                callback.invoke(curation.getModel(element_index).getDifference());

                break;

            case DataConstants.CURATION_DIRECTION:

                //callback.invoke(curator.getCreatorByIndex(element_index).getDirection());
                callback.invoke(curation.getModel(element_index).getDirection());

                break;

            case DataConstants.CURATION_MESSAGE:

                //callback.invoke(new Gson().toJson(curator.getCreatorByIndex(element_index).getMessage()));
                callback.invoke(new Gson().toJson(curation.getGuideMessage(element_index)));

                break;

        }
    }
}
