package com.dlogixs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dlogixs.networkstate.NetworkReachability;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class NetworkStateModule extends ReactContextBaseJavaModule {
    private ReactApplicationContext mReactContext;
    static final private String TAG = "NetworkStateModule";

    public NetworkStateModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;

    }

    @Override
    public String getName() {
        return "ReachabilityModule";
    }

    @ReactMethod
    private void startNetworkNotifier() {
        mReactContext.registerReceiver(netReceiver, networkReachabilityIntentFilter());
    }

    @ReactMethod
    private void stopNetworkNotifier() {
        mReactContext.unregisterReceiver(netReceiver);
    }

    private static IntentFilter networkReachabilityIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(NetworkReachability.ACTION_CONNECTION_STATE_NOTIFICATION);
        intentFilter.addAction(NetworkReachability.ACTION_WIFI_STATE_NOTIFICATION);
        return intentFilter;
    }

    private final BroadcastReceiver netReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            WritableMap map = Arguments.createMap();
            WritableMap map2 = Arguments.createMap();
            if (NetworkReachability.ACTION_CONNECTION_STATE_NOTIFICATION.equals(intent.getAction())) {
                int connMode = NetworkReachability.getConnectivityInfo(context);
                Log.d(TAG, "net123 connMode: " + connMode);
                switch (connMode) {
                    case NetworkReachability.TYPE_WIFI:
                        map.putInt(DataConstants.REACHABILITY_MODE, NetworkReachability.STATUS_WIFI);
                        map.putString(DataConstants.REACHABILITY_TYPE, "Wifi");
                        map2.putInt(DataConstants.REACHABILITY_MODE, NetworkReachability.STATUS_WIFI);
                        map2.putString(DataConstants.REACHABILITY_TYPE, "Wifi");
                        break;
                    case NetworkReachability.TYPE_WWAN:
                        map.putInt(DataConstants.REACHABILITY_MODE, NetworkReachability.STATUS_WWAN);
                        map.putString(DataConstants.REACHABILITY_TYPE, "WWAN");
                        map2.putInt(DataConstants.REACHABILITY_MODE, NetworkReachability.STATUS_WWAN);
                        map2.putString(DataConstants.REACHABILITY_TYPE, "WWAN");
                        break;
                    case NetworkReachability.TYPE_NOT_REACHABLE:
                        map.putInt(DataConstants.REACHABILITY_MODE, NetworkReachability.STATUS_NOT_REACHABLE);
                        map.putString(DataConstants.REACHABILITY_TYPE, "NotReachable");
                        map2.putInt(DataConstants.REACHABILITY_MODE, NetworkReachability.STATUS_NOT_REACHABLE);
                        map2.putString(DataConstants.REACHABILITY_TYPE, "NotReachable");
                        break;
                }
                sendEvent("ReachabilityStatusListener", map);
                sendEvent("ReachabilityStatusListener2", map2);
            }
        }
    };

    private void sendEvent(String eventName, @Nullable WritableMap params) {
        if (mReactContext != null) {
            mReactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
        }
    }
}
