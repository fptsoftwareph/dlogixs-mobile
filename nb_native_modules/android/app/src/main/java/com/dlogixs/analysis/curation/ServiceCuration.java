package com.dlogixs.analysis.curation;

import java.util.ArrayList;
import java.util.List;

public class ServiceCuration {

	List<Record> database = new ArrayList<Record>();
	Record data;
	Record[] model;

	public ServiceCuration(List<Record> database, Record data) {
		this.database = new ArrayList<Record>();
		for(int i = 0; i < database.size(); i++){
			this.database.add(Record.makeClone(database.get(i)));
		}
		this.data = Record.makeClone(data);
		this.model = new Record[8];
	}

	public void selectModel(){
		selectModel(Record.ELEMENT_TST);
		selectModel(Record.ELEMENT_GTBT);
		selectModel(Record.ELEMENT_SUN);
		selectModel(Record.ELEMENT_WALK);
		selectModel(Record.ELEMENT_COFFEE);
		selectModel(Record.ELEMENT_SMOKE);
		selectModel(Record.ELEMENT_DRINK);
		selectModel(Record.ELEMENT_BB);
	}

	private void selectModel(int element_enum){
		// 1. 과거 데이터베이스에서 해당 요소의 모든 값을 불러오고 그 차이 값을 구한다.
		int[] value_database = new int[database.size()];
		int[] value_abs = new int[database.size()];
		for(int i = 0; i < database.size(); i++){
			value_database[i] = database.get(i).getElement(element_enum);
			value_abs[i] = Math.abs(rounding(value_database[i], element_enum) - rounding(data.getElement(element_enum), element_enum));
		}
		// 2. data의 요소 값과 가장 가까운 nCompares 개의 Record를 database에서 선정하여  candidate로 복사
		int nCompares = 5;
		int[] rank = new int[database.size()];
		for(int i = 0; i < database.size(); i++){
			rank[i] = 1;
			for(int j = 0; j < database.size(); j++){
				if(value_abs[i] > value_abs[j]){
					rank[i]++;
				}
			}
		}
		List<Record> candidate = new ArrayList<Record>();;
		if(database.size() <= nCompares){ // database.size() <= nCompares
			for(int rank_val = 1; rank_val <= database.size(); rank_val++){ // rank_val = 1, 2, ... , database.size()
				for(int i = database.size()-1; i >= 0; i--){ // 순위가 같을 때, 최근 날짜부터 검색하도록 역으로 설정
					if(rank[i] == rank_val){
						candidate.add(Record.makeClone(database.get(i)));
					}
				}
			}
		}
		else{ // nCompares < database.size()
			for(int rank_val = 1; rank_val <= nCompares; rank_val++){ // rank_val = 1, 2, ... , nCompares
				for(int i = database.size()-1; i >= 0; i--){ // 순위가 같을 때, 최근 날짜부터 검색하도록 역으로 설정
					if(rank[i] == rank_val){
						candidate.add(Record.makeClone(database.get(i)));
					}
				}
			}
		}
		// 3. 모든 candidate 항목과 data의 INDEX_STANDARD를 비교
		boolean isCaseUpdated = true;
		for(int i = 0; i < candidate.size(); i++){
			if(candidate.get(i).getIndex(Record.INDEX_STANDARD) > data.getIndex(Record.INDEX_STANDARD)){
				isCaseUpdated = false;
			}
		}
		//System.out.print(element_enum+"\t");
		// 4-1. Case.A: candidate에서 가장 큰 INDEX_STANDARD 값이 검출(과거 데이터가 RM, Not Updated)
		if(!isCaseUpdated){
			//System.out.print("NU\t");
			// candidate의 항목 중 INDEX_STANDED가 가장 큰 항목을 model로 선정
			int max_index_standard = Integer.MIN_VALUE;
			for(int i = 0; i < candidate.size(); i++){
				if(candidate.get(i).getIndex(Record.INDEX_STANDARD) > max_index_standard){
					max_index_standard = candidate.get(i).getIndex(Record.INDEX_STANDARD);
					model[element_enum] = Record.makeClone(candidate.get(i));
				}
			}
			int value_model = rounding(model[element_enum].getElement(element_enum), element_enum);
			int value_data = rounding(data.getElement(element_enum), element_enum);
			model[element_enum].setDifference(value_model - value_data);
			// 4-1-1. Case.A1: model의 요소 값이 data의 요소 값 보다 클 때
			if(value_model > value_data){
				//System.out.println("1");
				model[element_enum].setDirection(1);
			}
			// 4-1-2. Case.A2: model의 요소 값이 data의 요소 값 보다 작을 때
			else if(value_model < value_data){
				//System.out.println("-1");
				model[element_enum].setDirection(-1);
			}
			// 4-1-3. Case.A3: model의 요소 값이 data의 요소 값과 같을 때
			else{
				//System.out.println("0");
				model[element_enum].setDirection(0);
			}
			model[element_enum].setUpdated(false);
		}
		// 4-2. Case.B: data의 INDEX_STANDARD 값이 가장 큼(현재 데이터가 RM, Updated)
		else{
			//System.out.print("U\t");
			// data를 model로 선정
			model[element_enum] = Record.makeClone(data);
			int value_model = 0;
			int value_canidate_max = Integer.MIN_VALUE;
			int value_canidate_min = Integer.MAX_VALUE;
			for(int i = 0; i < candidate.size(); i++){
				if(candidate.get(i).getElement(element_enum) > value_canidate_max){
					value_canidate_max = candidate.get(i).getElement(element_enum);
				}
				if(candidate.get(i).getElement(element_enum) < value_canidate_min){
					value_canidate_min = candidate.get(i).getElement(element_enum);
				}
			}
			value_model = rounding(model[element_enum].getElement(element_enum), element_enum);
			value_canidate_max = rounding(value_canidate_max, element_enum);
			value_canidate_min = rounding(value_canidate_min, element_enum);
			model[element_enum].setDifference(0);
			// 4-2-1. Case.B1: model의 요소 값이 cadidate의 모든 요소 값보다 클 때
			if(value_model >= value_canidate_max){
				System.out.println("1");
				//model[element_enum].setDirection(1);
			}
			// 4-2-2. Case.B2: model의 요소 값이 cadidate의 모든 요소 값보다 작을 때
			else if(value_model <= value_canidate_min){
				//System.out.println("-1");
				model[element_enum].setDirection(-1);
			}
			// 4-2-3. Case.B3: model의 요소 값이 cadidate의 모든 요소 값 안에 위치할 때
			else{
				//System.out.println("0");
				model[element_enum].setDirection(0);
			}
			model[element_enum].setUpdated(true);
		}

		// 5. Binaural Beat는 청취 Hz에 따라 추가 연산
		/*if(element_enum != Record.ELEMENT_BB) return;
		if(!isCaseUpdated){
			model[element_enum]
		}
		*/
	}

	private int timeRounding(int value){
		return (int)(Math.round((float)value/5.0)*5.0);
	}

	private int step2TimeRounding(int step){
		return timeRounding((int)(Math.round((float)step/60.0)));
	}

	private int rounding(int value, int element_enum){
		switch(element_enum){
			case Record.ELEMENT_WALK:
				return step2TimeRounding(value);
			case Record.ELEMENT_TST:
			case Record.ELEMENT_GTBT:
			case Record.ELEMENT_SUN:
				return timeRounding(value);
			case Record.ELEMENT_COFFEE:
			case Record.ELEMENT_SMOKE:
			case Record.ELEMENT_DRINK:
			case Record.ELEMENT_BB:
				return value;
		}
		return -2000;
	}

	public CurationMessage getGuideMessage(int element_enum){
		int diff = model[element_enum].getDifference();
		int modelFreq = model[element_enum].getIndexBinauralBeat();
		boolean isUpdated = model[element_enum].isUpdated();

		switch(modelFreq){
			case Record.BINAURAL_BEAT_3kHz: modelFreq = 3; break;
			case Record.BINAURAL_BEAT_6kHz: modelFreq = 6; break;
			case Record.BINAURAL_BEAT_9kHz: modelFreq = 9; break;
			case Record.BINAURAL_BEAT_12kHz: modelFreq = 12; break;
		}

		switch(element_enum){
			case Record.ELEMENT_TST:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 2);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 1);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 3);
				}
				else{
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 4);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 5);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 6);
				}
			case Record.ELEMENT_GTBT:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 2);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 1);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 3);
				}
				else{
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 4);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 5);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 6);
				}
			case Record.ELEMENT_SUN:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 2);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 1);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 3);
				}
				else{
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 4);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 5);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 6);
				}
			case Record.ELEMENT_WALK:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 2);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 1);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 3);
				}
				else{
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 4);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 5);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 6);
				}
			case Record.ELEMENT_COFFEE:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 2);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 1);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 3);
				}
				else{
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 4);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 5);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 6);
				}
			case Record.ELEMENT_SMOKE:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 2);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 1);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 3);
				}
				else{
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 4);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 5);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 6);
				}
			case Record.ELEMENT_DRINK:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 2);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 1);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 3);
				}
				else{
					if(model[element_enum].getDirection() < 0) return new CurationMessage(diff, 4);
					if(model[element_enum].getDirection() > 0) return new CurationMessage(diff, 5);
					if(model[element_enum].getDirection() == 0) return new CurationMessage(diff, 6);
				}
			case Record.ELEMENT_BB:
				if(!isUpdated){
					if(model[element_enum].getDirection() < 0)
						return new CurationMessage(diff, 2, modelFreq);
					if(model[element_enum].getDirection() > 0)
						return new CurationMessage(diff, 1, modelFreq);
					if(model[element_enum].getDirection() == 0)
						return new CurationMessage(diff, 3, modelFreq);
				}
				else{
					if(model[element_enum].getDirection() < 0)
						return new CurationMessage(diff, 4, modelFreq);
					if(model[element_enum].getDirection() > 0)
						return new CurationMessage(diff, 5, modelFreq);
					if(model[element_enum].getDirection() == 0)
						return new CurationMessage(diff, 6, modelFreq);
				}
		}
		return new CurationMessage(0,0, modelFreq);
	}


	public Record getData() {
		return data;
	}

	public Record getModel(int element_enum) {
		return model[element_enum];
	}
}
