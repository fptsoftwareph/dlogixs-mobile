package com.dlogixs.station;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.dlogixs.StationModule;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

public class BluetoothSppService
{
   // Debugging
   private static final String TAG = "BluetoothSppService";
   private static final boolean D = true;
   
   // Name for the SDP record when creating server socket
   private static final String NAME_SECURE = "BluetoothSppSecure";
   private static final String NAME_INSECURE = "BluetoothSppInsecure";
   
   // Unique UUID for this application
//   private static final UUID MY_UUID_SECURE = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
//   private static final UUID MY_UUID_INSECURE = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
   private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
   private static final UUID MY_UUID_INSECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
   
   // Member fields
   private final BluetoothAdapter mAdapter;
   private final Handler mHandler;
   private ConnectThread mConnectThread = null;
   private ConnectedThread mConnectedThread = null;
   public static int mState = 0;
   
   // Constants that indicate the current connection state
   public static final int STATE_NONE = 0;          // we're doing nothing
//   public static final int STATE_LISTEN = 1;        // now listening for incoming connections
   public static final int STATE_CONNECTING = 2;    // now initiating an outgoing connection
   public static final int STATE_CONNECTED = 3;     // now connected to a remote device

   private BluetoothA2dp mBluetoothA2DP = null;
   private BluetoothProfile.ServiceListener mProfileListener = new BluetoothProfile.ServiceListener() {
      @Override
      public void onServiceConnected(int profile, BluetoothProfile proxy) {
         if (profile == BluetoothProfile.A2DP) {
            Log.d(TAG, "A2DP onServiceConnected");
            mBluetoothA2DP = (BluetoothA2dp) proxy;
         }
      }

      @Override
      public void onServiceDisconnected(int profile) {
         if (profile == BluetoothProfile.A2DP) {
            mBluetoothA2DP = null;
            Log.d(TAG, "A2DP onServiceDisconnected");
         }
      }
   };

   /**
    * Constructor. Prepares a new BluetoothChat session.
    * 
    * @param context
    *           The UI Activity Context
    * @param handler
    *           A Handler to send messages back to the UI Activity
    */
   public BluetoothSppService(Context context, Handler handler)
   {
      mAdapter = BluetoothAdapter.getDefaultAdapter();
      mState = STATE_NONE;
      mHandler = handler;
      mAdapter.getProfileProxy(context, mProfileListener, BluetoothProfile.A2DP);
   }
   
   
   /**
    * Set the current state of the chat connection
    * 
    * @param state
    *           An integer defining the current connection state
    */
   private synchronized void setState(int state)
   {
      if (D)
         Log.i(TAG, "setState() " + mState + " -> " + state);
      mState = state;
      
      // Give the new state to the Handler so the UI Activity can update
      mHandler.obtainMessage(StationModule.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
   }
   
   
   /**
    * Return the current connection state.
    */
   public synchronized int getState()
   {
      return mState;
   }
   
   
   /**
    * Start the service. Specifically start AcceptThread to begin a session
    * in listening (server) mode. Called by the Activity onResume()
    */
   public synchronized void start()
   {
      if (D)
         Log.i(TAG, "start");
      
      // Cancel any thread attempting to make a connection
      if (mConnectThread != null)
      {
         mConnectThread.cancel();
         mConnectThread = null;
      }
      
      // Cancel any thread currently running a connection
      if (mConnectedThread != null)
      {
         mConnectedThread.cancel();
         mConnectedThread = null;
      }
   }

   private void connectA2DP(String deviceId){
      Log.d(TAG, "bt123 connectA2DP");

      BluetoothDevice mmDevice = mAdapter.getRemoteDevice(deviceId);

      if (mBluetoothA2DP != null) {
         Method connect = null;
         try {
            connect = BluetoothA2dp.class.getDeclaredMethod("connect", BluetoothDevice.class);
            Log.d(TAG, "connectA2DP mBluetoothA2DP getDeclaredMethod");
         } catch (NoSuchMethodException e) {
            Log.e("ERROR", "Unable to find connect(BluetoothDevice) method in BluetoothA2dp proxy.");
         }

         if (connect == null || mmDevice == null) {
            return;
         }
         try {
            connect.setAccessible(true);
            connect.invoke(mBluetoothA2DP, mmDevice);
            Log.d(TAG, "connectA2DP mBluetoothA2DP invoke");
         } catch (IllegalAccessException e) {
            Log.e("ERROR", "Illegal Access! " + e.toString());
         } catch (InvocationTargetException e) {
            Log.e("ERROR", "Unable to invoke connect(BluetoothDevice) method on proxy. " + e.toString());
         }
      } else {
         Log.d(TAG, "connectA2DP mBluetoothA2DP == null");
      }
   }

   private boolean disconnectA2DP(String deviceId){
   Log.d(TAG, "disconnectA2DP");
      BluetoothDevice mmDevice = mAdapter.getRemoteDevice(deviceId);
      boolean isDisconnected = false;
      if (mBluetoothA2DP != null && mmDevice != null) {
         Method connect = null;
         try {
            connect = BluetoothA2dp.class.getDeclaredMethod("disconnect", BluetoothDevice.class);
            //mAdapter.closeProfileProxy(BluetoothProfile.A2DP, mBluetoothA2DP);
            Log.d(TAG, "disconnectA2DP getDeclaredMethod");
         } catch (NoSuchMethodException e) {
            Log.e("ERROR", "Unable to find connect(BluetoothDevice) method in BluetoothA2dp proxy.");
         }

         if (connect == null || mmDevice == null) {
            // return;
            Log.d(TAG, "disconnectA2DP connect == null || mmDevice == null");
            return false;
         }
         try {
            connect.setAccessible(true);
            connect.invoke(mBluetoothA2DP, mmDevice);
            isDisconnected = true;
            Log.d(TAG, "disconnectA2DP connect.invoke()");
         } catch (IllegalAccessException e) {
            Log.e("ERROR", "Illegal Access! " + e.toString());
         } catch (InvocationTargetException e) {
            Log.e("ERROR", "Unable to invoke connect(BluetoothDevice) method on proxy. " + e.toString());
         }
      }
      return isDisconnected;
   }

   /**
    * Start the ConnectThread to initiate a connection to a remote device.
    * 
    * @param device
    *           The BluetoothDevice to connect
    * @param secure
    *           Socket Security type - Secure (true) , Insecure (false)
    */
   public synchronized void connect(BluetoothDevice device, boolean secure, String address)
   {
      if (D)
         Log.i(TAG, "connect to: " + device);
      
      // Cancel any thread attempting to make a connection
      if (mState == STATE_CONNECTING)
      {
         if (mConnectThread != null)
         {
            mConnectThread.cancel();
            mConnectThread = null;
         }
      }
      
      // Cancel any thread currently running a connection
      if (mConnectedThread != null)
      {
         mConnectedThread.cancel();
         mConnectedThread = null;
      }

      // Start the thread to connect with the given device
      mConnectThread = new ConnectThread(device, secure);
      connectA2DP(address);
      mConnectThread.start();
      setState(STATE_CONNECTING);
   }
   
   
   /**
    * Start the ConnectedThread to begin managing a Bluetooth connection
    * 
    * @param socket
    *           The BluetoothSocket on which the connection was made
    * @param device
    *           The BluetoothDevice that has been connected
    */
   public synchronized void connected(BluetoothSocket socket, BluetoothDevice device, final String socketType)
   {
      if (D)
         Log.i(TAG, "connected, Socket Type:" + socketType);
      
      // Cancel the thread that completed the connection
      if (mConnectThread != null)
      {
         mConnectThread.cancel();
         mConnectThread = null;
      }
      
      // Cancel any thread currently running a connection
      if (mConnectedThread != null)
      {
         mConnectedThread.cancel();
         mConnectedThread = null;
      }

      // Start the thread to manage the connection and perform transmissions
      mConnectedThread = new ConnectedThread(socket, socketType);
      mConnectedThread.start();
      
      // Send the name of the connected device back to the UI Activity
      Message msg = mHandler.obtainMessage(StationModule.MESSAGE_DEVICE_NAME);
      Bundle bundle = new Bundle();
      bundle.putString(StationModule.DEVICE_NAME, device.getName());
      msg.setData(bundle);
      mHandler.sendMessage(msg);
      
      setState(STATE_CONNECTED);
   }

   private void unpairDevice(String deviceId) {
      Set<BluetoothDevice> bondedDevices = mAdapter.getBondedDevices();
      try {
         Class<?> btDeviceInstance =  Class.forName(BluetoothDevice.class.getCanonicalName());
         Method m = btDeviceInstance.getMethod("removeBond");
         for (BluetoothDevice bluetoothDevice : bondedDevices) {
            String mac = bluetoothDevice.getAddress();
            if(mac.equals(deviceId)) {
               m.invoke(bluetoothDevice);
               Log.i("unpairDevice","Cleared Pairing");
               break;
            }
         }
      } catch (Exception e) {
         Log.e("unpairDevice", "Error pairing", e);
      }
   }
   
   /**
    * Stop all threads
    */
   public synchronized boolean stop(String deviceId)
   {
      boolean isDisconnected = false;
      isDisconnected = disconnectA2DP(deviceId);

      if (D)
         Log.i(TAG, "stop");
      
      if (mConnectThread != null)
      {
         mConnectThread.cancel();
         mConnectThread = null;
      }
      
      if (mConnectedThread != null)
      {
         mConnectedThread.cancel();
         mConnectedThread = null;
      }
      //unpairDevice(deviceId);
      setState(STATE_NONE);
      Log.d(TAG, "stop isDisconnected: " + isDisconnected);
      return isDisconnected;
   }
   
   
   /**
    * Write to the ConnectedThread in an unsynchronized manner
    * 
    * @param out
    *           The bytes to write
    * @see ConnectedThread#write(byte[])
    */
   public void write(byte[] out)
   {
      // Create temporary object
      ConnectedThread r;
      // Synchronize a copy of the ConnectedThread
      synchronized (this)
      {
         if (mState != STATE_CONNECTED)
            return;
         r = mConnectedThread;
      }
      // Perform the write unsynchronized
      r.write(out);
   }

   public void write(byte[] out, int len)
   {
      // Create temporary object
      ConnectedThread r;
      // Synchronize a copy of the ConnectedThread
      synchronized (this)
      {
         if (mState != STATE_CONNECTED)
            return;
         r = mConnectedThread;
      }
      // Perform the write unsynchronized
      r.write(out, len);
   }

   /**
    * Indicate that the connection attempt failed and notify the UI Activity.
    */
   private void connectionFailed()
   {
       mState = STATE_NONE;

      // Send a failure message back to the Activity
      Message msg = mHandler.obtainMessage(StationModule.MESSAGE_CONFAIL);
      Bundle bundle = new Bundle();
      bundle.putString(StationModule.CONMSG, "Unable to connect device");
      msg.setData(bundle);
      mHandler.sendMessage(msg);


      // Start the service over to restart listening mode
      BluetoothSppService.this.start();
   }
   
   
   /**
    * Indicate that the connection was lost and notify the UI Activity.
    */
   private void connectionLost()
   {
       mState = STATE_NONE;

      // Send a failure message back to the Activity
      Message msg = mHandler.obtainMessage(StationModule.MESSAGE_CONLOST);
      Bundle bundle = new Bundle();
      bundle.putString(StationModule.CONMSG, "Device connection was lost");
      msg.setData(bundle);
      mHandler.sendMessage(msg);
      
      // Start the service over to restart listening mode
      BluetoothSppService.this.start();
   }

   /**
    * This thread runs while attempting to make an outgoing connection with a
    * device. It runs straight through; the connection either succeeds or fails.
    */
   private class ConnectThread extends Thread
   {
      private final BluetoothSocket mmSocket;
      private final BluetoothDevice mmDevice;
      private String mSocketType;
      
      
      public ConnectThread(BluetoothDevice device, boolean secure)
      {
         mmDevice = device;
         BluetoothSocket tmp = null;
         mSocketType = secure ? "Secure" : "Insecure";
         
         // Get a BluetoothSocket for a connection with the given BluetoothDevice
         try
         {
            if (secure)
            {
               tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
            }
            else
            {
               tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
            }
         }
         catch (IOException e)
         {
            Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
         }
         mmSocket = tmp;
      }

      public BluetoothDevice getMmDevice() {
         return mmDevice;
      }

      public void run()
      {
         Log.i(TAG, "BEGIN mConnectThread SocketType:" + mSocketType);
         setName("ConnectThread" + mSocketType);
         
         // Always cancel discovery because it will slow down a connection
         mAdapter.cancelDiscovery();
         
         // Make a connection to the BluetoothSocket
         try
         {
            // This is a blocking call and will only return on a successful connection or an exception
            mmSocket.connect();
         }
         catch (IOException e)
         {
            // Close the socket
            try
            {
               mmSocket.close();
            }
            catch (IOException e2)
            {
               Log.e(TAG, "unable to close() " + mSocketType + " socket during connection failure", e2);
            }
            connectionFailed();
            return;
         }
         
         // Reset the ConnectThread because we're done
         synchronized (BluetoothSppService.this)
         {
            mConnectThread = null;
         }
         
         // Start the connected thread
         connected(mmSocket, mmDevice, mSocketType);
      }
      
      
      public void cancel()
      {
         try
         {
            mmSocket.close();
         }
         catch (IOException e)
         {
            Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
         }
      }
   }
   
   /**
    * This thread runs during a connection with a remote device. It handles all
    * incoming and outgoing transmissions.
    */
   private class ConnectedThread extends Thread
   {
      private final BluetoothSocket mmSocket;
      private final InputStream mmInStream;
      private final OutputStream mmOutStream;

      
      public ConnectedThread(BluetoothSocket socket, String socketType)
      {
         Log.i(TAG, "create ConnectedThread: " + socketType);
         mmSocket = socket;
         InputStream tmpIn = null;
         OutputStream tmpOut = null;

         // Get the BluetoothSocket input and output streams
         try
         {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
         }
         catch (IOException e)
         {
            Log.e(TAG, "temp sockets not created", e);
         }
         
         mmInStream = tmpIn;
         mmOutStream = tmpOut;
      }


      byte     SL_CMD_STX   = 0x02;
      byte     SL_CMD_ETX   = 0x03;
      int      SL_SD_MAX    = 256;
      int	   m_ReadSP = 0;
      int	   m_ReadEP = 0;
      int      SL_READ_BUFF_SIZE=1024;
      byte[]   m_ReadBuff = new byte[SL_READ_BUFF_SIZE];

      void ReadAndPush(byte[] a_RBuff, int a_Len)
      {
         byte[]     w_TBuff = new byte[SL_SD_MAX+1];
         int		w_ii;
         int		w_TempEP = 0;
         int		w_TempPos = 0;

// READ
         if(a_Len > 0) {
            Log.v(TAG, "ReadAndPush() : "+ Utils.byteArrayToHex(a_RBuff, a_Len, true));
            for(w_ii=0;w_ii<a_Len;w_ii++) {
               m_ReadBuff[m_ReadSP] = a_RBuff[w_ii];
               m_ReadSP = (m_ReadSP + 1) % SL_READ_BUFF_SIZE;
            }
   // PUSH
            w_TempEP = m_ReadEP;
            while(m_ReadSP != w_TempEP) {
               if(m_ReadBuff[w_TempEP] == SL_CMD_STX) {
                  w_TempPos = 0;
                  w_TBuff[w_TempPos] = m_ReadBuff[w_TempEP];
//                  Log.v(TAG, "w_TempPosA:" + w_TempPos + ":" + String.format("%02X ", (int)w_TBuff[w_TempPos]&0xff));
                  m_ReadEP = w_TempEP;
                  w_TempPos++;
               } else {
                  if(w_TempPos > 0) {
                     if(w_TempPos <= SL_SD_MAX) {
                        w_TBuff[w_TempPos] = m_ReadBuff[w_TempEP];
//                        Log.v(TAG, "w_TempPosB:" + w_TempPos + ":" + String.format("%02X ", (int)w_TBuff[w_TempPos]&0xff));
                        w_TempPos++;
                        if(m_ReadBuff[w_TempEP] == SL_CMD_ETX) {
//                           mHandler.obtainMessage(StationSppModule.MESSAGE_READ, w_TempPos, -1, w_TBuff).sendToTarget();
                             //StationSppModule.mHcareComm.recvPacket(null, w_TBuff, w_TempPos);
                        }
                     } else {
                        w_TempPos = 0;
                        m_ReadEP = w_TempEP;
                     }
                  }
               }
               w_TempEP = (w_TempEP + 1) % SL_READ_BUFF_SIZE;
            }
         }
      }
      
       public void run()
      {
         Log.i(TAG, "BEGIN mConnectedThread");
         byte[] buffer;
         int bytes;

         // Keep listening to the InputStream while connected
         while (true)
         {
            try
            {
               buffer = new byte[1024];
               // Read from the InputStream
               bytes = mmInStream.read(buffer);
               // Send the obtained bytes to the UI
               mHandler.obtainMessage(StationModule.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
               buffer = null;
            }
            catch (IOException e)
            {
               Log.e(TAG, "disconnected", e);
               connectionLost();
               // Start the service over to restart listening mode
               BluetoothSppService.this.start();
               break;
            }
         }
      }
      
      
      /**
       * Write to the connected OutStream.
       * 
       * @param buffer
       *           The bytes to write
       */
      public void write(byte[] buffer)
      {
         try
         {
            mmOutStream.write(buffer);
            
            // Share the sent message back to the UI Activity
//            mHandler.obtainMessage(StationSppModule.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
         }
         catch (IOException e)
         {
            Log.e(TAG, "Exception during write", e);
         }
      }

      public void write(byte[] buffer, int len)
      {
         try
         {
            mmOutStream.write(buffer, 0, len);

            // Share the sent message back to the UI Activity
//            mHandler.obtainMessage(StationSppModule.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
         }
         catch (IOException e)
         {
            Log.e(TAG, "Exception during write", e);
         }
      }
      
      public void cancel()
      {
         try
         {
            mmSocket.close();
         }
         catch (IOException e)
         {
            Log.e(TAG, "close() of connect socket failed", e);
         }
      }
   }
}
