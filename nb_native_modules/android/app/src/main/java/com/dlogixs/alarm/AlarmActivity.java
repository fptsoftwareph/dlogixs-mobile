package com.dlogixs.alarm;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dlogixs.R;
import android.util.Log;

public class AlarmActivity extends RingtoneActivity<Alarm> {

    private Button mbtnSnooze, mbtnStop;
    private boolean isRbsEnabled = false;
    private AlarmController mAlarmController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        mbtnSnooze = findViewById(R.id.btn_snooze);
        mbtnStop = findViewById(R.id.btn_stop);

        isRbsEnabled = getIntent().getExtras().getBoolean("rbs_enabled");
        Log.d("AlarmActivity", "onCreate: " + isRbsEnabled);

        mAlarmController = new AlarmController(this);
        Intent intent = new Intent();
        intent.putExtra("rbs_enabled", isRbsEnabled);
        intent.setAction(AlarmScheduler.ACTION_ALARM_ON_NOTIFICATION);
        sendBroadcast(intent);

        mbtnSnooze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("rbs_enabled", isRbsEnabled);
                intent.setAction(AlarmScheduler.ACTION_ALARM_STOPPED_NOTIFICATION);
                sendBroadcast(intent);

                System.out.println("ALARM_OBJECT:"+getRingingObject());
                mAlarmController.snoozeAlarm(getRingingObject());
                stopAndFinish();
            }
        });

        mbtnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("ALARM_OBJECT:"+getRingingObject());
                mAlarmController.cancelAlarm(getRingingObject(),true,false);
                stopAndFinish();
            }
        });

    }

    @Override
    protected Parcelable.Creator<Alarm> getParcelableCreator() {
        return Alarm.CREATOR;
    }

    @Override
    protected Class<? extends RingtoneService> getRingtoneServiceClass() {
        return AlarmRingtoneService.class;
    }
}
