package com.dlogixs;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.LongSparseArray;

import com.dlogixs.alarm.AlarmScheduler;
import com.dlogixs.alarm.AlarmToneManager;
import com.dlogixs.alarm.Downloader;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.modules.core.RCTNativeAppEventEmitter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fptsoftware on 14/03/2018.
 */

public class AlarmModule extends ReactContextBaseJavaModule {

    private AlarmScheduler alarmScheduler;
    private AlarmToneManager alarmToneManager;
    private Downloader downloader;
    List<Long> list = new ArrayList<>();
    private Callback onDone=null;

    public AlarmModule(ReactApplicationContext context) {
        super(context);
        alarmScheduler = new AlarmScheduler(context.getApplicationContext());
        context.registerReceiver(alarmReceiver, alarmNotifIntentFilter());
        context.registerReceiver(alarmOnReceiver, alarmOnNotifIntentFilter());
        context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        downloader = new Downloader(context);
        alarmToneManager = new AlarmToneManager(context.getApplicationContext());
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @ReactMethod
    public void schedule(ReadableMap alarmDetails) {
        Bundle bundle = Arguments.toBundle(alarmDetails);
        alarmScheduler.onTimeSet(bundle);
    }

    @ReactMethod
    public void scheduleUpdate(ReadableMap alarmDetails) {
        Bundle bundle = Arguments.toBundle(alarmDetails);
        System.out.println("scheduleUpdate"+alarmDetails);
        alarmScheduler.onTimeUpdate(bundle);
    }

    @ReactMethod
    public void scheduleDelete(int id) {
        System.out.println("scheduleDelete"+id);
        alarmScheduler.onTimeDelete(id);
    }

    @ReactMethod
    public void scheduleToggle(int id, boolean isAlarmNotRingingAndSwitchOff) {
        System.out.println("scheduleToggle"+id);
        alarmScheduler.setToggle(id, isAlarmNotRingingAndSwitchOff);
    }

    @ReactMethod
    public void disableAlarms() {
        System.out.println("disableAlarms");
        alarmScheduler.disableAlarms();
    }

    @ReactMethod
    public void getScheduledAlarms(Callback callback) {
        callback.invoke(alarmScheduler.getScheduledAlarms());
    }

    @ReactMethod
    public void getAlarmRingtones(Callback callback) {

        callback.invoke(alarmToneManager.getAlarmRingtones());
    }

    @ReactMethod
    public void playRingtone(String encodedUri) {
        Uri ringtone = Uri.parse(encodedUri);
        alarmToneManager.playRingtone(ringtone);
    }

    @ReactMethod
    public void stopRingtone() {
        alarmToneManager.destroyLocalPlayer();
    }

    @ReactMethod
    public void downloadRingtone(String url, ReadableMap headers, ReadableMap config, Callback onDone) {
        try {
            this.onDone= onDone;
            DownloadManager.Request request = downloader.createRequest(url, headers, config);
            long downloadId = downloader.queueDownload(request);
            list.add(downloadId);
        } catch (Exception e) {
            onDone.invoke(e.getMessage(), null);
        }
    }

    private void sendEvent(String eventName, @Nullable Object params) {
        getReactApplicationContext().getJSModule(RCTNativeAppEventEmitter.class).emit(eventName, params);
    }

    private static IntentFilter alarmNotifIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AlarmScheduler.ACTION_ALARM_STOPPED_NOTIFICATION);
        return intentFilter;
    }

    private static IntentFilter alarmOnNotifIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AlarmScheduler.ACTION_ALARM_ON_NOTIFICATION);
        return intentFilter;
    }

    private final BroadcastReceiver alarmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sendEvent("AlarmNotificationListener", intent.getExtras() != null ? 
            intent.getExtras().getBoolean("rbs_enabled") : false);
        }
    };

    private final BroadcastReceiver alarmOnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sendEvent("AlarmStatusListener", intent.getExtras().getBoolean("rbs_enabled"));
        }
    };

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            list.remove(downloadId);
            if (list.isEmpty())
            {
                if(onDone!=null) {
                    onDone.invoke("SUCCESS");
                    list.clear();
                }
            }
        }
    };

}
