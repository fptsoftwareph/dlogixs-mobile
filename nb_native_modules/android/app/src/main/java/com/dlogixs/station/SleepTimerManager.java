package com.dlogixs.station;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SleepTimerManager {
    // Debugging
    private static final String TAG = "SleepTimerManager";

    private static final int mInterval = 1500; // in milliseconds
    private static SleepTimerManager sharedInstance;
    private SleepTimerListener mListener;
    private Handler mTimeHandler = null;
    private int mPacketLen, mIntervalCtr;
    private List<SleepInfo> sleepInfoData = new ArrayList<>();
    private List<Integer> brainwaveInfo = new ArrayList<>();
    private Runnable msgQueue = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "sleep123 bufferUpdated: " + brainwaveInfo.size() + " packetLen: " + mPacketLen);
            if (brainwaveInfo.size() > mPacketLen) {
                mPacketLen = brainwaveInfo.size();
            } else {
                mIntervalCtr++;
            }
            if (mIntervalCtr > 0) {
                Log.d(TAG, "sleep123 there have been no packets appended.");
                mListener.handleSleepInfoInterruption(sleepInfoData);
                mIntervalCtr = 0;
                brainwaveInfo.clear();
                mTimeHandler.removeCallbacks(this);
                mTimeHandler = null;
            }
            if (mTimeHandler != null) {
                //I make it as dynamic interval to address the issue of time drift
                mTimeHandler.postDelayed(msgQueue, mInterval);
            }
        }
    };

    private SleepTimerManager(SleepTimerListener listener) {
        mListener = listener;
    }

    public static SleepTimerManager getSharedInstance(SleepTimerListener listener) {
        if (sharedInstance == null) {
            sharedInstance = new SleepTimerManager(listener);
        }
        return sharedInstance;
    }

    public void initTimer() {
        if (mTimeHandler == null) {
            System.out.println("sleep123 !_sleepTimer");
            mPacketLen = mIntervalCtr = 0;
            mTimeHandler = new Handler();
            mTimeHandler.post(msgQueue);
        }
    }

    public void addBrainwaveInfo(int eegChksum) {
        brainwaveInfo.add(eegChksum);
    }

    public void addSleepInfo(SleepInfo sleepInfo) {
        sleepInfoData.add(sleepInfo);
        Log.d(TAG, "sleep123 sleepDataUpdated: " + sleepInfoData.size());
    }

    public interface SleepTimerListener {
        void handleSleepInfoInterruption(List<SleepInfo> sleepData);
    }

}
