package com.dlogixs.analysis.sleep;

public class EEGAnalysis {
	
	private EEGData eegData;
	private static int MAF_ORDER = 30;
	private static double QUANTIZATION_LEVEL = 0.05;
	private int timer_min = 0;
	private long[] time_acq;
	private long[] time_acq_5min;
	private double[] signal_min;
	private double[] signal_maf;
	private double[] signal_aqu;
	private int[] signal_valley;
	private double[] signal_height;
	private int[] signal_poor;
	private int[] signal_rem;
	private int[] signal_deep;
	private int[] signal_wake;
	private int[] signal_off;
	private int[] sleep_stages;
	private int[] hypnogram;
	private int L;
	
	public EEGAnalysis(EEGData eegData){
		super();
		this.eegData = eegData;
		this.timer_min = eegData.getDurationInMin();
		time_acq = eegData.getTimeAcq();
		signal_min = eegData.getSignalMin();
		signal_maf = new double[1440];
		signal_aqu = new double[1440];
		signal_valley = new int[1440];
		signal_height = new double[1440];
		signal_poor = eegData.getSignalPoor();
		signal_rem = new int[1440];
		signal_deep = new int[1440];
		signal_wake = new int[1440];
		signal_off = new int[1440];
		sleep_stages = new int[1440];
		hypnogram = new int[288];
		L = 0;
		
		signal_maf = maf_filter(signal_min);
		signal_aqu = adaptive_quantize(signal_maf);
		signal_valley = detect_valley(signal_aqu);
		signal_height = calculate_height(signal_aqu, signal_valley);
		signal_rem = detect_rem(signal_height);
		signal_rem = calibrate_signal(signal_rem, 10);
		signal_deep = detect_deep(signal_height);
		signal_deep = calibrate_signal(signal_deep, 10);
		signal_wake = detect_wake(signal_min);
		signal_wake = calibrate_signal(signal_wake, 3);
		signal_off = detect_off(signal_min);
		signal_off = calibrate_signal(signal_off, 3);
		sleep_stages = integrate_stage(signal_rem, signal_deep, signal_wake, signal_off);
		L = post_process(sleep_stages, signal_poor, hypnogram);
		time_acq_5min = compress_time(time_acq);		
	}

	private double[] maf_filter(double[] x){
		int i = 0;
		int j = 0;
		double[] y = new double[1440];
		
		for(i = 0; i < MAF_ORDER; i++){
			for(j = 0; j < (i+1); j++){
				y[i] += x[j];
			}
			y[i] /= (i+1);
		}
		for(i = MAF_ORDER; i < timer_min; i++){
			for(j = 0; j < MAF_ORDER; j++){
				y[i] += x[i-j];
			}
			y[i] /= MAF_ORDER;
		}
		return y;
	}

	private double[] adaptive_quantize(double x[]){
		int i = 0;
		double pre_value = x[0];
		double[] y = new double[1440];
		for(i = 0; i < timer_min; i++){
			if(Math.abs(x[i] - pre_value) > QUANTIZATION_LEVEL){
				pre_value = x[i];
			}
			y[i] = pre_value;
		}
		return y;
	}

	private int[] detect_valley(double x[]){
		int i = 0;
		int state = 1; // 0: Decrease, 1: Increase
		int[] y = new int[1440];
		for(i = 1; i < timer_min; i++){
			if( (x[i-1] < x[i]) && (state == 0) ){
				state = 1;
				y[i-1] = 1;
			}
			if( (x[i-1] > x[i]) && (state == 1) ){
				state = 0;
			}
		}
		return y;
	}

	private double[] calculate_height(double x[], int y[]){
		int i = 0;
		double pre_value = 0.0;
		double tendency = 0.0;
		double[] z = new double[1440];
		for(i = 0; i < timer_min; i++){
			tendency += x[i];
		}
		tendency /= timer_min;
		pre_value = tendency;
		for(i = 0; i < timer_min; i++){
			if(y[i] != 0){
				pre_value = x[i];
			}
			z[i] = x[i] - pre_value;
		}
		return z;
	}

	private int[] detect_rem(double x[]){
		int i = 0;
		int[] y = new int[1440];
		for(i = 0; i < timer_min; i++){
			y[i] = (x[i] > 0.15) ? 1 : 0;
		}
		return y;
	}

	private int[] detect_deep(double x[]){
		int i = 0;
		int[] y = new int[1440];
		for(i = 0; i < timer_min; i++){
			y[i] = (x[i] < -0.1) ? 1 : 0;
		}
		return y;
	}

	private int[] morphology_dilation(int x[], int window_size){
		int i = 0;
		int j = 0;
		int[] y = new int[1440];
		for(i = 0; i < timer_min; i++){
			y[i] = x[i];
		}
		for(i = 0; i < timer_min-window_size; i++){
			if(x[i] == 0)
				continue;
			for(j = i; j < i+window_size; j++){
				y[j] = 1;
			}
		}
		for(i = timer_min-window_size; i < timer_min; i++){
			if(x[i] == 0)
				continue;
			for(j = i; j < timer_min; j++){
				y[j] = 1;
			}
		}
		return y;
	}

	private int[] morphology_erosion(int x[], int window_size){
		int i = 0;
		int j = 0;
		int[] y = new int[1440];
		for(i = 0; i < timer_min; i++){
			y[i] = x[i];
		}
		for(i = 0; i < window_size; i++){
			if(x[i] == 1)
				continue;
			for(j = 0; j < i; j++){
				y[j] = 0;
			}
		}
		for(i = window_size; i < timer_min; i++){
			if(x[i] == 1)
				continue;
			for(j = i-window_size; j < i; j++){
				y[j] = 0;
			}
		}
		return y;
	}

	private int[] morphology_close(int x[], int window_size){
		int i = 0;
		int[] tmp = new int[1440];
		int[] y = new int[1440];
		for(i = 0; i < 1440; i++){
			tmp[i] = 0;
		}
		tmp = morphology_dilation(x, window_size);
		y = morphology_erosion(tmp, window_size);
		return y;
	}

	private int[] morphology_open(int x[], int window_size){
		int i = 0;
		int[] tmp = new int[1440];
		int[] y = new int[1440];
		for(i = 0; i < 1440; i++){
			tmp[i] = 0;
		}
		tmp = morphology_erosion(x, window_size);
		y = morphology_dilation(tmp, window_size);
		return y;
	}

	private int[] calibrate_signal(int x[], int window_size){
		int i = 0;
		int[] tmp = new int[1440];
		int[] y = new int[1440];
		for(i = 0; i < 1440; i++){
			tmp[i] = 0;
		}
		tmp = morphology_close(x, window_size);
		y = morphology_open(tmp, window_size);
		return y;
	}

	private int[] detect_wake(double x[]){
		int i = 0;
		int[] y = new int[1440];
		for(i = 0; i < timer_min; i++){
			if(x[i] > 1.0){
				y[i] = 1;
			}
		}
		return y;
	}

	private int[] detect_off(double x[]){
		int i = 0;
		int[] y = new int[1440];
		for(i = 0; i < timer_min; i++){
			if(x[i] > 2.0){
				y[i] = 1;
			}
		}
		return y;
	}

	private int[] integrate_stage(int x[], int y[], int z[], int w[]){
		int i = 0;
		int sidx = (int)((MAF_ORDER+1.0)/2.0);
		int[] s = new int[1440];
		for(i = 0; i < timer_min; i++){
			s[i] = 2;
		}
		for(i = 0; i < timer_min; i++){
			if(x[i] == 1){
				s[i] = 3;
			}
			else if(y[i] == 1){
				s[i] = 1;
			}
		}
		sidx = (int)((MAF_ORDER+1.0)/2.0);
		for(i = sidx; i < timer_min; i++){
			s[i-sidx] = s[i];
		}

		for(i = timer_min-sidx; i < timer_min; i++){
			s[i] = s[timer_min-1];
		}
		
		for(i = 0; i < timer_min; i++){
			if(z[i] == 1){
				s[i] = 4;
			}
			if(w[i] == 1){
				s[i] = 5;
			}
		}
		return s;
	}

	private long[] compress_time(long[] time){
		int i = 0;
		long[] time_5min = new long[288];
		for(i = 0; i < timer_min+5; i += 5){
			time_5min[i/5] = time[i];
		}
		return time_5min;
	}
	
	private int post_process(int x[], int y[], int z[]){
		int i = 0;
		int j = 0;
		int L = (((int)(timer_min/5))+1);
		int[] signal_tmp1 = new int[288];
		int[] signal_tmp2 = new int[288];
		int[] vote = new int[5];
		int max_val = 0;
		int max_idx = 4;
		int t = 0;

		for(i = timer_min; i < 1440; i++){
			x[i] = 0;
		}
		for(i = 0; i < timer_min; i++){
			if(y[i] >= 26){
				x[i] = 5;
			}
		}

		// 0. 5�� ������ ����(�쵵 ���)
		for(i = 0; i < timer_min+5; i += 5){
			for(j = 0; j < 5; j++){
				vote[j] = 0;
			}
			for(j = 0; j < 5; j++){
				if(x[i+j] == 0) break;
				vote[x[i+j]-1] = vote[x[i+j]-1] + 1;
			}
			max_val = -1;
			max_idx = 4;
			for(j = 0; j < 5; j++){
				if(vote[j] >= max_val){
					max_val = vote[j];
					max_idx = j;
				}
			}
			signal_tmp1[(int)(i/5)] = max_idx + 1;
		}
		// 1. Wake ���� REM -> Light
		for(i = 0; i < L; i++){ signal_tmp2[i] = signal_tmp1[i]; }
		for(i = 1; i < L; i++){
			if((signal_tmp1[i-1] >= 4) && (signal_tmp1[i] == 3)){
				for(j = i; j < L; j++){
					if(signal_tmp1[j] != 3) break;
					signal_tmp2[j] = 2;
				}
			}
		}
		// 2. 50�� �ʰ� REM -> Light
		for(i = 0; i < L; i++){ signal_tmp1[i] = signal_tmp2[i]; }
		for(i = 1; i < L; i++){
			if(signal_tmp2[i] != 3) continue;
			t = 0;
			for(j = i; j < L; j++){
				if(signal_tmp2[j] == 3)
					t++;
				else
					break;
				if(t > 10)
					signal_tmp1[j] = 2;
			}
		}
		// 3. 240�� ���� Deep -> Light
		for(i = 0; i < L; i++){ signal_tmp2[i] = signal_tmp1[i]; }
		if(L > 48){
			for(i = 48; i < L; i++){
				if(signal_tmp1[i] != 1) continue;
				for(j = i; j > -1; j--){
					if(signal_tmp1[j] != 1) break;
					signal_tmp2[j] = 2;
				}
				for(j = i; j < L; j++){
					if(signal_tmp1[j] != 1) break;
					signal_tmp2[j] = 2;
				}
			}
		}
		// 4. 50�� ���� REM -> Wake
		for(i = 0; i < L; i++){ signal_tmp1[i] = signal_tmp2[i]; }
		if(L > 9){
			for(i = 0; i < 10; i++){
				if(signal_tmp2[i] != 3) continue;
				for(j = i; j > -1; j--){
					if(signal_tmp2[j] != 3) break;
					signal_tmp1[j] = 2;
				}
				for(j = i; j < L; j++){
					if(signal_tmp2[j] != 3) break;
					signal_tmp1[j] = 2;
				}
			}
		}
	    // 5. ���� Deep/Light -> Wake 5min Light 10min
		for(i = 0; i < L; i++){ signal_tmp2[i] = signal_tmp1[i]; }
		if(L > 3){
			if(signal_tmp1[0] < 3){
				signal_tmp2[0] = 4;
				signal_tmp2[1] = 2;
				signal_tmp2[2] = 2;
			}
		}
		// 6. REM/Wake ���� -> Light 5min
		for(i = 0; i < L; i++){ signal_tmp1[i] = signal_tmp2[i]; }
		for(i = 1; i < L; i++){
			if((signal_tmp2[i-1] == 5) && (signal_tmp2[i] == 1))
				signal_tmp1[i] = 2;
			if((signal_tmp2[i-1] == 4) && (signal_tmp2[i] == 1))
				signal_tmp1[i] = 2;
		}
		// 7. Light -> Light 1 and 2
		for(i = 0; i < L; i++){ signal_tmp2[i] = signal_tmp1[i]+1; }
		for(i = 1; i < L; i++){
			if(signal_tmp2[i] == 2) signal_tmp2[i] = 1;
			if(signal_tmp2[i] == 3) signal_tmp2[i] = 2;
			if((signal_tmp2[i-1] == 5) && (signal_tmp2[i] == 2))
				signal_tmp2[i] = 3;
			if((signal_tmp2[i-1] == 4) && (signal_tmp2[i] == 2))
				signal_tmp2[i] = 3;
		}

		
		for(i = 0; i < L; i++){ z[i] = signal_tmp2[i]; }
		
		return L;

	}

	public int[] getHypnogram() {
		return hypnogram;
	}

	public int getLength() {
		return L;
	}
	
	public long[] getTime() {
		return time_acq_5min;
	}
	
	
}
