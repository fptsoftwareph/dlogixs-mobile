package com.dlogixs.analysis.curation;

public class CurationCreator {
	
	private ElementData model;
	private ElementData data;
	private int element_index;
	private int difference;
	private int direction;
	private CurationMessage message;
	private int elementOfModel;
	private int elementOfData;
	private int binauralBeatFrequence;



	public CurationCreator(int element_index, ElementData model, ElementData data) {
		super();
		this.model = model;
		this.data = data;
		this.element_index = element_index;
		message = getGuideMessagebyIndex(element_index, model, data);
	}

	private CurationMessage getGuideMessagebyIndex(int element_index, ElementData model, ElementData data){
		switch(element_index){
		case ElementData.TST: return getTSTGuideMessage(model, data);
		case ElementData.GTBT: return getGTBTGuideMessage(model, data);
		case ElementData.SUN: return getSUNGuideMessage(model, data);
		case ElementData.WALK: return getWALKGuideMessage(model, data);
		case ElementData.COFFEE: return getCOFFEEGuideMessage(model, data);
		case ElementData.SMOKE: return getSMOKEGuideMessage(model, data);
		case ElementData.DRINK: return getDRINKGuideMessage(model, data);
		case ElementData.BB: return getBBGuideMessage(model, data);
		}
		return null;
	}

	private CurationMessage getTSTGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementTST();
		elementOfData = data.getElementTST();
		difference = Math.abs(data.getElementTST() - model.getElementTST());		
				
		if(!data.equal(model)){
			int diff = Math.abs(data.getElementTST() - model.getElementTST());
			if(data.getElementTST() < model.getElementTST()){
				direction = 1;
				//return "You have lack of sleep. " + diff + " minute/s more sleep would be better.";
				return new CurationMessage("You have lack of sleep. " + diff + " minute/s more sleep would be better.",
				diff, direction, 1);
			}
			else if(data.getElementTST() > model.getElementTST()){
				direction = -1;
				//return "You've slept more than the usual number of hours." + diff + " minute/s less sleep would be better.";
				return new CurationMessage("You've slept more than the usual number of hours." + diff + " minute/s less sleep would be better.",
						diff, direction, 2);
			}
			else{
				direction = 0;
				//return "It would be better to maintain a similar number of hours of sleep as before.";
				return new CurationMessage("It would be better to maintain a similar number of hours of sleep as before.",
						diff, direction, 3);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "How about trying to spend less time sleeping than yesterday to find more appropriate sleep time?";
				return new CurationMessage("How about trying to spend less time sleeping than yesterday to find more appropriate sleep time?",
						0, direction, 4);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 1;
				//return "How about trying to spend more time sleeping than yesterday to find more appropriate sleep time?";
				return new CurationMessage("How about trying to spend more time sleeping than yesterday to find more appropriate sleep time?",
						0, direction, 5);
			}
			else {
				direction = 0;
				//return "It would be better to maintain a similar level of sleep as yesterday.";
				return new CurationMessage("It would be better to maintain a similar level of sleep as yesterday.",
						0, direction, 6);
			}
		}
	}

	private CurationMessage getGTBTGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementGTBT();
		elementOfData = data.getElementGTBT();
		difference = Math.abs(data.getElementGTBT() - model.getElementGTBT());
		
		if(!data.equal(model)){
			int diff = Math.abs(data.getElementGTBT() - model.getElementGTBT());
			if(data.getElementGTBT() < model.getElementGTBT()){
				direction = 1;
				//return "Sleep latency is somewhat early. " + diff + " minute/s later in going to bed would be better. ";
				return new CurationMessage("Sleep latency is somewhat early. " + diff + " minute/s later in going to bed would be better. ",
						diff, direction, 1);
			}
			else if(data.getElementGTBT() > model.getElementGTBT()){
				direction = -1;
				//return "Sleep latency is somewhat late." + diff + " minute/s earlier in going to bed would be better. ";
				return new CurationMessage("Sleep latency is somewhat late." + diff + " minute/s earlier in going to bed would be better. ",
						diff, direction, 2);
			}
			else{
				direction = 0;
				//return "It would be better if you keep the same level of sleep latency as yesterday.";
				return new CurationMessage("It would be better if you keep the same level of sleep latency as yesterday.",
						diff, direction, 3);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "How about trying to go to bed a little earlier than yesterday to find more appropriate sleep latency?";
				return new CurationMessage("How about trying to go to bed a little earlier than yesterday to find more appropriate sleep latency?",
						0, direction, 4);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 1;
				//return "How about trying to go to bed a little later than yesterday to find more appropriate sleep latency?";
				return new CurationMessage("How about trying to go to bed a little later than yesterday to find more appropriate sleep latency?",
						0, direction, 5);
			}
			else {
				direction = 0;
				//return "It would be better if you keep the same level of sleep latency as yesterday.";
				return new CurationMessage("It would be better if you keep the same level of sleep latency as yesterday.",
						0, direction, 6);
			}
		}
	}

	private CurationMessage getSUNGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementSUN();
		elementOfData = data.getElementSUN();
		difference = Math.abs(data.getElementSUN() - model.getElementSUN());
		
		if(!data.equal(model)){
			int diff = Math.abs(data.getElementSUN() - model.getElementSUN());
			if(data.getElementSUN() < model.getElementSUN()){
				direction = 1;
				//return "Increasing " + diff + " minute/s or more of outdoor activities during day time would be better.";
				return new CurationMessage("Increasing " + diff + " minute/s or more of outdoor activities during day time would be better.",
						diff, direction, 1);
			}
			else if(data.getElementSUN() > model.getElementSUN()){
				direction = -1;
				//return "Minimizing " + diff + " minute/s or less of outdoor activities during day time would be better.";
				return new CurationMessage("Minimizing " + diff + " minute/s or less of outdoor activities during day time would be better.",
						diff, direction, 2);
			}
			else{
				direction = 0;
				//return "It would be better to keep time outdoors at the daytime, similar to yesterday.";
				return new CurationMessage("It would be better to keep time outdoors at the daytime, similar to yesterday.",
						diff, direction, 3);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "How about trying to reduce outdoor activities because too much of the sun's warm rays can be harmful to your skin.";
				return new CurationMessage("How about trying to reduce outdoor activities because too much of the sun's warm rays can be harmful to your skin.",
						0, direction, 4);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 1;
				//return "How about trying to increase outdoor activities because without enough sunlight exposure, your serotonin levels can dip low.";
				return new CurationMessage("How about trying to increase outdoor activities because without enough sunlight exposure, your serotonin levels can dip low.",
						0, direction, 5);
			}
			else {
				direction = 0;
				//return "Keep the same level of outdoor activity time the same as before. The right balance of sun exposure can have lots of mood lifting benefits.";
				return new CurationMessage("Keep the same level of outdoor activity time the same as before. The right balance of sun exposure can have lots of mood lifting benefits.",
						0, direction, 6);
			}
		}
	}

	private CurationMessage getWALKGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementWALK();
		elementOfData = data.getElementWALK();
		difference = Math.abs(data.getElementWALK() - model.getElementWALK());
		
		if(!data.equal(model)){
			int diff = Math.abs(data.getElementWALK() - model.getElementWALK());
			if(data.getElementWALK() < model.getElementWALK()){
				direction = 1;
				//return "Your activity is not enough. " + diff + " footsteps or more would be better.";
				return new CurationMessage("Your activity is not enough. " + diff + " footsteps or more would be better.",
						diff, direction, 1);
			}
			else if(data.getElementWALK() > model.getElementWALK()){
				direction = -1;
				//return "You've had a lot of activities today. " + diff + " footsteps or less would be better.";
				return new CurationMessage("You've had a lot of activities today. " + diff + " footsteps or less would be better.",
						diff, direction, 2);
			}
			else{
				direction = 0;
				//return "It would be better if you keep the same level of activity as yesterday.";
				return new CurationMessage("It would be better if you keep the same level of activity as yesterday.",
						diff, direction, 3);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "How about trying to reduce your activities than yesterday so you can balance your activity and rest time?";
				return new CurationMessage("How about trying to reduce your activities than yesterday so you can balance your activity and rest time?",
						0, direction, 4);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 1;
				//return "How about trying to increase your activities than yesterday so you can balance your activity and rest time?";
				return new CurationMessage("How about trying to increase your activities than yesterday so you can balance your activity and rest time?",
						0, direction, 5);
			}
			else {
				direction = 0;
				//return "It would be better if you keep the same level of activity as yesterday.";
				return new CurationMessage("It would be better if you keep the same level of activity as yesterday.",
						0, direction, 6);
			}
		}
	}

	private CurationMessage getCOFFEEGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementCOFFEE();
		elementOfData = data.getElementCOFFEE();
		difference = Math.abs(data.getElementCOFFEE() - model.getElementCOFFEE());
		
		if(!data.equal(model)){
			int diff = Math.abs(data.getElementCOFFEE() - model.getElementCOFFEE());
			if(data.getElementCOFFEE() < model.getElementCOFFEE()){
				direction = 0;
				//return "Awesome! You reach your goal for today.";
				return new CurationMessage("Awesome! You reach your goal for today.",
						diff, direction, 1);
			}
			else if(data.getElementCOFFEE() > model.getElementCOFFEE()){
				direction = -1;
				//return "Drinking, " + diff + " cups of coffee would be better.";
				return new CurationMessage("Minimizing, " + diff + " cup/s of coffee would be better.",
						diff, direction, 2);
			}
			else{
				direction = 0;
				//return "It would be better if you consume the same number of cups of coffee as yesterday.";
				return new CurationMessage("It is okay if you consume the same number of cups of coffee as yesterday.",
						diff, direction, 3);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "How about trying to reduce coffee intake? Too much caffeine consumption have harmful effects.";
				return new CurationMessage("How about trying to reduce coffee intake? Too much caffeine consumption have harmful effects.",
						0, direction, 4);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 0;
				//return "Awesome! You reach your goal for today.";
				return new CurationMessage("Awesome! You reach your goal for today.",
						0, direction, 5);
			}
			else {
				direction = 0;
				//return "It would be better if you keep the same level of coffee consumption as yesterday.";
				return new CurationMessage("It would be better if you keep the same level of coffee consumption as yesterday.",
						0, direction, 6);
			}
		}
	}

	private CurationMessage getSMOKEGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementSMOKE();
		elementOfData = data.getElementSMOKE();
		difference = Math.abs(data.getElementSMOKE() - model.getElementSMOKE());
		
		if(!data.equal(model)){
			int diff = Math.abs(data.getElementSMOKE() - model.getElementSMOKE());
			if(data.getElementSMOKE() < model.getElementSMOKE()){
				direction = 0;
				//return "Awesome! You reach your goal for today.";
				return new CurationMessage("Awesome! You reach your goal for today.",
						diff, direction, 1);
			}
			else if(data.getElementSMOKE() > model.getElementSMOKE()){
				direction = -1;
				//return "You've smoked more cigarettes today. " + diff + " sticks or less would be better.";
				return new CurationMessage("You've smoked more cigarettes today. " + diff + " sticks or less would be better.",
						diff, direction, 2);
			}
			else{
				direction = 0;
				//return "It would be better if you smoke the same number of cigarettes as before.";
				return new CurationMessage("It would be better if you smoke the same number of cigarettes as before.",
						diff, direction, 3);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "Smoking less than before will help you reach your goal";
				return new CurationMessage("Smoking less than before will help you reach your goal",
						0, direction, 4);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 0;
				//return "Awesome! You reach your goal for today.";
				return new CurationMessage("Awesome! You reach your goal for today.",
						0, direction, 5);
			}
			else {
				direction = 0;
				//return "It would be better if you smoke the same number of cigarettes as before.";
				return new CurationMessage("It would be better if you smoke the same number of cigarettes as before.",
						0, direction, 6);
			}
		}
	}

	private CurationMessage getDRINKGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementDRINK();
		elementOfData = data.getElementDRINK();
		difference = Math.abs(data.getElementDRINK() - model.getElementDRINK());
		
		if(!data.equal(model)){
			int diff = Math.abs(data.getElementDRINK() - model.getElementDRINK());
			if(data.getElementDRINK() < model.getElementDRINK()){
				direction = 1;
				//return "Awesome! You reach your goal for today.";
				return new CurationMessage("Awesome! You reach your goal for today.",
						diff, direction, 1);
			}
			else if(data.getElementDRINK() > model.getElementDRINK()){
				direction = -1;
				//return "You've consumed more alcohol today. " + diff + " cups or less would be better.";
				return new CurationMessage("You've consumed more alcohol today. " + diff + " cups or less would be better.",
						diff, direction, 2);
			}
			else{
				direction = 0;
				//return "It would be better if you keep your alcohol intake the same as before.";
				return new CurationMessage("It would be better if you keep your alcohol intake the same as before.",
						diff, direction, 3);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "How about if you try to reduce your alcohol intake as compared before to moderate your drinking habit?";
				return new CurationMessage("How about if you try to reduce your alcohol intake as compared before to moderate your drinking habit?",
						0, direction, 4);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 1;
				//return "Awesome! You reach your goal for today.";
				return new CurationMessage("Awesome! You reach your goal for today.",
						0, direction, 5);
			}
			else {
				direction = 0;
				//return "It would be better if you keep your alcohol intake the same as before.";
				return new CurationMessage("It would be better if you keep your alcohol intake the same as before.",
						0, direction, 6);
			}
		}
	}

	private CurationMessage getBBGuideMessage(ElementData model, ElementData data){
		elementOfModel = model.getElementBB();
		elementOfData = data.getElementBB();
		difference = Math.abs(data.getElementBB() - model.getElementBB());
		binauralBeatFrequence = (model.getIndexOfBB() + 1) * 3;
		
		int modelElement = model.getElementBB();
		int datalElement = data.getElementBB();
		int modelFreq = (model.getIndexOfBB() + 1) * 3;
		
		if(!data.equal(model)){
			int diff = Math.abs(datalElement - modelElement);
			if(datalElement < modelElement){
				direction = 1;
				//return "You've listened binaural beat with " + modelFreq + "Hz. " + diff + " minute/s more listening time would be better.";
				/*return new CurationMessage("You've listened binaural beat with " + modelFreq + "Hz. " + diff + " minute/s more listening time would be better.",
						diff, direction, 1, modelFreq);*/
					return new CurationMessage(diff + " minute/s more listening time would be better.",
						diff, direction, 1, modelFreq);
			}
			else if(datalElement > modelElement){
				direction = -1;
				//return "You've listened binaural beat with " + modelFreq + "Hz. " + diff + " minute/s less listening time would be better.";
				/*return new CurationMessage("You've listened binaural beat with " + modelFreq + "Hz. " + diff + " minute/s less listening time would be better.",
						diff, direction, 2, modelFreq);*/
					return new CurationMessage(diff + " minute/s less listening time would be better.",
						diff, direction, 2, modelFreq);	
			}
			else{
				direction = 0;
				//return "You've listened binaural beat with " + modelFreq + "Hz same as yesterday. It would be better if you keep it up.";
				/*return new CurationMessage("You've listened binaural beat with " + modelFreq + "Hz same as yesterday. It would be better if you keep it up.",
						diff, direction, 3, modelFreq);*/
					return new CurationMessage("It would be better if you keep it up.",
						diff, direction, 3, modelFreq);
			}
		}
		else{
			if(data.getIndexOfPosition() < 0){
				direction = -1;
				//return "How about you try to reduce listening time more than yesterday to find more appropriate binaural beats hearing time?";
				return new CurationMessage("How about you try to reduce listening time more than yesterday to find more appropriate binaural beats hearing time?",
						0, direction, 4, 0);
			}
			else if(data.getIndexOfPosition() > 0){
				direction = 1;
				//return "How about you try to increase listening time more than yesterday to find more appropriate binaural beats hearing time?";
				return new CurationMessage("How about you try to increase listening time more than yesterday to find more appropriate binaural beats hearing time?",
						0, direction, 5, 0);
			}
			else {
				direction = 0;
				//return "You've listened" + modelFreq + "binaural beats Hz same as yesterday. It would be better if you keep it up.";
				/*return new CurationMessage("You've listened" + modelFreq + "binaural beats Hz same as yesterday. It would be better if you keep it up.",
						0, direction, 6, modelFreq);*/
					return new CurationMessage("It would be better if you keep it up.",
						0, direction, 6, modelFreq);
			}
		}
	}

	public ElementData getModel() {
		return model;
	}

	public ElementData getData() {
		return data;
	}

	public int getDifference() {
		return difference;
	}

	public int getDirection() {
		return direction;
	}

	public CurationMessage getMessage() {
		return message;
	}

	public int getElementOfModel() {
		return elementOfModel;
	}

	public int getElementOfData() {
		return elementOfData;
	}

	public int getBinauralBeatFrequence() {
		return binauralBeatFrequence;
	}
	
	
}
