package com.dlogixs.analysis.curation;

public class CurationMessage {

    private String english;
    private int diff, direction, counter, modelFreq;

    public CurationMessage(String english, int diff, int direction, int counter) {
        this.english = english;
        this.diff = diff;
        this.direction = direction;
        this.counter = counter;
    }

    public CurationMessage(String english, int diff, int direction, int counter, int modelFreq) {
        this.english = english;
        this.diff = diff;
        this.direction = direction;
        this.counter = counter;
        this.modelFreq = modelFreq;
    }

    public CurationMessage(int diff, int counter) {
        this.diff = diff;
        this.counter = counter;
    }

    public CurationMessage(int diff, int counter, int modelFreq) {
        this.diff = diff;
        this.counter = counter;
        this.modelFreq = modelFreq;
    }

    public String getEnglish() {
        return english;
    }

    public int getDiff() {
        return diff;
    }

    public int getDirection() {
        return direction;
    }

    public int getCounter() {
        return counter;
    }

    public int getModelFreq() {
        return modelFreq;
    }

    @Override
    public String toString() {
        return "CurationMessage{" +
                "english='" + english + '\'' +
                ", diff=" + diff +
                ", direction=" + direction +
                ", counter=" + counter +
                '}';
    }
}
