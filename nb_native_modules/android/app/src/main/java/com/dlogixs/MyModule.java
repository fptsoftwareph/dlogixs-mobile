package com.dlogixs;

import android.widget.Toast;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.util.Set;
import java.lang.Class;
import java.lang.reflect.Method;

import android.util.Log;

import android.content.Intent;

public class MyModule extends ReactContextBaseJavaModule {

    private BluetoothAdapter bluetoothAdapter;
    private ReactContext reactContext;

    public MyModule(ReactApplicationContext reactContext) {
        super(reactContext);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "MyModule";
    }

    @ReactMethod
    public void alert(String message) {
        Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @ReactMethod
    public void unpairDevice(String deviceId, Callback callback) {
        if (bluetoothAdapter == null) {
            callback.invoke("Bluetooth NOT supported");
        } else {
            if (bluetoothAdapter.isEnabled()){
                
                Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
                
                try {
                    Class<?> btDeviceInstance =  Class.forName(BluetoothDevice.class.getCanonicalName());
                    Method removeBondMethod = btDeviceInstance.getMethod("removeBond");
                    boolean cleared = false;
                            
                    for (BluetoothDevice bluetoothDevice : bondedDevices) {
                       
                        String mac = bluetoothDevice.getAddress();
                        
                        if(mac.equals(deviceId)) {
                            removeBondMethod.invoke(bluetoothDevice);
                            Log.i("unpairDevice","Cleared Pairing");
                            cleared = true;
                            callback.invoke("Device successfully unpaired");
                            break;
                        }
                    }
            
                    if(!cleared) {
                        Log.i("unpairDevice","Not Paired");
                        callback.invoke("Not Paired");
                    }
                } catch (Throwable th) {
                    Log.e("unpairDevice", "Error pairing", th);
                    callback.invoke("Error pairing");
                }
            } else {
                callback.invoke("Bluetooth is NOT Enabled!");
            }
        }
    }

    @ReactMethod
    public void disableBluetooth(Callback callback) { 
        if (bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
            callback.invoke("disabled"); 
        }  
    }

    @ReactMethod
    public void openSettings() {
        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        reactContext.startActivity(intent);
    }
}
