package com.dlogixs.station;

/**
 * Created by fptsoftware on 20/02/2018.
 */

public class EEGStreamParser {
    public static final byte PARSER_TYPE_PACKETS  = 0x01;
    public static final byte PARSER_TYPE_2BYTERAW = 0x02;

    /* Member (state) variables */
    private int type, state, lastByte, payloadLength, payloadBytesReceived, payloadSum, chksum;
    private byte[] payload = new byte[256];
    private StreamParserListener listener = null;
    private Object customData;

    /* Parser CODE definitions */
    public static final int PARSER_CODE_BATTERY     = 0x01;
    public static final int PARSER_CODE_POOR_SIGNAL = 0x02;
    public static final int PARSER_CODE_ATTENTION   = 0x04;
    public static final int PARSER_CODE_MEDITATION  = 0x05;
    public static final int PARSER_CODE_8BIT_RAW    = 0x06;
    public static final int PARSER_CODE_RAW_MARKER  = 0x07;

    public static final int PARSER_CODE_RAW_SIGNAL   = 0x80;
    public static final int PARSER_CODE_EEG_POWERS   = 0x81;
    public static final int PARSER_CODE_ASIC_EEG_POWERS  = 0x83;

    /* Decoder states (Packet decoding) */
    private static final int PARSER_STATE_SYNC           = 0x01;
    private static final int PARSER_STATE_SYNC_CHECK     = 0x02;
    private static final int PARSER_STATE_PAYLOAD_LENGTH = 0x03;
    private static final int PARSER_STATE_PAYLOAD        = 0x04;
    private static final int PARSER_STATE_CHKSUM         = 0x05;

    /* Decoder states (2-byte raw decoding) */
    private static final int PARSER_STATE_WAIT_HIGH      = 0x06;
    private static final int PARSER_STATE_WAIT_LOW       = 0x07;

    /* Other internal constants */
    private static final int PARSER_SYNC_BYTE   = 0xAA;
    private static final int PARSER_EXCODE_BYTE = 0x55;

    public EEGStreamParser(byte parserType, StreamParserListener listener,
                         Object customData) {
        this.type = parserType;
        this.customData = customData;
        this.listener = listener;
        switch( parserType ) {
            case( PARSER_TYPE_PACKETS ):
                this.state = PARSER_STATE_SYNC;
                break;
            case( PARSER_TYPE_2BYTERAW ):
                this.state = PARSER_STATE_WAIT_HIGH;
                break;
            default:
//                System.out.println( "Invalid parserType");
                break;
        }
    }

    public int parseByte(int b) {
        int returnValue = 0;
        switch(state) {
            case PARSER_STATE_SYNC:
                if( b == PARSER_SYNC_BYTE )
                    state = PARSER_STATE_SYNC_CHECK;
                break;
            case PARSER_STATE_SYNC_CHECK:
                state = ((b == PARSER_SYNC_BYTE) ? PARSER_STATE_PAYLOAD_LENGTH : PARSER_STATE_SYNC);
                if( b == PARSER_SYNC_BYTE )
                    state = PARSER_STATE_PAYLOAD_LENGTH;
                else
                    state = PARSER_STATE_SYNC;
                break;

            case PARSER_STATE_PAYLOAD_LENGTH:
                payloadLength = b;
                if (payloadLength > 170) {
                    state = PARSER_STATE_SYNC;
                } else if (payloadLength == 170)
                    returnValue = -3;
                else {
                    payloadBytesReceived = 0;
                    payloadSum = 0;
                    state = PARSER_STATE_PAYLOAD;
                }
                break;
            case PARSER_STATE_PAYLOAD:
                payload[payloadBytesReceived++] = (byte)b;
                payloadSum += b;
                if (payloadBytesReceived >= payloadLength) {
                    state = PARSER_STATE_CHKSUM;
                }
                break;

            case PARSER_STATE_CHKSUM:
                chksum = b;
                state = PARSER_STATE_SYNC;
                if (chksum != ((~payloadSum) & 0xFF))
                    returnValue = -2;
                else {
                    returnValue = 1;
                    parsePacketPayload();
                }
                break;
            case PARSER_STATE_WAIT_HIGH:
                if ((b & 0xC0) == 0x80)
                    state = PARSER_STATE_WAIT_LOW;
                break;
            case PARSER_STATE_WAIT_LOW:
                if ((b & 0xC0) == 0x40) {
                    payload[0] = (byte) lastByte;
                    payload[1] = (byte) b;
                    if (listener != null) {
                        listener.handleEEGDataValue(0, PARSER_CODE_RAW_SIGNAL, 2, payload, customData);
                    }
                }
                state = PARSER_STATE_WAIT_HIGH;
                break;
            default:
                state = PARSER_STATE_SYNC;
                returnValue = -5;
                break;
        }
        lastByte = b;
        return returnValue;
    }

    private void parsePacketPayload() {
        int i = 0;
        int extendedCodeLevel = 0;
        int code;
        int numBytes;
//        System.out.println("parsePacketPayload() payloadLength: " + payloadLength);
        while(i < payloadLength) {
            while(payload[i] == PARSER_EXCODE_BYTE) {
                extendedCodeLevel++;
                i++;
            }
            code = payload[i++] & 0xff;
//            System.out.println(String.format("parsePacketPayload() payload:%02x ", payload[i]));
            numBytes = (code >= 0x80) ? payload[i++] & 0xff : 1;
            byte[] valueBytes = new byte[numBytes];
//            System.out.println("parsePacketPayload() valueBytesLength: " + valueBytes.length);
            for (int j = 0; j < numBytes; j++)
                valueBytes[j] = (byte) (payload[i + j] & 0xFF);
            if( listener != null )
                listener.handleEEGDataValue(extendedCodeLevel, code, numBytes,
                        valueBytes, customData);
            i += numBytes;
        }
    }

    public interface StreamParserListener {
        void handleEEGDataValue( int extendedCodeLevel, int code, int numBytes, byte[] valueBytes, Object customData );
    }
}
