package com.dlogixs;

 public class DataConstants {
  // SLEEP_ANALYSIS
  public static final String SLEEP_ANALYSIS_POINTS = "SLEEP_ANALYSIS_POINTS";
  public static final String SLEEP_ANALYSIS_TOTAL = "SLEEP_ANALYSIS_TOTAL";
  public static final String SLEEP_ANALYSIS_LATENCY = "SLEEP_ANALYSIS_LATENCY";
  public static final String SLEEP_ANALYSIS_WAKE = "SLEEP_ANALYSIS_WAKE";
  public static final String SLEEP_EFFICIENCY = "SLEEP_EFFICIENCY";
  public static final String SLEEP_REM_LATENCY = "SLEEP_REM_LATENCY";
  public static final String SLEEP_REM_EFFICIENCY = "SLEEP_REM_EFFICIENCY";
  public static final String SLEEP_CYCLE_COUNT = "SLEEP_CYCLE_COUNT";
  public static final String SLEEP_SQI_STABILITY = "SLEEP_SQI_STABILITY";
  public static final String SLEEP_SQI_AWAKEN = "SLEEP_SQI_AWAKEN";
  public static final String SLEEP_SQI_CYCLE = "SLEEP_SQI_CYCLE";
  public static final String SLEEP_STABILITY_BRAINWAVE = "SLEEP_STABILITY_BRAINWAVE";
  public static final String SLEEP_HYPNOGRAM = "SLEEP_HYPNOGRAM";
  public static final String SLEEP_GTBT = "SLEEP_GTBT";
  // CURATION_ANALYSIS
  public static final String CURATION_IS_UPDATED = "CURATION_IS_UPDATED";
  public static final String CURATION_MODEL_DATE = "CURATION_MODEL_DATE";
  public static final String CURATION_DATA_DATE = "CURATION_DATA_DATE";
  public static final String CURATION_ELEMENT_MODEL = "CURATION_ELEMENT_MODEL";
  public static final String CURATION_ELEMENT_DATA = "CURATION_ELEMENT_DATA";
  public static final String CURATION_DIFFERENCE = "CURATION_DIFFERENCE";
  public static final String CURATION_DIRECTION = "CURATION_DIRECTION";
  public static final String CURATION_MESSAGE = "CURATION_MESSAGE";
  // REACHABILITY
  public static final String REACHABILITY_MODE = "REACHABILITY_MODE";
  public static final String REACHABILITY_TYPE = "REACHABILITY_TYPE";
}