package com.dlogixs;

import android.bluetooth.BluetoothDevice;
import android.util.Base64;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;

import org.json.JSONException;
/**
 * Created by fptsoftware on 08/11/2017.
 */

/**
 * Peripheral wraps the BluetoothDevice and provides methods to convert to JSON.
 */
public class Peripheral {

    private BluetoothDevice device;
    private byte[] advertisingData;
    private int advertisingRSSI;
    private boolean connected = false;

    public Peripheral(BluetoothDevice device, int advertisingRSSI, byte[] scanRecord) {
        this.device = device;
        this.advertisingRSSI = advertisingRSSI;
        this.advertisingData = scanRecord;
    }

    public Peripheral(BluetoothDevice device) {
        this.device = device;
    }

    public BluetoothDevice getDevice() {
        return device;
    }


    public WritableMap asWritableMap() {
        WritableMap map = Arguments.createMap();
        try {
            map.putString("name", device.getName());
            map.putString("id", device.getAddress()); // mac address
            map.putMap("advertising", byteArrayToWritableMap(advertisingData));
            map.putInt("rssi", advertisingRSSI);
        } catch (Exception e) { // this shouldn't happen
            e.printStackTrace();
        }

        return map;
    }

    static WritableMap byteArrayToWritableMap(byte[] bytes) throws JSONException {
        WritableMap object = Arguments.createMap();
        object.putString("CDVType", "ArrayBuffer");
        object.putString("data", Base64.encodeToString(bytes, Base64.NO_WRAP));
        object.putArray("bytes", SMAModule.bytesToWritableArray(bytes));
        return object;
    }

    public boolean isConnected() {
        return connected;
    }
}
