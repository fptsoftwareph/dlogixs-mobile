package com.dlogixs;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bestmafen.easeblelib.entity.EaseDevice;
import com.bestmafen.easeblelib.scanner.EaseScanner;
import com.bestmafen.easeblelib.scanner.ScanOption;
import com.bestmafen.easeblelib.scanner.ScannerFactory;
import com.bestmafen.easeblelib.scanner.SimpleEaseScanCallback;
import com.bestmafen.easeblelib.util.L;
import com.bestmafen.smablelib.component.SimpleSmaCallback;
import com.bestmafen.smablelib.component.SmaManager;
import com.bestmafen.smablelib.entity.SmaHeartRate;
import com.bestmafen.smablelib.entity.SmaSport;
import com.bestmafen.smablelib.entity.SmaUserInfo;
import com.bestmafen.smablelib.util.SmaBleUtils;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.RCTNativeAppEventEmitter;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

class SMAModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    public static final String LOG_TAG = "logs";
    private static final int ENABLE_REQUEST = 539;

    private BluetoothAdapter bluetoothAdapter;
    private ReactApplicationContext reactContext;
    private Callback enableBluetoothCallback;

    // key is the MAC Address
    public Map<String, Peripheral> peripherals = new LinkedHashMap<>();

    private boolean forceLegacy = false;
    private SmaManager mSmaManager;
    private EaseScanner mScanner;
    private int scanDuration = 0;
    private volatile boolean isFailed;
    private WritableMap sportTemp = Arguments.createMap();
    private WritableMap heartRateTemp = Arguments.createMap();
    private SmaUserInfo userInfo = new SmaUserInfo();

    public SMAModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        SmaManager.getInstance().init(reactContext);
        reactContext.addActivityEventListener(this);
        sportTemp.putDouble("time", 0.0);
        sportTemp.putInt("step", 0);
        sportTemp.putString("date", "");
        sportTemp.putInt("calorie", 0);
        heartRateTemp.putDouble("time", 0.0);
        heartRateTemp.putInt("heartRate", 0);
        heartRateTemp.putString("date", "");
        Log.d(LOG_TAG, "BleManager created");
    }

    @Override
    public String getName() {
        return "SMAModule";
    }

    private BluetoothAdapter getBluetoothAdapter() {
        if (bluetoothAdapter == null) {
            BluetoothManager manager = (BluetoothManager) reactContext.getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = manager.getAdapter();
        }
        return bluetoothAdapter;
    }


    public void sendEvent(String eventName,
                          @Nullable Object params) {
        getReactApplicationContext()
                .getJSModule(RCTNativeAppEventEmitter.class)
                .emit(eventName, params);
    }

    @ReactMethod
    public void start(ReadableMap options, Callback callback) {
        L.d("BleManager -> initDevice");
        if (options.hasKey("forceLegacy")) {
            forceLegacy = options.getBoolean("forceLegacy");
        }
        callback.invoke();
    }

    @ReactMethod
    public void enableBluetooth(Callback callback) {
        if (getBluetoothAdapter() == null) {
            Log.d(LOG_TAG, "No bluetooth support");
            callback.invoke("No bluetooth support");
            return;
        }
        if (!getBluetoothAdapter().isEnabled()) {
            enableBluetoothCallback = callback;
            Intent intentEnable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            if (getCurrentActivity() == null)
                callback.invoke("Current activity not available");
            else
                getCurrentActivity().startActivityForResult(intentEnable, ENABLE_REQUEST);
        } else
            callback.invoke();
    }

    @ReactMethod
    public void scan(ReadableArray serviceUUIDs, final int scanSeconds, boolean allowDuplicates, ReadableMap options, final Callback errorCallback) {
        scanDuration = scanSeconds * 1000;
        mScanner = ScannerFactory.createScanner().setScanOption(new ScanOption().scanPeriod(scanDuration)).setEaseScanCallback(new SimpleEaseScanCallback() {

            @Override
            public void onDeviceFound(EaseDevice device) {
                for (Iterator<Map.Entry<String, Peripheral>> iterator = peripherals.entrySet().iterator(); iterator.hasNext(); ) {
                    Map.Entry<String, Peripheral> entry = iterator.next();
                    if (!entry.getValue().isConnected()) {
                        iterator.remove();
                    }
                }
                if (!peripherals.containsKey(device.device.getAddress())) {
                    Peripheral peripheral = new Peripheral(device.device, device.rssi, device.scanRecord);
                    peripherals.put(device.device.getAddress(), peripheral);
                    WritableMap map = peripheral.asWritableMap();
                    sendEvent("BleManagerDiscoverPeripheral", map);
                }
            }

            @Override
            public void onBluetoothDisabled() {
                peripherals.clear();
                errorCallback.invoke("Please enable Bluetooth");
            }
        });
        mScanner.startScan(!mScanner.isScanning);
    }

    @ReactMethod
    public void connect(String peripheralUUID, final Callback errorCallback, final Callback successCallback) {
        final Peripheral peripheral = retrieveOrCreatePeripheral(peripheralUUID);
        if (peripheral == null) {
            errorCallback.invoke("Invalid peripheral uuid");
        } else {
            successCallback.invoke("Connected!!!");
            isFailed = false;
        }

        System.out.println("123SMAModule-- connect()");
        mSmaManager = SmaManager.getInstance().addSmaCallback(new SimpleSmaCallback() {

            @Override
            public void onDeviceConnected(BluetoothDevice device) {
                if (!isFailed) {
                    System.out.println("123SMAModule-- connect() !isFailed");
                }
            }

            @Override
            public void onLogin(boolean ok) {
                System.out.println("123SMAModule-- onLogin()");
                if (ok) {
                    System.out.println("123SMAModule-- onLogin() -- ok");
                    peripherals.clear();
                    System.out.println("LOGIN DEVICE: " + mSmaManager.getSavedName());
                    fetchSportData();
                    fetchHeartData();
                }
            }
        });
        mSmaManager.bindWithDevice(peripheral.getDevice());
    }

    @ReactMethod
    public void disconnect(Callback callback) {
        SmaManager.getInstance().bind(false);
        callback.invoke();
    }

    @ReactMethod
    public void onReadSport(Float weight, Integer sex, Callback callback, Callback errorCallback) {
        mSmaManager = SmaManager.getInstance();
        userInfo.weight = weight;
        userInfo.gender = sex;
        mSmaManager.write(SmaManager.Cmd.SET, SmaManager.Key.SET_USER_INFO, userInfo);
        fetchSportData();
        try {
            callback.invoke(sportTemp);
        } catch (RuntimeException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    public void fetchSportData() {
        mSmaManager = SmaManager.getInstance().addSmaCallback(new SimpleSmaCallback() {
            @Override
            public void onReadSportData(List<SmaSport> list) {
                WritableMap sportMap = Arguments.createMap();
                SmaSport sport = list.get(list.size() - 1);
                sportMap.putDouble("time", sport.time);
                sportMap.putInt("step", sport.step);
                sportMap.putString("date", sport.date);
                int calories = SmaBleUtils.getCalorie(userInfo.weight, sport.step, userInfo.gender);
                sportMap.putInt("calorie", calories);
                L.d("SMAModule => onReadSport()", String.valueOf(sport));
                sportTemp = sportMap;
            }
        });
        mSmaManager.write(SmaManager.Cmd.DATA, SmaManager.Key.SPORT);
    }

    @ReactMethod
    public void onReadHeartRate(Callback callback, Callback errorCallback) {
        fetchHeartData();
        try {
            callback.invoke(heartRateTemp);
        } catch (RuntimeException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    public void fetchHeartData() {
        mSmaManager = SmaManager.getInstance().addSmaCallback(new SimpleSmaCallback() {
            @Override
            public void onReadHeartRateData(List<SmaHeartRate> list) {
                WritableMap hrMap = Arguments.createMap();
                SmaHeartRate rate = list.get(list.size() - 1);
                hrMap.putDouble("time", rate.time);
                hrMap.putInt("heartRate", rate.value);
                hrMap.putString("date", rate.date);
                heartRateTemp = hrMap;
            }
        });
        mSmaManager.write(SmaManager.Cmd.DATA, SmaManager.Key.RATE);
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        Log.d(LOG_TAG, "onActivityResult");
        if (requestCode == ENABLE_REQUEST && enableBluetoothCallback != null) {
            if (resultCode == RESULT_OK) {
                enableBluetoothCallback.invoke();
            } else {
                enableBluetoothCallback.invoke("User refused to enable");
            }
            enableBluetoothCallback = null;
        }
    }

    @Override
    public void onNewIntent(Intent intent) {

    }

    private Peripheral retrieveOrCreatePeripheral(String peripheralUUID) {
        Peripheral peripheral = peripherals.get(peripheralUUID);
        if (peripheral == null) {
            if (peripheralUUID != null) {
                peripheralUUID = peripheralUUID.toUpperCase();
            }
            if (BluetoothAdapter.checkBluetoothAddress(peripheralUUID)) {
                BluetoothManager manager = (BluetoothManager) reactContext.getSystemService(Context.BLUETOOTH_SERVICE);
                BluetoothAdapter btAdapter = manager.getAdapter();
                BluetoothDevice device = btAdapter.getRemoteDevice(peripheralUUID);
                peripheral = new Peripheral(device);
                peripherals.put(peripheralUUID, peripheral);
            }
        }
        return peripheral;
    }

    public static WritableArray bytesToWritableArray(byte[] bytes) {
        WritableArray value = Arguments.createArray();
        for (int i = 0; i < bytes.length; i++)
            value.pushInt((bytes[i] & 0xFF));
        return value;
    }

}