package com.dlogixs.analysis.sleep;

public class SleepAnalysis {

	private EEGData eegData;
	private EEGAnalysis eegAnalysis;
	
	private int L;
	private int[] hypnogram;
	private double[] signal_min;
	
	private int time_acq;
	private int time_off;
	private double loss_rate;

	private double eTST;
	private double eSL;
	private double eSE;
	private double eWASO;
	private double eRL;
	private double eRE;
	private double eSCC;
	private double eSWW;
	private double eSBW;
	
	private double indexAwaken;
	private double indexSleepCycle;
	private double indexStability;
	private double indexTotal;

	private int idx_fall_asleep;
	
	private static int STATE_REM = 4;
	private static int STATE_WAKE = 5;
	private static int STATE_OFF = 6;
	
	private static int STANDARD_TST = 420;
	private static int STANDARD_SL = 60;
	private static int STANDARD_WASO = 60;
	private static int STANDARD_RL1 = 30;
	private static int STANDARD_RL2 = 60;
	private static int STANDARD_RL3 = 120;
	private static int STANDARD_RL4 = 180;
	private static int STANDARD_RE1 = 5;
	private static int STANDARD_RE2 = 20;
	private static int STANDARD_RE3 = 30;
	private static int STANDARD_RE4 = 45;
	
	public SleepAnalysis(EEGData eegData, EEGAnalysis eegAnalysis){
		this.eegData = eegData;
		this.signal_min = this.eegData.getSignalMin();
		
		this.eegAnalysis = eegAnalysis;
		this.L = this.eegAnalysis.getLength();
		this.hypnogram = this.eegAnalysis.getHypnogram();
		
		time_acq = L;
		for(int i = 0; i < L; i++){
			if(hypnogram[i] == STATE_OFF){
				time_off++;
			}
		}
		time_acq *= 5;
		time_off *= 5;
		loss_rate = 1.0 * time_off / time_acq * 100.0;
		reCalculateAll();
	}
	
	public void reCalculateAll(){
		
		calcElementTotalSleepTim();
		calcElementSleepLatency();
		calcElementSleepEfficiency();
		calcElementWakeAfterSleepOnset();
		
		calcElementREMLatency();
		calcElementREMEfficiency();
		calcElementSleepCycleCount();
		calcElementStateWhenWake();
		
		calcElementStabilityOfBrainWave();
		
		calcIndexAwaken();
		calcIndexSleepCycle();
		calcIndexStability();
		calcIndexTotal();
		
		/*
		System.out.print(eTST + "\t");
		System.out.print(eSL + "\t");
		System.out.print(eSE + "\t");
		System.out.print(eWASO + "\t");
		System.out.print(eRL + "\t");
		System.out.print(eRE + "\t");
		System.out.print(eSCC + "\t");
		System.out.print(eSWW + "\t");
		System.out.print(eSBW + "\t");
		System.out.print(loss_rate + "\t");
		System.out.print(indexAwaken + "\t");
		System.out.print(indexSleepCycle + "\t");
		System.out.print(indexStability + "\t");
		System.out.println(indexTotal);
		*/
	}
	
	private int totalSleepTime = 0;
	private int sleepLatency = 0;
	private double sleepEfficiency = 0;
	private int wakeAfterSleepOnset = 0;
	private int REMLatancy = 0;
	private double REMEfficiency = 0;
	private int sleepCycleCount = 0;
	private int stateWhenWake = 0;
	private double stabilityBrainWave = 0;
	
	private void calcElementTotalSleepTim(){
		double TST = L * 5.0;
		totalSleepTime = (int)TST;
		eTST = Math.abs(STANDARD_TST - Math.abs(STANDARD_TST - TST)) / STANDARD_TST * 100.0;
		if(TST > STANDARD_TST * 2){
			eTST = 0;
		}
	}
	
	private void calcElementSleepLatency(){
		double SL = 0.0;
		idx_fall_asleep = 0;
		for(int i = 0; i < L; i++){
			if((hypnogram[i] != STATE_OFF) && (hypnogram[i] != STATE_WAKE)){
				SL = (double)i * 5.0;
				idx_fall_asleep = i+1;
				break;
			}
		}
		sleepLatency = (int)SL;
		eSL = (1.0 - (SL / (double)STANDARD_SL)) * 100.0;
		if(SL > STANDARD_SL){
			eSL = 0.0;
		}
	}
	
	private void calcElementSleepEfficiency() {
		double TST = L * 5.0;
		int SW = 0;
		for(int i = 0; i < L; i++){
			if(hypnogram[i] == STATE_WAKE){
				SW++;
			}
		}
		SW *= 5;
		eSE = (TST - (double)SW) / TST * 100.0;
		sleepEfficiency = eSE;
	}
	
	private void calcElementWakeAfterSleepOnset(){
		int WASO = 0;
		for(int i = idx_fall_asleep; i < L; i++){
			if(hypnogram[i] == STATE_WAKE){
				WASO++;
			}
		}
		WASO *= 5;
		wakeAfterSleepOnset = WASO;
		eWASO = (1.0 - ((double)WASO / STANDARD_WASO)) * 100.0;
	}
	
	private void calcElementREMLatency(){
		double RL = 0.0;
		for(int i = idx_fall_asleep; i < L; i++){
			if(hypnogram[i] == STATE_REM){
				RL = (double)i * 5.0;
				break;
			}
		}
		REMLatancy = (int)RL;
		if(RL < STANDARD_RL1){
			eRL = 0.0;
		}else if(RL < STANDARD_RL2){
			eRL = (RL - STANDARD_RL1) / (STANDARD_RL2 - STANDARD_RL1) * 100.0;
		}else if(RL < STANDARD_RL3){
			eRL = 100.0;
		}else if(RL < STANDARD_RL4){
			eRL = (1.0 - (RL - STANDARD_RL3) / (STANDARD_RL4 - STANDARD_RL3)) * 100.0;
		}else{
			eRL = 0.0;
		} 
	}
	
	private void calcElementREMEfficiency(){
		double TST = L * 5.0;
		double SL = 0.0;
		for(int i = 0; i < L; i++){
			if((hypnogram[i] != STATE_OFF) && (hypnogram[i] != STATE_WAKE)){
				SL = (double)i * 5.0;
				break;
			}
		}
		int RT = 0;
		for(int i = idx_fall_asleep; i < L; i++){
			if(hypnogram[i] == STATE_REM){
				RT++;
			}
		}
		RT *= 5;
		double RE = RT / (TST - SL) * 100.0;
		REMEfficiency = RE;
		if(RE < STANDARD_RE1){
		    eRE = 0.0;
		}else if(RE < STANDARD_RE2){
		    eRE = (RE - STANDARD_RE1) / (STANDARD_RE2 - STANDARD_RE1) * 100.0;
		}else if(RE < STANDARD_RE3){
		    eRE = 100.0;
		}else if(RE < STANDARD_RE4){
		    eRE = (1.0 - (RE - STANDARD_RE3) / (STANDARD_RE4 - STANDARD_RE3)) * 100.0;
		}else{
		    eRE = 0.0;
		}
	}
	
	private void calcElementSleepCycleCount(){
		int SCC = 0;
		for(int i = 1; i < L; i++){
			if((hypnogram[i-1] != STATE_REM) && (hypnogram[i] == STATE_REM)){
				SCC++;
			}
		}
		sleepCycleCount = SCC;
		eSCC = (2.5 - Math.abs(SCC - 4.5)) * 50.0;
		if(eSCC < 0){
		    eSCC = 0.0;
		}
	}
	
	private void calcElementStateWhenWake(){
		int SWW = 0;
		if(hypnogram[L-1] == STATE_REM){
			SWW = 1;
		}
		stateWhenWake = SWW;
		eSWW = SWW * 100.0;
	}
	
	private void calcElementStabilityOfBrainWave(){
		int[] signal_tmp1 = new int[1440];
		int[] signal_tmp2 = new int[1440];
		int stable_time = 120;
		for(int i = 0; i < 1440; i++){
			signal_tmp1[i] = (this.signal_min[i] < -0.15) ? 1 : 0;
		}
		signal_tmp2 = morphology_open(signal_tmp1, 2);
		for(int i = 0; i < 1440; i++){
			if(signal_tmp2[i] == 1){
				stable_time = i + 2;
				break;
			}
		}
		double minVal = Double.MAX_VALUE;
		for(int i = 0; i < stable_time; i++){
			if(this.signal_min[i] < minVal){
				minVal = this.signal_min[i];
			}
		}
		stable_time++;
		double stable_degree = -1.0 * minVal;
		double param = stable_degree/stable_time;
		stabilityBrainWave = param;
		eSBW = param * 100.0 / 0.15;
		if(param > 0.15){
			eSBW = 100.0;
		}
		
	}
	
	private int[] morphology_open(int x[], int window_size){
		int i = 0;
		int length = x.length;
		int[] tmp = new int[length];
		int[] y = new int[length];
		for(i = 0; i < length; i++){
			tmp[i] = 0;
		}
		tmp = morphology_erosion(x, window_size);
		y = morphology_dilation(tmp, window_size);
		return y;
	}
	
	private int[] morphology_dilation(int x[], int window_size){
		int i = 0;
		int j = 0;
		int length = x.length;
		int[] y = new int[length];
		for(i = 0; i < length; i++){
			y[i] = x[i];
		}
		for(i = 0; i < length-window_size; i++){
			if(x[i] == 0)
				continue;
			for(j = i; j < i+window_size; j++){
				y[j] = 1;
			}
		}
		for(i = length-window_size; i < length; i++){
			if(x[i] == 0)
				continue;
			for(j = i; j < length; j++){
				y[j] = 1;
			}
		}
		return y;
	}

	private int[] morphology_erosion(int x[], int window_size){
		int i = 0;
		int j = 0;
		int length = x.length;
		int[] y = new int[length];
		for(i = 0; i < length; i++){
			y[i] = x[i];
		}
		for(i = 0; i < window_size; i++){
			if(x[i] == 1)
				continue;
			for(j = 0; j < i; j++){
				y[j] = 0;
			}
		}
		for(i = window_size; i < length; i++){
			if(x[i] == 1)
				continue;
			for(j = i-window_size; j < i; j++){
				y[j] = 0;
			}
		}
		return y;
	}

	private void calcIndexAwaken(){
		this.indexAwaken = (0.25 * eTST) + (0.25 * eSL) + (0.25 * eSE) + (0.25 * eWASO);
	}

	private void calcIndexSleepCycle(){
		this.indexSleepCycle = (0.25 * eRL) + (0.25 * eRE) + (0.25 * eSCC) + (0.25 * eSWW);
	}
	
	private void calcIndexStability(){
		this.indexStability = eSBW;
	}
	
	private void calcIndexTotal(){
		this.indexTotal = (0.7 * indexAwaken) + (0.2 * indexSleepCycle) + (0.1 * indexStability);
	}

	
	private double getElementTotalSleepTime() {
		return eTST;
	}
	

	private double getElementSleepLatency() {
		return eSL;
	}
	

	private double getElementSleepEfficiency() {
		return eSE;
	}
	

	private double getElementWakeAfterSleepOnset() {
		return eWASO;
	}
	

	private double getElementREMLatency() {
		return eRL;
	}
	

	private double getElementREMEfficiency() {
		return eRE;
	}
	

	private double getElementSleepCycleCount() {
		return eSCC;
	}
	

	private double getElementStateWhenWake() {
		return eSWW;
	}
	

	private double getElementStabilityOfBrainWave() {
		return eSBW;
	}
	

	public double getIndexAwaken() {
		return indexAwaken;
	}
	

	public double getIndexSleepCycle() {
		return indexSleepCycle;
	}
	

	public double getIndexStability() {
		return indexStability;
	}

	public double getIndexTotal() {
		return indexTotal;
	}

	public double getDataLossRate(){
		return this.loss_rate;
	}

	public int getTotalSleepTime() {
		return totalSleepTime;
	}

	public int getSleepLatency() {
		return sleepLatency;
	}

	public double getSleepEfficiency() {
		return sleepEfficiency;
	}

	public int getWakeAfterSleepOnset() {
		return wakeAfterSleepOnset;
	}

	public int getREMLatancy() {
		return REMLatancy;
	}

	public double getREMEfficiency() {
		return REMEfficiency;
	}

	public int getSleepCycleCount() {
		return sleepCycleCount;
	}

	public int getStateWhenWake() {
		return stateWhenWake;
	}

	public double getStabilityBrainWave() {
		return stabilityBrainWave;
	}

	

}












