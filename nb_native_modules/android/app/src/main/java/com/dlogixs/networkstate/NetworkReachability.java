package com.dlogixs.networkstate;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkReachability {
    static final private String TAG = "NetworkReachability";
    public static final int TYPE_NOT_REACHABLE = -1;
    public static final int TYPE_WIFI = 1;
    public static final int TYPE_WWAN = 0;

    public static final int STATUS_WIFI = 1;
    public static final int STATUS_WWAN = 2;
    public static final int STATUS_NOT_REACHABLE = 3;

    public final static String ACTION_CONNECTION_STATE_NOTIFICATION = "android.net.conn.CONNECTIVITY_CHANGE";
    public final static String ACTION_WIFI_STATE_NOTIFICATION = "android.net.wifi.WIFI_STATE_CHANGED";

    public static int getConnectivityInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        Boolean isConnected = netInfo != null && netInfo.isConnectedOrConnecting();
        int mode = -2;
        if (isConnected) {
            Log.d(TAG, "net123 netType: " + netInfo.getType());
            switch (netInfo.getType()) {
                case NetworkReachability.TYPE_WIFI:
                    mode = NetworkReachability.TYPE_WIFI;
                    break;
                case NetworkReachability.TYPE_WWAN:
                    mode = NetworkReachability.TYPE_WWAN;
                    break;
            }
        } else {
            mode = NetworkReachability.TYPE_NOT_REACHABLE;
        }
        return mode;
    }

}
