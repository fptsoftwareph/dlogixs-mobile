package com.dlogixs.alarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;

import java.util.List;

/**
 * Created by fptsoftware on 15/03/2018.
 */

public class AlarmScheduler implements OnItemInteractionListener {

    private AsyncAlarmsTableUpdateHandler asyncAlarmsTableUpdateHandler;
    private AlarmController alarmController;
    private Context context;
    private List<Integer> recurringDays;
    private AlarmCursor alarmCursorItems, alarmCursor;
    public final static String ACTION_ALARM_STOPPED_NOTIFICATION = "com.neurobeat.action.STOPPED_NOTIFICATION";
    public final static String ACTION_ALARM_ON_NOTIFICATION = "com.neurobeat.action.ON_NOTIFICATION";

    public AlarmScheduler (Context context) {
        this.context = context;
        alarmController = new AlarmController(context);
        asyncAlarmsTableUpdateHandler = new AsyncAlarmsTableUpdateHandler(context, alarmController, this);
    }

    public void onTimeSet(Bundle bundle) {
        String fireDate = bundle.getString("alarm_date");
        boolean hasVibrate = bundle.getBoolean("vibrate");
        boolean isRbsEnabled = bundle.getBoolean("rbs_enabled");
        String soundName = bundle.getString("sound_name");
        String songName = bundle.getString("song_name");
        recurringDays = bundle.getIntegerArrayList("repeat_alarm");
        int snoozeTime = (int)bundle.getDouble("snooze");
        if (fireDate == null)
            throw new IllegalStateException("Failed To Schedule Notification");
        String[] time = fireDate.split("\\:");
        int hours = Integer.parseInt(time[0]);
        int minutes = Integer.parseInt(time[1]);
        Alarm alarm = Alarm.builder().hour(hours).minutes(minutes).ringtone(soundName).vibrates(hasVibrate).snoozeDuration(snoozeTime).songName(songName).build();
        alarm.setEnabled(true);
        alarm.setRbsEnabled(isRbsEnabled);
        asyncAlarmsTableUpdateHandler.asyncInsert(alarm);
    }

    public void onTimeUpdate(Bundle bundle) {
        String fireDate = bundle.getString("alarm_date");
        int alarmId = (int)bundle.getDouble("id");
        String songName = bundle.getString("song_name");
        String soundName = bundle.getString("sound_name");
        boolean hasVibrate = bundle.getBoolean("vibrate");
        boolean isRbsEnabled = bundle.getBoolean("rbs_enabled");
        int snoozeTime = (int)bundle.getDouble("snooze");
        if (fireDate == null)
            throw new IllegalStateException("Failed To Schedule Notification");
        String[] time = fireDate.split("\\:");
        int hours = Integer.parseInt(time[0]);
        int minutes = Integer.parseInt(time[1]);
        List<Integer> repeatDays=bundle.getIntegerArrayList("repeat_alarm");
        alarmCursor = getAlarmCursor(alarmId);
        Alarm oldAlarm = Preconditions.checkNotNull(alarmCursor.getItem());

        for (int i = 0; i < DaysOfWeek.NUM_DAYS; i++) {
            oldAlarm.setRecurring(i, false);
            asyncAlarmsTableUpdateHandler.asyncUpdate(oldAlarm.getId(), oldAlarm);
        }

        for (int i = 0; i < repeatDays.size(); i++) {
            int weekDayAtPos = repeatDays.get(i);
            oldAlarm.setRecurring(weekDayAtPos, true);
            asyncAlarmsTableUpdateHandler.asyncUpdate(oldAlarm.getId(), oldAlarm);
        }
        oldAlarm.setRbsEnabled(isRbsEnabled);
        Alarm newAlarm=oldAlarm.toBuilder().hour(hours).minutes(minutes).ringtone(soundName).vibrates(hasVibrate).
        snoozeDuration(snoozeTime).songName(songName).build();
        oldAlarm.copyMutableFieldsTo(newAlarm);
        newAlarm.setEnabled(true);
        newAlarm.setRbsEnabled(isRbsEnabled);
        asyncAlarmsTableUpdateHandler.asyncUpdate(alarmId,newAlarm);
        alarmCursor.close();
    }

    public void onTimeDelete(int id) {
        alarmCursor = getAlarmCursor(id);
        Alarm alarm = Preconditions.checkNotNull(alarmCursor.getItem());
        asyncAlarmsTableUpdateHandler.asyncDelete(alarm);
        alarmCursor.close();
    }

    public void setToggle(int id, boolean isAlarmNotRingingAndSwitchOff) {
        System.out.println("ALARM ID: " + id);
        alarmCursor = getAlarmCursor(id);
        Alarm alarm = Preconditions.checkNotNull(alarmCursor.getItem());
        if(alarm.isEnabled()) {
            alarmController.cancelAlarm(alarm,false,isAlarmNotRingingAndSwitchOff);
            alarm.setEnabled(false);
        } else {
            alarm.setEnabled(true);
        }
        asyncAlarmsTableUpdateHandler.asyncUpdate(id, alarm);
        alarmCursor.close();
    }

    public void disableAlarms() {
        alarmCursorItems = new AlarmsTableManager(context).queryItems();
        try {
            System.out.print("ALARM CURSOR:"+alarmCursorItems);
            while (alarmCursorItems.moveToNext()) {
                Alarm alarm = Preconditions.checkNotNull(alarmCursorItems.getItem());
                alarmController.cancelAlarm(alarm,false,false);
                alarm.setEnabled(false);
                asyncAlarmsTableUpdateHandler.asyncUpdate(alarm.getId(), alarm);
            }
        }finally{
            alarmCursorItems.close();
            }
        }

    public WritableArray getScheduledAlarms() {
        alarmCursorItems = new AlarmsTableManager(context).queryItems();
        WritableArray alarmList = Arguments.createArray();
        int alarmMins, alarmHour;
        String alarmTimeMins, alarmTimeHour, alarmMeridiem="";
        try {
            while (alarmCursorItems.moveToNext()) {
                WritableMap tempAlarm = Arguments.createMap();
                Alarm alarm = Preconditions.checkNotNull(alarmCursorItems.getItem());

                alarmMins= alarm.minutes();
                alarmHour= alarm.hour();
                alarmTimeHour= ""+alarmHour;

                if (alarmHour >= 12) {
                    alarmMeridiem = "pm";
                    alarmHour = alarmHour - 12;
                    alarmTimeHour= alarmHour+"";
                }
                else {
                    alarmMeridiem = "am";
                }

                if (alarmHour == 0) {
                    alarmTimeHour = "12";
                    alarmHour=12;
                }
                if (alarmHour < 10) {
                    alarmTimeHour = "0" + alarmHour;
                }

                if (alarmMins < 10) {
                    alarmTimeMins = "0" + alarmMins;
                }
                else {
                    alarmTimeMins = ""+alarmMins;
                }

                final boolean[] hasRecurringDays = alarm.recurringDays();
                WritableArray daysOfWeek = Arguments.createArray();
                for (int i = 0; i < hasRecurringDays.length; i++) {
                    daysOfWeek.pushBoolean(hasRecurringDays[i]);
                }

                System.out.println("getScheduledAlarm(): " + alarm);
                tempAlarm.putInt("alarm_id", (int)alarm.getId());
                tempAlarm.putInt("hours", alarm.hour());
                tempAlarm.putInt("minute", alarm.minutes());
                tempAlarm.putString("alarm_time",alarmTimeHour+":"+alarmTimeMins);
                tempAlarm.putString("alarm_meridiem",alarmMeridiem);
                tempAlarm.putBoolean("alarm_enabled",alarm.isEnabled());
                tempAlarm.putArray("repeat_days", daysOfWeek);
                tempAlarm.putInt("snooze",alarm.snoozeDuration());
                tempAlarm.putString("song_uri",alarm.ringtone());
                tempAlarm.putString("song_name",alarm.songName());
                tempAlarm.putBoolean("rbs_enabled",alarm.isRbsEnabled());
                alarmList.pushMap(tempAlarm);
            }
            return alarmList;
        } finally {
            alarmCursorItems.close();
        }

    }

    public static Class getMainActivityClass(@NonNull Context context) {
        String packageName = context.getPackageName();
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        String className = launchIntent.getComponent().getClassName();
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onItemInsert(Alarm alarm) {
        alarmCursor = getAlarmCursor(alarm.getId());
        Alarm tempAlarm = Preconditions.checkNotNull(alarmCursor.getItem());
        for (int i = 0; i < recurringDays.size(); i++) {
            int weekDayAtPos = recurringDays.get(i);
            tempAlarm.setRecurring(weekDayAtPos, true);
            asyncAlarmsTableUpdateHandler.asyncUpdate(tempAlarm.getId(), tempAlarm);
        }
        alarmCursor.close();
    }

    @Override
    public void onItemDeleted(Alarm item) {
        alarmController.save(item);
    }

    @Override
    public void onItemUpdate(Alarm alarm) {
        alarmController.scheduleAlarm(alarm);
        alarmController.save(alarm);
    }

    public AlarmCursor getAlarmCursor(long id)
    {
        return new AlarmsTableManager(context).queryItem(id);
    }
}
