package com.dlogixs.alarm;

import android.content.Context;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;

/**
 * Created by fptsoftware on 02/04/2018.
 */

public class AlarmToneManager {

    private RingtoneManager mRingtoneManager;
    private Context mContext;
    private RingtoneLoop mRingtone;

    public AlarmToneManager(Context context) {
        mContext = context;
        mRingtoneManager = new RingtoneManager(context);
        mRingtoneManager.setType(RingtoneManager.TYPE_ALARM);
    }

    public WritableArray getAlarmRingtones() {
        WritableArray ringtones = Arguments.createArray();
        Cursor alarmCursor = mRingtoneManager.getCursor();
        int alarmCnt = alarmCursor.getCount();
        if (alarmCnt == 0 && !alarmCursor.moveToFirst())
            return null;
        while (!alarmCursor.isAfterLast() && alarmCursor.moveToNext()) {
            WritableMap ringtoneObj = Arguments.createMap();
            int currPos = alarmCursor.getPosition();
            Ringtone ringtone = mRingtoneManager.getRingtone(currPos);
            Uri ringtoneUri = mRingtoneManager.getRingtoneUri(currPos);
            ringtoneObj.putString("alarm_title", ringtone.getTitle(mContext));
            ringtoneObj.putString("alarm_uri", ringtoneUri.toString());
            ringtones.pushMap(ringtoneObj);
        }
        return ringtones;
    }

    public void playRingtone(Uri ringtoneUri) {
        if (mRingtone != null) {
            destroyLocalPlayer();
        }
        System.out.println("Play Ringtone: " + ringtoneUri.toString());
        mRingtone = new RingtoneLoop(mContext, ringtoneUri);
        mRingtone.play();
    }

    public void destroyLocalPlayer() {
        if (mRingtone != null) {
            mRingtone.stop();
            mRingtone = null;
        }
    }
}
