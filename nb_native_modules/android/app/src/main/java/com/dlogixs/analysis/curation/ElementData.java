package com.dlogixs.analysis.curation;

import java.text.DecimalFormat;
import java.util.Random;

public class ElementData {
	public static final int TST = 1;
	public static final int GTBT = 2;
	public static final int SUN = 3;
	public static final int WALK = 4;
	public static final int COFFEE = 5;
	public static final int SMOKE = 6;
	public static final int DRINK = 7;
	public static final int BB = 8;
	
	private String date;
	private int elementSBW;
	private int elementTST;
	private int elementGTBT;
	private int elementSUN;
	private int elementWALK;
	private int elementCOFFEE;
	private int elementSMOKE;
	private int elementDRINK;
	private int elementBB3Hz;
	private int elementBB6Hz;
	private int elementBB9Hz;
	private int elementBB12Hz;
	private int elementBB;
	private int indexOfBB;
	private int indexOfPosition;
	private int sqiAWAKEN;
	private int sqiCYCLE;
	private int sqiSTABILITY;
	private int sqiTOTAL;
	private int sqiRM;
	
	public ElementData(){
		super();
		this.date = getCurrentDate();
		this.elementSBW = generateRandomInteger(0,100);
		
		this.elementTST = generateRandomInteger(320,470);
		this.elementGTBT = generateRandomInteger(-50,100);
		this.elementSUN = generateRandomInteger(60,240);
		this.elementWALK = generateRandomInteger(2000,10000);
		
		this.elementCOFFEE = generateRandomInteger(0,5);
		this.elementSMOKE = generateRandomInteger(10,25);
		this.elementDRINK = generateRandomInteger(0,5);
		
		while((elementBB3Hz+elementBB6Hz+elementBB9Hz+elementBB12Hz) == 0){
			this.elementBB3Hz = generateRandomIntegerBB();
			if((elementBB3Hz) == 0)
				this.elementBB6Hz = generateRandomIntegerBB();
			else
				this.elementBB6Hz = 0;
			if((elementBB3Hz+elementBB6Hz) == 0)
				this.elementBB9Hz = generateRandomIntegerBB();
			else
				this.elementBB9Hz = 0;
			if((elementBB3Hz+elementBB6Hz+elementBB9Hz) == 0)
			this.elementBB12Hz = generateRandomIntegerBB();
			else
				this.elementBB12Hz = 0;
		}
		if(elementBB3Hz != 0)
			this.indexOfBB = 0;
		if(elementBB6Hz != 0)
			this.indexOfBB = 1;
		if(elementBB9Hz != 0)
			this.indexOfBB = 2;
		if(elementBB3Hz != 0)
			this.indexOfBB = 3;
		this.indexOfPosition = -1;
		this.elementBB = elementBB3Hz+elementBB6Hz+elementBB9Hz+elementBB12Hz;
		
		this.sqiAWAKEN = generateRandomInteger(65, 95);
		this.sqiCYCLE = generateRandomInteger(20, 80);
		this.sqiSTABILITY = generateRandomInteger(0, 100);
		this.sqiTOTAL = (int) (this.sqiAWAKEN*0.7 + this.sqiCYCLE*0.2 + this.sqiSTABILITY*0.1);
		this.sqiRM = (int) (this.sqiAWAKEN*0.7 + this.sqiSTABILITY*0.1);
	}
	
	public ElementData(String date, int elementSBW,
			int elementTST, int elementGTBT, int elementSUN, int elementWALK,
			int elementCOFFEE, int elementSMOKE, int elementDRINK,
			int elementBB3Hz, int elementBB6Hz, int elementBB9Hz, int elementBB12Hz,
			int sqiAWAKEN, int sqiCYCLE, int sqiSTABILITY, int sqiTOTAL) {
		this();
		this.date = date;
		this.elementSBW = elementSBW;
		this.elementTST = elementTST;
		this.elementGTBT = elementGTBT;
		this.elementSUN = elementSUN;
		this.elementWALK = elementWALK;
		this.elementCOFFEE = elementCOFFEE;
		this.elementSMOKE = elementSMOKE;
		this.elementDRINK = elementDRINK;
		this.elementBB3Hz = elementBB3Hz;
		this.elementBB6Hz = elementBB6Hz;
		this.elementBB9Hz = elementBB9Hz;
		this.elementBB12Hz = elementBB12Hz;
		if(elementBB3Hz != 0)
			this.indexOfBB = 0;
		if(elementBB6Hz != 0)
			this.indexOfBB = 1;
		if(elementBB9Hz != 0)
			this.indexOfBB = 2;
		if(elementBB3Hz != 0)
			this.indexOfBB = 3;
		this.elementBB = elementBB3Hz+elementBB6Hz+elementBB9Hz+elementBB12Hz;
		this.sqiAWAKEN = sqiAWAKEN;
		this.sqiCYCLE = sqiCYCLE;
		this.sqiSTABILITY = sqiSTABILITY;
		this.sqiTOTAL = sqiTOTAL;
		this.sqiRM = (int) (this.sqiAWAKEN*0.7 + this.sqiSTABILITY*0.1);
	}

	private int generateRandomInteger(int min, int max){
		Random rand = new Random();
		return rand.nextInt(max - min + 1) + min;
	}
	
	private int generateRandomIntegerBB(){
		int min = 3;
		int max = 10;
		Random rand = new Random();
		int zero = (rand.nextDouble() > 0.66)? 1 : 0;
		return zero * (rand.nextInt(max - min + 1) + min);
	}
	
	private String getCurrentDate(){
		/* Today */
		/*
		DecimalFormat df = new DecimalFormat("00");
		Calendar currentCal = Calendar.getInstance();
		
		currentCal.add(Calendar.DATE, 0);

		String year = Integer.toString(currentCal.get(Calendar.YEAR));
		String month = df.format(currentCal.get(Calendar.MONTH)+1);
		String day= df.format(currentCal.get(Calendar.DAY_OF_MONTH));
		
		return year + "-" + month + "-" + day;
		*/
		
		/* Random Date */
		DecimalFormat df = new DecimalFormat("00");
		String year = "2017";
		String month = df.format(generateRandomInteger(1,12));
		String day= df.format(generateRandomInteger(1,30));
		return year + "-" + month + "-" + day;
	}

	public String getDate() {
		return date;
	}

	/*
	public int getElementSBW() {
		return elementSBW;
	}
	*/

	public int getElementTST() {
		return elementTST;
	}

	public int getElementGTBT() {
		return elementGTBT;
	}

	public int getElementSUN() {
		return elementSUN;
	}

	public int getElementWALK() {
		return elementWALK;
	}

	public int getElementCOFFEE() {
		return elementCOFFEE;
	}

	public int getElementSMOKE() {
		return elementSMOKE;
	}

	public int getElementDRINK() {
		return elementDRINK;
	}

	public int getElementBB3Hz() {
		return elementBB3Hz;
	}

	public int getElementBB6Hz() {
		return elementBB6Hz;
	}

	public int getElementBB9Hz() {
		return elementBB9Hz;
	}

	public int getElementBB12Hz() {
		return elementBB12Hz;
	}

	public int getElementBB() {
		return elementBB;
	}
	
	public int getIndexOfBB() {
		return indexOfBB;
	}
	
	public int getIndexOfPosition() {
		return indexOfPosition;
	}

	public void setIndexOfPosition(int indexOfPosition) {
		this.indexOfPosition = indexOfPosition;
	}

	public String toString(){
		return date + "\t" + elementSBW + "\t" + elementTST + "\t" + elementGTBT + "\t" + elementSUN + "\t" + elementWALK
				+ "\t" + elementCOFFEE + "\t" + elementSMOKE + "\t" + elementDRINK
				+ "\t" + elementBB3Hz + "\t" + elementBB6Hz + "\t" + elementBB9Hz + "\t" + elementBB12Hz
				 + "\t" + sqiAWAKEN + "\t" + sqiCYCLE + "\t" + sqiSTABILITY + "\t" + sqiTOTAL + "\t" + sqiRM;
	}
	
	public int getElement(int element_index){
		switch(element_index){
		case ElementData.TST: return elementTST;
		case ElementData.GTBT: return elementGTBT;
		case ElementData.SUN: return elementSUN;
		case ElementData.WALK: return elementWALK;
		case ElementData.COFFEE: return elementCOFFEE;
		case ElementData.SMOKE: return elementSMOKE;
		case ElementData.DRINK: return elementDRINK;
		}
		return Integer.MIN_VALUE;
	}
	
	
	
	public int getSqiAWAKEN() {
		return sqiAWAKEN;
	}

	public int getSqiCYCLE() {
		return sqiCYCLE;
	}

	public int getSqiSTABILITY() {
		return sqiSTABILITY;
	}

	public int getSqiTOTAL() {
		return sqiTOTAL;
	}

	public int getSqiRM() {
		return sqiRM;
	}

	public boolean equal(ElementData anotherOne) {
		if(!anotherOne.getDate().equals(getDate())) return false;
		if(anotherOne.getElementTST() != getElementTST()) return false;
		if(anotherOne.getElementGTBT() != getElementGTBT()) return false;
		if(anotherOne.getElementSUN() != getElementSUN()) return false;
		if(anotherOne.getElementWALK() != getElementWALK()) return false;
		if(anotherOne.getElementCOFFEE() != getElementCOFFEE()) return false;
		if(anotherOne.getElementSMOKE() != getElementSMOKE()) return false;
		if(anotherOne.getElementDRINK() != getElementDRINK()) return false;
		if(anotherOne.getElementBB3Hz() != getElementBB3Hz()) return false;
		if(anotherOne.getElementBB6Hz() != getElementBB6Hz()) return false;
		if(anotherOne.getElementBB9Hz() != getElementBB9Hz()) return false;
		if(anotherOne.getElementBB12Hz() != getElementBB12Hz()) return false;
		if(anotherOne.getSqiAWAKEN() != getSqiAWAKEN()) return false;
		if(anotherOne.getSqiCYCLE() != getSqiCYCLE()) return false;
		if(anotherOne.getSqiSTABILITY() != getSqiSTABILITY()) return false;
		if(anotherOne.getSqiTOTAL() != getSqiTOTAL()) return false;
		return true;
	}
}
