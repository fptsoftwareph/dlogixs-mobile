package com.dlogixs.alarm;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by fptsoftware on 18/04/2018.
 */

public class Downloader {
    private DownloadManager downloadManager;
    private Context context;

    public Downloader(Context ctx) {
        context = ctx;
        downloadManager = (DownloadManager) ctx.getSystemService(DOWNLOAD_SERVICE);
    }

    public DownloadManager.Request createRequest(String url, ReadableMap headers, ReadableMap requestConfig) {
        String downloadTitle = requestConfig.getString("downloadTitle");
        String downloadDescription = requestConfig.getString("downloadTitle");
        String saveAsName = requestConfig.getString("saveAsName");

        Boolean external = requestConfig.getBoolean("external");
        String external_path = requestConfig.getString("path");

        Boolean allowedInRoaming = requestConfig.getBoolean("allowedInRoaming");
        Boolean allowedInMetered = requestConfig.getBoolean("allowedInMetered");
        Boolean showInDownloads = requestConfig.getBoolean("showInDownloads");

        Uri downloadUri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);

        ReadableMapKeySetIterator iterator = headers.keySetIterator();
        while (iterator.hasNextKey()) {
            String key = iterator.nextKey();
            request.addRequestHeader(key, headers.getString(key));
        }

        if(external) {
            request.setDestinationInExternalPublicDir(external_path, saveAsName);
        } else {
            request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS, saveAsName);
        }

        request.setTitle(downloadTitle);
        request.setDescription(downloadDescription);
        request.setAllowedOverRoaming(allowedInRoaming);
        request.setAllowedOverMetered(allowedInMetered);
        request.setVisibleInDownloadsUi(showInDownloads);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        return request;
    }

    public long queueDownload(DownloadManager.Request request) {
        return downloadManager.enqueue(request);
    }

}
