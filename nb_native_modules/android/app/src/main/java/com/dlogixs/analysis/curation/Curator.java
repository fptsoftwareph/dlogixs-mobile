package com.dlogixs.analysis.curation;

import java.util.ArrayList;
import java.util.List;

public class Curator {
	private List<ElementData> database = new ArrayList<ElementData>();
	private ElementData data;
	private RoleModelSelector selector;
	private CurationCreator creatorByTST;
	private CurationCreator creatorByGTBT;
	private CurationCreator creatorBySUN;
	private CurationCreator creatorByWALK;
	private CurationCreator creatorByCOFFEE;
	private CurationCreator creatorBySMOKE;
	private CurationCreator creatorByDRINK;
	private CurationCreator creatorByBB;
	
	private ElementData modelByTST;
	private ElementData modelByGTBT;
	private ElementData modelBySUN;
	private ElementData modelByWALK;
	private ElementData modelByCOFFEE;
	private ElementData modelBySMOKE;
	private ElementData modelByDRINK;
	private ElementData modelByBB;
	
	public Curator(List<ElementData> database, ElementData data) {
		super();
		this.database = database;
		this.data = data;
		selectRoleModel();
	}
	
	private void selectRoleModel(){
		selector = new RoleModelSelector(this.database, this.data);
		
		modelByTST = selector.getRoleModel(ElementData.TST);
		modelByGTBT = selector.getRoleModel(ElementData.GTBT);
		modelBySUN = selector.getRoleModel(ElementData.SUN);
		modelByWALK = selector.getRoleModel(ElementData.WALK);
		modelByCOFFEE = selector.getRoleModel(ElementData.COFFEE);
		modelBySMOKE = selector.getRoleModel(ElementData.SMOKE);
		modelByDRINK = selector.getRoleModel(ElementData.DRINK);
		modelByBB = selector.getRoleModel(ElementData.BB);

		creatorByTST = new CurationCreator(ElementData.TST, modelByTST, data);
		creatorByGTBT = new CurationCreator(ElementData.GTBT, modelByGTBT, data);
		creatorBySUN = new CurationCreator(ElementData.SUN, modelBySUN, data);
		creatorByWALK = new CurationCreator(ElementData.WALK, modelByWALK, data);
		creatorByCOFFEE = new CurationCreator(ElementData.COFFEE, modelByCOFFEE, data);
		creatorBySMOKE = new CurationCreator(ElementData.SMOKE, modelBySMOKE, data);
		creatorByDRINK = new CurationCreator(ElementData.DRINK, modelByDRINK, data);
		creatorByBB = new CurationCreator(ElementData.BB, modelByBB, data);
	}
	
	public CurationCreator getCreatorByIndex(int element_index){
		switch(element_index){
		case ElementData.TST: return creatorByTST;
		case ElementData.GTBT: return creatorByGTBT;
		case ElementData.SUN: return creatorBySUN;
		case ElementData.WALK: return creatorByWALK;
		case ElementData.COFFEE: return creatorByCOFFEE;
		case ElementData.SMOKE: return creatorBySMOKE;
		case ElementData.DRINK: return creatorByDRINK;
		case ElementData.BB: return creatorByBB;
		}
		return null;
	}
	
}
