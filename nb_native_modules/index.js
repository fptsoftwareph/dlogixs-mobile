'use-strict'
import { NativeModules } from 'react-native';
const { MyModule, SleepModule, SMAModule, CurationModule, StationModule, AlarmModule, ReachabilityModule } = NativeModules;
export { MyModule, SleepModule, SMAModule, CurationModule, StationModule, AlarmModule, ReachabilityModule };
