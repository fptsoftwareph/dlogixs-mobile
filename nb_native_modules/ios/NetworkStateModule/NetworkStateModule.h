//
//  RCTNetworkStatus.h
//
//  Created by FPT Software on 14/09/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface NetworkStateModule : RCTEventEmitter<RCTBridgeModule>

@end

