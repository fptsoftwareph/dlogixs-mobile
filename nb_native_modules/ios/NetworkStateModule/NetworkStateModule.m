//
//  RCTNetworkStatus.h
//
//  Created by FPT Software on 14/09/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "NetworkStateModule.h"
#import "Reachability.h"
#import "Define.h"

//static NSString *const REMOTE_HOST = @"www.apple.com";

@interface NetworkStateModule() {
    NSMutableDictionary *_connStatusMap, *_connStatusMap2;
    BOOL _hasListener;
    Reachability *_internetReachability;
}
@end

@implementation NetworkStateModule {
    NetworkStatus prevStatus;
}

- (instancetype)init {
    if (self = [super init]) {
        _connStatusMap = [NSMutableDictionary new];
        _connStatusMap2 = [NSMutableDictionary new];
    }
    return self;
}

RCT_EXPORT_MODULE(ReachabilityModule);

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}

- (void)startObserving {
    _hasListener = YES;
}

- (void)stopObserving {
    _hasListener = NO;
}

- (NSArray<NSString *> *)supportedEvents {
    return @[@"ReachabilityStatusListener", @"ReachabilityStatusListener2"];
}

RCT_EXPORT_METHOD(startNetworkNotifier) {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    _internetReachability = [Reachability reachabilityForInternetConnection];
    [_internetReachability startNotifier];
    [self updateModeWithReachability:_internetReachability];
}

RCT_EXPORT_METHOD(stopNetworkNotifier) {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [_internetReachability stopNotifier];
}

/*!
 * Called by Reachability whenever status changes.
 */
- (void)reachabilityChanged:(NSNotification *)note {
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateModeWithReachability:curReach];
}

-(void)updateModeWithReachability:(Reachability *)reachability {
    if (reachability == _internetReachability) {
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        switch (netStatus) {
            case ReachableViaWiFi:
                [_connStatusMap setObject:@(1) forKey:REACHABILITY_MODE];
                [_connStatusMap setObject:@"Wifi" forKey:REACHABILITY_TYPE];
                [_connStatusMap2 setObject:@(1) forKey:REACHABILITY_MODE];
                [_connStatusMap2 setObject:@"Wifi" forKey:REACHABILITY_TYPE];
                break;
            case ReachableViaWWAN:
                [_connStatusMap setObject:@(2) forKey:REACHABILITY_MODE];
                [_connStatusMap setObject:@"WWAN" forKey:REACHABILITY_TYPE];
                [_connStatusMap2 setObject:@(2) forKey:REACHABILITY_MODE];
                [_connStatusMap2 setObject:@"WWAN" forKey:REACHABILITY_TYPE];
                break;
            case NotReachable:
                [_connStatusMap setObject:@(3) forKey:REACHABILITY_MODE];
                [_connStatusMap setObject:@"NotReachable" forKey:REACHABILITY_TYPE];
                [_connStatusMap2 setObject:@(3) forKey:REACHABILITY_MODE];
                [_connStatusMap2 setObject:@"NotReachable" forKey:REACHABILITY_TYPE];
                break;
        }
    }
    if (_hasListener) {
        [self sendEventWithName:@"ReachabilityStatusListener" body:_connStatusMap];
        [self sendEventWithName:@"ReachabilityStatusListener2" body:_connStatusMap2];
    }
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end

