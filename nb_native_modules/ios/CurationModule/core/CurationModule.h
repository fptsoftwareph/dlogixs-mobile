//
//  CurationModule.h
//  CurationModule
//
//  Created by FPT Software on 08/12/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface CurationModule : NSObject <RCTBridgeModule>

@end
