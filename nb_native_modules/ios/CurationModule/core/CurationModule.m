//
//  CurationModule.m
//  CurationModule
//
//  Created by FPT Software on 08/12/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import "CurationModule.h"
#import <React/RCTConvert.h>
#import "Define.h"
#import "Record.h"
#import "ServiceCuration.h"

typedef NS_ENUM(int, CurationServiceId) {
    CurationServiceIdModelDate = 1,
    CurationServiceIdDataDate,
    CurationServiceIdElementModel,
    CurationServiceIdElementData,
    CurationServiceIdDifference,
    CurationServiceIdDirection,
    CurationServiceIdMessage,
    CurationServiceIdIsUpdated
};

@implementation RCTConvert (CurationServiceId)
RCT_ENUM_CONVERTER(CurationServiceId, (@{ CURATION_MODEL_DATE: @(CurationServiceIdModelDate),
                                          CURATION_DATA_DATE: @(CurationServiceIdDataDate),
                                          CURATION_ELEMENT_MODEL: @(CurationServiceIdElementModel),
                                          CURATION_ELEMENT_DATA: @(CurationServiceIdElementData),
                                          CURATION_DIFFERENCE: @(CurationServiceIdDifference),
                                          CURATION_DIRECTION: @(CurationServiceIdDirection),
                                          CURATION_MESSAGE: @(CurationServiceIdMessage),
                                          CURATION_IS_UPDATED: @(CurationServiceIdIsUpdated)
                                          }),
                   0, intValue)
@end

@interface CurationModule() {
    ServiceCuration *_curation;
}
@end

@implementation CurationModule

RCT_EXPORT_MODULE()

-(NSDictionary *)constantsToExport {
    return @{ CURATION_MODEL_DATE: @(CurationServiceIdModelDate),
              CURATION_DATA_DATE: @(CurationServiceIdDataDate),
              CURATION_ELEMENT_MODEL: @(CurationServiceIdElementModel),
              CURATION_ELEMENT_DATA: @(CurationServiceIdElementData),
              CURATION_DIFFERENCE: @(CurationServiceIdDifference),
              CURATION_DIRECTION: @(CurationServiceIdDirection),
              CURATION_MESSAGE: @(CurationServiceIdMessage),
              CURATION_IS_UPDATED: @(CurationServiceIdIsUpdated)
              };
}

RCT_EXPORT_METHOD(initCurationAnalysis:(NSArray *)database data:(NSDictionary *)data callback:(nonnull RCTResponseSenderBlock)callback) {
    NSString *jsonDB = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:database options:0 error:nil] encoding:NSUTF8StringEncoding];
    NSString *jsonData = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:data options:0 error:nil] encoding:NSUTF8StringEncoding];
    NSLog(@"curation123 initCurationAnalysisModule");
    NSLog(@"curation123 initCuration DB: %@ \nData: %@", jsonDB, jsonData);
    NSMutableArray<Record *> *database2 = [[NSMutableArray alloc] init];
    NSString *date;
    int elementTST, elementGTBT, elementSUN, elementWALK, elementCOFFEE, elementSMOKE, elementDRINK, elementBB, sqiAWAKEN, sqiCYCLE, sqiSTABILITY, indexOfBb;
    
    for (int i = 0; i < database.count; i++) {
        date = [RCTConvert NSString:[[database objectAtIndex:i] objectForKey:@"date"]];
        elementTST = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementTST"]];
        elementGTBT = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementGTBT"]];
        elementSUN = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementSUN"]];
        elementWALK = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementWALK"]];
        elementCOFFEE = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementCOFFEE"]];
        elementSMOKE = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementSMOKE"]];
        elementDRINK = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementDRINK"]];
        elementBB = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"elementBB"]];
        sqiAWAKEN = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"sqiAWAKEN"]];
        sqiCYCLE = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"sqiCYCLE"]];
        sqiSTABILITY = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"sqiSTABILITY"]];
//        sqiTOTAL = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"sqiTOTAL"]];
        indexOfBb = [RCTConvert int:[[database objectAtIndex:i] objectForKey:@"indexOfBB"]];
        [database2 addObject:[[Record alloc] initWithDate:date
                                               elementTST:elementTST
                                              elementGTBT:elementGTBT
                                               elementSUN:elementSUN
                                              elementWALK:elementWALK
                                            elementCOFFEE:elementCOFFEE
                                             elementSMOKE:elementSMOKE
                                             elementDRINK:elementDRINK
                                                elementBB:elementBB
                                                sqiAWAKEN:sqiAWAKEN
                                                 sqiCYCLE:sqiCYCLE
                                             sqiSTABILITY:sqiSTABILITY
                                            indexBinaural:indexOfBb]];
        
    }
    
    date = [RCTConvert NSString:[data objectForKey:@"date"]];
    elementTST = [RCTConvert int:[data objectForKey:@"elementTST"]];
    elementGTBT = [RCTConvert int:[data objectForKey:@"elementGTBT"]];
    elementSUN = [RCTConvert int:[data objectForKey:@"elementSUN"]];
    elementWALK = [RCTConvert int:[data objectForKey:@"elementWALK"]];
    elementCOFFEE = [RCTConvert int:[data objectForKey:@"elementCOFFEE"]];
    elementSMOKE = [RCTConvert int:[data objectForKey:@"elementSMOKE"]];
    elementDRINK = [RCTConvert int:[data objectForKey:@"elementDRINK"]];
    sqiAWAKEN = [RCTConvert int:[data objectForKey:@"sqiAWAKEN"]];
    elementBB = [RCTConvert int:[data objectForKey:@"elementBB"]];
    sqiCYCLE = [RCTConvert int:[data objectForKey:@"sqiCYCLE"]];
    sqiSTABILITY = [RCTConvert int:[data objectForKey:@"sqiSTABILITY"]];
//    sqiTOTAL = [RCTConvert int:[data objectForKey:@"sqiTOTAL"]];
    indexOfBb = [RCTConvert int:[data objectForKey:@"indexOfBB"]];
    
    Record *data2 = [[Record alloc] initWithDate:date
                                      elementTST:elementTST
                                     elementGTBT:elementGTBT
                                      elementSUN:elementSUN
                                     elementWALK:elementWALK
                                   elementCOFFEE:elementCOFFEE
                                    elementSMOKE:elementSMOKE
                                    elementDRINK:elementDRINK
                                       elementBB:elementBB
                                       sqiAWAKEN:sqiAWAKEN
                                        sqiCYCLE:sqiCYCLE
                                    sqiSTABILITY:sqiSTABILITY
                                   indexBinaural:indexOfBb];
    _curation = [[ServiceCuration alloc] initServiceCuration:database2 data:data2];
    [_curation selectModel];
    callback(@[@"CurationAnalysis data initialized"]);
}

RCT_EXPORT_METHOD(getCuratorAnalysisData:(int)elemNdx key:(CurationServiceId)key callback:(nonnull RCTResponseSenderBlock)callback) {
    switch (key) {
        case CurationServiceIdModelDate:
            callback(@[[_curation getModel:elemNdx].date]);
            break;
        case CurationServiceIdDataDate:
            callback(@[[_curation getData].date]);
            break;
        case CurationServiceIdElementModel:
            callback(@[@([[_curation getModel:elemNdx] getElement:elemNdx])]);
            break;
        case CurationServiceIdElementData:
            callback(@[@([[_curation getData] getElement:elemNdx])]);
            break;
        case CurationServiceIdDifference:
            callback(@[@(ABS([_curation getModel:elemNdx].difference))]);
            break;
        case CurationServiceIdDirection:
            callback(@[@([_curation getModel:elemNdx].direction)]);
            break;
        case CurationServiceIdMessage:
            callback(@[[[_curation getGuideMessage:elemNdx] jsonizeString]]);
            break;
        case CurationServiceIdIsUpdated:
            callback(@[@([_curation getModel:elemNdx].isUpdated)]);
            break;
    }
}

@end
