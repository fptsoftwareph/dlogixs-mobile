//
//  Record.m
//  CurationModule
//
//  Created by FPT Software on 24/07/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "Record.h"

#define ELEMENT_LEN 8
#define INDEX_LEN 5
#define ELEMENT_BINAURAL_LEN 4

@interface Record() {
    NSMutableArray *_element, *_index, *_elementBinaural;
}
@end

@implementation Record

-(instancetype)init {
    if (self = [super init]) {
        _element = [[NSMutableArray alloc] initWithCapacity:ELEMENT_LEN];
        _index = [[NSMutableArray alloc] initWithCapacity:INDEX_LEN];
        _elementBinaural = [[NSMutableArray alloc] initWithCapacity:ELEMENT_BINAURAL_LEN];
        self.date = @"YYYY-MM-DD";
        _element[CurationElementIdTST] = @(0);
        _element[CurationElementIdGTBT] = @(0);
        _element[CurationElementIdSUN] = @(0);
        _element[CurationElementIdWALK] = @(0);
        
        _element[CurationElementIdCOFFEE] = @(0);
        _element[CurationElementIdSMOKE] = @(0);
        _element[CurationElementIdDRINK] = @(0);
        
        _elementBinaural[CurationBinauralId3Hz] = @(0);
        _elementBinaural[CurationBinauralId6Hz] = @(0);
        _elementBinaural[CurationBinauralId9Hz] = @(0);
        _elementBinaural[CurationBinauralId12Hz] = @(0);
        
        _element[CurationElementIdBB] = @([(NSNumber *)_elementBinaural[CurationBinauralId3Hz] integerValue] + [(NSNumber *)_elementBinaural[CurationBinauralId6Hz] integerValue] + [(NSNumber *)_elementBinaural[CurationBinauralId9Hz] integerValue] + [(NSNumber *)_elementBinaural[CurationBinauralId12Hz] integerValue]);
        
        _index[CurationIndexIdAwaken] = @(0);
        _index[CurationIndexIdCycle] = @(0);
        _index[CurationIndexIdStable] = @(0);
        _index[CurationIndexIdTotal] = @(0);
        _index[CurationIndexIdStandard] = @(0);
        
        self.direction = 0;
        self.difference = 0;
        self.updated = NO;
    }
    return self;
}

-(instancetype)initWithDate:(NSString *)date
                 elementTST:(NSInteger)elemTST
                elementGTBT:(NSInteger)elemGTBT
                 elementSUN:(NSInteger)elemSun
                elementWALK:(NSInteger)elemWALK
              elementCOFFEE:(NSInteger)elemCOFFEE
               elementSMOKE:(NSInteger)elemSMOKE
               elementDRINK:(NSInteger)elemDRINK
                  elementBB:(NSInteger)elemBB
                  sqiAWAKEN:(NSInteger)indexAWAKEN
                   sqiCYCLE:(NSInteger)indexCYCLE
               sqiSTABILITY:(NSInteger)indexSTABILITY
              indexBinaural:(NSInteger)indexBinaural {
    if (self = [self init]) {
        self.date = date;
        _element[CurationElementIdTST] = @(elemTST);
        _element[CurationElementIdGTBT] = @(elemGTBT);
        _element[CurationElementIdSUN] = @(elemSun);
        _element[CurationElementIdWALK] = @(elemWALK);
        _element[CurationElementIdCOFFEE] = @(elemCOFFEE);
        _element[CurationElementIdSMOKE] = @(elemSMOKE);
        _element[CurationElementIdDRINK] = @(elemDRINK);
        _element[CurationElementIdBB] = @(elemBB);
        
        _index[CurationIndexIdAwaken] = @(indexAWAKEN);
        _index[CurationIndexIdCycle] = @(indexCYCLE);
        _index[CurationIndexIdStable] = @(indexSTABILITY);
        _index[CurationIndexIdTotal] = @([(NSNumber *)_index[CurationIndexIdAwaken] integerValue] * 0.7f + [(NSNumber *)_index[CurationIndexIdCycle] integerValue] * 0.2f + [(NSNumber *)_index[CurationIndexIdStable] integerValue] * 0.1f);
        _index[CurationIndexIdStandard] = @([(NSNumber *)_index[CurationIndexIdAwaken] integerValue] * 0.7f + [(NSNumber *)_index[CurationIndexIdStable] integerValue] * 0.1f);
        
        _elementBinaural[indexBinaural] = @(elemBB);
        _indexBinaural = indexBinaural;
        
        self.direction = 0;
        self.difference = 0;
        self.updated = NO;
    }
    return self;
}

-(void)setElement:(NSInteger)elemEnum elemValue:(NSInteger)value {
    _element[elemEnum] = @(value);
}

-(NSInteger)getElement:(NSInteger)elemEnum {
    return [(NSNumber *)_element[elemEnum] integerValue];
}

-(void)setIndex:(NSInteger)indexEnum indexValue:(NSInteger)value {
    _index[indexEnum] = @(value);
}

-(NSInteger)getIndex:(NSInteger)indexEnum {
    return [(NSNumber *)_index[indexEnum] integerValue];
}

-(void)setElementBinaural:(NSInteger)binauralEnum elemBBValue:(NSInteger)value {
    _elementBinaural[binauralEnum] = @(value);
}

-(NSInteger)getElementBinaural:(NSInteger)binauralEnum {
    return [(NSNumber *)_elementBinaural[binauralEnum] integerValue];
}

+(Record *)makeCloneWithRecord:(Record *)record; {
    Record *clone = [[Record alloc] init];
    
    [clone setDate:record.date];
    //
    [clone setElement:CurationElementIdTST elemValue:[record getElement:CurationElementIdTST]];
    [clone setElement:CurationElementIdGTBT elemValue:[record getElement:CurationElementIdGTBT]];
    [clone setElement:CurationElementIdSUN elemValue:[record getElement:CurationElementIdSUN]];
    [clone setElement:CurationElementIdWALK elemValue:[record getElement:CurationElementIdWALK]];
    [clone setElement:CurationElementIdCOFFEE elemValue:[record getElement:CurationElementIdCOFFEE]];
    [clone setElement:CurationElementIdSMOKE elemValue:[record getElement:CurationElementIdSMOKE]];
    [clone setElement:CurationElementIdDRINK elemValue:[record getElement:CurationElementIdDRINK]];
    [clone setElement:CurationElementIdBB elemValue:[record getElement:CurationElementIdBB]];
    
    [clone setIndex:CurationIndexIdAwaken indexValue:[record getIndex:CurationIndexIdAwaken]];
    [clone setIndex:CurationIndexIdCycle indexValue:[record getIndex:CurationIndexIdCycle]];
    [clone setIndex:CurationIndexIdStable indexValue:[record getIndex:CurationIndexIdStable]];
    [clone setIndex:CurationIndexIdTotal indexValue:[record getIndex:CurationIndexIdTotal]];
    [clone setIndex:CurationIndexIdStandard indexValue:[record getIndex:CurationIndexIdStandard]];
    
    [clone setElementBinaural:CurationBinauralId3Hz elemBBValue:[record getElementBinaural:CurationBinauralId3Hz]];
    [clone setElementBinaural:CurationBinauralId6Hz elemBBValue:[record getElementBinaural:CurationBinauralId6Hz]];
    [clone setElementBinaural:CurationBinauralId9Hz elemBBValue:[record getElementBinaural:CurationBinauralId9Hz]];
    [clone setElementBinaural:CurationBinauralId12Hz elemBBValue:[record getElementBinaural:CurationBinauralId12Hz]];
    
    clone.indexBinaural = record.indexBinaural;
    clone.direction = record.direction;
    clone.difference = record.difference;
    clone.updated = record.isUpdated;
    return clone;
}

-(NSString *)toString {
    NSMutableString *text = [NSMutableString string];
    [text appendFormat:@"%@\t\t", self.date];
    [text appendFormat:@"%ld\t\t", [(NSNumber *)_element[CurationElementIdTST] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_element[CurationElementIdGTBT] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_element[CurationElementIdSUN] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_element[CurationElementIdWALK] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_element[CurationElementIdCOFFEE] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_element[CurationElementIdSMOKE] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_element[CurationElementIdDRINK] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_element[CurationElementIdBB] integerValue]];
    
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_elementBinaural[CurationBinauralId3Hz] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_elementBinaural[CurationBinauralId6Hz] integerValue]];
    [text appendFormat:@"%ld\t\t\t",  [(NSNumber *)_elementBinaural[CurationBinauralId9Hz] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_elementBinaural[CurationBinauralId12Hz] integerValue]];
    
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_index[CurationIndexIdAwaken] integerValue]];
    [text appendFormat:@"%ld\t\t\t",  [(NSNumber *)_index[CurationIndexIdCycle] integerValue]];
    [text appendFormat:@"%ld\t\t\t",  [(NSNumber *)_index[CurationIndexIdStable] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_index[CurationIndexIdTotal] integerValue]];
    [text appendFormat:@"%ld\t\t",  [(NSNumber *)_index[CurationIndexIdStandard] integerValue]];
    
    return text;
}

@end
