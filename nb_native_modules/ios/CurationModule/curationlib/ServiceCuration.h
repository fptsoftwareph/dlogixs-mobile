//
//  ServiceCuration.h
//  CurationModule
//
//  Created by FPT Software on 24/07/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Record.h"
#import "CurationMessage.h"

@interface ServiceCuration : NSObject

-(instancetype)initServiceCuration:(NSMutableArray *)database data:(Record *)data;

-(void)selectModel;
-(CurationMessage *)getGuideMessage:(NSInteger)elemEnum;
-(Record *)getData;
-(Record *)getModel:(NSInteger)elemEnum;

@end
