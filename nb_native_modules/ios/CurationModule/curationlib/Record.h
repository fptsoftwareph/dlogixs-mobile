//
//  Record.h
//  CurationModule
//
//  Created by FPT Software on 24/07/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CurationElementId) {
    CurationElementIdTST = 0,
    CurationElementIdGTBT,
    CurationElementIdSUN,
    CurationElementIdWALK,
    CurationElementIdCOFFEE,
    CurationElementIdSMOKE,
    CurationElementIdDRINK,
    CurationElementIdBB
};

typedef NS_ENUM(NSInteger, CurationIndexId) {
    CurationIndexIdAwaken = 0,
    CurationIndexIdCycle,
    CurationIndexIdStable,
    CurationIndexIdTotal,
    CurationIndexIdStandard
};

typedef NS_ENUM(NSInteger, CurationBinauralId) {
    CurationBinauralId3Hz = 0,
    CurationBinauralId6Hz,
    CurationBinauralId9Hz,
    CurationBinauralId12Hz
};

@interface Record : NSObject

@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) NSInteger indexBinaural;
@property (nonatomic, assign) NSInteger direction;
@property (nonatomic, assign) NSInteger difference;
@property (nonatomic, assign, getter=isUpdated) BOOL updated;


-(instancetype)initWithDate:(NSString *)date
                 elementTST:(NSInteger)elemTST
                elementGTBT:(NSInteger)elemGTBT
                 elementSUN:(NSInteger)elemSun
                elementWALK:(NSInteger)elemWALK
              elementCOFFEE:(NSInteger)elemCOFFEE
               elementSMOKE:(NSInteger)elemSMOKE
               elementDRINK:(NSInteger)elemDRINK
                  elementBB:(NSInteger)elemBB
                  sqiAWAKEN:(NSInteger)indexAWAKEN
                   sqiCYCLE:(NSInteger)indexCYCLE
               sqiSTABILITY:(NSInteger)indexSTABILITY
              indexBinaural:(NSInteger)indexBinaural;

-(void)setElement:(NSInteger)elemEnum elemValue:(NSInteger)value;
-(NSInteger)getElement:(NSInteger)elemEnum;
-(void)setIndex:(NSInteger)indexEnum indexValue:(NSInteger)value;
-(NSInteger)getIndex:(NSInteger)indexEnum;
-(void)setElementBinaural:(NSInteger)binauralEnum elemBBValue:(NSInteger)value;
-(NSInteger)getElementBinaural:(NSInteger)binauralEnum;

+(Record *)makeCloneWithRecord:(Record *)record;
-(NSString *)toString;


@end
