//
//  CurationMessage.m
//  CurationModule
//
//  Created by FPT Software on 12/01/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "CurationMessage.h"

@interface CurationMessage() {
    NSString *_englishMsg;
    NSInteger _diff, _direction, _counter, _modelFreq;
}
@end

@implementation CurationMessage

-(instancetype)initWithMessage:(NSInteger)diff counter:(NSInteger)ctr {
    if (self = [super init]) {
        _diff = diff;
        _counter = ctr;
    }
    return self;
}

-(instancetype)initWithMessage:(NSInteger)diff counter:(NSInteger)ctr modelFreq:(NSInteger)freq {
    if (self = [self initWithMessage:diff counter:ctr]) {
        _modelFreq = freq;
    }
    return self;
}

-(instancetype)initWithMessage:(NSString *)englishMsg difference:(NSInteger)diff direction:(NSInteger)direction counter:(NSInteger)ctr {
    if (self = [self initWithMessage:diff counter:ctr]) {
        _englishMsg = englishMsg;
        _direction = direction;
    }
    return self;
}

-(instancetype)initWithMessage:(NSString *)englishMsg difference:(NSInteger)diff direction:(NSInteger)direction counter:(NSInteger)ctr modelFreq:(NSInteger)freq {
    if (self = [self initWithMessage:englishMsg difference:diff direction:direction counter:ctr]) {
        _modelFreq = freq;
    }
    return self;
}

-(NSString *)jsonizeString {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(_counter) forKey:@"counter"];
    [dict setObject:@(_diff) forKey:@"diff"];
    [dict setObject:@(_direction) forKey:@"direction"];
//    [dict setObject:_englishMsg forKey:@"english"];
    [dict setObject:@(_modelFreq) forKey:@"modelFreq"];
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:0 error:nil] encoding:NSUTF8StringEncoding];
}

@end
