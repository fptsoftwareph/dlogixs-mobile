//
//  CurationMessage.h
//  CurationModule
//
//  Created by FPT Software on 12/01/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurationMessage : NSObject

-(instancetype)initWithMessage:(NSString *)englishMsg difference:(NSInteger)diff direction:(NSInteger)direction counter:(NSInteger)ctr;
-(instancetype)initWithMessage:(NSString *)englishMsg difference:(NSInteger)diff direction:(NSInteger)direction counter:(NSInteger)ctr modelFreq:(NSInteger)freq ;
-(instancetype)initWithMessage:(NSInteger)diff counter:(NSInteger)ctr;
-(instancetype)initWithMessage:(NSInteger)diff counter:(NSInteger)ctr modelFreq:(NSInteger)freq;

-(NSString *)jsonizeString;

@end
