//
//  ServiceCuration.m
//  CurationModule
//
//  Created by FPT Software on 24/07/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "ServiceCuration.h"

@interface ServiceCuration() {
    NSMutableArray<Record *> *_database;
    Record *_data;
    NSMutableArray<Record *> *_model;
}
@end

@implementation ServiceCuration

-(instancetype)initServiceCuration:(NSMutableArray *)database data:(Record *)data {
    if (self = [super init]) {
        _database = [[NSMutableArray alloc] init];
        for (int i = 0; i < database.count; i++) {
            [_database addObject:[Record makeCloneWithRecord:[database objectAtIndex:i]]];
        }
        NSLog(@"initCuration dbLen: %ld", _database.count);
        _data = [Record makeCloneWithRecord:data];
        _model = [[NSMutableArray alloc] initWithCapacity:8];
    }
    return self;
}

-(void)selectModel {
    [self selectModel:CurationElementIdTST];
    [self selectModel:CurationElementIdGTBT];
    [self selectModel:CurationElementIdSUN];
    [self selectModel:CurationElementIdWALK];
    [self selectModel:CurationElementIdCOFFEE];
    [self selectModel:CurationElementIdSMOKE];
    [self selectModel:CurationElementIdDRINK];
    [self selectModel:CurationElementIdBB];
}

-(CurationMessage *)getGuideMessage:(NSInteger)elemEnum {
    NSInteger diff = ABS([_model objectAtIndex:elemEnum].difference);
    NSInteger modelFreq = [_model objectAtIndex:elemEnum].indexBinaural;
    BOOL isUpdated = [_model objectAtIndex:elemEnum].isUpdated;
    
    switch (modelFreq) {
        case CurationBinauralId3Hz:
            modelFreq = 3;
            break;
        case CurationBinauralId6Hz:
            modelFreq = 6;
            break;
        case CurationBinauralId9Hz:
            modelFreq = 9;
            break;
        case CurationBinauralId12Hz:
            modelFreq = 12;
            break;
    }
    
    switch (elemEnum) {
        case CurationElementIdTST:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:1];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:3];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:4];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:5];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:6];
            }
            break;
        case CurationElementIdGTBT:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:1];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:3];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:4];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:5];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:6];
            }
            break;
        case CurationElementIdSUN:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:1];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:3];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:4];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:5];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:6];
            }
            break;
        case CurationElementIdWALK:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:1];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:3];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:4];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:5];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:6];
            }
            break;
        case CurationElementIdCOFFEE:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:1];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:3];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:4];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:5];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:6];
            }
            break;
        case CurationElementIdSMOKE:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:1];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:3];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:4];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:5];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:6];
            }
            break;
        case CurationElementIdDRINK:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:1];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:3];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:4];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:5];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:6];
            }
            break;
        case CurationElementIdBB:
            if (!isUpdated) {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                    return [[CurationMessage alloc] initWithMessage:diff counter:2 modelFreq:modelFreq];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                     return [[CurationMessage alloc] initWithMessage:diff counter:1 modelFreq:modelFreq];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                     return [[CurationMessage alloc] initWithMessage:diff counter:3 modelFreq:modelFreq];
            } else {
                if ([_model objectAtIndex:elemEnum].direction < 0)
                     return [[CurationMessage alloc] initWithMessage:diff counter:4 modelFreq:modelFreq];
                if ([_model objectAtIndex:elemEnum].direction > 0)
                     return [[CurationMessage alloc] initWithMessage:diff counter:5 modelFreq:modelFreq];
                if ([_model objectAtIndex:elemEnum].direction == 0)
                     return [[CurationMessage alloc] initWithMessage:diff counter:6 modelFreq:modelFreq];
            }
            break;
    }
    return [[CurationMessage alloc] initWithMessage:0 counter:0 modelFreq:modelFreq];
}

-(Record *)getData {
    return _data;
}

-(Record *)getModel:(NSInteger)elemEnum {
    return [_model objectAtIndex:elemEnum];
}

#pragma mark -
#pragma mark === Private Method ===
#pragma mark -
-(void)selectModel:(NSInteger)elemEnum {
    NSMutableArray *valueDatabase = [[NSMutableArray alloc] initWithCapacity:_database.count];
    NSMutableArray *valueAbs = [[NSMutableArray alloc] initWithCapacity:_database.count];
    for (NSInteger i = 0; i < _database.count; i++) {
        valueDatabase[i] = @([[_database objectAtIndex:i] getElement:elemEnum]);
        valueAbs[i] = @(ABS([self rounding:[(NSNumber *)valueDatabase[i] integerValue] elemEnum:elemEnum] - [self rounding:[_data getElement:elemEnum] elemEnum:elemEnum]));
    }
    //    2. Select nCompares records closest to the element value of data in the database and copy them to the candidate
    NSInteger nCompares = 5;
    int *rank = (int *)malloc(sizeof(int) * _database.count);
    for (NSInteger i = 0; i < _database.count; i++) {
        rank[i] = 1;
        for (NSInteger j = 0; j < _database.count; j++) {
            if ([valueAbs[i] integerValue] > [valueAbs[j] integerValue]) {
                rank[i]++;
            }
        }
    }
    NSMutableArray<Record *> *candidate = [NSMutableArray new];
    if (_database.count <= nCompares) {
        for (NSInteger rankVal = 1; rankVal <= _database.count; rankVal++) { // rank_val = 1, 2, ... , database.size()
            for (NSInteger i = _database.count - 1; i >= 0; i--) { // When rank is the same, set back to search from the latest date
                //                if ([rank[i] integerValue] == rankVal) {
                if (rank[i] == rankVal) {
                    [candidate addObject:[Record makeCloneWithRecord:[_database objectAtIndex:i]]];
                }
            }
        }
    } else { // nCompares < database.size()
        for (NSInteger rankval = 1; rankval <= nCompares; rankval++) { // rank_val = 1, 2, ... , nCompares
            for (NSInteger i = _database.count - 1; i >= 0; i--) {
                //                if ([rank[i] integerValue] == rankval) {
                if (rank[i] == rankval) {
                    [candidate addObject:[Record makeCloneWithRecord:[_database objectAtIndex:i]]];
                }
            }
        }
    }
    //    3. Compare all candidate items to INDEX_STANDARD of data
    BOOL isCaseUpdated = YES;
    for (NSInteger i = 0; i < candidate.count; i++) {
        if ([[candidate objectAtIndex:i] getIndex:CurationIndexIdStandard] > [_data getIndex:CurationIndexIdStandard])
            isCaseUpdated = NO;
    }
    if (!isCaseUpdated) { // 4-1. Case.A: The largest INDEX_STANDARD value is detected in candidate (past data is RM, Not Updated)
        NSInteger maxIndexStd = NSIntegerMin;
        for (NSInteger i = 0; i < candidate.count; i++) {
            if ([[candidate objectAtIndex:i] getIndex:CurationIndexIdStandard] > maxIndexStd) {
                maxIndexStd = [[candidate objectAtIndex:i] getIndex:CurationIndexIdStandard];
                //                _model[elemEnum] = [Record makeCloneWithRecord:[candidate objectAtIndex:i]];
                _model[elemEnum] = [Record makeCloneWithRecord:[candidate objectAtIndex:i]];
            }
        }
        NSInteger valueModel = [self rounding:[[_model objectAtIndex:elemEnum] getElement:elemEnum] elemEnum:elemEnum];
        NSInteger valueData = [self rounding:[_data getElement:elemEnum] elemEnum:elemEnum];
        [_model objectAtIndex:elemEnum].difference = valueModel - valueData;
        if (valueModel > valueData) { // 4-1-1. Case.A1: When the element value of model is larger than the element value of data
            [_model objectAtIndex:elemEnum].direction = 1;
        } else if (valueModel < valueData) { // 4-1-2. Case.A2: When the element value of model is smaller than the element value of data
            [_model objectAtIndex:elemEnum].direction = -1;
        } else { // 4-1-3. Case.A3: When the element value of model is equal to the element value of data
            [_model objectAtIndex:elemEnum].direction = 0;
        }
        [_model objectAtIndex:elemEnum].updated = NO;
    } else { // 4-2. Case.B: Data has the highest INDEX_STANDARD value (current data is RM, updated)
        _model[elemEnum] = [Record makeCloneWithRecord:_data];
        NSInteger valueModel;
        NSInteger valueCandidateMax = NSIntegerMin;
        NSInteger valueCandidateMin = NSIntegerMax;
        for (NSInteger i = 0; i < candidate.count; i++) {
            if ([[candidate objectAtIndex:i] getElement:elemEnum] > valueCandidateMax) {
                valueCandidateMax = [[candidate objectAtIndex:i] getElement:elemEnum];
            }
            if ([[candidate objectAtIndex:i] getElement:elemEnum] < valueCandidateMin) {
                valueCandidateMin = [[candidate objectAtIndex:i] getElement:elemEnum];
            }
        }
        valueModel = [self rounding:[[_model objectAtIndex:elemEnum] getElement:elemEnum] elemEnum:elemEnum];
        valueCandidateMax = [self rounding:valueCandidateMax elemEnum:elemEnum];
        valueCandidateMin = [self rounding:valueCandidateMin elemEnum:elemEnum];
        [_model objectAtIndex:elemEnum].difference = 0;
        if (valueModel >= valueCandidateMax) { // 4-2-1. Case.B1: When the element value of model is larger than the value of all elements of cadidate
            
        } else if (valueModel <= valueCandidateMin) {
            [_model objectAtIndex:elemEnum].direction = -1;
        } else {
            [_model objectAtIndex:elemEnum].direction = 0;
        }
        [_model objectAtIndex:elemEnum].updated = YES;
    }
    free(rank);
}

-(NSInteger)timeRounding:(NSInteger)value {
    return (NSInteger)roundf(value/5.0f) * 5.0f;
}

-(NSInteger)step2TimeRounding:(NSInteger)step {
    return [self timeRounding:roundf(step/60.0f)];
}

-(NSInteger)rounding:(NSInteger)value elemEnum:(NSInteger)elemEnum {
    switch (elemEnum) {
        case CurationElementIdWALK:
            return [self step2TimeRounding:value];
        case CurationElementIdTST:
        case CurationElementIdGTBT:
        case CurationElementIdSUN:
            return [self timeRounding:value];
        case CurationElementIdCOFFEE:
        case CurationElementIdSMOKE:
        case CurationElementIdDRINK:
        case CurationElementIdBB:
            return value;
        default:
            return -2000;
    }
}

@end

