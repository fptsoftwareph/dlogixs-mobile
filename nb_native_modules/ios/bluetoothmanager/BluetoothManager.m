//
//  SessionManager.m
//  BleManager
//
//  Created by FPT Software on 06/11/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "BluetoothManager.h"

NSString *const PREFS_DEVICE_UUID = @"prefs_device_uuid";

@interface BluetoothManager()  {
    NSUserDefaults *_prefs;
}
@end

@implementation BluetoothManager

+(instancetype)sharedBluetoothMgr {
    static BluetoothManager *sharedSession = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedSession = [[BluetoothManager alloc] init];
    });
    return sharedSession;
}

-(void)initBTMgrWithDelegate:(id)delegate queue:(dispatch_queue_t)queue options:(NSMutableDictionary *)options {
    if (!self.btMgr) {
        _prefs = [NSUserDefaults standardUserDefaults];
        _btMgr = [[CBCentralManager alloc] initWithDelegate:delegate queue:queue options:options];
    }
}

-(void)connectPeripheral:(CBPeripheral *)peripheral {
    if (peripheral) {
        _peripheral = peripheral;
    }
}

- (void)saveDeviceUUID:(NSString *)uuid {
    [_prefs setObject:uuid forKey:PREFS_DEVICE_UUID];
}

- (NSString *)getSaveUUID {
    return [_prefs objectForKey:PREFS_DEVICE_UUID];
}

- (BOOL)isLoggedIn {
    return ([_prefs objectForKey:PREFS_DEVICE_UUID] != nil)?YES:NO;
}

- (void)logout {
    [_prefs removeObjectForKey:PREFS_DEVICE_UUID];
}

@end
