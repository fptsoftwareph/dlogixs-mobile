//
//  SessionManager.h
//  BleManager
//
//  Created by FPT Software on 06/11/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BluetoothManager : NSObject

@property (readonly, strong, nonatomic) CBCentralManager *btMgr;
@property (readonly, strong, nonatomic) CBPeripheral *peripheral;

+ (instancetype)sharedBluetoothMgr;
-(void)initBTMgrWithDelegate:(id)delegate queue:(dispatch_queue_t)queue options:(NSMutableDictionary *)options;
-(void)connectPeripheral:(CBPeripheral *)peripheral;

- (void)saveDeviceUUID:(NSString *)uuid;
- (NSString *)getSaveUUID;
- (BOOL)isLoggedIn;
- (void)logout;

@end
