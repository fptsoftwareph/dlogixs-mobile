//
//  UARTUtils.h
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UARTUtils : NSObject

+(NSString*)byteArrayToHex:(Byte*)a_Data Len:(int)a_Len;
+(NSString*)byteArrayToHex:(Byte*)a_Data Len:(int)a_Len Space:(BOOL)a_UseSpace;
+(NSString*)NSDataToIntString:(NSData*)data;
+(id)respFromUARTPeripheral:(NSData*)aData;
+(Byte)checkSum:(Byte*)a_Val offset:(int)a_Offset len:(int)a_Len;
+(double)littleEndianBytesToDouble:(Byte*)bytes;

@end
