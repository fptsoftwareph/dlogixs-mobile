

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define AppFont(pt) [UIFont systemFontOfSize:pt]
#define BoldFont(pt) [UIFont boldSystemFontOfSize:pt]

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define RGBTintColor() [UIColor colorWithRed:kGrayTintRed/255.0 green:kGrayTintGreen/255.0 blue:kGrayTintBlue/255.0 alpha:1]

//#define SIMULATION          YES
//#define ENCRYPT             NO

#define kThemeColor         RGB(0x24, 0x97, 0xe3)

#define kDefBlack           RGB(61, 61, 61)
#define kDefWhite           RGB(0xff, 0xff, 0xff)
#define kDefDarkGray        RGB(80, 80, 80)
#define kDefLightGray       RGB(157, 157, 157)
#define kDefBackgroundGray  RGB(0xf2, 0xef, 0xe8)
#define kDefGreen           RGB(104, 159, 56)
#define kDefRed             RGB(0xc4, 0, 0)
#define kDefBlue            RGB(0, 0, 0xc4)
#define kDefBlack           RGB(61, 61, 61)
#define kDefLightRed        RGB(231, 49, 62)
#define kDefLightBlue       RGB(38, 144, 232)
#define kMainMenuBG         RGB(0xff, 0xc0, 0x00)
#define kMainMenuItemBG     RGB(0xff, 0xae, 0x00)
#define kMainMenuItemBGOn   RGB(0xf2, 0xa5, 0x00)
#define kMenuBackgroudGray  RGB(0xfa, 0xfa, 0xfa)
#define kMenuLineGray       RGB(0xb2, 0xac, 0xa1)
#define kCancelGray         RGB(0x9d, 0x94, 0x8d)
#define kConfirmGray        RGB(0x72, 0x69, 0x5c)

#define kTextBlack          RGB(0x1b, 0x1b, 0x1b)
#define kTextGray           RGB(0x86, 0x85, 0x81)
#define kTextYellow         RGB(0xff, 0xae, 0x00)


#define sURLBase                @"http://www.big.kr.pe"
#define sUrlJSON                sURLBase@"/embroadcast/api/"
#define sInAuth                 @"test"


#define sAppName                @"EMBroadcast"
#define sDefTitle               @"EMBroadcast"

#define kIPhone5H               568
#define kIPhone4H               480

#define kNavBarHeight           44
#define kNavBarH                kNavBarHeight
#define kStatusBarHeight        20
#define kStatusBarH             kStatusBarHeight


#define kOFFSET_FOR_KEYBOARD    80.0

  // SLEEP_ANALYSIS
#define SLEEP_ANALYSIS_POINTS   @"SLEEP_ANALYSIS_POINTS"
#define SLEEP_ANALYSIS_TOTAL    @"SLEEP_ANALYSIS_TOTAL"
#define SLEEP_ANALYSIS_LATENCY  @"SLEEP_ANALYSIS_LATENCY"
#define SLEEP_ANALYSIS_WAKE     @"SLEEP_ANALYSIS_WAKE"
#define SLEEP_EFFICIENCY        @"SLEEP_EFFICIENCY"
#define SLEEP_REM_LATENCY       @"SLEEP_REM_LATENCY"
#define SLEEP_REM_EFFICIENCY    @"SLEEP_REM_EFFICIENCY"
#define SLEEP_CYCLE_COUNT       @"SLEEP_CYCLE_COUNT"
#define SLEEP_SQI_STABILITY     @"SLEEP_SQI_STABILITY"
#define SLEEP_SQI_AWAKEN        @"SLEEP_SQI_AWAKEN"
#define SLEEP_SQI_CYCLE         @"SLEEP_SQI_CYCLE"
#define SLEEP_STABILITY_BRAINWAVE @"SLEEP_STABILITY_BRAINWAVE"
#define SLEEP_HYPNOGRAM         @"SLEEP_HYPNOGRAM"
#define SLEEP_WITH_DATA         @"SLEEP_WITH_DATA"
#define SLEEP_GTBT              @"SLEEP_GTBT"

 // CURATION_ANALYSIS
#define CURATION_MODEL_DATE     @"CURATION_MODEL_DATE"
#define CURATION_DATA_DATE      @"CURATION_DATA_DATE"
#define CURATION_ELEMENT_MODEL  @"CURATION_ELEMENT_MODEL"
#define CURATION_ELEMENT_DATA   @"CURATION_ELEMENT_DATA"
#define CURATION_DIFFERENCE     @"CURATION_DIFFERENCE"
#define CURATION_DIRECTION      @"CURATION_DIRECTION"
#define CURATION_MESSAGE        @"CURATION_MESSAGE"
#define CURATION_IS_UPDATED     @"CURATION_IS_UPDATED"

 // REACHABILITY
#define REACHABILITY_REQUIRED   @"REACHABILITY_REQUIRED"
#define REACHABILITY_MODE       @"REACHABILITY_MODE"
#define REACHABILITY_TYPE       @"REACHABILITY_TYPE"
