//
//  UARTUtils.m
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "UARTUtils.h"
#import "StationUARTManager.h"

@implementation UARTUtils
+(NSString*)byteArrayToHex:(Byte*)a_Data Len:(int)a_Len
{
    return [self byteArrayToHex:a_Data Len:a_Len Space:YES];
}

+(NSString*)byteArrayToHex:(Byte*)a_Data Len:(int)a_Len Space:(BOOL)a_UseSpace
{
    return [self byteArrayToHex:a_Data Start:0 Len:a_Len Space:a_UseSpace];
}

+(NSString*)byteArrayToHex:(Byte*)a_Data Start:(int)a_Start Len:(int)a_Len Space:(BOOL)a_UseSpace
{
    NSString *aHex = @"";
    
    for (int i = 0; i < a_Len; i++) {
        if (a_UseSpace) {
            aHex = [NSString stringWithFormat:@"%@ 0x%02x", aHex, a_Data[a_Start+i]];
        } else {
            aHex = [NSString stringWithFormat:@"%@0x%02x", aHex, a_Data[a_Start+i]];
        }
    }
    return aHex;
}

+(id)respFromUARTPeripheral:(NSData*)aData {
    int wLen = (int)aData.length;
//    NSLog(@"respFromUARTPeripheral() Length: %d", wLen);
    Byte *dBytes = (Byte*)[aData bytes];
    if (wLen <= 3)
        return @"Invalid Data Format";
    Byte wChksum = [UARTUtils checkSum:dBytes offset:1 len:wLen-3];
    if (((dBytes[0] & 0xFF) != CMD_STX) || ((dBytes[wLen-1] & 0xFF) != CMD_ETX))
        return @"STX, ETX Error";
    if ( (dBytes[wLen-2] & 0xFF) != (wChksum & 0xFF))
        return @"Checksum Error";
    return aData;
}

+(NSString*)NSDataToIntString:(NSData*)data {
    const unsigned char *dbytes = (Byte *)[data bytes];
    NSMutableString *hexStr = [NSMutableString stringWithCapacity:[data length]*2];
    int i;
    for (i = 0; i < [data length]; i++) {
        [hexStr appendFormat:@"%02d", dbytes[i]];
    }
    return [NSString stringWithString: hexStr];
}

+(Byte)checkSum:(Byte*)a_Val offset:(int)aOffset len:(int)aLen {
    Byte wSum = 0x00;
    for(int wNdx = 0; wNdx < aLen; wNdx++)
        wSum += (Byte)a_Val[aOffset + wNdx];
    return ((Byte)(0x100 - (int)(wSum &0xff)));
}

+(double)littleEndianBytesToDouble:(Byte*)bytes {
    int bits = ((bytes[0] & 0xFF) << 24) |
    ((bytes[1] & 0xFF) << 16)  |
    ((bytes[2] & 0xFF) << 8) |
    (bytes[3] & 0xFF);
    delete [] bytes;
    return [self convertFloatToDouble:[self intBitsToFloat:bits]];
}

+(float)intBitsToFloat:(int)bits {
    int s = ((bits >> 31) == 0) ? 1 : -1;
    int e = ((bits >> 23) & 0xff);
    int m = (e == 0) ? (bits & 0x7fffff) << 1 : (bits & 0x7fffff) | 0x800000;
    return s * m * pow(2,e -150);
}

+(double)convertFloatToDouble:(float)f {
    char buf[42];
    sprintf(buf, "%.6g", f);
    return atof(buf);
}

@end
