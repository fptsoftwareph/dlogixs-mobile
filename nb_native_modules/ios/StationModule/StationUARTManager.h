//
//  StationUARTManager.h
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#define CMD_STX     0x02
#define CMD_ETX     0x03
#define BUFF_LEN    10
#define WIFI_PACKET_LEN 60
#define WIFI_DATA_LEN 52

typedef NS_OPTIONS(NSUInteger, DLStationCommand) {
    DLStationCommandVolume                 = 0xA2,
    DLStationCommandLight                  = 0xA0,
    DLStationCommandBinauralBeat           = 0xA6,
    DLStationCommandAudioInput             = 0xAA,
    DLStationCommandPlayerCtrl             = 0xAC,
    DLStationCommandBattery                = 0xA4,
    DLStationCommandEEG                    = 0xAD,
    DLStationCommandSleepInfo              = 0xAE,
    DLStationCommandWifi                   = 0xB0,
    DLStationCommandHeadset                = 0x16,
    DLStationCommandDevEquipmentState      = 0xDC
};

typedef NS_ENUM(NSInteger, DLStationInfoMode) {
    DLStationInfoModeControl                = 0,
    DLStationInfoModeBattery,
    DLStationInfoModeSleepInfo,
    DLStationInfoModeHeadset,
    DLStationInfoModeVolume,
    DLStationInfoModeLight,
    DLStationInfoModeBinaural,
    DLStationInfoModeDevEquipment
};

typedef NS_ENUM(NSInteger, DLStationEEGInfo) {
    DLStationEEGInfoAttention          = 3,
    DLStationEEGInfoMeditation,
    DLStationEEGInfoBand
};

@protocol StationUARTDelegate
@required
-(void)didBluetoothUpdateState:(CBCentralManager *)central;
@optional
-(void)didStationParsingWithMode:(DLStationInfoMode)mode data:(id)data;
-(void)didParsingEEGWithMode:(DLStationEEGInfo)mode data:(id)data;
-(void)didScannedPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI;
-(void)didStopScanning;
-(void)didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error;
-(void)didConnectPeripheral:(CBPeripheral *)peripheral;
@end

@interface StationUARTManager : NSObject

@property (readonly, strong, nonatomic) CBCentralManager *stationManager;
@property (readonly, strong, nonatomic) CBPeripheral *bluetoothPeripheral;
@property (weak, nonatomic) id<StationUARTDelegate> delegate;

+(instancetype)sharedStationUARTMgr;
-(void)initStationWithQueue:(dispatch_queue_t)queue options:(NSMutableDictionary *)options;
-(void)scanBluetoothWithTime:(float)time services:(NSArray<CBUUID *> *)services options:(NSDictionary *)options;
-(void)stopScan;
-(void)connectPeripheral:(CBPeripheral *)peripheral connectCallback:(NSMutableDictionary *)connectCallback;
-(id )retrievePeripheralWithUUID:(NSString *)UUIDString;
-(void)disconnectPeripheral:(CBPeripheral *)peripheral disconnectCallback:(NSMutableDictionary *)disconnectCallback;
-(void)writeRXCharacteristic:(NSData *)data;

-(void)setLight:(int)pLight;
-(void)setVolume:(int)pVol;
-(void)setBinauralBeat:(int)pBB;
-(void)setAudioInput:(int)pAudInp;
-(void)setAudioControl1:(int)pControl;
-(void)setAudioControl2:(int)pCtrl1 pCtrl2:(int)pCtrl2;
-(void)setWifiWithSSID:(NSString *)ssid passphrase:(NSString *)passphrase;
-(void)deleteWifi;
-(void)queryPeripheralEquipmentStatus;

-(NSString *)getSaveUUID;
-(BOOL)isLoggedIn;

@end
