//
//  SleepAnalysis.m
//  SleepAnalysis
//
//  Created by FPT Software on 12/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import "SleepAnalysis.h"
#import "EEGData.h"
#import "EEGAnalysis.h"

static const int STATE_REM = 4;
static const int STATE_WAKE = 5;
static const int STATE_OFF = 6;

static const int STANDARD_TST = 420;
static const int STANDARD_SL = 60;
static const int STANDARD_WASO = 60;
static const int STANDARD_RL1 = 30;
static const int STANDARD_RL2 = 60;
static const int STANDARD_RL3 = 120;
static const int STANDARD_RL4 = 180;
static const int STANDARD_RE1 = 5;
static const int STANDARD_RE2 = 20;
static const int STANDARD_RE3 = 30;
static const int STANDARD_RE4 = 45;

@interface SleepAnalysis() {
    EEGData *_eegData;
    EEGAnalysis *_eegAnalysis;
    int _L;
    int *_hypnogram;
    double *_signalMin;
    
    int _timeAcq;
    int _timeOff;
    double _lossRate;
    
    double _eTST;
    double _eSL;
    double _eSE;
    double _eWASO;
    double _eRL;
    double _eRE;
    double _eSCC;
    double _eSWW;
    double _eSBW;
    
    double _indexAwaken;
    double _indexSleepCycle;
    double _indexStability;
    double _indexTotal;
    
    int _idxFallAsleep;
    
    int _totalSleepTime;
    int _sleepLatency;
    double _sleepEfficiency;
    int _wakeAfterSleepOnset;
    int _REMLatancy;
    double _REMEfficiency;
    int _sleepCycleCount;
    int _stateWhenWake;
    double _stabilityBrainWave;
}
@end

@implementation SleepAnalysis

-(instancetype)init {
    if (self = [super init]) {
        _totalSleepTime = 0;
        _sleepLatency = 0;
        _sleepEfficiency = 0;
        _wakeAfterSleepOnset = 0;
        _REMLatancy = 0;
        _REMEfficiency = 0;
        _sleepCycleCount = 0;
        _stateWhenWake = 0;
        _stabilityBrainWave = 0;
    }
    return self;
}

-(instancetype)initWithEEGData:(EEGData *)eegData EEGAnalysis:(EEGAnalysis *)eegAnalysis {
    if (self = [self init]) {
        _eegData = eegData;
        _signalMin = [_eegData getSignalMin];
        _eegAnalysis = eegAnalysis;
        _L = [_eegAnalysis getLength];
        _hypnogram = [_eegAnalysis getHypnogram];
        
        for(int i = 0; i < _L; i++) {
            if(_hypnogram[i] == STATE_OFF)
                _timeOff++;
        }
        _timeAcq *= 5;
        _timeOff *= 5;
        _lossRate = 1.0f * _timeOff / _timeAcq * 100.0f;
        
        [self recalculateAll];
    }
    return self;
}

-(void)recalculateAll {
    [self calcElementTotalSleepTime];
    [self calcElementSleepLatency];
    [self calcElementSleepEfficiency];
    [self calcElementWakeAfterSleepOnset];
    
    [self calcElementREMLatency];
    [self calcElementREMEfficiency];
    [self calcElementSleepCycleCount];
    [self calcElementStateWhenWake];
    [self calcElementStabilityOfBrainwave];
    
    [self calcIndexAwaken];
    [self calcIndexSleepCycle];
    [self calcIndexStability];
    [self calcIndexTotal];
}

-(void)calcElementTotalSleepTime {
    double TST = _L * 5.0f;
    _totalSleepTime = (int)TST;
    _eTST = fabs(STANDARD_TST - fabs(STANDARD_TST - TST)) / STANDARD_TST * 100.0f;
    if (TST > STANDARD_TST * 2)
        _eTST = 0;
}

-(void)calcElementSleepLatency {
    double SL = 0.0f;
    _idxFallAsleep = 0;
    int i;
    for (i = 0; i < _L; i++) {
        if ((_hypnogram[i] != STATE_OFF) && (_hypnogram[i] != STATE_WAKE)) {
            SL = (double)i * 5.0f;
            _idxFallAsleep = i+1;
            break;
        }
    }
    _sleepLatency = (int)SL;
    _eSL = (1.0f - (SL/(double)STANDARD_SL)) * 100.0f;
    if (SL > STANDARD_SL)
        _eSL = 0.0f;
}

-(void)calcElementSleepEfficiency {
    double TST = _L * 5.0f;
    int SW = 0, i;
    for (i = 0; i < _L; i++) {
        if (_hypnogram[i] == STATE_WAKE)
            SW++;
    }
    SW *= 5;
    _eSE = (TST - (double)SW) / TST * 100.0f;
    _sleepEfficiency = _eSE;
}

-(void)calcElementWakeAfterSleepOnset {
    int WASO = 0, i;
    for (i = _idxFallAsleep; i < _L; i++) {
        if (_hypnogram[i] == STATE_WAKE)
            WASO++;
    }
    WASO *= 5;
    _wakeAfterSleepOnset = WASO;
    _eWASO = (1.0 - ((double)WASO / STANDARD_WASO)) * 100.0f;
}

-(void)calcElementREMLatency {
    double RL = 0.0f;
    int i;
    for (i = _idxFallAsleep; i < _L; i++) {
        if (_hypnogram[i] == STATE_REM) {
            RL = i * 5.0f;
            break;
        }
    }
    _REMLatancy = (int)RL;
    if (RL < STANDARD_RL1) {
        _eRL = 0.0;
    } else if (RL < STANDARD_RL2) {
        _eRL = (RL - STANDARD_RL1) / (STANDARD_RL2 - STANDARD_RL1) * 100.0f;
    } else if (RL < STANDARD_RL3) {
        _eRL = 100.0;
    } else if (RL < STANDARD_RL4) {
        _eRL = (1.0 - (RL - STANDARD_RL3) / (STANDARD_RL4 - STANDARD_RL3)) * 100.0f;
    } else {
        _eRL = 0.0f;
    }
}

-(void)calcElementREMEfficiency {
    double TST = _L * 5.0f;
    double SL = 0.0f;
    int i;
    for (i = 0; i < _L; i++) {
        if ((_hypnogram[i] != STATE_OFF) && (_hypnogram[i] != STATE_WAKE)) {
            SL = (double)i *5.0f;
            break;
        }
    }
    int RT = 0;
    for (i = _idxFallAsleep; i < _L; i++) {
        if (_hypnogram[i] == STATE_REM)
            RT++;
    }
    RT *= 5;
    double RE = RT / (TST - SL) * 100.0f;
    _REMEfficiency = RE;
    if (RE < STANDARD_RE1) {
        _eRE = 0.0f;
    } else if (RE < STANDARD_RE2) {
        _eRE = (RE - STANDARD_RE1) / (STANDARD_RE2 - STANDARD_RE1) * 100.0f;
    } else if (RE < STANDARD_RE3) {
        _eRE = 100.0f;
    } else if (RE < STANDARD_RE4) {
        _eRE = (1.0f - (RE - STANDARD_RE3) / (STANDARD_RE4 - STANDARD_RE3)) * 100.0f;
    } else {
        _eRE = 0.0f;
    }
}

-(void)calcElementSleepCycleCount {
    int SCC = 0, i;
    for (i = 1; i < _L; i++) {
        if ((_hypnogram[i-1] != STATE_REM) && (_hypnogram[i] == STATE_REM))
            SCC++;
    }
    _sleepCycleCount = SCC;
    _eSCC = (2.5f - fabs(SCC - 4.5)) * 50.0;
    if (_eSCC < 0)
        _eSCC = 0.0f;
}

-(void)calcElementStateWhenWake {
    int SWW = 0;
    if (_hypnogram[_L-1] == STATE_REM)
        SWW = 1;
    _stateWhenWake = SWW;
    _eSWW = SWW * 100.0f;
    free(_hypnogram);
}

-(void)calcElementStabilityOfBrainwave {
    int *signalTmp1, *signalTmp2, stableTime, i;
    signalTmp1 = (int *)malloc(sizeof(int) * DATA_BUFFER_LEN);
    stableTime = 120;
    for (i = 0; i < DATA_BUFFER_LEN; i++) {
        signalTmp1[i] = (_signalMin[i] < -0.15) ? 1 : 0;
    }
    signalTmp2 = [self morphologyOpen:signalTmp1 windowSize:2];
    for (i = 0; i < DATA_BUFFER_LEN; i++) {
        if (signalTmp2[i] == 1) {
            stableTime = i + 2;
            break;
        }
    }
    double minVal = DBL_MAX;
    for (i = 0; i < stableTime; i++) {
        if (_signalMin[i] < minVal)
            minVal = _signalMin[i];
    }
    stableTime++;
    double stableDegree = -1.0 * minVal;
    double param = stableDegree/stableTime;
    _stabilityBrainWave = param;
    _eSBW = param * 100.0f / 0.15f;
    if (param > 0.15) {
        _eSBW = 100.0;
    }
    free(signalTmp2);
}

-(int *)morphologyOpen:(int *)x windowSize:(int)windowSize {
    int length, *tmp, *y;
    length = DATA_BUFFER_LEN;
    tmp = [self morphologyErosion:x windowSize:windowSize];
    y = [self morphologyDilation:tmp windowSize:windowSize];
    return y;
}

-(int *)morphologyDilation:(int *)x windowSize:(int)windowSize {
    int i, j, length, *y;
    length = DATA_BUFFER_LEN;
    y = (int *)malloc(sizeof(int) * length);
    for (int k = 0; k < length; k++) {
        y[k] = 0;
    }
    for (i = 0; i < length; i++) {
        y[i] = x[i];
    }
    int lengthDiff = length-windowSize;
    for (i = 0; i < lengthDiff; i++) {
        if (x[i] == 0)
            continue;
        int iSum = i+windowSize;
        for (j = i; j < iSum; j++) {
            y[j] = 1;
        }
    }
    for (i = length-windowSize; i < length; i++) {
        if (x[i] == 0)
            continue;
        for (j = i; j < length; j++) {
            y[j] = 1;
        }
    }
    free(x);
    return y;
}

-(int *)morphologyErosion:(int *)x windowSize:(int)windowSize {
    int i, j, length, *y;
    length = DATA_BUFFER_LEN;
    y = (int *)malloc(sizeof(int) * length);
    for (int k = 0; k < length; k++) {
        y[k] = 0;
    }
    for (i = 0; i < length; i++) {
        y[i] = x[i];
    }
    for (i = 0; i < windowSize; i++) {
        if (x[i] == 1)
            continue;
        for (j = 0; j < i; j++) {
            y[j] = 0;
        }
    }
    for (i = windowSize; i < length; i++) {
        if (x[i] == 1)
            continue;
        for (j = i - windowSize; j < i; j++) {
            y[j] = 0;
        }
    }
    free(x);
    return y;
}

-(void)dealloc {
    
}

-(void)calcIndexAwaken {
    _indexAwaken = (0.25f * _eTST) + (0.25f * _eSL) + (0.25f * _eSE) + (0.25f * _eWASO);
}

-(void)calcIndexSleepCycle{
    _indexSleepCycle = (0.25f * _eRL) + (0.25f * _eRE) + (0.25f * _eSCC) + (0.25f * _eSWW);
}

-(void)calcIndexStability{
    _indexStability = _eSBW;
}

-(void)calcIndexTotal{
    _indexTotal = (0.7f * _indexAwaken) + (0.2f * _indexSleepCycle) + (0.1f * _indexStability);
}

-(double)getElementTotalSleepTime{
    return _eTST;
}

-(double)getElementSleepLatency {
    return _eSL;
}


-(double)getElementSleepEfficiency {
    return _eSE;
}


-(double)getElementWakeAfterSleepOnset {
    return _eWASO;
}


-(double)getElementREMLatency {
    return _eRL;
}


-(double)getElementREMEfficiency {
    return _eRE;
}


-(double)getElementSleepCycleCount {
    return _eSCC;
}


-(double)getElementStateWhenWake {
    return _eSWW;
}


-(double)getElementStabilityOfBrainWave {
    return _eSBW;
}


-(double)getIndexAwaken {
    return _indexAwaken;
}


-(double)getIndexSleepCycle {
    return _indexSleepCycle;
}


-(double)getIndexStability {
    return _indexStability;
}

-(double)getIndexTotal {
    return _indexTotal;
}

-(double)getDataLossRate {
    return _lossRate;
}

-(int)getTotalSleepTime {
    return _totalSleepTime;
}

-(int)getSleepLatency {
    return _sleepLatency;
}

-(double)getSleepEfficiency {
    return _sleepEfficiency;
}

-(int)getWakeAfterSleepOnset {
    return _wakeAfterSleepOnset;
}

-(int)getREMLatency {
    return _REMLatancy;
}

-(double)getREMEfficiency {
    return _REMEfficiency;
}

-(int)getSleepCycleCount {
    return _sleepCycleCount;
}

-(int)getStateWhenWake {
    return _stateWhenWake;
}

-(double)getStabilityBrainWave {
    return _stabilityBrainWave;
}

@end
