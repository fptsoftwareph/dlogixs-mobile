//
//  EEGData.h
//  SleepAnalysis
//
//  Created by FPT Software on 12/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EEGData : NSObject

-(instancetype)initEEGData:(long long *)time_acq signalMin:(double*)signalMin signalPoor:(int*)signalPoor duration:(int)durationInMin;

-(long long*)getTimeAcq;
-(double*)getSignalMin;
-(int*)getSignalPoor;
-(int)getDurationInMin;

@end

