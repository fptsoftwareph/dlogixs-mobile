//
//  EEGAnalysis.m
//  SleepAnalysis
//
//  Created by FPT Software on 13/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import "EEGAnalysis.h"
#import "EEGData.h"

static const int MAF_ORDER = 30;
static const double QUANTIZATION_LEVEL = 0.05f;

@interface EEGAnalysis() {
    int _timerMin;
    long long *_timeAcq;
    double *_signalMin;
    int *_signalPoor;
    
    long long *_timeAcq5min;
    double *_signalMaf;
    double *_signalAqu;
    int *_signalValley;
    double *_signalHeight;
    int *_signalRem;
    int *_signalDeep;
    int *_signalWake;
    int *_signalOff;
    int *_sleepStages;
    int *_hypnogram;
    int _L;
}
@end

@implementation EEGAnalysis

-(instancetype)initWithEEGData:(EEGData *)eegData {
    if(self = [super init]){
        _timerMin = [eegData getDurationInMin];
        
        _timeAcq = [eegData getTimeAcq];
        _signalMin = [eegData getSignalMin];
        _signalPoor = [eegData getSignalPoor];
        _hypnogram = (int*)malloc(sizeof(int) * SIGNAL_BUFFER_LEN);
        _L = 0;
        
        _signalMaf = [self mafFilter:_signalMin];
        _signalAqu = [self adaptiveQuantize:_signalMaf];
        _signalValley = [self detectValley:_signalAqu];
        _signalHeight = [self calculateHeight:_signalAqu y:_signalValley];
        _signalRem = [self detectRem:_signalHeight];
        _signalRem = [self calibrateSignal:_signalRem windowSize:10];
        _signalDeep = [self detectDeep:_signalHeight];
        _signalDeep = [self calibrateSignal:_signalDeep windowSize:10];
        _signalWake = [self detectWake:_signalMin];
        _signalWake = [self calibrateSignal:_signalWake windowSize:3];
        _signalOff = [self detectOff:_signalMin];
        _signalOff = [self calibrateSignal:_signalOff windowSize:3];
        _sleepStages = [self integrateStage:_signalRem y:_signalDeep z:_signalWake w:_signalOff];
        _L = [self postProcess:_sleepStages y:_signalPoor z:_hypnogram];
        _timeAcq5min = [self compressTime:_timeAcq];
    }
    return self;
}

-(void)dealloc {
    
}

- (double *)mafFilter:(double *)x {
    int i, j;
    double *y = (double*)malloc(sizeof(double) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < MAF_ORDER; i++) {
        for (j = 0; j < (i+1); j++) {
            y[i] += x[j];
        }
        y[i] /= (i+1);
    }
    for (i = MAF_ORDER; i < _timerMin; i++) {
        for (j = 0; j < MAF_ORDER; j++) {
            y[i] += x[i-j];
        }
        y[i] /= MAF_ORDER;
    }
    return y;
}

- (double *)adaptiveQuantize:(double *)x {
    int i;
    double preValue = x[0];
    double *y = (double*)malloc(sizeof(double) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        if (fabs(x[i] - preValue) > QUANTIZATION_LEVEL)
            preValue = x[i];
        y[i] = preValue;
    }
    free(x);
    return y;
}

- (int *)detectValley:(double *)x {
    int i;
    int state = 1; //0: Decrease, 1: Increase
    int *y = (int*)malloc(sizeof(int) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 1; i < _timerMin; i++) {
        if ((x[i-1] < x[i]) && (state == 0)) {
            state = 1;
            y[i-1] = 1;
        }
        if ((x[i-1] > x[i]) && (state == 1))
            state = 0;
    }
    return y;
}

-(double *)calculateHeight:(double *)x y:(int *)y {
    int i;
    double preValue, tendency, *z;
    preValue = tendency = 0.0f;
    z = (double*)malloc(sizeof(double) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        z[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        tendency += x[i];
    }
    tendency /= _timerMin;
    preValue = tendency;
    for (i = 0; i < _timerMin; i++) {
        if (y[i] != 0)
            preValue = x[i];
        z[i] = x[i] - preValue;
    }
    free(x);
    free(y);
    return z;
}

-(int *)detectRem:(double *)x {
    int i, *y;
    y = (int*)malloc(sizeof(int) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        y[i] = (x[i] > 0.15f) ? 1 : 0;
    }
    return y;
}

-(int *)detectDeep:(double *)x {
    int i, *y;
    y = (int*)malloc(sizeof(int) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        y[i] = (x[i] < -0.1f) ? 1 : 0;
    }
    free(x);
    return y;
}

-(int *)morphologyDilation:(int *)x windowSize:(int)windowSize {
    int i, j, *y;
    y = (int*)malloc(sizeof(int) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        y[i] = x[i];
    }
    int timerWindowDiff = _timerMin - windowSize;
    for (i = 0; i < timerWindowDiff; i++) {
        if (x[i] == 0)
            continue;
        int iSum = i + windowSize;
        for (j = i; j < iSum; j++) {
            y[j] = 1;
        }
    }
    for (i = timerWindowDiff; i < _timerMin; i++) {
        if (x[i] == 0)
            continue;
        for (j = i; j < _timerMin; j++) {
            y[j] = 1;
        }
    }
    free(x);
    return y;
}

-(int *)morphologyErosion:(int *)x windowSize:(int)windowSize {
    int i, j, *y;
    y = (int*)malloc(sizeof(int) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        y[i] = x[i];
    }
    for (i = 0; i < windowSize; i++) {
        if (x[i] == 1)
            continue;
        for (j = 0; j < i; j++) {
            y[j] = 0;
        }
    }
    for (i = windowSize; i < _timerMin; i++) {
        if (x[i] == 1)
            continue;
        int iDiff = i - windowSize;
        for (j = iDiff; j < i; j++) {
            y[j] = 0;
        }
    }
    free(x);
    return y;
}

-(int *)morphologyClose:(int *)x windowSize:(int)windowSize {
    int *y, *tmp;
    tmp = [self morphologyDilation:x windowSize:windowSize];
    y = [self morphologyErosion:tmp windowSize:windowSize];
    return y;
}

-(int *)morphologyOpen:(int *)x windowSize:(int)windowSize {
    int *y, *tmp;
    tmp = [self morphologyErosion:x windowSize:windowSize];
    y = [self morphologyDilation:tmp windowSize:windowSize];
    return y;
}

-(int *)calibrateSignal:(int *)x windowSize:(int)windowSize {
    int *y, *tmp;
    tmp = [self morphologyClose:x windowSize:windowSize];
    y = [self morphologyOpen:tmp windowSize:windowSize];
    return y;
}

-(int *)detectWake:(double *)x {
    int i, *y;
    y = (int*)malloc(sizeof(int) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        if (x[i] > 1.0f)
            y[i] = 1;
    }
    return y;
}

-(int *)detectOff:(double *)x {
    int i, *y;
    y = (int*)malloc(sizeof(int) * DATA_BUFFER_LEN);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        y[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        if (x[i] > 2.0f)
            y[i] = 1;
    }
    return y;
}

-(int *)integrateStage:(int *)x y:(int *)y z:(int *)z w:(int *)w {
    int i, sidx, *s;
    sidx = (int)((MAF_ORDER + 1.0f)/2.0f);
    s = (int*)malloc(sizeof(int) * 1440);
    for (int k = 0; k < DATA_BUFFER_LEN; k++) {
        s[k] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        s[i] = 2;
    }
    for (i = 0; i <_timerMin; i++) {
        if (x[i] == 1)
            s[i] = 3;
        else if (y[i] == 1)
            s[i] = 1;
    }
    sidx = (int)((MAF_ORDER + 1.0f)/2.0f);
    for (i = sidx; i < _timerMin; i++) {
        s[i-sidx] = s[i];
    }
    int timerDiff = _timerMin - sidx;
    for (i = timerDiff; i < _timerMin; i++) {
        s[i] = s[_timerMin - 1];
    }
    for (i = 0; i < _timerMin; i++) {
        if (z[i] == 1)
            s[i] = 4;
        if (w[i] == 1)
            s[i] = 5;
    }
    free(x);
    free(y);
    free(z);
    free(w);
    return s;
}

-(long long *)compressTime:(long long *)time {
    int i;
    long long *time5Min = (long long *)malloc(sizeof(long long) * SIGNAL_BUFFER_LEN);
    int timeMinSum = _timerMin + 5;
    for (i = 0; i < timeMinSum; i += 5) {
        time5Min[i/5] = time[i];
    }
    return time5Min;
}

-(int)postProcess:(int *)x y:(int *)y z:(int *)z {
    int i, j, L, *signalTmp1, *signalTmp2, *vote, maxVal, maxIdx, t;
    L = (((int)(_timerMin/5))+1);
    signalTmp1 = (int*)malloc(sizeof(int) * SIGNAL_BUFFER_LEN);
    signalTmp2 = (int*)malloc(sizeof(int) * SIGNAL_BUFFER_LEN);
    vote = (int*)malloc(sizeof(int) * 5);
    maxVal = 0;
    maxIdx = 4;
    i = j = t = 0;
    
    for (i = _timerMin; i < DATA_BUFFER_LEN; i++) {
        x[i] = 0;
    }
    for (i = 0; i < _timerMin; i++) {
        if (y[i] >= 26)
            x[i] = 5;
    }
    int timerSum = _timerMin + 5;
    for (i = 0; i < timerSum; i += 5) {
        for (j = 0; j < 5; j++) {
            vote[j] = 0;
        }
        for (j = 0; j < 5; j++) {
            if (x[i+j] == 0)
                break;
            vote[x[i+j]-1] = vote[x[i+j]-1] + 1;
        }
        maxVal = -1;
        maxIdx = 4;
        for (j = 0; j < 5; j++) {
            if (vote[j] >= maxVal) {
                maxVal = vote[j];
                maxIdx = j;
            }
        }
        signalTmp1[(int)(i/5)] = maxIdx + 1;
    }
    for (i = 0; i < L; i++) {
        signalTmp2[i] = signalTmp1[i];
    }
    for (i = 1; i < L; i++) {
        if ((signalTmp1[i-1] >= 4) && (signalTmp1[i] == 3)) {
            for (j = i; j < L; j++) {
                if (signalTmp1[j] != 3)
                    break;
                signalTmp2[j] = 2;
            }
        }
    }
    for (i = 0; i < L; i++) {
        signalTmp1[i] = signalTmp2[i];
    }
    for (i = 1; i < L; i++) {
        if (signalTmp2[i] != 3)
            continue;
        t = 0;
        for (j = i; j < L; j++) {
            if (signalTmp2[j] == 3)
                t++;
            else
                break;
            if (t > 10)
                signalTmp1[j] = 2;
        }
    }
    for (i = 0; i < L; i++) {
        signalTmp2[i] = signalTmp1[i];
    }
    if (L > 48) {
        for (i = 48; i < L; i++) {
            if (signalTmp1[i] != 1)
                continue;
            for (j = i; j > -1; j--) {
                if (signalTmp1[j] != 1)
                    break;
                signalTmp2[j] = 2;
            }
            for (j = i; j < L; j++) {
                if (signalTmp1[j] != 1)
                    break;
                signalTmp2[j] = 2;
            }
        }
    }
    for(i = 0; i < L; i++) {
        signalTmp1[i] = signalTmp2[i];
    }
    if(L > 9){
        for(i = 0; i < 10; i++) {
            if(signalTmp2[i] != 3)
                continue;
            for(j = i; j > -1; j--) {
                if(signalTmp2[j] != 3)
                    break;
                signalTmp1[j] = 2;
            }
            for(j = i; j < L; j++) {
                if(signalTmp2[j] != 3)
                    break;
                signalTmp1[j] = 2;
            }
        }
    }
    for(i = 0; i < L; i++) {
        signalTmp2[i] = signalTmp1[i];
    }
    if(L > 3){
        if(signalTmp1[0] < 3){
            signalTmp2[0] = 4;
            signalTmp2[1] = 2;
            signalTmp2[2] = 2;
        }
    }
    for(i = 0; i < L; i++) {
        signalTmp1[i] = signalTmp2[i];
    }
    for(i = 1; i < L; i++){
        if((signalTmp2[i-1] == 5) && (signalTmp2[i] == 1))
            signalTmp1[i] = 2;
        if((signalTmp2[i-1] == 4) && (signalTmp2[i] == 1))
            signalTmp1[i] = 2;
    }
    for(i = 0; i < L; i++) {
        signalTmp2[i] = signalTmp1[i]+1;
    }
    for(i = 1; i < L; i++) {
        if(signalTmp2[i] == 2)
            signalTmp2[i] = 1;
        if(signalTmp2[i] == 3)
            signalTmp2[i] = 2;
        if((signalTmp2[i-1] == 5) && (signalTmp2[i] == 2))
            signalTmp2[i] = 3;
        if((signalTmp2[i-1] == 4) && (signalTmp2[i] == 2))
            signalTmp2[i] = 3;
    }
    for(i = 0; i < L; i++) {
        z[i] = signalTmp2[i];
    }
    free(x);
    free(vote);
    free(signalTmp1);
    free(signalTmp2);
    return L;
}

-(int *)getHypnogram {
    return _hypnogram;
}

-(int)getLength {
    return _L;
}

-(long long *)getTime {
    return _timeAcq5min;
}

@end
