//
//  SleepAnalysis.h
//  SleepAnalysis
//
//  Created by FPT Software on 12/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@class EEGData;
@class EEGAnalysis;

@interface SleepAnalysis : NSObject

-(instancetype)initWithEEGData:(EEGData *)eegData EEGAnalysis:(EEGAnalysis *)eegAnalysis;
-(void)recalculateAll;
-(double)getIndexTotal;
-(double)getDataLossRate;
-(int)getTotalSleepTime;
-(int)getSleepLatency;
-(double)getSleepEfficiency;
-(int)getWakeAfterSleepOnset;
-(int)getREMLatency;
-(double)getREMEfficiency;
-(int)getSleepCycleCount;
-(int)getStateWhenWake;
-(double)getStabilityBrainWave;
-(double)getIndexAwaken;
-(double)getIndexStability;
-(double)getIndexSleepCycle;

@end
