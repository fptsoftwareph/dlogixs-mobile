//
//  EEGData.m
//  SleepAnalysis
//
//  Created by FPT Software on 12/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import "EEGData.h"


@interface EEGData() {
    long long *_timeAcq;
    double *_signalMin;
    int *_signalPoor;
    int _durationInMin;
}
@end

@implementation EEGData

-(instancetype)initEEGData:(long long *)timeAcq
                 signalMin:(double*)signalMin
                signalPoor:(int*)signalPoor
                  duration:(int)durationInMin {
    if (self = [super init]) {
        _timeAcq = timeAcq;
        _signalMin = signalMin;
        _signalPoor = signalPoor;
        _durationInMin = durationInMin;
    }
    return self;
}

-(long long*)getTimeAcq {
    return _timeAcq;
}

-(double*)getSignalMin {
    return _signalMin;
}

-(int*)getSignalPoor {
    return _signalPoor;
}

-(int)getDurationInMin {
    return _durationInMin;
}

@end
