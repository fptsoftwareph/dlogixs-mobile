//
//  EEGAnalysis.h
//  SleepAnalysis
//
//  Created by FPT Software on 13/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@class EEGData;

#define DATA_BUFFER_LEN 1440
#define SIGNAL_BUFFER_LEN 288

@interface EEGAnalysis : NSObject

-(instancetype)initWithEEGData:(EEGData *)eegData;
-(int*)getHypnogram;
-(int)getLength;
-(long long*)getTime;

@end

