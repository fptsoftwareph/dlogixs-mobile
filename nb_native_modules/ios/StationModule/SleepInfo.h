//
//  SleepInfo.h
//  StationModule
//
//  Created by FPT Software on 27/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SleepInfo : NSObject

@property (nonatomic, strong) NSString *timeAcq;
@property (nonatomic, assign) double signalMin;
@property (nonatomic, assign) int signalPoor;

@end
