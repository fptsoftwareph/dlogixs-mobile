//
//  StationUARTManager.m
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "StationUARTManager.h"
#import "CBPeripheral+Extensions.h"
#import <React/RCTBridgeModule.h>
#import "UARTUtils.h"
#import "EEGStreamParser.h"
#import "SleepInfo.h"
#import "SleepTimerManager.h"

static NSString *const PREFS_DEVICE_UUID = @"prefs_device";
static NSString * const uartServiceUUIDString = @"00001100-06E1-006A-03E8-001F4708EF3B";
static NSString * const uartRXCharacteristicUUIDString = @"00001101-06E1-006A-03E8-001F4708EF3B";
static NSString * const uartTXCharacteristicUUIDString = @"00001102-06E1-006A-03E8-001F4708EF3B";

@interface StationUARTManager() <CBCentralManagerDelegate, CBPeripheralDelegate, EEGStreamParserDelegate> {
    NSUserDefaults *_prefs;
    NSMutableDictionary *_connectCallback;
    NSMutableDictionary *_disconnectCallback;
    CBUUID *_serviceUUID;
    CBUUID *_characteristicRXUUID;
    CBUUID *_characteristicTXUUID;
    EEGStreamParser *_eegParser;
    NSTimer *_scanTimer;
    SleepTimerManager *_sleepTimerManager;
}
@end

@implementation StationUARTManager

+(instancetype)sharedStationUARTMgr {
    static StationUARTManager *sharedSession = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedSession = [[StationUARTManager alloc] init];
    });
    return sharedSession;
}

-(void)initStationWithQueue:(dispatch_queue_t)queue options:(NSMutableDictionary *)options {
    if (!_stationManager) {
        _prefs = [NSUserDefaults standardUserDefaults];
        _stationManager = [[CBCentralManager alloc] initWithDelegate:self queue:queue options:options];
        _serviceUUID = [CBUUID UUIDWithString:uartServiceUUIDString];
        _characteristicRXUUID = [CBUUID UUIDWithString:uartRXCharacteristicUUIDString];
        _characteristicTXUUID = [CBUUID UUIDWithString:uartTXCharacteristicUUIDString];
        _eegParser = [[EEGStreamParser alloc] initParser:PARSER_TYPE_PACKETS customData:NULL];
        _eegParser.delegate = self;
        _sleepTimerManager = [SleepTimerManager sharedInstance];
    }
}

-(void)scanBluetoothWithTime:(float)time services:(nullable NSArray<CBUUID *> *)services options:(NSDictionary *)options {
    [_stationManager scanForPeripheralsWithServices:services options:options];
    dispatch_async(dispatch_get_main_queue(), ^{
        _scanTimer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(scanTimer) userInfo:nil repeats:NO];
    });
}

-(void)connectPeripheral:(CBPeripheral *)peripheral connectCallback:(NSMutableDictionary *)connectCallback {
    if (peripheral) {
        _connectCallback = connectCallback;
        [_stationManager connectPeripheral:peripheral options:nil];
    }
}

-(id)retrievePeripheralWithUUID:(NSString *)UUIDString {
    CBPeripheral *peripheral;
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:UUIDString];
    if (uuid != nil) {
        NSArray<CBPeripheral *> *peripheralArray = [_stationManager retrievePeripheralsWithIdentifiers:@[uuid]];
        if([peripheralArray count] > 0) {
            peripheral = [peripheralArray objectAtIndex:0];
            return peripheral;
        } else {
            return [NSString stringWithFormat:@"No retrieved peripherals %@", UUIDString];
        }
    } else {
        return [NSString stringWithFormat:@"Wrong UUID format %@", UUIDString];
    }
}

-(void)disconnectPeripheral:(CBPeripheral *)peripheral disconnectCallback:(NSMutableDictionary *)disconnectCallback {
    if (peripheral) {
        _disconnectCallback = disconnectCallback;
        if (peripheral.services != nil)
            [self disablePeripheralServicesNotification:_bluetoothPeripheral];
        [_stationManager cancelPeripheralConnection:peripheral];
    }
}

-(NSString *)getSaveUUID {
    return [_prefs objectForKey:PREFS_DEVICE_UUID];
}

-(BOOL)isLoggedIn {
    return ([_prefs objectForKey:PREFS_DEVICE_UUID] != nil)?YES:NO;
}

-(void)stopScan {
    if (_scanTimer) {
        [_scanTimer invalidate];
        _scanTimer = nil;
    }
    [_stationManager stopScan];
}

-(void)setLight:(int)pLight {
    Byte *lightData = new Byte[BUFF_LEN-1];
    int lightNdx = 0;
    lightData[lightNdx++] = CMD_STX;
    lightData[lightNdx++] = DLStationCommandLight;
    lightData[lightNdx++] = 0x00;
    lightData[lightNdx++] = 0x00;
    lightData[lightNdx++] = 0x01;
    lightData[lightNdx++] = 0x00;
    lightData[lightNdx++] = (Byte)pLight;
    lightData[lightNdx] = [UARTUtils checkSum:lightData offset:1 len:lightNdx-1];
    lightNdx++;
    lightData[lightNdx++] = CMD_ETX;
    NSLog(@"setLight byteArrayToHex:%@", [UARTUtils byteArrayToHex:lightData Len:BUFF_LEN-1]);
    NSData *aData = [[NSData alloc] initWithBytes:lightData length:BUFF_LEN-1];
    [self writeRXCharacteristic:aData];
    delete [] lightData;
}

-(void)setVolume:(int)pVol {
    Byte *volData = new Byte[BUFF_LEN-1];
    int volNdx = 0;
    volData[volNdx++] = CMD_STX;
    volData[volNdx++] = DLStationCommandVolume;
    volData[volNdx++] = 0x00;
    volData[volNdx++] = 0x00;
    volData[volNdx++] = 0x01;
    volData[volNdx++] = 0x00;
    volData[volNdx++] = (Byte)pVol;
    volData[volNdx] = [UARTUtils checkSum:volData offset:1 len:volNdx-1];
    volNdx++;
    volData[volNdx++] = CMD_ETX;
    NSLog(@"setVolume byteArrayToHex:%@", [UARTUtils byteArrayToHex:volData Len:BUFF_LEN-1]);
    NSData *aData = [[NSData alloc] initWithBytes:volData length:BUFF_LEN-1];
    [self writeRXCharacteristic:aData];
    delete [] volData;
}

-(void)setBinauralBeat:(int)pBB {
    Byte *bbData = new Byte[BUFF_LEN];
    int bbNdx = 0;
    NSLog(@"wDATA: %ld",sizeof(bbData));
    bbData[bbNdx++] = CMD_STX;
    bbData[bbNdx++] = DLStationCommandBinauralBeat;
    bbData[bbNdx++] = 0x00;
    bbData[bbNdx++] = 0x00;
    bbData[bbNdx++] = 0x02;
    bbData[bbNdx++] = 0x00;
    bbData[bbNdx++] = 0x00;
    bbData[bbNdx++] = (Byte)pBB;
    bbData[bbNdx] = [UARTUtils checkSum:bbData offset:1 len:bbNdx-1];
    bbNdx++;
    bbData[bbNdx++] = CMD_ETX;
    NSLog(@"setBB byteArrayToHex:%@", [UARTUtils byteArrayToHex:bbData Len:BUFF_LEN] );
    NSData *aData = [[NSData alloc] initWithBytes:bbData length:BUFF_LEN];
    [self writeRXCharacteristic:aData];
    delete [] bbData;
}

-(void)queryPeripheralEquipmentStatus {
    Byte *wData = new Byte[BUFF_LEN-3];
    int ndxCtr = 0;
    
    wData[ndxCtr++] = CMD_STX;
    wData[ndxCtr++] = DLStationCommandDevEquipmentState;
    wData[ndxCtr++] = 0x00;
    wData[ndxCtr++] = 0x00;
    wData[ndxCtr++] = 0x00;
    wData[ndxCtr] = [UARTUtils checkSum:wData offset:1 len:ndxCtr-1];
    ndxCtr++;
    wData[ndxCtr++] = CMD_ETX;
    NSLog(@"sim123 equipmentRqst: %@", [UARTUtils byteArrayToHex:wData Len:ndxCtr]);
    NSData *aData = [[NSData alloc] initWithBytes:wData length:ndxCtr];
    [self writeRXCharacteristic:aData];
    delete [] wData;
}

-(void)setAudioInput:(int)pAudInp {
    Byte *audInpData = new Byte[BUFF_LEN-1];
    int audIntNdx = 0;
    audInpData[audIntNdx++] = CMD_STX;
    audInpData[audIntNdx++] = DLStationCommandAudioInput;
    audInpData[audIntNdx++] = 0x00;
    audInpData[audIntNdx++] = 0x00;
    audInpData[audIntNdx++] = 0x01;
    audInpData[audIntNdx++] = 0x00;
    audInpData[audIntNdx++] = (Byte)pAudInp;
    audInpData[audIntNdx] = [UARTUtils checkSum:audInpData offset:1 len:audIntNdx-1];
    audIntNdx++;
    audInpData[audIntNdx++] = CMD_ETX;
    NSLog(@"setAudioInput byteArrayToHex:%@", [UARTUtils byteArrayToHex:audInpData Len:BUFF_LEN-1]);
    NSData *aData = [[NSData alloc] initWithBytes:audInpData length:BUFF_LEN-1];
    [self writeRXCharacteristic:aData];
    delete [] audInpData;
}

-(void)setAudioControl1:(int)pControl {
    Byte *audCtrlData = new Byte[BUFF_LEN-1];
    int audioCtrlNdx = 0;
    audCtrlData[audioCtrlNdx++] = CMD_STX;
    audCtrlData[audioCtrlNdx++] = DLStationCommandPlayerCtrl;
    audCtrlData[audioCtrlNdx++] = 0x00;
    audCtrlData[audioCtrlNdx++] = 0x00;
    audCtrlData[audioCtrlNdx++] = 0x01;
    audCtrlData[audioCtrlNdx++] = 0x00;
    audCtrlData[audioCtrlNdx++] = (Byte)pControl;
    audCtrlData[audioCtrlNdx] =  [UARTUtils checkSum:audCtrlData offset:1 len:audioCtrlNdx-1];
    audioCtrlNdx++;
    audCtrlData[audioCtrlNdx++] = CMD_ETX;
    NSLog(@"setAudioControl1 byteArrayToHex:%@", [UARTUtils byteArrayToHex:audCtrlData Len:BUFF_LEN-1]);
    NSData *aData = [[NSData alloc] initWithBytes:audCtrlData length:BUFF_LEN-1];
    [self writeRXCharacteristic:aData];
    delete [] audCtrlData;
}

-(void)setAudioControl2:(int)pCtrl1 pCtrl2:(int)pCtrl2 {
    Byte *audioCtrlData = new Byte[BUFF_LEN];
    int sndCtrlNdx = 0;
    audioCtrlData[sndCtrlNdx++] = CMD_STX;
    audioCtrlData[sndCtrlNdx++] = DLStationCommandPlayerCtrl;
    audioCtrlData[sndCtrlNdx++] = 0x00;
    audioCtrlData[sndCtrlNdx++] = 0x00;
    audioCtrlData[sndCtrlNdx++] = 0x02;
    audioCtrlData[sndCtrlNdx++] = 0x00;
    audioCtrlData[sndCtrlNdx++] = (Byte)pCtrl1;
    audioCtrlData[sndCtrlNdx++] = (Byte)pCtrl2;
    audioCtrlData[sndCtrlNdx] = [UARTUtils checkSum:audioCtrlData offset:1 len:sndCtrlNdx-1];
    sndCtrlNdx++;
    audioCtrlData[sndCtrlNdx++] = CMD_ETX;
    NSLog(@"setAudioControl2 byteArrayToHex:%@", [UARTUtils byteArrayToHex:audioCtrlData Len:BUFF_LEN]);
    NSData *aData = [[NSData alloc] initWithBytes:audioCtrlData length:BUFF_LEN];
    [self writeRXCharacteristic:aData];
    delete [] audioCtrlData;
}

-(void)setWifiWithSSID:(NSString *)ssid passphrase:(NSString *)passphrase {
    Byte *wifiPacket = new Byte[WIFI_PACKET_LEN];
    int wifiNdx = 0;
    int pwCtr = 0;
    
    wifiPacket[wifiNdx++] = CMD_STX;
    wifiPacket[wifiNdx++] = DLStationCommandWifi;
    wifiPacket[wifiNdx++] = 0x00;
    wifiPacket[wifiNdx++] = 0x00;
    wifiPacket[wifiNdx++] = 0x52;
    wifiPacket[wifiNdx++] = 0x00;
    for (int i = 0; i < WIFI_DATA_LEN;) {
        if (i < 32) {
            for (int j = i; j < ssid.length; j++, i++)
                wifiPacket[wifiNdx++] = (Byte)([ssid characterAtIndex:j] & 0xFF);
            wifiPacket[wifiNdx++] = 0x00;
            i++;
        } else {
            if (pwCtr < passphrase.length) {
                for (int k = 0; k < passphrase.length; k++, i++) {
                    wifiPacket[wifiNdx++] = (Byte)([passphrase characterAtIndex:k] & 0xFF);
                    pwCtr++;
                }
            }
            wifiPacket[wifiNdx++] = 0x00;
            i++;
        }
    }
    wifiPacket[wifiNdx] = [UARTUtils checkSum:wifiPacket offset:1 len:wifiNdx-1];
    wifiNdx++;
    wifiPacket[wifiNdx++] = CMD_ETX;
    NSLog(@"setStationWifi() sent byteArrayToHex:%@", [UARTUtils byteArrayToHex:wifiPacket Len:WIFI_PACKET_LEN]);
    NSData *aData = [[NSData alloc] initWithBytes:wifiPacket length:WIFI_PACKET_LEN];
    [self writeRXCharacteristic:aData];
    delete [] wifiPacket;
}

-(void)deleteWifi {
    NSLog(@"deleteWifi()");
    Byte *wifiPacket = new Byte[WIFI_PACKET_LEN];
    int wifiNdx = 0;
    wifiPacket[wifiNdx++] = CMD_STX;
    wifiPacket[wifiNdx++] = DLStationCommandWifi;
    wifiPacket[wifiNdx++] = 0x00;
    wifiPacket[wifiNdx++] = 0x00;
    wifiPacket[wifiNdx++] = 0x52;
    wifiPacket[wifiNdx++] = 0x00;
    for (int i = 0; i < 52; i++)
        wifiPacket[wifiNdx++] = 0x00;
    wifiPacket[wifiNdx] = [UARTUtils checkSum:wifiPacket offset:1 len:wifiNdx-1];
    wifiNdx++;
    wifiPacket[wifiNdx++] = CMD_ETX;
    NSLog(@"setStationWifi() sent byteArrayToHex:%@", [UARTUtils byteArrayToHex:wifiPacket Len:WIFI_PACKET_LEN]);
    NSData *aData = [[NSData alloc] initWithBytes:wifiPacket length:WIFI_PACKET_LEN];
    [self writeRXCharacteristic:aData];
    delete [] wifiPacket;
}

#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
-(void)scanTimer {
    NSLog(@"scanTimer()");
    if (_scanTimer) {
        [_scanTimer invalidate];
        _scanTimer = nil;
    }
    [_stationManager stopScan];
    if (self.delegate != nil)
        [self.delegate didStopScanning];
}

-(void)disablePeripheralServicesNotification:(CBPeripheral *)peripheral {
    for (CBService *service in peripheral.services) {
        if (service.characteristics != nil)
            [self disableServiceCharacteristicsNotification:service peripheral:peripheral];
    }
}

-(void)disableServiceCharacteristicsNotification:(CBService *)service peripheral:(CBPeripheral *)peripheral {
    for (CBCharacteristic *characteristic in service.characteristics) {
        if (characteristic.isNotifying)
            [peripheral setNotifyValue:NO forCharacteristic:characteristic];
    }
}

- (void)discoverServices:(CBPeripheral *)peripheral {
    if (peripheral && peripheral.state == CBPeripheralStateConnected) {
        [peripheral discoverServices:@[_serviceUUID]];
    }
}

-(SleepInfo *)parseSleepInfoStream:(NSData *)data {
    const unsigned char *timeBytes = (Byte *)[data bytes];
//    NSLog(@"sleepinfo() rawBytes: %@", [UARTUtils byteArrayToHex:(Byte *)timeBytes Len:data.length]);
    Byte *timeAcqBuffer = new Byte[5];
    Byte *signalMinBuffer = new Byte[4];
    //time_acquired
    timeAcqBuffer[0] = timeBytes[6];
    timeAcqBuffer[1] = timeBytes[7];
    timeAcqBuffer[2] = timeBytes[8];
    timeAcqBuffer[3] = timeBytes[9];
    timeAcqBuffer[4] = timeBytes[10];
    //signal_minimum
    signalMinBuffer[0] = timeBytes[11];
    signalMinBuffer[1] = timeBytes[12];
    signalMinBuffer[2] = timeBytes[13];
    signalMinBuffer[3] = timeBytes[14];
    NSData *tempTimeAcq = [NSData dataWithBytes:(const void*)timeAcqBuffer length:5];
    SleepInfo *sleepInfo = [[SleepInfo alloc] init];
    sleepInfo.timeAcq = [UARTUtils NSDataToIntString:tempTimeAcq];
    sleepInfo.signalMin = [UARTUtils littleEndianBytesToDouble:signalMinBuffer];
    sleepInfo.signalPoor = (int)(timeBytes[15] & 0xFF);
    delete [] timeAcqBuffer;
    return sleepInfo;
}

-(NSNumber *)parseBatteryDataStream:(NSData *)battData {
    NSInteger battDLen = [battData length];
    const unsigned char *dbytes = (Byte *)[battData bytes];
    return (battDLen > 0) ? [NSNumber numberWithInt:(int)(dbytes[battDLen-3] & 0xFF)] : nil;
}

-(NSNumber *)parseBinauralStream:(NSData *)dspData {
    NSInteger dspDLen = [dspData length];
    Byte *dbytes = (Byte *)[dspData bytes];
    return [NSNumber numberWithInt:dbytes[dspDLen-3] & 0xFF];
}

-(NSNumber *)parseVolLightHeadsetStream:(NSData *)bData {
    NSInteger bLen = [bData length];
    Byte *dbytes = (Byte *)[(NSData *)bData bytes];
    return [NSNumber numberWithInteger:(int)(dbytes[bLen - 3] & 0xFF)];
}

-(NSDictionary *)parseEquipmentStatusStream:(NSData *)devData {
    NSInteger devLen = [devData length];
    NSMutableDictionary *devMap = [[NSMutableDictionary alloc] init];
    Byte *dbytes = (Byte *)[devData bytes];
    NSLog(@"sim123 equipmentResp: %@", [UARTUtils byteArrayToHex:dbytes Len:devLen]);
    NSInteger lightData = (NSInteger)dbytes[devLen - 10] & 0xFF;
    NSInteger volData = (NSInteger)dbytes[devLen - 9] & 0xFF;
    NSInteger binauralData = (NSInteger)dbytes[devLen - 6] & 0xFF;
    [devMap setObject:@(lightData) forKey:@"light"];
    [devMap setObject:@(volData) forKey:@"volume"];
    [devMap setObject:@(binauralData) forKey:@"binaural"];
    return devMap;
}

-(const char *)CBUUIDToString:(CBUUID *) UUID {
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}

-(void)handleResponseValue:(CBCharacteristic *)characteristic {
    if (self.delegate != nil && characteristic.value != nil && characteristic.value.length > 0) {
        NSData *recvData = characteristic.value;
        Byte *recvDataBytes = (Byte*)[recvData bytes];
        Byte wCmd = recvDataBytes[1];
        id respData = [UARTUtils respFromUARTPeripheral:recvData];
        if ([respData isKindOfClass:[NSData class]]) {
            NSLog(@"packet123 recv: %@", [UARTUtils byteArrayToHex:(Byte *)[respData bytes] Len:(int)recvData.length Space:YES]);
            switch (wCmd) {
                case DLStationCommandBattery:
                    if (self.delegate != nil)
                        [self.delegate didStationParsingWithMode:DLStationInfoModeBattery data:[self parseBatteryDataStream:respData]];
                    break;
                case DLStationCommandEEG: {
                    if ([respData isKindOfClass:[NSData class]]) {
                        [_sleepTimerManager initBuffer];
                        [_sleepTimerManager initTimer];
                        int r = arc4random_uniform(100);
                        [_sleepTimerManager addBrainwaveInfo:r];
                        Byte *eegBytes = (Byte*)[respData bytes];
                        NSInteger eegLen = ([respData length]) - 2;
                        Byte streamByte;
                        int i;
                        for (i = 0; i < eegLen; i++) {
                            streamByte = eegBytes[i+6];
                            [_eegParser parseByte:streamByte];
                        }
                    }
                }
                    break;
                case DLStationCommandSleepInfo:
                    if (self.delegate != nil) {
                        SleepInfo *sleepInfo = [self parseSleepInfoStream:respData];
                        [self.delegate didStationParsingWithMode:DLStationInfoModeSleepInfo data:sleepInfo];
                    }
                    break;
                case DLStationCommandHeadset:
                    if (self.delegate != nil)
                        [self.delegate didStationParsingWithMode:DLStationInfoModeHeadset data:[self parseVolLightHeadsetStream:respData]];
                    break;
                case DLStationCommandVolume:
                    if (self.delegate != nil)
                        [self.delegate didStationParsingWithMode:DLStationInfoModeVolume data:[self parseVolLightHeadsetStream:respData]];
                    break;
                case DLStationCommandLight:
                    if (self.delegate != nil)
                        [self.delegate didStationParsingWithMode:DLStationInfoModeLight data:[self parseVolLightHeadsetStream:respData]];
                    break;
                case DLStationCommandBinauralBeat:
                    if (self.delegate != nil)
                        [self.delegate didStationParsingWithMode:DLStationInfoModeBinaural data:[self parseBinauralStream:respData]];
                    break;
                case DLStationCommandDevEquipmentState:
                    if (self.delegate != nil)
                        [self.delegate didStationParsingWithMode:DLStationInfoModeDevEquipment data:[self parseEquipmentStatusStream:respData]];
                    break;
                default:
                    if (self.delegate != nil )
                        [self.delegate didStationParsingWithMode:DLStationInfoModeControl data:respData];
                    break;
            }
        } else {
            NSLog(@"packet123 Error: %@ - %@", respData, [UARTUtils byteArrayToHex:(Byte *)[respData bytes] Len:(int)recvData.length]);
        }
    }
}

-(void)writeRXCharacteristic:(NSData *)data {
    if (_bluetoothPeripheral.services.count > 0) {
        CBService *service = [_bluetoothPeripheral.services objectAtIndex:0];
        for(int i = 0; i < service.characteristics.count; i++) { //Show every one
            CBCharacteristic *characteristic1 = [service.characteristics objectAtIndex:i];
            if ([characteristic1.UUID isEqual:[CBUUID UUIDWithString:uartRXCharacteristicUUIDString]]) {
                [self.bluetoothPeripheral writeValue:data forCharacteristic:characteristic1 type:CBCharacteristicWriteWithResponse];
            }
        }
    } else {
        NSLog(@"writeRXCharacteristic() requestError");
    }
}

#pragma mark -
#pragma mark === Core Bluetooth Central Manager Delegate ===
#pragma mark -
-(void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (self.delegate != nil) {
        [self.delegate didBluetoothUpdateState:central];
    }
}

-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    if ([peripheral name].length) {
        if (self.delegate != nil)
            [self.delegate didScannedPeripheral:peripheral advertisementData:advertisementData RSSI:RSSI];
    }
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.002 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        _bluetoothPeripheral = peripheral;
        _bluetoothPeripheral.delegate = self;
        [_prefs setObject:[peripheral uuidAsString] forKey:PREFS_DEVICE_UUID];
        RCTResponseSenderBlock connectCallback = [_connectCallback valueForKey:[peripheral uuidAsString]];
        if (connectCallback) {
            connectCallback(@[@(YES)]);
            [_connectCallback removeObjectForKey:[peripheral uuidAsString]];
        }
        if (self.delegate != nil)
            [self.delegate didConnectPeripheral:peripheral];
        [self discoverServices:peripheral];
    });
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"didFailToConnectPeripheral(): %@", peripheral);
    NSString *errorStr = [NSString stringWithFormat:@"%@", [error localizedDescription]];
    RCTResponseSenderBlock connectCallback = [_connectCallback valueForKey:[peripheral uuidAsString]];
    if (connectCallback) {
        connectCallback(@[errorStr]);
        [_connectCallback removeObjectForKey:[peripheral uuidAsString]];
    }
    _bluetoothPeripheral.delegate = nil;
    _bluetoothPeripheral = nil;
}

-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    RCTResponseSenderBlock disconnectCallback = [_disconnectCallback valueForKey:[peripheral uuidAsString]];
    if (error) {
        if (disconnectCallback) {
            disconnectCallback(@[[NSString stringWithFormat:@"Error: %@", [error localizedDescription]]]);
            [_disconnectCallback removeObjectForKey:[peripheral uuidAsString]];
        }
    } else {
        [_prefs removeObjectForKey:PREFS_DEVICE_UUID];
        if (disconnectCallback) {
            disconnectCallback(@[@"Disconnected"]);
            [_disconnectCallback removeObjectForKey:[peripheral uuidAsString]];
        }
    }
    if (self.delegate != nil)
        [self.delegate didDisconnectPeripheral:peripheral error:error];
    _bluetoothPeripheral.delegate = nil;
    _bluetoothPeripheral = nil;
}

#pragma mark -
#pragma mark === Core Bluetooth Peripheral Delegate ===
#pragma mark -
-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (!error) {
        for (CBService *uartService in peripheral.services) {
            if ([uartService.UUID isEqual:_serviceUUID])
                [_bluetoothPeripheral discoverCharacteristics:nil forService:uartService];
        }
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if (!error) {
        printf("didDiscoverCharacteristicsForService() : %s found\r\n",[self CBUUIDToString:service.UUID]);
        for(int i = 0; i < service.characteristics.count; i++) { //Show every one
            CBCharacteristic *c = [service.characteristics objectAtIndex:i];
            CBUUID *characteristicUUID = [CBUUID UUIDWithString:[_characteristicTXUUID UUIDString]];
            if ([c.UUID isEqual:characteristicUUID]) {
                [peripheral setNotifyValue:YES forCharacteristic:c];
            }
        }
        
    } else {
        printf("Characteristic discorvery unsuccessfull !\r\n");
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didWriteValueForCharacteristic: %@", characteristic);
    if (!error) {
        NSLog(@"Data written to %@", characteristic.UUID.UUIDString);
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    //    NSLog(@"didUpdateValueForCharacteristic: %@", characteristic);
    if (!error) {
        [self handleResponseValue:characteristic];
    }
}

#pragma mark -
#pragma mark === EEG Stream Parser Delegate ===
#pragma mark -
-(void)handleEEGDataValue:(Byte)extendedCodeLevel code:(Byte)code valueLength:(Byte)valueLength valuesBytes:(const Byte *)value customData:(void *)customData {
    if (extendedCodeLevel == 0) {
        switch (code) {
            case(PARSER_CODE_ATTENTION):
                if (self.delegate != nil)
                    [self.delegate didParsingEEGWithMode:DLStationEEGInfoAttention data:[NSNumber numberWithInt:(int)(value[0] & 0xFF)]];
                break;
            case(PARSER_CODE_MEDITATION):
                if (self.delegate != nil)
                    [self.delegate didParsingEEGWithMode:DLStationEEGInfoMeditation data:[NSNumber numberWithInt:(int)(value[0] & 0xFF)]];
                break;
            case (PARSER_CODE_ASIC_EEG_POWER_INT): {
                int delta = ((value[0] & 0xFF) << 16  | (value[1] & 0xFF) << 8  | (value[2] & 0xFF) << 0);
                int theta = ((value[3] & 0xFF) << 16 | (value[4] & 0xFF) << 8 | (value[5] & 0xFF) << 0);
                int lowAlpha = ((value[6] & 0xFF) << 16 | (value[7] & 0xFF) << 8 | (value[8] & 0xFF) << 0);
                int highAlpha = ((value[9] & 0xFF) << 16 | (value[10] & 0xFF) << 8 | (value[11] & 0xFF) << 0);
                float alpha = (lowAlpha * 0.5f) + (highAlpha * 0.5f);
                int lowBeta = ((value[12] & 0xFF) << 16 | (value[13] & 0xFF) << 8 | (value[14] & 0xFF) << 0);
                int highBeta = ((value[15] & 0xFF) << 16 | (value[16] & 0xFF) << 8 | (value[17] & 0xFF) << 0);
                float beta = (lowBeta * 0.5f) + (highBeta * 0.5f);
                int lowGamma = ((value[18] & 0xFF) << 16 | (value[19] & 0xFF) << 8 | (value[20] & 0xFF) << 0);
                int midGamma = ((value[21] & 0xFF) << 16 | (value[22] & 0xFF) << 8 | (value[23] & 0xFF) << 0);
                float gamma = (lowGamma * 0.1f) + (midGamma * 0.1f);
                if (self.delegate != nil)
                    [self.delegate didParsingEEGWithMode:DLStationEEGInfoBand data:@[@(delta * 0.01f), @(theta * 0.1f), @(alpha), @(beta), @(gamma)]];
            }
        }
    }
}

@end
