//
//  SleepTimerManager.m
//  StationModule
//
//  Created by FPT Software on 13/07/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "SleepTimerManager.h"

#define TIME_INTERVAL    1.5 //in seconds

@interface SleepTimerManager() {
//    __block NSTimer *_sleepTimer;
    __block NSUInteger _packetLen, _intervalCtr;
    __block NSMutableArray *_sleepInfoData;
    __block NSMutableArray *_brainwaveInfo;
    dispatch_source_t _timer;
    dispatch_queue_t _queue;
}
@end

dispatch_source_t CreateDispatchTimer(double interval, dispatch_queue_t queue, dispatch_block_t block) {
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer) {
        dispatch_source_set_timer(timer, DISPATCH_TIME_NOW - 1.0 * NSEC_PER_SEC, interval * NSEC_PER_SEC, 0);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}

@implementation SleepTimerManager

+(instancetype)sharedInstance {
    static SleepTimerManager *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SleepTimerManager alloc] init];
    });
    return sharedInstance;
}

-(void)initBuffer {
    if (!_sleepInfoData) {
        _sleepInfoData = [NSMutableArray new];
    }
    if (!_brainwaveInfo) {
        _brainwaveInfo = [NSMutableArray new];
    }
}

-(void)initTimer {
    if (!_timer) {
        _packetLen = _intervalCtr = 0;
        _queue = dispatch_queue_create("com.neurobeat.SLEEPANALYSIS", DISPATCH_QUEUE_CONCURRENT);
        _timer = CreateDispatchTimer(TIME_INTERVAL, _queue, ^{
            NSLog(@"sleep123 bufferUpdated: %ld packetLen: %ld", _brainwaveInfo.count, _packetLen);
            if (_brainwaveInfo.count > _packetLen) {
                _packetLen = _brainwaveInfo.count;
            } else {
                _intervalCtr++;
            }
            if (_intervalCtr > 0) {
                NSLog(@"sleep123 there have been no packets appended.");
                [_brainwaveInfo removeAllObjects];
                _intervalCtr = 0;
                dispatch_source_cancel(_timer);
                _timer = nil;
                if (self.delegate != nil) {
                    [self.delegate didStopSleepInfoSending:_sleepInfoData];
                }
            }
        });
    }
}

-(void)addBrainwaveInfo:(int)randNum {
    if (_brainwaveInfo) {
        [_brainwaveInfo addObject:@(randNum)];
    }
}

-(void)addSleepInfo:(SleepInfo *)sleepInfo {
    if (_sleepInfoData) {
         [_sleepInfoData addObject:sleepInfo];
    }
    NSLog(@"sleep123 sleepDataUpdated: %ld", _sleepInfoData.count);
}

//-(void)initTimerWithSleepInfo:(SleepInfo *)sleepInfo {
//    if (!_sleepTimer) {
//        NSLog(@"sleep123 !_sleepTimer");
//        _ctr = _packetLen = _intervalCtr = 0;
//        _sleepInfoData = [NSMutableArray new];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            _sleepTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(sleepPacketTimer) userInfo:nil repeats:YES];
//            NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
//            [runLoop addTimer:_sleepTimer forMode:NSDefaultRunLoopMode];
//            [runLoop run];
//        });
//    }
//    [_sleepInfoData addObject:sleepInfo];
//}

@end
