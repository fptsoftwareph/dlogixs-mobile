//
//  StationModule.h
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface StationModule : RCTEventEmitter <RCTBridgeModule>

@end
