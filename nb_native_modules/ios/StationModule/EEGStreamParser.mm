//
//  EEGStreamParser.m
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "EEGStreamParser.h"

/* Decoder states (Packet decoding) */
#define PARSER_STATE_SYNC           0x01
#define PARSER_STATE_SYNC_CHECK     0x02
#define PARSER_STATE_PAYLOAD_LENGTH 0x03
#define PARSER_STATE_PAYLOAD        0x04
#define PARSER_STATE_CHKSUM         0x05

/* Decoder states (2-byte raw decoding) */
#define PARSER_STATE_WAIT_HIGH      0x06
#define PARSER_STATE_WAIT_LOW       0x07

/* Other internal constants */
#define PARSER_SYNC_BYTE            0xAA
#define PARSER_EXCODE_BYTE          0x55

@interface EEGStreamParser () {
    Byte _type, _state, _lastByte, _payloadLength ,_payloadBytesReceived, *_payload, _payloadSum, _chksum;
    void *_customData;
}
@end

@implementation EEGStreamParser

@synthesize delegate;

-(id)init {
    self = [super init];
    if (self) {
        _payload = new Byte[256];
        _customData = NULL;
    }
    return self;
}

-(instancetype)initParser:(Byte)parserType customData:(void*)customData {
    if (self = [self init]) {
        _type = parserType;
        _customData = customData;
        switch( parserType ) {
            case( PARSER_TYPE_PACKETS ):
                _state = PARSER_STATE_SYNC;
                break;
            case( PARSER_TYPE_2BYTERAW ):
                _state = PARSER_STATE_WAIT_HIGH;
                break;
            default:
                NSLog(@"Invalid parserType");
                break;
        }
    }
    return self;
}

-(int)parseByte:(Byte)byte {
    int returnValue = 0;
//    NSLog(@"EEG BYTE: %x", byte);
    switch(_state) {
        case PARSER_STATE_SYNC:
            if(byte == PARSER_SYNC_BYTE)
                _state = PARSER_STATE_SYNC_CHECK;
            break;
        case PARSER_STATE_SYNC_CHECK:
            _state = ( byte == PARSER_SYNC_BYTE ) ? PARSER_STATE_PAYLOAD_LENGTH : PARSER_STATE_SYNC;
            break;
        case PARSER_STATE_PAYLOAD_LENGTH:
            _payloadLength = byte;
            if (_payloadLength > 170) {
                _state = PARSER_STATE_SYNC;
                returnValue = -3;
            } else if (_payloadLength == 170) {
                returnValue = -4;
            } else {
                _payloadBytesReceived = 0;
                _payloadSum = 0;
                _state = PARSER_STATE_PAYLOAD;
            }
            break;
        case PARSER_STATE_PAYLOAD:
            _payload[_payloadBytesReceived++] = byte;
            _payloadSum += byte;
            if(_payloadBytesReceived >= _payloadLength) {
                _state = PARSER_STATE_CHKSUM;
            }
            break;
        case PARSER_STATE_CHKSUM:
            _chksum = byte;
            _state = PARSER_STATE_SYNC;
            if( _chksum != ((~_payloadSum) & 0xFF) )
                returnValue = -2;
            else {
                returnValue = 1;
                [self parsePacketPayload];
            }
            break;
        case PARSER_STATE_WAIT_HIGH:
            if ((byte & 0xC0) == 0x80)
                _state = PARSER_STATE_WAIT_LOW;
            break;
        case PARSER_STATE_WAIT_LOW:
            if ((byte & 0xC0) == 0x40) {
                _payload[0] = _lastByte;
                _payload[1] = byte;
                if (self.delegate != nil) {
                    [self.delegate handleEEGDataValue:0 code:PARSER_CODE_RAW_SIGNAL valueLength:2 valuesBytes:_payload customData:_customData];
                }
                returnValue = 1;
            }
            _state = PARSER_STATE_WAIT_HIGH;
            break;
        default:
            _state = PARSER_STATE_SYNC;
            returnValue = -5;
            break;
    }
    _lastByte = byte;
    return returnValue;
}

-(void)parsePacketPayload {
    Byte i = 0;
    Byte extendedCodeLevel = 0;
    Byte code = 0;
    Byte numBytes = 0;
//    NSLog(@"parsePacketPayload() payloadLength: %d", _payloadLength);
    while(i < _payloadLength) {
        while( _payload[i] == PARSER_EXCODE_BYTE ) {
            extendedCodeLevel++;
            i++;
        }
        code = _payload[i++];
//        NSLog(@"parsePacketPayload() payload: %02x:", _payload[i]);
        numBytes = ( code >= 0x80 ) ? _payload[i++] : 1;
//        NSLog(@"parsePacketPayload() numBytesLength: %d", numBytes);
        if (self.delegate != nil)
            [self.delegate handleEEGDataValue:extendedCodeLevel code:code valueLength:numBytes valuesBytes:_payload+i customData:_customData];
        i += numBytes;
    }
}

@end
