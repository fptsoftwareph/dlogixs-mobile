//
//  StationModule.m
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "StationModule.h"
#import "CBPeripheral+Extensions.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "StationUARTManager.h"
#import <React/RCTConvert.h>
#import "EEGAnalysis.h"
#import "EEGData.h"
#import "SleepAnalysis.h"
#import "SleepInfo.h"
#import "Define.h"
#import "SleepTimerManager.h"
#import "UARTUtils.h"

@interface StationModule() <StationUARTDelegate, SleepTimerDelegate> {
    NSMutableSet *_peripherals;
    StationUARTManager *_stationManager;
    RCTResponseSenderBlock _stationCallback;
    BOOL _hasListeners, _isConnected, _isTriggeredFromApp;
    NSMutableDictionary *_eegSpectrumMap, *_sleepInfoMap;
    NSMutableDictionary *_volumeCallback, *_lightCallback, *_binauralCallback, *_audioInputCallback, *_playerCtrl1Callback, *_playerCtrl2Callback, *_wifiCallback, *_btStateCallback, *_connectStationCallback;
    NSString *_savePeripheralUUID;
    SleepTimerManager *_sleepTimerManager;
}

@end

static NSString *const BT_STATE = @"BT_STATE";
static NSString *const CONNECT_STATION_SHORTCUT = @"CONNECT_STATION_SHORTCUT";

@implementation StationModule

RCT_EXPORT_MODULE()

-(instancetype)init {
    if (self = [super init]) {
        _peripherals = [NSMutableSet set];
        _stationManager = [StationUARTManager sharedStationUARTMgr];
        _eegSpectrumMap = [NSMutableDictionary new];
        _sleepInfoMap = [NSMutableDictionary new];
        _volumeCallback = [NSMutableDictionary new];
        _lightCallback = [NSMutableDictionary new];
        _binauralCallback = [NSMutableDictionary new];
        _audioInputCallback = [NSMutableDictionary new];
        _playerCtrl1Callback = [NSMutableDictionary new];
        _playerCtrl2Callback = [NSMutableDictionary new];
        _wifiCallback = [NSMutableDictionary new];
        _btStateCallback = [NSMutableDictionary new];
        _connectStationCallback = [NSMutableDictionary new];
        _savePeripheralUUID = [NSString string];
        _sleepTimerManager = [SleepTimerManager sharedInstance];
        _isConnected = NO;
        _isTriggeredFromApp = NO;
    }
    return self;
}

-(void)startObserving {
    _hasListeners = YES;
}

-(void)stopObserving {
    _hasListeners = NO;
}

- (NSArray<NSString *> *)supportedEvents {
    return @[@"StationManagerStopScan", @"StationManagerDiscoverPeripherals", @"StationSleepInfoListener", @"StationBatteryStatusListener", @"EEGSpectrumListener", @"EEGAttentionListener", @"EEGMeditationListener", @"HeadsetStatusListener", @"StationConnectionStatusListener", @"StationConnectionStatus2Listener", @"StationVolumeStatusListener", @"StationLightStatusListener", @"StationBinauralStatusListener", @"StationLookupListener", @"StationEEGInterruptionListener"];
}

#pragma mark -
#pragma mark === Helper Method ===
#pragma mark -
-(void)initilizeStationManager:(NSDictionary *)options {
    NSMutableDictionary *initOptions = [[NSMutableDictionary alloc] init];
    if ([[options allKeys] containsObject:@"showAlert"]) {
        [initOptions setObject:[NSNumber numberWithBool:[[options valueForKey:@"showAlert"] boolValue]]
                        forKey:CBCentralManagerOptionShowPowerAlertKey];
    }
    if ([[options allKeys] containsObject:@"restoreIdentifierKey"]) {
        [initOptions setObject:[options valueForKey:@"restoreIdentifierKey"]
                        forKey:CBCentralManagerOptionRestoreIdentifierKey];
    }
    [_stationManager initStationWithQueue:dispatch_get_main_queue() options:initOptions];
}

-(CBPeripheral*)findPeripheralByUUID:(NSString*)uuid {
    CBPeripheral *peripheral = nil;
    NSLog(@"findPeripheralByUUID(): %ld", _peripherals.count);
    for (CBPeripheral *p in _peripherals) {
        NSString* other = p.identifier.UUIDString;
        if ([uuid isEqualToString:other]) {
            peripheral = p;
            break;
        }
    }
    return peripheral;
}

#pragma mark -
#pragma mark === React-Natrive Export Method ===
#pragma mark -
RCT_EXPORT_METHOD(serviceInit:(NSDictionary *)options callback:(nonnull RCTResponseSenderBlock)callback) {
    NSLog(@"STATION SERVICE INITIALIZE");
    [self initilizeStationManager:options];
    _stationManager.delegate = self;
    _sleepTimerManager.delegate = self;
}

RCT_EXPORT_METHOD(scan:(NSArray *)serviceUUIDStrings
                  timeoutSeconds:(nonnull NSNumber *)timeoutSeconds
                  allowDuplicates:(BOOL)allowDuplicates
                  options:(nonnull NSDictionary*)scanningOptions
                  callback:(nonnull RCTResponseSenderBlock)callback) {
    NSLog(@"STATION BT SCANNING");
    NSArray *services = [RCTConvert NSArray:serviceUUIDStrings];
    NSDictionary *options = nil;
    if (allowDuplicates){
        options = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
    }
    NSMutableArray *serviceUUIDs = [NSMutableArray new];
    for (int i = 0; i < [services count]; i++) {
        CBUUID *serviceUUID =[CBUUID UUIDWithString:[services objectAtIndex: i]];
        [serviceUUIDs addObject:serviceUUID];
    }
    [_peripherals removeAllObjects];
    [_connectStationCallback removeObjectForKey:CONNECT_STATION_SHORTCUT];
    [_stationManager scanBluetoothWithTime:[timeoutSeconds floatValue] services:serviceUUIDs options:options];
    callback(@[]);
}

RCT_EXPORT_METHOD(stopScan) {
    NSLog(@"StationModule stopScan()");
    [_stationManager stopScan];
}

-(BOOL)isDiscoverable:(CBPeripheral *)peripheral {
    NSLog(@"isDiscoverable(): %ld", _peripherals.count);
    for (CBPeripheral *p in _peripherals) {
        NSString* other = p.identifier.UUIDString;
        if ([peripheral.identifier.UUIDString isEqualToString:other]) {
            return YES;
        }
    }
    return NO;
}

RCT_EXPORT_METHOD(connectStationShortcut:(NSString *)peripheralUUID successCallback:(nonnull RCTResponseSenderBlock)successCallback) {
    NSLog(@"connectStationShortcut()");
    [_connectStationCallback setObject:successCallback forKey:CONNECT_STATION_SHORTCUT];
    _savePeripheralUUID = [NSString stringWithString:peripheralUUID];
    [_peripherals removeAllObjects];
    [_stationManager scanBluetoothWithTime:5.0f services:@[] options:@{}];
}

-(void)connectPeripheralDevice:(CBPeripheral *)peripheral connectCallback:(NSMutableDictionary *)connectCallback {    
    RCTResponseSenderBlock connectStation = [connectCallback objectForKey:[peripheral uuidAsString]];
    if ([self isDiscoverable:peripheral]) {
        NSLog(@"connectStationShortcut() peripheralAvailable");
        [self connectPeripheral:peripheral connectCallback:connectCallback];
    } else {
        if (connectStation) {
            connectStation(@[@(2)]);
            [connectCallback removeObjectForKey:[peripheral uuidAsString]];
        }
    }
}

- (void)connectPeripheral:(CBPeripheral *)peripheral connectCallback:(NSMutableDictionary *)successCallback {
    if (peripheral) {
        [_stationManager connectPeripheral:peripheral connectCallback:successCallback];
    }
}

RCT_EXPORT_METHOD(connectStation:(NSString *)peripheralUUID successCallback:(nonnull RCTResponseSenderBlock)successCallback) {
    CBPeripheral *peripheral = [self findPeripheralByUUID:peripheralUUID];
    NSLog(@"connectStation()");
    NSMutableDictionary *connectCallback = [NSMutableDictionary new];
    if (peripheral == nil) {
        NSLog(@"connectStation() if(peripheral == nil)");
        id result = [_stationManager retrievePeripheralWithUUID:peripheralUUID];
        [connectCallback setObject:successCallback forKey:[(CBPeripheral *)result uuidAsString]];
        if ([result isKindOfClass:[CBPeripheral class]]) {
            NSLog(@"connectStation() [result isKindOfClass:[CBPeripheral class]]");
            [_stationManager connectPeripheral:(CBPeripheral *)result connectCallback:connectCallback];
        }
    } else {
        [connectCallback setObject:successCallback forKey:[peripheral uuidAsString]];
        [self connectPeripheral:peripheral connectCallback:connectCallback];
    }
}

RCT_EXPORT_METHOD(disconnect:(nonnull RCTResponseSenderBlock)callback) {
    NSMutableDictionary *disconnectCallback = [NSMutableDictionary new];
    NSLog(@"DISCONNECT PERIPHERAL UUID: %@", [_stationManager getSaveUUID]);
    [_peripherals removeAllObjects];
    [_connectStationCallback removeObjectForKey:CONNECT_STATION_SHORTCUT];
    _savePeripheralUUID = [NSString string];
    id result = [_stationManager retrievePeripheralWithUUID:[_stationManager getSaveUUID]];
    if ([result isKindOfClass:[CBPeripheral class]]) {
        NSLog(@"disconnect()");
        [disconnectCallback setObject:callback forKey:[(CBPeripheral *)result uuidAsString]];
        [_stationManager disconnectPeripheral:result disconnectCallback:disconnectCallback];
    } else {
        NSLog(@"bluetoothPeripheral is null: %@", result);
        callback(@[result]);
    }
}

RCT_EXPORT_METHOD(setLight:(NSInteger)aLight callback:(nonnull RCTResponseSenderBlock)callback) {
    [_stationManager setLight:(int)aLight];
    [_lightCallback setObject:callback forKey:@(DLStationCommandLight)];
}

RCT_EXPORT_METHOD(setVolume:(NSInteger)aVol callback:(nonnull RCTResponseSenderBlock)callback) {
    [_stationManager setVolume:(int)aVol];
    [_volumeCallback setObject:callback forKey:@(DLStationCommandVolume)];
}

RCT_EXPORT_METHOD(setBB:(NSInteger)aBB isFromApp:(BOOL)hasTriggered callback:(nonnull RCTResponseSenderBlock)callback) {
    _isTriggeredFromApp = hasTriggered;
    [_stationManager setBinauralBeat:(int)aBB];
    [_binauralCallback setObject:callback forKey:@(DLStationCommandBinauralBeat)];
}

RCT_EXPORT_METHOD(queryStationStatus) {
    [_stationManager queryPeripheralEquipmentStatus];
}

RCT_EXPORT_METHOD(setAudioInput:(NSInteger)aInput callback:(nonnull RCTResponseSenderBlock)callback) {
    [_stationManager setAudioInput:(int)aInput];
    [_audioInputCallback setObject:callback forKey:@(DLStationCommandAudioInput)];
}

RCT_EXPORT_METHOD(setControl1:(NSString *)aCtrl callback:(nonnull RCTResponseSenderBlock)callback) {
    int audioCtrl = [aCtrl characterAtIndex:0];
    [_stationManager setAudioControl1:audioCtrl];
    [_playerCtrl1Callback setObject:callback forKey:@(DLStationCommandPlayerCtrl)];
}

RCT_EXPORT_METHOD(setControl2:(NSString *)aCtrl1 callback:(nonnull RCTResponseSenderBlock)callback) {
    int audioCtrl1 = [aCtrl1 characterAtIndex:0];
    int audioCtrl2 = [aCtrl1 characterAtIndex:1];
    [_stationManager setAudioControl2:audioCtrl1 pCtrl2:audioCtrl2];
    [_playerCtrl2Callback setObject:callback forKey:@(DLStationCommandBattery)];
}

RCT_EXPORT_METHOD(setWifi:(NSString *)ssid password:(NSString *)password callback:(nonnull RCTResponseSenderBlock)callback ) {
    [_stationManager setWifiWithSSID:ssid passphrase:password];
    [_wifiCallback setObject:callback forKey:@(DLStationCommandWifi)];
}

RCT_EXPORT_METHOD(forgetWifi:(nonnull RCTResponseSenderBlock)callback) {
    [_stationManager deleteWifi];
    [_wifiCallback setObject:callback forKey:@(DLStationCommandWifi)];
}

RCT_EXPORT_METHOD(checkState:(nonnull RCTResponseSenderBlock)callback) {
    if (_stationManager.stationManager != nil) {
        [_btStateCallback setObject:callback forKey:BT_STATE];
        [self didBluetoothUpdateState:_stationManager.stationManager];
    }
}

-(NSString*)getCurrentDate {
    NSDate* utcDate = [NSDate date];
    NSDateFormatter *localDateFormat = [NSDateFormatter new];
    [localDateFormat setDateFormat:@"YYYY-MM-dd"];
    [localDateFormat setTimeZone:[NSTimeZone localTimeZone]];
    return [localDateFormat stringFromDate:utcDate];
}

RCT_EXPORT_METHOD(printLog:(NSString *)msg) {
    NSLog(@"%@", msg);
}

#pragma mark -
#pragma mark === Station UART Manager Delegate ===
#pragma mark -
-(void)didBluetoothUpdateState:(CBCentralManager *)central {
    RCTResponseSenderBlock btStateCallback = [_btStateCallback objectForKey:BT_STATE];
    switch (central.state) {
        case CBCentralManagerStatePoweredOn:
            if (btStateCallback)
                btStateCallback(@[@"on"]);
            break;
        case CBCentralManagerStatePoweredOff:
            if (btStateCallback)
                btStateCallback(@[@"off"]);
            if (_isConnected) {
                _isConnected = NO;
                if (_hasListeners)
                    [self sendEventWithName:@"StationConnectionStatusListener" body:@(5)];
                if (_hasListeners)
                    [self sendEventWithName:@"StationConnectionStatus2Listener" body:@(5)];
            }
            break;
    }
    [_btStateCallback removeObjectForKey:BT_STATE];
}

-(void)didScannedPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    if ([peripheral name].length) {
        [_peripherals addObject:peripheral];
        [peripheral setAdvertisementData:advertisementData RSSI:RSSI];
        NSLog(@"didScannedPeripheral() Discover peripheral: %@", [peripheral name]);
        if (_hasListeners)
            [self sendEventWithName:@"StationManagerDiscoverPeripherals" body:[peripheral asDictionary]];
    }
    NSLog(@"didScannedPeripheral() :%ld", _peripherals.count);
}

-(void)didStopScanning {
    if (_hasListeners) {
        [self sendEventWithName:@"StationManagerStopScan" body:@{}];
    }
    RCTResponseSenderBlock connectStation = [_connectStationCallback objectForKey:CONNECT_STATION_SHORTCUT];
    if (connectStation && [_savePeripheralUUID length] > 0) {
        NSLog(@"connectStationShortcut() connectStationShortcutTriggered");
        NSLog(@"connectStationShortcut() _peripheralSize: %ld", _peripherals.count);
        CBPeripheral *peripheral = [self findPeripheralByUUID:_savePeripheralUUID];
        NSMutableDictionary *connectCallback = [NSMutableDictionary new];
        if (peripheral == nil) {
            id result = [_stationManager retrievePeripheralWithUUID:_savePeripheralUUID];
            [connectCallback setObject:connectStation forKey:[(CBPeripheral *)result uuidAsString]];
            if ([result isKindOfClass:[CBPeripheral class]])
                [self connectPeripheralDevice:(CBPeripheral *)result connectCallback:connectCallback];
        } else {
            NSLog(@"connectStationShortcut() peripheral != nil");
            [connectCallback setObject:connectStation forKey:[peripheral uuidAsString]];
            [self connectPeripheralDevice:peripheral connectCallback:connectCallback];
        }
    }
}

-(void)didConnectPeripheral:(CBPeripheral *)peripheral {
    _isConnected = YES;
}

-(void)didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSInteger errCode = error.code;
    _isConnected = NO;
    if (errCode == 6) {
        if (_hasListeners)
            [self sendEventWithName:@"StationConnectionStatusListener" body:@(5)];
        if (_hasListeners)
            [self sendEventWithName:@"StationConnectionStatus2Listener" body:@(5)];
    }
}

-(void)didStationParsingWithMode:(DLStationInfoMode)mode data:(id)data {
    RCTResponseSenderBlock volumeCallback = [_volumeCallback objectForKey:@(DLStationCommandVolume)];
    RCTResponseSenderBlock lightCallback = [_lightCallback objectForKey:@(DLStationCommandLight)];
    RCTResponseSenderBlock binauralCallback = [_binauralCallback objectForKey:@(DLStationCommandBinauralBeat)];
    if (volumeCallback) {
        volumeCallback(@[@(YES)]);
        [_volumeCallback removeObjectForKey:@(DLStationCommandVolume)];
    }
    if (lightCallback) {
        lightCallback(@[@(YES)]);
        [_lightCallback removeObjectForKey:@(DLStationCommandLight)];
    }
    if (binauralCallback) {
        binauralCallback(@[@(YES)]);
        [_binauralCallback removeObjectForKey:@(DLStationCommandBinauralBeat)];
    }
    switch (mode) {
        case DLStationInfoModeControl: {
            NSLog(@"DLStationInfoModeControl");
            RCTResponseSenderBlock audioInputCallback = [_audioInputCallback objectForKey:@(DLStationCommandAudioInput)];
            RCTResponseSenderBlock wifiCallback = [_wifiCallback objectForKey:@(DLStationCommandWifi)];
            RCTResponseSenderBlock playerControl1Callback = [_playerCtrl1Callback objectForKey:@(DLStationCommandPlayerCtrl)];
            RCTResponseSenderBlock playerControl2Callback = [_playerCtrl2Callback objectForKey:@(DLStationCommandBattery)];
            if (audioInputCallback) {
                if ([data isKindOfClass:[NSData class]])
                    audioInputCallback(@[@(YES)]);
                else
                    audioInputCallback(@[data]);
                [_audioInputCallback removeObjectForKey:@(DLStationCommandAudioInput)];
            }
            if (playerControl1Callback) {
                if ([data isKindOfClass:[NSData class]])
                    playerControl1Callback(@[@(YES)]);
                else
                    playerControl1Callback(@[data]);
                [_playerCtrl1Callback removeObjectForKey:@(DLStationCommandPlayerCtrl)];
            }
            if (playerControl2Callback) {
                if ([data isKindOfClass:[NSData class]])
                    playerControl2Callback(@[@(YES)]);
                else
                    playerControl2Callback(@[data]);
                [_playerCtrl2Callback removeObjectForKey:@(DLStationCommandBattery)];
            }
            if (wifiCallback) {
                if ([data isKindOfClass:[NSData class]]) {
                    Byte *wifiBytes = (Byte *)[(NSData *)data bytes];
                    int dataLen = wifiBytes[4] & 0xFF;
                    int val = (dataLen != 0) ? wifiBytes[6] & 0xFF : 0;
                    wifiCallback(@[@(val)]);
                } else {
                    wifiCallback(@[data]);
                }
                [_wifiCallback removeObjectForKey:@(DLStationCommandWifi)];
            }
        }
            break;
        case DLStationInfoModeBattery:
            NSLog(@"battery value: %d\n", [data intValue]);
            if (_hasListeners && data != nil)
                [self sendEventWithName:@"StationBatteryStatusListener" body:data];
            break;
        case DLStationInfoModeSleepInfo: {
            [_sleepTimerManager addSleepInfo:data];
        }
            break;
        case DLStationInfoModeHeadset: {
            if (_hasListeners)
                [self sendEventWithName:@"HeadsetStatusListener" body:data];
        }
            break;
        case DLStationInfoModeVolume:
            if (_hasListeners)
                [self sendEventWithName:@"StationVolumeStatusListener" body:@([(NSNumber*)data intValue])];
            break;
        case DLStationInfoModeLight:
            if (_hasListeners)
                [self sendEventWithName:@"StationLightStatusListener" body:@([(NSNumber *)data intValue])];
            break;
        case DLStationInfoModeBinaural:
            if (!_isTriggeredFromApp) {
                if (_hasListeners)
                    [self sendEventWithName:@"StationBinauralStatusListener" body:@([(NSNumber *)data intValue])];
            } else {
                _isTriggeredFromApp = NO;
            }
            break;
        case DLStationInfoModeDevEquipment:
            if (_hasListeners)
                [self sendEventWithName:@"StationLookupListener" body:(NSDictionary *)data];
            break;
        default:
            break;
    }
}

-(NSInteger)parseGtbtFromDateString:(NSString *)strDate {
    NSMutableString *s = [NSMutableString string];
    if (strDate.length == 0 || strDate.length < 10) {
        return 0;
    } else {
        [s appendFormat:@"%c", [strDate characterAtIndex:strDate.length - 2]];
        [s appendFormat:@"%c", [strDate characterAtIndex:strDate.length - 1]];
        return [s integerValue];
    }
}

#pragma mark -
#pragma mark === Sleep Timer Delegate ===
#pragma mark -
-(void)didStopSleepInfoSending:(NSMutableArray *)sleepData {
    NSLog(@"sleep123 didStop() count: %ld", sleepData.count);
    NSUInteger L = [sleepData count];
    if (L > 29) {
        long long *timeAcqPtr = (long long *)malloc(sizeof(long long) * L);
        double *signalMinPtr = (double *)malloc(sizeof(double) * L);
        int *signalPoorPtr = (int *)malloc(sizeof(int) * L);
        NSMutableArray *hypnogramArr = [[NSMutableArray alloc] init];
        NSInteger goToBedTime = [self parseGtbtFromDateString:[[sleepData objectAtIndex:0] timeAcq]];
        for (int i = 0; i < L; i++) {
            NSLog(@"sleepinfo() TimeAcq: %lld \tSignalMin:%f \tSignalPoor:%d", [[[sleepData objectAtIndex:i] timeAcq] longLongValue], [[sleepData objectAtIndex:i] signalMin], [[sleepData objectAtIndex:i] signalPoor]);
            timeAcqPtr[i] = [[[sleepData objectAtIndex:i] timeAcq] longLongValue];
            signalMinPtr[i] = [[sleepData objectAtIndex:i] signalMin];
            signalPoorPtr[i] = [[sleepData objectAtIndex:i] signalPoor];
        }
        EEGData *eegData = [[EEGData alloc] initEEGData:timeAcqPtr signalMin:signalMinPtr signalPoor:signalPoorPtr duration:(int)L];
        EEGAnalysis *eegAnalysis = [[EEGAnalysis alloc] initWithEEGData:eegData];
        SleepAnalysis *sleepAnalysis = [[SleepAnalysis alloc] initWithEEGData:eegData EEGAnalysis:eegAnalysis];
        for (int j = 0; j < [eegAnalysis getLength]; j++) {
            int hypVal = ([eegAnalysis getHypnogram][j] < 0 || [eegAnalysis getHypnogram][j] > 6) ? 0 : [eegAnalysis getHypnogram][j];
            [hypnogramArr addObject:@(hypVal)];
        }
        
        NSMutableDictionary *sleepInfoMap = [[NSMutableDictionary alloc] init];
        [sleepInfoMap setObject:[self getCurrentDate] forKey:@"DATE"];
        
        [sleepInfoMap setObject:@([sleepAnalysis getTotalSleepTime]) forKey:SLEEP_ANALYSIS_TOTAL];
        [sleepInfoMap setObject:@([sleepAnalysis getSleepLatency]) forKey:SLEEP_ANALYSIS_LATENCY];
        [sleepInfoMap setObject:@([sleepAnalysis getWakeAfterSleepOnset]) forKey:SLEEP_ANALYSIS_WAKE];
        [sleepInfoMap setObject:@([sleepAnalysis getIndexTotal]) forKey:SLEEP_ANALYSIS_POINTS];
        [sleepInfoMap setObject:@([sleepAnalysis getIndexStability]) forKey:SLEEP_SQI_STABILITY];
        [sleepInfoMap setObject:@([sleepAnalysis getIndexAwaken]) forKey:SLEEP_SQI_AWAKEN];
        [sleepInfoMap setObject:@([sleepAnalysis getIndexSleepCycle]) forKey:SLEEP_SQI_CYCLE];
        [sleepInfoMap setObject:hypnogramArr forKey:SLEEP_HYPNOGRAM];
        [sleepInfoMap setObject:@([sleepAnalysis getStabilityBrainWave]) forKey:SLEEP_STABILITY_BRAINWAVE];
        [sleepInfoMap setObject:@(goToBedTime) forKey:SLEEP_GTBT];
        NSLog(@"sleepinfo() API output: %@", sleepInfoMap);
        if (_hasListeners) {
            NSLog(@"StationSleepInfoListener()");
            [self sendEventWithName:@"StationSleepInfoListener" body:sleepInfoMap];
        }
        [sleepData removeAllObjects];
        free([eegAnalysis getTime]);
        free(timeAcqPtr);
        free(signalMinPtr);
        free(signalPoorPtr);
    } else {
        [sleepData removeAllObjects];
    }
    if (_hasListeners) {
        [self sendEventWithName:@"StationEEGInterruptionListener" body:@(YES)];
    }
}

-(void)didParsingEEGWithMode:(DLStationEEGInfo)mode data:(id)data {
    switch (mode) {
        case DLStationEEGInfoAttention:
            if (_hasListeners)
                [self sendEventWithName:@"EEGAttentionListener" body:data];
            break;
        case DLStationEEGInfoMeditation:
            if (_hasListeners)
                [self sendEventWithName:@"EEGMeditationListener" body:data];
            break;
        case DLStationEEGInfoBand:
            if (_hasListeners) {
                [self eegSpectrumToMap:data];
                [self sendEventWithName:@"EEGSpectrumListener" body:_eegSpectrumMap];
                [_eegSpectrumMap removeAllObjects];
            }
            break;
    }
}

-(void)eegSpectrumToMap:(NSArray *)date {
    [_eegSpectrumMap setObject:@([[date objectAtIndex:0] floatValue]) forKey:@"delta"];
    [_eegSpectrumMap setObject:@([[date objectAtIndex:1] floatValue]) forKey:@"theta"];
    [_eegSpectrumMap setObject:@([[date objectAtIndex:2] floatValue]) forKey:@"alpha"];
    [_eegSpectrumMap setObject:@([[date objectAtIndex:3] floatValue]) forKey:@"beta"];
    [_eegSpectrumMap setObject:@([[date objectAtIndex:4] floatValue]) forKey:@"gamma"];
}

@end
