//
//  SleepTimerManager.h
//  StationModule
//
//  Created by FPT Software on 13/07/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SleepInfo.h"

@protocol SleepTimerDelegate
@required
-(void)didStopSleepInfoSending:(NSMutableArray *)sleepData;
@end

@interface SleepTimerManager : NSObject

@property (weak, nonatomic) id<SleepTimerDelegate> delegate;

+(instancetype)sharedInstance;
-(void)initBuffer;
-(void)initTimer;
-(void)addSleepInfo:(SleepInfo *)sleepInfo;
-(void)addBrainwaveInfo:(int)randNum;

@end
