//
//  EEGStreamParser.h
//  StationModule
//
//  Created by FPT Software on 06/02/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PARSER_TYPE_PACKETS  0x01
#define PARSER_TYPE_2BYTERAW 0x02

/* Parser CODE definitions */
#define PARSER_CODE_BATTERY            0x01
#define PARSER_CODE_POOR_QUALITY       0x02
#define PARSER_CODE_ATTENTION          0x04
#define PARSER_CODE_MEDITATION         0x05
#define PARSER_CODE_8BITRAW_SIGNAL     0x06
#define PARSER_CODE_RAW_MARKER         0x07

#define PARSER_CODE_RAW_SIGNAL         0x80
#define PARSER_CODE_EEG_POWERS         0x81
#define PARSER_CODE_ASIC_EEG_POWER_INT 0x83


@protocol EEGStreamParserDelegate <NSObject>
-(void)handleEEGDataValue:(Byte)extendedCodeLevel code:(Byte)code valueLength:(Byte)valueLength valuesBytes:(const Byte*)value customData:(void*)customData;
@end

@interface EEGStreamParser : NSObject

@property (nonatomic, weak) id <EEGStreamParserDelegate> delegate;

-(instancetype)initParser:(Byte)parserType customData:(void*)customData;
-(int)parseByte:(Byte)byte;
@end
