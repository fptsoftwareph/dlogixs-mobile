//
//  AlarmModule.m
//  AlarmModule
//
//  Created by FPT Software on 08/03/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import "AlarmModule.h"
#import "AlarmModule-Swift.h"
#import <AVFoundation/AVFoundation.h>
#import <React/RCTConvert.h>
#import "AppDelegate.h"

@interface AlarmModule() <AppStateDelegate> {
    AVAudioPlayer *_player;
    Alarms *_alarmModel;
    Scheduler *_alarmScheduler;
    BOOL _hasListeners;
    AlarmManager *_alarmManager;
    AppDelegate *_appDelegate;
    RingtoneManager *_ringtoneManager;
    Downloader *_downloader;
    BOOL _hasNotification;
}

@end

@implementation AlarmModule

RCT_EXPORT_MODULE()

-(instancetype)init {
    if (self = [super init]) {
        _alarmManager = [[AlarmManager alloc] init];
        _alarmScheduler = [[Scheduler alloc] init];
        _ringtoneManager = [[RingtoneManager alloc] init];
        _downloader = [[Downloader alloc] init];
        _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _appDelegate.delegate = self;
        _hasNotification = NO;
    }
    return self;
}

-(void)startObserving {
    _hasListeners = YES;
}

-(void)stopObserving {
    _hasListeners = NO;
}

-(NSArray<NSString *> *)supportedEvents {
    return @[@"AlarmNotificationListener", @"AlarmSnoozeNotificationListener"];
}

-(void)displayAlarmNotificationDialog:(UILocalNotification *)notification {
    _hasNotification = YES;
    _alarmModel = [[Alarms alloc] init];
    NSDictionary *userInfo = notification.userInfo;
    NSString *soundName = @"";
    int index = -1;
    BOOL isBinauralEnabled = false;
    if ([userInfo objectForKey:@"soundName"] || [userInfo objectForKey:@"index"] || [userInfo objectForKey:@"isBinauralEnabled"]) {
        soundName = [userInfo objectForKey:@"soundName"];
        index = [[userInfo objectForKey:@"index"] intValue];
        isBinauralEnabled = [[userInfo objectForKey:@"isBinauralEnabled"] boolValue];
    }
    NSLog(@"didReceiveNotification() soundName: %@", soundName);
//    if (!_ringtoneManager.audioPlayer.isPlaying) {
    [_ringtoneManager playRingtone:soundName];
    [_ringtoneManager setVibrate];
    [_ringtoneManager registerSessionInterruptionForNotification];
//    }
    if (_hasListeners && _alarmModel.alarms.count > 0) {
        NSLog(@"didReceiveNotification() soundName: %@", soundName);
        [self sendEventWithName:@"AlarmSnoozeNotificationListener" body:@{@"alarm_index" : @(index), @"alarm_isBinauralEnabled" : @(isBinauralEnabled)}];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    });
}

-(void)app:(UIApplication *)app didReceiveNotification:(UILocalNotification *)notification {
    NSLog(@"app:didReceiveNotification()");    
    [self displayAlarmNotificationDialog:notification];
}

-(void)appDidBecomeActive:(UIApplication *)app {
    NSLog(@"appDidBecomeActive()");
    [_alarmScheduler checkNotification];
}

-(void)appDidEnterBackground:(UIApplication *)app {
    NSLog(@"appDidEnterBackground(): %d", _hasNotification);
    if (!_hasNotification) {
        [self stopAlarmRingtone];
    }
}

RCT_EXPORT_METHOD(schedule:(NSDictionary *)json) {
    NSDictionary *alarm = [self createUpdateAlarmFromJSON:json];
    NSDate *date = [alarm objectForKey:@"alarm_date"];
    NSString *mediaLabel = [alarm objectForKey:@"sound_name"];
    NSArray *recurringDays = [alarm objectForKey:@"repeat_alarm"];
    NSInteger snoozeLen = [[alarm objectForKey:@"snooze"] integerValue];
    BOOL hasVibrate = [[alarm objectForKey:@"vibrate"] boolValue];
    NSString *mediaTitle = [alarm objectForKey:@"song_name"];
    BOOL isBinauralEnabled = [[alarm objectForKey:@"isBinauralEnabled"] boolValue];
    [_alarmManager onTimeSet:date vibrateEnabled:hasVibrate mediaLabel:mediaLabel repeatDays:recurringDays snoozeDuration:snoozeLen mediaTitle:mediaTitle isBinauralEnabled:isBinauralEnabled];
}

-(NSDictionary *)createUpdateAlarmFromJSON:(NSDictionary *)json {
    NSLog(@"createUpdateAlarmFromJSON(): %@", json);
    _alarmModel = [[Alarms alloc] init];
    NSMutableDictionary *alarmObj = [[NSMutableDictionary alloc] init];
    NSDate *jsonDate = [RCTConvert NSDate:[json objectForKey:@"alarm_date"]];
    if (jsonDate == nil)
        RCTLogError(@"Failed To Schedule Notification - Invalid Date");
    NSDateFormatter *jsonDF = [[NSDateFormatter alloc] init];
    [jsonDF setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [jsonDF setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *utcDate = [jsonDF stringFromDate:jsonDate];
    NSDateFormatter *alarmDF = [[NSDateFormatter alloc] init];
    [alarmDF setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *localDate = [alarmDF dateFromString:utcDate];
    NSCalendar *gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *alarmDate = [Scheduler correctSecondComponentWithDate:localDate calendar:gregorianCal];
    NSString *mediaLabel = [RCTConvert NSString:[json objectForKey:@"sound_name"]];
    NSString *mediaTitle = [RCTConvert NSString:[json objectForKey:@"song_name"]];
    NSInteger snoozeLen = (NSInteger)[RCTConvert double:[json objectForKey:@"snooze"]];
    if (snoozeLen < 1)
        RCTLogError(@"Cannot snooze for %ld minutes", snoozeLen);
    NSArray *recurringDays = [RCTConvert NSArray:[json objectForKey:@"repeat_alarm"]];
    for (int i = 0; i < recurringDays.count; i++) {
        if ([[recurringDays objectAtIndex:i] intValue] < 0 || [[recurringDays objectAtIndex:i] intValue] > 6) {
            RCTLogError(@"Week days (%d) out of range", i);
        }
    }
    BOOL hasVibrate = [RCTConvert BOOL:[json objectForKey:@"vibrate"]];
    BOOL isBinauralEnabled = [RCTConvert BOOL:[json objectForKey:@"rbs_enabled"]];
    NSInteger alarmId = ([NSNull null] != [json objectForKey:@"id"]) ? [[json objectForKey:@"id"] integerValue] : 0;
    [alarmObj setObject:@(alarmId) forKey:@"index"];
    [alarmObj setObject:alarmDate forKey:@"alarm_date"];
    [alarmObj setObject:mediaLabel forKey:@"sound_name"];
    [alarmObj setObject:recurringDays forKey:@"repeat_alarm"];
    [alarmObj setObject:@(snoozeLen) forKey:@"snooze"];
    [alarmObj setObject:@(hasVibrate) forKey:@"vibrate"];
    [alarmObj setObject:mediaTitle forKey:@"song_name"];
    [alarmObj setObject:@(isBinauralEnabled) forKey:@"isBinauralEnabled"];
    return alarmObj;
}

RCT_EXPORT_METHOD(scheduleUpdate:(NSDictionary *)json) {
    NSDictionary *alarm = [self createUpdateAlarmFromJSON:json];
    NSInteger index = [[alarm objectForKey:@"index"] integerValue];
    NSDate *date = [alarm objectForKey:@"alarm_date"];
    NSString *mediaLabel = [alarm objectForKey:@"sound_name"];
    NSArray *recurringDays = [alarm objectForKey:@"repeat_alarm"];
    NSInteger snoozeLen = [[alarm objectForKey:@"snooze"] integerValue];
    BOOL hasVibrate = [[alarm objectForKey:@"vibrate"] boolValue];
    NSString *mediaTitle = [alarm objectForKey:@"song_name"];
    BOOL isBinauralEnabled = [[alarm objectForKey:@"isBinauralEnabled"] boolValue];
    NSLog(@"scheduleUpdate() INDEX: %ld", index);
    [_alarmManager onTimeUpdateAtIndex:index date:date vibrateEnabled:hasVibrate mediaLabel:mediaLabel repeatDays:recurringDays snoozeDuration:snoozeLen mediaTitle:mediaTitle isBinauralEnabled:isBinauralEnabled];
}

RCT_EXPORT_METHOD(scheduleDelete:(int)alarmId) {
    NSLog(@"scheduleDelete(): %d",alarmId);
    [_alarmManager onTimeDeleteAtIndex:alarmId];
}

RCT_EXPORT_METHOD(scheduleToggle:(int)alarmId) {
    NSLog(@"scheduleToggle(): %d",alarmId);
    [_alarmManager switchToggleAtIndex:alarmId];
}

RCT_EXPORT_METHOD(getScheduledAlarms:(nonnull RCTResponseSenderBlock)callback) {
    callback(@[[_alarmManager getScheduledAlarms]]);
}

RCT_EXPORT_METHOD(onSnooze:(int)alarmId) {
    NSLog(@"onSnooze(): %d",alarmId);
    [_alarmManager onSetSnoozeAtIndex:alarmId];
    [self stopAlarmRingtone];
    _hasNotification = NO;
}

-(void)stopAlarmRingtone {
//    if (_ringtoneManager.audioPlayer.isPlaying) {
//        [_ringtoneManager stopRingtone];
//    } else {
//        [_ringtoneManager stopMusicPlayer];
//    }
    [_ringtoneManager stopMusicPlayer];
}

RCT_EXPORT_METHOD(onDismiss:(int)alarmId) {
    NSLog(@"onDismiss(): %d",alarmId);
    [_alarmManager onStopAlarmAtIndex:alarmId];
    [self stopAlarmRingtone];
    _hasNotification = NO;
    if (_hasListeners) {
        [self sendEventWithName:@"AlarmNotificationListener" body:nil];
    }
}

RCT_EXPORT_METHOD(getAlarmRingtones:(nonnull RCTResponseSenderBlock)callback) {
    callback(@[[_ringtoneManager getAlarmRingtones]]);
}

RCT_EXPORT_METHOD(playRingtone:(NSString *)url) {
    [_ringtoneManager playRingtone:url];
}

RCT_EXPORT_METHOD(stopRingtone) {
    NSLog(@"stopRingtone()");
    [self stopAlarmRingtone];
}

RCT_EXPORT_METHOD(stopMusic) {
    [_ringtoneManager stopMusicPlayer];
}

RCT_EXPORT_METHOD(downloadRingtone:(NSString *)url requestHeader:(NSDictionary *)header config:(NSDictionary *)config callback:(nonnull RCTResponseSenderBlock)callback) {
    [_downloader startDownloadRequest:url config:config succesCallback:callback];
}

RCT_EXPORT_METHOD(disableAlarms) {
    [_alarmManager cancelScheduledAlarms];
}

@end

