//
//  AlarmModule.h
//  AlarmModule
//
//  Created by FPT Software on 11/05/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface AlarmModule : RCTEventEmitter <RCTBridgeModule>

@end
