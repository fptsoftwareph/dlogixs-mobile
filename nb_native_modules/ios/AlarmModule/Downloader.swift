//
//  Downloader.swift
//  AlarmModule
//
//  Created by FPT Software on 19/04/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

import Alamofire
import Foundation

@objc class Downloader : NSObject {
    
    override init() {
        super.init()
    }
    
    @objc internal func startDownloadRequest(_ stringUrl: String, config: [String: Any], succesCallback: @escaping RCTResponseSenderBlock) -> Void {
        print("startTaskRequest() URL:\(stringUrl) config:\(config)")
        let savePathName = config["saveAsName"] as? String
        let subdir = config["path"] as? String
        let fileUrl:URL? = URL(string: stringUrl)
        let destPath: DownloadRequest.DownloadFileDestination = { _, _ in
            let libraryDir = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
            let fileURL = libraryDir.appendingPathComponent(subdir! + savePathName!)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        Alamofire.download(fileUrl!, to: destPath)
            .downloadProgress { progress in
                print("Download Progress: \(progress.fractionCompleted)")
            }
            .responseData { response in
                if response.error == nil, let destURL = response.destinationURL?.path {
                    print("DOWNLOADED FILE PATH: \(destURL)")
                    succesCallback(["Download Successfully"])
                } else {
                    succesCallback(["Download Failed"])
                }
        }
    }
}
