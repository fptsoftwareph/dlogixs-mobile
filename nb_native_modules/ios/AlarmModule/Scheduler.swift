//
//  Scheduler.swift
//  AlarmModule
//
//  Created by FPT Software on 16/05/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

import Foundation
import UIKit

@objc class Scheduler : NSObject, AlarmSchedulerDelegate {
    var alarmModel: Alarms = Alarms()
    func setupNotificationSettings() {
        // Specify the notification types.
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.sound]
        
        // Register the notification settings.
        let newNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        DispatchQueue.main.async {
            UIApplication.shared.registerUserNotificationSettings(newNotificationSettings)
        }
    }
    
    private func removeSecondFromDate(_ date: Date) -> Date {
        let calendar = Calendar.current
        let dateComponents = (calendar as NSCalendar).components([.month, .day, .year, .hour, .minute], from: date)
        return calendar.date(from: dateComponents)!
    }
    
    func correctDateForNonRepeating(_ date: Date) -> Date {
        let now = Date()
        let calendar = Calendar.current
        var wdDate: Date!
        if date.compare(now) == .orderedDescending {
            print("correctDate() weekdaysEmpty now")
            wdDate = date
        } else { //schedule for next day
            print("correctDate() weekdaysEmpty later")
            wdDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: 1, to: date, options: .matchStrictly)!
        }
        return removeSecondFromDate(wdDate)
    }
    
    func correctRepeatingDate(_ date: Date, onWeekdaysForNotify weekdays: [Int]) -> [Date] {
        print("correctDate() Date: \(date) DaysOfWeek1: \(weekdays)")
        var mappedDate: [Date] = [Date]()
        let calendar = Calendar.current
        let flags: NSCalendar.Unit = [NSCalendar.Unit.weekday, NSCalendar.Unit.weekdayOrdinal, NSCalendar.Unit.day]
        let dateComponents = (calendar as NSCalendar).components(flags, from: date)
        let weekday:Int = dateComponents.weekday!
        var wdDate: Date! = date
        var sortedAlarms: [Date] = [Date]()
        let now = Date()
        
        if !weekdays.isEmpty { // for repeating
            let daysInWeek = 7
            for wd in weekdays {
                if wd > weekday {
                    print("correctDate() wd > weekday ")
                    let weekdayDiff = wd - weekday
                    wdDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: weekdayDiff, to: date, options:.matchStrictly)!
                } else if wd == weekday {
                    print("correctDate() wd == weekday ")
                    if date.compare(now) == .orderedAscending {
                        wdDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: daysInWeek, to: date, options:.matchStrictly)!
                    } else {
                        wdDate = date
                    }
                } else {
                    print("correctDate() wd <= weekday ")
                    let futureWkdayDiff = weekday - wd
                    let dayOffset = daysInWeek - futureWkdayDiff
                    wdDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: dayOffset, to: date, options: .matchStrictly)!
                }
                mappedDate.append(wdDate)
            }
            sortedAlarms = mappedDate.sorted(by: <)
            print("correctDate() repeatingDates: ", sortedAlarms)
            return sortedAlarms
        } else {
            return sortedAlarms
        }
    }
    
    func scheduleRepeatingDate(_ dates: [Date]) -> Date {
        let now = removeSecondFromDate(Date())
        var repeatDays: [Date] = [Date]()
        var dateAlarm: Date = Date()
        for date in dates {
            repeatDays.append(removeSecondFromDate(date))
        }
        print("correctDate() scheduleDays: ", repeatDays)
        for alarmDate in repeatDays {
            if alarmDate > now {
                dateAlarm = alarmDate
                break
            }
        }
        print("correctDate() dateToBeScheduled: ", removeSecondFromDate(dateAlarm))
        return removeSecondFromDate(dateAlarm)
    }
    
    internal static func resetTimeByCurrentDate(_ date: Date) -> Date {
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([Calendar.Component.hour, Calendar.Component.minute], from: date)
        let hour: Int? = dateComponents.hour
        let minute: Int? = dateComponents.minute
        return Calendar.current.date(bySettingHour: hour!, minute: minute!, second: 0, of: Date())!
    }
    
    public static func correctSecondComponent(date: Date, calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian))->Date {
        let second = calendar.component(.second, from: date)
        let d = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.second, value: -second, to: date, options:.matchStrictly)!
        return d
    }
    
    func setNotificationWithDate(_ date: Date, onWeekdaysForNotify weekdays:[Int], snoozeEnabled:Bool,  onSnooze: Bool, soundName: String, index: Int, snoozeDuration: Int, isBinauralEnabled: Bool) {
        setupNotificationSettings()
        print("setNotificationWithDate() soundName: \(soundName)")
        let AlarmNotification: UILocalNotification = UILocalNotification()
        AlarmNotification.alertBody = "Wake Up!"
        AlarmNotification.alertAction = "Open App"
        AlarmNotification.soundName = soundName
        AlarmNotification.timeZone = TimeZone.current
        let repeating: Bool = !weekdays.isEmpty
        AlarmNotification.userInfo = ["snooze" : snoozeEnabled, "index": index, "soundName": soundName, "repeating" : repeating, "snoozeDuration" : snoozeDuration, "isBinauralEnabled" : isBinauralEnabled]
        syncAlarmModel()
        if onSnooze {
            print("setNotificationWithDate() onSnooze")
            alarmModel.alarms[index].date = Scheduler.correctSecondComponent(date: alarmModel.alarms[index].date)
            AlarmNotification.repeatInterval = NSCalendar.Unit.minute
        }
        
        AlarmNotification.fireDate = date
        print("setNotificationWithDate() alarmNotification: \(AlarmNotification)")
        UIApplication.shared.scheduleLocalNotification(AlarmNotification)
        
        if let notifs = UIApplication.shared.scheduledLocalNotifications {
            print("setNotificationWithDate() localNotifCount: \(notifs.count)")
            for i in 0..<notifs.count {
                let notification:UILocalNotification? = notifs[i]
                print("setNotificationWithDate() scheduledNotificationDate: \(notification!.fireDate)")
            }
        }
    }
    
    func setNotificationForSnooze(snoozeMinute: Int, soundName: String, index: Int) {
        print("setNotificationForSnooze()");
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let now = Date()
        let snoozeTime = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.minute, value: snoozeMinute, to: now, options:.matchStrictly)!
        setNotificationWithDate(snoozeTime, onWeekdaysForNotify: [Int](), snoozeEnabled: true, onSnooze:true, soundName: soundName, index: index, snoozeDuration: snoozeMinute, isBinauralEnabled: false)
    }
    
    func reSchedule() {
        //cancel all and register all is often more convenient
        print("reSchedule()");
        DispatchQueue.main.async(execute: {
            UIApplication.shared.cancelAllLocalNotifications()
            self.syncAlarmModel()
            for i in 0..<self.alarmModel.count {
                let alarm = self.alarmModel.alarms[i]
                print("reSchedule() alarmDate: \(alarm.date)")
                if alarm.enabled {
                    self.setNotificationWithDate(alarm.date as Date, onWeekdaysForNotify: alarm.repeatWeekdays, snoozeEnabled: alarm.snoozeEnabled, onSnooze: false, soundName: alarm.mediaLabel, index: i, snoozeDuration: alarm.snoozeDuration, isBinauralEnabled: alarm.isBinauralEnabled)
                }
            }
        })
    }

    // workaround for some situation that alarm model is not setting properly (when app on background or not launched)
    func checkNotification() {
        alarmModel = Alarms()
        DispatchQueue.main.async(execute: {
        let notifications = UIApplication.shared.scheduledLocalNotifications
            if notifications!.isEmpty {
                for i in 0..<self.alarmModel.count {
                    self.alarmModel.alarms[i].enabled = false
                }
            }
            else {
                for (i, alarm) in self.alarmModel.alarms.enumerated() {
                    var isOutDated = true
                    if alarm.onSnooze {
                        isOutDated = false
                    }
                    for n in notifications! {
                        if alarm.date >= n.fireDate! {
                            isOutDated = false
                        }
                    }
                    if isOutDated {
                        self.alarmModel.alarms[i].enabled = false
                    }
                }
            }
        })
    }
    
    private func syncAlarmModel() {
        alarmModel = Alarms()
    }
}
