//
//  AlarmManager.swift
//  AlarmModule
//
//  Created by FPT Software on 11/05/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

import Foundation
import UIKit

@objc class AlarmManager : NSObject {
    static let NUM_DAYS = 7
    var alarmModel: Alarms = Alarms()
    var alarmScheduler: AlarmSchedulerDelegate = Scheduler()
  
    override init() {
        super.init()
        alarmModel = Alarms()
        UIApplication.shared.isIdleTimerDisabled = true
        alarmScheduler.checkNotification();
    }
    
    internal func onTimeSet(_ date: Date, vibrateEnabled:Bool, mediaLabel: String, repeatDays: [Int], snoozeDuration: Int, mediaTitle: String, isBinauralEnabled:Bool) -> Void {
        let tempAlarm = Alarm();
        tempAlarm.dateInitial = date
        tempAlarm.repeatDays = alarmScheduler.correctRepeatingDate(date, onWeekdaysForNotify: getDaysOfWeek(repeatDays))
        tempAlarm.date = !repeatDays.isEmpty ? alarmScheduler.scheduleRepeatingDate(tempAlarm.repeatDays) : alarmScheduler.correctDateForNonRepeating(date)
        tempAlarm.label = "";
        tempAlarm.enabled = true;
        tempAlarm.mediaLabel = RingtoneManager.getDefaultRingtone(mediaLabel);
        tempAlarm.mediaID = ""
        tempAlarm.snoozeEnabled = true;
        tempAlarm.repeatWeekdays = getDaysOfWeek(repeatDays);
        tempAlarm.uuid = UUID().uuidString;
        tempAlarm.onSnooze = false;
        tempAlarm.snoozeDuration = snoozeDuration;
        tempAlarm.mediaTitle = RingtoneManager.getDefaultRingtoneTitle(mediaTitle)
        tempAlarm.isBinauralEnabled = isBinauralEnabled;
        alarmModel.alarms.append(tempAlarm);
        alarmScheduler.reSchedule()
    }
    
    private func getDaysOfWeek(_ weekDays: [Int]) -> [Int] {
        var daysOfWeek = [Int]()
        for i in 0..<weekDays.count {
            daysOfWeek.append(weekDays[i] + 1)
        }
        return daysOfWeek.sorted(by: <)
    }
    
    private func setDaysOfWeek(_ weekDays: [Int]) -> [Int] {
        var daysOfWeek:[Int] = [-1, -1, -1, -1, -1 ,-1 ,-1];
        var dayInWeek: Int?
        for i in 0..<daysOfWeek.count {
            if i < weekDays.count {
                dayInWeek = weekDays[i] - 1
            }
            if let day = dayInWeek {
                daysOfWeek[day] = day
            }
        }
        return daysOfWeek
    }
    
    internal func onTimeUpdate(AtIndex index: Int, date: Date, vibrateEnabled:Bool, mediaLabel: String, repeatDays: [Int], snoozeDuration: Int, mediaTitle: String, isBinauralEnabled:Bool) -> Void {
        let tempAlarm = Alarm();
        tempAlarm.dateInitial = date
        tempAlarm.repeatDays = alarmScheduler.correctRepeatingDate(date, onWeekdaysForNotify: getDaysOfWeek(repeatDays))
        tempAlarm.date = !repeatDays.isEmpty ? alarmScheduler.scheduleRepeatingDate(tempAlarm.repeatDays) : alarmScheduler.correctDateForNonRepeating(date)
        tempAlarm.label = "";
        tempAlarm.enabled = true;
        tempAlarm.mediaLabel = mediaLabel;
        tempAlarm.mediaID = ""
        tempAlarm.snoozeEnabled = true;
        tempAlarm.repeatWeekdays = getDaysOfWeek(repeatDays);
        tempAlarm.uuid = UUID().uuidString;
        tempAlarm.onSnooze = false;
        tempAlarm.snoozeDuration = snoozeDuration;
        tempAlarm.mediaTitle = mediaTitle
        tempAlarm.isBinauralEnabled = isBinauralEnabled
        alarmModel.alarms[index] = tempAlarm;
        alarmScheduler.reSchedule();
    }
    
    internal func onTimeDelete(AtIndex alarmId: Int) -> Void {
        alarmModel.alarms.remove(at: alarmId)
        alarmScheduler.reSchedule()
    }
    
    internal func switchToggle(AtIndex alarmId: Int) -> Void {
        self.alarmModel = Alarms()
        let alarm: Alarm = alarmModel.alarms[alarmId]
        if alarm.enabled {
            alarm.enabled = false
        } else {
            print("switchToggle() resettedTime: ", Scheduler.resetTimeByCurrentDate(alarm.date))
            let date = Scheduler.resetTimeByCurrentDate(alarm.date)
            alarm.enabled = true
            alarm.repeatDays = alarmScheduler.correctRepeatingDate(date, onWeekdaysForNotify: alarm.repeatWeekdays)
            alarm.date = !alarm.repeatWeekdays.isEmpty ? alarmScheduler.scheduleRepeatingDate(alarm.repeatDays) : alarmScheduler.correctDateForNonRepeating(date)
        }
        alarmModel.alarms[alarmId] = alarm
        alarmScheduler.reSchedule()
    }
    
    internal func onSetSnooze(AtIndex alarmId: Int) -> Void {
        let alarm: Alarm = alarmModel.alarms[alarmId]
        if alarm.snoozeEnabled {
            alarmScheduler.setNotificationForSnooze(snoozeMinute: alarm.snoozeDuration, soundName: alarm.mediaLabel, index: alarmId)
        }
    }
    
    internal func cancelScheduledAlarms() -> Void {
        print("cancelScheduledAlarms()")
        self.alarmModel = Alarms()
        for i in 0..<alarmModel.count {
            let alarm = alarmModel.alarms[i]
            if alarm.enabled {
                alarm.enabled = false;
                alarmModel.alarms[i] = alarm
            }
        }
        alarmScheduler.reSchedule();
    }
    
    internal func onStopAlarm(AtIndex alarmId: Int) -> Void {
        print("onStopAlarm() alarmIndex: \(alarmId)")
        self.alarmModel = Alarms()
        let alarm: Alarm = alarmModel.alarms[alarmId]
//        alarm.onSnooze = false
        var maxLen: Int = 0
        for alarm in self.alarmModel.alarms {
            if alarm.repeatDays.count > maxLen {
                maxLen = alarm.repeatDays.count
            }
        }
        if alarm.repeatWeekdays.isEmpty {
            alarm.enabled = false;
            alarmModel.alarms[alarmId] = alarm
        } else {
            let calendar = Calendar.current
            let daysInWeek = 7
            let now: Date = Date()
            print("onStopAlarm() alarmDate: ", alarm.date)
            if alarm.repeatDays.contains(alarm.date) {
                if let index = alarm.repeatDays.index(of: alarm.date) {
                    alarm.repeatDays[index] = (calendar as NSCalendar).date(byAdding: .day, value: daysInWeek, to: alarm.date, options: .matchStrictly)!
                    print("onStopAlarm() alarmRepeatDays: ", alarm.repeatDays)
                    print("onStopAlarm() hasElementWithIndex: \(index)")
                    if index < alarm.repeatDays.endIndex {
                        print("onStopAlarm() notEndOfList: \(alarm.repeatDays.endIndex)")
                        if index == alarm.repeatDays.endIndex - 1 {
                            print("onStopAlarm() if index == endList")
                            if alarm.repeatDays[0] < now {
                                alarm.repeatDays[0] = (calendar as NSCalendar).date(byAdding: .day, value: daysInWeek, to: alarm.repeatDays[0], options: .matchStrictly)!
                            }
                            alarm.date = alarm.repeatDays[0]
                        } else {
                            print("onStopAlarm() if index != endList")
                            alarm.date = alarm.repeatDays[index + 1]
                        }
                        print("onStopAlarm() alarmDateToBeScheduled: \(alarm.date)")
                    }
                    alarmModel.alarms[alarmId] = alarm
                }
            }
        }
        alarmScheduler.reSchedule()
    }
    
    private func unwrapOptional<T>(optional: T?, defaultValue: @autoclosure () -> String) -> String {
        return optional.map { String(describing: $0) } ?? defaultValue()
    }
    
    internal func getScheduledAlarms() -> [[String: Any]] {
        var alarmList: [[String: Any]] = [[String: Any]]()
        var alarmTimeMins = "", alarmTimeHour = "", alarmMeridiem = ""
        for i in 0..<alarmModel.alarms.count {
            var tempAlarm: [String: Any] = [String: Any]()
            let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            let dateComponents = calendar.dateComponents([Calendar.Component.hour, Calendar.Component.minute], from: alarmModel.alarms[i].dateInitial)
            
            let hour: Int? = dateComponents.hour
            let minute: Int? = dateComponents.minute
            alarmTimeHour = "\(self.unwrapOptional(optional: hour, defaultValue: alarmTimeHour))"
            if var alarmHour = dateComponents.hour, alarmHour >= 12 {
                alarmMeridiem = "pm"
                alarmHour -= 12
                if alarmHour == 0 {
                    alarmTimeHour = "12"
                } else if alarmHour < 10 {
                    alarmTimeHour = "0\(self.unwrapOptional(optional: alarmHour, defaultValue: alarmTimeHour))"
                } else {
                    alarmTimeHour = "\(self.unwrapOptional(optional: alarmHour, defaultValue: alarmTimeHour))"
                }
            } else {
                if let alarmHour = hour, alarmHour == 0 {
                    alarmTimeHour = "12"
                } else if let alarmHour = hour, alarmHour < 10 {
                    alarmTimeHour = "0\(self.unwrapOptional(optional: alarmHour, defaultValue: alarmTimeHour))"
                }
                alarmMeridiem = "am"
            }
            if let minutes = dateComponents.minute, minutes < 10 {
                alarmTimeMins = "0\(minutes)"
            } else {
                alarmTimeMins = "\(minute!)"
            }
            
            tempAlarm["alarm_id"] = i
            tempAlarm["hours"] = hour
            tempAlarm["minute"] = minute
            tempAlarm["alarm_time"] = alarmTimeHour + ":" + alarmTimeMins
            tempAlarm["alarm_meridiem"] = alarmMeridiem
            tempAlarm["alarm_enabled"] = alarmModel.alarms[i].enabled
            tempAlarm["repeat_days"] = setRecurringDays(alarmModel.alarms[i].repeatWeekdays)
            tempAlarm["snooze"] = alarmModel.alarms[i].snoozeDuration
            tempAlarm["song_uri"] = alarmModel.alarms[i].mediaLabel
            tempAlarm["song_name"] = alarmModel.alarms[i].mediaTitle
            tempAlarm["rbs_enabled"] = alarmModel.alarms[i].isBinauralEnabled
            alarmList.append(tempAlarm)
        }
        return alarmList
    }
    
    private func setRecurringDays(_ daysInWeek: [Int]) -> [Bool] {
        var recurringDays:[Bool] = [false, false, false, false, false, false, false]
        let adjustedDays = setDaysOfWeek(daysInWeek)
        for i in 0..<AlarmManager.NUM_DAYS {
            if adjustedDays[i] != -1 {
                recurringDays[i] = true
            }
        }
        return recurringDays
    }
    
}
