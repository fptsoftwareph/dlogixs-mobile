//
//  AlarmSchedulerDelegate.swift
//  AlarmModule
//
//  Created by FPT Software on 16/05/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

import Foundation

protocol AlarmSchedulerDelegate {
    func setNotificationWithDate(_ date: Date, onWeekdaysForNotify weekdays:[Int], snoozeEnabled:Bool,  onSnooze: Bool, soundName: String, index: Int, snoozeDuration: Int, isBinauralEnabled: Bool)
    //helper
    func setNotificationForSnooze(snoozeMinute: Int, soundName: String, index: Int)
    func setupNotificationSettings()
    func reSchedule()
    func checkNotification()
    func correctRepeatingDate(_ date: Date, onWeekdaysForNotify weekdays: [Int]) -> [Date]
    func scheduleRepeatingDate(_ dates: [Date]) -> Date
    func correctDateForNonRepeating(_ date: Date) -> Date
}
