//
//  RingtoneManager.swift
//  AlarmModule
//
//  Created by FPT Software on 13/04/2018.
//  Copyright © 2018 FPT Software. All rights reserved.
//

import Foundation
import AudioToolbox
import AVFoundation
import MediaPlayer
import NotificationCenter

@objc class RingtoneManager : NSObject, AlarmApplicationDelegate {
    
//    var audioPlayer: AVAudioPlayer?
//    var musicPlayer: MPMusicPlayerController?
    var avPlayer: AVPlayer?
    static let DEFAULT_TONE = "Fur Elise"
    
    override init() {
        super.init()
        var error: NSError?
//        musicPlayer = MPMusicPlayerController.applicationMusicPlayer()
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
        } catch let error1 as NSError {
            error = error1
            print("could not set session. err:\(error!.localizedDescription)")
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error1 as NSError{
            error = error1
            print("could not active session. err:\(error!.localizedDescription)")
        }
    }
    
    internal func getAlarmRingtones() -> [[String: String]] {
        var ringtones = [[String: String]]()
        if let path = Bundle.main.path(forResource: "Ringtone", ofType: "plist") {
            print("getAlarmRingtones() path: \(path)")
            if let arrayOfRingtones = NSArray(contentsOfFile: path) {
                for ringtone in arrayOfRingtones {
                    ringtones.append(ringtone as! [String: String])
                }
            }
        }
        ringtones.append(contentsOf: getSoundFilesFromLibraryDir())
        return ringtones
    }
    
    private func getSoundFilesFromLibraryDir() -> [[String: String]] {
        let fileManager = FileManager.default
        var soundFiles = [[String: String]]()
        let libraryDir = fileManager.urls(for: .libraryDirectory, in: .userDomainMask)[0]
        do {
            let soundFilesURL = try fileManager.contentsOfDirectory(at: libraryDir.appendingPathComponent("Sounds/"), includingPropertiesForKeys: nil)
            for i in 0..<soundFilesURL.count {
                print("SOUND FILE: \(soundFilesURL[i].lastPathComponent)")
                let soundTitle = soundFilesURL[i].lastPathComponent.replacingOccurrences(of: ".caf", with: "")
                soundFiles.append(["alarm_title": soundTitle, "alarm_uri": soundFilesURL[i].path])
            }
        } catch {
            print("Error while enumerating files \(libraryDir.path): \(error.localizedDescription)")
        }
        return soundFiles
    }
    
    private func extractURLFromPath(_ path: String) -> URL? {
        var url: URL?
        if path.range(of: "/Library/Sounds") != nil {
            url = URL(fileURLWithPath: path)
        } else {
            let newPath = path.replacingOccurrences(of: ".caf", with: "")
            url = URL(fileURLWithPath: Bundle.main.path(forResource: newPath, ofType: "caf")!)
        }
        return url
    }
    
    static func getDefaultRingtoneTitle(_ mediaTitle: String) -> String {
        var ringtone: String
        if mediaTitle.range(of: "Default tone") != nil {
            ringtone = RingtoneManager.DEFAULT_TONE
        } else {
            ringtone = mediaTitle
        }
        return ringtone
    }
    
    static func getDefaultRingtone(_ mediaLabel: String) -> String {
        var ringtone: String
        if mediaLabel.isEmpty {
            let filePath = Bundle.main.path(forResource: RingtoneManager.DEFAULT_TONE, ofType: "caf")!
            let url = URL(fileURLWithPath: filePath)
            ringtone = url.lastPathComponent
        } else {
            ringtone = mediaLabel
        }
        print("getDefaultRingtone(): ", ringtone)
        return ringtone
    }
    
    internal func playRingtone(_ path: String) -> Void {
        print("playRingtone(): \(path)")
//        if path.range(of: ".caf") != nil {
//            if audioPlayer?.isPlaying == true {
//                stopRingtone()
//            }
//            if let urlFromPath = extractURLFromPath(path) {
//                playSound(urlFromPath)
//            }
//        } else {
//            stopMusicPlayer()
//            playMusic(path)
//        }
        
        if path.range(of: ".caf") != nil {
            if let urlFromPath = extractURLFromPath(path) {
                playMusic(urlFromPath)
            }
        } else {
            let musicURL: URL = NSURL(string: path)! as URL
            playMusic(musicURL)
        }
    }
    
//    internal func stopRingtone() -> Void {
//        audioPlayer?.stop()
//        AudioServicesRemoveSystemSoundCompletion(kSystemSoundID_Vibrate)
//    }
    
    internal func stopMusicPlayer() -> Void {
//        self.musicPlayer?.stop()
        avPlayer?.rate = 0.0
        avPlayer?.pause()
        AudioServicesRemoveSystemSoundCompletion(kSystemSoundID_Vibrate)
    }
    
    internal func setVibrate() -> Void {
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        AudioServicesAddSystemSoundCompletion(SystemSoundID(kSystemSoundID_Vibrate),nil,
                                              nil,
                                              { (_:SystemSoundID, _:UnsafeMutableRawPointer?) -> Void in
                                                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        },
                                              nil)
    }
    
    private func playMusic(_ urlPath: URL) -> Void {
        print("playMusic() urlPath: ", urlPath.absoluteURL)
//        let mediaQuery = MPMediaQuery.songs()
//        let predicateByTitle = MPMediaPropertyPredicate(value: title, forProperty: MPMediaItemPropertyTitle)
//        mediaQuery.filterPredicates = NSSet(object: predicateByTitle) as? Set<MPMediaPredicate>
//        let mediaCollection = MPMediaItemCollection(items: mediaQuery.items!)
//        //        musicPlayer = MPMusicPlayerController.iPodMusicPlayer()
//        musicPlayer?.setQueue(with: mediaCollection)
//        musicPlayer?.repeatMode = MPMusicRepeatMode.one
//        musicPlayer?.play()
        
        let urlAsset: AVURLAsset = AVURLAsset(url: urlPath)
        let statusKey = "tracks"
        urlAsset.loadValuesAsynchronously(forKeys: [statusKey], completionHandler: {
            let playerItem: AVPlayerItem = AVPlayerItem(asset: urlAsset)
            self.avPlayer = AVPlayer(playerItem: playerItem)
            self.avPlayer?.play()
            NotificationCenter.default.addObserver(self, selector: #selector(self.didFinishPlaying(_:)), name: .AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem)
        })
    }
    
    @objc func registerSessionInterruptionForNotification() -> Void {
        NotificationCenter.default.addObserver(self, selector: #selector(handleSessionInterruption(_:)), name: .AVAudioSessionInterruption, object: AVAudioSession.sharedInstance())
    }
    
    @objc private func handleSessionInterruption(_ notification: Notification) -> Void {
        guard let info = notification.userInfo,
            let typeValue = info[AVAudioSessionInterruptionTypeKey] as? UInt,
            let type = AVAudioSessionInterruptionType(rawValue: typeValue) else {
                return
        }
        if type == .ended {
            guard let optionsVal = info[AVAudioSessionInterruptionOptionKey] as? UInt else {
                return
            }
            let options = AVAudioSessionInterruptionOptions(rawValue: optionsVal)
            if options.contains(.shouldResume) {
                print("Audio Interruption Ended")
                avPlayer?.play()
                setVibrate()
            }
        }
    }
    
    @objc private func didFinishPlaying(_ notification: NSNotification) {
        print("didFinishPlaying()")
        self.avPlayer?.seek(to: kCMTimeZero)
        self.avPlayer?.play()
    }
    
    // MARK: === Alarm Application Delegate ===
    func playSound(_ soundURL: URL) {
        print("playSound(): URL: \(soundURL.absoluteString)")
//        var error: NSError?
//
//        do {
//            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
//        } catch let error1 as NSError {
//            error = error1
//            audioPlayer = nil
//        }
//
//        if let err = error {
//            print("audioPlayer error \(err.localizedDescription)")
//            return
//        } else {
//            audioPlayer!.delegate = self
//            audioPlayer!.prepareToPlay()
//        }
//        audioPlayer!.numberOfLoops = -1
//        audioPlayer!.play()
    }

}

