//
//  SMAModule.h
//  SMAModule
//
//  Created by FPT Software on 10/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface SMAModule : RCTEventEmitter <RCTBridgeModule>

@end
