//
//  SMAModule.m
//  SMAModule
//
//  Created by FPT Software on 10/11/2017.
//  Copyright © 2017 FPT Software. All rights reserved.
//

#import "SMAModule.h"
#import <React/RCTBridge.h>
#import <React/RCTConvert.h>
#import <React/RCTEventDispatcher.h>
#import "CBPeripheral+Extensions.h"
#import "BluetoothManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "SmaBLE.h"
#import "Define.h"

NSString *const USER_ID = @"1";
NSString *const UUID_CHARACTER_WRITE = @"6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
NSString *const UUID_CHARACTER_READ = @"6E400003-B5A3-F393-E0A9-E50E24DCCA9E";

@interface SMAModule() <CBCentralManagerDelegate, CBPeripheralDelegate, SmaCoreBlueToolDelegate> {
    SmaBLE *_wristband;
    NSMutableDictionary* _connectCallbacks;
    NSMutableDictionary *_bluetoothStateCallback;
    NSMutableDictionary *_sportDataCallback;
    NSMutableDictionary *_heartRateCallback;
    BOOL _hasListeners;
    NSString *_sportCallbackKey;
    NSString *_heartRateCallbackKey;
    NSDateFormatter *_tempDateFormatter;
    BOOL _isBluetoothEnabled;
}
@property (nonatomic,strong) CBCharacteristic *characteristicWrite;
@property (nonatomic,strong) CBCharacteristic *characteristicRead;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (strong, nonatomic) NSMutableSet *peripherals;
@property (strong, nonatomic) CBCentralManager *manager;
@property (strong, nonatomic) BluetoothManager *btManager;
@property (weak, nonatomic) NSTimer *scanTimer;

@end

@implementation SMAModule

RCT_EXPORT_MODULE();

@synthesize bridge = _bridge;

@synthesize manager;
@synthesize btManager;
@synthesize peripherals;
@synthesize scanTimer;

- (instancetype)init {
    if (self = [super init]) {
        peripherals = [NSMutableSet set];
        _connectCallbacks = [NSMutableDictionary new];
        _bluetoothStateCallback = [NSMutableDictionary new];
        _sportDataCallback = [NSMutableDictionary new];
        _heartRateCallback = [NSMutableDictionary new];
        _sportCallbackKey = [NSString stringWithFormat:@"%d", CUFFSPORTDATA];
        _heartRateCallbackKey = [NSString stringWithFormat:@"%d", CUFFHEARTRATE];
        _wristband = [SmaBLE sharedCoreBlue];
        btManager = [BluetoothManager sharedBluetoothMgr];
        _tempDateFormatter = [[NSDateFormatter alloc] init];
    }
    return self;
}

-(void)startObserving {
    _hasListeners = YES;
}

-(void)stopObserving {
    _hasListeners = NO;
}

- (NSArray<NSString *> *)supportedEvents {
    return @[@"SMAManagerStopScan", @"SMAManagerDiscoverPeripheral", @"SMAManagerConnectPeripheral"];
}

- (NSString *) periphalStateToString: (int)state {
    switch (state) {
        case CBPeripheralStateDisconnected:
            return @"disconnected";
        case CBPeripheralStateDisconnecting:
            return @"disconnecting";
        case CBPeripheralStateConnected:
            return @"connected";
        case CBPeripheralStateConnecting:
            return @"connecting";
        default:
            return @"unknown";
    }
}

- (NSString *) centralManagerStateToString: (int)state {
    switch (state) {
        case CBCentralManagerStateUnknown:
            return @"unknown";
        case CBCentralManagerStateResetting:
            return @"resetting";
        case CBCentralManagerStateUnsupported:
            return @"unsupported";
        case CBCentralManagerStateUnauthorized:
            return @"unauthorized";
        case CBCentralManagerStatePoweredOff:
            return @"off";
        case CBCentralManagerStatePoweredOn:
            return @"on";
        default:
            return @"unknown";
    }
}

- (CBPeripheral*)findPeripheralByUUID:(NSString*)uuid {
    CBPeripheral *peripheral = nil;
    for (CBPeripheral *p in peripherals) {
        NSString* other = p.identifier.UUIDString;
        if ([uuid isEqualToString:other]) {
            peripheral = p;
            break;
        }
    }
    return peripheral;
}

-(void)stopScanTimer:(NSTimer *)timer {
    self.scanTimer = nil;
    [manager stopScan];
    if (_hasListeners) {
        [self sendEventWithName:@"SMAManagerStopScan" body:@{}];
    }
}

#pragma mark -
#pragma mark === Core Bluetooth Central Manager Delegate ===
#pragma mark -
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    _isBluetoothEnabled = (central.state == CBCentralManagerStatePoweredOn)?YES:NO;
    switch (central.state) {
        case CBManagerStatePoweredOn:
            if ([btManager isLoggedIn]) {
                NSUUID *uuid = [[NSUUID alloc]initWithUUIDString:[btManager getSaveUUID]];
                [self connectRetrieveDevice:uuid];
            }
            break;
        case CBManagerStatePoweredOff:
            if ([btManager isLoggedIn]) {
                RCTResponseSenderBlock btStateCallback = [_bluetoothStateCallback valueForKey:[btManager getSaveUUID]];
                if (btStateCallback) {
                    btStateCallback(@[@"Please Enable Bluetooth Again to Retrieve Connected Peripheral"]);
                    [_bluetoothStateCallback removeObjectForKey:[btManager getSaveUUID]];
                }
            }
            break;
    }
}

-(void)connectRetrieveDevice:(NSUUID *)uuid {
    if (uuid != nil) {
        NSArray<CBPeripheral *> *peripheralArray = [manager retrievePeripheralsWithIdentifiers:@[uuid]];
        if([peripheralArray count] > 0) {
            self.peripheral = [peripheralArray objectAtIndex:0];
            self.peripheral.delegate = self;
            [manager connectPeripheral:self.peripheral options:nil];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI {
    
    if ([peripheral name].length) {
        [peripherals addObject:peripheral];
        [peripheral setAdvertisementData:advertisementData RSSI:RSSI];
        NSLog(@"Discover peripheral: %@", [peripheral name]);
        if (_hasListeners) {
            [self sendEventWithName:@"SMAManagerDiscoverPeripheral" body:[peripheral asDictionary]];
        }
    }
}

-(void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *,id> *)dict {
    NSLog(@"centralManager willRestoreState");
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"Peripheral Connected: %@", peripheral);
    // The state of the peripheral isn't necessarily updated until a small delay after didConnectPeripheral is called
    // and in the meantime didFailToConnectPeripheral may be called
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.002 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        // didFailToConnectPeripheral should have been called already if not connected by now
        [btManager saveDeviceUUID:[peripheral uuidAsString]];
        RCTResponseSenderBlock connectCallback = [_connectCallbacks valueForKey:[peripheral uuidAsString]];
        if (connectCallback) {
            connectCallback(@[[NSNull null], [peripheral asDictionary]]);
            [_connectCallbacks removeObjectForKey:[peripheral uuidAsString]];
        }
        if ([btManager isLoggedIn]) {
            RCTResponseSenderBlock btStateCallback = [_bluetoothStateCallback valueForKey:[btManager getSaveUUID]];
            if (btStateCallback) {
                btStateCallback(@[@"Successfully connected!"]);
                [_bluetoothStateCallback removeObjectForKey:[btManager getSaveUUID]];
            }
        }
        if (_hasListeners) {
            [self sendEventWithName:@"SMAManagerConnectPeripheral" body:@{@"peripheral": [peripheral uuidAsString]}];
        }
        [self discoverServices:peripheral];
    });
}

- (void)discoverServices:(CBPeripheral *)peripheral {
    if (peripheral && peripheral.state == CBPeripheralStateConnected) {
        [peripheral discoverServices:nil];
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSString *errorStr = [NSString stringWithFormat:@"Peripheral connection failure: %@. (%@)", peripheral, [error localizedDescription]];
    RCTResponseSenderBlock connectCallback = [_connectCallbacks valueForKey:[peripheral uuidAsString]];
    
    if (connectCallback) {
        connectCallback(@[errorStr]);
        [_connectCallbacks removeObjectForKey:[peripheral uuidAsString]];
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"Peripheral Disconnected: %@", [peripheral uuidAsString]);
    if (error) {
        NSLog(@"Error: %@", error);
    }
    NSString *peripheralUUIDString = [peripheral uuidAsString];
    NSString *errorStr = [NSString stringWithFormat:@"Peripheral did disconnect: %@", peripheralUUIDString];
    [btManager logout];
    [_wristband logOut];
    RCTResponseSenderBlock connectCallback = [_connectCallbacks valueForKey:peripheralUUIDString];
    if (connectCallback) {
        connectCallback(@[errorStr]);
        [_connectCallbacks removeObjectForKey:peripheralUUIDString];
    }
    RCTResponseSenderBlock sportCallback = [_sportDataCallback valueForKey:_sportCallbackKey];
    if (sportCallback) {
        sportCallback(@[errorStr]);
        [_sportDataCallback removeObjectForKey:_sportCallbackKey];
    }
    RCTResponseSenderBlock heartRateCallback = [_heartRateCallback valueForKey:_heartRateCallbackKey];
    if (heartRateCallback) {
        heartRateCallback(@[errorStr]);
        [_heartRateCallback valueForKey:_heartRateCallbackKey];
    }
}

#pragma mark -
#pragma mark === Core Bluetooth Peripheral Delegate ===
#pragma mark -
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (error) {
        NSLog(@"Error: %@", error);
        return;
    }
    NSMutableSet *servicesForPeriperal = [NSMutableSet new];
    [servicesForPeriperal addObjectsFromArray:peripheral.services];
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:nil forService:service]; // discover all is slow
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    for (CBCharacteristic * characteristic in service.characteristics) {
        if ([characteristic.UUID.UUIDString isEqualToString:UUID_CHARACTER_WRITE])
            self.characteristicWrite = characteristic;
        if ([characteristic.UUID.UUIDString isEqualToString:UUID_CHARACTER_READ]) {
            self.characteristicRead = characteristic;
            [_wristband.p readValueForCharacteristic:characteristic];
            [_wristband.p setNotifyValue:YES forCharacteristic:characteristic];
        }
    }
    _wristband.p = peripheral;
    _wristband.delegate = self;
    _wristband.Write = self.characteristicWrite;
    if ([btManager isLoggedIn] && _wristband.p.state == CBPeripheralStateConnected) {
        [_wristband LoginUserWithUserID:USER_ID];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        return;
    }
    [_wristband handleResponseValue:characteristic];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didWrite");
}

- (void)peripheral:(CBPeripheral*)peripheral didReadRSSI:(NSNumber*)rssi error:(NSError*)error {
    NSLog(@"didReadRSSI %@", rssi);
}

#pragma mark -bleDataParsingWithMode:
#pragma mark === SmaCore Delegate ===
#pragma mark -
-(void)bleDataParsingWithMode:(SMA_INFO_MODE)mode dataArr:(NSMutableArray *)array Checkout:(BOOL)check {
    switch (mode) {
        case CUFFSPORTDATA: {
            NSDictionary *tempSport = [NSDictionary dictionaryWithDictionary:[array objectAtIndex:[array count]-1]];
            NSLog(@"SPORTS DATA: %@", tempSport);
            [self parseSMASport:tempSport];
            break;
        }
        case CUFFHEARTRATE: {
            NSDictionary *tempHeartRate = [NSDictionary dictionaryWithDictionary:[array objectAtIndex:[array count]-1]];
            NSLog(@"HEARTRATE DATA: %@", tempHeartRate);
            [self parseSMAHeartRate:tempHeartRate];
            break;
        }
    }
}

-(NSDate *)convertToUnixTime:(NSString *)strDate {
    [_tempDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *date = [_tempDateFormatter dateFromString:strDate];
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger gmtOffset = [tz secondsFromGMTForDate:date];
    NSDate *localTZ = [NSDate dateWithTimeInterval:gmtOffset sinceDate:date];
    return localTZ;
}

- (void)parseSMASport:(NSDictionary *)sport {
    NSArray *callbackArr = [_sportDataCallback valueForKey:_sportCallbackKey];
    @try {
        [_wristband setUserMnerberInfoWithHeight:0.0f weight:[[callbackArr objectAtIndex:2] floatValue] sex:[[callbackArr objectAtIndex:3] integerValue] age:0];
        RCTResponseSenderBlock sportCallback = [callbackArr objectAtIndex:0];
        NSMutableDictionary *sportMap = [[NSMutableDictionary alloc] init];
        NSDate *sportDate = [self convertToUnixTime:[sport objectForKey:@"DATE"]];
        [sportMap setObject:[NSString stringWithFormat:@"%@",sportDate] forKey:@"date"];
        [sportMap setObject:@([[sport objectForKey:@"STEP"] intValue]) forKey:@"step"];
        float calories = [_wristband countCalWithSex:[NSString stringWithFormat:@"%ld", [[callbackArr objectAtIndex:3] integerValue]] userWeight:[NSString stringWithFormat:@"%f", [[callbackArr objectAtIndex:2] floatValue]] step:[NSString stringWithFormat:@"%d", [[sport objectForKey:@"STEP"] intValue]]];
        [sportMap setObject:@(calories) forKey:@"calorie"];
        sportCallback(@[sportMap]);
    } @catch(NSException *e) {
        RCTResponseSenderBlock errorCallback = [callbackArr objectAtIndex:1];
        errorCallback(@[[e reason]]);
    }
    [_sportDataCallback removeObjectForKey:_sportCallbackKey];
}

- (void)parseSMAHeartRate:(NSDictionary *)heartRate {
    NSArray *callbackArr = [_heartRateCallback valueForKey:_heartRateCallbackKey];
    @try {
        RCTResponseSenderBlock heartRateCallback = [callbackArr objectAtIndex:0];
        NSMutableDictionary *heartMap = [[NSMutableDictionary alloc] init];
        NSDate *heartRateDate = [self convertToUnixTime:[heartRate objectForKey:@"DATE"]];
        [heartMap setObject:[NSString stringWithFormat:@"%@",heartRateDate] forKey:@"date"];
        [heartMap setObject:@([[heartRate objectForKey:@"HEART"] intValue]) forKey:@"heartRate"];
        heartRateCallback(@[heartMap]);
    } @catch(NSException *e) {
        RCTResponseSenderBlock errorCallback = [callbackArr objectAtIndex:1];
        errorCallback(@[[e reason]]);
    }
    [_heartRateCallback removeObjectForKey:_heartRateCallbackKey];
}

#pragma mark -
#pragma mark === React-Natrive Export Method ===
#pragma mark -

#pragma mark === RCT start ===
RCT_EXPORT_METHOD(start:(NSDictionary *)options errorCallback:(nonnull RCTResponseSenderBlock)errorCallback) {
    [self initializeCentralMgr:options];
    if ([btManager isLoggedIn]) {
        [_bluetoothStateCallback setObject:errorCallback forKey:[btManager getSaveUUID]];
    }
}

- (void)initializeCentralMgr:(NSDictionary *)options {
    NSMutableDictionary *initOptions = [[NSMutableDictionary alloc] init];
    if ([[options allKeys] containsObject:@"showAlert"]){
        [initOptions setObject:[NSNumber numberWithBool:[[options valueForKey:@"showAlert"] boolValue]]
                        forKey:CBCentralManagerOptionShowPowerAlertKey];
    }
    if ([[options allKeys] containsObject:@"restoreIdentifierKey"]) {
        [initOptions setObject:[options valueForKey:@"restoreIdentifierKey"]
                        forKey:CBCentralManagerOptionRestoreIdentifierKey];
    }
    [btManager initBTMgrWithDelegate:self queue:dispatch_get_main_queue() options:initOptions];
    manager = btManager.btMgr;
}

RCT_EXPORT_METHOD(openBluetoothSettings:(RCTResponseSenderBlock)callback) {
    NSLog(@"iOS: trying to open settings");
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"] options:@{} completionHandler:nil];
        });
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
    callback(@[[NSNull null]]);
}

#pragma mark === RCT scan ===
RCT_EXPORT_METHOD(scan:(NSArray *)serviceUUIDStrings
        timeoutSeconds:(nonnull NSNumber *)timeoutSeconds
       allowDuplicates:(BOOL)allowDuplicates
               options:(nonnull NSDictionary*)scanningOptions
              callback:(nonnull RCTResponseSenderBlock)callback) {
    
    NSArray * services = [RCTConvert NSArray:serviceUUIDStrings];
    NSDictionary *options = nil;
    if (allowDuplicates){
        options = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
    }
    [self scanPeripheralsWithServices:services option:options];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.scanTimer = [NSTimer scheduledTimerWithTimeInterval:[timeoutSeconds floatValue] target:self selector:@selector(stopScanTimer:) userInfo: nil repeats:NO];
    });
    callback(@[]);
}

- (void)scanPeripheralsWithServices:(NSArray *)services option:(NSDictionary *)options{
    NSMutableArray *serviceUUIDs = [NSMutableArray new];
    for (int i = 0; i < [services count]; i++) {
        CBUUID *serviceUUID =[CBUUID UUIDWithString:[services objectAtIndex: i]];
        [serviceUUIDs addObject:serviceUUID];
    }
    [manager scanForPeripheralsWithServices:serviceUUIDs options:options];
}

#pragma mark === RCT stop scan ===
RCT_EXPORT_METHOD(stopScan:(nonnull RCTResponseSenderBlock)callback) {
    if (self.scanTimer) {
        [self.scanTimer invalidate];
        self.scanTimer = nil;
    }
    [manager stopScan];
    callback(@[[NSNull null]]);
}

#pragma mark === RCT connect ===
RCT_EXPORT_METHOD(connect:(NSString *)peripheralUUID
            errorCallback:(nonnull RCTResponseSenderBlock)errorCallback
            successCallback:(nonnull RCTResponseSenderBlock)successCallback) {
    
    CBPeripheral *peripheral = [self findPeripheralByUUID:peripheralUUID];
    if (peripheral == nil)
        peripheral = [self retrievePeripheralWithUUID:peripheralUUID errorCallback:errorCallback];
    [self connectPeripheral:peripheral errorCallback:errorCallback successCallback:successCallback];
}

- (void)connectPeripheral:(CBPeripheral *)peripheral errorCallback:(nonnull RCTResponseSenderBlock)errorCallback successCallback:(RCTResponseSenderBlock)successCallback {
    if (peripheral) {
        [btManager connectPeripheral:peripheral];
        self.peripheral = btManager.peripheral;
        self.peripheral.delegate = self;
        if (successCallback)
            [_connectCallbacks setObject:successCallback forKey:[self.peripheral uuidAsString]];
        [manager connectPeripheral:self.peripheral options:nil];
    } else {
        NSString *error = [NSString stringWithFormat:@"Could not find peripheral %@.", peripheral];
        NSLog(@"%@", error);
        errorCallback(@[error, [NSNull null]]);
    }
}

- (CBPeripheral *)retrievePeripheralWithUUID:(NSString *)peripheralUUID errorCallback:(nonnull RCTResponseSenderBlock)errorCallback {
    CBPeripheral *peripheral;
    NSUUID *uuid = [[NSUUID alloc]initWithUUIDString:peripheralUUID];
    if (uuid != nil) {
        NSArray<CBPeripheral *> *peripheralArray = [manager retrievePeripheralsWithIdentifiers:@[uuid]];
        if([peripheralArray count] > 0) {
            peripheral = [peripheralArray objectAtIndex:0];
            [peripherals addObject:peripheral];
        } else {
            NSString *error = [NSString stringWithFormat:@"No retrieved peripherals %@", peripheralUUID];
            errorCallback(@[error, [NSNull null]]);
        }
        return peripheral;
    } else {
        NSString *error = [NSString stringWithFormat:@"Wrong UUID format %@", peripheralUUID];
        errorCallback(@[error, [NSNull null]]);
        return nil;
    }
}

#pragma mark === RCT disconnect ===
RCT_EXPORT_METHOD(disconnect:(nonnull RCTResponseSenderBlock)callback) {
    if (self.peripheral) {
        if (self.peripheral.services != nil)
            [self disableCharacteristicNotification:self.peripheral];
        [manager cancelPeripheralConnection:self.peripheral];
    }
}

-(void)disableCharacteristicNotification:(CBPeripheral *)peripheral {
    for (CBService *service in peripheral.services) {
        [self traverseServiceCharacteristics:service peripheral:peripheral];
    }
}

-(void)traverseServiceCharacteristics:(CBService *)service peripheral:(CBPeripheral *)peripheral{
    if (service.characteristics != nil) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            if (characteristic.isNotifying) {
                [peripheral setNotifyValue:NO forCharacteristic:characteristic];
            }
        }
    }
}

#pragma mark === RCT read sport activity ===
RCT_EXPORT_METHOD(onReadSport:(float)weight sex:(NSInteger)sex successCallback:(nonnull RCTResponseSenderBlock)successCallback errorCallback:(nonnull RCTResponseSenderBlock)errorCallback) {
    [_wristband setSystemTime];
    if (_isBluetoothEnabled) {
        [_wristband requestCuffSportData];
        [_sportDataCallback setObject:@[successCallback, errorCallback, @(weight), @(sex)] forKey:_sportCallbackKey];
    } else {
        errorCallback(@[@"unable to fetch sports data"]);
    }
}

#pragma mark === RCT read heart rate activity ===
RCT_EXPORT_METHOD(onReadHeartRate:(nonnull RCTResponseSenderBlock)successCallback errorCallback:(nonnull RCTResponseSenderBlock)errorCallback) {
    if (_isBluetoothEnabled) {
        [_wristband requestCuffHRData];
        [_heartRateCallback setObject:@[successCallback, errorCallback] forKey:_heartRateCallbackKey];
    } else {
        errorCallback(@[@"unable to fetch heart rate data"]);
    }
}

#pragma mark === RCT check bt state ===
RCT_EXPORT_METHOD(checkState:(nonnull RCTResponseSenderBlock)callback) {
    _isBluetoothEnabled?callback(@[@"on"]):callback(@[@"off"]);
}

RCT_EXPORT_METHOD(getDiscoveredPeripherals:(nonnull RCTResponseSenderBlock)callback) {
    NSMutableArray *discoveredPeripherals = [NSMutableArray array];
    @synchronized(peripherals) {
        for(CBPeripheral *peripheral in peripherals){
            NSDictionary * obj = [peripheral asDictionary];
            [discoveredPeripherals addObject:obj];
        }
    }
    callback(@[[NSArray arrayWithArray:discoveredPeripherals]]);
}

@end
