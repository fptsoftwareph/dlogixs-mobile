//
//  SmaSeatInfo.h
//  SmaLife
//
//  Created by 有限公司 深圳市 on 15/4/14.
//  Copyright (c) 2015年 SmaLife. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmaSeatInfo : NSObject<NSCoding>
//Open or not  0：close  1:open
@property (nonatomic,strong) NSString *isOpen;
//Steps threshold （0~65535 step）
@property (nonatomic,strong) NSString *stepValue;
//Detection period（0~255 min）
@property (nonatomic,strong) NSString *seatValue;
//Start time
@property (nonatomic,strong) NSString *beginTime0;
//End time
@property (nonatomic,strong) NSString *endTime0;
//是否开启提醒0
@property (nonatomic,strong) NSString *isOpen0;
//Start time
@property (nonatomic,strong) NSString *beginTime1;
//End time
@property (nonatomic,strong) NSString *endTime1;
//是否开启提醒1
@property (nonatomic,strong) NSString *isOpen1;
//week Repeat
@property (nonatomic,strong) NSString *repeatWeek;

@end
