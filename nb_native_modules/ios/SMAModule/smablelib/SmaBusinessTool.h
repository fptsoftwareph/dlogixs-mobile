

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
@interface SmaBusinessTool : NSObject

+(void)getSpliceCmd:(Byte)cmd Key:(Byte)key bytes1:(Byte [])bytes1 len:(int)len results:(Byte [])results;
+(void)getSpliceCmd1:(Byte)cmd Key:(Byte)key bytes1:(Byte [])bytes1 len:(int)len results:(Byte [])results;
+(void)copyValue:(Byte[])bytes len:(Byte)len dataBytes:(Byte[])dataBytes len1:(int)len1;
+(void)getSpliceCmdBand:(Byte)cmd Key:(Byte)key bytes1:(Byte [])bytes1 len:(int)len results:(Byte [])results;
+ (void)setBinData:(Byte [])data result:(Byte [])result byteInt:(int)byInt;
/**
 *  <#Description#> CRC16    Inspection method
 *
 *   @param arrByte    Incoming the byte array need be checked by CRC
 *
 *  @return  Back to the results of the CRC validation true false;
 */
+(BOOL)checkCRC16:(Byte [])arrByte;
/**
 *  <#Description#> CRC16  Inspection method
 *
 *  @param arrByte  Incoming the byte array need be checked by CRC
 *
 *  @return  Back to the results of the CRC validation
 */
+(void)getCRC16:(Byte [])arrByte;

/**
 *  <#Description#> If response signal
 *
 *  @param bytes Bluetooth device back to the result of the byte array
 *
 *  @return  Return to the result of judge
 */
+(BOOL)checkNckBytes:(Byte [])bytes;
+(void)setAckCmdSeqId:(int16_t)seqId peripheral:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic;
+(void)setNackCmdSeqId:(int16_t)seqId peripheral:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic;
+(NSData *)getOTAdata;
+ (void)setSerialNum;
@end
