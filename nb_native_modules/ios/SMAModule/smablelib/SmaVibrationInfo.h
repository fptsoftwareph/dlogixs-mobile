

#import <Foundation/Foundation.h>

@interface SmaVibrationInfo : NSObject
/*
 @“1”  Binding
 @“2”  Binding completed
 @“3”  Message notification
 @“4”  Incoming call notification
 @“5”  Clock alarm
 @“6”  Sedentary alarm
 @“7”  Remote Capture
 @“8”  Anti-lost
 @“9”  Aerobic exercise
 @“10” Anaerobic exercise
 */
@property (nonatomic, strong) NSString *type;  //Vibration type
@property (nonatomic, strong) NSString *level; //Reserved( wait for be added)
@property (nonatomic, strong) NSString *freq;  //Vibration frequency（0~15）
@end
