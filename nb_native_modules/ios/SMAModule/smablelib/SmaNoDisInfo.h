//
//  SmaNoDisInfo.h
//  SmaLife
//
//  Created by 有限公司 深圳市 on 16/4/21.
//  Copyright © 2016年 SmaLife. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmaNoDisInfo : NSObject
//Open or not   0: close  1:open
@property (nonatomic,strong) NSString *isOpen;
//Begin time
@property (nonatomic,strong) NSString *beginTime1;
//End time
@property (nonatomic,strong) NSString *endTime1;
//Open or not   0: close  1:open
@property (nonatomic,strong) NSString *isOpen1;
//Begin time
@property (nonatomic,strong) NSString *beginTime2;
//End time
@property (nonatomic,strong) NSString *endTime2;
//Open or not   0: close  1:open
@property (nonatomic,strong) NSString *isOpen2;
//Begin time
@property (nonatomic,strong) NSString *beginTime3;
//End time
@property (nonatomic,strong) NSString *endTime3;
//Open or not   0: close  1:open
@property (nonatomic,strong) NSString *isOpen3;
//Repeat week
@property (nonatomic,strong) NSString *repeatWeek;
@end
