//
//  SmaBLE.h
//  SmaBLE
//
//  Created by 有限公司 深圳市 on 16/1/15.
//  Copyright © 2016年 SmaLife. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "SmaSeatInfo.h"
#import "SmaHRHisInfo.h"
#import "SmaAlarmInfo.h"
#import "SmaBusinessTool.h"
#import "SmaNoDisInfo.h"
#import "SmaVibrationInfo.h"
typedef enum {
    BAND =0,      //connnect
    LOGIN ,       //login
    SPORTDATA,    //activity data
    SLEEPDATA,    //sleep data
    SLEEPSETDATA, //sleep setting data
    SETSLEEPAIDS, //Setting sleep auxiliary detection（07）
    ALARMCLOCK,   //alarm list
    SYSTEMTIME,   //system time
    ELECTRIC,     //battery power
    VERSION,      // Syestem version
    OTA,          //whether enter into OTA or not
    BOTTONSTYPE,  //Buttom for return
    MAC,          //MAC back
    WATHEARTDATA, //Watch heart rate back
    CUFFSPORTDATA   ,  //Activity data（07）
    CUFFHEARTRATE    ,  //Heart rate data(05、07)
    CUFFSLEEPDATA,     //Sleep data（07）
    WATCHFACE,     //Obtain watchfaces（10-A）
    XMODEM,          //XM0DEM mode（10-A）
    RUNMODE,         //exercise  Mode
    NOTIFICATION,    //Change notification
    GOALCALLBACK,   // Return of R1 series device moving target
    LONGTIMEBACK,   //Return of sedentary setting
    FINDPHONE,      //Find the phone
}SMA_INFO_MODE;

@protocol SmaCoreBlueToolDelegate <NSObject>

@optional
/*Send instruction sequence number identify, it is used to identify response instructions data serial number
 @param identifier Received the response data feedback instruction sequence number identify
 @discussion       When send an instruction to the device (Such as set the system time instruction: setSystemTime), When the application calls:handleResponseValue: then call-back, After the instruction is sent  the serialNum and identifier are consistent
 */
- (void)sendIdentifier:(int)identifier;

/*Processing device feedback data
 @param mode     Data belong to the type of enumeration: SMA_INFO_MODE
 @param array    An array of data processing, Combination type view DEMO
 @discussion      Verify successful or not. If verify failed, don’t need to deal with the data. The device will resend the data again after 8 second, no more than 3 time. If check result stays No, need to reissue the request.
 */
- (void)bleDataParsingWithMode:(SMA_INFO_MODE)mode dataArr:(NSMutableArray *)array Checkout:(BOOL)check;

/*Update schedule(Watchface)
 */
- (void)updateProgress:(float)pregress;

- (void)updateProgressEnd:(BOOL)success;
@end

@interface SmaBLE : NSObject
@property (nonatomic, assign, readonly) NSInteger serialNum;// Send instruction sequence number identify
@property (nonatomic,strong)  CBPeripheral*p;// connect device
@property (nonatomic,strong)  CBCharacteristic*Write;// connect device characteristics
@property (nonatomic, weak) id<SmaCoreBlueToolDelegate> delegate;
+ (instancetype)sharedCoreBlue;// found SmaBLE object

/* processing device feedback data
 @param characteristic   device update data characteristics
 @discussion             When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling
 */
- (void)handleResponseValue:(CBCharacteristic *)characteristic;

/* connect watch
 @param userid   Currently support pure number, such as phone number
 @discussion     To the device registration userid ID, In order to achieve normal data transmission ( activity, sleep data)
 */
-(void)bindUserWithUserID:(NSString *)userid;//

/*disconnect
 @discussion   disconnect, Remove user data of the device internal setting
 */
-(void)relieveWatchBound;//

/*log in command
 @param userid   Currently support pure number, such as phone number
 @discussion     The log in user ID must be the connected user ID, otherwise cause data won't be able to sync        （activity，sleep data）
 */
-(void)LoginUserWithUserID:(NSString *)userid;//

/* log out
 @discussion   logged off, Can lead to device disconnect for a short time, device won’t auto upload activity, sleep data
 */
-(void)logOut;//

/*Set User Information
 @param he    height（cm）Decimal points can only take 0 or 0.5；
 @param we    weight (kg) Decimal points can only take 0 or 0.5；
 @param sex   gender (0: female，1：male)
 @param age   age （0~127）
 @discussion  To the device set user height, weight, gender and age information
 */
-(void)setUserMnerberInfoWithHeight:(float)he weight:(float)we sex:(int)sex age:(int)age;//

/* set system time
 @discussion  set mobile phone current time to device
 */
-(void)setSystemTime;//

/*set anti-lost
 @param bol   YES:ON；NO:OFF
 */
-(void)setDefendLose:(BOOL)bol;//

/** Camera switch
 *  @param bol   YES:ON；NO:OFF
 */
- (void)setBLcomera:(BOOL)bol;

/*Phone calls
 @param bol YES:ON；NO:OFF
 */
-(void)setphonespark:(BOOL)bol;//

/*Message
 @param bol YES:ON；NO:OFF
 */
-(void)setSmspark:(BOOL)bol;//

/*Set pedometer goal
 @param count  Steps
 */
-(void)setStepNumber:(int)count;//

/*No disturb settings(05)
 @param bol YES:Open；NO:Close
 */
- (void)setNoDisturb:(BOOL)bol;

/*backlight settings(05、10)
 @param time Backlight duration（0~10s）
 */
- (void)setBacklight:(int)time;//

/*Vibration settings（05、10）
 @param freq  Vibration frequency（0~10次）
 */
- (void)setVibrationFrequency:(int)freq;//

/*Vibration settings（07、10）
 @param info refer to SmaVibrationInfo
 */
- (void)setVibration:(SmaVibrationInfo *)info;//

/*set sedentary
 @param week  The incoming type is @"0~127". Analysis：124  which is “1111100” of the decimal;//Delegates are open Monday through Friday, closed on Saturdays and Sundays
 @param begin start time
 @param end   end time
 @param seat  Sedentary time(0~255)  Such as set up for 5 minutes, will start at 0 minutes, once every 5 minutes will check sedentary condition,  it is not check sedentary since set up time after 5 minutes situation
 */
-(void)seatLongTimeWithWeek:(NSString *)week beginTime:(int)begin endTime:(int)end seatTime:(int)seat;

/*Close the sedentary
 */
-(void)closeLongTimeInfo;

/*  Sedentary alarm V2
 @param info refer to SmaSeatInfo
 */
-(void)seatLongTimeInfoV2:(SmaSeatInfo *)info;//

/*Set alarm
 @param smaACs   The alarm clock arrays，objects of type SmaAlarmInfo
 @discussion   Clock alarm attribute refer to SmaAlarmInfo description, the number of 02,04,05 alarm can set no more than 5, the number of 07 alarm no more than 8.
 */
-(void)setCalarmClockInfo:(NSMutableArray *)smaACs;//

/*Set alarmV2
 @param smaACs   The alarm clock arrays，objects of type SmaAlarmInfo
 @discussion   Clock alarm attribute refer to SmaAlarmInfo description, the number of 02,04,05 alarm can set no more than 5, the number of 07 alarm no more than 8.
 */
-(void)setClockInfoV2:(NSMutableArray *)smaACs;//

/*Data Synchronism
 @param bol YES:ON；NO:OFF
 @discussion   The user must ensure that the device successful login and  open synchronization, then synchronization activity, sleep data (Only suitable for 02,04,05 device)
 */
-(void)Syncdata:(BOOL)bol;//

/*Set OTA
 @discussion    Enter OTA status, for upgrade device software, please refer to IOS-nRF-Toolbox-master.zip，the technology support website is https://developer.nordicsemi.com/nRF51_SDK/nRF51_SDK_v8.x.x/doc/8.0.0/s110/html/a00103.html
 */
-(void)setOTAstate;//

/*Set APP data
 @param cal    Calories（cal）
 @param metre  Distance（m）
 @param step   Steps
 @discussion   Set the calories, distance, steps data to device（Only suitable for 02,04,05 device）
 */
- (void)setAppSportDataWithcal:(float)cal distance:(int)metre stepNnumber:(int)step;//

/*set language(The bracelet changes only weekly)
 @param lanNumber  0：chinese（fit coachKorean）  1：english 2：Turkish 3：Not defined 4：Russian 5：Spanish 6:italian 7:korean
 */
- (void)setLanguage:(int)lanNumber;//

/*Setting heart rate（07）
 @parmar info  Heart rate setting type (refer to the type parameters)
 
 */
- (void)setHRWithHR:(SmaHRHisInfo *)info;//

/*Setting gesture wake up（07）
 @parmar open  Yes—turn on, No---turn off
 */
- (void)setLiftBright:(BOOL)open;//

/*Vertical screen settings（07）
 @parmar open    Yes—turn on, No---turn off
 */
- (void)setVertical:(BOOL)open;//

/* No disturb settings（07）
 @parmar info  No disturb type (refer to the type parameters)
 */
- (void)setNoDisInfo:(SmaNoDisInfo *)info;//

/*Setting sleep auxiliary detectio
 @parmar open  YES：Turn on，NO： Turn off
 */
- (void)setSleepAIDS:(BOOL)open;//

/*BEACON Broadcast settings
 @parmar interval(0~225min) time(0~225s)
 */
- (void)setRadioInterval:(int)interval Continuous:(int)time;//

/*12/24 hours formatsettings(No heart versio)
 @parmar hourly  YES 12 hours formats，NO 24 hours formats
 */
- (void)setHourly:(BOOL) hourly;//

/*Metric setting
 @parmar british  YES Imperial，NO Metric
 */
- (void)setBritishSystem:(BOOL)british;//

/*High-speed mode
 @parmar open  YES：ON，NO：OFF
 */
- (void)setHighSpeed:(BOOL)open;//

/*Stop time proofreading
 @discussion Stop setSystemTiming and setCustomTimingHour:minute:second:
 */
- (void)setStopTiming;

/*Name & Team settings
 @param name Name
 @param group Team
 @discussion i-Med Extra port for customized project
 */
- (void)setNickName:(NSString *)name group:(NSString *)group;

/**
 @discussion Pair request （If the device is not paired, the pairing window will pop up. If the device is paired, this port will not work. Suggest to invoke this port every time after connecting successfully.）
 */
- (void)setPairAncs;

/*Prepare time proofreading
 @discussion When the pointer is working properly, stop the pointer to prepare time proofreading
 */
- (void)setPrepareTiming;

/*Cancel time proofreading
 @discussion Before time proofreading ( did not call setSystemTiming or setCustomTimingHour:minute:second:),recover the pointer to work properly.
 */
- (void)setCancelTiming;

/* pointer position
 @param hour    hour hand position（0~11）
 @param min     minute hand position（0~59）
 @param second  second hand position（0~59）
 */
- (void)setPointerHour:(int)hour minute:(int)min second:(int)second;

/*Proofreading according to system time
 @discussion Proofreading according to system time
 */
- (void)setSystemTiming;

/* Specify time to proofread time
 @param hour    hour hand position（0~11）
 @param min     minute hand position（0~59）
 @param second  second hand position（0~59）
 */
- (void)setCustomTimingHour:(int)hour minute:(int)min second:(int)second;

/*Ask for the 07 activity data
 @discussion return request data max groups no more than 20, if more than 20 groups, need to reissue the request until it less than 20 groups
 */
-(void)requestCuffSportData;//

/*Ask for sleep data
 @discussion  return request data max groups no more than 20, if more than 20 groups, need to reissue the request until it less than 20 groups
 */
- (void)requestCuffSleepData;//

/*Ask for heart rate data
 @discussion return request data max groups no more than 20, if more than 20 groups, need to reissue the request until it less than 20 groups
 */
- (void)requestCuffHRData;//

/*Ask for last time detect heart rate
 @discussion The detect heart rate value is the last time value of SM05 smart watch last
 */
- (void)requestLastHRData;//

/**Find the device
 Description Find the device and the device will play music according to buzzing intensity
 
 @param intensity  Buzzing intensity(intensity=0 no beep (Close); Intensity=1 medium; Intensity =2 High)
 */
- (void)requestFindDeviceWithBuzzing:(int)intensity;

/*Reset the watch
 @discussion Restart the watch
 */
- (void)BLrestoration;//

/*test mode
 @param on YES: Enter，NO： Exit
 @discussion  Can test the LED and the motor working condition（Only suitable for 02,04 device）
 */
- (void)enterTextMode:(BOOL)on;//

/*light up LED
 @discussion  Must enter test mode
 */
- (void)lightLED;//

/*vibrating motor
 @discussion  Must enter test mode
 */
- (void)vibrationMotor;//

/* Access to the alarm list
 @discussion   When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling: handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
-(void)getCalarmClockList;//

/*Access to the alarm list（07）
 @discussion  When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling: handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
-(void)getCuffCalarmClockList;//

/*Request activity data
 @discussion   When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling:handleResponseValue: triggered  bleDataParsingWithMode: dataArr  abeyance
 */
-(void)requestExerciseData;

/*Access to watch time
 @discussion   When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling:handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
- (void)getWatchDate;//

/*Access to electricity
 @discussion   When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling:handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
-(void)getElectric;//

/* Get the bluetooth hardware version
 @discussion   When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling:handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
- (void)getBLVersion;//

/*Obtain the MAC address
 @discussion   When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling:handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
- (void)getBLmac;//

/*Obtain watchface number（10）
 */
- (void)getSwitchNumber;//

/**Acquire target steps of user (R1 series)
 Description When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling:handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
- (void)getGoal;


/**Acquire sedentary setting information (R1 series) 
 Description When the application is triggered:peripheral:didUpdateValueForCharacteristic:error: next to calling:handleResponseValue: triggered  bleDataParsingWithMode: dataArr
 */
- (void)getLongTime;

/*X0MDEM Mode（10-A）
 */
- (void)enterXmodem;

/*Route calculation
 @parmar height User’s height（cm）
 @parmar step   steps
 */
- (float )countDisWithHeight:(NSString *)height Step:(NSString *)step;

/*Calories calculation
 @parmar sex Gender
 @parmar weight Weight
 @parmar step   steps
 */
- (float)countCalWithSex:(NSString *)sex userWeight:(NSString *)weight step:(NSString *)step;

/*Analysis of dial packets
 @parmar data Resolution of the corresponding table bin file
 @parmar number Need to replace the dial position（1，2，3）
 */
- (void)analySwitchsWithdata:(NSData *)data replace:(int)number;
@end
