//
//  SmaAlarmInfo.h
//  SmaLife
//
//  Created by 有限公司 深圳市 on 15/4/15.
//  Copyright (c) 2015年 SmaLife. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmaAlarmInfo : NSObject
@property (nonatomic,strong) NSString *dayFlags;   //cycle period @"1,1,1,1,1,1,0"; On behalf of open Monday to Saturday      Closed on Sunday
@property (nonatomic,strong) NSString *aid;       // The alarm clock number
@property (nonatomic,strong) NSString *minute;    //Minute
@property (nonatomic,strong) NSString *hour;      //hour
@property (nonatomic,strong) NSString *day;       //date
@property (nonatomic,strong) NSString *mounth;    //month
@property (nonatomic,strong) NSString *year;      //year
@property (nonatomic,strong) NSString *tagname;   //alarm name(07)

@end
