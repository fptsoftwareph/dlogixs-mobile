//
//  SmaHRHisInfo.h
//  SmaLife
//
//  Created by 有限公司 深圳市 on 16/3/31.
//  Copyright © 2016年 SmaLife. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmaHRHisInfo : NSObject
@property (nonatomic,strong) NSString *dayFlags;   //Weekdays, type such as @"0,1,1,1,1,1,1";// means turn on from Tues. to Sun., turn off  on Monday.
@property (nonatomic,strong) NSString *beginhour0; //The first heart rate start time setting, can go through isopen0 to confirm open or not
@property (nonatomic,strong) NSString *endhour0;   //The first heart rate end time setting, can go through isopen0 to confirm open or not
@property (nonatomic,strong) NSString *beginhour1; //The second heart rate start time setting, can go through isopen1 to confirm open or not
@property (nonatomic,strong) NSString *endhour1;   //The second heart rate end time setting, can go through isopen1 to confirm open or not
@property (nonatomic,strong) NSString *beginhour2; //The third heart rate start time setting, can go through isopen2 to confirm open or not
@property (nonatomic,strong) NSString *endhour2;   //The third heart rate end time setting, can go through isopen2 to confirm open or not
@property (nonatomic,strong) NSString *tagname; // Detect period
@property (nonatomic,strong) NSString *isopen;  //open or not ( Only the child switch is open will make the heart rate open, if close, all the settings of heart rate is close)
@property (nonatomic,strong) NSString *isopen0; //open or not
@property (nonatomic,strong) NSString *isopen1; //open or not
@property (nonatomic,strong) NSString *isopen2; //open or not
@end
