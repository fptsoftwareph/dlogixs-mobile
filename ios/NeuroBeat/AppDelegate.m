/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <RNGoogleSignin/RNGoogleSignin.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <TwitterKit/TWTRTwitter.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import <React/RCTLinkingManager.h>
#import "NeuroBeat-Swift.h"
#import <AlarmModule/AlarmModule.h>

@interface AppDelegate()

@property (nonatomic, strong) AlarmModule *alarmModule;

@end

@implementation AppDelegate

#pragma mark === UI Application Delegate ===
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  self.alarmModule = [[AlarmModule alloc] init];
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];
  NSURL *jsCodeLocation;  
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
//  jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"NeuroBeat"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
 
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}

//receive local notification when app in foreground
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
  NSLog(@"didReceiveLocalNotification()");
  if (self.delegate != nil) {
    [self.delegate app:application didReceiveNotification:notification];
  }
}

//snooze notification handler when app in background
-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
  completionHandler();
}

-(void)applicationDidEnterBackground:(UIApplication *)application {
  NSLog(@"applicationDidEnterBackground()");
  if (self.delegate != nil) {
    [self.delegate appDidEnterBackground:application];
  }
}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
  if ([KOSession isKakaoAccountLoginCallback:url])
    return [KOSession handleOpenURL:url];
  return [RNGoogleSignin application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationLaunchOptionsAnnotationKey]] || [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationLaunchOptionsAnnotationKey]] || [[Twitter sharedInstance] application:app openURL:url options:options] || [RCTLinkingManager application:app openURL:url options:options];
}

-(void)applicationDidBecomeActive:(UIApplication *)application {
  NSLog(@"applicationDidBecomeActive()");
  [KOSession handleDidBecomeActive];
}

-(void)applicationWillTerminate:(UIApplication *)application {
  NSLog(@"applicationWillTerminate()");
}

@end
