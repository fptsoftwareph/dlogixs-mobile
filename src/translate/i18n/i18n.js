import I18n from 'react-native-i18n';
import en from './en';
import kor from './kor';  
import chi from './chi';  
import jap from './jap';  

I18n.fallbacks = true;

I18n.translations = {
  en,
  kor,
  chi,
  jap
};

export default I18n; 