import { RESET, UP_MODULE_UPDATE } from './types';



export const updateUPModule = ({ prop, value }) => {
    return {
        type: UP_MODULE_UPDATE,
        payload: { prop, value }
    };
};

export const resetUPModule = () => {
    return {
        type: RESET
    };
};