import { ADD_FOOD_MODULE_UPDATE } from './types';
import { Platform } from 'react-native';

export const updateAddFoodModule = (value) => {
  return {
    type: ADD_FOOD_MODULE_UPDATE,
    payload: value
  };
};

