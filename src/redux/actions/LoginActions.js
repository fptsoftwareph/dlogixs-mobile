import { LOGIN_SESSION, LOGOUT } from './types';

export const createLoginSession = (userId, name, loginType) => {
  return {
    type: LOGIN_SESSION,
    userId: userId,
    name: name,
    loginType: loginType
  };
};

export const logoutUser = userId => {
  return {
    type: LOGOUT,
    userId: userId
  };
};
