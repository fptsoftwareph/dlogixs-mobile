import { MANAGE_ITEMS } from './types';

export const manageItems = ({ prop, value }) => {
  return {
    type: MANAGE_ITEMS,
    payload: { prop, value }
  };
};