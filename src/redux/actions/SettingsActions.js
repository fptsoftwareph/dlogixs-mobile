import { SETTINGS_MODULE_UPDATE, APP_LATEST_VERSION, ALARM_MODULE_UPDATE, SET_IS_CONNECTED, RESET_SETTINGS } from './types';
import axios from 'axios';
import { Platform } from 'react-native';

export const updateAlarm = ({ prop, value }) => {
  return {
    type: ALARM_MODULE_UPDATE,
    payload: { prop, value }
  };
};

export const updateSettingsModule = ({ prop, value }) => {
  return {
    type: SETTINGS_MODULE_UPDATE,
    payload: { prop, value }
  };
};

export const getSettingsModule = ({ prop, value }) => {
  return {
    type: '',
    payload: { prop, value }
  };
};

export const setIsConnectedToAccessPoint = isConnected => {
  return {
    type: SET_IS_CONNECTED,
    payload: isConnected
  };
};

export const resetSettingsModule = () => {
  return {
    type: RESET_SETTINGS
  };
};

export const getLatestAppVersion = () => {
  const requestUrl = axios.get(`http://52.79.113.245/accounts/app-version?os=${Platform.OS}`)
    .catch(error => {
      console.log(`getLatestAppVersion err: ${JSON.stringify(error)}`);
      throw error;
    });

  return (dispatch) => {
    requestUrl.then(({ data }) => {
      console.log('getLatestAppVersion res: ' + data);
      console.log('getLatestAppVersion response: ' + JSON.stringify(data));
      console.log('getLatestAppVersion response2: ' + data['version'])
      dispatch({ type: APP_LATEST_VERSION, payload: data['version'] })
    }).catch(error => {
      console.log(`getLatestAppVersion promise err: ${JSON.stringify(error)}`);
      getDefaultAppVersion(dispatch, '1.0')
      throw error;
    });
  };
};

const getDefaultAppVersion = (dispatch, value) => {
  console.log('getDefaultAppVersion value: ' + value);
  dispatch({
    type: APP_LATEST_VERSION,
    payload: value
  });
};
