import { MANAGE_BINAURAL } from './types';

export const manageBinaural = ({ prop, value }) => {
  return {
    type: MANAGE_BINAURAL,
    payload: { prop, value }
  };
};