export const SETTINGS_MODULE_UPDATE = 'SETTINGS_MODULE_UPDATE';
export const APP_LATEST_VERSION = 'APP_LATEST_VERSION';
export const ADD_FOOD_MODULE_UPDATE = 'ADD_FOOD_MODULE_UPDATE';
export const RESET = 'RESET';
export const RESET_SETTINGS = 'RESET_SETTINGS';
export const UP_MODULE_UPDATE = 'UP_MODULE_UPDATE';
export const QRATION_GET_DATA = 'QRATION_GET_DATA';
export const QRATION_GET_DATA_SUCCESS = 'QRATION_GET_DATA_SUCCESS';
export const QRATION_GET_DATA_FAILURE = 'QRATION_GET_DATA_FAILURE';
export const SET_IS_CONNECTED = 'SET_IS_CONNECTED';
export const LOGIN_SESSION = 'LOGIN_SESSION';
export const LOGOUT = 'LOGOUT';
export const MANAGE_ITEMS = 'MANAGE_ITEMS';
export const MANAGE_BINAURAL = 'MANAGE_BINAURAL';

