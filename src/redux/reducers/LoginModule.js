import { LOGIN_SESSION, LOGOUT } from '../actions/types';

const INITIAL_STATE = {
  userId: '',
  name: '',
  isSignedIn: false,
  loginType: ''
};

export default (state = INITIAL_STATE, action) => {
  //  console.log(`LoginModule: ${action.type}`);
  //  console.log(`LoginModule userId: ${action.userId}`);
  switch (action.type) {
    case LOGIN_SESSION:
      return { ...state, userId: action.userId, name: action.name,
        isSignedIn: true, loginType: action.loginType };
    case LOGOUT:
      return INITIAL_STATE;
    default:
      return state;
  }
};
