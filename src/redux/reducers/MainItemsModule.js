import { MANAGE_ITEMS } from '../actions/types';

const INITIAL_STATE = {
  showBrainWave: true,
  showBinaural: true,
  showAlarm: true,
  showFood: true,
  showWater: true,
  showCaffeine: true,
  showAlcohol: false,
  showCigarette: false
};

export default (state = INITIAL_STATE, action) => {  
  switch (action.type) {
    case MANAGE_ITEMS:
      return { ...state, [action.payload.prop]: action.payload.value };
    default:
      return state;
  }
};
