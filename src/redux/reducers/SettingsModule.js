import { SETTINGS_MODULE_UPDATE, APP_LATEST_VERSION, SET_IS_CONNECTED, RESET_SETTINGS } from '../actions/types';
import I18n from './../../translate/i18n/i18n';

const INITIAL_STATE = {
  isBstationHeadSetEnabled: false,
  isSmartBandEnabled: false,
  isRbsEnabled: false,
  isWstationHeadSetEnabled: false,
  dateSmartBandConnected: '',
  dateSmartBandDisconnected: '',
  analysisTimeFrom: '6:00 ' + I18n.t('PM'),
  analysisTimeTo: '6:00 ' + I18n.t('PM'),
  appLatestVersion: '1.0',
  deviceVersion: '1.0',
  dateAndDocumentMap: {},
  connectedBStation: 'Device Name',
  connectedWristband: 'Device Name',
  connectedPeripheralId: '',
  rememberedPeripheralId: '',
  rememberedPeripheralName: '',
  isAudioSelectorEnabled: false,
  connectedPeripheral: '',
  connectedAccessPoint: 'Network Name',
  rememberedAccessPoint: '',
  rememberedAccessPointPword: '',
  isConnectedToAccessPoint: false,
  minsInADay: [0],
  deductedMins: 0,
  targetSteps: 0,
  targetSleepHours: 0,
  targetBinauralHours: 0,
  targetCals: 0,
  targetGlassWater: 0,
  recordGlassWater: 0,
  targetCups: 0,
  recordCups: 0,
  targetGlassAlcohol: 0,
  recordGlassAlcohol: 0,
  targetSticks: 0,
  recordSticks: 0,
  latestSteps: '0',
  latestCal: '0',
  latestActivityTime: '0',
  latestBPM: '0',
  volume: 0,
  volumeLastValue: 0,
  stationLight: 0,
  stationLightLastValue: 0,
  audioSelected: 0,
  binauralBeat: 0,
  connectedDevice: 'Device Name',
  alarmTone: '',
  alarmUri: '',
  batStatus: 'bat_inactive_empty',
  batStatusInt: 0,
  sMVolState: false,
  sMLightState: false,
  sMBbState: false,
  headsetStatus: 'headset',
  uploadMode: 0,
  wasUploaded: false,
  connectionStatus: 0
};


export default (state = INITIAL_STATE, action) => {
  //  console.log("SettingsModule: " + action.type);
  //  console.log("SettingsModule val: " + action.payload);
  switch (action.type) {
    case SETTINGS_MODULE_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case APP_LATEST_VERSION:
      return { ...state, appLatestVersion: action.payload };
    case SET_IS_CONNECTED:
      return { ...state, isConnectedToAccessPoint: action.payload };
    case RESET_SETTINGS:
      return INITIAL_STATE;
    default:
      return state;
  }
};
