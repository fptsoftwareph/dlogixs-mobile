import { MANAGE_BINAURAL } from '../actions/types';

const INITIAL_STATE = {
  binauralFrequency: 0,
  binauralLastFrequency: 0,
  binauralStart: 0,
  binauralEnd: 0,
  binauralTotalHoursToday: 0,
  binauralTotal: 0,
  BBisPlaying: false,
  binauralDocId: "",
  binauralDocRev: "",
  isRepeatEnabled: false,
  isRepeatOneSong: false,
  isAllPlaySongOnce: false,
  isRepeatAllSongs: false,
  BB3Hz: 0, 
  BB6Hz: 0,
  BB9Hz: 0, 
  BB12Hz: 0
};

export default (state = INITIAL_STATE, action) => {  
  switch (action.type) {
    case MANAGE_BINAURAL:
      return { ...state, [action.payload.prop]: action.payload.value };
    default:
      return state;
  }
};
