import { combineReducers } from 'redux';
import SettingsModule from './SettingsModule';
import UserProfileModule from './UserProfileModule';
import AddFoodModule from './AddFoodModule';
import LoginModule from './LoginModule';
import MainItemsModule from './MainItemsModule';
import BinauralModule from './BinauralModule';

export default combineReducers({
  settingsModule: SettingsModule,
  addFoodModule: AddFoodModule,
  userProfileModule: UserProfileModule,
  loginModule: LoginModule,
  mainItems: MainItemsModule,
  binaural: BinauralModule
});


