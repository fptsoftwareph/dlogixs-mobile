import { RESET, UP_MODULE_UPDATE } from '../actions/types';

const UP_STATE = {
    onFirstNameChange: '',
    onLastNameChange: '',
    onEmailChange: '',
    onCountryChange: 'Country',
    onBirthdateChange: '',
    onGenderChange: 'Gender',
    onOccupationChange: '',
    onWeightChange: '',
    onHeightChange: '',
}


export default (state = UP_STATE, action, payload) => {
    //  console.log("userProfileModule: " + action.type);
    switch (action.type) {
        case UP_MODULE_UPDATE:
            return { ...state, [action.payload.prop]: action.payload.value };
        case RESET:
            return UP_STATE;
        default:
            return state;
    }
}; 