import { ADD_FOOD_MODULE_UPDATE, FOOD_NAME, FOOD_CAL } from '../actions/types';

const INITIAL_STATE = {
  onFoodNameChange: 'pork',
  onFoodCalsChange: '200',
  // arr: [{ food_name: 'Pork', food_cal: '100' }]
  arr: new Map,
};

export default (state = INITIAL_STATE, action) => {
  //  console.log(action.payload);
  switch (action.type) {
    case ADD_FOOD_MODULE_UPDATE:
      return { ...state, arr: action.payload };
    default:
      return state;
  }
}; 