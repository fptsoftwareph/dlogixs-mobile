import React, { Component } from 'react';
import {
  View, Text, TextInput, TouchableOpacity, ScrollView, Alert, Platform, NativeEventEmitter,
  NativeModules
} from 'react-native';
import I18n from './../translate/i18n/i18n';
import { widgets } from '../styles/widgets';
import MenuIconBack from './common/MenuIconBack';
import { text } from '../styles/text';
import Header from './Header';
import { Icon } from 'react-native-elements';
import { DEFAULT_RESET_HEADER_COLOR } from '../styles/colors';
import { APIClient } from '../utils/APIClient';
import { Actions } from 'react-native-router-flux';
import { getPreference } from '../utils/CommonMethods';

let apiClient;

class ResetPassword extends Component {
  constructor() {
    super();

    apiClient = new APIClient();

    this.state = {
      isPasswordReset: '',
      email: '',
      language: ''
    };
    getPreference('language').then(lang => {
      this.setState({ language: lang });
    });
  }

  onClickConfirm() {
    apiClient.resetPassword(this.state.email, I18n.t('EMAIL_GREETING'), I18n.t('EMAIL_BODY'), I18n.t('EMAIL_END'), this.state.language, isSuccessful => {
      if (isSuccessful) {
        this.setState({ isPasswordReset: true });
      } else {
        this.setState({ isPasswordReset: false });
        Alert.alert('', I18n.t('FAILED_TO_SEND_RESET_LINK'));
      }
    });
  }

  onClickSignin() {
    Actions.login();
  }

  render() {
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={widgets.commonCon}>
          <View>
            <Header />
            <MenuIconBack />
          </View>
          <Text style={[
            widgets.signupHeader, {
              marginTop: 15,
              color: DEFAULT_RESET_HEADER_COLOR
            }
          ]}>{I18n.t('RESET_PASSWORD')}</Text>
          {!this.state.isPasswordReset ?
            <View style={widgets.commonCon2}>
              <View style={widgets.idPassCon}>
                <Icon
                  color='#979797'
                  size={25}
                  type="material-community" name="email-outline" />
                <TextInput
                  placeholder={I18n.t('EMAIL')}
                  keyboardType="email-address"
                  style={[widgets.loginTextInput, widgets.commonFont]}
                  onChangeText={(email) => this.setState({ email: email })} />
              </View>
              <View style={widgets.confirmButton}>
                <TouchableOpacity
                  onPress={() => this.onClickConfirm()}
                  style={{ flex: 1, height: 64 }}>
                  <Text
                    style={[widgets.loginBtn, widgets.commonMarginTop2, { flex: 1 }]}>{I18n.t('CONFIRM')}</Text>
                </TouchableOpacity>
              </View>
            </View>
            :
            <View style={[widgets.commonCon2, { marginTop: 20 }]}>
              <Icon color='#979797' size={25} type="material-community" name="email-outline" />
              <Text style={[text.yourEmailId, widgets.commonFont]}>{I18n.t('CHECK_EMAIL')}</Text>
              <View style={widgets.confirmButton}>
                <Text onPress={() => this.onClickSignin()}
                  style={[widgets.commonFont, widgets.commonMarginTop2, { textAlign: 'center', flex: 1, textDecorationLine: 'underline' }]}>
                  {I18n.t('BACK_TO_SIGN_IN')}
                </Text>
              </View>
            </View>
          }
        </View>
      </ScrollView>
    );
  }
}

export default ResetPassword;
