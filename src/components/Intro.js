import React, { Component } from 'react';
import {
  View, Text, Image, ImageBackground, Platform, TouchableHighlight, NativeEventEmitter,
  NativeModules, DeviceEventEmitter
} from 'react-native';
import Swiper from 'react-native-swiper';
import { widgets } from '../styles/widgets';
import { containers } from '../styles/containers';
import { Actions } from 'react-native-router-flux';
import I18n from './../translate/i18n/i18n';
import { savePreference } from '../utils/CommonMethods';

class Intro extends Component {

  constructor() {
    super();
  }

  _handlePress() {
    // alert("Go to Main Page!");
    Actions.homePage();
    savePreference('isLoginViaRegistration', JSON.stringify(false));
  }

  renderSlide1() {
    return (
      <View style={{ flex: 1 }}>
        <View style={containers.introLogCon}>
          <Image style={widgets.logo}
            source={require('../images/logo.png')} />
        </View>
        <View style={widgets.slide}>
          <Text style={widgets.congratsText}>{I18n.t('CONGRATULATIONS')}</Text>
          <Text style={widgets.activatedText}>{I18n.t('ACCOUNT_ACTIVATED')}</Text>
        </View>
        <View style={widgets.footer}>
          <TouchableHighlight
            style={widgets.buttonSkip}
            onPress={() => this._handlePress()}>
            <Text style={widgets.skipText}>{I18n.t('SKIP')}</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  renderSlide2() {
    return (
      <View style={{ flex: 1 }}>
        <View style={containers.introLogCon}>
          <Image style={widgets.logo}
            source={require('../images/logo.png')} />
        </View>
        <View style={widgets.slide}>
          <Text style={widgets.activatedText}>{I18n.t('INTRO_MSG_1')}</Text>
        </View>
        <View style={widgets.footer}>
          <TouchableHighlight
            style={widgets.buttonSkip}
            onPress={() => this._handlePress()}>
            <Text style={widgets.skipText}>{I18n.t('SKIP')}</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  renderSlide3() {
    return (
      <View style={{ flex: 1 }}>
        <View style={containers.introLogCon}>
          <Image style={widgets.logo}
            source={require('../images/logo.png')} />
        </View>
        <View style={widgets.slide}>
          <Text style={widgets.activatedText}>{I18n.t('INTRO_MSG_2')}</Text>
        </View>
        <View style={widgets.footer}>
          <TouchableHighlight
            style={widgets.buttonSkip}
            onPress={() => this._handlePress()}>
            <Text style={widgets.skipText}>{I18n.t('NEXT')}</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  renderSwiperAndroid() {
    return (
      <Swiper style={widgets.wrapper}
        horizontal={true}
        dot={<View style={widgets.dotStyle} />}
        loop={false}
        activeDot={<View style={widgets.activeDotStyle} />}>
        <View style={widgets.slide}>
          {this.renderSlide1()}
        </View>

        <View style={widgets.slide}>
          {this.renderSlide2()}
        </View>

        <View style={widgets.slide}>
          {this.renderSlide3()}
        </View>

      </Swiper>
    );
  }

  renderSwiperIOS() {
    return (
      <Swiper containerStyle={widgets.wrapper}
        horizontal={true}
        dot={<View style={widgets.dotStyle} />}
        loop={false}
        activeDot={<View style={widgets.activeDotStyle} />}>
        <View style={widgets.slide}>
          {this.renderSlide1()}
        </View>

        <View style={widgets.slide}>
          {this.renderSlide2()}
        </View>

        <View style={widgets.slide}>
          {this.renderSlide3()}
        </View>
      </Swiper>
    );
  }

  render() {
    return (
      <ImageBackground source={require('../images/header_bg.png')} style={[widgets.commonCon3]}>
        {
          Platform.OS === 'ios' ? this.renderSwiperIOS() : this.renderSwiperAndroid()
        }
      </ImageBackground>
    );
  }
}

export default Intro;
