import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView,
    Platform,
    NativeEventEmitter,
    NativeModules,
    Switch
} from 'react-native';
import * as DateUtil from '../../utils/DateUtils';
import DatePicker from 'react-native-datepicker';
import { Card } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { widgets } from './../../styles/widgets'
import CheckBox from 'react-native-check-box';
import { text } from './../../styles/text';
import I18n from './../../translate/i18n/i18n';
import { CARD_COLOR, DEFAULT_TEXT_COLOR } from '../../styles/colors';
import AlarmToneModal from './modals/AlarmToneModal.js'
import SnoozeModal from './modals/SnoozeModal.js'
import ToneModal from './modals/ToneModal.js'
import DownloadTonesModal from './modals/DownloadTonesModal.js'
import MusicToneModal from './modals/MusicToneModal.js'
import { getPreference } from '../../utils/CommonMethods';
import { AlarmModule, StationModule } from 'nb_native_modules';
import { connect } from 'react-redux';
import { updateSettingsModule } from '../../redux/actions';

const StationManagerModule = NativeModules.StationModule;
const StationManagerEmitter = new NativeEventEmitter(StationManagerModule);

let alarmTime, alarmMins, alarmHours;
var alarmNotifData = {}, doc = {};
let LOCAL_ALARM;
let ALARMS = [], weekAlarm = [], repeatDays = [];
let alarmMeridiem = '';
const inactiveColor = 'gray';
const activeColor = DEFAULT_TEXT_COLOR;
let month = new Date().getMonth() + 1 < 10 ? "0" + (new Date().getMonth() + 1) : new Date().getMonth() + 1;
let day = new Date().getDate() < 10 ? "0" + (new Date().getDate()) : new Date().getDate();
let year = new Date().getFullYear();
let currentDate = year + '-' + month + '-' + day;

class AlarmDetail extends Component {
    constructor() {
        super();

        this.state = {
            rbsSoundToogle: false,
            alarmTime: '',
            actualAlarmTime: '',
            alarmMeridiem: '',
            valueOf: '',
            alarmId: '',
            serverId: '',
            alarmTone: '',
            displayRepeat: '',
            disabled: false,
            isAlarmEnabled: false,
            checkRepeat: false,
            checkMon: false,
            checkTue: false,
            checkWed: false,
            checkThu: false,
            checkFri: false,
            checkSat: false,
            checkSun: false,
            alarmToneModal: false,
            snoozeModal: false,
            toneModal: false,
            downloadToneModal: false,
            musicToneModal: false,
            toneSelect: '',
            snoozeSelect: 1,
            snoozeDefault: 1,
            rbsSoundSelect: 0,
            _rev: '',
            _id: '',
            rbsEnabled: false
        }
    }

    componentWillUnmount() {
        this.props.updateSettingsModule({ prop: 'alarmTone', value: '' });
        this.props.updateSettingsModule({ prop: 'alarmUri', value: '' });
        this.setState({ displayRepeat: '' });
        ALARMS = [];
        weekAlarm = [];
        repeatDays = [];
    }


    handleReceiveAlarmNotification(result) {
        alert('ALARM RECEIVE: ' + result);
    }

    componentWillMount() {
        ALARMS = [];
        weekAlarm = [];
        repeatDays = [];
        this.setState
            ({
                displayRepeat: '',
                snoozeSelect: this.props.alarmObject['snooze'],
                checkMon: this.props.alarmObject['repeat_days'][1],
                checkTue: this.props.alarmObject['repeat_days'][2],
                checkWed: this.props.alarmObject['repeat_days'][3],
                checkThu: this.props.alarmObject['repeat_days'][4],
                checkFri: this.props.alarmObject['repeat_days'][5],
                checkSat: this.props.alarmObject['repeat_days'][6],
                checkSun: this.props.alarmObject['repeat_days'][0],
            })
        if (this.props.alarmObject['alarmId'] !== null)
            setTimeout(() => {
                if (this.state.checkMon === true) {
                    repeatDays.push(I18n.t('MON'));
                    weekAlarm.push(1);
                }

                if (this.state.checkTue === true) {
                    repeatDays.push(I18n.t('TUE'));
                    weekAlarm.push(2);
                }

                if (this.state.checkWed === true) {
                    repeatDays.push(I18n.t('WED'));
                    weekAlarm.push(3);
                }

                if (this.state.checkThu === true) {
                    repeatDays.push(I18n.t('THU'));
                    weekAlarm.push(4);
                }

                if (this.state.checkFri === true) {
                    repeatDays.push(I18n.t('FRI'));
                    weekAlarm.push(5);
                }

                if (this.state.checkSat === true) {
                    repeatDays.push(I18n.t('SAT'));
                    weekAlarm.push(6);
                }

                if (this.state.checkSun === true) {
                    repeatDays.push(I18n.t('SUN'));
                    weekAlarm.push(0);
                }

                if (repeatDays.length === 7) {
                    this.setState({ displayRepeat: I18n.t('EVERYDAY') })
                }
                else {
                    this.setState({ displayRepeat: repeatDays })
                }

                console.log('WAHEHE:', repeatDays);
            }, 100)

    }

    componentDidMount() {
        ALARMS = [];
        this.setupViewAndQuery().then(res => {
            for (let i = 0; i < res.obj.rows[0].doc.alarms.length; i++) {
                ALARMS[i] = res.obj.rows[0].doc.alarms[i];
            }
            this.setState({ _rev: res.obj.rows[0].doc._rev, _id: res.obj.rows[0].doc._id })
            console.log('KINI_ALARM: ', res.obj.rows[0].doc);
            console.log('_rev: ', this.state._rev);
            console.log('_id: ', this.state._id);
        });

        getPreference('userId').then(res => {
            console.log('123redux: getPreference userId: ' + res);
            user_id = res;
        })

        this.setState({
            actualAlarmTime: this.props.alarmObject['actualAlarmTime'],
            alarmTime: this.props.alarmObject['alarmTime'],
            alarmId: this.props.alarmObject['alarmId'],
            serverId: this.props.alarmObject['serverId'],
            alarmMeridiem: this.props.alarmObject['alarmMeridiem'],
            toneSelect: this.props.alarmObject['song_name'],
            rbsEnabled: this.props.alarmObject['rbs_enabled']
        });

        this.props.updateSettingsModule({ prop: 'alarmTone', value: this.props.alarmObject['song_name'] });
        this.props.updateSettingsModule({ prop: 'alarmUri', value: this.props.alarmObject['song_uri'] });

        setTimeout(() => {
            alarmNotifData = {
                id: this.state.alarmId,
                vibrate: true,
                vibration: 100,
                play_sound: true,
                sound_name: this.props.alarmUri,
                song_name: this.props.alarmTone,
                snooze: parseInt(this.state.snoozeSelect),
                repeat_alarm: weekAlarm,
                alarm_date: this.state.actualAlarmTime,
                repeat_days: repeatDays,
                rbs_enabled: this.state.rbsEnabled,
                ...Platform.select({
                    ios: {
                        alarm_date: currentDate + "T" + this.state.actualAlarmTime + ':' + '00.000Z',
                        alert_body: I18n.t('WAKE_UP'),
                        alert_action: I18n.t('OPEN_APP'),
                    }
                }),
            }
            console.log('alarmNotifData:', alarmNotifData);

        }, 100);
    }

    setupViewAndQuery() {
        return manager.query.get_db_design_ddoc_view_view({
            db: DB_NAME,
            ddoc: 'main',
            view: 'alarmByDocId',
            include_docs: true
        });
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                < ScrollView>
                    <Card containerStyle={{ margin: 0 }}>
                        <View style={{ alignItems: 'center', alignContent: 'center' }}>
                            <TouchableOpacity onPress={() => this.refs.datepickerDialog.onPressDate()}>
                                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                    <Text style={text.textAlarmDetailTitle}>{this.state.alarmTime}</Text>
                                    <Text style={[text.subTitle, widgets.commonMarginLeft, text.textAlarmSubtitle, { marginTop: 25 }]}>{this.state.alarmMeridiem.toUpperCase()}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={{
                            marginTop: 30,
                            flexDirection: 'row',
                            justifyContent: 'space-around'
                        }}>

                        </View>

                        <View style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            justifyContent: 'space-around'
                        }}>

                            <View>
                                <CheckBox
                                    checkBoxColor={DEFAULT_TEXT_COLOR}
                                    style={{ flex: 1, padding: 10 }}
                                    onClick={() => {
                                        if (this.state.checkMon === false) {
                                            this.setState({ checkMon: true })
                                            console.log('monday checked'); weekAlarm.push(1); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays.push(I18n.t('MON'));
                                        }
                                        else {
                                            this.setState({ checkMon: false })
                                            console.log('monday unchecked'); weekAlarm = weekAlarm.filter(item => item !== 1); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays = repeatDays.filter(item => item !== I18n.t('MON'));
                                        }
                                        alarmNotifData['repeat_days'] = repeatDays;
                                        alarmNotifData['repeat_alarm'] = weekAlarm;
                                        this.setState({ displayRepeat: repeatDays.length === 7 ? I18n.t('EVERYDAY') : repeatDays });
                                    }}
                                    isChecked={this.state.checkMon}
                                />
                                <Text style={[widgets.textLanguagePosition, { color: DEFAULT_TEXT_COLOR, fontSize: 10, fontWeight: 'bold' }]}>{I18n.t('MON')}</Text>
                            </View>

                            <View>
                                <CheckBox
                                    checkBoxColor={DEFAULT_TEXT_COLOR}
                                    style={{ flex: 1, padding: 10 }}
                                    onClick={() => {
                                        if (this.state.checkTue === false) {
                                            this.setState({ checkTue: true })
                                            console.log('tuesday checked'); weekAlarm.push(2); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays.push(I18n.t('TUE'));
                                        }
                                        else {
                                            this.setState({ checkTue: false })
                                            console.log('tuesday unchecked'); weekAlarm = weekAlarm.filter(item => item !== 2); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays = repeatDays.filter(item => item !== I18n.t('TUE'));
                                        }
                                        alarmNotifData['repeat_days'] = repeatDays;
                                        alarmNotifData['repeat_alarm'] = weekAlarm;
                                        this.setState({ displayRepeat: repeatDays.length === 7 ? I18n.t('EVERYDAY') : repeatDays });
                                    }}
                                    isChecked={this.state.checkTue}
                                />
                                <Text style={[widgets.textLanguagePosition, { color: DEFAULT_TEXT_COLOR, fontSize: 10, fontWeight: 'bold' }]}>{I18n.t('TUE')}</Text>
                            </View>

                            <View>
                                <CheckBox
                                    checkBoxColor={DEFAULT_TEXT_COLOR}
                                    style={{ flex: 1, padding: 10 }}
                                    onClick={() => {
                                        if (this.state.checkWed === false) {
                                            this.setState({ checkWed: true })
                                            console.log('wednesday checked'); weekAlarm.push(3); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays.push(I18n.t('WED'));
                                        }
                                        else {
                                            this.setState({ checkWed: false })
                                            console.log('wednesday unchecked'); weekAlarm = weekAlarm.filter(item => item !== 3); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays = repeatDays.filter(item => item !== I18n.t('WED'));
                                        }
                                        alarmNotifData['repeat_days'] = repeatDays;
                                        alarmNotifData['repeat_alarm'] = weekAlarm;
                                        this.setState({ displayRepeat: repeatDays.length === 7 ? I18n.t('EVERYDAY') : repeatDays });
                                    }}
                                    isChecked={this.state.checkWed}
                                />
                                <Text style={[widgets.textLanguagePosition, { color: DEFAULT_TEXT_COLOR, fontSize: 10, fontWeight: 'bold' }]}>{I18n.t('WED')}</Text>
                            </View>

                            <View>
                                <CheckBox
                                    checkBoxColor={DEFAULT_TEXT_COLOR}
                                    style={{ flex: 1, padding: 10 }}
                                    onClick={() => {
                                        if (this.state.checkThu === false) {
                                            this.setState({ checkThu: true })
                                            console.log('thursday checked'); weekAlarm.push(4); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays.push(I18n.t('THU'));
                                        }
                                        else {
                                            this.setState({ checkThu: false })
                                            console.log('thursday unchecked'); weekAlarm = weekAlarm.filter(item => item !== 4); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays = repeatDays.filter(item => item !== I18n.t('THU'));
                                        }
                                        alarmNotifData['repeat_days'] = repeatDays;
                                        alarmNotifData['repeat_alarm'] = weekAlarm;
                                        this.setState({ displayRepeat: repeatDays.length === 7 ? I18n.t('EVERYDAY') : repeatDays });
                                    }}
                                    isChecked={this.state.checkThu}
                                />
                                <Text style={[widgets.textLanguagePosition, { color: DEFAULT_TEXT_COLOR, fontSize: 10, fontWeight: 'bold' }]}>{I18n.t('THU')}</Text>
                            </View>

                            <View>
                                <CheckBox
                                    checkBoxColor={DEFAULT_TEXT_COLOR}
                                    style={{ flex: 1, padding: 10 }}
                                    onClick={() => {
                                        if (this.state.checkFri === false) {
                                            this.setState({ checkFri: true })
                                            console.log('friday checked'); weekAlarm.push(5); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays.push(I18n.t('FRI'));
                                        }
                                        else {
                                            this.setState({ checkFri: false })
                                            console.log('friday unchecked'); weekAlarm = weekAlarm.filter(item => item !== 5); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays = repeatDays.filter(item => item !== I18n.t('FRI'));
                                        }
                                        alarmNotifData['repeat_days'] = repeatDays;
                                        alarmNotifData['repeat_alarm'] = weekAlarm;
                                        this.setState({ displayRepeat: repeatDays.length === 7 ? I18n.t('EVERYDAY') : repeatDays });
                                    }}
                                    isChecked={this.state.checkFri}
                                />
                                <Text style={[widgets.textLanguagePosition, { color: DEFAULT_TEXT_COLOR, fontSize: 10, fontWeight: 'bold' }]}>{I18n.t('FRI')}</Text>
                            </View>

                            <View>
                                <CheckBox
                                    checkBoxColor={DEFAULT_TEXT_COLOR}
                                    style={{ flex: 1, padding: 10 }}
                                    onClick={() => {
                                        if (this.state.checkSat === false) {
                                            this.setState({ checkSat: true })
                                            console.log('saturday checked'); weekAlarm.push(6); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays.push(I18n.t('SAT'));
                                        }
                                        else {
                                            this.setState({ checkSat: false })
                                            console.log('saturday unchecked'); weekAlarm = weekAlarm.filter(item => item !== 6); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays = repeatDays.filter(item => item !== I18n.t('SAT'));
                                        }
                                        alarmNotifData['repeat_days'] = repeatDays;
                                        alarmNotifData['repeat_alarm'] = weekAlarm;
                                        this.setState({ displayRepeat: repeatDays.length === 7 ? I18n.t('EVERYDAY') : repeatDays });
                                    }}
                                    isChecked={this.state.checkSat}
                                />
                                <Text style={[widgets.textLanguagePosition, { color: DEFAULT_TEXT_COLOR, fontSize: 10, fontWeight: 'bold' }]}>{I18n.t('SAT')}</Text>
                            </View>

                            <View>
                                <CheckBox
                                    checkBoxColor={DEFAULT_TEXT_COLOR}
                                    style={{ flex: 1, padding: 10 }}
                                    onClick={() => {
                                        if (this.state.checkSun === false) {
                                            this.setState({ checkSun: true })
                                            console.log('sunday checked'); weekAlarm.push(0); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays.push(I18n.t('SUN'));
                                        }
                                        else {
                                            this.setState({ checkSun: false })
                                            console.log('sunday unchecked'); weekAlarm = weekAlarm.filter(item => item !== 0); console.log('WEEK_ALARM: ', weekAlarm);
                                            repeatDays = repeatDays.filter(item => item !== I18n.t('SUN'));
                                        }
                                        alarmNotifData['repeat_days'] = repeatDays;
                                        alarmNotifData['repeat_alarm'] = weekAlarm;
                                        this.setState({ displayRepeat: repeatDays.length === 7 ? I18n.t('EVERYDAY') : repeatDays });
                                    }}
                                    isChecked={this.state.checkSun}
                                />
                                <Text style={[widgets.textLanguagePosition, { color: DEFAULT_TEXT_COLOR, fontSize: 10, fontWeight: 'bold' }]}>{I18n.t('SUN')}</Text>
                            </View>

                        </View>
                    </Card>

                    <Card containerStyle={{ marginTop: 20, backgroundColor: CARD_COLOR }}>
                        <View style={widgets.commonConRow}>
                            <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                    <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{I18n.t('REPEAT')}</Text>
                                </View>
                                <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{this.state.displayRepeat}</Text>
                            </View>
                        </View>
                    </Card>

                    <Card containerStyle={{ marginTop: 0, backgroundColor: CARD_COLOR }}>
                        <View style={widgets.commonConRow}>
                            <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                    <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{I18n.t('ALARM_TONE')}</Text>
                                </View>
                                <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{this.state.toneSelect}</Text>
                            </View>
                            <View style={widgets.alignRightView}>
                                <TouchableOpacity onPress={() => { { this.setState({ alarmToneModal: true }); } }}>
                                    <Image source={require('./../../images/alarm/more_circle.png')}
                                        style={{ width: 30, height: 30, resizeMode: 'contain', marginRight: 10 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>

                    <Card containerStyle={{ marginTop: 0, backgroundColor: CARD_COLOR }}>
                        <View style={widgets.commonConRow}>
                            <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                    <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{I18n.t('SNOOZE')}</Text>
                                </View>
                                <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{this.state.snoozeSelect} {I18n.t('MINS')}</Text>
                            </View>
                            <View style={widgets.alignRightView}>
                                <TouchableOpacity onPress={() => { { this.setState({ snoozeModal: true }); } }}>
                                    <Image source={require('./../../images/alarm/more_circle.png')}
                                        style={{ width: 30, height: 30, resizeMode: 'contain', marginRight: 10 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>
                    <Card containerStyle={{ marginTop: 0, backgroundColor: CARD_COLOR }}>
                        <View style={widgets.commonConRow}>
                            <View style={[widgets.commonFlex, widgets.commonConRow]}>
                              <View style={widgets.settingsModuleTitleCon}>
                                <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{I18n.t('BINAURAL_BEAT')}</Text>
                              </View>
                            </View>
                            <View style={widgets.alignRightView}>
                              <Switch
                                style={widgets.switch}
                                onValueChange={value => {
                                    console.log('AlarmDetail rbsEnabled: ' + value);
                                    this.setState({ rbsEnabled: value });
                                    alarmNotifData['rbs_enabled'] = value;
                                }}
                                value={this.state.rbsEnabled}
                              />
                            </View>
                        </View>
                    </Card>

                    <DatePicker
                        ref="datepickerDialog"
                        hideText={true}
                        key={'datepickerDialog'}
                        is24Hour={false}
                        style={{
                            width: 0,
                            height: 0,
                            borderWidth: 0,
                            borderStyle: null
                        }}
                        showIcon={false}
                        mode="time"
                        format="HH:mm"
                        minuteInterval={1}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        onDateChange={value => {

                            if (this.state.alarmId === null) {
                                alarmNotifData = {
                                    id: null,
                                    vibrate: true,
                                    vibration: 100,
                                    play_sound: true,
                                    sound_name: this.props.alarmUri,
                                    song_name: this.props.alarmTone,
                                    snooze: parseInt(this.state.snoozeSelect),
                                    repeat_alarm: weekAlarm,
                                    alarm_date: value,
                                    repeat_days: repeatDays,
                                    rbs_enabled: this.state.rbsEnabled,
                                    ...Platform.select({
                                        ios: {
                                            alarm_date: currentDate + "T" + value + ':' + '00.000Z',
                                            alert_body: I18n.t('WAKE_UP'),
                                            alert_action: I18n.t('OPEN_APP'),
                                        }
                                    }),

                                }
                            }
                            else {
                                alarmNotifData = {
                                    id: this.state.alarmId,
                                    vibrate: true,
                                    vibration: 100,
                                    play_sound: true,
                                    sound_name: this.props.alarmUri,
                                    song_name: this.props.alarmTone,
                                    snooze: parseInt(this.state.snoozeSelect),
                                    repeat_alarm: weekAlarm,
                                    alarm_date: value,
                                    repeat_days: repeatDays,
                                    rbs_enabled: this.state.rbsEnabled,
                                    ...Platform.select({
                                        ios: {
                                            alarm_date: currentDate + "T" + value + ':' + '00.000Z',
                                            alert_body: I18n.t('WAKE_UP'),
                                            alert_action: I18n.t('OPEN_APP'),
                                        }
                                    }),
                                }
                            }

                            console.log('notif_data:', alarmNotifData);
                            alarmHours = value.split(':')[0];
                            if (alarmHours >= 12) {
                                alarmMeridiem = 'pm'
                                alarmHours = alarmHours - 12;
                                if (alarmHours < 10) {
                                    alarmHours = '0' + alarmHours;
                                }
                                if (alarmHours === '00') {
                                    alarmHours = 12;
                                }
                            }
                            else {
                                if (alarmHours === '00') {
                                    alarmHours = 12;
                                }
                                alarmMeridiem = 'am'
                            }

                            alarmMins = value.split(':')[1];
                            alarmTime = alarmHours + ':' + alarmMins;

                            this.setState({ alarmTime: alarmTime, alarmMeridiem: alarmMeridiem, valueOf: value })
                            console.log('DOCUMENT:', JSON.stringify(doc));
                            console.log('DATA: ', ALARMS);
                        }}
                        style={{ height: 0, width: 0 }}
                        customStyles={{
                            dateInput: {
                                width: 0,
                                height: 0,
                                borderWidth: 0,
                                borderStyle: null
                            }
                        }}
                    />

                    <View style={{
                        marginTop: 30,
                        marginBottom: 20,
                        flexDirection: 'row',
                        justifyContent: 'space-around'
                    }}>
                        <TouchableOpacity onPress={() => {
                            Actions.pop();
                        }}>
                            <View>
                                <Image source={require('./../../images/alarm/close.png')}
                                    style={{ width: 50, height: 50, resizeMode: 'contain', marginRight: 50 }} />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity disabled={this.state.disabled} onPress={() => {
                            this.setState({ disabled: true });
                            console.log('AlarmDetail onSave: ' + JSON.stringify(alarmNotifData));
                            setTimeout(() => {
                                if (this.state.alarmId === null || this.state.serverId === null) {
                                    AlarmModule.schedule(alarmNotifData);
                                    console.log('save!' + ' repeat:' + weekAlarm);
                                    console.log('notif_data:', alarmNotifData);

                                    ALARMS.push(
                                        {
                                            "alarmNotifData": alarmNotifData,
                                            "timestamp": DateUtil.getFormattedTimestamp()
                                        }
                                    )
                                }
                                else {
                                    AlarmModule.scheduleUpdate(alarmNotifData);
                                    console.log('update!' + ' repeat:' + weekAlarm);
                                    console.log('notif_data:', alarmNotifData);

                                    ALARMS[this.state.serverId] = {
                                        "alarmNotifData": alarmNotifData,
                                        "timestamp": DateUtil.getFormattedTimestamp()
                                    }
                                }

                                doc = {
                                    user_id: user_id,
                                    date: DateUtil.getFormattedTimestamp(),
                                    type: 'alarm',
                                    channels: [user_id],
                                    alarms: ALARMS
                                };
                                console.log('LENGTH: ', ALARMS.length);
                                if (ALARMS.length - 1 > 0) {
                                    manager.document.put({ db: DB_NAME, doc: this.state._id, body: doc, rev: this.state._rev }).then(res => {
                                        console.log('notification_data: ', alarmNotifData);
                                    }).catch(err => {
                                        console.log('TAE', err);
                                    });
                                }
                                else {
                                    manager.document.post({ db: DB_NAME, body: doc }).then(res => {
                                        console.log('notification_data: ', alarmNotifData);
                                    }).catch(err => {
                                        console.log('TAE', err);
                                        console.log(ALARMS);
                                    });
                                }
                                Actions.pop({ refresh: { isUpdated: Math.floor(Math.random() * 100000) } });

                            }, 100)
                        }}>
                            <View>
                                <Image source={require('./../../images/alarm/save.png')}
                                    style={{ width: 50, height: 50, resizeMode: 'contain', marginLeft: 50 }} />
                            </View>
                        </TouchableOpacity>

                    </View >

                </ ScrollView>

                <AlarmToneModal alarmToneModalVisible={this.state.alarmToneModal} closeAlarmToneModal={() => { this.setState({ alarmToneModal: false }) }}
                    onPressDownloadTones={() => {
                        {
                            this.setState({ alarmToneModal: false })
                            setTimeout(() => { this.setState({ downloadToneModal: true }) }, 100)
                        }
                    }}
                    onPressAlarmTones={() => {
                        {
                            this.setState({ alarmToneModal: false })
                            setTimeout(() => { this.setState({ toneModal: true }) }, 100)
                        }
                    }}
                    onPressMusic={() => {
                        {
                            this.setState({ alarmToneModal: false })
                            setTimeout(() => { this.setState({ musicToneModal: true }) }, 100)
                        }
                    }} />

                <SnoozeModal snoozeModalVisible={this.state.snoozeModal} closeSnoozeModal={() => { this.setState({ snoozeModal: false }) }}
                    onPressSelect={() => {
                        {
                            this.setState({ snoozeSelect: this.state.snoozeDefault })
                            setTimeout(() => {
                                alarmNotifData['snooze'] = parseInt(this.state.snoozeSelect);
                                this.setState({ snoozeModal: false })
                            }, 100)

                        }
                    }}
                    snoozeSelect={this.state.snoozeDefault}
                    snoozeOnValueChange={snooze => {
                        this.setState({ snoozeDefault: snooze })
                    }} />

                <DownloadTonesModal downloadToneModalVisible={this.state.downloadToneModal} closeDownloadToneModal={() => { this.setState({ downloadToneModal: false }) }}
                    onPressDownload={() => {
                        {
                            this.setState({ downloadToneModal: false })
                        }
                    }} onDownloadFinished={() => { Actions.refresh({ key: Math.random() * 100000 }) }} />

                <ToneModal toneModalVisible={this.state.toneModal}
                    closeToneModal={() => {
                        setTimeout(() => {
                            alarmNotifData['sound_name'] = this.props.alarmUri;
                            alarmNotifData['song_name'] = this.props.alarmTone;
                            this.setState({ toneModal: false, toneSelect: this.props.alarmTone });
                            AlarmModule.stopRingtone();
                            console.log(alarmNotifData);
                        }, 100)

                    }} />

                <MusicToneModal musicToneModalVisible={this.state.musicToneModal}
                    closeMusicToneModal={() => {
                        setTimeout(() => {
                            alarmNotifData['sound_name'] = this.props.alarmUri;
                            alarmNotifData['song_name'] = this.props.alarmTone;
                            this.setState({ musicToneModal: false, toneSelect: this.props.alarmTone });
                            Platform.OS === 'android' ? AlarmModule.stopRingtone() : AlarmModule.stopMusic();
                            console.log(alarmNotifData);
                        }, 100)
                    }}
                />
            </View >
        );
    }
}


const mapStateToProps = (state) => {
    const { alarmTone, alarmUri, isBstationHeadSetEnabled } = state.settingsModule;

    return {
        alarmTone, alarmUri, isBstationHeadSetEnabled
    };
};

export default connect(mapStateToProps, { updateSettingsModule })(AlarmDetail);

