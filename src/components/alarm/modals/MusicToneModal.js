import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ListView,
    Modal,
    PermissionsAndroid,
    ToastAndroid,
    Platform,
    TouchableWithoutFeedback
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from './../../../styles/widgets'
import { text } from './../../../styles/text';
import I18n from './../../../translate/i18n/i18n';
import { DEFAULT_TEXT_COLOR } from '../../../styles/colors';
import MusicFiles from 'react-native-get-music-files';
import { AlarmModule } from 'nb_native_modules';
import { connect } from 'react-redux';
import { updateSettingsModule } from '../../../redux/actions';

const inactiveColor = 'gray';
const activeColor = DEFAULT_TEXT_COLOR;
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
let dataSource = ds.cloneWithRows([]);
let selectedMusic;


class MusicToneModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fileName: '',
            path: ''
        }

        this.musicToneModalVisible = this.props.musicToneModalVisible;
        this.closeMusicToneModal = this.props.closeMusicToneModal;
        this.onPressSelect = this.props.onPressSelect;
    }

    componentWillUnmount() {
        dataSource = ds.cloneWithRows([]);
    }

    componentDidMount() {

        if (Platform.OS === 'android') {
            if (Platform.Version >= 23) {
                PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
                    if (result) {
                        this.getMusicFiles();
                    } else {
                        console.log('Permission is denied');
                    }
                });
            } else {
                this.getMusicFiles();
                console.log('No permission required in this device.');
            }
        } else {
            this.getMusicFiles();
        }

        this.setState({ fileName: this.props.alarmTone });
    }

    getMusicFiles() {
        MusicFiles.getAll({
            id: true,
            blured: false, // works only when 'cover' is set to true
            artist: true,
            duration: true, //default : true
            cover: false, //default : true,
            genre: true,
            title: true,
            date: true,
            lyrics: false,
            comments: false,
            minimumSongDuration: 10000, // get songs bigger than 10000 miliseconds duration,
            fields: ['title', 'albumTitle', 'genre', 'lyrics', 'artwork', 'duration', 'artist', 'assetUrl'] // for iOs Version
        }).then(response => { 
          console.log('getMusicFiles() SUCCESS: ', JSON.stringify(response));
          dataSource = ds.cloneWithRows(response);
        })
        .catch(error => { 
            console.log('ERROR: ', error);
        });
    }

    render() {
        return (

            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    style={widgets.modal}
                    visible={this.props.musicToneModalVisible}
                    presentation
                    onRequestClose={this.props.closeMusicToneModal}>
                    <TouchableWithoutFeedback onPressIn={this.props.closeMusicToneModal}>
                        <View style={[widgets.bluetoothScanPromptCon]}>
                            <TouchableWithoutFeedback onPressIn={() => { }}>
                                <View style={widgets.modalAlarmTone}>
                                    <View style={widgets.modalCloseCon} />
                                    <View style={[widgets.policyHeaderCon, { marginBottom: 5 }]}>
                                        <Text style={{ fontSize: 20, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('MUSIC_TONE')}</Text>
                                        <Text style={{ fontSize: 10, color: inactiveColor, textAlign: 'center' }}>{I18n.t('SELECT_YOUR_MUSIC_TONE')}</Text>
                                        <Text style={{ fontSize: 12, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('SELECTED')}: {this.state.fileName}</Text>
                                    </View>


                                    <ListView
                                        showsVerticalScrollIndicator={true}
                                        dataSource={dataSource}
                                        renderRow={(rowData) =>

                                            <TouchableOpacity onPress={() => {
                                                if (Platform.OS === 'android') {
                                                    ToastAndroid.show(rowData.fileName + ' selected', ToastAndroid.SHORT);
                                                }
                                                selectedMusic = (Platform.OS === 'android') ? rowData.path : rowData.assetUrl;
                                                AlarmModule.playRingtone(selectedMusic);
                                                this.setState({ fileName: (Platform.OS === 'android') ? rowData.fileName : rowData.title, path: (Platform.OS === 'android') ? rowData.path : rowData.assetUrl });
                                            }}>
                                                <Card containerStyle={[widgets.alarmPadding, { backgroundColor: 'white', borderColor: 'transparent' }]}>
                                                    <View style={widgets.commonConRow}>
                                                        <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                                            <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                                                {Platform.OS === 'ios' ? <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{rowData.title}</Text> :
                                                                    <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{rowData.fileName}</Text>
                                                                }
                                                            </View>
                                                            <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{rowData.author}</Text>
                                                        </View>
                                                    </View>
                                                </Card>
                                            </TouchableOpacity>
                                        }
                                    />

                                    <TouchableOpacity onPress={() => {
                                        this.props.updateSettingsModule({ prop: 'alarmTone', value: this.state.fileName });
                                        this.props.updateSettingsModule({ prop: 'alarmUri', value: this.state.path });
                                        this.props.closeMusicToneModal();
                                    }}>
                                        <View style={{ marginTop: 20, marginBottom: 20, alignSelf: 'center', borderColor: DEFAULT_TEXT_COLOR, borderWidth: 1, borderRadius: 20 }}>
                                            <Text style={{ fontSize: 15, padding: 10, color: DEFAULT_TEXT_COLOR, marginLeft: 20, marginRight: 20 }}> {I18n.t('SELECT')} </Text>
                                        </View>
                                    </TouchableOpacity>

                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View >

        );
    };
}

const mapStateToProps = (state) => {
    const { alarmTone, alarmUri } = state.settingsModule;

    return {
        alarmTone, alarmUri
    };
};

export default connect(mapStateToProps, { updateSettingsModule })(MusicToneModal);
