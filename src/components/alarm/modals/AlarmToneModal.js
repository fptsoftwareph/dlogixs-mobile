import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    Modal,
    TouchableWithoutFeedback
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from './../../../styles/widgets'
import { text } from './../../../styles/text';
import I18n from './../../../translate/i18n/i18n';
import PropTypes from 'prop-types';
import { DEFAULT_TEXT_COLOR } from '../../../styles/colors';

const AlarmToneModal = ({ alarmToneModalVisible, closeAlarmToneModal, onPressDownloadTones, onPressAlarmTones, onPressMusic }) => {
    const inactiveColor = 'gray';
    const activeColor = DEFAULT_TEXT_COLOR;

    return (

        <View>
            <Modal
                animationType="slide"
                transparent={true}
                style={widgets.modal}
                visible={alarmToneModalVisible}
                presentation
                onRequestClose={closeAlarmToneModal}>
                <TouchableWithoutFeedback onPressIn={closeAlarmToneModal}>
                    <View style={[widgets.bluetoothScanPromptCon]}>
                        <TouchableWithoutFeedback onPressIn={() => { }}>
                            <View style={widgets.modalAlarmTone}>
                                <View style={widgets.modalCloseCon} />
                                <View style={widgets.policyHeaderCon}>
                                    <Text style={{ fontSize: 20, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('ALARM_TONE').toUpperCase()}</Text>
                                    <Text style={{ fontSize: 10, color: inactiveColor, textAlign: 'center' }}>{I18n.t('SELECT_ALARM_TONE')}</Text>
                                </View>

                                <TouchableOpacity onPress={onPressDownloadTones}>
                                    <Card containerStyle={[widgets.commonPadding, { backgroundColor: 'white', borderColor: DEFAULT_TEXT_COLOR }]}>
                                        <View style={widgets.commonConRow}>
                                            <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                                <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                                    <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{I18n.t('DOWNLOAD_TONES')}</Text>
                                                </View>
                                                <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{I18n.t('APPLICATION')}</Text>
                                            </View>
                                            <View style={widgets.alignRightView}>
                                                <Image source={require('./../../../images/alarm/download.png')}
                                                    style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                            </View>
                                        </View>
                                    </Card>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={onPressAlarmTones}>
                                    <Card containerStyle={[widgets.commonPadding, { backgroundColor: 'white', borderColor: DEFAULT_TEXT_COLOR }]}>
                                        <View style={widgets.commonConRow}>
                                            <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                                <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                                    <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{I18n.t('ALARM_TONES')}</Text>
                                                </View>
                                                <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{I18n.t('PHONE')}</Text>
                                            </View>
                                            <View style={widgets.alignRightView}>
                                                <Image source={require('./../../../images/alarm/more.png')}
                                                    style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                            </View>
                                        </View>
                                    </Card>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={onPressMusic}>
                                    <Card containerStyle={[widgets.commonPadding, { backgroundColor: 'white', borderColor: DEFAULT_TEXT_COLOR }]}>
                                        <View style={widgets.commonConRow}>
                                            <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                                <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                                    <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{I18n.t('MUSIC')}</Text>
                                                </View>
                                                <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{I18n.t('PHONE')}</Text>
                                            </View>
                                            <View style={widgets.alignRightView}>
                                                <Image source={require('./../../../images/alarm/more.png')}
                                                    style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                            </View>
                                        </View>
                                    </Card>
                                </TouchableOpacity>


                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </View>

    );
};

AlarmToneModal.PropTypes = {
    alarmToneModalVisible: PropTypes.func.isRequired,
    closeAlarmToneModal: PropTypes.func.isRequired,
    onPressDownloadTones: PropTypes.func.isRequired,
    onPressAlarmTones: PropTypes.func.isRequired,
    onPressMusic: PropTypes.func.isRequired
};

export default AlarmToneModal;
