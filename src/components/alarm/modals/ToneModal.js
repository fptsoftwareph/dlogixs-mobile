import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ListView,
    Modal,
    TouchableWithoutFeedback
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from './../../../styles/widgets';
import { text } from './../../../styles/text';
import I18n from './../../../translate/i18n/i18n';
import { DEFAULT_TEXT_COLOR } from '../../../styles/colors';
import { AlarmModule } from 'nb_native_modules';
import { connect } from 'react-redux';
import { updateSettingsModule } from '../../../redux/actions';

const inactiveColor = 'gray';
const activeColor = DEFAULT_TEXT_COLOR;
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
let dataSource = ds.cloneWithRows([]);
let selectedMusic;


class ToneModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            alarm_title: '',
            alarm_uri: ''
        }

        this.toneModalVisible = this.props.toneModalVisible;
        this.closeToneModal = this.props.closeToneModal;
        this.onPressSelect = this.props.onPressSelect;
    }

    componentDidMount() {

        AlarmModule.getAlarmRingtones(results => {
            console.log('KINI: ', results);
            dataSource = ds.cloneWithRows(results)
        });

        this.setState({ alarm_title: this.props.alarmTone });
    }


    render() {
        return (

            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    style={widgets.modal}
                    visible={this.props.toneModalVisible}
                    presentation
                    onRequestClose={this.props.closeToneModal}>
                    <TouchableWithoutFeedback onPressIn={this.props.closeToneModal}>
                        <View style={[widgets.bluetoothScanPromptCon]}>
                            <TouchableWithoutFeedback onPressIn={() => { }}>
                                <View style={widgets.modalAlarmTone}>
                                    <View style={widgets.modalCloseCon} />
                                    <View style={[widgets.policyHeaderCon, { marginBottom: 5 }]}>
                                        <Text style={{ fontSize: 20, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('ALARM_TONE')}</Text>
                                        <Text style={{ fontSize: 10, color: inactiveColor, textAlign: 'center' }}>{I18n.t('SELECT_YOUR_ALARM_TONE')}</Text>
                                        <Text style={{ fontSize: 12, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('SELECTED')}: {this.state.alarm_title}</Text>
                                    </View>


                                    <ListView
                                        showsVerticalScrollIndicator={true}
                                        dataSource={dataSource}
                                        renderRow={(rowData) =>

                                            <TouchableOpacity onPress={() => {
                                                selectedMusic = rowData.alarm_uri;
                                                AlarmModule.playRingtone(selectedMusic);
                                                this.setState({ alarm_title: rowData.alarm_title, alarm_uri: rowData.alarm_uri })
                                            }}>
                                                <Card containerStyle={[widgets.alarmPadding, { backgroundColor: 'white', borderColor: 'transparent' }]}>
                                                    <View style={widgets.commonConRow}>
                                                        <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                                            <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                                                <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{rowData.alarm_title}</Text>
                                                            </View>
                                                            <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{rowData.author}</Text>
                                                        </View>
                                                    </View>
                                                </Card>
                                            </TouchableOpacity>
                                        }
                                    />

                                    <TouchableOpacity onPress={() => {
                                        this.props.updateSettingsModule({ prop: 'alarmTone', value: this.state.alarm_title });
                                        this.props.updateSettingsModule({ prop: 'alarmUri', value: this.state.alarm_uri });
                                        this.props.closeToneModal();
                                    }}>
                                        <View style={{ marginTop: 20, marginBottom: 20, alignSelf: 'center', borderColor: DEFAULT_TEXT_COLOR, borderWidth: 1, borderRadius: 20 }}>
                                            <Text style={{ fontSize: 15, padding: 10, color: DEFAULT_TEXT_COLOR, marginLeft: 20, marginRight: 20 }}> {I18n.t('SELECT')} </Text>
                                        </View>
                                    </TouchableOpacity>

                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>

        );
    };
}

const mapStateToProps = (state) => {
    const { alarmTone, alarmUri } = state.settingsModule;

    return {
        alarmTone, alarmUri
    };
};

export default connect(mapStateToProps, { updateSettingsModule })(ToneModal);
