import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    Modal,
    Picker,
    Platform
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { widgets } from './../../../styles/widgets'
import { text } from './../../../styles/text';
import I18n from './../../../translate/i18n/i18n';
import PropTypes from 'prop-types';
import { DEFAULT_TEXT_COLOR } from '../../../styles/colors';

const SnoozeModal = ({ snoozeModalVisible, closeSnoozeModal, onPressSelect, snoozeSelect, snoozeOnValueChange }) => {
    const inactiveColor = 'gray';
    const activeColor = DEFAULT_TEXT_COLOR;

    return (

        <View>
            <Modal
                animationType="slide"
                transparent={true}
                style={widgets.modal}
                visible={snoozeModalVisible}
                presentation
                onRequestClose={closeSnoozeModal}>
                <View style={widgets.modalSnooze}>
                    <View style={widgets.modalCloseCon} />
                    <View style={widgets.policyHeaderCon}>

                    </View>

                    <Text style={{ fontSize: 20, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('SNOOZE')}</Text>
                    <Text style={{ fontSize: 10, color: inactiveColor, textAlign: 'center' }}>{I18n.t('SELECT_SNOOZE_TIME')}</Text>

                    <View style={{
                        marginTop: 30,
                        ...Platform.select({
                            ios: {
                                marginTop: 10,
                            }
                        })
                    }}>
                        <Picker
                            style={[widgets.dropdownPadding, { color: DEFAULT_TEXT_COLOR }]}
                            selectedValue={snoozeSelect}
                            onValueChange={snoozeOnValueChange}>
                            <Picker.Item label={"1 " + I18n.t('MINUTE')} value="1" />
                            <Picker.Item label={"5 " + I18n.t('MINUTES')} value="5" />
                            <Picker.Item label={"10 " + I18n.t('MINUTES')} value="10" />
                            <Picker.Item label={"15 " + I18n.t('MINUTES')} value="15" />
                            <Picker.Item label={"30 " + I18n.t('MINUTES')} value="30" />
                        </Picker>
                    </View>

                    <TouchableOpacity onPress={onPressSelect}>
                        <View style={{
                            marginTop: 50,
                            ...Platform.select({
                                ios: {
                                    marginTop: 10,
                                }
                            }), alignSelf: 'center', borderColor: DEFAULT_TEXT_COLOR, borderWidth: 1, borderRadius: 20
                        }}>
                            <Text style={{ fontSize: 15, padding: 10, color: DEFAULT_TEXT_COLOR, marginLeft: 20, marginRight: 20 }}> {I18n.t('SELECT')} </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </Modal>
        </View >

    );
};

SnoozeModal.PropTypes = {
    snoozeModalVisible: PropTypes.func.isRequired,
    closeSnoozeModal: PropTypes.func.isRequired,
    onPressSelect: PropTypes.func.isRequired,
    snoozeSelect: PropTypes.func.isRequired,
    snoozeOnValueChange: PropTypes.func.isRequired,
};

export default SnoozeModal;
