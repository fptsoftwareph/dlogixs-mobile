import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ListView,
    Modal,
    Platform,
    TouchableWithoutFeedback,
    NetInfo
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from './../../../styles/widgets'
import CheckBox from 'react-native-check-box';
import Toast from 'react-native-simple-toast';
import { text } from './../../../styles/text';
import I18n from './../../../translate/i18n/i18n';
import { DEFAULT_TEXT_COLOR, COLOR_THUMB_TINT } from '../../../styles/colors';
import { AlarmModule } from 'nb_native_modules';
import { showAlert, showToast } from '../../../utils/CommonMethods';
import * as Progress from 'react-native-progress'
import ReactNativeModal from 'react-native-modal';
import RenderIf from '../../RenderIf';

let DOWNLOADS = [];

let config;

let sampleData;


const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

let dataSource;
const inactiveColor = 'gray';
const activeColor = DEFAULT_TEXT_COLOR;


class DownloadTonesModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fileName: '',
            path: '',
            progressModal: false
        }

        this.downloadToneModalVisible = this.props.downloadToneModalVisible;
        this.closeDownloadToneModal = this.props.closeDownloadToneModal;
        this.onPressDownload = this.props.onPressDownload;
        this.onDownloadFinished = this.props.onDownloadFinished;
    }


    componentDidMount() {
        if (Platform.OS === 'ios') {

            sampleData = [{
                "songName": "Fruhlingslied",
                "songArtist": "Artist",
                "url": "https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/Fruhlingslied.caf",
                "isChecked": false,
            }, {
                "songName": "Greensleeves",
                "songArtist": "Artist",
                "url": "https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/Greensleeves.caf",
                "isChecked": false,
            }, {
                "songName": "Home on the Range",
                "songArtist": "Artist",
                "url": "https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/Home-on-the-Range.caf",
                "isChecked": false,
            }];

        }
        else {
            sampleData =
                [{
                    "songName": "Fruhlingslied",
                    "songArtist": "Artist",
                    "url": "https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/Fruhlingslied.mp3",
                    "isChecked": false,
                }, {
                    "songName": "Fur elise",
                    "songArtist": "Artist",
                    "url": "https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/Fur-elise.mp3",
                    "isChecked": false,
                }, {
                    "songName": "Greensleeves",
                    "songArtist": "Artist",
                    "url": "https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/Greensleeves.mp3",
                    "isChecked": false,
                }, {
                    "songName": "Home on the Range",
                    "songArtist": "Artist",
                    "url": "https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/Home-on-the-Range.mp3",
                    "isChecked": false,
                }];

        }

        dataSource = ds.cloneWithRows(sampleData);
    }

    render() {
        return (

            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    style={widgets.modal}
                    visible={this.props.downloadToneModalVisible}
                    presentation
                    onRequestClose={this.props.closeDownloadToneModal}>
                    <TouchableWithoutFeedback onPressIn={this.props.closeDownloadToneModal}>
                        <View style={[widgets.bluetoothScanPromptCon]}>
                            <TouchableWithoutFeedback onPressIn={() => { }}>
                                <View style={widgets.modalAlarmTone}>
                                    <View style={widgets.modalCloseCon} />
                                    <View style={widgets.policyHeaderCon}>
                                        <Text style={{ fontSize: 20, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('DOWNLOAD_TONES')}</Text>
                                        <Text style={{ fontSize: 10, color: inactiveColor, textAlign: 'center' }}>{I18n.t('SELECT_TO_DOWNLOAD')}</Text>
                                    </View>


                                    <ListView
                                        showsVerticalScrollIndicator={true}
                                        dataSource={dataSource}
                                        renderRow={(rowData) =>

                                            <TouchableOpacity>
                                                <Card containerStyle={[widgets.alarmPadding, { backgroundColor: 'white', borderColor: 'transparent' }]}>
                                                    <View style={widgets.commonConRow}>
                                                        <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                                                            <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                                                                <Text style={[text.title, { color: DEFAULT_TEXT_COLOR }]}>{rowData.songName}</Text>
                                                            </View>
                                                            <Text style={[text.subTitle, widgets.commonMarginLeft2, { color: inactiveColor }]}>{rowData.songArtist}</Text>
                                                        </View>
                                                        <View style={widgets.alignRightView}>
                                                            <CheckBox
                                                                checkBoxColor={DEFAULT_TEXT_COLOR}
                                                                onClick={() => {
                                                                    if (rowData.isChecked === false) {
                                                                        rowData.isChecked = true
                                                                        DOWNLOADS.push(rowData);
                                                                    }
                                                                    else {
                                                                        rowData.isChecked = false
                                                                        DOWNLOADS = DOWNLOADS.filter(item => item !== rowData);
                                                                    }
                                                                    console.log('rowData:', rowData);
                                                                    console.log('DOWNLOADS:', DOWNLOADS);
                                                                }}
                                                                isChecked={rowData.isChecked}
                                                            />
                                                        </View>
                                                    </View>
                                                </Card>
                                            </TouchableOpacity>

                                        }
                                    />

                                    <TouchableOpacity onPress={() => {

                                        NetInfo.isConnected.fetch().then(isConnected => {
                                            if (isConnected && DOWNLOADS.length > 0) {
                                                showToast(I18n.t('DOWNLOADING'));
                                            }
                                            if (isConnected) {
                                                for (let i = 0; i < DOWNLOADS.length; i++) {

                                                    config = {
                                                        downloadTitle: DOWNLOADS[i]['songName'],
                                                        downloadDescription: 'NeuroBeat Alarm Sound',
                                                        saveAsName: DOWNLOADS[i]['songName'] + '.mp3',
                                                        ...Platform.select({
                                                            ios: {
                                                                saveAsName: DOWNLOADS[i]['songName'] + '.caf'
                                                            }
                                                        }),
                                                        allowedInRoaming: true,
                                                        allowedInMetered: true,
                                                        showInDownloads: true,
                                                        external: true,
                                                        path: Platform.OS === 'android' ? 'Download/' : 'Sounds/'
                                                    };
                                                    console.log('downloadRingtone() config: ', JSON.stringify(config));
                                                    AlarmModule.downloadRingtone(DOWNLOADS[i]['url'], {}, config, response => {
                                                        if (Platform.OS === 'ios') {
                                                            setTimeout(() => {
                                                                this.setState({ progressModal: false });
                                                                setTimeout(() => {
                                                                    if (response === 'Download Successfully') {
                                                                        this.props.onDownloadFinished();
                                                                        showToast(I18n.t('DOWNLOAD_SUCCESS'));
                                                                    }
                                                                    else {
                                                                        showToast(I18n.t('DOWNLOAD_ERROR'));
                                                                    }

                                                                }, 2000)
                                                            }, 300)

                                                        }
                                                        else {

                                                            if (response === 'SUCCESS') {
                                                                this.props.onDownloadFinished()
                                                                showToast(I18n.t('DOWNLOAD_SUCCESS'));
                                                            }
                                                            else {
                                                                showToast(I18n.t('DOWNLOAD_ERROR'));
                                                            }
                                                        }
                                                    });

                                                }
                                                this.props.onPressDownload();
                                                if (Platform.OS === 'ios') {
                                                    if (DOWNLOADS.length > 0) {
                                                        setTimeout(() => { this.setState({ progressModal: true }) }, 300)
                                                    }
                                                }
                                                DOWNLOADS = [];
                                            }
                                            else {
                                                this.props.closeDownloadToneModal();
                                                if (Platform.OS === 'ios') {
                                                    setTimeout(() => {
                                                        showAlert('', I18n.t('NO_INTERNET'));
                                                    }, 2000);
                                                }
                                                else {
                                                    setTimeout(() => {
                                                        showAlert('', I18n.t('NO_INTERNET'));
                                                    }, 25000);

                                                }


                                            }
                                        });


                                    }
                                    }>
                                        <View style={{ marginTop: 20, marginBottom: 20, alignSelf: 'center', borderColor: DEFAULT_TEXT_COLOR, borderWidth: 1, borderRadius: 20 }}>
                                            <Text style={{ fontSize: 15, padding: 10, color: DEFAULT_TEXT_COLOR, marginLeft: 20, marginRight: 20 }}> {I18n.t('DOWNLOAD')} </Text>
                                        </View>
                                    </TouchableOpacity>

                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>

                {RenderIf(Platform.OS === 'ios')(
                    <ReactNativeModal
                        animationIn='zoomIn'
                        animationOut='zoomOut'
                        backdropColor='transparent'
                        onBackButtonPress={() => {
                            this.setState({ progressModal: false });
                        }}
                        isVisible={this.state.progressModal}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 50 }}>
                            <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
                        </View>
                    </ReactNativeModal>
                )}

            </View >

        );
    };
}


export default DownloadTonesModal;
