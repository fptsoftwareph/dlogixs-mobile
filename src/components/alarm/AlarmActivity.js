import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView,
    NativeEventEmitter,
    NativeModules
} from 'react-native';
import { Card } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { widgets } from './../../styles/widgets'
import { text } from './../../styles/text';
import I18n from './../../translate/i18n/i18n';
import { DEFAULT_TEXT_COLOR } from '../../styles/colors';
import { AlarmModule } from 'nb_native_modules';
import { getPreference } from '../../utils/CommonMethods';

const inactiveColor = 'gray';


class AlarmActivity extends Component {
    constructor() {
        super();
        this.state = { alarmIndex: '', snooze: 0 }

        getPreference('alarmIndex').then(alarmIndex => {
            this.setState({ alarmIndex: alarmIndex });
        });
    }

    componentWillUnmount() {

        if (this.state.snooze !== 1) {
            AlarmModule.onDismiss(parseInt(this.state.alarmIndex));
        }
    }

    onSnooze() {
        AlarmModule.onSnooze(parseInt(this.state.alarmIndex));
        this.setState({ snooze: 1 })
        Actions.pop();
    }

    onStop() {
        // AlarmModule.onDismiss(parseInt(this.state.alarmIndex));
        Actions.pop();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>

                    <View style={[widgets.policyHeaderCon, { marginTop: 50 }]}>
                        <Text style={{ fontSize: 30, color: DEFAULT_TEXT_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{I18n.t('ALARM')}</Text>
                        <Text style={{ fontSize: 15, color: inactiveColor, textAlign: 'center' }}>{I18n.t('SELECT_SNOOZE_STOP')}</Text>
                    </View>

                    <Image source={require('./../../images/alarm/alarm.png')}
                        style={{ marginTop: 20, resizeMode: 'contain', alignSelf: 'center' }} />


                    <View style={{
                        marginTop: 80,
                        flexDirection: 'row',
                        justifyContent: 'space-around'
                    }}>
                        <TouchableOpacity onPress={() => { this.onSnooze() }}>
                            <View style={{ alignSelf: 'center', borderColor: DEFAULT_TEXT_COLOR, borderWidth: 1, borderRadius: 20 }}>
                                <Text style={{ fontSize: 15, padding: 10, color: DEFAULT_TEXT_COLOR, marginLeft: 20, marginRight: 20 }}>{I18n.t('SNOOZE')}</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { this.onStop() }}>
                            <View style={{ alignSelf: 'center', borderColor: DEFAULT_TEXT_COLOR, borderWidth: 1, borderRadius: 20 }}>
                                <Text style={{ fontSize: 15, padding: 10, color: DEFAULT_TEXT_COLOR, marginLeft: 30, marginRight: 30 }}>{I18n.t('STOP')}</Text>
                            </View>
                        </TouchableOpacity>
                    </View >
                </ScrollView>
            </View>
        );
    }
}

export default AlarmActivity;
