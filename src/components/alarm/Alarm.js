import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Switch,
  ScrollView,
  PermissionsAndroid,
  ListView,
  Alert,
  Platform,
  NativeEventEmitter,
  NativeModules
} from 'react-native';
import { Card } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { widgets } from './../../styles/widgets'
import { text } from './../../styles/text';
import I18n from './../../translate/i18n/i18n';
import { COLOR_THUMB_TINT, COLOR_TINT_ON } from '../../styles/colors';
import { getPreference, showAlertWithActions } from '../../utils/CommonMethods';
import { AlarmModule, StationModule } from 'nb_native_modules';
import { connect } from 'react-redux';
import { updateSettingsModule, manageBinaural } from '../../redux/actions';
import Binaural from '../../components/common/BinauralFunctions';

let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
let ALARMS = [];
let alarmObject = {};
let staticAlarmObject = {};
let alarmMeridiem = '', alarmTime = '', alarmMins = '', alarmHours = '', actualAlarmTime = '', hour = '';
let alarmNotifData = {};

const AlarmManagerModule = NativeModules.AlarmModule;
const AlarmManagerEmitter = new NativeEventEmitter(AlarmManagerModule);

class Alarm extends Component {
  constructor() {
    super();

    this.state = {
      rowId: 0,
      dataSource: ds,
      _rev: '',
      _id: '',
      isAlarmNotRingingAndSwitchOff: true
    }

    this.deleteAlarm = this.deleteAlarm.bind(this);
    this.setRbsEnabled = this.setRbsEnabled.bind(this);

  }

  setRbsEnabled(isEnabled) {
    console.log('Alarm setRbsEnabled isEnabled: ' + isEnabled);
    if (!isEnabled) {
      this.props.manageBinaural({ prop: 'binauralStart', value: 0 });
    }

    StationModule.setBB(isEnabled ? 1 : 0, true, response => {
      console.log('Alarm StationModule.setBB response: ' + response);
      if (response === true) {
        if (isEnabled) {
          this.props.manageBinaural({ prop: 'binauralStart', value: new Date() });
          this.binaural.setBinauralTimer(new Date());
        } else {
          this.binaural.clearBinauralTimer();
        }

        console.log("Alarm setRbsEnabled this.props.isRbsEnabled: " + this.props.isRbsEnabled);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    this.populateAlarm();
  }

  componentWillUnmount() {
    this.handleAlarmRbsStatus.remove();
    this.handleAlarmRbsStop.remove();
    ALARMS = [];
  }

  componentDidMount() {
    this.populateAlarm();
    if (Platform.OS === 'android') {
      if (Platform.Version >= 23) {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
          if (result) {
            console.log('Permission is OK');
          } else {
            PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]).then((result2) => {
              if (result2) {
                console.log('User accept');
              } else {
                console.log('User denied');
              }
            });
          }
        });
      } else {
        console.log('No permission required in this device.');
      }
    }

    console.log('Alarm componentDidMount');
    console.log("Alarm componentDidMount this.props.isRbsEnabled: " + this.props.isRbsEnabled);

    this.binaural = Binaural.getInstance(this.props.targetBinauralHours);

    this.handleAlarmRbsStatus = AlarmManagerEmitter.addListener('AlarmStatusListener', status => {
      console.log("AlarmStatusListener: " + status);
      this.setState({ isAlarmNotRingingAndSwitchOff: false });

      if (this.props.isBstationHeadSetEnabled) {
        if (!this.props.isRbsEnabled && status === true) {
          this.setRbsEnabled(true);
        } else if (this.props.isRbsEnabled && status === false) {
          this.setRbsEnabled(false);
        }
      }
    });

    this.handleAlarmRbsStop = AlarmManagerEmitter.addListener('AlarmNotificationListener', result => {
      this.setState({ isAlarmNotRingingAndSwitchOff: true });

      console.log(result);
      console.log("AlarmStopListener: " + result);
      console.log("AlarmStopListener this.props.isRbsEnabled: " + this.props.isRbsEnabled);

      if (this.props.isBstationHeadSetEnabled) {
        this.setRbsEnabled(this.props.isRbsEnabled);
      }

      this.populateAlarm();
    });
  }

  populateAlarm() {
    ALARMS = [];
    this.setState({ dataSource: ds.cloneWithRows(ALARMS) });

    AlarmModule.getScheduledAlarms(results => {
      console.log("getScheduledAlarms(): ", JSON.stringify(results));
      for (let i = 0; i < results.length; i++) {
        ALARMS[i] = results[i];
      }
      this.setState({ dataSource: ds.cloneWithRows(ALARMS), _rev: '0', _id: '0' })
      console.log('KINI: ', results);
      console.log('_rev: ', this.state._rev);
      console.log('_id: ', this.state._id);
    });

    getPreference('userId').then(res => {
      console.log('123redux: getPreference userId: ' + res);
      user_id = res;
    })
  }

  deleteAlarm() {

    AlarmModule.scheduleDelete(this.state.rowId);
    this.populateAlarm();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          <View>
            <ListView
              showsVerticalScrollIndicator={true}
              dataSource={this.state.dataSource}
              renderRow={(rowData, sectionID, rowID, higlightRow) =>
                <TouchableOpacity onPress={() => {
                  alarmObject = {
                    alarmId: rowData.alarm_id,
                    ...Platform.select({
                      ios: {
                        alarmId: rowID,
                      }
                    }),
                    serverId: rowID,
                    alarmTime: rowData.alarm_time,
                    alarmMeridiem: rowData.alarm_meridiem,
                    actualAlarmTime: rowData.hours + ':' + rowData.minute,
                    repeat_days: rowData.repeat_days,
                    snooze: rowData.snooze,
                    song_name: rowData.song_name,
                    song_uri: rowData.song_uri,
                    rbs_enabled: rowData.rbs_enabled
                  };
                  console.log('Alarm detail: ' + JSON.stringify(alarmObject));
                  Actions.alarmDetail({
                    alarmObject: alarmObject
                  });
                }}>
                  <Card containerStyle={widgets.commonPadding}>
                    <View style={widgets.commonConRow}>
                      <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                        <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                          <TouchableOpacity onPress={() => {

                            if (Platform.OS === 'ios') {
                              this.setState({ rowId: parseInt(rowID) })
                            } else {
                              this.setState({ rowId: parseInt(rowData.alarm_id) })
                            }

                            setTimeout(() => {
                              showAlertWithActions('', I18n.t('DELETE_ALARM'), this.deleteAlarm);
                            }, 100)

                          }}>
                            <Image style={[widgets.alarmModuleCloseIcon, { marginRight: 10, marginTop: 5 }]}
                              source={require('./../../images/alarm/close.png')} />
                          </TouchableOpacity>
                          <View style={widgets.commonConColumn}>
                          <Text style={text.textAlarmTitle}>{rowData.alarm_time}</Text>
                          <Text style={text.subTitle}>{rowData.rbs_enabled === true ? I18n.t('RBS_ENABLED') : I18n.t('RBS_DISABLED')}</Text>
                          </View>
                          <Text style={[text.subTitle, widgets.commonMarginLeft, text.textAlarmSubtitle, { marginTop: 10 }]}>{rowData.alarm_meridiem}</Text>
                        </View>
                      </View>
                      <View style={widgets.alignRightView}>
                        <Switch
                          thumbTintColor={COLOR_THUMB_TINT}
                          style={widgets.switch}
                          onTintColor={COLOR_TINT_ON}
                          onValueChange={value => {
                            if (Platform.OS === 'ios') {
                              AlarmModule.scheduleToggle(parseInt(rowID));
                            }
                            else {
                              AlarmModule.scheduleToggle(rowData.alarm_id, this.state.isAlarmNotRingingAndSwitchOff);
                            }
                            setTimeout(() => {
                              this.populateAlarm();
                            }, 100)

                          }}
                          value={rowData.alarm_enabled}
                        />
                      </View>
                    </View>
                  </Card>
                </TouchableOpacity>

              }
            />

            <View style={{ alignItems: 'center', alignContent: 'center', marginTop: 20, marginBottom: 20 }}>
              <TouchableOpacity onPress={() => {
                {
                  alarmHours = new Date().getHours();
                  hour = new Date().getHours();
                  if (alarmHours >= 12) {
                    alarmMeridiem = I18n.t('PM')
                    alarmHours = alarmHours - 12;
                    if (alarmHours < 10) {
                      alarmHours = '0' + alarmHours;
                    }
                    if (alarmHours === 0 || alarmHours === '00') {
                      alarmHours = 12;
                    }
                  }
                  else {
                    if (alarmHours === 0 || alarmHours === '00') {
                      alarmHours = 12;
                    }

                    if (alarmHours < 10) {
                      alarmHours = '0' + alarmHours;
                    }
                    alarmMeridiem = I18n.t('AM')
                  }

                  if (new Date().getMinutes() < 10) {

                    if (parseInt(new Date().getMinutes() + 2) == 10 || parseInt(new Date().getMinutes() + 2) == 11) {
                      alarmMins = parseInt(new Date().getMinutes() + 2)
                    }
                    else {
                      alarmMins = '0' + parseInt(new Date().getMinutes() + 2)
                    }
                  }

                  else {
                    if (parseInt(new Date().getMinutes() + 2) == 60) {
                      alarmMins = '0' + 0;
                      if (hour === 12) {
                        hour = 13;
                        alarmHours = '0' + 1;
                      }
                      else if (hour === 0) {
                        hour = 1;
                        alarmHours = '0' + 1;
                      }
                      else {
                        hour = hour + 1;
                        alarmHours = parseInt(alarmHours) + 1;
                      }

                    }
                    else if (parseInt(new Date().getMinutes() + 2) == 61) {
                      alarmMins = '0' + 1;
                      if (hour === 12) {
                        hour = 13;
                        alarmHours = '0' + 1;
                      }
                      else if (hour === 0) {
                        hour = 1;
                        alarmHours = '0' + 1;
                      }
                      else {
                        hour = hour + 1;
                        if (parseInt(alarmHours) + 1 < 10) {
                          alarmHours = '0' + (parseInt(alarmHours) + 1);
                        }
                        else {
                          alarmHours = parseInt(alarmHours) + 1;
                        }
                      }
                    }
                    else {
                      alarmMins = new Date().getMinutes() + 2
                    }
                  }

                  alarmTime = alarmHours + ':' + alarmMins;
                  actualAlarmTime = hour + ':' + alarmMins;
                  console.log('TIME:', actualAlarmTime);
                  staticAlarmObject = {
                    alarmTime: alarmTime,
                    alarmId: null,
                    serverId: null,
                    alarmMeridiem: alarmMeridiem,
                    actualAlarmTime: actualAlarmTime,
                    repeat_days: [false, false, false, false, false, false, false],
                    snooze: 1,
                    song_name: I18n.t('DEFAULT_TONE'),
                    song_uri: ''
                  };
                  Actions.alarmDetail({ alarmObject: staticAlarmObject })
                }
              }}>
                <Image
                  style={{
                    width: 50,
                    height: 50
                  }}
                  source={require('./../../images/alarm/add.png')}
                />
              </TouchableOpacity>
            </View>

          </View>
        </ScrollView>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { isBstationHeadSetEnabled, isRbsEnabled, targetBinauralHours } = state.settingsModule;

  return { isBstationHeadSetEnabled, isRbsEnabled, targetBinauralHours };
};

export default connect(mapStateToProps, { updateSettingsModule, manageBinaural })(Alarm);
