import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { getBooleanPreference, getPreference, setLocalization } from '../utils/CommonMethods';

class Splash extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    getPreference('language').then(lang => {
      setLocalization(lang);
    });

    setTimeout(() => {
      getBooleanPreference('isAlreadyPickLanguage')
        .then(response => {
          console.log(`isAlreadyPickLanguage Splash value: ${response}`);

          if (response) {
            if (this.props.isSignedIn) {
              Actions.homePage();
            } else {
              Actions.login();
            }
          } else {
            Actions.selectLang();
          }
        })
        .catch(error => console.log(error));
    }, 3000);
    return <Image source={require('./../images/splash_sceen.png')} style={styles.backgroundImage} />;
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  }
});

const mapStateToProps = state => {
  const { isSignedIn } = state.loginModule;

  return { isSignedIn };
};

export default connect(mapStateToProps, null)(Splash);
