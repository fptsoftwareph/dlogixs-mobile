import React, { Component } from 'react';
import { widgets } from '../styles/widgets';
import { text } from '../styles/text';
import {
  View, Text, Image,
  TextInput, TouchableOpacity,
  ScrollView, Alert, Platform,
  NetInfo
} from 'react-native';
import { updateUPModule, resetUPModule } from '../redux/actions';
import DatePicker from 'react-native-datepicker'
import { connect } from 'react-redux';
import I18n from './../translate/i18n/i18n';
import Picker from 'react-native-picker';
import RenderIf from './RenderIf';
import { APIClient } from '../utils/APIClient';
import { showAlert } from '../utils/CommonMethods';


const image = require("../images/edit.png");
let userinfo = {};

class UserProfile extends Component {

  constructor(props) {
    super(props);

    this.state = {
      textcolor: text.titleColorGray,
      nameColor: '#3bafda',
      editable: false,
      disabled: true,
      clicked: false,
      editclicked: false,
      pickerenable: false,
      firstName: this.props.onFirstNameChange,
      emailAdd: this.props.onEmailChange,
      country: this.props.onCountryChange,
      birthDate: this.props.onBirthdateChange,
      gender: this.props.onGenderChange,
      occupation: this.props.onOccupationChange,
      weight: this.props.onWeightChange,
      height: this.props.onHeightChange,
      picker_underline_color: '#e0e2e5',
      dropdowncolor: 'gray',
      display: 'none',
      displayText: 'flex',
      image_edit: '../images/edit.png',
    };

    apiClient = new APIClient();

  }

  componentDidMount() {
    if (this.props.onCountryChange === I18n.t('COUNTRY') || this.props.onBirthdateChange === '' || this.props.onGenderChange === I18n.t('GENDER') ||
      (this.props.onWeightChange === undefined || this.props.onWeightChange === '')) {
      this.editProfile();
    }
  }

  Picker1() {
    let country_data = [];
    if (this.state.editclicked) {
      for (let i = 0; i < countries.length; i++) {
        country_data[i] = countries[i]['name'];
      }
      Picker.init({
        pickerTitleText: I18n.t('PLEASE_SELECT'),
        pickerConfirmBtnText: I18n.t('CONFIRM'),
        pickerCancelBtnText: I18n.t('CANCEL'),
        pickerData: country_data,
        selectedValue: [this.state.country],
        onPickerConfirm: countyData => {
          this.setState({ country: countyData[0] });
          Picker.hide()
        },
        onPickerCancel: countyData => {
          Picker.hide()
        },
        onPickerSelect: countyData => {
        }
      });
      Picker.show()
    }

  }

  Picker2() {
    let gender_data = [I18n.t('MALE'), I18n.t('FEMALE')];
    if (this.state.editclicked) {
      Picker.init({
        pickerTitleText: I18n.t('PLEASE_SELECT'),
        pickerConfirmBtnText: I18n.t('CONFIRM'),
        pickerCancelBtnText: I18n.t('CANCEL'),
        pickerData: gender_data,
        selectedValue: [this.state.gender],
        onPickerConfirm: genderData => {
          this.setState({ gender: genderData[0] });
          Picker.hide()
        },
        onPickerCancel: genderData => {
          Picker.hide()
        },
        onPickerSelect: genderData => {
        }
      });
      Picker.show()
    }
  }

  componentWillUnmount() {
    Picker.hide();
    this.setState({ dropdowncolor: 'gray', textcolor: text.titleColorGray });
    image = require("../images/edit.png");
  }

  onOkClick() {
    userinfo = {
      weight: this.state.weight,
      height: this.state.height,
      country: this.state.country,
      birthdate: this.state.birthDate,
      gender: this.state.gender,
      occupation: this.state.occupation
    };
    console.log('userinfo: ', userinfo);

    NetInfo.isConnected.fetch()
    .then(isConnected => {
      if (isConnected) {
        Alert.alert(
          '',
          I18n.t('UPDATE_SUCCESSFUL'),
          [
            {
              text: I18n.t('OK'), onPress: () => {
                console.log('Ok Pressed')
    
                this.props.updateUPModule({ prop: 'onCountryChange', value: this.state.country });
                this.props.updateUPModule({ prop: 'onBirthdateChange', value: this.state.birthDate });
                this.props.updateUPModule({ prop: 'onGenderChange', value: this.state.gender });
                this.props.updateUPModule({ prop: 'onOccupationChange', value: this.state.occupation });
                this.props.updateUPModule({ prop: 'onWeightChange', value: this.state.weight });
                this.props.updateUPModule({ prop: 'onHeightChange', value: this.state.height });
    
                apiClient.updateProfile(userinfo);
    
              }, style: 'cancel'
            },
          ],
          { cancelable: false }
        );
      } else {
        showAlert('', I18n.t('NO_INTERNET'));
      }
    });
    image = require("../images/edit.png");

    this.setState({ editable: false, disabled: true, display: 'none', clicked: true, displayText: 'flex', editclicked: false, picker_underline_color: '#e0e1e2', dropdowncolor: 'gray', textcolor: text.titleColorGray, nameColor: '#3bafda' });

  }

  editProfile = () => {
    image = require("../images/edit1.png");

    this.setState({ editable: true, disabled: false, display: 'flex', displayText: 'none', editclicked: true, picker_underline_color: 'black', dropdowncolor: '#3bafda', textcolor: text.titleColorBlue, nameColor: 'gray' });

  };

  isValueInt(value) {
    var x;
    if (isNaN(value)) {
      return false;
    }
    x = parseFloat(value);
    return (x | 0) === x;
  }

  onclickbtnUpdate = () => {

    if (this.state.weight === '' || this.state.birthDate === '' || this.state.country === I18n.t('COUNTRY') || this.state.gender === I18n.t('GENDER')) {
      showAlert('', I18n.t('FILL_UP_REQUIRED'));
    }
    else if (this.state.weight !== '' && !this.isValueInt(this.state.weight)) {
      showAlert('', I18n.t('INVALID_WEIGHT'));
    }
    else if (this.state.height !== '' && !this.isValueInt(this.state.height)) {
      showAlert('', I18n.t('INVALID_HEIGHT'));
    }
    else {
      Alert.alert(
        '',
        I18n.t('SAVE_CHANGES'),
        [
          { text: I18n.t('CANCEL'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: I18n.t('OK'), onPress: () => this.onOkClick() }
        ],
        { cancelable: false }
      )
    }
  };

  render() {

    return (
      <ScrollView>
        <View>

          <View style={widgets.idPassCon}>
            <View style={widgets.commonFlex}>
              <Text style={[text.titleFirstname, { color: this.state.nameColor }]}>
                {this.state.firstName}
              </Text>

            </View>


            <View style={widgets.alignRightView}>
              <TouchableOpacity onPress={this.editProfile} >
                <Image source={image} style={{ height: 20, width: 20 }} />
              </TouchableOpacity>
            </View>

          </View>
          <ScrollView style={widgets.scrollView}>
            <View>
              <View style={widgets.idPassCon}>
                <Image source={require('../images/email.png')}
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    marginRight: 30
                  }} />
                <Text style={{ display: this.state.display }}> </Text>
                <TextInput
                  maxLength={30}
                  placeholder={I18n.t('EMAIL')}
                  editable={false}
                  style={[widgets.loginTextInput, widgets.commonFont, text.titleColorGray]}
                  value={this.state.emailAdd} />
              </View>
              <View style={widgets.idPassCon} />


              <View style={widgets.idPassCon}>
                <Image source={require('../images/location.png')}
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    marginRight: 44
                  }} />
                <Text style={{ display: this.state.display }}> </Text>
                <TouchableOpacity style={{ flex: 1, marginRight: 3 }} onPress={() => { { this.Picker1() } }}>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderBottomColor: this.state.picker_underline_color,
                      ...Platform.select({
                        ios: {
                          borderBottomWidth: 1,
                          borderBottomColor: 'gray',
                          marginLeft: -5
                        }
                      })
                    }}>
                    <Text
                      style={{
                        flex: 1,
                        backgroundColor: 'transparent',
                        textAlign: 'left',
                        color: this.state.dropdowncolor,
                        fontSize: 15,
                        marginBottom: 5,
                      }}>{this.state.country}</Text>
                  </View>
                </TouchableOpacity>

              </View>
              <View style={widgets.idPassCon} />

              <View style={{ display: this.state.displayText }}>
                <View style={widgets.idPassCon}>
                  <Image source={require('../images/birthday.png')}
                    style={{
                      width: 25,
                      height: 25,
                      resizeMode: 'contain',
                      marginRight: 30
                    }} />
                  <Text style={{ display: this.state.display }}> </Text>
                  <TextInput
                    placeholder={I18n.t('BIRTHDAY')+'*'}
                    editable={false}
                    style={[widgets.loginTextInput, widgets.commonFont, text.titleColorGray]}
                    value={this.state.birthDate} />
                </View>
              </View>

              <View style={{ display: this.state.display }}>
                <View style={widgets.idPassCon}>
                  <Image source={require('../images/birthday.png')}
                    style={{
                      width: 25,
                      height: 25,
                      resizeMode: 'contain',
                      marginRight: 31
                    }} />
                  <Text style={{ display: this.state.display }}> </Text>
                  <DatePicker
                    style={[widgets.birthdateTextInput, widgets.borderNone, this.state.textcolor]}
                    date={this.state.birthDate}
                    showIcon={false}
                    mode="date"
                    maxDate={new Date()}
                    androidMode="spinner"
                    placeholder={I18n.t('BIRTHDAY')+'*'}
                    customStyles={{
                      placeholderText: {
                        color: '#595959',
                        textAlign: 'left',
                      },
                      dateInput: {
                        color: '#3bafda',
                        borderColor: 'transparent',
                        borderBottomColor: 'black',
                        justifyContent: 'flex-start',
                        flexDirection: 'row',
                        marginRight: 2,
                        ...Platform.select({
                          ios: {
                            marginLeft: -5,
                            borderBottomWidth: 1,
                            borderBottomColor: 'gray',
                          }
                        })
                      },
                      dateText: {
                        marginBottom: -10,
                        color: this.state.dropdowncolor,
                        justifyContent: 'flex-start'
                      }
                    }}
                    format="DD/MM/YYYY"
                    cancelBtnText={I18n.t('CANCEL')}
                    confirmBtnText={I18n.t('SAVE')}
                    onDateChange={value => {
                      this.setState({ birthDate: value });
                    }} />
                </View>
              </View>

              <View style={widgets.idPassCon} />


              <View style={widgets.idPassCon}>
                <Image source={require('../images/gender.png')}
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    marginRight: 44
                  }} />
                <Text style={{ display: this.state.display }}> </Text>
                <TouchableOpacity style={{ flex: 1, marginRight: 3 }} onPress={() => { { this.Picker2() } }}>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderBottomColor: this.state.picker_underline_color,
                      ...Platform.select({
                        ios: {
                          borderBottomWidth: 1,
                          borderBottomColor: 'gray',
                          marginLeft: -5
                        }
                      })
                    }}>
                    <Text
                      style={{
                        backgroundColor: 'transparent',
                        textAlign: 'left',
                        color: this.state.dropdowncolor,
                        fontSize: 15,
                        marginBottom: 5
                      }} >{this.state.gender}</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={widgets.idPassCon} />
              <View style={widgets.idPassCon}>
                <Image source={require('../images/occupation.png')}
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    marginRight: 30
                  }} />
                <Text style={{ display: this.state.display }}> </Text>
                <TextInput
                  maxLength={30}
                  editable={this.state.editable}
                  placeholder={I18n.t('OCCUPATION')}
                  style={[widgets.loginTextInput, this.state.textcolor]}
                  onChangeText={value => {
                    this.setState({ occupation: value });
                  }}
                  value={this.state.occupation} />
              </View>
              <View style={widgets.idPassCon} />
              <View style={widgets.idPassCon}>
                <Image source={require('../images/weight.png')}
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    marginRight: 3
                  }} />
                <Text>{I18n.t('KG')}</Text>
                <Text style={{ display: this.state.display }}> </Text>
                <TextInput
                  editable={this.state.editable}
                  placeholder={I18n.t('WEIGHT')+'*'}
                  keyboardType="numeric"
                  maxLength={3}
                  style={[widgets.loginTextInput, this.state.textcolor]}
                  onChangeText={value => {
                    this.setState({ weight: value });
                  }}
                  value={this.state.weight} />
              </View>
              <View style={widgets.idPassCon} />
              <View style={widgets.idPassCon}>
                <Image source={require('../images/height.png')}
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain'
                  }} />
                <Text>{I18n.t('CM')}</Text>
                <Text style={{ display: this.state.display }}> </Text>
                <TextInput
                  editable={this.state.editable}
                  placeholder={I18n.t('HEIGHT')}
                  keyboardType="numeric"
                  maxLength={3}
                  style={[widgets.loginTextInput, this.state.textcolor]}
                  onChangeText={value => {
                    this.setState({ height: value });
                  }}
                  value={this.state.height} />
              </View>
              <View style={widgets.idPassCon} />

                {RenderIf(this.state.display === 'flex')(
                     <TouchableOpacity onPress={() => this.onclickbtnUpdate()}>
                       <Text style={widgets.updateBtn}>{I18n.t('UPDATE')}</Text>
                     </TouchableOpacity>
                )}
              
            </View>
          </ScrollView>
          {RenderIf(Platform.OS === 'ios')(
            <View style={{ display: this.state.display }}>
              <View style={widgets.idDoctor} />
            </View>
          )}
        </View>
      </ScrollView >
    );
  }
}

const countries = require('../../countries.json');

const mapStateToProps = (state) => {
  const { onFirstNameChange, onLastNameChange, onEmailChange, onCountryChange, onBirthdateChange, onGenderChange,
    onOccupationChange, onWeightChange, onHeightChange } = state.userProfileModule;

  return {
    onFirstNameChange, onLastNameChange, onEmailChange, onCountryChange, onBirthdateChange, onGenderChange,
    onOccupationChange, onWeightChange, onHeightChange
  };
};


export default connect(mapStateToProps, { updateUPModule, resetUPModule })(UserProfile); 