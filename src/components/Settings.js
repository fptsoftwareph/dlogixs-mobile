import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Switch,
  Slider,
  ScrollView,
  Modal,
  Linking,
  Alert,
  Platform,
  TouchableWithoutFeedback,
  NativeModules,
  NativeEventEmitter
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from '../styles/widgets';
import { text } from '../styles/text';
import {
  updateSettingsModule,
  getLatestAppVersion,
  getDeviceVersion,
  setIsConnectedToAccessPoint,
  logoutUser,
  resetUPModule,
  manageBinaural,
  resetSettingsModule
} from '../redux/actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker';
import { convert24To12Hr, isConnectedToInternet, savePreference, showAlertWithActions, removePreferenceKeys } from '../utils/CommonMethods';
import RenderIf from './RenderIf';
import DeviceInfo from 'react-native-device-info';
import I18n from './../translate/i18n/i18n';
import { ScanBDevicesPrompt, ScanAccessPointsPrompt, ConnectAccessPointPrompt,
  UploadModeDialog } from './common';
import { MyModule, SMAModule, StationModule } from 'nb_native_modules';
import {
  IS_WIFI_STATION_ENABLED, DEVICE_NAME, BSTATION, WRISTBAND, WIFI_STATION,
  IS_RBS_ENABLED
} from '../utils/DataConstants';
import { COLOR_THUMB_TINT, COLOR_TINT_ON, COLOR_SLIDER_TRACK_TINT } from '../styles/colors';
import Toast from 'react-native-simple-toast';
import CouchbaseManager from '../utils/CouchbaseManager';
import Binaural from '../components/common/BinauralFunctions';
import SleepMgr from '../components/common/SleepInfoManager';
import { LoginManager } from 'react-native-fbsdk';
import { AlarmModule, ReachabilityModule } from 'nb_native_modules';

const uri = 'https://s3.ap-northeast-2.amazonaws.com/bucket-neurobeat/User-Guide.pdf';

const { width, height } = Dimensions.get('window');

const StationManagerModule = NativeModules.StationModule;
const StationManagerEmitter = new NativeEventEmitter(StationManagerModule);

const NetStatusManager = NativeModules.ReachabilityModule;

let iscBStationNameMatchDefault = false, iscWristbandNameMatchDefault = false,
  iscAccessPointNameMatchDefault = false;

class Settings extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
      isToScanAccessPoints: false,
      isToConnectAccessPointPrompt: false,
      isToOpenSettingsPrompt: false,
      sourceOfScan: '',
      isToScanBDevices: false,
      sourceOfDisconnect: ''
    };

    this.scanBDevicesPrompt = this.scanBDevicesPrompt.bind(this);
    this.scanAccessPtsPrompt = this.scanAccessPtsPrompt.bind(this);
    this.updateSettingsItemModule = this.updateSettingsItemModule.bind(this);
    this.setSettingsModuleSwitchMode = this.setSettingsModuleSwitchMode.bind(this);
    this.isToDisablePeripheral = this.isToDisablePeripheral.bind(this);
    this.isSliderThumbTintEnabled = this.isSliderThumbTintEnabled.bind(this);
    this.isSliderTrackTintEnabled = this.isSliderTrackTintEnabled.bind(this);
    this.isSliderDisabled = this.isSliderDisabled.bind(this);
    this.isSwitchThumbTintEnabled = this.isSwitchThumbTintEnabled.bind(this);
    this.isConnectedPeripheralMatch = this.isConnectedPeripheralMatch.bind(this);
    this.isRbsSwitchThumbTintEnabled = this.isRbsSwitchThumbTintEnabled.bind(this);
    this.isRbsSwitchDisabled = this.isRbsSwitchDisabled.bind(this);
    this.openConnectAccessPointPrompt = this.openConnectAccessPointPrompt.bind(this);
    this.openSettings = this.openSettings.bind(this);
    this.openSettingsPrompt = this.openSettingsPrompt.bind(this);
    this.closeSettingsPrompt = this.closeSettingsPrompt.bind(this);
    this.setBluetoothModulesToDefault = this.setBluetoothModulesToDefault.bind(this);
    this.disconnectDeviceAction = this.disconnectDeviceAction.bind(this);
    this.clearBinauralTimer = this.clearBinauralTimer.bind(this);
    this.sleepMgr = SleepMgr.sharedInstance();
  }

  scanBDevicesPrompt(visibility) {
    this.setState({ isToScanBDevices: visibility });
    console.log(`scanBDevicesPrompt: ${this.state.isToScanBDevices}`);
  }

  scanAccessPtsPrompt(visibility) {
    this.setState({ isToScanAccessPoints: visibility });
    console.log(`isToScanAccessPoints: ${this.state.isToScanAccessPoints}`);
  }

  openConnectAccessPointPrompt(visibility) {
    this.setState({ isToConnectAccessPointPrompt: visibility });
    console.log(`isToConnectAccessPointPrompt: ${this.state.isToConnectAccessPointPrompt}`);
  }

  updateSettingsItemModule(property, value) {
    this.props.updateSettingsModule({ prop: property, value: value });
  }

  setSettingsModuleSwitchMode(type, value) {
    switch (type) {
      case BSTATION:
        this.props.updateSettingsModule({ prop: 'isBstationHeadSetEnabled', value });
        break;
      case WRISTBAND:
        this.props.updateSettingsModule({ prop: 'isSmartBandEnabled', value });
        if (!value) {
          const arrMins = this.props.minsInADay;
          arrMins.push(this.props.deductedMins);
          this.props.updateSettingsModule({ prop: 'minsInADay', value: arrMins });
          this.props.updateSettingsModule({ prop: 'deductedMins', value: 0 });
        }
        break;
      case WIFI_STATION:
        this.props.updateSettingsModule({ prop: IS_WIFI_STATION_ENABLED, value });
        break;
      default:
        console.log('Connection type not found');
        break;
    }
  }

  componentWillMount() {
    console.log(`Dimensions width:${width} height:${height}`);

    this.props.updateSettingsModule({ prop: 'deviceVersion', value: DeviceInfo.getVersion() });

    if (isConnectedToInternet) {
      this.props.getLatestAppVersion();
    }
    this.binaural = Binaural.getInstance(this.props.targetBinauralHours);

    if (Platform.OS === 'android') {
      this.handleStationConnectionLost = StationManagerEmitter.addListener('StationConnectionLostListener', status => {
        console.log("StationConnectionLostListener: " + status);
      });
    }
  }

  componentWillUnmount() {
    console.log('Settings componentWillUnmount');
    if (Platform.OS === 'android') {
      this.handleStationConnectionLost.remove();
    }
  }

  dismissOutside() {
    this.setModalVisibility(false);
  }

  isToDisablePeripheral(peripheralType) {
    return this.props.connectedPeripheral === '' || this.props.connectedPeripheral === peripheralType
      ? false
      : true;
  }

  isSettingsModuleMatchConnectedPeripheral(peripheralType) {
    return this.props.connectedPeripheral === '' || this.props.connectedPeripheral === peripheralType
      ? [widgets.commonPadding]
      : [widgets.commonPadding, widgets.disabledCard];
  }

  isConnectedPeripheralMatch(connectedPeripheral) {
    return connectedPeripheral === this.props.connectedPeripheral
      ? [widgets.commonPadding]
      : [widgets.commonPadding, widgets.disabledCard];
  }

  isAudioConnectedToWifi() {
    return this.isConnectedToAccessPoint()
      ? [widgets.commonPadding]
      : [widgets.commonPadding, widgets.disabledCard];
  }

  isSliderThumbTintEnabled() {
    return BSTATION === this.props.connectedPeripheral ? COLOR_THUMB_TINT : 'gray';
  }

  isSliderTrackTintEnabled() {
    return BSTATION === this.props.connectedPeripheral ? COLOR_SLIDER_TRACK_TINT : 'gray';
  }

  checkIfNotAndroid5AndIfiOS() {
    if (Platform.OS === 'android') {
      if (Platform.Version !== 21) {
        return true;
      }

      return false;
    }

    return true;
  }

  isSliderDisabled() {
    return BSTATION !== this.props.connectedPeripheral ? true : false;
  }

  isAudioSliderThumbTintEnabled() {
    return this.isConnectedToAccessPoint() ? COLOR_THUMB_TINT : 'gray';
  }

  isAudioSliderTrackTintEnabled() {
    return this.isConnectedToAccessPoint() ? COLOR_SLIDER_TRACK_TINT : 'gray';
  }

  isAudioSliderDisabled() {
    return this.isConnectedToAccessPoint() ? false : true;
  }

  isConnectedToAccessPoint() {
    return this.props.isWstationHeadSetEnabled && this.props.connectedAccessPoint !== I18n.t('NETWORK_NAME')
    && this.props.isBstationHeadSetEnabled && this.props.connectedBStation !== I18n.t('DEVICE_NAME');
  }

  isSwitchThumbTintEnabled(peripheralType) {
    return this.props.connectedPeripheral === '' || this.props.connectedPeripheral === peripheralType
      ? COLOR_THUMB_TINT
      : 'gray';
  }

  isRbsSwitchThumbTintEnabled() {
    return BSTATION === this.props.connectedPeripheral ? COLOR_THUMB_TINT : 'gray';
  }

  isRbsSwitchDisabled() {
    return BSTATION === this.props.connectedPeripheral ? false : true;
  }

  setBinauralOff(value) {
    this.props.manageBinaural({ prop: 'binauralStart', value: 0 });
    StationModule.setBB(value, true, response => {
      if (response === true) {
        this.updateSettingsItemModule('binauralBeat', value);
        this.updateSettingsItemModule('sMBbState', false);
        this.binaural.clearBinauralTimer();
      } else {
        Toast.show(response, Toast.SHORT);
      }
    });
  }

  clearBinauralTimer() {
    this.props.manageBinaural({ prop: 'binauralStart', value: 0 });
    this.binaural.clearBinauralTimer();
  }

  onConnectToStationAndScanWifi() {
    if (this.props.isWstationHeadSetEnabled && this.props.connectedAccessPoint !== I18n.t('NETWORK_NAME') &&
    !this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else if (this.props.isBstationHeadSetEnabled) {
      if (Platform.OS === 'android') {
        this.setState({ isToScanAccessPoints: true });
      } else {
        this.openConnectAccessPointPrompt(true);
      }
    } else {
      //  Show connect to bluetooth station
      this.setState({
        isToScanBDevices: true, sourceOfScan: WIFI_STATION,
        connectedDevice: this.props.connectedAccessPoint
      });
    }
  }

  showToastIfBStationIsNotConnected() {
    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    }
  }

  changeBatStatusToInactive(batStatusValue) {
    if (batStatusValue === 0) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_empty');
    } else if (batStatusValue === 1) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_1bar');
    } else if (batStatusValue === 2) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_2bars');
    } else if (batStatusValue === 3) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_3bars');
    } else if (batStatusValue === 4) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_4bars');
    } else if (batStatusValue === 5) {
      this.updateSettingsItemModule('batStatus', 'bat_full_inactive');
    }
  }

  disconnectDeviceAction() {
    this.disconnectDevice(BSTATION);
  }

  checkIfPeripheralNamesMatchDefault() {
    iscBStationNameMatchDefault = this.props.connectedBStation === I18n.t('DEVICE_NAME') ? true : false;
    iscWristbandNameMatchDefault = this.props.connectedWristband === I18n.t('DEVICE_NAME') ? true : false;
    iscAccessPointNameMatchDefault = this.props.connectedAccessPoint === I18n.t('NETWORK_NAME') ? true : false;
  }

  translateDefaultPeripherals() {
    this.updateSettingsItemModule('connectedBStation', iscBStationNameMatchDefault ? I18n.t('DEVICE_NAME') : this.props.connectedBStation);
    this.updateSettingsItemModule('connectedWristband', iscWristbandNameMatchDefault ? I18n.t('DEVICE_NAME') : this.props.connectedWristband);
    this.updateSettingsItemModule('connectedAccessPoint', iscAccessPointNameMatchDefault ? I18n.t('NETWORK_NAME') : this.props.connectedAccessPoint);
    this.updateSettingsItemModule('connectedDevice', I18n.t('DEVICE_NAME'));
  }

  render() {
    return (
      <ScrollView>
        <View style={widgets.settingsCon}>
          <TouchableOpacity
            disabled={this.isToDisablePeripheral(BSTATION)}
            onPress={() => {
              if (this.props.isBstationHeadSetEnabled) {
                this.setState({ sourceOfDisconnect: BSTATION });
                showAlertWithActions('', I18n.t('DISCONNECT_STATION'), this.disconnectDeviceAction);
              } else {
                this.setState({
                  isToScanBDevices: true, sourceOfScan: BSTATION,
                  connectedDevice: this.props.connectedBStation,
                  isToScanAccessPoints: false
                });
              }
            }}
          >
            <Card containerStyle={this.isSettingsModuleMatchConnectedPeripheral(BSTATION)}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image
                    style={widgets.settingsModuleIcon}
                    source={require('../images/shortcut_icons/bluetooth_off.png')}
                  />
                </View>
                <View style={widgets.settingsModuleTitleCon}>
                  <Text style={text.title}>{I18n.t('BLUETOOTH')}</Text>
                  <Text style={text.subTitle}>{this.props.connectedBStation}</Text>
                </View>
                <Text style={[text.subTitle, { flex: 1 }]}>{I18n.t('STATION')}</Text>
                <View style={widgets.alignRightView}>
                  <Switch
                    style={widgets.switch}
                    thumbTintColor={this.isSwitchThumbTintEnabled(BSTATION)}
                    onTintColor={COLOR_TINT_ON}
                    disabled={this.isToDisablePeripheral(BSTATION)}
                    onValueChange={value => {
                      this.props.updateSettingsModule({ prop: 'isBstationHeadSetEnabled', value });

                      if (!value) {
                        this.setState({ sourceOfDisconnect: BSTATION });
                        this.disconnectDevice(BSTATION);
                      } else {
                        //  open scan dialog
                        this.setState({
                          isToScanBDevices: true, sourceOfScan: BSTATION,
                          connectedDevice: this.props.connectedBStation,
                          isToScanAccessPoints: false
                        });
                      }
                    }}
                    value={this.props.isBstationHeadSetEnabled}
                  />
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={this.isToDisablePeripheral(WRISTBAND)}
            onPress={() => {
              this.setState({
                isToScanBDevices: true, sourceOfScan: WRISTBAND,
                connectedDevice: this.props.connectedWristband
              });
            }}
          >
            <Card containerStyle={this.isSettingsModuleMatchConnectedPeripheral(WRISTBAND)}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image style={widgets.settingsModuleIcon} source={require('../images/wristband32darker.png')} />
                </View>
                <View style={widgets.settingsModuleTitleCon}>
                  <Text style={text.title}>{I18n.t('PAIR_WRISTBAND')}</Text>
                  <Text style={text.subTitle}>{this.props.connectedWristband}</Text>
                </View>
                <View style={widgets.alignRightView}>
                  <Switch
                    style={widgets.switch}
                    thumbTintColor={this.isSwitchThumbTintEnabled(WRISTBAND)}
                    onTintColor={COLOR_TINT_ON}
                    disabled={this.isToDisablePeripheral(WRISTBAND)}
                    onValueChange={value => {
                      this.props.updateSettingsModule({ prop: 'isSmartBandEnabled', value });
                      console.log(`updateSettingsModule isSmartBandEnabled: ${value}`);

                      if (!value) {
                        this.setState({ sourceOfDisconnect: WRISTBAND });
                        this.disconnectDevice(WRISTBAND);
                      } else {
                        //  open scan dialog
                        this.setState({
                          isToScanBDevices: true, sourceOfScan: WRISTBAND,
                          connectedDevice: this.props.connectedWristband
                        });
                      }
                    }}
                    value={this.props.isSmartBandEnabled}
                  />
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={this.isToDisablePeripheral(BSTATION)}
            onPress={() => {
              this.onConnectToStationAndScanWifi();
            }}
          >
            <Card containerStyle={this.isSettingsModuleMatchConnectedPeripheral(BSTATION)}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image style={widgets.settingsModuleIcon} source={require('../images/shortcut_icons/wifi_off.png')} />
                </View>
                <View style={[widgets.commonConColumn, widgets.commonFlex]}>
                  <View style={[widgets.settingsModuleTitleCon, widgets.commonConRow, { marginRight: 0 }]}>
                    <Text style={text.title}>{I18n.t('CONNECT_WIFI')}</Text>
                  </View>
                  <Text style={[text.subTitle, widgets.commonMarginLeft2]}>{this.props.connectedAccessPoint}</Text>
                </View>
                <View style={widgets.alignRightView}>
                  <Switch
                    style={widgets.switch}
                    thumbTintColor={this.isSwitchThumbTintEnabled(BSTATION)}
                    onTintColor={COLOR_TINT_ON}
                    disabled={this.isToDisablePeripheral(BSTATION)}
                    onValueChange={value => {
                      console.log(`updateSettingsModule isWstationHeadSetEnabled: ${value}`);

                      if (!value) {
                        if (!this.props.isBstationHeadSetEnabled) {
                          Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
                        } else {
                          this.props.updateSettingsModule({ prop: IS_WIFI_STATION_ENABLED, value });
                          
                          this.setState({ sourceOfDisconnect: WIFI_STATION });
                          this.disconnectDevice(WIFI_STATION);
                        }
                      } else {
                        this.props.updateSettingsModule({ prop: IS_WIFI_STATION_ENABLED, value });

                        this.onConnectToStationAndScanWifi();
                      }
                    }}
                    value={this.props.isWstationHeadSetEnabled}
                  />
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            this.showToastIfBStationIsNotConnected();
          }}>
            <Card containerStyle={this.isConnectedPeripheralMatch(BSTATION)}>
              <View style={widgets.commonConRow}>
                <View style={[widgets.commonFlex, widgets.commonConRow]}>
                  <View style={widgets.centerView}>
                    <Image
                      style={widgets.settingsModuleIcon}
                      source={require('../images/shortcut_icons/binaural_off.png')}
                    />
                  </View>
                  <View style={widgets.settingsModuleTitleCon}>
                    <Text style={text.title}>{I18n.t('BINAURAL_BEAT')}</Text>
                  </View>
                </View>
                <View style={widgets.alignRightView}>
                  <Switch
                    style={widgets.switch}
                    thumbTintColor={this.isRbsSwitchThumbTintEnabled()}
                    onTintColor={this.isRbsSwitchDisabled() ? 'gray' : COLOR_TINT_ON}
                    disabled={this.isRbsSwitchDisabled()}
                    onValueChange={value => {
                      console.log(`updateSettingsModule isRbsEnabled: ${value}`);
                      this.updateSettingsItemModule(IS_RBS_ENABLED, value);
                      let newVal;

                      if (!value) {
                        newVal = 0;
                        console.log("yo save newVal:" + newVal);
                        this.setBinauralOff(newVal);
                      } else {
                        newVal = 1;
                        StationModule.setBB(newVal, true, response => {
                          //console.log(`setBB: ${response}`);
                          if (response === true) {
                            this.updateSettingsItemModule('binauralBeat', newVal);
                            this.updateSettingsItemModule('sMBbState', true);
                            this.props.manageBinaural({ prop: 'binauralStart', value: new Date() });
                            this.binaural.setBinauralTimer(new Date());
                          } else {
                            Toast.show(response, Toast.SHORT);
                          }
                        });
                      }
                    }}
                    value={this.props.isRbsEnabled}
                  />
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            this.showToastIfBStationIsNotConnected();
          }}>
            <Card containerStyle={this.isConnectedPeripheralMatch(BSTATION)}>
              <View style={widgets.commonConRow}>
                <View style={[widgets.commonFlex, widgets.commonConRow]}>
                  <View style={widgets.centerView}>
                    <Image
                      style={widgets.settingsModuleIcon}
                      source={require('../images/shortcut_icons/speaker_off.png')}
                    />
                  </View>
                  <View style={widgets.settingsModuleTitleWithNoDescCon}>
                    <Text style={text.title}>{I18n.t('VOLUME')}</Text>
                  </View>
                </View>
                <View style={widgets.commonCon}>
                  <Slider
                    thumbTintColor={this.isSliderThumbTintEnabled()}
                    maximumTrackTintColor={this.checkIfNotAndroid5AndIfiOS() ? this.isSliderTrackTintEnabled() : 'undefined'}
                    disabled={this.isSliderDisabled()}
                    step={1}
                    minimumValue={0}
                    maximumValue={20}
                    value={this.props.volume}
                    onSlidingComplete={val => {
                      StationModule.setVolume(val, response => {
                        console.log(`parent setVolume: ${response}`);
                        if (response === true) {
                          this.updateSettingsItemModule('volume', val);
                          this.updateSettingsItemModule('sMVolState', val === 0 ? false : true);
                        } else {
                          Toast.show(response, Toast.SHORT);
                        }
                      });
                    }}
                  />
                  <View style={[widgets.commonConRow, widgets.commonMarginLeftRight]}>
                    <Text>0</Text>
                    <View style={widgets.commonFlex}>
                      <Text style={widgets.textAlignRight}>{this.props.volume}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            this.showToastIfBStationIsNotConnected();
          }}>
            <Card containerStyle={this.isConnectedPeripheralMatch(BSTATION)}>
              <View style={widgets.commonConRow}>
                <View style={[widgets.commonFlex, widgets.commonConRow]}>
                  <View style={widgets.centerView}>
                    <Image
                      style={widgets.settingsModuleIcon}
                      source={require('../images/shortcut_icons/lights_off.png')}
                    />
                  </View>
                  <View style={widgets.settingsModuleTitleWithNoDescCon}>
                    <Text style={text.title}>{I18n.t('STATION_LIGHT')}</Text>
                  </View>
                </View>
                <View style={widgets.commonCon}>
                  <Slider
                    thumbTintColor={this.isSliderThumbTintEnabled()}
                    maximumTrackTintColor={this.checkIfNotAndroid5AndIfiOS() ? this.isSliderTrackTintEnabled() : 'undefined'}
                    disabled={this.isSliderDisabled()}
                    step={1}
                    minimumValue={0}
                    maximumValue={10}
                    value={this.props.stationLight}
                    onSlidingComplete={val => {
                      StationModule.setLight(val, response => {
                        console.log(`setLight: ${response}`);
                        if (response === true) {
                          this.updateSettingsItemModule('stationLight', val);
                          this.updateSettingsItemModule('sMLightState', val === 0 ? false : true);
                        } else {
                          Toast.show(response, Toast.SHORT);
                        }
                      });
                    }}
                  />
                  <View style={[widgets.commonConRow, widgets.commonMarginLeftRight]}>
                    <Text>0</Text>
                    <View style={widgets.commonFlex}>
                      <Text style={widgets.textAlignRight}>{this.props.stationLight}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            if (!this.props.isBstationHeadSetEnabled) {
              Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
            } else if (!this.isConnectedToAccessPoint()) {
              Toast.show(I18n.t('CONNECT_WIFI_TO_ENABLE_ITEM'), Toast.SHORT);
            }
          }}>
            <Card containerStyle={this.isAudioConnectedToWifi()}>
              <View style={[widgets.commonConRow]}>
                <View style={[widgets.commonFlex, widgets.commonConRow]}>
                  <View style={widgets.centerView}>
                    <Image
                      style={widgets.settingsModuleIcon}
                      source={require('../images/shortcut_icons/music_off.png')}
                    />
                  </View>
                  <View style={widgets.settingsModuleTitleCon}>
                    <Text style={text.title}>{I18n.t('AUDIO_SELECTOR')}</Text>
                  </View>
                </View>
                <View style={widgets.commonCon}>
                  <Slider
                    thumbTintColor={this.isAudioSliderThumbTintEnabled()}
                    maximumTrackTintColor={this.checkIfNotAndroid5AndIfiOS() ? this.isAudioSliderTrackTintEnabled() : 'undefined'}
                    disabled={this.isAudioSliderDisabled()}
                    step={1}
                    minimumValue={0}
                    maximumValue={1}
                    value={this.props.audioSelected}
                    onSlidingComplete={val => {
                      StationModule.setAudioInput(val, response => {
                        console.log(`setAudioInput: ${response}`);
                        if (response === true) {
                          this.updateSettingsItemModule('audioSelected', val);
                        } else {
                          Toast.show(response, Toast.SHORT);
                        }
                      });
                    }}
                  />
                  <View style={widgets.audioSelectorCon}>
                    <Image
                      style={widgets.audioSelectorOptionIcon}
                      source={require('../images/shortcut_icons/bluetooth_off.png')}
                    />
                    <Image
                      style={widgets.audioSelectorOptionIcon}
                      source={require('../images/shortcut_icons/wifi_off.png')}
                    />
                  </View>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.getAppLatestVersion.bind(this)}
            disabled={parseFloat(this.props.appLatestVersion) <= parseFloat(this.props.deviceVersion)}
          >
            <Card containerStyle={widgets.commonPadding}>
              <View style={widgets.commonConRow}>
                <View style={[widgets.commonFlex, widgets.commonConRow]}>
                  <View style={widgets.centerView}>
                    <Image style={widgets.settingsModuleIcon} source={require('../images/flag.png')} />
                  </View>
                  <View
                    style={
                      parseFloat(this.props.appLatestVersion) > parseFloat(this.props.deviceVersion)
                        ? [widgets.settingsModuleTitleCon, widgets.commonMarginBottom]
                        : [widgets.settingsModuleTitleCon, widgets.commonPaddingTopAndBottom]
                    }
                  >
                    {RenderIf(parseFloat(this.props.appLatestVersion) > parseFloat(this.props.deviceVersion))(
                      <View style={widgets.alignRightViewIcon}>
                        <Image
                          style={widgets.audioSelectorOptionIcon}
                          source={require('../images/exclamation-button.png')}
                        />
                      </View>
                    )}
                    <Text style={text.title}>{I18n.t('VERSION')}</Text>
                  </View>
                </View>
                <View style={[widgets.alignRightView, widgets.centerView]}>
                  <Text>{this.props.deviceVersion}</Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={Actions.userProfile}>
            <Card containerStyle={widgets.commonPadding}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image style={widgets.settingsModuleIcon} source={require('../images/user.png')} />
                </View>
                <View style={widgets.settingsModuleTitleCon}>
                  <Text style={text.title}>{I18n.t('PROFILE')}</Text>
                  <Text style={text.subTitle}>{this.props.onEmailChange}</Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.setModalVisibility(true);
            }}
          >
            <Card containerStyle={widgets.settingModuleWithNoDescPad}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image style={widgets.settingsModuleIcon} source={require('../images/language.png')} />
                </View>
                <View style={widgets.settingsModuleTitleCon}>
                  <Text style={text.title}>{I18n.t('LANGUAGE')}</Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Alert.alert(
                I18n.t('CONFIRM'),
                I18n.t('USER_GUIDE_CONFIRMATION'),
                [
                  { text: I18n.t('CANCEL'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  {
                    text: I18n.t('OK'),
                    onPress: () => {
                      Linking.openURL(uri).catch(err =>
                        Alert.alert(
                          '',
                          I18n.t('SOMETHING_WENT_WRONG'),
                          [{ text: I18n.t('OK'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' }],
                          { cancelable: false }
                        )
                      );
                    }
                  }
                ],
                { cancelable: false }
              );
            }}
          >
            <Card containerStyle={widgets.settingModuleWithNoDescPad}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image style={widgets.settingsModuleIcon} source={require('../images/open-book.png')} />
                </View>
                <View style={widgets.settingsModuleTitleCon}>
                  <Text style={text.title}>{I18n.t('USER_GUIDE')}</Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this.openSettingsPrompt(true); }}
          >
            <Card containerStyle={widgets.settingModuleWithNoDescPad}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image style={widgets.settingsModuleIcon} source={require('../images/upload-mode.png')} />
                </View>
                <View style={widgets.settingsModuleTitleCon}>
                  <Text style={text.title}>{I18n.t('UPLOAD_MODE')}</Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.logout.bind(this)}>
            <Card containerStyle={widgets.settingModuleWithNoDescPad}>
              <View style={widgets.commonConRow}>
                <View style={widgets.centerView}>
                  <Image style={widgets.settingsModuleIcon} source={require('../images/logout.png')} />
                </View>
                <View style={widgets.settingsModuleTitleCon}>
                  <Text style={text.title}>{I18n.t('LOGOUT')}</Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
        </View>

        <View>
          <Modal
            animationType="slide"
            transparent={true}
            style={widgets.modal}
            visible={this.state.modalVisible}
            presentation
            onRequestClose={() => {
              this.setState({ modalVisible: false });
            }}
          >
            <TouchableWithoutFeedback
              onPressIn={() => {
                this.dismissOutside();
              }}
            >
              <View style={[widgets.bluetoothScanPromptCon]}>
                <TouchableWithoutFeedback onPressIn={() => { }}>
                  <View style={widgets.modalSelectLang}>
                    <View style={widgets.modalCloseCon} />
                    <ScrollView style={widgets.modalScrollView}>
                      <View>
                        <View style={widgets.policyHeaderCon}>
                          <Text style={widgets.LanguageHeader}>{I18n.t('LANGUAGE')}</Text>
                        </View>
                        <View style={widgets.policyTextCon}>
                          <View style={{ flex: 0.2 }}>
                            <View style={widgets.LanguageContainer}>
                              <TouchableOpacity
                                onPress={() => {
                                  this.checkIfPeripheralNamesMatchDefault();
                                  I18n.locale = 'en';
                                  savePreference('language', 'english');
                                  this.translateDefaultPeripherals();

                                  Alert.alert(
                                    '',
                                    I18n.t('ENGLISH') + ' ' + I18n.t('SELECTED'),
                                    [{ text: I18n.t('OK'), onPress: () => { this.setModalVisibility(false); Actions.homePage() }, style: 'cancel' }],
                                    { cancelable: false }
                                  );
                                }}
                              >
                                <View>
                                  <Image
                                    source={require('../images/united-states.png')}
                                    style={widgets.languagesIcons}
                                  />
                                  <Text style={widgets.textLanguagePosition}>{I18n.t('ENGLISH')}</Text>
                                </View>
                              </TouchableOpacity>

                              <TouchableOpacity
                                onPress={() => {
                                  this.checkIfPeripheralNamesMatchDefault();
                                  I18n.locale = 'kor';
                                  savePreference('language', 'korean');
                                  this.translateDefaultPeripherals();

                                  Alert.alert(
                                    '',
                                    I18n.t('KOREAN') + ' ' + I18n.t('SELECTED'),
                                    [{ text: I18n.t('OK'), onPress: () => { this.setModalVisibility(false); Actions.homePage() }, style: 'cancel' }],
                                    { cancelable: false }
                                  );
                                }}
                              >
                                <View>
                                  <Image source={require('../images/south-korea.png')} style={widgets.languagesIcons} />
                                  <Text style={widgets.textLanguagePosition}>{I18n.t('KOREAN')}</Text>
                                </View>
                              </TouchableOpacity>

                              <TouchableOpacity
                                onPress={() => {
                                  this.checkIfPeripheralNamesMatchDefault();
                                  I18n.locale = 'chi';
                                  savePreference('language', 'chinese');
                                  this.translateDefaultPeripherals();

                                  Alert.alert(
                                    '',
                                    I18n.t('CHINESE') + ' ' + I18n.t('SELECTED'),
                                    [{ text: I18n.t('OK'), onPress: () => { this.setModalVisibility(false); Actions.homePage() }, style: 'cancel' }],
                                    { cancelable: false }
                                  );
                                }}
                              >
                                <View>
                                  <Image source={require('../images/china.png')} style={widgets.languagesIcons} />
                                  <Text style={widgets.textLanguagePosition}>{I18n.t('CHINESE')}</Text>
                                </View>
                              </TouchableOpacity>

                              <TouchableOpacity
                                onPress={() => {
                                  this.checkIfPeripheralNamesMatchDefault();
                                  I18n.locale = 'jap';
                                  savePreference('language', 'japanese');
                                  this.translateDefaultPeripherals();

                                  Alert.alert(
                                    '',
                                    I18n.t('JAPANESE') + ' ' + I18n.t('SELECTED'),
                                    [{ text: I18n.t('OK'), onPress: () => { this.setModalVisibility(false); Actions.homePage() }, style: 'cancel' }],
                                    { cancelable: false }
                                  );
                                }}
                              >
                                <View>
                                  <Image source={require('../images/japan.png')} style={widgets.languagesIcons} />
                                  <Text style={widgets.textLanguagePosition}>{I18n.t('JAPANESE')}</Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      </View>
                    </ScrollView>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>

        <ScanBDevicesPrompt
          type={this.state.sourceOfScan}
          visibility={this.state.isToScanBDevices}
          scanBDevicesPrompt={this.scanBDevicesPrompt}
          isToOpenBluetoothDevicesListing={false}
          updateSettingsItemModule={this.updateSettingsItemModule}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
          connectedDevice={this.props.connectedDevice}
          scanAccessPtsPrompt={this.scanAccessPtsPrompt}
          message={I18n.t('CONNECT_STATION')}
          rightBtnLabel={I18n.t('OK')}
          openConnectAccessPointPrompt={this.openConnectAccessPointPrompt}
          setBluetoothModulesToDefault={this.setBluetoothModulesToDefault}
          audioSelectorWifi={this.isWstationHeadSetEnabled}
          clearBinauralTimer={this.clearBinauralTimer}
        />

        <ScanAccessPointsPrompt
          visibility={this.state.isToScanAccessPoints}
          scanAccessPtsPrompt={this.scanAccessPtsPrompt}
          isToOpenAccessPointsListing={false}
          updateSettingsItemModule={this.updateSettingsItemModule}
          connectedAccessPoint={this.props.connectedAccessPoint}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
        />

        <ConnectAccessPointPrompt
          visibility={this.state.isToConnectAccessPointPrompt}
          openConnectAccessPointPrompt={this.openConnectAccessPointPrompt}
          updateSettingsItemModule={this.updateSettingsItemModule}
          connectedAccessPoint={this.props.connectedAccessPoint}
          ssid={''}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
        />

        <UploadModeDialog visibility={this.state.isToOpenSettingsPrompt}
          connectionStatus={this.props.connectionStatus}
          uploadMode={this.props.uploadMode}
          updateSettingsItemModule={this.updateSettingsItemModule}
          dismissDialog={this.openSettingsPrompt}/>
      </ScrollView>
    );
  }

  openSettingsPrompt(visibility) {
    this.setState({ isToOpenSettingsPrompt: visibility });
  }

  closeSettingsPrompt() {
    this.setState({ isToOpenSettingsPrompt: false });
  }

  setModalVisibility(isVisible) {
    this.setState({
      modalVisible: isVisible
    });
  }

  disconnectDevice(type) {
    const peripheralId = this.props.connectedPeripheralId;

    if (peripheralId !== '') {
      if (type === WRISTBAND) {
        if (Platform.OS === 'ios') {
          console.log('disconnectDevice(): iOS');
          SMAModule.checkState(btState => {
            console.log(`disconnectDevice(): BT STATE: ${btState}`);
            if (btState === 'on') {
              SMAModule.disconnect(success => {
                console.log('disconnected');
              });
              this.setBluetoothModulesToDefault(type);
            } else {
              Toast.show('Enable Bluetooth To Disconnect Device', Toast.SHORT);
              this.updateSettingsItemModule('isSmartBandEnabled', true);
            }
          });
        } else {
          SMAModule.disconnect(result => {
            console.log('Disconnected');
          });
          this.setBluetoothModulesToDefault(type);
        }
      } else if (type === BSTATION) {
        // for BSTATION ('bStation')
        if (this.props.isRbsEnabled) {
          this.clearRbsData();
        }

        if (Platform.OS === 'android') {
          StationModule.disconnectStation("settings", peripheralId);
          this.setBluetoothModulesToDefault(type);
        } else {
          StationModule.disconnect(response => {
            console.log(`StationModule.disconnect(): ${response}`);
          });
          this.setBluetoothModulesToDefault(type);
        }
      } else {
        StationModule.forgetWifi(result => {
          console.log('disconnectWifi(): ', result);
        });
        this.setBluetoothModulesToDefault(type);
      }
    } else {
      //  console.log('Peripheral id not found. Make sure youre connected to a device');
      this.setBluetoothModulesToDefault(type);
    }
  }

  setBluetoothModulesToDefault(type) {
    if (type !== WIFI_STATION) {
      this.updateSettingsItemModule('connectedPeripheral', '');
      this.updateSettingsItemModule('connectedPeripheralId', '');
    }
    this.updateSettingsItemModule('connectedDevice', I18n.t('DEVICE_NAME'));

    switch (type) {
      case BSTATION:
        this.updateSettingsItemModule('connectedBStation', I18n.t('DEVICE_NAME'));
        this.updateSettingsItemModule('isBstationHeadSetEnabled', false);
        this.changeBatStatusToInactive(this.props.batStatusInt);
        this.updateSettingsItemModule('sMVolState', false);
        this.updateSettingsItemModule('sMLightState', false);
        this.updateSettingsItemModule('sMBbState', false);
        this.updateSettingsItemModule('isRbsEnabled', false);
        this.updateSettingsItemModule('headsetStatus', 'headset');
        break;
      case WRISTBAND:
        {
          this.updateSettingsItemModule('connectedWristband', I18n.t('DEVICE_NAME'));
          this.updateSettingsItemModule('isSmartBandEnabled', false);
          this.updateSettingsItemModule('dateSmartBandConnected', '');
          this.updateSettingsItemModule('dateSmartBandDisonnected', new Date());

          const arrMins = this.props.minsInADay;
          arrMins.push(this.props.deductedMins);
          this.props.updateSettingsModule({ prop: 'minsInADay', value: arrMins });
          this.props.updateSettingsModule({ prop: 'deductedMins', value: 0 });
        }
        break;
      case WIFI_STATION:
        this.updateSettingsItemModule('connectedAccessPoint', I18n.t('NETWORK_NAME'));
        this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, false);
        break;
      default:
        console.log('Connection type not found');
        break;
    }
  }

  getAppLatestVersion() {
    console.log(`appLatestVersion: ${JSON.stringify(this.props.appLatestVersion)}`);
    if (parseFloat(this.props.appLatestVersion) > parseFloat(this.props.deviceVersion)) {
      let appUrl = '';
      if (Platform.OS === 'ios') {
        appUrl = 'http://dlogixs.com/';
      } else {
        appUrl = 'https://play.google.com/store/apps/details?id=com.dlogixs.neurobeat';
      }
      Linking.openURL(appUrl);
    }
  }

  clearAnalysisRecords() {
    this.props.updateSettingsModule({ prop: 'latestTotalSleep', value: '' });
    this.props.updateSettingsModule({ prop: 'latestSleepLatency', value: '' });
    this.props.updateSettingsModule({ prop: 'latestWakeOnset', value: '' });

    this.props.updateSettingsModule({ prop: 'latestSteps', value: '' });
    this.props.updateSettingsModule({ prop: 'latestCal', value: '' });
    this.props.updateSettingsModule({ prop: 'latestActivityTime', value: '' });
    this.props.updateSettingsModule({ prop: 'latestBPM', value: '' });
  }

  logout() {
    Alert.alert(
      '',
      `${I18n.t('LOGOUT')}?`,
      [
        { text: I18n.t('CANCEL'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: I18n.t('OK'),
          onPress: () => {


            if (this.props.loginType === 'facebook') {
              LoginManager.logOut();
            }

            //  removes && disconnects connected peripheral
            if (this.props.connectedPeripheral !== '') {
              /* this condition fully removes the connected access point along with the station
              because wifi module will not be removed if we only call disconnectDevice(BSTATION) */
              if (this.props.isWstationHeadSetEnabled) {
                this.disconnectDevice(WIFI_STATION);
                this.disconnectDevice(BSTATION);
              } else {
                this.disconnectDevice(this.props.connectedPeripheral);
              }
            }

            //  RBS Clear Data
            this.clearRbsData();
            
            CouchbaseManager.logout();
            this.props.logoutUser(1);
            this.props.resetUPModule();
            this.clearAnalysisRecords();
            AlarmModule.disableAlarms();
            this.props.resetSettingsModule();
            removePreferenceKeys(['sleepHasListened']);
            this.sleepMgr.removeSleepListener();
            NetStatusManager.stopNetworkNotifier();

            this.updateSettingsItemModule('connectedBStation', I18n.t('DEVICE_NAME'));
            this.updateSettingsItemModule('connectedWristband', I18n.t('DEVICE_NAME'));
            this.updateSettingsItemModule('connectedAccessPoint', I18n.t('NETWORK_NAME'));

            Actions.login();
          }
        }
      ],
      { cancelable: false }
    );
  }

  clearRbsData() {
    this.props.manageBinaural({ prop: 'binauralStart', value: 0 });
    this.updateSettingsItemModule('binauralBeat', 0);
    this.updateSettingsItemModule('sMBbState', false);
    this.updateSettingsItemModule('isRbsEnabled', false);
    this.binaural.clearBinauralTimer();
  }

  openSettings() {
    if (Platform.OS === 'ios') {
      SMAModule.openBluetoothSettings(result => {
      });
    } else {
      MyModule.openSettings();
    }
  }
}

const mapStateToProps = state => {
  const {
    targetBinauralHours,
    isBstationHeadSetEnabled,
    isSmartBandEnabled,
    isWstationHeadSetEnabled,
    analysisTimeFrom,
    analysisTimeTo,
    appLatestVersion,
    deviceVersion,
    connectedBStation,
    connectedWristband,
    connectedPeripheralId,
    isAudioSelectorEnabled,
    minsInADay,
    deductedMins,
    connectedPeripheral,
    connectedAccessPoint,
    volume,
    stationLight,
    audioSelected,
    binauralBeat,
    isRbsEnabled,
    connectedDevice,
    batStatusInt,
    connectionStatus,
    uploadMode
  } = state.settingsModule;

  const { onEmailChange } = state.userProfileModule;
  const {
    binauralFrequency,
    binauralLastFrequency,
    binauralPlay,
    binauralTotalHoursToday,
    binauralTotal,
    binauralStart,
    BBisPlaying
  } = state.binaural;

  const { loginType } = state.loginModule;

  console.log(`mapStateToProps appLatestVersion: ${appLatestVersion}`);
  console.log(`connectedPeripheral: ${connectedPeripheral}`);

  return {
    targetBinauralHours,
    isBstationHeadSetEnabled,
    isSmartBandEnabled,
    isWstationHeadSetEnabled,
    analysisTimeFrom,
    minsInADay,
    deductedMins,
    analysisTimeTo,
    appLatestVersion,
    deviceVersion,
    onEmailChange,
    connectedBStation,
    connectedWristband,
    connectedPeripheralId,
    isAudioSelectorEnabled,
    connectedPeripheral,
    connectedAccessPoint,
    volume,
    stationLight,
    audioSelected,
    binauralBeat,
    isRbsEnabled,
    binauralStart,
    connectedDevice,
    batStatusInt,
    loginType,
    connectionStatus,
    uploadMode
  };
};

export default connect(mapStateToProps, {
  updateSettingsModule,
  getLatestAppVersion,
  getDeviceVersion,
  setIsConnectedToAccessPoint,
  logoutUser,
  resetUPModule,
  resetSettingsModule,
  manageBinaural
})(Settings);
