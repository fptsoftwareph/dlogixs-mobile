import React, { Component } from 'react';
import {
  Alert, View, Text, ScrollView, TouchableOpacity,
  ImageBackground, Image, Dimensions, DeviceEventEmitter, RefreshControl, Platform, NativeEventEmitter, NativeModules,
  AppState
} from 'react-native';
import HeaderMain from './common/HeaderMain';
import MainItems from './common/MainItems';
import { Actions } from 'react-native-router-flux';
import ProgressCircle from 'react-native-progress-circle';
import * as DateUtil from '../utils/DateUtils';
import { widgets } from '../styles/widgets';
import Qration from './Qration';
import I18n from './../translate/i18n/i18n';
import { updateSettingsModule, updateUPModule } from '../redux/actions';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';
import Couchbase from 'react-native-couchbase-lite';
import CouchbaseManager from '../utils/CouchbaseManager';
import { getPreference, savePreference, showToast, showAlert } from '../utils/CommonMethods';
import Session from '../utils/Session';
import getCurrentDate from '../utils/DateUtils';
import { ReachabilityModule } from 'nb_native_modules';

const NetStatusManager = NativeModules.ReachabilityModule;
const ReachabilityManagerEmitter = new NativeEventEmitter(NetStatusManager);

let mLast2MosCurationData = [];
let isDialogiOSDisplayed = false;
const { height } = Dimensions.get('window');

var radius;
var sleepAreaSize;
var circleTopMargin;
var sleepMarginHorizontal;
var sleepMarginBottom;
var sleepMarginTop;
var circleBorderWidth;
let queryActivityCtr = 0;
let querySleepCtr = 0;
var dbCtr = 0;
var curationLast10DaysCtr = 0;
var curationTwentyFourHoursCtr = 0;

// 0 == Wifi mode - once LTE is connected, we won't allow syncing
// 1 == wifi + lte mode, either LTE and wifi will allow auto-syncing


let currDateTime = new Date(getCurrentDate()).getTime();

switch (height) {
  case 736: { //iphone 6-8+
    sleepAreaSize = 190;
    radius = 85;
    circleTopMargin = 42;
    sleepMarginHorizontal = 12;
    sleepMarginBottom = 12;
    sleepMarginTop = 8;
    circleBorderWidth = 3;
    break;
  }
  case 667: {//iphone6-8
    sleepAreaSize = 150;
    radius = 65;
    circleTopMargin = 38;
    sleepMarginHorizontal = 8;
    sleepMarginBottom = 8;
    sleepMarginTop = 4;
    circleBorderWidth = 2;
    break;
  }
  case 640: { //common android
    sleepAreaSize = 116;
    radius = 52;
    circleTopMargin = 34;
    sleepMarginHorizontal = 8;
    sleepMarginBottom = 8;
    sleepMarginTop = 2;
    circleBorderWidth = 2;
    break;
  }
  default: { //iphone5/SE else heigher
    if (height < 640) {
      radius = 50;
      sleepAreaSize = height < 640 ? 112 : 200;
      circleTopMargin = 34;
      sleepMarginHorizontal = 8;
      sleepMarginBottom = 8;
      sleepMarginTop = 2;
      circleBorderWidth = 2;
    } else {
      sleepAreaSize = 190;
      radius = 85;
      circleTopMargin = 42;
      sleepMarginHorizontal = 12;
      sleepMarginBottom = 12;
      sleepMarginTop = 8;
      circleBorderWidth = 3;
    }
    break;
  }
}


const mapStateToProps = (state) => {
  const {
    latestSteps, latestCal, latestActivityTime, latestBPM, latestTotalSleep, latestSleepLatency, latestWakeOnset,
    uploadMode, wasUploaded, connectionStatus } = state.settingsModule;
  const { onCountryChange, onBirthdateChange, onGenderChange, onOccupationChange, onWeightChange, onHeightChange } = state.userProfileModule;

  return {
    latestSteps, latestCal, latestActivityTime, latestBPM, latestTotalSleep, latestSleepLatency, latestWakeOnset,
    onCountryChange, onBirthdateChange, onGenderChange, onOccupationChange, onWeightChange, onHeightChange,
    uploadMode, wasUploaded, connectionStatus
  };
};

let { extractMinsFromTimeStr, extractHoursFromTimeStr, computeTotalMins, formatTime, getMonthNames, getWeekNames } = DateUtil;


class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      backPressProp: 0,
      sleepPoints: 0,
      modalVisible: false,
      last10DaysCurationData: [],
      targetCurationData: [],
      isRefreshing: false
    };

    currDateTime = new Date(getCurrentDate()).getTime();
    this.getDatabase = this.getDatabase.bind(this);
  }

  componentWillMount() {
    console.log('Home componentWillMount');
    this.initCouchbase();
    this.checkUserProfile();
  }

  componentDidMount() {
    console.log('Home componentDidMount');

    // stop and start are needed because the OS appears to kill the listener when the app
  // becomes inactive (when the screen is locked, or its put in the background)
  AppState.addEventListener('change', (appState) => {
    if (String(appState).match(/inactive|background/)) {
      Couchbase.stopListener();
    } else if (String(appState).match(/active/)) {
      Couchbase.startListener();
    }
  });

    this.handleNetReachability = ReachabilityManagerEmitter.addListener('ReachabilityStatusListener2', reachabilityResult => {
      console.log('handleNetReachability2: ' + JSON.stringify(reachabilityResult));
      
      this.props.updateSettingsModule({
        prop: 'connectionStatus', value: reachabilityResult.REACHABILITY_MODE
      });
      
      getPreference('userId').then(userId => {
        getPreference('password').then(password => {

          this.checkUploadMode(userId, password);
        });
      });
    });
    NetStatusManager.startNetworkNotifier();
  }

  shouldComponentUpdate() {
    console.log('Home shouldComponentUpdate');
    //  this.queryCurationActivityAndSleepData();
    return true;
  }

  componentWillUnmount() {
    console.log('Home componentWillUnmount');
    isDialogiOSDisplayed = false;
    //this.handleNetReachability.remove();
  }

  componentWillReceiveProps(nextProps) {
    console.log('Home componentWillReceiveProps');
    if (this.state.backPressProp === 0) {
      this.setState({ backPressProp: nextProps.backPressProp });
    }

    // This condition ensures that the checkUserProfile won't get called
    // if there are changes/updates to redux props.
    if (nextProps.backPressProp !== undefined && this.state.backPressProp !== nextProps.backPressProp) {
      this.checkUserProfile();
      this.setState({ backPressProp: nextProps.backPressProp });
    }
  }

  checkUserProfile() {
    if (this.props.onCountryChange === I18n.t('COUNTRY') || this.props.onBirthdateChange === '' || this.props.onGenderChange === I18n.t('GENDER') ||
      (this.props.onWeightChange === undefined || this.props.onWeightChange === '')) {
      Alert.alert('', I18n.t('UPDATE_PROFILE'), [{
        text: I18n.t('OK'), onPress: () => Actions.userProfile()
      }],
        { cancelable: false }
      );
      savePreference('isProfileLacking', JSON.stringify(true));
    } else {
      savePreference('isProfileLacking', JSON.stringify(false));
    }
  }

  initCouchbase() {
    if (manager === undefined) {
      Couchbase.initRESTClient(mgr => {
        CouchbaseManager.init(mgr);
        getPreference('userId').then(userId => {
          getPreference('password').then(password => {

            console.log('Home connectionStatus: ' + JSON.stringify(this.props.connectionStatus));
            this.checkUploadMode(userId, password);
          });
        });
      });
    } else {
      console.log('123couchbase: else getDatabase()');
      this.getDatabase();
    }
  }

  checkUploadMode(userId, password) {
    console.log('Home checkUploadMode uploadMode: ' + this.props.uploadMode);
    console.log('Home checkUploadMode connectionStatus: ' + this.props.connectionStatus);
   
    if (this.props.uploadMode === 0) {
      if (this.props.connectionStatus === 1) {
        // auto-sync should be enabled
        CouchbaseManager.enableAutosync(userId, password, this.getDatabase);
      } else {
        // auto-sync should be disabled. Destroy sync-gateway session
        CouchbaseManager.disableAutosync(userId, password, this.getDatabase, false);
      }
    } else {
      if (this.props.connectionStatus === 2 || this.props.connectionStatus === 1) {
        CouchbaseManager.enableAutosync(userId, password, this.getDatabase);
      } else {
        // connectionType must be none - meaning lte or wifi isn't turned on, then in this case auto-sync should be disabled.
        CouchbaseManager.disableAutosync(userId, password, this.getDatabase, false);
      }
    }
  }

  onRefresh = () => {
    currDateTime = new Date(getCurrentDate()).getTime();
    
    isDialogiOSDisplayed = false;
    this.setState({ isRefreshing: true });
    
    this.queryLast2MosCuration();
    //this.queryWithinTwentyFourHoursCuration();
    this.queryWithinTwentyFourHoursActivity();
    this.queryWithinTwentyFourHoursSleep();
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  openSleepAnalysis() {
    Actions.sleepAnalysis();
  }

  loadActivitySleepAnalysis() {
    Actions.activitySleepAnalysis();
  }

  updateSleepPoints() {
    isDialogiOSDisplayed = false;
    this.queryLast2MosCuration();
    //this.queryWithinTwentyFourHoursCuration();
    this.queryWithinTwentyFourHoursSleep();
  }

  getLatestDate() {
    const strDate = new Date();
    console.log('MONTHS**', 'months: ' + getMonthNames());
    return ' ' + getMonthNames()[strDate.getMonth() + 1].toUpperCase() + ' ' + strDate.getDate();
  }

  getDatabase() {
    manager.database.get_db({ db: DB_NAME })
      .then(res => {
        console.log('123couchbase: getDabase res: ' + JSON.stringify(res));
        this.queryCurationActivityAndSleepData();
      }).catch(err => {
        console.log('123signin: getDatabase() err: ' + JSON.stringify(err) + '\tstatus: ' + err.status);
        if ((err.status === 0 || err.status === 404) && dbCtr < 3) {
          setTimeout(() => {
            this.queryCurationActivityAndSleepData();
            dbCtr++;
          }, 1000);
        }
      });
  }

  queryCurationActivityAndSleepData() {
    this.queryLast2MosCuration();
    //this.queryWithinTwentyFourHoursCuration();
    this.queryWithinTwentyFourHoursActivity();
    this.queryWithinTwentyFourHoursSleep();
  }

  setActivityProps(steps, kcal, recTime, heartRate) {
    this.props.updateSettingsModule({
      prop: 'latestSteps', value: steps
    });
    this.props.updateSettingsModule({
      prop: 'latestCal', value: kcal
    });
    this.props.updateSettingsModule({
      prop: 'latestActivityTime', value: recTime
    });
    this.props.updateSettingsModule({
      prop: 'latestBPM', value: heartRate
    });
  }

  queryWithinTwentyFourHoursActivity() {
    this.setupViewAndQueryLatestActivityDoc().then(res => {
      const rows = res.obj.rows;
      let row;

      for (let i = 0; i < rows.length; i++) {
        const doc = rows[i].doc;
        if (doc !== undefined && new Date(doc.date).getTime() === currDateTime) {
          row = doc;
          break;
        }
      }

      if (row !== undefined) {

        const hourMinsArr = DateUtil.formatTime(row.time);
        const hour = hourMinsArr[0];
        const mins = hourMinsArr[1];
        const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');

        this.setActivityProps(typeof row.steps === 'undefined' ? 0 : row.steps, typeof row.kcal === 'undefined' || isNaN(row.kcal) ? 0 : row.kcal,
          recordedTime, row.heart_rate);
      } else {
        const recordedTime = 0 + ' ' + I18n.t('MIN');
        this.setActivityProps(0, 0, recordedTime, 0);
      }
    }).catch(err => {
      console.log('123activity err:' + JSON.stringify(err));
      console.log('123activity err status: ' + err.status);
      if (err.status === 404 && queryActivityCtr < 3) {
        console.log('123signin requerying activity...');
        setTimeout(() => {
          this.queryWithinTwentyFourHoursActivity();
          queryActivityCtr++;
        }, 1000);
      }
    });
  }

  setSleepProps(sleep, latency, wake, sleepPts) {
    this.props.updateSettingsModule({
      prop: 'latestTotalSleep', value: sleep
    });
    this.props.updateSettingsModule({
      prop: 'latestSleepLatency', value: latency
    });
    this.props.updateSettingsModule({
      prop: 'latestWakeOnset', value: wake
    });

    this.setState({ sleepPoints: sleepPts });
  }

  queryWithinTwentyFourHoursSleep() {
    this.setupViewAndQueryLatestSleepDoc().then(res => {

      const rows = res.obj.rows;
      let row;

      for (let i = 0; i < rows.length; i++) {
        const doc = rows[i].doc;
        if (doc !== undefined && new Date(doc.date).getTime() === currDateTime) {
          row = doc;
          break;
        }
      }

      if (row !== undefined) {

        const hourMinsArrSleep = DateUtil.formatTime(row.sleep_analysis_total);
        const hour = hourMinsArrSleep[0];
        const mins = hourMinsArrSleep[1];
        const recordedSleepTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');

        const hourMinsArrLatency = DateUtil.formatTime(row.sleep_analysis_latency);
        const hourLatency = hourMinsArrLatency[0];
        const minsLatency = hourMinsArrLatency[1];
        const recordedLatencyTime = (hourLatency > 0) ? hourLatency + ' ' + I18n.t('HR') + ' ' + minsLatency + ' ' + I18n.t('MINS') : minsLatency + ' ' + I18n.t('MINS');

        const hourMinsArrWake = DateUtil.formatTime(row.sleep_analysis_wake);
        const hourWake = hourMinsArrWake[0];
        const minsWake = hourMinsArrWake[1];
        const recordedWakeTime = (hourWake > 0) ? hourWake + ' ' + I18n.t('HR') + ' ' + minsWake + ' ' + I18n.t('MINS') : minsWake + ' ' + I18n.t('MINS');
        const recordedSleepPts = row.sleep_analysis_points;

        this.setSleepProps(recordedSleepTime, recordedLatencyTime, recordedWakeTime, recordedSleepPts);
      } else {
        this.setSleepProps(0 + ' ' + I18n.t('MINS'), 0 + ' ' + I18n.t('MINS'), 0 + ' ' + I18n.t('MINS'), 0);
      }
    }).catch(err => {
      if (err.status === 404 && querySleepCtr < 3) {
        console.log('123signin requerying sleep...');
        setTimeout(() => {
          this.queryWithinTwentyFourHoursSleep();
          querySleepCtr++;
        }, 1000);
      }
    });
  }

  queryWithinTwentyFourHoursCuration() {
    this.setupViewAndQueryOfTargetData().then(r => {
      console.log('sleep123 CurationQueryTargetData rows: ' + JSON.stringify(r.obj.rows));
      this.setState({ targetCurationData: r.obj.rows });

      const element = {};

      if (this.state.targetCurationData.length > 0) {
        let maxDate = new Date(this.state.targetCurationData[0].doc.modified_date);
        let maxIndex = 0;

        for (let i = 0; i < this.state.targetCurationData.length; i++) {
          if (new Date(this.state.targetCurationData[i].doc.modified_date) > maxDate) {
            maxDate = new Date(this.state.targetCurationData[i].doc.modified_date);
            maxIndex = i;
            console.log("maxDate: " + maxDate);
            console.log("maxIndex: " + maxIndex);
          }
        }
          
        // get doc element
        const sbw = this.state.targetCurationData[maxIndex].doc.sbw;
        const tst = this.state.targetCurationData[maxIndex].doc.tst;
        const sun = this.state.targetCurationData[maxIndex].doc.sun;
        const bb3Hz = this.state.targetCurationData[maxIndex].doc.bb3Hz;
        const bb6Hz = this.state.targetCurationData[maxIndex].doc.bb6Hz;
        const bb9Hz = this.state.targetCurationData[maxIndex].doc.bb9Hz;
        const bb12Hz = this.state.targetCurationData[maxIndex].doc.bb12Hz;
        const indexOfBb = this.state.targetCurationData[maxIndex].doc.index_of_bb;
        const indexOfPosition = this.state.targetCurationData[maxIndex].doc.index_of_position;
        const sqiAwaken = this.state.targetCurationData[maxIndex].doc.sqi_awaken;
        const sqiCycle = this.state.targetCurationData[maxIndex].doc.sqi_cycle;
        const sqiStability = this.state.targetCurationData[maxIndex].doc.sqi_stability;
        const sqiTotal = this.state.targetCurationData[maxIndex].doc.sqi_total;
        const sqiRM = this.state.targetCurationData[maxIndex].doc.sqi_rm;
        const actualDrink = this.state.targetCurationData[maxIndex].doc.actual_drink;
        const actualSmoke = this.state.targetCurationData[maxIndex].doc.actual_smoke;
        const actualCoffee = this.state.targetCurationData[maxIndex].doc.actual_coffee;
        const actualWalk = this.state.targetCurationData[maxIndex].doc.actual_walk;
        const actualGTBT = this.state.targetCurationData[maxIndex].doc.actual_gtbt;
        const actualBb = this.state.targetCurationData[maxIndex].doc.bb;

        const date = new Date(this.state.targetCurationData[maxIndex].doc.modified_date);
        const dateDay = date.getDate();
        const dateMonth = date.getMonth() + 1;
        const dateYear = date.getFullYear();
        const modifiedDate = dateYear + '-' + dateMonth + '-' + dateDay;

        element.date = modifiedDate;
        element.elementSBW = sbw;
        element.elementTST = tst;
        element.elementGTBT = actualGTBT;
        element.elementSUN = sun;
        element.elementWALK = actualWalk;
        element.elementCOFFEE = actualCoffee;
        element.elementSMOKE = actualSmoke;
        element.elementDRINK = actualDrink;
        element.elementBB3Hz = bb3Hz;
        element.elementBB6Hz = bb6Hz;
        element.elementBB9Hz = bb9Hz;
        element.elementBB12Hz = bb12Hz;
        element.elementBB = actualBb;
        element.indexOfBB = indexOfBb;
        element.indexOfPosition = indexOfPosition;
        element.sqiAWAKEN = sqiAwaken;
        element.sqiCYCLE = sqiCycle;
        element.sqiSTABILITY = sqiStability;
        element.sqiTOTAL = sqiTotal;
        element.sqiRM = sqiRM;
      } else {
        if (curationTwentyFourHoursCtr < 3) {
          isRequerying = true;
          console.log('curation params target element requerying');
          setTimeout(() => {
            this.queryWithinTwentyFourHoursCuration();
            curationTwentyFourHoursCtr++;
          }, 1000);
        }
      }

      console.log('curation params mLast2MosCurationData: ' + JSON.stringify(mLast2MosCurationData));
      console.log('curation params target element: ' + JSON.stringify(element));

      if (mLast2MosCurationData.length >= 3) {
        DeviceEventEmitter.emit('emitAndApplyCuration', mLast2MosCurationData, element);
      } else {
          if(!isDialogiOSDisplayed) {
            showAlert(I18n.t('CURATION'), I18n.t('NEED_SAVE_MORE_DATA'));
            isDialogiOSDisplayed = true;
          }
      }

      this.setState({ isRefreshing: false });
    }).catch(err => {
      console.log('QueryOfTargetData rows err: ' + err);
      this.setState({ isRefreshing: false });
      if (err.status === 404 && curationTwentyFourHoursCtr < 3) {
        console.log('123signin requerying within 24 hours...');
        setTimeout(() => {
          this.queryWithinTwentyFourHoursCuration();
          curationTwentyFourHoursCtr++;
        }, 1000);
      }
    });
  }

  queryLast2MosCuration() {
    this.setupViewAndQueryOfLast2Mos().then(r => {
      console.log('123signin rows: ' + JSON.stringify(r.obj.rows));
      this.setState({ last10DaysCurationData: r.obj.rows });
      mLast2MosCurationData = [];

      if (this.state.last10DaysCurationData.length >= 3) {
        //  Transform data here what keys alike and required in the CurationModule.
        for (let i = 0; i < this.state.last10DaysCurationData.length; i++) {
          //  get doc elements
          const sbw = this.state.last10DaysCurationData[i].doc.sbw;
          const tst = this.state.last10DaysCurationData[i].doc.tst;
          const actualGTBT = this.state.last10DaysCurationData[i].doc.actual_gtbt;
          const sun = this.state.last10DaysCurationData[i].doc.sun;
          const actualWalk = this.state.last10DaysCurationData[i].doc.actual_walk;
          const actualCoffee = this.state.last10DaysCurationData[i].doc.actual_coffee;
          const actualSmoke = this.state.last10DaysCurationData[i].doc.actual_smoke;
          const actualDrink = this.state.last10DaysCurationData[i].doc.actual_drink;
          const bb3Hz = this.state.last10DaysCurationData[i].doc.bb3Hz;
          const bb6Hz = this.state.last10DaysCurationData[i].doc.bb6Hz;
          const bb9Hz = this.state.last10DaysCurationData[i].doc.bb9Hz;
          const bb12Hz = this.state.last10DaysCurationData[i].doc.bb12Hz;
          const bb = this.state.last10DaysCurationData[i].doc.bb;
          const indexOfBb = this.state.last10DaysCurationData[i].doc.index_of_bb;
          const indexOfPosition = this.state.last10DaysCurationData[i].doc.index_of_position;
          const sqiAwaken = this.state.last10DaysCurationData[i].doc.sqi_awaken;
          const sqiCycle = this.state.last10DaysCurationData[i].doc.sqi_cycle;
          const sqiStability = this.state.last10DaysCurationData[i].doc.sqi_stability;
          const sqiTotal = this.state.last10DaysCurationData[i].doc.sqi_total;
          const sqiRM = this.state.last10DaysCurationData[i].doc.sqi_rm;

          const date = new Date(this.state.last10DaysCurationData[i].doc.modified_date);
          const dateDay = date.getDate();
          const dateMonth = date.getMonth() + 1;
          const dateYear = date.getFullYear();
          const modifiedDate = dateYear + '-' + dateMonth + '-' + dateDay;

          const element = {};
          element.date = modifiedDate;
          element.elementSBW = sbw;
          element.elementTST = tst;
          element.elementGTBT = actualGTBT;
          element.elementSUN = sun;
          element.elementWALK = actualWalk;
          element.elementCOFFEE = actualCoffee;
          element.elementSMOKE = actualSmoke;
          element.elementDRINK = actualDrink;
          element.elementBB3Hz = bb3Hz;
          element.elementBB6Hz = bb6Hz;
          element.elementBB9Hz = bb9Hz;
          element.elementBB12Hz = bb12Hz;
          element.elementBB = bb;
          element.indexOfBB = indexOfBb;
          element.indexOfPosition = indexOfPosition;
          element.sqiAWAKEN = sqiAwaken;
          element.sqiCYCLE = sqiCycle;
          element.sqiSTABILITY = sqiStability;
          element.sqiTOTAL = sqiTotal;
          element.sqiRM = sqiRM;

          mLast2MosCurationData.push(element);
        }
      }

      this.queryWithinTwentyFourHoursCuration();
    }).catch(err => {
      if (err.status === 404 && curationLast10DaysCtr < 3) {
        console.log('123signin requerying last 10 days...');
        setTimeout(() => {
          this.queryLast2MosCuration();
          curationLast10DaysCtr++;
        }, 1000);
      }
    });
  }

  setupViewAndQueryOfLast2Mos() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'last2MosDataByDocId',
      include_docs: true
    });
  }

  setupViewAndQueryOfTargetData() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'curationActualDataByDocId',
      include_docs: true
    });
  }

  setupViewAndQueryLatestActivityDoc() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'activityWithinToday',
      include_docs: true
    });
  }

  setupViewAndQueryLatestSleepDoc() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'sleepWithinToday',
      include_docs: true
    });
  }

  render() {
    const today = new Date();
    const month = today.getMonth();
    const date = this.getLatestDate();
    const year = today.getFullYear();
    const day = today.getDay();
    const { sleepDetailsArea, sleepReadsContainer, itemContainer, reads, activityContentWrapper,
      readsTitle, activityBoxContainer, activityIcon, swipe, progressCircleBed } = widgets;

    return (
      <View style={{ flex: 1 }}>
        <HeaderMain/>
        {/* <ScrollView> */}
        {/* <GestureRecognizer
            onSwipeLeft={(state) => this.onSwipeLeft(state)}
            config={config}
          > */}
        <Swiper
          dot={<View style={{ backgroundColor: 'rgba(255,255,255,.3)', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7 }} />}
          activeDot={<View style={{ backgroundColor: '#fff', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7 }} />}
          paginationStyle={{
            bottom: 10
          }}
          index={0}
          loop={false}
        >
          <View style={swipe}>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.onRefresh}
                  tintColor='white'
                  title={I18n.t('LOADING')}
                  titleColor="white"
                  colors={['rgb(233, 186, 0)']}
                  progressBackgroundColor='white' />
              }>
              <TouchableOpacity
                onPress={() => {
                  //if (Platform.OS !== 'ios')
                  this.openSleepAnalysis()
                }

                }
              >
                <ImageBackground style={sleepDetailsArea} >
                  <View style={{ marginHorizontal: sleepMarginHorizontal, marginBottom: sleepMarginBottom, marginTop: sleepMarginTop }}>
                    <View style={{ alignItems: 'center', marginBottom: sleepAreaSize }}>
                      <Text style={{ color: '#fff' }}>
                        {getWeekNames()[day].toUpperCase()}, {date}
                      </Text>
                    </View>
                    <View style={sleepReadsContainer}>
                      <View style={{ alignItems: 'center' }}>
                        <Text style={reads}>{(typeof this.props.latestTotalSleep === 'undefined' || this.props.latestTotalSleep === '') ?
                          0 + ' ' + I18n.t('MIN') : this.props.latestTotalSleep}</Text>
                        <Text style={readsTitle}>{I18n.t('TOTAL').toUpperCase()}</Text>
                      </View>
                      <View style={{ alignItems: 'center' }}>
                        <Text style={reads}>{(typeof this.props.latestSleepLatency === 'undefined' || this.props.latestSleepLatency === '') ?
                          0 + ' ' + I18n.t('MIN') : this.props.latestSleepLatency}</Text>
                        <Text style={readsTitle}>{I18n.t('LATENCY').toUpperCase()}</Text>
                      </View>
                      <View style={{ alignItems: 'center' }}>
                        <Text style={reads}>{(typeof this.props.latestWakeOnset === 'undefined' || this.props.latestWakeOnset === '') ?
                          0 + ' ' + I18n.t('MIN') : this.props.latestWakeOnset}</Text>
                        <Text style={readsTitle}>{I18n.t('WAKE').toUpperCase()}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
              {/* <View style={{ top: 34, left: 0, right: 0, bottom: 0,  alignItems: 'center', position: 'absolute', borderRadius: 4,
      borderWidth: 0.5,
      borderColor: '#d6d7da', }} > */}
              <TouchableOpacity style={{ position: 'absolute', alignSelf: 'center', top: circleTopMargin }} onPress={() => { this.updateSleepPoints(); }}>
                <ProgressCircle percent={this.state.sleepPoints} radius={radius} borderWidth={circleBorderWidth} color="#1e9aa8" shadowColor="#a6d4df" bgColor="#77b1c2" style={{ backgroundColor: 'transparent' }} >
                  <Image style={widgets.progressCircleBed} source={require('../images/bed.png')} />
                  <Text style={widgets.progressCirclePoints}>{this.state.sleepPoints}</Text>
                  <Text style={{ color: '#fff', fontSize: 14 }}>{I18n.t('POINTS')}</Text>
                </ProgressCircle>
              </TouchableOpacity>
              {/* </View> */}
              <View>
                <View style={[itemContainer]}>
                  <TouchableOpacity
                    onPress={() => {
                      //  this.loadActivitySleepAnalysis()
                    }
                    }
                    style={[activityBoxContainer, activityContentWrapper]} >
                    <View style={{ flex: 1, flexDirection: 'row', alignContent: 'flex-start', paddingTop: height <= 640 ? 2.5 : 5, paddingBottom: height <= 640 ? 3 : 6 }}>
                      <Image style={activityIcon} source={require('../images/activity/steps.png')} />
                      <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: height < 640 ? 12 : 16, marginLeft: 15 }}>
                          {
                            (typeof this.props.latestSteps === 'undefined' || this.props.latestSteps === '') ?
                              0 + ' ' + I18n.t('STEP').toLowerCase() : this.props.latestSteps + ' ' + (this.props.latestSteps > 1 ? I18n.t('STEPS') : I18n.t('STEP'))
                          }
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      //  this.loadActivitySleepAnalysis()
                    }}
                    style={[activityBoxContainer, activityContentWrapper]}>
                    <View style={{ flex: 1, flexDirection: 'row', alignContent: 'flex-start', paddingTop: height <= 640 ? 2.5 : 5, paddingBottom: height <= 640 ? 3 : 6 }}>
                      <Image style={activityIcon} source={require('../images/activity/calories.png')} />
                      <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: height < 640 ? 12 : 16, marginLeft: 15 }}>
                          {
                            (typeof this.props.latestCal === 'undefined' || this.props.latestCal === '') ? 0 + ' ' + I18n.t('KCAL') : this.props.latestCal + ' ' + I18n.t('KCAL')
                          }
                        </Text>

                        <Text style={{ fontSize: 10, marginLeft: 15 }}>{I18n.t('BURNED_CALORIES')}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      //  this.loadActivitySleepAnalysis()
                    }}
                    style={[activityBoxContainer, activityContentWrapper]}>
                    <View style={{ flex: 1, flexDirection: 'row', alignContent: 'flex-start', paddingTop: height <= 640 ? 2.5 : 5, paddingBottom: height <= 640 ? 3 : 6 }}>
                      <Image style={activityIcon} source={require('../images/activity/time.png')} />
                      <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: height < 640 ? 12 : 16, marginLeft: 15 }}>
                          {
                            (typeof this.props.latestActivityTime === 'undefined' || this.props.latestActivityTime === '') ?
                              0 + ' ' + I18n.t('MIN') : this.props.latestActivityTime.split(" ")[0] + " " + I18n.t('MIN')
                          }
                        </Text>

                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      // this.loadActivitySleepAnalysis()
                    }}
                    style={[activityBoxContainer, activityContentWrapper]}>
                    <View style={{ flex: 1, flexDirection: 'row', alignContent: 'flex-start', paddingTop: height <= 640 ? 2.5 : 5, paddingBottom: height <= 640 ? 3 : 6 }}>
                      <Image style={activityIcon} source={require('../images/activity/heartbeat.png')} />
                      <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: height < 640 ? 12 : 16, marginLeft: 15 }}>
                          {/* <Text style={{ fontSize: 16, marginLeft: 15 }}> */}
                          {
                            ((typeof this.props.latestBPM === 'undefined' || this.props.latestBPM === '') ? 0 + ' ' + I18n.t('BPM') : this.props.latestBPM + " " + I18n.t('BPM'))
                          }
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View>
                <Qration />
              </View>
            </ScrollView>
          </View>
          <View style={swipe}>
            <MainItems></MainItems>
          </View>
        </Swiper>
        {/* </GestureRecognizer> */}
        {/* </ScrollView> */}
      </View>
    );
  }
}

export default connect(mapStateToProps, { updateSettingsModule, updateUPModule })(Home);
