import React, { Component } from 'react';
import {
  Text, View, Image,
  Alert, TouchableOpacity, Dimensions, DeviceEventEmitter
} from 'react-native';
import { widgets } from '../styles/widgets';
import { text } from '../styles/text';
import { CurationModule } from 'nb_native_modules';
import I18n from './../translate/i18n/i18n';
import {
  CURATION_COUNTER_1, CURATION_COUNTER_2, CURATION_COUNTER_3,
  CURATION_COUNTER_4, CURATION_COUNTER_5, CURATION_COUNTER_6
} from '../utils/DataConstants';
import RenderIf from './RenderIf';

const { height } = Dimensions.get('window');

// CURATION_ANALYSIS
const CURATION_MODEL_DATE = 'CURATION_MODEL_DATE';
const CURATION_DATA_DATE = 'CURATION_DATA_DATE';
const CURATION_ELEMENT_MODEL = 'CURATION_ELEMENT_MODEL';
const CURATION_ELEMENT_DATA = 'CURATION_ELEMENT_DATA';
const CURATION_DIFFERENCE = 'CURATION_DIFFERENCE';
const CURATION_DIRECTION = 'CURATION_DIRECTION';
const CURATION_MESSAGE = 'CURATION_MESSAGE';

const TST = 0;      // Total Sleep Time
const GTBT = 1;     // Elevation
const SUN = 2;      // Sun
const WALK = 3;     // Walk
const COFFEE = 4;   // Coffee
const SMOKE = 5;    // Cigarette
const DRINK = 6;    // Beer
const BB = 7;       // Binaural Beat

const imageSize = height <= 640 ? 25 : 30;

//ill transfer it later in 1 file.
var qRationImageMargin;
var sleepAreaSize;

switch (height) {
  case 812: {
    sleepAreaSize = 200;
    qRationImageMargin = 18;
    break;
  }
  case 740:
  case 736: { //iphone 6-8+
    sleepAreaSize = 200;
    qRationImageMargin = 18;
    break;
  }
  case 667: {//iphone6-8
    sleepAreaSize = 150;
    qRationImageMargin = 15;
    break;
  }
  case 640: { //common android
    sleepAreaSize = 116;
    qRationImageMargin = 16;
    break;
  }
  default: { //iphone5/SE else heigher
    if (height < 640) {
      sleepAreaSize = 116;
      qRationImageMargin = 10;
    } else {
      sleepAreaSize = 200;
      qRationImageMargin = 18;
    }
    break;
  }
}


const qrationImages = {
  sleep: {
    empty: require('../images/qration/Sleep/sleep_almost.png'),
    half: require('../images/qration/Sleep/sleep_half.png'),
    full: require('../images/qration/Sleep/sleep_full.png')
  },
  elevation: {
    empty: require('../images/qration/Elevation/elevation_almost.png'),
    half: require('../images/qration/Elevation/elevation_half.png'),
    full: require('../images/qration/Elevation/elevation_full.png')
  },
  sunshine: {
    empty: require('../images/qration/Sun/light_quarter.png'),
    half: require('../images/qration/Sun/light_half.png'),
    full: require('../images/qration/Sun/light_full.png')
  },
  steps: {
    empty: require('../images/qration/Walk/stand.png'),
    half: require('../images/qration/Walk/walk.png'),
    full: require('../images/qration/Walk/run.png')
  },
  coffee: {
    empty: require('../images/qration/Coffee/coffee_almost.png'),
    half: require('../images/qration/Coffee/coffee_half.png'),
    full: require('../images/qration/Coffee/coffee_full.png')
  },
  smoke: {
    empty: require('../images/qration/Cigarette/cigarette_almost.png'),
    half: require('../images/qration/Cigarette/cigarette_half.png'),
    full: require('../images/qration/Cigarette/cigarette_full.png')

  },
  beer: {
    empty: require('../images/qration/Beer/beer_almost_empty.png'),
    half: require('../images/qration/Beer/beer_half.png'),
    full: require('../images/qration/Beer/beer_full.png')
  },
  binauralbeat: {
    empty: require('../images/qration/Headphone/headphone_almost.png'),
    half: require('../images/qration/Headphone/headphone_half.png'),
    full: require('../images/qration/Headphone/headphone_full.png')
  }
}

const adviceImg = {
  text: '20 mins',
  arrowUp: require('../images/qration/Arrows/increase.png'),
  arrowDown: require('../images/qration/Arrows/decrease.png'),
  balance: require('../images/qration/Arrows/balance.png')
};

let imgSrc = 'sleep_almost';
let directionSrc = 'balance';

class Qration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sleepSrc: '../images/qration/Elevation/elevation_half.png',
      latencySrc: '../images/qration/Elevation/elevation_half.png',
      sunshineSrc: '../images/qration/Elevation/elevation_half.png',
      stepsSrc: '../images/qration/Elevation/elevation_half.png',
      coffeeSrc: '../images/qration/Elevation/elevation_half.png',
      cigaretteSrc: '../images/qration/Elevation/elevation_half.png',
      beerSrc: '../images/qration/Elevation/elevation_half.png',
      binauralBeatSrc: '../images/qration/Elevation/elevation_half.png',
      sleepAdviceText: '20 mins',
      latencyAdviceText: '20 mins',
      sunshineAdviceText: '20 mins',
      stepsAdviceText: '20 mins',
      coffeeAdviceText: '20 mins',
      cigaretteAdviceText: '20 mins',
      beerAdviceText: '20 mins',
      binauralBeatAdviceText: '20 mins',
      sleepAdviceMsg: 'Sleep time is somewhat lacking',
      latencyAdviceMsg: 'Sleep time is somewhat lacking',
      sunshineAdviceMsg: 'Sleep time is somewhat lacking',
      stepsAdviceMsg: 'Sleep time is somewhat lacking',
      coffeeAdviceMsg: 'Sleep time is somewhat lacking',
      cigaretteAdviceMsg: 'Sleep time is somewhat lacking',
      beerAdviceMsg: 'Sleep time is somewhat lacking',
      binauralBeatAdviceMsg: 'Sleep time is somewhat lacking',
      sleepDirectionSrc: '../images/qration/Arrows/decrease.png',
      latencyDirectionSrc: '../images/qration/Arrows/decrease.png',
      sunshineDirectionSrc: '../images/qration/Arrows/decrease.png',
      stepsDirectionSrc: '../images/qration/Arrows/decrease.png',
      coffeeDirectionSrc: '../images/qration/Arrows/decrease.png',
      cigaretteDirectionSrc: '../images/qration/Arrows/decrease.png',
      beerDirectionSrc: '../images/qration/Arrows/decrease.png',
      binauralBeatDirectionSrc: '../images/qration/Arrows/decrease.png',
      sleepAdviceMsgMap: new Map(),
      latencyAdviceMsgMap: new Map(),
      sunshineAdviceMsgMap: new Map(),
      stepsAdviceMsgMap: new Map(),
      coffeeAdviceMsgMap: new Map(),
      cigaretteAdviceMsgMap: new Map(),
      beerAdviceMsgMap: new Map(),
      binauralBeatAdviceMsgMap: new Map(),
      databaseArr: [],
      elementObj: {}
    };

    this.getCurationAnalysisHandler = this.getCurationAnalysisHandler.bind(this);
  }

  getSleep(response2) {
    // 2. TST
    //console.log('getSleep: ' + response2);
    if (response2 === 1) {
      //console.log('inside empty');
      return 'sleep_almost';
    }
    else if (response2 === -1) {
      return 'sleep_full';
    }
    else {
      return 'sleep_half';
    }
  }

  getLatency(response2) {
    // 2. Latency
    //console.log('getLatency: ' + response2);
    if (response2 === 1) {
      return 'elevation_almost';
    }
    else if (response2 === -1) {
      return 'elevation_full';
    }
    else {
      return 'elevation_half';
    }
  }

  getSunshine(response2) {
    // 3. Sunshine
    //console.log('getSunshine: ' + response2);
    if (response2 === 1) {
      //console.log('inside empty');
      return 'light_almost';
    }
    else if (response2 === -1) {
      return 'light_full';
    }
    else {
      return 'light_half';
    }
  }

  getSteps(response2) {
    // 4. Steps
    //console.log('getSteps: ' + response2);
    if (response2 === 1) {
      return 'stand';
    }
    else if (response2 === -1) {
      return 'run';
    }
    else {
      return 'walk';
    }
  }

  getCoffee(response2) {
    // 5. Coffee
    //console.log('getCoffee: ' + response2);
    if (response2 === 1) {
      return 'coffee_almost';
    }
    else if (response2 === -1) {
      return 'coffee_full';
    }
    else {
      return 'coffee_half';
    }
  }

  getCigarette(response2) {
    // 6. Cigarette
    //console.log('getCigarette: ' + response2);
    if (response2 === 1) {
      return 'cigarette_almost';
    }
    else if (response2 === -1) {
      return 'cigarette_full';
    }
    else {
      return 'cigarette_half';
    }
  }

  getBeer(response2) {
    // 7. Beer
    //console.log('getBeer: ' + response2);
    if (response2 === 1) {
      return 'beer_almost';
    }
    else if (response2 === -1) {
      return 'beer_full';
    }
    else {
      return 'beer_half';
    }

  }

  getBinauralBeat(response2) {
    // 8. Binaural Beat
    //console.log('getBinauralBeat: ' + response2);
    if (response2 === 1) {
      return 'headphone_almost';
    }
    else if (response2 === -1) {
      return 'headphone_full';
    }
    else {
      return 'headphone_half';
    }

  }

  onPressSleep() {
    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('SLEEP'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const sleepAdviceMsgMap = this.state.sleepAdviceMsgMap;
      const diff = sleepAdviceMsgMap["diff"];
      const counter = sleepAdviceMsgMap["counter"];

      //console.log('Sleep counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          adviceMsg2 = `${I18n.t('LACK_OF_SLEEP')} ${I18n.t('MORE_SLEEP_BETTER')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_2:

          adviceMsg2 = `${I18n.t('SLEEP_MORE_THAN_USUAL')} ${I18n.t('LESS_SLEEP_BETTER')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          adviceMsg = I18n.t('MAINTAIN_SIMILAR_HOURS_SLEEP');

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('SPEND_LESS_TIME_SLEEPING');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('SPEND_MORE_TIME_SLEEPING');

          break;

        case CURATION_COUNTER_6:

          adviceMsg = I18n.t('MAINTAIN_SIMILAR_LEVEL_SLEEP');

          break;
      }

      Alert.alert(I18n.t('SLEEP'), adviceMsg);
    }
  }

  onPressLatency() {

    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('LATENCY'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const latencyAdviceMsgMap = this.state.latencyAdviceMsgMap;
      const diff = latencyAdviceMsgMap["diff"];
      const counter = latencyAdviceMsgMap["counter"];

      //console.log('Latency counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          adviceMsg2 = `${I18n.t('SLEEP_LATENCY_EARLY')} ${I18n.t('GOING_TO_BED_LATER')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_2:

          adviceMsg2 = `${I18n.t('SLEEP_LATENCY_LATE')} ${I18n.t('GOING_TO_BED_EARLIER')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          adviceMsg = I18n.t('KEEP_SLEEP_LEVEL_LATENCY');

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('TRY_GO_TO_BED_EARLIER');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('TRY_GO_TO_BED_LATER');

          break;

        case CURATION_COUNTER_6:

          adviceMsg = I18n.t('KEEP_SLEEP_LEVEL_LATENCY');

          break;
      }

      Alert.alert(I18n.t('LATENCY'), adviceMsg);
    }
  }

  onPressSunshine() {
    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('SUNSHINE'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const sunshineAdviceMsgMap = this.state.sunshineAdviceMsgMap;
      const diff = sunshineAdviceMsgMap["diff"];
      const counter = sunshineAdviceMsgMap["counter"];

      //console.log('Sunshine counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          adviceMsg2 = `${I18n.t('INCREASING_MORE_OUTDOOR')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_2:

          adviceMsg2 = `${I18n.t('MINIMIZING_LESS_OUTDOOR')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          adviceMsg = I18n.t('KEEP_TIME_OUTDOORS');

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('REDUCE_OUTDOOR');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('INCREASE_OUTDOOR');

          break;

        case CURATION_COUNTER_6:

          adviceMsg = I18n.t('KEEP_SAME_LEVEL_OUTDOORS');

          break;
      }

      Alert.alert(I18n.t('SUNSHINE'), adviceMsg);
    }
  }

  onPressSteps() {
    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('STEPS_2'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const stepsAdviceMsgMap = this.state.stepsAdviceMsgMap;
      const diff = stepsAdviceMsgMap["diff"];
      const counter = stepsAdviceMsgMap["counter"];

      //console.log('Steps counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          adviceMsg2 = `${I18n.t('ACTIVITY_NOT_ENOUGH')} ${I18n.t('MORE_FOOTSTEPS')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_2:

          adviceMsg2 = `${I18n.t('LOT_OF_ACTIVITIES')} ${I18n.t('LESS_FOOTSTEPS')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          adviceMsg = I18n.t('KEEP_SAME_LEVEL_ACTIVITY');

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('REDUCE_ACTIVITIES');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('INCREASE_ACTIVITIES');

          break;

        case CURATION_COUNTER_6:

          adviceMsg = I18n.t('KEEP_SAME_LEVEL_ACTIVITY');

          break;
      }

      Alert.alert(I18n.t('STEPS_2'), adviceMsg);
    }
  }

  onPressCoffee() {
    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('COFFEE'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const coffeeAdviceMsgMap = this.state.coffeeAdviceMsgMap;
      const diff = coffeeAdviceMsgMap["diff"];
      const counter = coffeeAdviceMsgMap["counter"];

      //console.log('Coffee counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          adviceMsg = `${I18n.t('FIGURES_MODERATE_REDUCE_COFFEE')}`;

          break;

        case CURATION_COUNTER_2:

          adviceMsg2 = `${I18n.t('DRINKING_MORE_CUPS_OF_COFFEE')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          adviceMsg = I18n.t('KEEP_NO_OF_CUPS');

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('REDUCE_COFFEE_INTAKE');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('FIGURES_MODERATE_REDUCE_COFFEE');

          break;

        case CURATION_COUNTER_6:

          adviceMsg = I18n.t('KEEP_SAME_LEVEL_OF_COFFEE');

          break;
      }

      Alert.alert(I18n.t('COFFEE'), adviceMsg);
    }
  }

  onPressCigarette() {
    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('CIGARETTE'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const cigaretteAdviceMsgMap = this.state.cigaretteAdviceMsgMap;
      const diff = cigaretteAdviceMsgMap["diff"];
      const counter = cigaretteAdviceMsgMap["counter"];

      //console.log('Cigarette counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          adviceMsg = `${I18n.t('FIGURES_MODERATE_REDUCE_SMOKE')}`;

          break;

        case CURATION_COUNTER_2:

          adviceMsg2 = `${I18n.t('MORE_CIGARETTES_SMOKED')} ${I18n.t('LESS_STICKS_BETTER')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          adviceMsg = I18n.t('SMOKE_SAME_NO');

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('LESS_SMOKE_BEFORE');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('FIGURES_MODERATE_REDUCE_SMOKE');

          break;

        case CURATION_COUNTER_6:

          adviceMsg = I18n.t('SMOKE_SAME_NO');

          break;
      }

      Alert.alert(I18n.t('CIGARETTE'), adviceMsg);
    }
  }

  onPressDrink() {
    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('ALCOHOL'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const beerAdviceMsgMap = this.state.beerAdviceMsgMap;
      const diff = beerAdviceMsgMap["diff"];
      const counter = beerAdviceMsgMap["counter"];

      //console.log('Alcohol counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          adviceMsg = `${I18n.t('FIGURES_MODERATE_REDUCE_ALCOHOL')}`;

          break;

        case CURATION_COUNTER_2:

          adviceMsg2 = `${I18n.t('MORE_ALCOHOL_CONSUMED')} ${I18n.t('LESS_CUPS_BETTER')}`;
          adviceMsg = adviceMsg2.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          adviceMsg = I18n.t('KEEP_ALCOHOL_INTAKE');

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('REDUCE_ALCOHOL_INTAKE');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('FIGURES_MODERATE_REDUCE_ALCOHOL');

          break;

        case CURATION_COUNTER_6:

          adviceMsg = I18n.t('KEEP_ALCOHOL_INTAKE');

          break;
      }

      Alert.alert(I18n.t('ALCOHOL'), adviceMsg);
    }
  }

  onPressBinauralBeat() {
    if (!this.isQrationDataNotEmpty()) {
      Alert.alert(I18n.t('BINAURAL_BEAT'), I18n.t('NO_DATA'));
    }
    else {

      let adviceMsg = '';
      const binauralBeatAdviceMsgMap = this.state.binauralBeatAdviceMsgMap;
      const diff = binauralBeatAdviceMsgMap["diff"];
      const counter = binauralBeatAdviceMsgMap["counter"];
      //  const modelFreq = binauralBeatAdviceMsgMap["modelFreq"];

      //console.log('Binaural Beat counter: ' + counter);

      switch (counter) {

        case CURATION_COUNTER_1:

          //  adviceMsg2 = `${I18n.t('LISTENED_BINAURAL_BEAT')}`;
          adviceMsg3 = `${I18n.t('MORE_MIN_LISTENING_BETTER')}`;
          //  adviceMsg = adviceMsg2.replace('00', modelFreq) + adviceMsg3.replace('00', diff);
          adviceMsg = adviceMsg3.replace('00', diff);

          break;

        case CURATION_COUNTER_2:

          //  adviceMsg2 = `${I18n.t('LISTENED_BINAURAL_BEAT')}`;
          adviceMsg3 = `${I18n.t('LESS_MIN_LISTENING_BETTER')}`;
          //  adviceMsg = adviceMsg2.replace('00', modelFreq) + adviceMsg3.replace('00', diff);
          adviceMsg = adviceMsg3.replace('00', diff);

          break;

        case CURATION_COUNTER_3:

          //  adviceMsg = `${I18n.t('LISTENED_BINAURAL_BEAT')} ${modelFreq} ${I18n.t('HZ_SAME_BETTER')}`;
          //  adviceMsg = `${I18n.t('HZ_SAME_BETTER')}`;
          adviceMsg = `${I18n.t('KEEP_IT_UP')}`;

          break;

        case CURATION_COUNTER_4:

          adviceMsg = I18n.t('REDUCE_LISTENING_TIME');

          break;

        case CURATION_COUNTER_5:

          adviceMsg = I18n.t('INCREASE_LISTENING_TIME');

          break;

        case CURATION_COUNTER_6:

          //  adviceMsg2 = `${I18n.t('YOUVE_LISTENED_BB_HZ_BETTER')}`;
          //  adviceMsg = adviceMsg2.replace('00', modelFreq);
          adviceMsg = `${I18n.t('KEEP_IT_UP')}`;

          break;
      }

      Alert.alert(I18n.t('BINAURAL_BEAT'), adviceMsg);
    }
  }

  isQrationDataNotEmpty() {
    const isEmpty = this.state.databaseArr.length > 0 && Object.keys(this.state.elementObj).length !== 0;
    //  console.log('isQrationDataNotEmpty: ' + isEmpty);
    return isEmpty;
  }

  getCurationAnalysisHandler() {
    console.log(`getCurationAnalysisHandler database: ${JSON.stringify(this.state.databaseArr)}`);
    console.log(`getCurationAnalysisHandler element: ${JSON.stringify(this.state.elementObj)}`);

    if (this.isQrationDataNotEmpty()) {
      CurationModule.initCurationAnalysis(this.state.databaseArr, this.state.elementObj,
        response => {
          //console.log('initCurationAnalysis', response);

          // TST Image
          CurationModule.getCuratorAnalysisData(TST, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getSleep(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ sleepDirectionSrc: directionSrc });
                this.setState({ sleepSrc: imgSrc });
                //console.log('sleepSrc: ' + imgSrc);
                //console.log('sleepDirectionSrc: ' + directionSrc);
              }
            });

          // TST Message  
          CurationModule.getCuratorAnalysisData(TST, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  sleepAdviceMsg: parseResponse["english"],
                  sleepAdviceMsgMap: parseResponse
                });
                //console.log('sleepAdviceMsg: ' + JSON.stringify(this.state.sleepAdviceMsg));
              }
            });


          // TST Diff
          CurationModule.getCuratorAnalysisData(TST, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ sleepAdviceText: response4 });
                console.log('Curation Result TST diff: ' + response4);
              }
            });

          // Latency Image
          CurationModule.getCuratorAnalysisData(GTBT, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getLatency(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ latencyDirectionSrc: directionSrc });
                this.setState({ latencySrc: imgSrc });
                //console.log('latencySrc: ', imgSrc);
                //console.log('latencyDirectionSrc: ' + directionSrc);
              }
            });

          // Latency Message
          CurationModule.getCuratorAnalysisData(GTBT, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  latencyAdviceMsg: parseResponse["english"],
                  latencyAdviceMsgMap: parseResponse
                });
                //console.log('latencyAdviceMsg: ' + JSON.stringify(this.state.latencyAdviceMsg));
              }
            });

          // GTBT Diff
          CurationModule.getCuratorAnalysisData(GTBT, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ latencyAdviceText: response4 });
                console.log('Curation Result GTBT diff: ' + response4);
              }
            });

          // Sunshine
          CurationModule.getCuratorAnalysisData(SUN, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getSunshine(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ sunshineDirectionSrc: directionSrc });
                this.setState({ sunshineSrc: imgSrc });
                //console.log('sunshineSrc: ' + JSON.stringify(imgSrc));
                //console.log('sunshineDirectionSrc: ' + directionSrc);
              }
            });

          // Sunshine Message
          CurationModule.getCuratorAnalysisData(SUN, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  sunshineAdviceMsg: parseResponse["english"],
                  sunshineAdviceMsgMap: parseResponse
                });
                //console.log('sunshineAdviceMsg: ' + JSON.stringify(this.state.sunshineAdviceMsg));
              }
            });

          // Sunshine Diff
          CurationModule.getCuratorAnalysisData(SUN, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ sunshineAdviceText: response4 });
                //console.log('sunshineAdviceText: ' + JSON.stringify(this.state.sunshineAdviceText));
                console.log('Curation Result Sunshine diff: ' + response4);
              }
            });

          // Steps Image
          CurationModule.getCuratorAnalysisData(WALK, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getSteps(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ stepsDirectionSrc: directionSrc });
                this.setState({ stepsSrc: imgSrc });
                //console.log('stepsSrc: ' + JSON.stringify(imgSrc));
                console.log('stepsDirectionResp: ' + response2);
                console.log('stepsDirectionSrc: ' + directionSrc);
              }

            });

          // Steps Message  
          CurationModule.getCuratorAnalysisData(WALK, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  stepsAdviceMsg: parseResponse["english"],
                  stepsAdviceMsgMap: parseResponse
                });
                //console.log('stepsAdviceMsg: ' + JSON.stringify(this.state.stepsAdviceMsg));
              }
            });

          // Steps Diff
          CurationModule.getCuratorAnalysisData(WALK, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ stepsAdviceText: response4 });
                console.log('Curation Result Steps diff: ' + response4);
              }
            });

          // Coffee Img
          CurationModule.getCuratorAnalysisData(COFFEE, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getCoffee(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ coffeeDirectionSrc: directionSrc });
                this.setState({ coffeeSrc: imgSrc });
                //console.log('coffeeSrc: ' + JSON.stringify(imgSrc));
                console.log('coffeeDirectionResp: ' + response2);
                console.log('coffeeDirectionSrc: ' + directionSrc);
              }
            });

          // Coffee Message
          CurationModule.getCuratorAnalysisData(COFFEE, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  coffeeAdviceMsg: parseResponse["english"],
                  coffeeAdviceMsgMap: parseResponse
                });
                //console.log('coffeeAdviceMsg: ' + JSON.stringify(this.state.coffeeAdviceMsg));
              }
            });

          //  Coffee Diff
          CurationModule.getCuratorAnalysisData(COFFEE, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ coffeeAdviceText: response4 });
                console.log('Curation Result Coffee diff: ' + response4);
              }
            });

          // Cigarette Icon
          CurationModule.getCuratorAnalysisData(SMOKE, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getCigarette(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ cigaretteDirectionSrc: directionSrc });
                this.setState({ cigaretteSrc: imgSrc });
                //console.log('cigaretteSrc: ' + JSON.stringify(imgSrc));
                console.log('cigaretteDirectionResp: ' + response2);
                console.log('cigaretteDirectionSrc: ' + directionSrc);
              }
            });

          // Cigarette Message
          CurationModule.getCuratorAnalysisData(SMOKE, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  cigaretteAdviceMsg: parseResponse["english"],
                  cigaretteAdviceMsgMap: parseResponse
                });
                //console.log('cigaretteAdviceMsg: ' + JSON.stringify(this.state.cigaretteAdviceMsg));
              }
            });

          // Cigarette Diff
          CurationModule.getCuratorAnalysisData(SMOKE, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ cigaretteAdviceText: response4 });
                console.log('Curation Result Smoke diff: ' + response4);
              }
            });

          // Beer Img
          CurationModule.getCuratorAnalysisData(DRINK, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getBeer(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ beerDirectionSrc: directionSrc });
                this.setState({ beerSrc: imgSrc });
                //console.log('beerSrc: ' + JSON.stringify(imgSrc));
                //console.log('beerDirectionSrc: ' + directionSrc);
              }
            });

          // Beer Message
          CurationModule.getCuratorAnalysisData(DRINK, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  beerAdviceMsg: parseResponse["english"],
                  beerAdviceMsgMap: parseResponse
                });
                //console.log('beerAdviceMsg: ' + JSON.stringify(this.state.beerAdviceMsg));
              }
            });

          // Beer Diff
          CurationModule.getCuratorAnalysisData(DRINK, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ beerAdviceText: response4 });
                console.log('Curation Result Beer diff: ' + response4);
              }
            });

          // Binaural Beat
          CurationModule.getCuratorAnalysisData(BB, CurationModule.CURATION_DIRECTION,
            response2 => {
              if (this.isComponentVisible) {
                imgSrc = this.getBinauralBeat(response2);
                directionSrc = response2 !== -1 ? response2 === 1 ? "increase" : "balance" : "decrease";
                this.setState({ binauralBeatDirectionSrc: directionSrc });
                this.setState({ binauralBeatSrc: imgSrc });
                //console.log('binauralBeatSrc: ' + JSON.stringify(imgSrc));
                console.log('binauralBeatDirectionResp: ' + response2);
                console.log('binauralBeatDirectionSrc: ' + directionSrc);
              }
            });

          // Binaural Beat Message
          CurationModule.getCuratorAnalysisData(BB, CurationModule.CURATION_MESSAGE,
            response3 => {
              if (this.isComponentVisible) {
                const parseResponse = JSON.parse(response3)
                this.setState({
                  binauralBeatAdviceMsg: parseResponse["english"],
                  binauralBeatAdviceMsgMap: parseResponse
                });
                //console.log('binauralBeatAdviceMsg: ' + JSON.stringify(this.state.binauralBeatAdviceMsg));
              }
            });

          // Binaural Diff        
          CurationModule.getCuratorAnalysisData(BB, CurationModule.CURATION_DIFFERENCE,
            response4 => {
              if (this.isComponentVisible) {
                this.setState({ binauralBeatAdviceText: response4 });
                console.log('Curation Result BB diff: ' + response4);
              }
            });
        });
    }
  }

  componentWillUnmount() {
    this.isComponentVisible = false;
  }

  componentDidMount() {
    this.isComponentVisible = true;

    DeviceEventEmitter.addListener('emitAndApplyCuration', (database, element) => {
      console.log('getCurationAnalysisHandler deviceEventEmitter d: ' + JSON.stringify(database));
      console.log('getCurationAnalysisHandler deviceEventEmitter e: ' + JSON.stringify(element));
      this.setState({ databaseArr: database });
      this.setState({ elementObj: element });
      this.getCurationAnalysisHandler();
    });
  }

  _renderSunshine = () => (
    <TouchableOpacity onPress={() => this.onPressSunshine()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('SUNSHINE')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.sunshineSrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.sunshineDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.sunshineAdviceText} {I18n.t('MIN_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>
  );

  _renderCoffee = () => (
    <TouchableOpacity onPress={() => this.onPressCoffee()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('COFFEE')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.coffeeSrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.coffeeDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.coffeeAdviceText} {I18n.t('CUP_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>

  );

  _renderAlcohol = () => (
    <TouchableOpacity onPress={() => this.onPressDrink()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('ALCOHOL')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.beerSrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.beerDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.beerAdviceText} {I18n.t('MUG_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>

  );

  _renderCigarette = () => (
    <TouchableOpacity onPress={() => this.onPressCigarette()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('CIGARETTE')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.cigaretteSrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.cigaretteDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.cigaretteAdviceText} {I18n.t('STICK_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>
  );

  _renderSteps = () => (
    <TouchableOpacity onPress={() => this.onPressSteps()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('STEPS_2')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.stepsSrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.stepsDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.stepsAdviceText} {I18n.t('STEP_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>
  );

  _renderLatency = () => (
    <TouchableOpacity onPress={() => this.onPressLatency()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('LATENCY')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.latencySrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.latencyDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.latencyAdviceText} {I18n.t('MIN_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>

  );

  _renderSleep = () => (
    <TouchableOpacity onPress={() => this.onPressSleep()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('SLEEP')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.sleepSrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.sleepDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.sleepAdviceText} {I18n.t('MIN_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>

  );

  _renderBinaural = () => (
    <TouchableOpacity onPress={() => this.onPressBinauralBeat()} style={widgets.qrationIconsWrapper}>
      <View>
        <Text style={{ alignSelf: 'center', fontSize: 12, color: '#a094c0' }}>{I18n.t('BINAURAL')}</Text>
        {RenderIf(this.isQrationDataNotEmpty())(
          <Image style={{ width: imageSize, height: imageSize, marginVertical: qRationImageMargin, alignSelf: 'center' }}
            source={{ uri: this.state.binauralBeatSrc }} />
        )}
        {RenderIf(this.isQrationDataNotEmpty())(
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Image style={{ height: 10, width: 10 }} source={{ uri: this.state.binauralBeatDirectionSrc }} />
            <Text style={{ fontSize: 10 }} >{this.state.binauralBeatAdviceText} {I18n.t('MIN_S')}</Text>
          </View>
        )}
        {RenderIf(!this.isQrationDataNotEmpty())(
          <Text style={text.qrationElemNoData}>{I18n.t('NO_DATA')}</Text>
        )}
      </View>
    </TouchableOpacity>

  );

  render() {
    //console.log('--------------------------Render--------------------------')
    const { itemContainer, boxContainer, activityContentWrapper, iconContainerStyle, iconStyle } = widgets;
    return (
      <View>
        <View style={[itemContainer]}>
          <View style={[activityContentWrapper]}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              {this._renderSunshine()}
              {this._renderCoffee()}
            </View>
          </View>
          <View style={[activityContentWrapper]}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              {this._renderAlcohol()}
              {this._renderCigarette()}
            </View>
          </View>
          <View style={[activityContentWrapper]}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              {this._renderSteps()}
              {this._renderLatency()}
            </View>
          </View>
          <View style={[activityContentWrapper]}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              {this._renderSleep()}
              {this._renderBinaural()}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default Qration;
