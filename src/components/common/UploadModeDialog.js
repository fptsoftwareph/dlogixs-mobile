import React, { Component } from 'react';
import { View, Text, Modal, TouchableWithoutFeedback } from 'react-native';
import { widgets } from '../../styles/widgets';
import { text } from '../../styles/text';
import { containers } from '../../styles/containers';
import I18n from './../../translate/i18n/i18n';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { CARD_COLOR, COLOR_TINT_ON } from '../../styles/colors';
import { getPreference } from '../../utils/CommonMethods';
import CouchbaseManager from '../../utils/CouchbaseManager';

class UploadModeDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      connectionStatus: false,
      uploadMode: 0
    };

    this.dismissDialog = this.props.dismissDialog;
    this.updateSettingsItemModule = this.props.updateSettingsItemModule;
  }

  setModalVisible(visibility) {
    this.dismissDialog(visibility);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isVisible: nextProps.visibility,
      connectionStatus: nextProps.connectionStatus,
      uploadMode: nextProps.uploadMode
    });
  }

  onSelect(index, value) {
    console.log('onSelect: ' + index + ' ' + value);
    this.updateSettingsItemModule('uploadMode', index);

    getPreference('userId').then(userId => {
      getPreference('password').then(password => {
        this.checkUploadMode(userId, password, index);
      });
    });

    setTimeout(() => {
      this.setModalVisible(false);
    }, 10);
  }

  checkUploadMode(userId, password, index) {
    console.log('UploadModeDialog checkUploadMode connectionStatus: ' + this.state.connectionStatus);

    if (index === 0) {
      if (this.state.connectionStatus === 1) {
        // auto-sync should be enabled
        CouchbaseManager.enableAutosync(userId, password, () => {});
      } else {
        // auto-sync should be disabled. Destroy sync-gateway session
        CouchbaseManager.disableAutosync(userId, password, () => {}, false);
      }
    } else {
      if (this.state.connectionStatus === 2 || this.state.connectionStatus === 1) {
        CouchbaseManager.enableAutosync(userId, password, () => {});
      } else {
        // connectionType must be none - meaning lte or wifi isn't turned on, then in this case auto-sync should be disabled.
        CouchbaseManager.disableAutosync(userId, password, () => {}, false);
      }
    }
  }

  render() {
    return (
      <View>
        <Modal animationType="slide"
          transparent={true}
          style={widgets.modal}
          visible={this.state.isVisible}
          presentation
          onRequestClose={() => {
            this.setModalVisible(false);
          }}>
          <TouchableWithoutFeedback onPressIn={() =>
            this.setModalVisible(false)}>
          <View style={[widgets.bluetoothScanPromptCon]}>
          <TouchableWithoutFeedback onPressIn={() => { }}>
          <View style={[widgets.bluetoothScanPrompt, containers.sendWifiDetailsCon]}>
          <Text style={[text.bluetoothTitleTask, widgets.commonMarginRight, widgets.commonMarginLeft]}>{I18n.t('UPLOAD_MODE')}</Text>
              <RadioGroup
                highlightColor={CARD_COLOR}
                selectedIndex={this.state.uploadMode}
                onSelect = {(index, value) => this.onSelect(index, value)}>
                <RadioButton value={'WIFI'} color={COLOR_TINT_ON}>
                  <Text>{I18n.t('WIFI')}</Text>
                </RadioButton>
                <RadioButton value={'WIFI_LTE'} color={COLOR_TINT_ON}>
                  <Text>{I18n.t('WIFI_LTE')}</Text>
                </RadioButton>
              </RadioGroup>
          </View>
          </TouchableWithoutFeedback>
          </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  }
}

export { UploadModeDialog };

