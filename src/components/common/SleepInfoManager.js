import {
  NativeModules,
  NativeEventEmitter,
  Platform
} from 'react-native';
import getCurrentDate from '../../utils/DateUtils';
import { getPreference } from '../../utils/CommonMethods';
import { StationModule } from 'nb_native_modules';
import TargetDoc from './TargetDoc';
import CurationActualData from './CurationActualData';
import * as DateUtil from '../../utils/DateUtils';

const StationManagerModule = NativeModules.StationModule;
const StationManagerEmitter = new NativeEventEmitter(StationManagerModule);

class SleepManager {
  constructor() {
    this.targetCurationData = [];
    this.user_id = '';
    this.sbw = 0;
    this.sqiAwaken = 0;
    this.sqiCycle = 0;
    this.sqiRM = 0;
    this.sqiStability = 0;
    this.sqiTotal = 0;
    this.analysisPoints = 0;
    this.gtbt = 0;
    this.targetSleepHours = 0;
    this.refreshSleepCallback = null;
    this.uuidv4 = require('uuid/v4');
    this.initSleepMgr();
    this.handleSleepInfo = this.handleSleepInfo.bind(this);
    this.handleStationSleepInfo = null;
  }

  static sharedInstance() {
    if (SleepManager.sleepMgr === undefined) {
      SleepManager.sleepMgr = new SleepManager();
    }
    return SleepManager.sleepMgr;
  }

  handleSleepInfo(result) {
    console.log('sleep123 handleSleepInfo: ' + JSON.stringify(result));
    if (Platform.OS === 'ios') {
      StationModule.printLog(`sleep123 handleSleepInfo: ${JSON.stringify(result)}`);
    }
    // generate a random _id and user_id  for the document
    let _id = this.uuidv4();
    let sleepAnalysisWake = result.SLEEP_ANALYSIS_WAKE;
    let sleepHypnogram = result.SLEEP_HYPNOGRAM;
    let sleepAnalysisLatency = result.SLEEP_ANALYSIS_LATENCY;
    let sleepSQIAwaken = result.SLEEP_SQI_AWAKEN;
    let sleepSQICycle = result.SLEEP_SQI_CYCLE;
    let sleepSQIStability = result.SLEEP_SQI_STABILITY;
    let sleepAnalysisTotal = result.SLEEP_ANALYSIS_TOTAL;
    let sleepAnalysisPoints = result.SLEEP_ANALYSIS_POINTS;
    let sleepSBW = result.SLEEP_STABILITY_BRAINWAVE;
    let sleepGTBT = result.SLEEP_GTBT;
    let date = result.DATE;

    sleepHypnogram.splice(0, 0, 0);
    this.storeSQIData(sleepSQIAwaken, sleepSQICycle, sleepSQIStability, sleepAnalysisTotal, sleepAnalysisPoints, sleepSBW, sleepGTBT,
      (analysisPoints, awaken, stability, cycle) => {
        // we need to execute the storage of the sleep document after the sqi data have been averaged/summed.
        getPreference('targetSleepHours')
        .then(prefRes => {
          let targetSleepHours = isNaN(parseInt(prefRes, 10)) ? 0 : parseInt(prefRes, 10);
          this.targetSleepHours = targetSleepHours;
          console.log('123curation storeSQIData() Callback analysisPoints: ' + analysisPoints);
          let doc = {
            _id: _id, user_id: this.user_id, sleep_sqi_cycle: cycle, sleep_analysis_points: analysisPoints,
            sleep_sqi_stability: stability, sleep_analysis_wake: sleepAnalysisWake,
            sleep_hypnogram: sleepHypnogram, sleep_analysis_latency: sleepAnalysisLatency,
            sleep_analysis_total: sleepAnalysisTotal, sleep_sqi_awaken: awaken, date: date,
            type: 'sleep-activity', target_hours: targetSleepHours,
            timestamp: DateUtil.getFormattedTimestamp(), channels: [this.user_id]
          };
          this.storeSleepData(doc);
        });
      });
  }

  initSleepMgr() {
    manager.database.get_db({ db: DB_NAME })
    .then(res => {
      console.log('sleep123 couchbase12: res: ' + JSON.stringify(res));
    });
    getPreference('userId')
    .then(res => {
      console.log('sleep123: getPreference userId: ' + res);
      this.user_id = res;
    });
  }

  setSleepListener() {
    if (this.handleStationSleepInfo === null) {
      console.log('sleep123 initSleepListener');
      this.handleStationSleepInfo = StationManagerEmitter.addListener('StationSleepInfoListener', this.handleSleepInfo);
    }
  }

  removeSleepListener() {
    console.log('sleep123 removeSleepListener:');
    this.handleStationSleepInfo.remove();
    this.handleStationSleepInfo = null;
  }

  storeSQIData(sleepSQIAwaken, sleepSQICycle, sleepSQIStability, sleepAnalysisTotal, sleepAnalysisPoints, sleepSBW, sleepGTBT, callback) {
    // we know that there's only one document per day, so we just use the first element of the targetCurationData array.
    let doc;
    let docs = [];

    this.setupViewAndQueryOfTargetData()
    .then(r => {
      let rows = r.obj.rows;

      for (let i = 0; i < rows.length; i++) {
        let modifiedDate = rows[i].doc.modified_date.split(' ')[0];
        let dateNow = new Date().getMonth() + 1 + '/' + new Date().getDate() + '/' + new Date().getFullYear();

        if (new Date(modifiedDate).getTime() === new Date(dateNow).getTime()) {
          docs.push(rows[i]);
        }
      }
      console.log('123sleep curationDocs: ' + JSON.stringify(docs));
      this.targetCurationData = docs;

      if (this.targetCurationData !== undefined && this.targetCurationData[0] !== undefined) {
        doc = this.targetCurationData[0].doc;
      }

      let sleepSQITotal = Math.round(sleepSQIAwaken * 0.7 + sleepSQICycle * 0.2 + sleepSQIStability * 0.1);
      let sleepSQIRM = Math.round(sleepSQIAwaken * 0.7 + sleepSQIStability * 0.1);
  
      let aveSBW = 0;
      let aveSQIAwaken = 0;
      let aveSQICycle = 0;
  
      let aveSQIStability = 0;
      let aveSQITotal = 0;
      let aveGTBT = 0;
  
      let aveSQIRM = 0;
      let aveAnalysisPoints = 0;
      let sleepTotal = 0;
  
      if (doc !== undefined) {
        console.log('storeSQIData: doc !== undefined');
        aveSBW = doc.sbw > 0 ? (sleepSBW + doc.sbw) / 2 : sleepSBW;
        aveSQIAwaken = doc.sqi_awaken > 0 ? (sleepSQIAwaken + doc.sqi_awaken) / 2 : sleepSQIAwaken;
        aveSQICycle = doc.sqi_cycle > 0 ? (sleepSQICycle + doc.sqi_cycle) / 2 : sleepSQICycle;
        aveSQIStability = doc.sqi_stability > 0 ? (sleepSQIStability + doc.sqi_stability) / 2 : sleepSQIStability;
        aveSQITotal = doc.sqi_total > 0 ? (sleepSQITotal + doc.sqi_total) / 2 : sleepSQITotal;
        aveSQIRM = doc.sqi_rm > 0 ? (sleepSQIRM + doc.sqi_rm) / 2 : sleepSQIRM;
        aveGTBT = doc.actual_gtbt > 0 ? (sleepGTBT + doc.actual_gtbt) / 2 : sleepGTBT;
        sleepTotal = doc.tst + sleepAnalysisTotal;
        aveAnalysisPoints = doc.sleep_analysis_points > 0 ? (sleepAnalysisPoints + doc.sleep_analysis_points) / 2 : sleepAnalysisPoints;
      } else {
        console.log('storeSQIData: doc === undefined');
        aveSBW = sleepSBW;
        aveSQIAwaken = sleepSQIAwaken;
        aveSQICycle = sleepSQICycle;
        aveSQIStability = sleepSQIStability;
        aveSQITotal = sleepSQITotal;
        aveSQIRM = sleepSQIRM;
        aveAnalysisPoints = sleepAnalysisPoints;
        aveGTBT = sleepGTBT;
        sleepTotal = sleepAnalysisTotal;
      }
  
      this.sbw = Math.round(aveSBW);
      this.sqiAwaken = Math.round(aveSQIAwaken);
      this.sqiCycle = Math.round(aveSQICycle);
      this.sqiRM = Math.round(aveSQIRM);
      this.sqiStability = Math.round(aveSQIStability);
      this.sqiTotal = Math.round(aveSQITotal);
      this.analysisPoints = Math.round(aveAnalysisPoints);
      this.gtbt = Math.round(aveGTBT);
      this.tst = Math.round(sleepTotal);
      console.log('123curation storeSQIData() analysisPoints: ' + this.analysisPoints);
      setTimeout(() => {
        this.upsertTargetDocument();
      }, 5);
      callback(Math.round(aveAnalysisPoints), Math.round(aveSQIAwaken), Math.round(sleepSQIStability), Math.round(aveSQICycle));
    });
  }

  upsertTargetDocument() {
    setTimeout(() => {
      let target = {
        sbw: this.sbw, sqi_awaken: this.sqiAwaken, sqi_cycle: this.sqiCycle,
        sqi_rm: this.sqiRM, sqi_stability: this.sqiStability, sqi_total: this.sqiTotal,
        actual_gtbt: this.gtbt, target_gtbt: this.targetSleepHours, modified_date: DateUtil.getFormattedTimestamp(),
        tst: this.tst, sleep_analysis_points: this.analysisPoints
      };

      let actualData = {
        sbw: this.sbw, sqi_awaken: this.sqiAwaken, sqi_cycle: this.sqiCycle,
        sqi_rm: this.sqiRM, sqi_stability: this.sqiStability, sqi_total: this.sqiTotal,
        actual_gtbt: this.gtbt, modified_date: DateUtil.getFormattedTimestamp(),
        tst: this.tst, sleep_analysis_points: this.analysisPoints
      };

      TargetDoc.upsertTargetDocument('sleep', target, () => {});
      CurationActualData.upsertTargetDocument('sleep', actualData, () => {});
    }, 5);
  }

  storeSleepData(newDoc) {
    // before we post the document to couchbase, we must first check our local
    // documents to see if a document with the same date as the one we're going
    // to pass is already there, if it's there, then we just need to use the same
    // document and modify its content
    console.log('123sleep document!: ' + JSON.stringify(newDoc));

    let now = new Date(getCurrentDate()).getTime();
    if (typeof manager.document !== 'undefined') {
      let doc = {};

      this.setupViewAndQueryLatestSleepDoc()
      .then(res => {
        if (res.obj.rows !== undefined) {
          let rows = res.obj.rows;
          // we search through the daily records to see if there's a document with equal current date
          // if there is, we grab that document and update it
          for (let i = 0; i < rows.length; i++) {
            let row = rows[i].doc;
            if (row !== undefined && new Date(row.date).getTime() === now) {
              doc = row;
              break;
            }
          }
          console.log('123sleep sleepDBRecords: ' + JSON.stringify(doc));
          getPreference('targetSleepHours')
          .then(prefRes => {
            let targetHours = isNaN(parseInt(prefRes, 10)) ? 0 : parseInt(prefRes, 10);
            if (doc._id !== undefined) {
              // update existing record
              console.log('123sleep updating a doc to couchbase, with id: ' + doc._id);
              let tHours = targetHours;

              if (targetHours === 0 && doc.target_hours > 0) {
                tHours = doc.target_hours;
              }

              console.log('123targetcals1: tHours: ' + tHours);
              console.log('123targetcals1: doc: ' + JSON.stringify(doc));
              let timestamp = doc.timestamp;
              doc = this.computeTotalHours(doc, newDoc);
              let rev = doc.rev;
              doc.timestamp = timestamp;
              doc.rev = undefined;
              doc.target_hours = tHours;
              doc.sleep_hypnogram = newDoc.sleep_hypnogram;
              manager.document.put({ db: DB_NAME, doc: doc._id, body: doc, rev: rev })
              .then(res => {
                console.log('sleep123: success in updating document: ' + JSON.stringify(res));
                if (Platform.OS === 'ios') { // debugging in release IPA
                  StationModule.printLog(`sleep123: success in updating document: ' ${JSON.stringify(res)}`);
                }
                this.onRefreshSleepData();
              })
              .catch(err => {
                console.log('sleep123-- error: ' + JSON.stringify(err));
              });
            } else {
              // we store a new document to couchbase...
              console.log('sleep123 ');
              doc = newDoc;
              doc.target_hours = targetHours;
              console.log('sleep123 storing a new doc to couchbase, with id: ' + doc._id);
              manager.document.post({ db: DB_NAME, body: doc })
              .then(res => {
                console.log('sleep123: success in posting document: ' + JSON.stringify(res));
                if (Platform.OS === 'ios') { // debugging in release IPA
                  StationModule.printLog(`sleep123: success in posting document: ${JSON.stringify(res)}`);
                }
                this.onRefreshSleepData();
              })
              .catch(err => {
                console.log('sleep123: error in posting document to couchbase: ' + err);
              });
            }
          });
        }
      })
      .catch(err => {
        console.log('couchbase123 err: ' + JSON.stringify(err));
      });
    }
  }

  onRefreshSleepData() {
    if (this.refreshSleepCallback !== undefined && this.refreshSleepCallback !== null) {
      this.refreshSleepCallback();
    }
  }

  onStoreSQIData(aveSBW, aveSQIAwaken, aveSQICycle, aveSQIRM, aveSQIStability, aveSQITotal, aveAnalysisPoints, sleepTotal) {
    if (this.onStoreSQICallback !== undefined && this.onStoreSQICallback !== null) {
      this.onStoreSQICallback(aveSBW, aveSQIAwaken, aveSQICycle, aveSQIRM, aveSQIStability, aveSQITotal, aveAnalysisPoints, sleepTotal);
    }
  }

  setRefreshSleepCallback(refreshSleepCallback) {
    this.refreshSleepCallback = refreshSleepCallback;
  }

  computeTotalHours(docToUpdate, newDoc) {
    console.log('123sleep: docToUpdate: ' + JSON.stringify(docToUpdate) + '\tnewDoc: ' + JSON.stringify(newDoc));
    // we need to get the total of the following data: "SLEEP_ANALYSIS_WAKE", "SLEEP_ANALYSIS_LATENCY", "SLEEP_ANALYSIS_TOTAL"
    let sleepAnalysisWake = newDoc.sleep_analysis_wake + docToUpdate.sleep_analysis_wake;
    let sleepAnalysisLatency = newDoc.sleep_analysis_latency + docToUpdate.sleep_analysis_latency;
    let sleepAnalysisTotal = newDoc.sleep_analysis_total + docToUpdate.sleep_analysis_total;

    docToUpdate.sleep_analysis_wake = sleepAnalysisWake;
    docToUpdate.sleep_analysis_latency = sleepAnalysisLatency;
    docToUpdate.sleep_analysis_total = sleepAnalysisTotal;

    console.log('123sleep latest docToUpdate: ' + JSON.stringify(docToUpdate));
    return docToUpdate;
  }

  setupViewAndQueryOfTargetData() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'targetDataByDocId',
      include_docs: true
    });
  }

  setupViewAndQueryLatestSleepDoc() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'sleepWithinToday',
      include_docs: true
    });
  }
}

export default SleepManager;
