import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Switch, ScrollView, Dimensions, DeviceEventEmitter } from 'react-native';
import { widgets } from '../../styles/widgets';
import Modal from 'react-native-modal';
import I18n from '../../translate/i18n/i18n';
import { manageItems } from '../../redux/actions';
import { connect } from 'react-redux';
import { COLOR_THUMB_TINT, COLOR_TINT_ON } from '../../styles/colors';
import { normalize } from 'react-native-elements';

const { height } = Dimensions.get('window');

class MainItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
      // showBrainWave: true,
      // showBinaural: true,
      // showAlarm: true,
      // showFood: true,
      // showWater: true,
      // showCaffeine: true,
      // showAlcohol: false,
      // showCigarette: false
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }


  render() {
    const { itemContainer, boxContainer, itemWrapper, itemText, itemIcon, circleShape, manageItemIcon } = widgets;

    return (
      <View style={[{ marginBottom: 10, marginTop: 5 }]}>
        <ScrollView>
          <View style={itemContainer}>
            {
              this.props.showBrainWave &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 0);
                }}>
                <Image style={itemIcon} source={require('../../images/manage_items_icons/brainwave_128.png')} />
                <Text style={itemText}>{I18n.t('BRAINWAVE')}</Text>
              </TouchableOpacity>
            }
            {
              this.props.showBinaural &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 1);
                }}>
                <Image style={itemIcon} source={require('../../images/manage_items_icons/binaural-beat_128.png')} />
                <Text style={itemText}>{I18n.t('BINAURAL_BEAT')}</Text>
              </TouchableOpacity>
            }
            {
              this.props.showAlarm &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 2);
                }}>
                <Image style={itemIcon} source={require('../../images/manage_items_icons/alarm_128.png')} />
                <Text style={itemText}>{I18n.t('ALARM')}</Text>
              </TouchableOpacity>
            }
            {
              this.props.showFood &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 3);
                }}
              >
                <Image style={itemIcon} source={require('../../images/manage_items_icons/food_128.png')} />
                <Text style={itemText}>{I18n.t('FOOD')}</Text>
              </TouchableOpacity>
            }
            {
              this.props.showWater &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 4);
                }}
              >
                <Image style={itemIcon} source={require('../../images/manage_items_icons/water_128.png')} />
                <Text style={itemText}>{I18n.t('WATER')}</Text>
              </TouchableOpacity>
            }
            {
              this.props.showCaffeine &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 5);
                }}
              >
                <Image style={itemIcon} source={require('../../images/manage_items_icons/caffeine_128.png')} />
                <Text style={itemText}>{I18n.t('CAFFEINE')}</Text>
              </TouchableOpacity>
            }
            {
              this.props.showAlcohol &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 6);
                }}
              >
                <Image style={itemIcon} source={require('../../images/manage_items_icons/alcohol_128.png')} />
                <Text style={itemText}>{I18n.t('ALCOHOL')}</Text>
              </TouchableOpacity>
            }
            {
              this.props.showCigarette &&
              <TouchableOpacity style={[boxContainer, itemWrapper]}
                onPress={() => {
                  DeviceEventEmitter.emit('openGraph', 7);
                }}
              >
                <Image style={itemIcon} source={require('../../images/manage_items_icons/cigarette_128.png')} />
                <Text style={itemText}>{I18n.t('CIGARETTE')}</Text>
              </TouchableOpacity>
            }
          </View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: 4 }}>
            <TouchableOpacity style={circleShape}
              onPress={() => {
                this.setModalVisible(true);
              }}>
              <Image style={{ margin: 8, height: 36, width: 36 }} source={require('../../images/manage_items_icons/plus.png')} />
            </TouchableOpacity>
          </View>
          <Text style={[itemText]}>{I18n.t('MANAGE_ITEMS')}</Text>
        </ScrollView>
        <Modal
          animationIn='zoomIn'
          animationOut='zoomOut'
          onBackButtonPress={() => {
            this.setModalVisible(false);
          }}
          isVisible={this.state.modalVisible}>
          <View style={{ flex: 1, backgroundColor: '#ffffff', borderRadius: 10 }}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: 12, paddingBottom: 28, paddingLeft: 25, paddingRight: 15 }}>
              <View style={{ flex: 1, marginTop: 20, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20, paddingBottom: 10 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/brainwave_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('BRAINWAVE')} {I18n.t('DEFAULT')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    disabled={true}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showBrainWave}
                    onValueChange={value => this.props.manageItems({ prop: 'showBrainWave', value })}
                  />
                </View>
              </View>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/binaural-beat_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('BINAURAL')} {I18n.t('DEFAULT')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    disabled={true}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showBinaural}
                    onValueChange={value => this.props.manageItems({ prop: 'showBinaural', value })}
                  />
                </View>
              </View>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/alarm_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('ALARM')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showAlarm}
                    onValueChange={value => this.props.manageItems({ prop: 'showAlarm', value })}
                  />
                </View>
              </View>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/food_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('FOOD')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showFood}
                    onValueChange={value => this.props.manageItems({ prop: 'showFood', value })}
                  />
                </View>
              </View>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/water_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('WATER')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showWater}
                    onValueChange={value => this.props.manageItems({ prop: 'showWater', value })}
                  />
                </View>
              </View>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/caffeine_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('CAFFEINE')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showCaffeine}
                    onValueChange={value => this.props.manageItems({ prop: 'showCaffeine', value })}
                  />
                </View>
              </View>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/alcohol_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('ALCOHOL')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showAlcohol}
                    onValueChange={value => this.props.manageItems({ prop: 'showAlcohol', value })}
                  />
                </View>
              </View>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ marginRight: 20 }}>
                  <Image style={manageItemIcon} source={require('../../images/manage_items_icons/cigarette_128.png')} />
                </View>
                <View style={{ flex: height < 640 ? .90 : .86, alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Text style={{ fontSize: normalize(12) }}>{I18n.t('CIGARETTE')}</Text>
                </View>
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                  <Switch style={widgets.switch}
                    thumbTintColor={COLOR_THUMB_TINT}
                    onTintColor={COLOR_TINT_ON}
                    value={this.props.showCigarette}
                    onValueChange={value => this.props.manageItems({ prop: 'showCigarette', value })}
                  />
                </View>
              </View>
            </View>
          </View>
          <TouchableOpacity
            style={{ position: 'absolute', right: 10, top: 10 }}
            onPress={() => { this.setModalVisible(!this.state.modalVisible) }}
          >
            <View style={{ borderRadius: 50, borderWidth: 1, paddingLeft: 10, paddingRight: 10, paddingTop: 3.5, paddingBottom: 3.5, backgroundColor: '#fff' }}>
              <Text style={{ fontSize: 18 }}>X</Text>
            </View>
          </TouchableOpacity>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const {
    showBrainWave, showBinaural, showAlarm, showFood, showWater, showCaffeine, showAlcohol, showCigarette
  } = state.mainItems;
  return {
    showBrainWave, showBinaural, showAlarm, showFood, showWater, showCaffeine, showAlcohol, showCigarette
  };
};

export default connect(mapStateToProps, { manageItems })(MainItems);
