import React from 'react';
import {
  Button,
  Modal,
  TextInput,
  Platform,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import PropTypes from 'prop-types';
import { containers } from './../../styles/containers';
import { widgets } from '../../styles/widgets';
import I18n from '../../translate/i18n/i18n';

const SetTargetModal = ({ onSetTarget, isModalVisible, inputChange, closeModal, maxLength, buttonTitle, placeholder }) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      style={widgets.modal}
      visible={isModalVisible}
      onRequestClose={closeModal}>
      <TouchableWithoutFeedback onPressIn={closeModal}>
        <View style={containers.setTargetCon}>
          <TouchableWithoutFeedback onPressIn={() => { }}>
            <View style={containers.targetTextInputCon}>
              <View style={{
                ...Platform.select({
                  ios: {
                    borderStyle: 'solid', borderBottomColor: 'black', borderBottomWidth: 2
                  }
                })
              }}>
                <TextInput keyboardType='numeric' placeholder={placeholder} maxLength={maxLength} onChangeText={inputChange} onSubmitEditing={onSetTarget} />
              </View>
              <Button title={buttonTitle} onPress={onSetTarget} />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

SetTargetModal.PropTypes = {
  closeModal: PropTypes.func.isRequired,
  onChangeTarget: PropTypes.func.isRequired,
  onSetTarget: PropTypes.func.isRequired,
  isModalVisible: PropTypes.bool.isRequired,
  inputChange: PropTypes.func.isRequired,
  maxLength: PropTypes.func.isRequired,
  buttonTitle: PropTypes.func.isRequired,
  placeholder: PropTypes.func.isRequired
};

export default SetTargetModal;
