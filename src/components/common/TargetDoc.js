import * as DateUtil from '../../utils/DateUtils';
import { getPreference } from '../../utils/CommonMethods';

let uuidv4 = require('uuid/v4');

let TARGET = {
  channels: [],
  created_date: DateUtil.getFormattedTimestamp(),
  sleep_analysis_points: 0,
  sbw: 0,
  tst: 0,
  actual_gtbt: 0,
  sun: 0,
  actual_walk: 0,
  actual_coffee: 0,
  actual_smoke: 0,
  actual_drink: 0,
  bb3Hz: 0,
  bb6Hz: 0,
  bb9Hz: 0,
  bb12Hz: 0,
  bb: 0,
  index_of_bb: 0,
  index_of_position: -1,
  sqi_awaken: 0,
  sqi_cycle: 0,
  sqi_stability: 0,
  sqi_total: 0,
  sqi_rm: 0,
  type: 'curation',
  _id: uuidv4(),
  target_drink: 0,
  target_smoke: 0,
  target_coffee: 0,
  target_walk: 0,
  target_gtbt: 0,
  target_bb: 0,
  target_food: 0,
  target_water: 0,
  modified_date: DateUtil.getFormattedTimestamp()
};

export default {
  getDocumentByCurrentDate(rows) {
    for (let i = 0; i < rows.length; i++) {
      let modifiedDate = rows[i].doc.modified_date.split(' ')[0];
      let dateNow = new Date().getMonth() + 1 + '/' + new Date().getDate() + '/' + new Date().getFullYear();

      if (new Date(modifiedDate).getTime() === new Date(dateNow).getTime()) {
        console.log('123curation: returning row...');
        return rows[i];
      }
    }

    return null;
  },
  getTargetConst() {
    return TARGET;
  },
  setupCurationViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'curationByDocId',
      include_docs: true
    });
  },
  // Note: upsertTargetDocument will require two params:
  // 1st is the recordToUpdate. Values can be activity, sleep, water, caffeine, binaural, etc...
  // 2nd is an object containing the target values to be updated, i.e. for activity:
  // { actualWalk: 500, targetWalk: 500, modified_date: '03/05/2018 20:07:09' }

  upsertTargetDocument(recordToUpdate, target, callback) {
    this.setupCurationViewAndQuery().then(res => {
      let rows = res.obj.rows;
      let row = this.getDocumentByCurrentDate(rows) !== null ? this.getDocumentByCurrentDate(rows).doc : null;
      let shouldUpdate = false;

      switch (recordToUpdate) {
        case 'activity':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_walk = target.actual_walk;
            row.target_walk = target.target_walk;
            row.modified_date = target.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            TARGET.actual_walk = target.actual_walk;
            TARGET.target_walk = target.target_walk;
            row = TARGET;
            shouldUpdate = false;
          }
          break;
        case 'sleep':
          if (row !== null) {
            // document for today has been created, we'll just update it.

            row.sbw = target.sbw;
            row.sqi_awaken = target.sqi_awaken;
            row.sqi_cycle = target.sqi_cycle;
            row.sqi_rm = target.sqi_rm;
            row.sqi_stability = target.sqi_stability;
            row.sqi_total = target.sqi_total;
            row.actual_gtbt = target.actual_gtbt;
            row.target_gtbt = target.target_gtbt;
            row.modified_date = target.modified_date;
            row.tst = target.tst;
            row.sleep_analysis_points = target.sleep_analysis_points;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            TARGET.sbw = target.sbw;
            TARGET.sqi_awaken = target.sqi_awaken;
            TARGET.sqi_cycle = target.sqi_cycle;
            TARGET.sqi_rm = target.sqi_rm;
            TARGET.sqi_stability = target.sqi_stability;
            TARGET.sqi_total = target.sqi_total;
            TARGET.actual_gtbt = target.actual_gtbt;
            TARGET.target_gtbt = target.target_gtbt;
            TARGET.modified_date = target.modified_date;
            TARGET.tst = target.tst;
            TARGET.sleep_analysis_points = target.sleep_analysis_points;
            row = TARGET;
            shouldUpdate = false;
          }
          break;

        case 'caffeine':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_coffee = target.actual_coffee;
            row.target_coffee = target.target_coffee;
            row.modified_date = target.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            TARGET.actual_coffee = target.actual_coffee;
            TARGET.target_coffee = target.target_coffee;
            row = TARGET;
            shouldUpdate = false;
          }
          break;


        case 'alcohol':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_drink = target.actual_drink;
            row.target_drink = target.target_drink;
            row.modified_date = target.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            TARGET.actual_drink = target.actual_drink;
            TARGET.target_drink = target.target_drink;
            row = TARGET;
            shouldUpdate = false;
          }
          break;

        case 'cigarette':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_smoke = target.actual_smoke;
            row.target_smoke = target.target_smoke;
            row.modified_date = target.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            TARGET.actual_smoke = target.actual_smoke;
            TARGET.target_smoke = target.target_smoke;
            row = TARGET;
            shouldUpdate = false;
          }
          break;
        case 'binaural':
          if (row !== null) {
            row.bb = target.bb;
            row.bb6Hz = target.bb;
            row.index_of_bb = target.index_of_bb;
            row.modified_date = target.modified_date;
            shouldUpdate = true;
          } else {
            TARGET.channels = [res];
            TARGET.bb = target.bb;
            TARGET.bb6Hz = target.bb;
            TARGET.index_of_bb = target.index_of_bb;
            row = TARGET;
            shouldUpdate = false;
          }
          break;
        default:
          console.log('123curation default.');
          break;
      }

      getPreference('userId').then(userId => {
        row.channels = [userId];
        row.user_id = userId;

        if (shouldUpdate) {
          manager.document.put({ db: DB_NAME, doc: row._id, body: row, rev: row._rev }).then(updateRes => {
            // we then update our dateAndDocMap stored in redux.
            console.log('123curation res of updated: ' + JSON.stringify(updateRes));
            if (callback !== undefined) {
              callback();
            }
          }).catch(err => {
            console.log('123curation: error in putting document to couchbase: ' + JSON.stringify(err));
          });
        } else {
          manager.document.post({ db: DB_NAME, body: row }).then(saveRes => {
            console.log('123curation res of saved: ' + JSON.stringify(saveRes));
            if (callback !== undefined) {
              callback();
            }
          }).catch(err => {
            console.log('123curation: error in posting document to couchbase: ' + JSON.stringify(err));
          });
        }
      });
    }).catch(err => {
      console.log('123curation err: ' + JSON.stringify(err));
    });
  }
};


