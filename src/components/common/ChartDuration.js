import React, { Component } from 'react';
import { View, TouchableHighlight, Text } from 'react-native';
import { widgets } from './../../styles/widgets';
import { containers } from './../../styles/containers';
import { text } from './../../styles/text';
import PropTypes from 'prop-types';
import I18n from './../../translate/i18n/i18n';

var pressStatusDaily = true;
var pressStatusWeekly = false;
var pressStatusMonthly = false;
var pressStatusYearly = false;

class ChartDuration extends Component {
  constructor(props) {
    super(props);
    pressStatusDaily = true;
    pressStatusWeekly = false;
    pressStatusMonthly = false;
    pressStatusYearly = false;
  }

  static setDailyTargets() {
    pressStatusDaily = true;
    pressStatusWeekly = false;
    pressStatusMonthly = false;
    pressStatusYearly = false;
  }


  _onShowUnderlay(buttonClicked) {
    if (buttonClicked === 'daily') {
      pressStatusDaily = true;
      pressStatusWeekly = false;
      pressStatusMonthly = false;
      pressStatusYearly = false;

    }
    else if (buttonClicked === 'weekly') {
      pressStatusDaily = false;
      pressStatusWeekly = true;
      pressStatusMonthly = false;
      pressStatusYearly = false;

    }
    else if (buttonClicked === 'monthly') {
      pressStatusDaily = false;
      pressStatusWeekly = false;
      pressStatusMonthly = true;
      pressStatusYearly = false;
    }
    else {
      pressStatusDaily = false;
      pressStatusWeekly = false;
      pressStatusMonthly = false;
      pressStatusYearly = true;
    }

  }

  render() {


    return (
      <View style={containers.durationContainer}>
        <View style={widgets.buttonDuration}>
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={1}
            onPress={this.props.onPressDaily}
            onShowUnderlay={() => this._onShowUnderlay('daily')}>
            <Text style={pressStatusDaily ? text.durationSelectedText : text.durationText}>{I18n.t('DAILY')}</Text>
          </TouchableHighlight>
        </View>
        <View style={widgets.buttonDuration}>
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={1}
            onPress={this.props.onPressWeekly}
            onShowUnderlay={() => this._onShowUnderlay('weekly')}>
            <Text style={pressStatusWeekly ? text.durationSelectedText : text.durationText}>{I18n.t('WEEKLY')}</Text>
          </TouchableHighlight>
        </View>
        <View style={widgets.buttonDuration}>
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={1}
            onPress={this.props.onPressMonthly}
            onShowUnderlay={() => this._onShowUnderlay('monthly')}>
            <Text style={pressStatusMonthly ? text.durationSelectedText : text.durationText}>{I18n.t('MONTHLY')}</Text>
          </TouchableHighlight>
        </View>
        <View style={widgets.buttonDuration}>
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={1}
            onPress={this.props.onPressYearly}
            onShowUnderlay={() => this._onShowUnderlay('yearly')}>
            <Text style={pressStatusYearly ? text.durationSelectedText : text.durationText}>{I18n.t('YEARLY')}</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

ChartDuration.propTypes = {
  onPressDaily: PropTypes.func.isRequired,
  onPressWeekly: PropTypes.func.isRequired,
  onPressMonthly: PropTypes.func.isRequired,
  onPressYearly: PropTypes.func.isRequired
};

export default ChartDuration;
