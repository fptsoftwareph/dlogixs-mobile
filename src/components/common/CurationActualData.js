import * as DateUtil from '../../utils/DateUtils';
import { getPreference } from '../../utils/CommonMethods';

let uuidv4 = require('uuid/v4');

let ACTUAL = {
  channels: [],
  created_date: DateUtil.getFormattedTimestamp(),
  sleep_analysis_points: 0,
  sbw: 0,
  tst: 0,
  actual_gtbt: 0,
  sun: 0,
  actual_walk: 0,
  actual_coffee: 0,
  actual_smoke: 0,
  actual_drink: 0,
  bb3Hz: 0,
  bb6Hz: 0,
  bb9Hz: 0,
  bb12Hz: 0,
  bb: 0,
  index_of_bb: 0,
  index_of_position: -1,
  sqi_awaken: 0,
  sqi_cycle: 0,
  sqi_stability: 0,
  sqi_total: 0,
  sqi_rm: 0,
  type: 'curation_actual',
  _id: uuidv4(),
  modified_date: DateUtil.getFormattedTimestamp()
};

export default {
  setupCurationViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'curationActualDataByDocId',
      include_docs: true
    });
  },
  // Note: upsertTargetDocument will require two params:
  // 1st is the recordToUpdate. Values can be activity, sleep, water, caffeine, binaural, etc...
  // 2nd is an object containing the target values to be updated, i.e. for activity:
  // { actualWalk: 500, modified_date: '03/05/2018 20:07:09' }

  upsertTargetDocument(recordToUpdate, data, callback) {
    console.log('upsertTargetDocument recordToUpdate: ' + JSON.stringify(recordToUpdate));
    console.log('upsertTargetDocument data: ' + JSON.stringify(data));

    this.setupCurationViewAndQuery().then(res => {
      console.log('upsertTargetDocument res: ' + JSON.stringify(res));
      console.log('upsertTargetDocument res length: ' + res.obj.rows.length);
      
      let row = res.obj.rows.length > 0 ? res.obj.rows[0].doc : null;
      let shouldUpdate = false;

      switch (recordToUpdate) {
        case 'activity':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_walk = data.actual_walk;
            row.modified_date = data.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            ACTUAL.actual_walk = data.actual_walk;
            row = ACTUAL;
            shouldUpdate = false;
          }
          break;
        case 'sleep':
          if (row !== null) {
            // document for today has been created, we'll just update it.

            row.sbw = data.sbw;
            row.sqi_awaken = data.sqi_awaken;
            row.sqi_cycle = data.sqi_cycle;
            row.sqi_rm = data.sqi_rm;
            row.sqi_stability = data.sqi_stability;
            row.sqi_total = data.sqi_total;
            row.actual_gtbt = data.actual_gtbt;
            row.modified_date = data.modified_date;
            row.tst = data.tst;
            row.sleep_analysis_points = data.sleep_analysis_points;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            ACTUAL.sbw = data.sbw;
            ACTUAL.sqi_awaken = data.sqi_awaken;
            ACTUAL.sqi_cycle = data.sqi_cycle;
            ACTUAL.sqi_rm = data.sqi_rm;
            ACTUAL.sqi_stability = data.sqi_stability;
            ACTUAL.sqi_total = data.sqi_total;
            ACTUAL.actual_gtbt = data.actual_gtbt;
            ACTUAL.modified_date = data.modified_date;
            ACTUAL.tst = data.tst;
            ACTUAL.sleep_analysis_points = data.sleep_analysis_points;
            row = ACTUAL;
            shouldUpdate = false;
          }
          break;

        case 'caffeine':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_coffee = data.actual_coffee;
            row.modified_date = data.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            ACTUAL.actual_coffee = data.actual_coffee;
            row = ACTUAL;
            shouldUpdate = false;
          }
          break;


        case 'alcohol':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_drink = data.actual_drink;
            row.modified_date = data.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            ACTUAL.actual_drink = data.actual_drink;
            row = ACTUAL;
            shouldUpdate = false;
          }
          break;

        case 'cigarette':
          if (row !== null) {
            // document for today has been created, we'll just update it.
            row.actual_smoke = data.actual_smoke;
            row.modified_date = data.modified_date;
            shouldUpdate = true;
          } else {
            // no document for today, we need to create a new one.
            ACTUAL.actual_smoke = data.actual_smoke;
            row = ACTUAL;
            shouldUpdate = false;
          }
          break;
        case 'binaural':
          if (row !== null) {
            row.bb = data.bb;
            row.bb6Hz = data.bb;
            row.index_of_bb = data.index_of_bb;
            row.modified_date = data.modified_date;
            shouldUpdate = true;
          } else {
            ACTUAL.channels = [res];
            ACTUAL.bb = data.bb;
            ACTUAL.bb6Hz = data.bb;
            ACTUAL.index_of_bb = data.index_of_bb;
            row = ACTUAL;
            shouldUpdate = false;
          }
          break;
        default:
          console.log('123curation_actual default.');
          break;
      }

      getPreference('userId').then(userId => {
        row.channels = [userId];
        row.user_id = userId;

        if (shouldUpdate) {
          manager.document.put({ db: DB_NAME, doc: row._id, body: row, rev: row._rev }).then(updateRes => {
            // we then update our dateAndDocMap stored in redux.
            console.log('123curation_actual res of updated: ' + JSON.stringify(updateRes));
            if (callback !== undefined) {
              callback();
            }
          }).catch(err => {
            console.log('123curation_actual: error in putting document to couchbase: ' + JSON.stringify(err));
          });
        } else {
          manager.document.post({ db: DB_NAME, body: row }).then(saveRes => {
            console.log('123curation_actual res of saved: ' + JSON.stringify(saveRes));
            if (callback !== undefined) {
              callback();
            }
          }).catch(err => {
            console.log('123curation_actual: error in posting document to couchbase: ' + JSON.stringify(err));
          });
        }
      });
    }).catch(err => {
      console.log('123curation_actual err: ' + JSON.stringify(err));
    });
  }
};


