import React, { Component } from 'react';
import {
  View, Text, Modal, TouchableHighlight,
  ListView, TouchableOpacity, ActivityIndicator,
  TouchableWithoutFeedback
} from 'react-native';
import { widgets } from '../../styles/widgets';
import RenderIf from '../RenderIf';
import I18n from './../../translate/i18n/i18n';
import { ConnectAccessPointPrompt } from './ConnectAccessPointPrompt';
import { NETWORK_NAME } from '../../utils/DataConstants';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class AccessPointsListing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      isToConnectAccessPointPrompt: false,
      item: {},
      animating: true,
      scanning: false,
      connectedAccessPoint: '',
      accessPtsArray: new Map(),
      ssid: ''
    };

    this.openAccessPointsListing = this.props.openAccessPointsListing;
    this.updateSettingsItemModule = this.props.updateSettingsItemModule;
    this.scanAccessPoints = this.props.scanAccessPoints;
    this.setSettingsModuleSwitchMode = this.props.setSettingsModuleSwitchMode;

    this.connectAccessPointOnPress = this.connectAccessPointOnPress.bind(this);
    this.openConnectAccessPointPrompt = this.openConnectAccessPointPrompt.bind(this);

    this.dismissOutside = this.dismissOutside.bind(this);
  }

  dismissOutside() {
    if (this.state.connectedAccessPoint === I18n.t('NETWORK_NAME')) {
      this.updateSettingsItemModule('isWstationHeadSetEnabled', false);
    }
    this.setModalVisible(false);
  }

  connectAccessPointOnPress() {

  }

  render() {
    //  console.log('render');

    const list = Array.from(this.state.accessPtsArray);
    const dataSource = ds.cloneWithRows(list);

    const animating = this.state.animating;

    return (
      <View>
        <Modal animationType="slide"
          transparent={true}
          style={widgets.modal}
          visible={this.state.isVisible}
          presentation
          onRequestClose={this.dismissOutside}>

          <TouchableWithoutFeedback onPressIn={this.dismissOutside}>
            <View style={[widgets.bluetoothScanPromptCon]}>
              <TouchableWithoutFeedback onPressIn={() => { }}>

                <View style={[widgets.bluetoothScanPrompt, { marginTop: 150, marginBottom: 150 }]}>

                  <View style={widgets.commonFlex}>
                    <View style={widgets.commonConRow}>
                      <View style={[widgets.commonFlex, widgets.commonPaddingSmall,
                      { paddingLeft: 3, backgroundColor: '#bbbcbb', flex: 3 }]}>
                        <Text style={{ fontWeight: 'bold' }}>{I18n.t('AVAILABLE_WIFI')}</Text>
                      </View>
                      <View style={[widgets.alignRightViewWithNoMargin, widgets.commonPaddingSmall,
                      { backgroundColor: '#3bafda', alignItems: 'center', flex: 1.3, justifyContent: 'center' }]}>
                        {RenderIf(this.state.scanning)(
                          <View style={[widgets.commonConRow]}>
                            <Text style={{ color: 'white' }}>{I18n.t('SCANNING')}</Text>
                            <ActivityIndicator
                              animating={animating}
                              color='white'
                              size="small" />
                          </View>
                        )}
                        {RenderIf(!this.state.scanning)(
                          <TouchableOpacity onPress={() => {
                            this.scanAccessPoints();
                          }}>
                            <Text style={{ color: 'white' }}>{I18n.t('SCAN')}</Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>

                    <ListView
                      enableEmptySections={true}
                      style={{ marginTop: 5, marginBottom: 8 }}
                      dataSource={dataSource}
                      removeClippedSubviews={false}
                      showsVerticalScrollIndicator={true}
                      renderRow={(item) => {
                        return (
                          <TouchableHighlight onPress={() => {
                            this.setState({ ssid: item.SSID });
                            this.openConnectAccessPointPrompt(true);
                            this.openAccessPointsListing(false);
                          }}>
                            <View style={{ backgroundColor: 'white', padding: 5 }}>
                              <Text style={{ marginLeft: 20, fontSize: 14, color: '#333333' }}>
                                {item.SSID}</Text>
                            </View>
                          </TouchableHighlight>
                        );
                      }} />

                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
        <ConnectAccessPointPrompt
          visibility={this.state.isToConnectAccessPointPrompt}
          openConnectAccessPointPrompt={this.openConnectAccessPointPrompt}
          updateSettingsItemModule={this.updateSettingsItemModule}
          connectedAccessPoint={this.props.connectedAccessPoint}
          ssid={this.state.ssid}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode} />
      </View>
    );
  }

  openConnectAccessPointPrompt(visibility) {
    this.setState({ isToConnectAccessPointPrompt: visibility });
    console.log('isToConnectAccessPointPrompt: ' + this.state.isToConnectAccessPointPrompt);
  }

  componentWillMount() {
    //  console.log('AccessPointsListing componentWillMount');
  }

  componentDidMount() {
    //  console.log('AccessPointsListing componentDidMount');
  }

  componentWillReceiveProps(nextProps) {
    /*  console.log('AccessPointsListing componentWillReceiveProps');
    console.log('AccessPointsListing visibility: ' + nextProps.visibility);
    console.log('AccessPointsListing accessPtsArray: ' + JSON.stringify(nextProps.accessPtsArray));
    console.log('AccessPointsListing accessPtsArray stringify: ' + JSON.stringify(nextProps.accessPtsArray));*/

    this.setState({
      isVisible: nextProps.visibility,
      scanning: nextProps.scanning,
      connectedAccessPoint: nextProps.connectedAccessPoint,
      accessPtsArray: nextProps.accessPtsArray
    });
  }

  setModalVisible(visibility) {
    if (this.props.scanning) {
      this.props.stopScan();
    }

    this.setState({
      isVisible: visibility,
      accessPtsArray: new Map()
    });
  }
}

export { AccessPointsListing };
