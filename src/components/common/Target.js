import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import { containers } from './../../styles/containers';
import { text } from './../../styles/text';

const Target = ({ onPressAdd, onPressSubtract, targetVal, onTextPress }) => {
  return (
    <View style={containers.trophyContainer}>
      <TouchableOpacity onPress={onPressSubtract}>
        <Image style={{ height: 23, width: 23, marginLeft: 15 }} source={require('../../images/minus32.png')} />
      </TouchableOpacity>
      <Image style={{ height: 23, width: 23, marginLeft: 15 }} source={require('../../images/target_set32.png')} />
      <Text onPress={onTextPress} style={text.targetVal}>{targetVal}</Text>
      <TouchableOpacity onPress={onPressAdd}>
        <Image style={{ height: 23, width: 23, marginRight: 15 }} source={require('../../images/plus32.png')} />
      </TouchableOpacity>
    </View>
  );
};

Target.PropTypes = {
  onPressAdd: PropTypes.func.isRequired,
  onPressSubtract: PropTypes.func.isRequired,
  onTextPress: PropTypes.func.isRequired,
  targetVal: PropTypes.string.isRequired
};

export default Target;
