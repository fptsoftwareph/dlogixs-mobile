import React from 'react';
import { Icon } from 'react-native-elements';
import { TouchableOpacity } from 'react-native';
import { ARROW_BACK_WHITE } from '../../styles/colors';
import { widgets } from '../../styles/widgets';
import { Actions } from 'react-native-router-flux';

const MenuIconBack = () => {
  return (
    <TouchableOpacity style={widgets.arrowBack} onPress={() => { 
      Actions.pop();
    }}>
      <Icon
        iconStyle={{ marginTop: 10 }}
        color={ARROW_BACK_WHITE}
        size={50}
        type='ionicon'
        name='ios-arrow-round-back'/>
    </TouchableOpacity>
  );
};

export default MenuIconBack;
