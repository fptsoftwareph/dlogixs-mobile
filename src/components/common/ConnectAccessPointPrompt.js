import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Modal, TextInput, ScrollView, Platform } from 'react-native';
import { widgets } from '../../styles/widgets';
import { text } from '../../styles/text';
import I18n from './../../translate/i18n/i18n';
import { WIFI_STATION, IS_WIFI_STATION_ENABLED, 
  BT_ADAPTER_NOT_INIT, DEVICE_DOESNT_SUPPORT_UART, NETWORK_NAME } from '../../utils/DataConstants';
import { containers } from '../../styles/containers';
import RenderIf from '../RenderIf';
import { StationModule } from 'nb_native_modules';
import { showAlert } from '../../utils/CommonMethods';
import Toast from 'react-native-simple-toast';

class ConnectAccessPointPrompt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      connectedAccessPoint: '',
      ssid: '',
      password: ''
    };

    this.setSettingsModuleSwitchMode = this.props.setSettingsModuleSwitchMode;
    this.openConnectAccessPointPrompt = this.props.openConnectAccessPointPrompt;
    this.updateSettingsItemModule = this.props.updateSettingsItemModule;

    this.connectWifi = this.connectWifi.bind(this);
  }

  connectWifi(ssid, password) {
    StationModule.setWifi(ssid, password, result => {
      if (typeof result === 'string') {
        if (result.indexOf(BT_ADAPTER_NOT_INIT) !== -1 |
          result.indexOf(DEVICE_DOESNT_SUPPORT_UART) !== -1) {
          this.connectStationAndEmitEvent(ssid, password);
        } else {
          showAlert('', result);
        }
      } else {
        if (result === 1) { // data = 1 for success; data = 0 for failed
          this.setAccessPointAndEnableWifiStation(ssid, password);
        } else {
          showAlert('', I18n.t('STATION_FAILED_CONNECT_TO_WIFI'));
        }
      }
    });
  }

  connectStationAndEmitEvent(ssid, password) {
    console.log('connectStationAndEmitEvent cred: ' + ssid + " | " + password);
    Toast.show(I18n.t('CONNECTING_STATION'), Toast.SHORT);
    StationModule.connectStation(this.props.connectedPeripheralId, response2 => {
    console.log('connectStationAndEmitEvent response2: ' + response2);
      if (response2) {
        setTimeout(() => {
          this.setWifi(ssid, password);
        }, 6000);
      } else {
        Toast.show(response2, Toast.SHORT);
      }
    });
  }

  setWifi(ssid, password) {
    console.log('setWifi cred: ' + ssid + " | " + password);
    StationModule.setWifi(ssid, password, result => {
      console.log('setWifi result: ' + result);
      if (typeof result === 'string') {
        showAlert('', I18n.t('RECONNECT_STATION'));
      } else {
        if (result === 1) { // data = 1 for success; data = 0 for failed
          this.setAccessPointAndEnableWifiStation(ssid, password);
        } else {
          showAlert('', I18n.t('STATION_FAILED_CONNECT_TO_WIFI'));
        }
      }
    });
  }

  setAccessPointAndEnableWifiStation(ssid, password) {
    this.updateSettingsItemModule('connectedAccessPoint', ssid);
    this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, true);
    this.updateSettingsItemModule('rememberedAccessPoint', ssid);
    this.updateSettingsItemModule('rememberedAccessPointPword', password);
  }

  setModalVisible(visibility) {
    this.setState({ password: '' });
    this.openConnectAccessPointPrompt(visibility);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isVisible: nextProps.visibility,
      connectedAccessPoint: nextProps.connectedAccessPoint,
      ssid: nextProps.ssid,
      password: nextProps.password
    });
  }

  render() {
    return (
      <View>
      <Modal animationType="slide"
             transparent={true}
             style={widgets.modal}
             visible={this.state.isVisible}
             presentation
             onRequestClose={() => {
               if (this.state.connectedAccessPoint === I18n.t('NETWORK_NAME')) { 
                 this.setSettingsModuleSwitchMode(WIFI_STATION, false);
                } 
               this.setModalVisible(false);
             }}>
        <ScrollView>
        <View style={[widgets.bluetoothScanPrompt, containers.sendWifiDetailsCon]}>
          <Text style={[text.bluetoothTitleTask, widgets.commonMarginTop2]}>{I18n.t('SEND_WIFI_TO_STATION')}</Text>
          {RenderIf(Platform.OS === 'ios')(
          <TextInput placeholder={I18n.t('SSID') + '*'} style={widgets.connectTextInput}
            onChangeText={ssid => this.setState({ ssid })} value={this.state.ssid}/>
          )}
          <TextInput placeholder={I18n.t('PASSWORD') + '*'} style={Platform.OS === 'ios' ?
            [widgets.connectTextInput, widgets.commonMarginBottom] : [widgets.connectTextInput]} secureTextEntry={true}
            onChangeText={password => this.setState({ password })} value={this.state.password}/>
          <View style={[widgets.commonConRow, widgets.centerItemsInside, widgets.commonMarginBottom]}>
            <TouchableOpacity onPress= {() => {
              if (this.state.connectedAccessPoint === I18n.t('NETWORK_NAME')) {
                this.setSettingsModuleSwitchMode(WIFI_STATION, false);
              }
              this.setModalVisible(false);
            }}>
              <View style={[widgets.cancelBg, widgets.commonMarginRight]}>
                <Text style={{ color: 'black' }}>{I18n.t('CANCEL')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress= { () => {
              if (Platform.OS === 'ios') {
                if ((this.state.password === '' || this.state.password === undefined) || (this.state.ssid === '' || this.state.ssid === undefined)) {
                  showAlert('', I18n.t('FILL_UP_REQUIRED'));
                  return;
                }
              } else {
                if (this.state.password === '' || this.state.password === undefined) {
                  showAlert('', I18n.t('FILL_UP_REQUIRED'));
                  return;
                }
              }

              console.log('connectWifi data: ' + this.state.ssid + ' ' + this.state.password);
              this.connectWifi(this.state.ssid, this.state.password);
              this.setModalVisible(false);
            }}>
              <View style={[widgets.scanBg]}>
                <Text style={{ color: 'white' }}>{I18n.t('SEND')}</Text>
              </View>
            </TouchableOpacity>
          </View>
          </View>
          </ScrollView>
        </Modal>
      </View>
    );
  }
}

export { ConnectAccessPointPrompt };

