import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Modal } from 'react-native';
import { widgets } from '../../styles/widgets';
import { text } from '../../styles/text';
import { containers } from '../../styles/containers';
import I18n from './../../translate/i18n/i18n';

class ConnectDevicePrompt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      type: '',
      connectedDevice: ''
    };

    this.connect = this.props.connect;
    this.openConnectDevicePrompt = this.props.openConnectDevicePrompt;
    this.setSettingsModuleSwitchMode = this.props.setSettingsModuleSwitchMode;
  }

  setModalVisible(visibility) {
    this.openConnectDevicePrompt(visibility);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isVisible: nextProps.visibility,
      type: nextProps.type,
      connectedDevice: nextProps.connectedDevice
    });
  }

  render() {
    return (
      <View>
        <Modal animationType="slide"
          transparent={true}
          style={widgets.modal}
          visible={this.state.isVisible}
          presentation
          onRequestClose={() => {
            if (this.state.connectedDevice === I18n.t('DEVICE_NAME')) {
              this.setSettingsModuleSwitchMode(this.state.type, false);
            }
            this.setModalVisible(false);
          }}>
          <View style={[widgets.bluetoothScanPrompt, containers.commonModalCon]}>
            <Text style={text.bluetoothTitleTask}>{I18n.t('CONNECT_DEVICE')}</Text>
            <View style={[widgets.commonConRow, widgets.commonMarginTop]}>
              <TouchableOpacity onPress={() => {
                if (this.state.connectedDevice === I18n.t('DEVICE_NAME')) {
                  this.setSettingsModuleSwitchMode(this.state.type, false);
                }
                this.setModalVisible(false);
              }}>
                <View style={[widgets.cancelBg, widgets.commonMarginRight]}>
                  <Text style={{ color: 'black' }}>{I18n.t('CANCEL')}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
                this.connect();
                this.setModalVisible(false);
              }}>
                <View style={[widgets.scanBg]}>
                  <Text style={{ color: 'white' }}>{I18n.t('CONNECT')}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export { ConnectDevicePrompt };

