import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Modal, TextInput, ScrollView } from 'react-native';
import { widgets } from '../../styles/widgets';
import { text } from '../../styles/text';
import { containers } from '../../styles/containers';
import I18n from './../../translate/i18n/i18n';
import RenderIf from '../RenderIf';

class CommonDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      message: '',
      leftBtnLabel: '',
      rightBtnLabel: '',
      isLeftBtnEnabled: false
    };

    this.leftBtnPress = this.props.leftBtnPress;
    this.dismissDialog = this.props.dismissDialog;
    this.rightBtnPress = this.props.rightBtnPress;
  }

  setModalVisible(visibility) {
    this.dismissDialog(visibility);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isVisible: nextProps.visibility,
      message: nextProps.message,
      leftBtnLabel: nextProps.leftBtnLabel,
      rightBtnLabel: nextProps.rightBtnLabel,
      isLeftBtnEnabled: nextProps.isLeftBtnEnabled
    });
  }

  render() {
    return (
      <View>
        <Modal animationType="slide"
          transparent={true}
          style={widgets.modal}
          visible={this.state.isVisible}
          presentation
          onRequestClose={() => {
            this.setModalVisible(false);
          }}>
          <ScrollView>
            <View style={[widgets.bluetoothScanPrompt, containers.commonModalCon]}>
              <Text style={[text.bluetoothTitleTask, widgets.commonMarginTop2, widgets.commonMarginRight, widgets.commonMarginLeft]}>{this.state.message}</Text>
              <View style={[widgets.commonConRow, widgets.centerItemsInside, widgets.commonMarginBottom, widgets.commonMarginTop2]}>
                {RenderIf(this.state.isLeftBtnEnabled)(
                <TouchableOpacity onPress={() => {
                  this.setModalVisible(false);
                  this.leftBtnPress();
                }}>
                  <View style={[widgets.cancelBg, widgets.commonMarginRight]}>
                    <Text style={{ color: 'black' }}>{this.state.leftBtnLabel}</Text>
                  </View>
                </TouchableOpacity>
                )}
                <TouchableOpacity onPress={() => {
                  this.setModalVisible(false);
                  if(this.state.isLeftBtnEnabled){
                    this.rightBtnPress();
                  }
                }}>
                  <View style={[widgets.scanBg]}>
                    <Text style={{ color: 'white' }}>{this.state.rightBtnLabel}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </Modal>
      </View>
    );
  }
}

export { CommonDialog };

