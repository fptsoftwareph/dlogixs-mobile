import React, { Component } from 'react';
import {
  View,
  Text,
  Modal,
  NativeEventEmitter,
  TouchableHighlight,
  NativeModules,
  TouchableOpacity,
  ToastAndroid,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Platform,
  FlatList
} from 'react-native';
import { widgets } from '../../styles/widgets';
import BleManager from 'react-native-ble-manager';
import { MyModule, SMAModule, StationModule } from 'nb_native_modules';
import { ConnectDevicePrompt } from './ConnectDevicePrompt';
import RenderIf from '../RenderIf';
import { showAlert } from '../../utils/CommonMethods';
import I18n from './../../translate/i18n/i18n';
import Toast from 'react-native-simple-toast';
import { CommonDialog } from './CommonDialog';
import { BSTATION, DEVICE_NAME, WRISTBAND, WIFI_STATION } from '../../utils/DataConstants';
import { savePreference } from '../../utils/CommonMethods';
import EasyBluetooth from 'easy-bluetooth-classic';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const SMAManagerModule = NativeModules.SMAModule;
const SMAManagerEmitter = new NativeEventEmitter(SMAManagerModule);

const StationManagerModule = NativeModules.StationModule;
const StationManagerEmitter = new NativeEventEmitter(StationManagerModule);

let isCurrentlyConnected = false;
let isPairing = false;

//let responseCounter = 0;

class BluetoothDevicesListing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      peripherals: new Map(),
      pairedPeripherals: new Map(),
      type: '',
      isToConnectDevicePrompt: false,
      connectType: '',
      item: {},
      animating: true,
      scanning: false,
      connectedDevice: '',
      isSuccessfullyConnected: false,
      peripheral: new Map(),
      audioSelectorWifi: false
    };

    this.openBluetoothDevicesListing = this.props.openBluetoothDevicesListing;
    this.updateSettingsItemModule = this.props.updateSettingsItemModule;
    this.setSettingsModuleSwitchMode = this.props.setSettingsModuleSwitchMode;
    this.scanBLEDevices = this.props.scanBLEDevices;
    this.scanAccessPtsPrompt = this.props.scanAccessPtsPrompt;
    this.openConnectAccessPointPrompt = this.props.openConnectAccessPointPrompt;
    this.setBluetoothModulesToDefault = this.props.setBluetoothModulesToDefault;
    this.clearBinauralTimer = this.props.clearBinauralTimer;

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.connectDeviceOnPress = this.connectDeviceOnPress.bind(this);
    this.connectStation = this.connectStation.bind(this);
    this.openConnectDevicePrompt = this.openConnectDevicePrompt.bind(this);

    this.dismissOutside = this.dismissOutside.bind(this);
    this.openSuccessfullyConnectedDialog = this.openSuccessfullyConnectedDialog.bind(this);
  }

  pairPeripheral(peripheral, where) {
    console.log(`pairPeripheral: ${JSON.stringify(peripheral)}`);
    if (!isPairing) {
      isPairing = true;
      ToastAndroid.show(I18n.t('PAIRING_PLEASE_WAIT'), ToastAndroid.SHORT);
    
      BleManager.createBond(peripheral.id)
        .then(() => {
          console.log('createBond success or there is already an existing one');

          if (this.state.type === BSTATION) {
            showAlert('', I18n.t('PAIRED_SUCCESSFUL'));
          }

          var peripherals = this.state.peripherals;
          if (peripherals.has(peripheral.id)) {
            console.log('Deleted peripheral', peripheral);
            peripherals.delete(peripheral.id);
            this.setState({ peripherals });
          }

          //  Android only
          this.connectDevice(peripheral, where);
          this.getPairedPeripherals();
          isPairing = false;
        })
        .catch(error => {
          console.log(`Fail to bond: ${error}`);

          if (this.state.connectedDevice === I18n.t('DEVICE_NAME')) {
            this.setSettingsModuleSwitchMode(this.state.type, false);
          }

          this.setSettingsModuleSwitchMode(this.state.type, false);
          showAlert('', I18n.t('FAIL_TO_BOND'));
          isPairing = false;
        });
    }
  }

  dismissOutside() {
    if (this.state.connectedDevice === I18n.t('DEVICE_NAME')) {
      this.setSettingsModuleSwitchMode(this.state.type, false);
    }
    this.setModalVisible(false);
  }

  render() {
    //  console.log('render');

    const list = Array.from(this.state.peripherals.values());
    const list2 = Array.from(this.state.pairedPeripherals);

    const animating = this.state.animating;

    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          style={widgets.modal}
          visible={this.state.isVisible}
          presentation
          onRequestClose={this.dismissOutside}
        >
          <TouchableWithoutFeedback onPressIn={this.dismissOutside}>
            <View style={[widgets.bluetoothScanPromptCon]}>
              <TouchableWithoutFeedback onPressIn={() => { }}>
                <View style={[widgets.bluetoothScanPrompt, widgets.bTDevListingViewMargin]}>
                  {RenderIf(list2.length > 0)(
                    <View style={widgets.commonFlex}>
                      <Text
                        style={[
                          widgets.commonPaddingSmall,
                          { fontWeight: 'bold', paddingLeft: 3, backgroundColor: '#bbbcbb' }
                        ]}
                      >
                        {I18n.t('PAIRED_DEVICES')}
                      </Text>
                      <FlatList
                        enableEmptySections={true}
                        style={{ marginTop: 5, marginBottom: 8 }}
                        data={list2}
                        removeClippedSubviews={false}
                        showsVerticalScrollIndicator={true}
                        keyExtractor={(item, index) => index}
                        renderItem={({ item }) =>
                          <TouchableHighlight
                            onPress={() => {
                              console.log('Paired device onPress: ' + JSON.stringify(item));
                              this.setState({ connectType: 'connect' });
                              this.connectDeviceOnPress(item, 'connect');
                            }}
                          >
                            <View style={[widgets.commonConRow, { backgroundColor: 'white', padding: 5 }]}>
                              <View style={[widgets.commonFlex, widgets.commonConColumn]}>
                                <Text style={{ marginLeft: 20, fontSize: 14, color: '#333333' }}>
                                  {item === null || item === undefined || item.name === undefined || item.name === null
                                    ? I18n.t('N_A')
                                    : item.name}
                                </Text>
                                {/* <Text style={{ marginLeft: 20, fontSize: 8, color: '#333333', paddingBottom: 10 }}>{item.id}</Text> */}
                              </View>
                              <TouchableOpacity
                                onPress={() => {
                                  ToastAndroid.show(I18n.t('UNPAIRING_PLEASE_WAIT'), ToastAndroid.SHORT);
                                  if (Platform.OS === 'android') {
                                    console.log(`item.id: ${item.id}`);

                                    MyModule.unpairDevice(item.id, response => {
                                      if (response === 'Device successfully unpaired') {
                                        var pairedPeripherals = this.state.pairedPeripherals.filter(obj => obj.id !== item.id);
                                        SMAModule.disconnect(success => {
                                          console.log('disconnected');
                                        });
                                        this.setState({ pairedPeripherals });
                                        console.log('paired peripheral data', pairedPeripherals);
                                        console.log('removed paired peripheral data', item);
                                        this.updateSettingsItemModule('connectedPeripheralId', '');
                                        this.addPeripheralToAvailableDevices(item, pairedPeripherals);

                                        if (item.name === this.state.connectedDevice) {
                                          this.setBluetoothModulesToDefault(this.state.type);
                                        } else {
                                          this.openBluetoothDevicesListing(true);
                                        }
                                      }
                                      showAlert('', I18n.t('DEVICE_UNPAIRED'));
                                    });
                                  }
                                }}
                              >
                                <View
                                  style={[
                                    widgets.commonMarginRight,
                                    { alignItems: 'center', justifyContent: 'center' }
                                  ]}
                                >
                                  <Text style={{ fontSize: 14, color: '#3bafda' }}>{I18n.t('UNPAIR')}</Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                          </TouchableHighlight>
                        }
                      />
                    </View>
                  )}

                  <View style={widgets.commonFlex}>
                    <View style={widgets.commonConRow}>
                      <View
                        style={[
                          widgets.commonFlex,
                          widgets.commonPaddingSmall,
                          { paddingLeft: 3, backgroundColor: '#bbbcbb', flex: 3 }
                        ]}
                      >
                        <Text style={{ fontWeight: 'bold' }}>{I18n.t('AVAILABLE_DEVICE')}</Text>
                      </View>
                      <View
                        style={[
                          widgets.alignRightViewWithNoMargin,
                          widgets.commonPaddingSmall,
                          { backgroundColor: '#3bafda', alignItems: 'center', flex: 1.3, justifyContent: 'center' }
                        ]}
                      >
                        {RenderIf(this.state.scanning)(
                          <View style={[widgets.commonConRow]}>
                            <Text style={{ color: 'white' }}>{I18n.t('SCANNING')}</Text>
                            <ActivityIndicator animating={animating} color="white" size="small" />
                          </View>
                        )}
                        {RenderIf(!this.state.scanning)(
                          <TouchableOpacity
                            onPress={() => {
                              this.scanBLEDevices();
                            }}
                          >
                            <Text style={{ color: 'white' }}>{I18n.t('SCAN')}</Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>

                    <FlatList
                      enableEmptySections={true}
                      style={{ marginTop: 5, marginBottom: 8 }}
                      data={list}
                      removeClippedSubviews={false}
                      showsVerticalScrollIndicator={true}
                      keyExtractor={(item, index) => index}
                      renderItem={({ item }) =>
                        <TouchableHighlight
                          onPress={() => {
                            console.log('Available device onPress');
                            if (Platform.OS === 'android') {
                              this.setState({ connectType: 'pair' });
                              this.connectDeviceOnPress(item, 'pair');
                            } else {
                              this.setState({ connectType: 'connect' });
                              this.connectDeviceOnPress(item, 'connect');
                            }
                          }}
                        >
                          <View style={{ backgroundColor: 'white', padding: 5 }}>
                            <Text style={{ marginLeft: 20, fontSize: 14, color: '#333333' }}>
                              {item === null || item === undefined || item.name === undefined || item.name === null
                                ? 'N/A'
                                : item.name}
                            </Text>
                            {/* <Text style={{ marginLeft: 20, fontSize: 8, color: '#333333', paddingBottom: 10 }}>{item.id}</Text> */}
                          </View>
                        </TouchableHighlight>
                      }
                    />
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
        <ConnectDevicePrompt
          type={this.state.type}
          visibility={this.state.isToConnectDevicePrompt}
          connect={this.connectStation}
          openConnectDevicePrompt={this.openConnectDevicePrompt}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
          connectedDevice={this.props.connectedDevice}
        />
        <CommonDialog
          visibility={this.state.isSuccessfullyConnected}
          message={I18n.t('CONNECT_SUCCESSFUL')}
          rightBtnLabel={I18n.t('OK')}
          dismissDialog={this.openSuccessfullyConnectedDialog}
        />
      </View>
    );
  }

  openConnectDevicePrompt(visibility) {
    this.setState({ isToConnectDevicePrompt: visibility });
    console.log(`isToConnectDevicePrompt: ${this.state.isToConnectDevicePrompt}`);
  }

  openSuccessfullyConnectedDialog(visibility) {
    this.setState({ isSuccessfullyConnected: visibility });
  }

  setItemConnectDevicePromptAndHideModal(item) {
    this.setState({ item });
    this.openConnectDevicePrompt(true);
    this.setModalVisible(false);
  }

  connectDeviceOnPress(item, where) {
    switch (this.state.type) {
      case BSTATION:
        this.setItemConnectDevicePromptAndHideModal(item);
        break;
      case WRISTBAND:
        if (Platform.OS === 'android') {
          if (where === 'pair') {
            this.pairPeripheral(item, 'pair');
          } else {
            this.connectDevice(item);
          }
        } else {
          // ios
          this.connectDevice(item);
        }
        break;
      case WIFI_STATION:
        this.setItemConnectDevicePromptAndHideModal(item);
        break;
      default:
        console.log('Connection type not found');
        break;
    }
  }

  connectStation() {
    if (Platform.OS === 'android') {
      if (this.state.connectType === 'pair') {
        this.pairPeripheral(this.state.item, 'pair');
      } else {
        this.connectDevice(this.state.item);
      }
    } else {
      this.connectDevice(this.state.item);
    }
  }

  componentWillMount() {
    //  console.log('BluetoothDevicesListing componentWillMount');

    if (Platform.OS === 'android') {
      this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
      this.onDeviceFoundEvent = EasyBluetooth.addOnDeviceFoundListener(this.onDeviceFound.bind(this));
    } else {
      console.log('componentDidMount() BluetiithDeviceListing TYPE: ' + this.state.type);
      this.handlerDiscover = StationManagerEmitter.addListener('StationManagerDiscoverPeripherals', this.handleDiscoverPeripheral);
      this.handlerDiscover = SMAManagerEmitter.addListener('SMAManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
    }
    this.handleStationConnectionStatus = StationManagerEmitter.addListener('StationConnectionStatusListener', status => {
      console.log("StationConnectionStatusListener: " + status);

      if (status === 3) {
        this.showSuccessPromptAndUpdateDeviceStatus(this.state.peripheral);
      } else if (status === 5) {
        if (this.state.connectedDevice === I18n.t('DEVICE_NAME')) {
          this.setSettingsModuleSwitchMode(this.state.type, false);
        }
        this.setBluetoothModulesToDefault(BSTATION);
        Toast.show(I18n.t('UNABLE_TO_CONNECT_DEVICE'), Toast.SHORT);
        this.clearBinauralTimer();
      }
    });
  }

  componentDidMount() {
    //  console.log('BluetoothDevicesListing componentDidMount');
  }

  componentWillReceiveProps(nextProps) {
    //  console.log('BluetoothDevicesListing componentWillReceiveProps');
    isCurrentlyConnected = false;
    
    this.setState({
      isVisible: nextProps.visibility,
      type: nextProps.type,
      scanning: nextProps.scanning,
      pairedPeripherals: nextProps.pairedPeripherals,
      peripherals: nextProps.peripherals,
      connectedDevice: nextProps.connectedDevice,
      audioSelectorWifi: nextProps.audioSelectorWifi
    });
    this.getPairedPeripherals();
  }

  shouldComponentUpdate() {
    //  First Called when there are new states or new peripherals discovered
    //  Return true to re-render and called the below lifecycle methods
    //  console.log('BluetoothDevicesListing shouldComponentUpdate');
    return true;
  }

  componentWillUpdate() {
    //  Called when there are new states or new peripherals discovered
    //  console.log('BluetoothDevicesListing ------------------componentWillUpdate----------------');
  }

  componentDidUpdate() {
    //  console.log('BluetoothDevicesListing componentDidUpdate');
  }

  componentWillUnmount() {
    //  console.log('BluetoothDevicesListing componentWillUnmount');

    this.handlerDiscover.remove();
    if (Platform.OS === 'android') {
      this.handleStationConnectionStatus.remove();
      this.onDeviceFoundEvent.remove();
    }
    this.handlerDiscover.remove();
  }

  onDeviceFound(device) {
    //  console.log("bt123 EasyBluetooth onDeviceFound: " + JSON.stringify(device));
    if (device.name !== null) {
      const classicPeripheral = {
        "name": device.name,
        "id": device.address
      };
      this.addPeripheralToAvailableDevices(classicPeripheral, this.state.pairedPeripherals);
    }
  }

  handleDiscoverPeripheral(peripheral) {
    //  console.log('discovered peripheral: ' + peripheral.name + ' ' + peripheral.id);
    if (peripheral.name !== null) {
      this.addPeripheralToAvailableDevices(peripheral, this.state.pairedPeripherals);
    }
  }

  addPeripheralToAvailableDevices(peripheral, pairedPeripherals) {
    //  console.log("bt123 addedPeripheral: " + JSON.stringify(peripheral));
    var peripherals = this.state.peripherals;

    if (!peripherals.has(peripheral.id) && peripheral.name !== null && !this.checkIfItemIsAvailable(pairedPeripherals, peripheral.id)) {
      //  console.log('Added discovered peripheral: ' + peripheral.name);
      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals });
    }
  }

  checkIfItemIsAvailable(arr, val) {
    if (Platform.OS === 'android' && typeof arr !== 'undefined' && Array.from(arr).length > 0) {
      return arr.some(arrVal => {
        return val === arrVal.id;
      });
    }

    return false;
  }

  getDiscoveredPeripherals() {
    BleManager.getDiscoveredPeripherals([])
      .then(peripheralsArray => {
        // Success code
        //  console.log(`Discovered peripherals length: ${peripheralsArray.length}`);
        //  console.log(`Discovered peripherals: ${JSON.stringify(peripheralsArray)}`);
      })
      .catch(error => {
        console.log(`getDiscoveredPeripherals err: ${error}`);
      });
  }

  getConnectedPeripherals() {
    BleManager.getConnectedPeripherals([])
      .then(peripheralsArray => {
        //  console.log(`Connected peripherals: ${JSON.stringify(peripheralsArray)}`);
      })
      .catch(error => {
        //  console.log(`getConnectedPeripherals err: ${error}`);
      });
  }

  getPairedPeripherals() {
    if (Platform.OS === 'android') {
      BleManager.getBondedPeripherals([])
        .then(peripheralsArray => {
          //  console.log(`Paired peripherals: ${JSON.stringify(peripheralsArray)}`);
          this.setState({ pairedPeripherals: peripheralsArray });
        })
        .catch(error => {
          console.log(`getPairedPeripherals err: ${error}`);
        });
    } else {
      BleManager.getConnectedPeripherals([])
        .then(peripheralsArray => {
          this.setState({ pairedPeripherals: peripheralsArray });
        })
        .catch(error => {
          console.log(`getPairedPeripherals err: ${error}`);
        });
    }
  }

  connectDevice(peripheral) {
    this.setState({ peripheral: peripheral });

    console.log(`Peripheral id: + ${peripheral.id}`);
    Toast.show(I18n.t('CONNECTING_STATION'), Toast.SHORT);
    if (this.state.type === WRISTBAND) {
      console.log('connected => connectDevice()');
      SMAModule.connect(
        peripheral.id,
        error => {
          // Failure code
          console.log(`connected error: ${error}`);
          if (this.state.connectedDevice === I18n.t('DEVICE_NAME')) {
            this.setSettingsModuleSwitchMode(this.state.type, false);
          }
          Toast.show(error, Toast.SHORT);
        },
        success => {
          // Success code
          console.log(`connected success: ${success}`);
          this.showSuccessPromptAndUpdateDeviceStatus(peripheral);
        }
      );
    } else {
      if (Platform.OS === 'android') {
        StationModule.connectStation(peripheral.id, "settings");
      } else {
        StationModule.connectStation(peripheral.id, response => {
          console.log('connectStation(): ', response);
          if (response === true) {
            console.log('connectDevice() device:', response);
            this.showSuccessPromptAndUpdateDeviceStatus(peripheral);
          } else {
            if (response === 2) {
              Toast.show(I18n.t('UNABLE_TO_CONNECT_DEVICE'), Toast.SHORT);
            } else {
              Toast.show(response, Toast.SHORT);
            }
          }
        });
      }
    }
  }

  showSuccessPromptAndUpdateDeviceStatus(peripheral) {
    console.log("showSuccessPromptAndUpdateDeviceStatus type: " + this.state.type);
    if (Platform.OS === 'android') {
      if (this.state.type !== WIFI_STATION) {
        if (!isCurrentlyConnected) {
          showAlert('', I18n.t('CONNECT_SUCCESSFUL'));
          isCurrentlyConnected = true;
        }
      }
    } else {
      if (this.state.type !== WIFI_STATION) {
        console.log('connectDevice(): openSuccessDialog()');
        this.openSuccessfullyConnectedDialog(true);
      }
    }

    console.log(`Type: ${this.state.type}`);
    
    if (this.state.type === WIFI_STATION) {
      this.updateSettingsItemModule('connectedPeripheral', BSTATION);
    } else {
      this.updateSettingsItemModule('connectedPeripheral', this.state.type);
    }
   
    if (this.state.type === BSTATION) {
      this.updateSettingsItemModule('rememberedPeripheralId', peripheral.id);
      this.updateSettingsItemModule('rememberedPeripheralName', peripheral.name);
    }

    this.updateSettingsItemModule('connectedPeripheralId', peripheral.id);
    this.updateSettingsItemModule('connectedDevice', peripheral.name);
    switch (this.state.type) {
      case BSTATION:
        this.enableAndSetBstation(peripheral);
        StationModule.queryStationStatus();
        if (!this.state.audioSelectorWifi) {
          StationModule.setAudioInput(0, response => {
            console.log(`setAudioInput: ${response}`);
            if (response === true) {
              this.updateSettingsItemModule('audioSelected', 0);
            } else {
              Toast.show(response, Toast.SHORT);
            }
          });
        }
        break;
      case WRISTBAND:
        this.updateSettingsItemModule('connectedWristband', peripheral.name);
        this.updateSettingsItemModule('isSmartBandEnabled', true);
        this.updateSettingsItemModule('dateSmartBandConnected', new Date());
        savePreference('shouldUpdateDateSmartBandConnected', JSON.stringify(true));
        this.updateSettingsItemModule('dateSmartBandDisconnected', '');
        break;
      case WIFI_STATION:
        // open scan wifi
        this.enableAndSetBstation(peripheral);
        this.updateSettingsItemModule('rememberedPeripheralId', peripheral.id);
        this.updateSettingsItemModule('rememberedPeripheralName', peripheral.name);

        if (Platform.OS === 'ios') {
          this.openConnectAccessPointPrompt(true);
        } else {
          this.scanAccessPtsPrompt();
        }
        break;
      default:
        console.log('Connection type not found');
        break;
    }
    this.setModalVisible(false);
  }

  enableAndSetBstation(peripheral) {
    this.updateSettingsItemModule('connectedBStation', peripheral.name);
    this.updateSettingsItemModule('isBstationHeadSetEnabled', true);
  }

  setModalVisible(visibility) {
    if (this.props.scanning) {
      this.props.stopScan();
    }

    this.setState({
      isVisible: visibility,
      pairedPeripherals: new Map(),
      peripherals: new Map()
    });
  }
}

export { BluetoothDevicesListing };
