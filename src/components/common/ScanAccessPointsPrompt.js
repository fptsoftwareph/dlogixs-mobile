import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Modal, NetInfo, Platform
} from 'react-native';
import { widgets } from '../../styles/widgets';
import { text } from '../../styles/text';
import I18n from './../../translate/i18n/i18n';
import { AccessPointsListing } from './AccessPointsListing';
import { showAlert } from '../../utils/CommonMethods';
import { IS_WIFI_STATION_ENABLED, DEVICE_NAME, NETWORK_NAME } from '../../utils/DataConstants';
import { containers } from '../../styles/containers';

var wifi = require('react-native-android-wifi');

class ScanAccessPointsPrompt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      isToOpenAccessPointsListing: false,
      scanning: false,
      connectedAccessPoint: '',
      accessPtsArray: new Map(),
      ssid: '',
      password: '',
      isFromSM: false
    };

    this.scanAccessPtsPrompt = this.props.scanAccessPtsPrompt;
    this.updateSettingsItemModule = this.props.updateSettingsItemModule;
    this.setSettingsModuleSwitchMode = this.props.setSettingsModuleSwitchMode;
    this.setSourceOfScanToEmpty = this.props.setSourceOfScanToEmpty;

    this.openAccessPointsListing = this.openAccessPointsListing.bind(this);
    this.stopScan = this.stopScan.bind(this);
    this.scanAccessPoints = this.scanAccessPoints.bind(this);
  }

  setModalVisible(visibility) {
    this.scanAccessPtsPrompt(visibility);
    if (this.state.isFromSM) {
      this.setSourceOfScanToEmpty();
    }
  }

  openAccessPointsListing(visibility) {
    this.setState({ isToOpenAccessPointsListing: visibility });
    console.log('isToOpenAccessPointsListing: ' + this.state.isToOpenAccessPointsListing);
  }

  stopScan() {
    wifi.disconnect();
  }

  startWifiManager() {
    console.log('-----------startWifiManager-----------');
    this.checkIfWifiIsEnabledInAndroid();
  }

  checkIfWifiIsEnabledInAndroid() {
    wifi.isEnabled((isEnabled) => {
      if (isEnabled) {
        console.log("wifi service enabled");
        this.scanAccessPoints();
      } else {
        console.log("wifi service is disabled");
        //Set TRUE to enable and FALSE to disable;
        wifi.setEnabled(true);
        console.log("wifi set to enabled");
        this.scanAccessPoints();
      }
    });
  }

  scanAccessPoints() {
    console.log("-------------------scanAccessPoints-------------------");
    this.setState({
      accessPtsArray: new Map()
    });

    wifi.loadWifiList((wifiStringList) => {
      const accessPtsArray = JSON.parse(wifiStringList);
      console.log('accessPtsArray: ' + accessPtsArray);

      this.setState({
        accessPtsArray: accessPtsArray
      });

      if (Object.keys(this.state.accessPtsArray).length !== 0) {
        this.setModalVisible(false);
        this.openAccessPointsListing(true);
      } else {
        showAlert('', I18n.t('WIFI_LOC_NOT_FOUND'));
      }
    }, error => {
      console.log(error);
    }
    );
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectivityChange);
  }

  componentWillUnmount() {
    console.log('ScanAccessPointsPrompt componentWillUnmount');
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectivityChange);
  }

  _handleConnectivityChange = isConnected => {
    console.log('isConnectedToAccessPoint: ' + isConnected);
    /*  if (Platform.OS === 'android') {
      if (isConnected) {
        //  showAlert('', 'Successfully connected!');
        this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, true);
        this.updateSettingsItemModule('connectedAccessPoint', this.state.ssid);
      } else {
        this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, false);
        this.updateSettingsItemModule('connectedAccessPoint', DEVICE_NAME);
      }

      this.openAccessPointsListing(false);
    } */
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isVisible: nextProps.visibility,
      isToOpenAccessPointsListing: nextProps.isToOpenAccessPointsListing,
      connectedAccessPoint: nextProps.connectedAccessPoint,
      isFromSM: nextProps.isFromSM
    });
  }

  render() {
    return (
      <View>
      <Modal animationType="slide"
             transparent={true}
             style={widgets.modal}
             visible={this.state.isVisible}
             presentation
             onRequestClose={() => {
               if (this.state.connectedAccessPoint === I18n.t('NETWORK_NAME')) {
                 this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, false);
               }  
               this.setModalVisible(false);
             }}>
        <View style={[widgets.bluetoothScanPrompt, containers.commonModalCon]}>
          <Text style={text.bluetoothTitleTask}> {I18n.t('SCAN_FOR_WIFI')}</Text>
          <View style={[widgets.commonConRow, widgets.commonMarginTop]}>
            <TouchableOpacity onPress= {() => {
              if (this.state.connectedAccessPoint === I18n.t('NETWORK_NAME')) {
                this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, false);
              }  
              this.setModalVisible(false);
            }}>
              <View style={[widgets.cancelBg, widgets.commonMarginRight]}>
                <Text style={{ color: 'black' }}> {I18n.t('CANCEL')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress= { () => {
              this.startWifiManager();
            }}>
              <View style={[widgets.scanBg]}>
                <Text style={{ color: 'white' }}> {I18n.t('SCAN')}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <AccessPointsListing
        visibility={this.state.isToOpenAccessPointsListing}
        openAccessPointsListing={this.openAccessPointsListing}
        scanning={this.state.scanning}
        updateSettingsItemModule={this.updateSettingsItemModule}
        scanAccessPoints={this.scanAccessPoints}
        stopScan={this.stopScan}
        connectedAccessPoint={this.state.connectedAccessPoint}
        accessPtsArray = {this.state.accessPtsArray}
        setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode} />
      </View>
    );
  }
}

export { ScanAccessPointsPrompt };

