import * as DateUtil from '../../utils/DateUtils';
import TargetDoc from './TargetDoc';
import CurationActualData from './CurationActualData';
import uuidv4 from 'uuid/v4';
import I18n from './../../translate/i18n/i18n';
import getCurrentDate from '../../utils/DateUtils';
import { getPreference } from '../../utils/CommonMethods';

class Binaural {
  constructor(targetBinauralHours) {
    var binauralMinutes = 0;
    var user_id = '';
    this.binauralInterval = 0;
    this.refreshCallback = null;
    this.targetBinauralHours = targetBinauralHours;
    console.log('123targetbb: ' + this.targetBinauralHours);

    this.TIME = {
      HOUR: I18n.t('HOUR'),
      MIN: I18n.t('MINS'),
      SEC: I18n.t('SECONDS')
    };
    this.rows = [];
    this.rows2 = [];
    setTimeout(() => {
      this.getDatabase();
    }, 2000);

    this.err404Counter = 0;
  }

  static getInstance(targetBinauralHours) {
    if (Binaural.binaural === undefined) {
      Binaural.binaural = new Binaural(targetBinauralHours);
    }
    return Binaural.binaural;
  }

  getDatabase() {
    manager.database.get_db({ db: DB_NAME })
      .then(res => {
      });
  }

  queryBinauralDocumentsAndSave() {
    this.setupBinauralViewAndQuery().then(r => {

      if (r.obj.rows !== undefined) {
        this.rows2 = r.obj.rows;
        this.rows = [];
        console.log('queryBinauralDocumentsAndSave orig rows: ' + JSON.stringify(this.rows2));

        if (this.rows2.length > 0) {
          let dateNow = getCurrentDate();
          let minDate = new Date(this.rows2[0].doc.timestamp);
          console.log('queryBinauralDocumentsAndSave minDate: ' + minDate);
          let minData = this.rows2[0];

          for (let i = 0; i < this.rows2.length; i++) {
            if (new Date(this.rows2[i].doc.date).getTime() === new Date(dateNow).getTime() &&
              new Date(this.rows2[i].doc.timestamp) < minDate) {
              minDate = new Date(this.rows2[i].doc.timestamp);
              minData = this.rows2[i];
              console.log('queryBinauralDocumentsAndSave compared date: ' + new Date(this.rows2[i].doc.timestamp));
            }
          }

          this.rows.push(minData);
        }

        console.log('queryBinauralDocumentsAndSave rows: ' + JSON.stringify(this.rows));
        this.saveBinaural();
      }
    }).catch(err => {
      console.log('123bbstart: 2. err: ' + JSON.stringify(err));
      if (this.err404Counter < 3 && err.obj.status === 404) {
        // The couchbase binaural view creation took longer than 2 seconds to create, so the view cannot be found.
        // We catch this error by querying the binaural documents at max three tries. This is a safety precaution
        // we're doing to ensure that we will only create 1 document/day. In some devices, Couchbase cannot create
        // the views w/in the 2 second delay we've added before.
        this.queryBinauralDocumentsAndSave();
        this.err404Counter++;
      }
    });
  }

  setupBinauralViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'binauralWithinToday',
      include_docs: true
    });
  }

  saveForCuration(newBinauralValue) {
    setTimeout(() => {
      let target = {
        bb: newBinauralValue, index_of_bb: 1, modified_date: DateUtil.getFormattedTimestamp()
      };
      TargetDoc.upsertTargetDocument('binaural', target);
      CurationActualData.upsertTargetDocument('binaural', target);
    }, 5);
  }

  setBinauralTimer(binauralStart) {
    if (this.binauralInterval === 0) {
      this.binauralInterval = setInterval(() => {
        console.log('123bb timer start... binauralStart: ' + binauralStart);
        let timeDiff = new Date() - binauralStart;
        if (binauralStart === 0) {
          timeDiff = 0;
        }
        console.log('123bb timeDiff: ' + timeDiff);

        this.binauralMinutes = this.computeTimeDiff(timeDiff, this.TIME.MIN);
        console.log('123xz: this.binauralMinutes: ' + this.binauralMinutes);

        getPreference('targetBinauralHours').then(res => {
          let target = isNaN(parseInt(res, 10)) ? 0 : parseInt(res, 10);
          console.log('123bb pref targetBinauralHours: ' + target);
          this.targetBinauralHours = target;
          
          // Note: The reason we're querying is because there might
          // be times when a user uses two smart phones and if one device made RBS related changes
          // and the other device is used for RBS storing of data their, data will not be in sync
          // because the latter device will store a new document. This ensures two devices can store
          // or read data synchronously.
          this.queryBinauralDocumentsAndSave();
        });
        console.log('123bb refreshCallback: ' + this.refreshCallback);
        setTimeout(() => {
          if (this.refreshCallback !== undefined && this.refreshCallback !== null) {
            this.refreshCallback();
          }
        }, 2000);
        binauralStart = new Date(); // this reset binauralStart to current Date and (binauralMins to 0).
        binauralStart.setSeconds(binauralStart.getSeconds() - 3);
      }, 60000);
    }
  }

  clearBinauralTimer() {
    console.log('123bb clearBinauralTimer');
    if (this.binauralInterval !== 0) {
      clearInterval(this.binauralInterval);
      this.binauralInterval = 0;
    }
  }

  setRefreshCallback(refreshCallback) {
    this.refreshCallback = refreshCallback;
  }

  saveBinaural() {
    console.log('123bb: saveBinaural: ');
    var newBinauralValue = 0;
    let _id = uuidv4();
    let doc = {
      _id: _id,
      date: getCurrentDate(),
      binaural: this.binauralMinutes,
      target_binaural: (isNaN(this.targetBinauralHours) || this.targetBinauralHours === undefined) ? 0 : this.targetBinauralHours,
      timestamp: DateUtil.getFormattedTimestamp(),
      type: "binaural",
      channels: [],
      user_id: '',
      modified_date: DateUtil.getFormattedTimestamp()
    };

    let docExists = false;
    let rev = '';

    if (this.getDocToday() !== null) {
      let targetBinaural = doc.target_binaural;
      let binaural = this.binauralMinutes;
      doc = this.getDocToday();
      console.log('123bb binauralDBRecords: ' + JSON.stringify(doc));
      rev = doc._rev;
      doc._rev = undefined;
      docExists = true;

      doc.binaural = binaural + doc.binaural;
      doc.target_binaural = targetBinaural;
      doc.modified_date = DateUtil.getFormattedTimestamp();
    }

    getPreference('userId').then(res => {
      doc.channels = [res];
      doc.user_id = res;

      if (doc.binaural === null || doc.binaural === undefined) {
        doc.binaural = 0;
      }

      newBinauralValue = doc.binaural;
      console.log('newBinauralValue: ' + newBinauralValue);

      if (!docExists) {
        manager.document.post({ db: DB_NAME, body: doc }).then(res => {
          console.log('123bbstart: post res: ' + JSON.stringify(res));
        }).catch(err => {
          console.log('123bbstart: post err: ' + JSON.stringify(err));
        });
      } else {
        manager.document.put({ db: DB_NAME, doc: doc._id, body: doc, rev: rev }).then(res => {
          console.log('123bbstart: res: ' + JSON.stringify(res));
        }).catch(err => {
          console.log('123bbstart: put err: ' + JSON.stringify(err));
        });
      }

      setTimeout(() => {
        console.log('123bbstart: this.getDocToday(): ' + JSON.stringify(this.getDocToday()));
        this.saveForCuration(newBinauralValue);
      }, 1000);
    }).catch(err => {
      console.log('couchbasehhhh user_id err: ' + err);
    });
  }

  computeTimeDiff(timeDiff, type) {
    let hours, mins, seconds;

    seconds = Math.floor(timeDiff / 1000);
    mins = Math.floor(seconds / 60);
    //seconds = seconds % 60;
    hours = Math.floor(mins / 60);
    //mins = mins % 60;  //to be use only if we display by figure e.g. 1hour 30mins = `${hours} hr ${mins} mins`

    switch (type) {
      case this.TIME.HOUR:
        return hours;
        break;
      case this.TIME.MIN:
        return mins;
        break
      case this.TIME.SEC:
        return seconds;
        break;
      default: return 0;
    }
  }

  getDocToday() {
    let dateNow = getCurrentDate();
    if (this.rows.length > 0 && this.rows[0].doc.date === DateUtil.convertDateToYYYYMMDD(dateNow)) {
      return this.rows[0].doc;
    }
    return null;
  }
}

export default Binaural;