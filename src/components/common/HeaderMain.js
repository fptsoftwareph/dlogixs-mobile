import React, { Component } from 'react';
import {
  View, Platform, UIManager, TouchableOpacity, Image, Dimensions, NativeModules,
  NativeEventEmitter, DeviceEventEmitter
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import { widgets } from '../../styles/widgets';
import { StationModule, SMAModule } from 'nb_native_modules';
import { manageBinaural, updateSettingsModule } from '../../redux/actions';
import { connect } from 'react-redux';
import Binaural from '../common/BinauralFunctions';
import SleepMgr from '../common/SleepInfoManager';
import Toast from 'react-native-simple-toast';
import I18n from './../../translate/i18n/i18n';
import { ScanBDevicesPrompt, ScanAccessPointsPrompt, ConnectAccessPointPrompt } from '../common';
import {
  IS_WIFI_STATION_ENABLED, DEVICE_NAME, BSTATION, WIFI_STATION, NETWORK_NAME, WRISTBAND
} from '../../utils/DataConstants';
import { showAlert, savePreference, getPreference, getBooleanPreference } from '../../utils/CommonMethods';
import { CommonDialog } from './CommonDialog';
import BleManager from 'react-native-ble-manager';
import AlarmActivity from '../alarm/AlarmActivity.js'
import { AlarmModule, MyModule } from 'nb_native_modules';

const AlarmManagerModule = NativeModules.AlarmModule;
const AlarmManagerEmitter = new NativeEventEmitter(AlarmManagerModule);

const { shortcutWrapper, shortcutContainer, touchWrapper } = widgets;
const { height } = Dimensions.get('window');

const StationManagerModule = NativeModules.StationModule;
const StationManagerEmitter = new NativeEventEmitter(StationManagerModule);

let isCurrentlyConnected = false, lastRbsState = false;

if (Platform.OS === 'android') {
  UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
}

class HeaderMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isToScanAccessPoints: false,
      isToConnectAccessPointPrompt: false,
      sourceOfScan: '',
      isToScanBDevices: false,
      isSuccessfullyConnected: false,
      isToOpenIosSettingsPrompt: false,
      isRbsAlreadyClicked: false
    };

    this.scanBDevicesPrompt = this.scanBDevicesPrompt.bind(this);
    this.setSettingsModuleSwitchMode = this.setSettingsModuleSwitchMode.bind(this);
    this.updateSettingsItemModule = this.updateSettingsItemModule.bind(this);
    this.scanAccessPtsPrompt = this.scanAccessPtsPrompt.bind(this);
    this.openConnectAccessPointPrompt = this.openConnectAccessPointPrompt.bind(this);
    this.openSuccessfullyConnectedDialog = this.openSuccessfullyConnectedDialog.bind(this);
    this.changeBatStatusToActive = this.changeBatStatusToActive.bind(this);
    this.updateManageBinaural = this.updateManageBinaural.bind(this);
    this.manageBinauralSettings = this.manageBinauralSettings.bind(this);
    this.openIosSettingsPrompt = this.openIosSettingsPrompt.bind(this);
    this.leftBtnPress = this.openAccessories.bind(this);
    this.setSourceOfScanToEmpty = this.setSourceOfScanToEmpty.bind(this);
    this.setBluetoothModulesToDefault = this.setBluetoothModulesToDefault.bind(this);

    this.binaural = Binaural.getInstance(typeof this.props.targetBinauralHours === 'undefined' ? 0 : this.props.targetBinauralHours);
    this.handleStationVolumeControl = this.handleStationVolumeControl.bind(this);
    this.handleStationLightControl = this.handleStationLightControl.bind(this);
    this.handleStationBinauralControl = this.handleStationBinauralControl.bind(this);
    this.handleStationEquipmentLookup = this.handleStationEquipmentLookup.bind(this);
    this.sleepMgr = SleepMgr.sharedInstance();

    this.removeCustomEventListeners = this.removeCustomEventListeners.bind(this);
  }

  componentWillMount() {    
    if (Platform.OS === 'android') {
      StationModule.serviceInit(response => {
        console.log(`station componentWillMount serviceInit: ${response}`);
        if (this.props.isBstationHeadSetEnabled) {
          StationModule.isSppServiceNull(response2 => {
            console.log(`station componentWillMount isSppServiceNull: ${response2}`);

            if (!response2 && this.props.rememberedPeripheralId !== '') {
              this.setBluetoothModulesToDefault(BSTATION);
            }
          });
        }
      });
    } else {
      StationModule.serviceInit({ showAlert: false }, result => {
        console.log(`station componentWillMount serviceInit: ${result}`);
      });
    }

    this.handleStationVolume = StationManagerEmitter.addListener('StationVolumeStatusListener', this.handleStationVolumeControl);
    this.handleStationLight = StationManagerEmitter.addListener('StationLightStatusListener', this.handleStationLightControl);
    this.handleStationBinaural = StationManagerEmitter.addListener('StationBinauralStatusListener', this.handleStationBinauralControl);
    this.handleStationEquipmentState = StationManagerEmitter.addListener('StationLookupListener', this.handleStationEquipmentLookup);
    this.binaural.setRefreshCallback(this.props.refreshCallback);
    this.sleepMgr.setSleepListener();
    this.sleepMgr.setRefreshSleepCallback(this.props.refreshSleepCallback);
  }

  componentWillUnmount() {
    console.log('HeaderMain componentWillUnmount');
  }

  removeCustomEventListeners() {
    console.log('HeaderMain removeCustomEventListeners');

    if (Platform.OS === 'ios') {
      this.handleAlarm.remove();
    }
    
    if (Platform.OS === 'android') {
      this.handleStationConnectionLost.remove();
      this.handleStationConnectionStatus.remove();
    }

    this.handleStationVolume.remove();
    this.handleStationLight.remove();
    this.handleStationBinaural.remove();
    this.handleBatteryStatus.remove();
    this.handleHeadsetStatus.remove();
    this.handleStationEquipmentState.remove();
  }

  componentDidMount() {
    lastRbsState = this.props.isRbsEnabled;

    this.handleHeadsetStatus = StationManagerEmitter.addListener('HeadsetStatusListener', res => {
      if (res === 1) {
        this.updateSettingsItemModule('headsetStatus', 'headset_active');
      } else {
        this.updateSettingsItemModule('headsetStatus', 'headset');
      }
    });

    this.handleBatteryStatus = StationManagerEmitter.addListener('StationBatteryStatusListener', batStatusValue => {
      this.changeBatStatusToActive(batStatusValue);
    });

    this.handleStationConnectionStatus = StationManagerEmitter.addListener('StationConnectionStatus2Listener', status => {
      console.log("StationConnectionStatus2Listener: " + status);

      if (status === 3) {
        if (this.state.sourceOfScan === WIFI_STATION) {
          this.enableAndSetBstation();
          this.updateSettingsItemModule('connectedPeripheral', BSTATION);
          this.updateSettingsItemModule('connectedPeripheralId', this.props.rememberedPeripheralId);
          if (this.props.rememberedAccessPoint !== '' && this.props.rememberedAccessPointPword !== '') {
            this.connectWifi(this.props.rememberedAccessPoint, this.props.rememberedAccessPointPword);
          } else {
            //  open connect input access points and its password
            this.openAccessPointPrompt();
          }
        } else {
          this.showSuccessPromptAndUpdateDeviceStatus();
        }
      } else if (status === 5) {
        if (this.state.sourceOfScan === WIFI_STATION) {
          this.setSourceOfScanToEmpty();
        }
        if (this.props.isBstationHeadSetEnabled) {
          this.setBluetoothModulesToDefault(BSTATION);
        }
        Toast.show(I18n.t('UNABLE_TO_CONNECT_DEVICE'), Toast.SHORT);

        this.setBinaural(0);
      }
    });
    if (Platform.OS === 'android') {
      this.handleStationConnectionLost = StationManagerEmitter.addListener('StationConnectionLost2Listener', status => {
        console.log("StationConnectionLost2Listener: " + status);
      });
    }

    if (Platform.OS === 'ios') {
      this.handleAlarm = AlarmManagerEmitter.addListener('AlarmSnoozeNotificationListener', result => {
        console.log("alarm123 AlarmSnoozeNotificationListener: ", JSON.stringify(result));
        savePreference('alarmIndex', JSON.stringify(result["alarm_index"]));
        setTimeout(() => {
          Actions.alarmActivity();
        }, 100)
      });
    }

    this.openGraph = DeviceEventEmitter.addListener('openGraph', graphNumber => {
      this.removeCustomEventListeners();

      switch (graphNumber) {

        case 0:
          Actions.brainwaveAnalysis();
          break;

        case 1:
          Actions.binauralBeat();
          break;

        case 2:
          Actions.alarm();
          break;
        
        case 3:
          Actions.foodRecord();
          break;
          
        case 4:
          Actions.waterRecord();
          break;

        case 5:
          Actions.caffeineRecord();
          break;

        case 6:
          Actions.alcoholRecord();
          break;

        case 7:
          Actions.cigaretteRecord();
          break;
      }

      if (graphNumber !== 2) {
        this.openGraph.remove();
      }
    });

    this.updateSettingsItemModule('sMBbState', this.props.sMBbState);
    this.updateSettingsItemModule('isRbsEnabled', this.props.isRbsEnabled);
  }

  handleStationVolumeControl(result) {
    console.log('station123 volResult: ' + result);

    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      this.updateSettingsItemModule('volume', result);
      this.updateSettingsItemModule('sMVolState', result === 0 ? false : true);
    }
  }

  handleStationLightControl(result) {
    console.log('station123 lightResult: ' + result);

    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      this.updateSettingsItemModule('stationLight', result);
      this.updateSettingsItemModule('sMLightState', result === 0 ? false : true);
    }
  }

  handleStationBinauralControl(result) {
    console.log('station123 binauralResult: ' + result);
    let newResult = result === 0 ? false : true;

    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      this.setBinaural(result);
    }

    console.log('handleStationBinauralControl lastRbsState: ' + lastRbsState);
    console.log('handleStationBinauralControl newResult: ' + newResult);
    if (this.props.setMarker !== undefined) {
      // we set the "rbs on/off" marker on the brainwave chart.
      if (lastRbsState !== newResult) {
        this.props.setMarker(newResult);
        lastRbsState = newResult;
      }
    }
  }

  handleStationEquipmentLookup(result) {
    console.log('sim123 equipmentLookupResult: ' + JSON.stringify(result));
    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      this.updateSettingsItemModule('stationLight', result.light);
      this.updateSettingsItemModule('sMLightState', result.light === 0 ? false : true);
      this.updateSettingsItemModule('volume', result.volume);
      this.updateSettingsItemModule('sMVolState', result.volume === 0 ? false : true);
      this.setBinaural(result.binaural);
    }
  }

  setBinaural(result) {
    this.manageBinauralSettings(result, this.updateSettingsItemModule, result === 0 ? false : true,
      this.props.binauralStart, this.updateManageBinaural);

    if (result === 0) {
      setTimeout(() => {
        if (this.props.refreshCallback !== undefined) {
          this.props.refreshCallback();
        }
      }, 2500);
    }
  }

  changeBatStatusToActive(batStatusValue) {
    if (batStatusValue === 0) {
      this.updateSettingsItemModule('batStatus', 'bat_active_empty');
      this.updateSettingsItemModule('batStatusInt', 0);
    } else if (batStatusValue === 1) {
      this.updateSettingsItemModule('batStatus', 'bat_active_1bar');
      this.updateSettingsItemModule('batStatusInt', 1);
    } else if (batStatusValue === 2) {
      this.updateSettingsItemModule('batStatus', 'bat_active_2bars');
      this.updateSettingsItemModule('batStatusInt', 2);
    } else if (batStatusValue === 3) {
      this.updateSettingsItemModule('batStatus', 'bat_active_3bars');
      this.updateSettingsItemModule('batStatusInt', 3);
    } else if (batStatusValue === 4) {
      this.updateSettingsItemModule('batStatus', 'bat_active_4bars');
      this.updateSettingsItemModule('batStatusInt', 4);
    } else if (batStatusValue === 5) {
      this.updateSettingsItemModule('batStatus', 'bat_full_active');
      this.updateSettingsItemModule('batStatusInt', 5);
    }
  }

  changeBatStatusToInactive(batStatusValue) {
    if (batStatusValue === 0) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_empty');
    } else if (batStatusValue === 1) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_1bar');
    } else if (batStatusValue === 2) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_2bars');
    } else if (batStatusValue === 3) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_3bars');
    } else if (batStatusValue === 4) {
      this.updateSettingsItemModule('batStatus', 'bat_inactive_4bars');
    } else if (batStatusValue === 5) {
      this.updateSettingsItemModule('batStatus', 'bat_full_inactive');
    }
  }

  checkIfBluetoothIsEnabled() {
    if (Platform.OS === 'android') {
      BleManager.enableBluetooth()
        .then(() => {
          // Success code
          console.log('The bluetooth is already enabled or the user confirm');
          Toast.show(I18n.t('CONNECTING_STATION'), Toast.LONG);
          this.connectStation();
        })
        .catch(error => {
          // Failure code
          console.log('The user refuse to enable bluetooth: ' + error);
        });
    } else {
      StationManagerModule.checkState(btState => {
        console.log("BT STATE: " + btState);
        if (btState === 'on') {
          Toast.show(I18n.t('CONNECTING_STATION'), Toast.LONG);
          this.connectStation();
        } else {
          this.openIosSettingsPrompt(true);
        }
      });
    }
  }

  openIosSettingsPrompt(visibility) {
    this.setState({ isToOpenIosSettingsPrompt: visibility });
  }

  openAccessories() {
    console.log('openAccessories');
    SMAModule.openBluetoothSettings(result => {
    });
  }

  connectStation() {
    if (Platform.OS === 'android') {
      BleManager.createBond(this.props.rememberedPeripheralId)
      .then(() => {
        console.log('createBond success or there is already an existing one');
        StationModule.connectStation(this.props.rememberedPeripheralId, "shortcut_menu");
      })
      .catch(error => {
        console.log(`Fail to bond: ${error}`);
      });
    } else {
      StationModule.connectStationShortcut(this.props.rememberedPeripheralId, response => {
        // console.log('connectDevice() device:' + response);
        if (response === true) {
          console.log('connectDevice() response:');
          this.showSuccessPromptAndUpdateDeviceStatus();
        } else {
          console.log('connectDevice() noresponse');
          if (response === 2) {
            Toast.show(I18n.t('UNABLE_TO_CONNECT_DEVICE'), Toast.SHORT);
          } else {
            Toast.show(response, Toast.SHORT);
          }
        }
      });
    }
  }

  setSourceOfScanToEmpty() {
    this.setState({ sourceOfScan: '' });
  }

  enableStation(isToEnable) {
    this.setState({ sourceOfScan: BSTATION });
    console.log("enableStation: " + isToEnable);

    if (this.props.connectedPeripheral === WRISTBAND) {
      Toast.show(I18n.t('DISCONNECT_WRISTBAND_FIRST'), Toast.SHORT);
    } else {
      if (isToEnable) {
        //  if there is peripheralId connect directly
        isCurrentlyConnected = false;

        if (this.props.rememberedPeripheralId !== '') {
          console.log("enableStation has this.props.rememberedPeripheralId");
          this.checkIfBluetoothIsEnabled();
        } else {
          console.log("enableStation has no this.props.rememberedPeripheralId");
          // else open station connect flow
          this.setState({
            isToScanBDevices: true, connectedDevice: this.props.connectedAccessPoint
          });
        }
      } else {
        console.log("enableStation else");
        // disconnect station
        this.disconnectDevice(BSTATION);
      }
    }
  }

  scanBDevicesPrompt(visibility) {
    this.setState({ isToScanBDevices: visibility });
    console.log(`scanBDevicesPrompt: ${this.state.isToScanBDevices}`);
  }

  setSettingsModuleSwitchMode(type, value) {
    switch (type) {
      case BSTATION:
        this.updateSettingsItemModule('isBstationHeadSetEnabled', value);
        break;
      case WIFI_STATION:
        this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, value);
        break;
      default:
        console.log('Connection type not found');
        break;
    }
  }

  scanAccessPtsPrompt(visibility) {
    this.setState({ isToScanAccessPoints: visibility });
    console.log(`isToScanAccessPoints: ${this.state.isToScanAccessPoints}`);
  }

  openConnectAccessPointPrompt(visibility) {
    this.setState({ isToConnectAccessPointPrompt: visibility });
    console.log(`isToConnectAccessPointPrompt: ${this.state.isToConnectAccessPointPrompt}`);
  }

  disconnectDevice(type) {
    console.log("disconnectDevice: " + type);
    const peripheralId = this.props.connectedPeripheralId;
    console.log("disconnectDevice peripheralId: " + peripheralId);

    if (peripheralId !== '') {
      if (type === BSTATION) {
        if (this.props.sMBbState) {
          this.setBinaural(0);
        }
        // for BSTATION ('bStation')
        if (Platform.OS === 'android') {
          console.log("disconnectDevice for android");
          StationModule.disconnectStation("shortcut_menu", peripheralId);
          this.setBluetoothModulesToDefault(type);
        } else {
          StationModule.disconnect(response => {
            console.log(`StationModule.disconnect(): ${response}`);
          });
          this.setBluetoothModulesToDefault(type);
        }
      } else {
        StationModule.forgetWifi(result => {
          console.log('disconnectWifi(): ', result);
        });
        this.setBluetoothModulesToDefault(type);
      }
    } else {
      //  console.log('Peripheral id not found. Make sure youre connected to a device');
      this.setBluetoothModulesToDefault(type);
    }
  }

  setBluetoothModulesToDefault(type) {
    if (type !== WIFI_STATION) {
      this.updateSettingsItemModule('connectedPeripheral', '');
      this.updateSettingsItemModule('connectedPeripheralId', '');
    }
    this.updateSettingsItemModule('connectedDevice', I18n.t('DEVICE_NAME'));

    switch (type) {
      case BSTATION:
        this.updateSettingsItemModule('connectedBStation', I18n.t('DEVICE_NAME'));
        this.updateSettingsItemModule('isBstationHeadSetEnabled', false);
        this.changeBatStatusToInactive(this.props.batStatusInt);
        this.updateSettingsItemModule('sMVolState', false);
        this.updateSettingsItemModule('sMLightState', false);
        this.updateSettingsItemModule('sMBbState', false);
        this.updateSettingsItemModule('isRbsEnabled', false);
        this.updateSettingsItemModule('headsetStatus', 'headset');
        break;
      case WIFI_STATION:
        this.updateSettingsItemModule('connectedAccessPoint', I18n.t('NETWORK_NAME'));
        this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, false);
        break;
      default:
        console.log('Connection type not found');
        break;
    }
  }

  showSuccessPromptAndUpdateDeviceStatus() {
    if (Platform.OS === 'android') {
      if (this.state.sourceOfScan !== WIFI_STATION) {
        if (!isCurrentlyConnected) {
          showAlert('', I18n.t('CONNECT_SUCCESSFUL'));
          isCurrentlyConnected = true;
        }
      }
    } else {
      if (this.state.sourceOfScan !== WIFI_STATION) {
        console.log('connectDevice(): openSuccessDialog()');
        this.openSuccessfullyConnectedDialog(true);
      }
    }
    console.log(`Type: ${this.state.sourceOfScan}`);
    if (this.state.sourceOfScan === WIFI_STATION) {
      this.updateSettingsItemModule('connectedPeripheral', BSTATION);
    } else {
      this.updateSettingsItemModule('connectedPeripheral', this.state.sourceOfScan);
    }
    this.updateSettingsItemModule('connectedPeripheralId', this.props.rememberedPeripheralId);
    this.updateSettingsItemModule('connectedDevice', this.props.rememberedPeripheralName);
    this.updateSettingsItemModule('rememberedPeripheralId', this.props.rememberedPeripheralId);
    this.updateSettingsItemModule('rememberedPeripheralName', this.props.rememberedPeripheralName);
    switch (this.state.sourceOfScan) {
      case BSTATION:
        this.enableAndSetBstation();
        StationModule.queryStationStatus();
        break;
      case WIFI_STATION:
        // open scan wifi
        this.enableAndSetBstation();
        if (Platform.OS === 'ios') {
          this.openConnectAccessPointPrompt(true);
        } else {
          this.scanAccessPtsPrompt();
        }
        break;
      default:
        console.log('Connection type not found');
        break;
    }
  }

  openSuccessfullyConnectedDialog(visibility) {
    this.setState({ isSuccessfullyConnected: visibility });
  }

  enableAndSetBstation() {
    this.updateSettingsItemModule('connectedBStation', this.props.rememberedPeripheralName);
    this.updateSettingsItemModule('isBstationHeadSetEnabled', true);
  }

  onConnectToStationAndScanWifi(isToEnable) {
    this.setState({ sourceOfScan: WIFI_STATION });
    console.log("onConnectToStationAndScanWifi isToEnable: " + isToEnable);

    if (this.props.connectedPeripheral === WRISTBAND && !this.props.isWstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      if (isToEnable) {
        //  if there is peripheralId connect directly
        if (this.props.rememberedPeripheralId !== '') {
          console.log("onConnectToStationAndScanWifi has rememberedPeripheralId");
          if (this.props.isBstationHeadSetEnabled) {
            if (this.props.rememberedAccessPoint !== '' && this.props.rememberedAccessPointPword !== '') {
              Toast.show(I18n.t('CONNECTING_STATION'), Toast.LONG);
              this.connectWifi(this.props.rememberedAccessPoint, this.props.rememberedAccessPointPword);
            } else {
              //  open connect input access points and its password
              this.openAccessPointPrompt();
            }
          } else {
            this.checkIfBluetoothIsEnabled();
          }
        } else {
          // else open station connect flo or show connect to bluetooth station
          if (this.props.isBstationHeadSetEnabled) {
            console.log("onConnectToStationAndScanWifi no rememberedPeripheralId if");
            this.openAccessPointPrompt();
          } else {
            console.log("onConnectToStationAndScanWifi no rememberedPeripheralId else");
            this.setState({
              isToScanBDevices: true, connectedDevice: this.props.connectedAccessPoint
            });
          }
        }
      } else {
        // disconnect station
        if (!this.props.isBstationHeadSetEnabled) {
          Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
        } else {
          this.disconnectDevice(WIFI_STATION);
          this.setSourceOfScanToEmpty();
        }
      }
    }
  }

  openAccessPointPrompt() {
    if (Platform.OS === 'android') {
      this.setState({ isToScanAccessPoints: true });
    } else {
      this.openConnectAccessPointPrompt(true);
    }
  }

  connectWifi(ssid, password) {
    StationModule.setWifi(ssid, password, result => {
      console.log('connectWifi result: ' + result);
      if (typeof result === 'string') {
        showAlert('', result);
      } else {
        if (result === 1) { // data = 1 for success; data = 0 for failed
          this.setAccessPointAndEnableWifiStation(ssid);
        } else {
          showAlert('', I18n.t('STATION_FAILED_CONNECT_TO_WIFI'));
        }
      }
    });
    this.setSourceOfScanToEmpty();
  }

  setAccessPointAndEnableWifiStation(ssid) {
    this.updateSettingsItemModule('connectedAccessPoint', ssid);
    this.updateSettingsItemModule(IS_WIFI_STATION_ENABLED, true);
  }

  enableVolume(isToEnable) {
    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      if (isToEnable) {
        if (typeof this.props.volume !== 'undefined' && this.props.volume !== 0) {
          //  set saved volume
          this.setStationVolume(this.props.volume, isToEnable);
        } else {
          // No volume saved for the first time, prompt to set his first volume in settings
          this.setStationVolume(10, isToEnable);
        }
      } else {
        //  set value to 0
        this.setStationVolume(0, isToEnable);
      }
    }
  }

  setStationVolume(volume, sMVolState) {
    StationModule.setVolume(volume, response => {
      console.log(`parent setVolume: ${response}`);
      if (typeof response !== 'boolean') {
        Toast.show(response, Toast.SHORT);
      } else {
        this.updateSettingsItemModule('volume', volume);
        this.updateSettingsItemModule('sMVolState', sMVolState);
      }
    });
  }

  //  For lights--------------------------
  enableLight(isToEnable) {
    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      if (isToEnable) {
        if (typeof this.props.stationLight !== 'undefined' && this.props.stationLight !== 0) {
          //  set saved light
          this.setStationLight(this.props.stationLight, isToEnable);
        } else {
          // No light saved for the first time, prompt to set his first light in settings
          this.setStationLight(5, isToEnable);
        }
      } else {
        //  set value to 0
        this.setStationLight(0, isToEnable);
      }
    }
  }

  setStationLight(lightValue, sMLightState) {
    StationModule.setLight(lightValue, response => {
      console.log(`parent setLight: ${response}`);
      if (typeof response !== 'boolean') {
        Toast.show(response, Toast.SHORT);
      } else {
        this.updateSettingsItemModule('stationLight', lightValue);
        this.updateSettingsItemModule('sMLightState', sMLightState);
      }
    });
  }

  //  For binaural--------------------
  enableBB(isToEnable) {
    if (!this.props.isBstationHeadSetEnabled) {
      Toast.show(I18n.t('CONNECT_STATION_TO_ENABLE_ITEM'), Toast.SHORT);
    } else {
      if (!this.state.isRbsAlreadyClicked) {

      let newResult = isToEnable;
      console.log('isRbsAlreadyClicked: true');
      this.setState({ isRbsAlreadyClicked: true });

      if (isToEnable) {
        this.setStationBB(1, isToEnable);
      } else {
        // set value to 0
        this.setStationBB(0, isToEnable);
        setTimeout(() => {
          if (this.props.refreshCallback !== undefined) {
            this.props.refreshCallback();
          }
        }, 2500);
      }

      if (this.props.setMarker !== undefined) {
        // we set the "rbs on/off" marker on the brainwave chart.
        if (lastRbsState !== newResult) {
          this.props.setMarker(newResult);
          lastRbsState = newResult;
        }
      }
     }
    }
  }

  setStationBB(bbValue, sMBbState) {
    StationModule.setBB(bbValue, true, response => {
      console.log(`parent setBB: ${response}`);

      if (response === true) {
        this.manageBinauralSettings(bbValue, this.updateSettingsItemModule, sMBbState,
          this.props.binauralStart, this.updateManageBinaural);
      } else {
        Toast.show(response, Toast.SHORT);
        setTimeout(() => {
          this.setState({ isRbsAlreadyClicked: false });
        }, 1500);
      }
    });
  }

  manageBinauralSettings(bbValue, updateSettingsItemModule, sMBbState,
    binauralStart, updateManageBinaural) {

    if (bbValue === 0) {
      updateManageBinaural(0);
      updateSettingsItemModule('isRbsEnabled', false);

      let timeDiff = new Date() - binauralStart;

      if (binauralStart === 0) {
        timeDiff = 0;
      }

      this.binaural.clearBinauralTimer();
    } else {
      updateManageBinaural(new Date());
      updateSettingsItemModule('isRbsEnabled', true);
      this.binaural.setBinauralTimer(new Date());
    }

    updateSettingsItemModule('sMBbState', sMBbState);
    setTimeout(() => {
      this.setState({ isRbsAlreadyClicked: false });
    }, 1500);
  }

  updateManageBinaural(val) {
    this.props.manageBinaural({ prop: 'binauralStart', value: val });
  }

  updateSettingsItemModule(property, value) {
    this.props.updateSettingsModule({ prop: property, value: value });
  }

  renderCenterComponent() {
    return (
      <Image style={{ height: height <= 640 ? 18 : 20, width: height <= 640 ? 135 : 145, marginLeft: 25 }} source={require('../../images/Logo-header.png')} />
    );
  }

  renderRightComponent() {
    return (
      <TouchableOpacity onPress={() => { Actions.settings() }} >
        <Image style={{
          height: 15,
          width: 15,
          padding: 10,
          marginRight: 15,
          ...Platform.select({
            android: {
              marginRight: 0
            }
          })
        }} source={require('../../images/shortcut_icons/settings.png')} />
      </TouchableOpacity>
    );
  }

  renderLeftComponent() {
    if (this.props.showLeft) {
      return (
        <TouchableOpacity onPress={() => { Actions.pop() }} >
          <Icon
            iconStyle={{ marginTop: 10 }}
            color='#979797'
            size={50}
            type='ionicon'
            name='ios-arrow-round-back'
          />
        </TouchableOpacity>
      );
    }
    return (
      <View></View>
    );
  }

  renderHeader() {
    if (this.props.hideTopHeader) {
      return (
        <View></View>
      );
    }

    return (
      <View style={{ paddingBottom: -4, ...Platform.select({ android: { marginTop: -12 } }) }}>
        <Header
          leftComponent={this.renderLeftComponent()}
          outerContainerStyles={{ position: 'relative', borderBottomColor: 'transparent' }} //, borderBottomColor: '#bbb'}
          centerComponent={this.renderCenterComponent()}
          rightComponent={this.renderRightComponent()}
        />
      </View>
    );
  }

  render() {
    return (
      <View>
        {this.renderHeader()}
        <View style={shortcutWrapper}>
          {
            !this.props.isBstationHeadSetEnabled &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => { this.enableStation(true); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/bluetooth_128.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            this.props.isBstationHeadSetEnabled &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => {
                this.enableStation(false);
              }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/bluetooth_128_on.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            !this.props.isWstationHeadSetEnabled &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => { this.onConnectToStationAndScanWifi(true); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/wifi_128.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            this.props.isWstationHeadSetEnabled &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => { this.onConnectToStationAndScanWifi(false); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/wifi_128_on.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            !this.props.sMVolState &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => { this.enableVolume(true); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/speaker_off.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            this.props.sMVolState &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => { this.enableVolume(false); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/speaker_on.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            !this.props.sMLightState &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => { this.enableLight(true); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/lights_off.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            this.props.sMLightState &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper} onPress={() => { this.enableLight(false); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/lights_on.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            !this.props.sMBbState &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper}
                disabled={this.state.isRbsAlreadyClicked}
                onPress={() => { 
                  console.log('isRbsAlreadyClicked press: true');
                  this.enableBB(true); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/binaural_off.png')} />
              </TouchableOpacity>
            </View>
          }
          {
            this.props.sMBbState &&
            <View style={shortcutContainer}>
              <TouchableOpacity style={touchWrapper}
                disabled={this.state.isRbsAlreadyClicked}
                onPress={() => { 
                  console.log('isRbsAlreadyClicked press: false');
                  this.enableBB(false); }}>
                <Image style={{ alignItems: 'center', height: 20, width: 20, marginTop: 2 }} source={require('../../images/shortcut_icons/binaural_on.png')} />
              </TouchableOpacity>
            </View>
          }
          <View style={shortcutContainer}>
            <TouchableOpacity style={touchWrapper}>
              <Image style={{ alignItems: 'center', height: 20, width: 20, marginLeft: 8 }}
                source={{ uri: this.props.batStatus }} />
            </TouchableOpacity>
          </View>
          <View style={shortcutContainer}>
            <TouchableOpacity style={touchWrapper}>
              <Image style={{ alignItems: 'center', height: 20, width: 20, marginLeft: 8 }}
                source={{ uri: this.props.headsetStatus }} />
            </TouchableOpacity>
          </View>
        </View>

        <ScanBDevicesPrompt
          type={this.state.sourceOfScan}
          visibility={this.state.isToScanBDevices}
          scanBDevicesPrompt={this.scanBDevicesPrompt}
          isToOpenBluetoothDevicesListing={false}
          updateSettingsItemModule={this.updateSettingsItemModule}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
          connectedDevice={this.props.connectedDevice}
          scanAccessPtsPrompt={this.scanAccessPtsPrompt}
          message={I18n.t('CONNECT_STATION')}
          rightBtnLabel={I18n.t('OK')}
          openConnectAccessPointPrompt={this.openConnectAccessPointPrompt}
          setBluetoothModulesToDefault={this.setBluetoothModulesToDefault}
          audioSelectorWifi={this.isWstationHeadSetEnabled}
          setSourceOfScanToEmpty={this.setSourceOfScanToEmpty}
          isFromSM={true}
          clearBinauralTimer={() => this.setBinaural(0)}
        />

        <ScanAccessPointsPrompt
          visibility={this.state.isToScanAccessPoints}
          scanAccessPtsPrompt={this.scanAccessPtsPrompt}
          isToOpenAccessPointsListing={false}
          updateSettingsItemModule={this.updateSettingsItemModule}
          connectedAccessPoint={this.props.connectedAccessPoint}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
          setSourceOfScanToEmpty={this.setSourceOfScanToEmpty}
          isFromSM={true}
        />

        <ConnectAccessPointPrompt
          visibility={this.state.isToConnectAccessPointPrompt}
          openConnectAccessPointPrompt={this.openConnectAccessPointPrompt}
          updateSettingsItemModule={this.updateSettingsItemModule}
          connectedAccessPoint={this.props.connectedAccessPoint}
          ssid={''}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
        />

        <CommonDialog
          visibility={this.state.isSuccessfullyConnected}
          message={I18n.t('CONNECT_SUCCESSFUL')}
          rightBtnLabel={I18n.t('OK')}
          dismissDialog={this.openSuccessfullyConnectedDialog}
        />

        <CommonDialog visibility={this.state.isToOpenIosSettingsPrompt}
          message={I18n.t('TURN_ON_BLUETOOTH_STATION')}
          leftBtnLabel={I18n.t('SETTINGS')}
          rightBtnLabel={I18n.t('OK')}
          leftBtnPress={this.leftBtnPress}
          rightBtnPress={() => { }}
          dismissDialog={this.openIosSettingsPrompt}
          isLeftBtnEnabled={true} />

      </View>
    );
  }
}

const mapStateToProps = state => {
  const {
    binauralStart
  } = state.binaural;

  const {
    targetBinauralHours,
    connectedPeripheralId,
    connectedPeripheral,
    isBstationHeadSetEnabled,
    isAudioSelectorEnabled,
    connectedBStation,
    audioSelected,
    isRbsEnabled,
    volume,
    volumeLastValue,
    stationLightLastValue,
    stationLight,
    connectedDevice,
    rememberedPeripheralId,
    rememberedPeripheralName,
    isWstationHeadSetEnabled,
    rememberedAccessPoint,
    rememberedAccessPointPword,
    batStatus,
    batStatusInt,
    sMVolState,
    sMLightState,
    sMBbState,
    headsetStatus
  } = state.settingsModule;


  return {
    targetBinauralHours,
    binauralStart,
    connectedPeripheralId,
    connectedPeripheral,
    isRbsEnabled,
    connectedBStation,
    isBstationHeadSetEnabled,
    isAudioSelectorEnabled,
    audioSelected,
    volume,
    volumeLastValue,
    stationLightLastValue,
    stationLight,
    connectedDevice,
    rememberedPeripheralId,
    rememberedPeripheralName,
    isWstationHeadSetEnabled,
    rememberedAccessPoint,
    rememberedAccessPointPword,
    batStatus,
    batStatusInt,
    sMVolState,
    sMLightState,
    sMBbState,
    headsetStatus
  };
};

HeaderMain.propTypes = {
  refreshCallback: PropTypes.func,
  refreshSleepCallback: PropTypes.func,
  setMarker: PropTypes.func
};

export default connect(mapStateToProps, { updateSettingsModule, manageBinaural })(HeaderMain);