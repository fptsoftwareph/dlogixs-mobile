import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Modal,
  Platform, PermissionsAndroid, NativeModules,
  NativeEventEmitter
} from 'react-native';
import { widgets } from '../../styles/widgets';
import { text } from '../../styles/text';
import { containers } from '../../styles/containers';
import { BluetoothDevicesListing } from './BluetoothDevicesListing';
import { CommonDialog } from './CommonDialog';
import BleManager from 'react-native-ble-manager';
import I18n from './../../translate/i18n/i18n';
import { SMAModule, StationModule } from 'nb_native_modules';
import Toast from 'react-native-simple-toast';
import { DEVICE_NAME, BSTATION, WRISTBAND, WIFI_STATION } from '../../utils/DataConstants';
import { showAlert } from '../../utils/CommonMethods';
import EasyBluetooth from 'easy-bluetooth-classic';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const SMAManagerModule = NativeModules.SMAModule;
const SMAManagerEmitter = new NativeEventEmitter(SMAManagerModule);

const StationManagerModule = NativeModules.StationModule;
const StationManagerEmitter = new NativeEventEmitter(StationManagerModule);

class ScanBDevicesPrompt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      isToOpenBluetoothDevicesListing: false,
      isToOpenIosSettingsPrompt: false,
      scanning: false,
      type: '',
      peripherals: new Map(),
      pairedPeripherals: new Map(),
      connectedDevice: '',
      message: '',
      rightBtnLabel: '',
      audioSelectorWifi: false,
      isFromSM: false,
      isFirstScanning: false
    };

    this.scanBDevicesPrompt = this.props.scanBDevicesPrompt;
    this.updateSettingsItemModule = this.props.updateSettingsItemModule;
    this.setSettingsModuleSwitchMode = this.props.setSettingsModuleSwitchMode;
    this.scanAccessPtsPrompt = this.props.scanAccessPtsPrompt;
    this.openConnectAccessPointPrompt = this.props.openConnectAccessPointPrompt;
    this.setBluetoothModulesToDefault = this.props.setBluetoothModulesToDefault;
    this.setSourceOfScanToEmpty = this.props.setSourceOfScanToEmpty;
    this.clearBinauralTimer = this.props.clearBinauralTimer;

    this.stopScan = this.stopScan.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.openBluetoothDevicesListing = this.openBluetoothDevicesListing.bind(this);
    this.scanBLEDevices = this.scanBLEDevices.bind(this);
    this.leftBtnPress = this.openAccessories.bind(this);
    this.rightBtnPress = this.setSwitchToDefaultIfNotConnected.bind(this);
    this.openIosSettingsPrompt = this.openIosSettingsPrompt.bind(this);
    this.removePeripherals = this.removePeripherals.bind(this);
  }

  setModalVisible(visibility) {
    this.scanBDevicesPrompt(visibility);
    if (this.state.isFromSM) {
      this.setSourceOfScanToEmpty();
    }
  }

  openBluetoothDevicesListing(visibility) {
    this.setState({ isToOpenBluetoothDevicesListing: visibility });
    console.log('isToOpenBluetoothDevicesListing: ' + this.state.isToOpenBluetoothDevicesListing);
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      SMAModule.start({ showAlert: false }, result => {
        if (result.includes('Please Enable Bluetooth')) {
          Toast.show(I18n.t('MAKE_SURE') + this.props.connectedDevice + I18n.t('IS_TURNED_ON'), Toast.SHORT);
          console.log('start() error: ' + result);
        } else if (result.includes('Successfully')) {
          Toast.show(result, Toast.SHORT);
        }
      });
      this.handlerStop = SMAManagerEmitter.addListener('SMAManagerStopScan', this.handleStopScan);
      this.handlerStop = StationManagerEmitter.addListener('StationManagerStopScan', this.handleStopScan);
    } else {
      BleManager.start({ showAlert: false });
      this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isVisible: nextProps.visibility,
      isToOpenBluetoothDevicesListing: nextProps.isToOpenBluetoothDevicesListing,
      type: nextProps.type,
      connectedDevice: nextProps.connectedDevice,
      message: nextProps.message,
      rightBtnLabel: nextProps.rightBtnLabel,
      audioSelectorWifi: nextProps.audioSelectorWifi,
      isFromSM: nextProps.isFromSM
    });

    //  console.log('scanbdevices type: ' + this.state.type);
  }

  componentWillUnmount() {
    this.handlerStop.remove();
  }

  setSwitchToDefaultIfNotConnected() {
    console.log('scanbdevices connectedDevice: ' + this.state.connectedDevice);
    if (this.state.connectedDevice === I18n.t('DEVICE_NAME')) {
      this.setSettingsModuleSwitchMode(this.state.type, false);
    }
  }

  render() {
    return (
      <View>
        <Modal animationType="slide"
          transparent={true}
          style={widgets.modal}
          visible={this.state.isVisible}
          presentation
          onRequestClose={() => {
            this.setSwitchToDefaultIfNotConnected();
            this.setModalVisible(false);
          }}>
          <View style={[widgets.bluetoothScanPrompt, containers.commonModalCon]}>
            <Text style={this.state.type === WIFI_STATION ? [widgets.commonMarginTop2, widgets.commonMarginRight, widgets.commonMarginLeft, text.centerText]
              : [text.bluetoothTitleTask]}>
              {this.state.type === WIFI_STATION ? this.state.message : I18n.t('SCAN_FOR_BT')}</Text>
            <View style={[widgets.commonConRow, widgets.commonMarginTop]}>
              <TouchableOpacity onPress={() => {
                this.setSwitchToDefaultIfNotConnected();
                this.setModalVisible(false);
              }}>
                <View style={[widgets.cancelBg, widgets.commonMarginRight]}>
                  <Text style={{ color: 'black' }}>{I18n.t('CANCEL')}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
                this.removePeripherals();
                this.scanBDevicesPrompt(false);
                setTimeout(() => {
                   this.startBLEManager();
                }, 1500);
              }}>
                <View style={[widgets.scanBg]}>
                  <Text style={{ color: 'white' }}>{this.state.type === WIFI_STATION ? this.state.rightBtnLabel : I18n.t('SCAN')}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <BluetoothDevicesListing type={this.state.type}
          openBluetoothDevicesListing={this.openBluetoothDevicesListing}
          visibility={this.state.isToOpenBluetoothDevicesListing}
          scanning={this.state.scanning}
          stopScan={this.stopScan}
          updateSettingsItemModule={this.updateSettingsItemModule}
          setSettingsModuleSwitchMode={this.setSettingsModuleSwitchMode}
          scanBLEDevices={this.scanBLEDevices}
          peripherals={this.state.peripherals}
          pairedPeripherals={this.state.pairedPeripherals}
          connectedDevice={this.props.connectedDevice}
          scanAccessPtsPrompt={this.scanAccessPtsPrompt}
          openConnectAccessPointPrompt={this.openConnectAccessPointPrompt}
          setBluetoothModulesToDefault={this.setBluetoothModulesToDefault}
          audioSelectorWifi={this.audioSelectorWifi}
          clearBinauralTimer={this.clearBinauralTimer} />
        <CommonDialog visibility={this.state.isToOpenIosSettingsPrompt}
          message={this.state.type === WRISTBAND ? I18n.t('TURN_ON_BLUETOOTH') : I18n.t('TURN_ON_BLUETOOTH_STATION')}
          leftBtnLabel={I18n.t('SETTINGS')}
          rightBtnLabel={I18n.t('OK')}
          leftBtnPress={this.leftBtnPress}
          rightBtnPress={this.rightBtnPress}
          dismissDialog={this.openIosSettingsPrompt}
          isLeftBtnEnabled={true} />
      </View>
    );
  }

  removePeripherals(){
    this.setState({
      peripherals: new Map()
    });
  }

  startBLEManager() {
    if (Platform.OS === 'android') {
      if (Platform.Version >= 23) {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
          if (result) {
            console.log('Permission is OK');
            this.enableBluetoothInAndroid();
          } else {
            PermissionsAndroid.requestMultiple(
              [PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
              PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION]).then(result2 => {
                if (result2) {
                  console.log('User accept');
                  this.enableBluetoothInAndroid();
                } else {
                  this.setSettingsModuleSwitchMode(this.state.type, false);
                }
              });
          }
        });
      } else {
        console.log('No permission required in this device.');
        this.enableBluetoothInAndroid();
      }
    } else {
      StationManagerModule.checkState(btState => {
        console.log("BT STATE: " + btState);
        if (btState === 'on') {
          this.scanBLEDevices();
        } else {
          this.openIosSettingsPrompt(true);
        }
      });
    }
  }

  scanBLEDevices() {
    //  console.log('scanBLEDevices');
    this.setState({
      pairedPeripherals: new Map()
    });

    Toast.show(I18n.t('SCANNING_PLEASE_WAIT'), Toast.SHORT);

    if (Platform.OS === 'android') {
      BleManager.start({ showAlert: false });
      BleManager.scan([], 5, false)
        .then((results) => {
          // Success code
          this.setState({ scanning: true, isFirstScanning: this.state.isFirstScanning ? false : true });

          //  console.log('Scan results: ' + JSON.stringify(results));
          this.openBluetoothDevicesListing(true);
        }).catch(error => {
          console.log('Scan err: ' + error);
        });

      const config = {
          "uuid": "00001101-0000-1000-8000-00805f9b34fb",
          "deviceName": "NeuroBeat",
          "bufferSize": 1024,
          "characterDelimiter": "\n"
      }
    
      EasyBluetooth.init(config)
      .then(config => {
        //  console.log("EasyBluetooth config done!");
        EasyBluetooth.startScan()
        .then(devices => {
          /*  console.log("EasyBluetooth all devices found:");
          console.log("EasyBluetooth scanned devices: " + JSON.stringify(devices));*/
        })
        .catch(ex => {
          console.warn("EasyBluetooth startScan catch: " + ex);
        });
      }).catch(ex => {
        console.warn("EasyBluetooth init catch: " + ex);
      });
    } else {
      console.log('ScanBDevicesPrompt scanBLEDevices() TYPE: ' + this.state.type);
      if (this.state.type === WRISTBAND) {
        SMAModule.scan([], 10, false, {}, success => {
          this.setState({ scanning: true });
          //  console.log('Scan results: ' + JSON.stringify(success));
          this.openBluetoothDevicesListing(true);
        });
      } else {
        StationModule.scan([], 5, true, {}, success => {
          this.setState({ scanning: true });
          //  console.log('Scan results: ' + JSON.stringify(success));
          this.openBluetoothDevicesListing(true);
        });
      }
    }
  }

  stopScan() {
    //  console.log('ScanBDevicesPrompt stopScan() TYPE: ', this.state.type);
    if (Platform.OS === 'android') {
      BleManager.stopScan()
        .then(() => {
          //  console.log('Scan stopped');
          EasyBluetooth.stopScan();
        }).catch(error => {
          console.log('stopScan err: ' + error);
        });
    } else {
      if (this.state.type === BSTATION || this.state.type === WIFI_STATION) {
        StationModule.stopScan();
      } else if (this.state.type === WRISTBAND) {
        SMAModule.stopScan(result => {
          console.log('Scan stopped');
        });
      }
    }
  }

  handleStopScan() {
    //  console.log('bt123 stoppedScan');
    Toast.show(I18n.t('SCAN_COMPLETED'), Toast.SHORT);
    this.setState({ scanning: false });

    if (Platform.OS === 'android' && Platform.Version >= 23 && this.state.peripherals.size === 0) {
      showAlert('', I18n.t('BT_LOC_NOT_FOUND'));
    }
    if (Platform.OS === 'android') {
      if (this.state.isFirstScanning) {
        this.setState({ isFirstScanning: true });
        this.scanBLEDevices();
      }
    }
  }

  //  Android only
  enableBluetoothInAndroid() {
    BleManager.enableBluetooth()
      .then(() => {
        // Success code
        console.log('The bluetooth is already enabled or the user confirm');
        this.scanBLEDevices();
      })
      .catch(error => {
        // Failure code
        console.log('The user refuse to enable bluetooth: ' + error);
        this.setSettingsModuleSwitchMode(this.state.type, false);
      });
  }

  openAccessories() {
    console.log('openAccessories');
    SMAModule.openBluetoothSettings(result => {

    });
  }

  openIosSettingsPrompt(visibility) {
    this.setState({ isToOpenIosSettingsPrompt: visibility });
  }
}

export { ScanBDevicesPrompt };

