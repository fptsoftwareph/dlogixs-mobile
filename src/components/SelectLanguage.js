import React, { Component } from 'react';
import {
  View, Text, Picker, Platform, NativeEventEmitter,
  NativeModules, DeviceEventEmitter
} from 'react-native';
import { savePreference, setLocalization, getPreference, getBooleanPreference } from '../utils/CommonMethods';
import Header from './Header';
import I18n from './../translate/i18n/i18n';
import { Actions } from 'react-native-router-flux';
import { widgets } from '../styles/widgets';
import { updateSettingsModule } from '../redux/actions';
import { connect } from 'react-redux'
// import { SleepModule } from 'nb_native_modules';


class SelectLanguage extends Component {
  constructor() {
    super();
    this.state = { language: 'english' };
  }

  componentWillMount() {
    console.log('SelectLanguage componentWillMount');
  }

  componentDidMount() {
    console.log('SelectLanguage componentDidMount');

    getPreference('language').then(response => {
      console.log(`SelectLang lang: ${response}`);
      this.setState({ language: response });
      setLocalization(response);
    }).catch(error => console.log(error));
  }

  shouldComponentUpdate() {
    //  First Called when there are new states or new peripherals discovered
    //  Return true to re-render and called the below lifecycle methods
    console.log('SelectLanguage shouldComponentUpdate');
    return true;
  }

  componentWillUpdate() {
    //  Called when there are new states or new peripherals discovered
    console.log('SelectLanguage ------------------componentWillUpdate----------------');
  }

  componentDidUpdate() {
    console.log('SelectLanguage componentDidUpdate');
    /* getPreference('language').then(response => {
      console.log(`SelectLang lang: ${response}`);
      this.setState({ language: response });
      this.setLocalization(response);
    });*/
  }

  componentWillUnmount() {
    console.log('SelectLanguage componentWillUnmount');
  }

  render() {
    return (
      <View style={widgets.commonCon}>
        <Header></Header>
        <Text style={widgets.selectLangTitle}>{I18n.t('SELECT_LANGUAGE')}</Text>
        <Picker
          style={widgets.dropdownPadding}
          selectedValue={this.state.language}
          onValueChange={lang => {
            setTimeout(() => {
              setLocalization(lang);
              this.setState({ language: lang });
              savePreference('language', lang);
              this.props.updateSettingsModule({ prop: 'connectedBStation', value: I18n.t('DEVICE_NAME') });
              this.props.updateSettingsModule({ prop: 'connectedWristband', value: I18n.t('DEVICE_NAME') });
              this.props.updateSettingsModule({ prop: 'connectedAccessPoint', value: I18n.t('NETWORK_NAME') });
              this.props.updateSettingsModule({ prop: 'connectedDevice', value: I18n.t('DEVICE_NAME') });
              console.log(`Language selected: ${lang}`);
            }, 100);
          }}>
          <Picker.Item label={I18n.t('ENGLISH')} value="english" />
          <Picker.Item label={I18n.t('KOREAN')} value="korean" />
          <Picker.Item label={I18n.t('CHINESE')} value="chinese" />
          <Picker.Item label={I18n.t('JAPANESE')} value="japanese" />
        </Picker>

        <View style={widgets.alignBottom}>
          <Text style={widgets.continueBtn}
            onPress={onClickContinue}>{I18n.t('CONTINUE')}</Text>
        </View>
      </View>

    );
  }
}

const onClickContinue = () => {
  getBooleanPreference('isAlreadyPickLanguage')
    .then(response => {
      console.log(`isAlreadyPickLanguage SelectLanguage value: ${response}`);

      if (!response) {
        savePreference('isAlreadyPickLanguage', JSON.stringify(true));
      }
    })
    .catch(error => console.log(error));

  Actions.login();
};


const mapStateToProps = (state) => {
  const { defaultLanguage } = state.settingsModule;

  return {
    defaultLanguage
  };
};


export default connect(mapStateToProps, { updateSettingsModule })(SelectLanguage); 
