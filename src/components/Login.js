import React, { Component } from 'react';
import {
  View, Text, Image, TextInput, TouchableOpacity, Platform, NativeEventEmitter,
  NativeModules, DeviceEventEmitter
} from 'react-native';
import I18n from './../translate/i18n/i18n';
import { widgets } from '../styles/widgets';
import { SOCIAL_LOGIN_ICON_COLOR } from '../styles/colors';
import Header from './Header';
import FBSDK from 'react-native-fbsdk';
import InstagramLogin from 'react-native-instagram-login';
import { GoogleSignin } from 'react-native-google-signin';
import { Icon } from 'react-native-elements';
import { RNTwitterSignIn } from 'NativeModules';
import { Actions } from 'react-native-router-flux';
import { savePreference, getPreference } from '../utils/CommonMethods';
import Couchbase from 'react-native-couchbase-lite';
import CouchbaseManager from '../utils/CouchbaseManager';
import { showAlert } from '../utils/CommonMethods';
import { connect } from 'react-redux';
import RenderIf from './RenderIf';
import KakaoSignin from 'react-native-kakao-signin';
import Auth0 from 'react-native-auth0';
import axios from 'axios';
import EventEmitter from 'EventEmitter';
import { APIClient } from '../utils/APIClient';
import { createLoginSession, updateUPModule, updateSettingsModule } from '../redux/actions';
import Session from '../utils/Session';
import * as WeChat from 'react-native-wechat';
import { FB_SWITCH_ACCT_ERR } from '../utils/DataConstants';
import { ReachabilityModule } from 'nb_native_modules';

const NetStatusManager = NativeModules.ReachabilityModule;
const ReachabilityManagerEmitter = new NativeEventEmitter(NetStatusManager);

const { GraphRequest, GraphRequestManager } = FBSDK;
const API_Constants = require('../config/api_keys.json');
const auth0 = new Auth0({ domain: API_Constants.YAHOO_DOMAIN, clientId: API_Constants.YAHOO_CLIENT_ID });
const getPreferenceEmitter = new EventEmitter();

let userNameOrId = '', passwordOrName = '', loginType = '';

class Login extends Component {

  constructor() {
    super();

    this.state = { language: '', nBUserNameOrEmail: '', nBPassword: ''};

    apiClient = new APIClient();

    this.createLoginSession = this.createLoginSession.bind(this);
  }

  getPreferenceHandler(arg) {
    console.log(`Login lang arg: ${arg}`);
    getPreference('language').then(response => {
      console.log(`Login lang: ${response}`);
      if (this.isComponentVisible) this.setState({ language: response });
    });
  }

  componentWillMount() {
    console.log(`Logged user: ${this.props.userId}`);
    RNTwitterSignIn.init(API_Constants.TWITTER_COMSUMER_KEY, API_Constants.TWITTER_CONSUMER_SECRET);
    this.handleNetReachability = ReachabilityManagerEmitter.addListener('ReachabilityStatusListener', reachabilityResult => {
      console.log('handleNetReachability: ' + JSON.stringify(reachabilityResult));
      this.props.updateSettingsModule({
        prop: 'connectionStatus', value: reachabilityResult.REACHABILITY_MODE
      });
    });
    NetStatusManager.startNetworkNotifier();
  }

  componentWillUnmount() {
    this.isComponentVisible = false;
    clearInterval(this.checkLoginTimer);
    this.getPreferenceEmitterObj.remove();
  }

  componentDidMount() {
    this.isComponentVisible = true;
    this.getPreferenceEmitterObj = getPreferenceEmitter.addListener('GetPreference', this.getPreferenceHandler.bind(this));
    getPreferenceEmitter.emit('GetPreference', 'GetPreferenceArg');
  }

  createLoginSession(userId) {
    let p = '';

    if (loginType === 'neurobeat') {
      p = passwordOrName;
    } else {
      p = userId;
    }

    Couchbase.initRESTClient(mgr => {
      CouchbaseManager.init(mgr);

        savePreference('userId', userId);
        savePreference('password', p);
           
          console.log('Login uploadMode: ' + this.props.uploadMode);
          console.log('Login connectionStatus: ' + this.props.connectionStatus);

            if (this.props.uploadMode === 0) {
              // console.log('net123 this.props.uploadMode === 0');
              if (this.props.connectionStatus === 1) {
                // console.log('net123 this.props.connectionStatus === 1)');
                // auto-sync should be enabled
                CouchbaseManager.enableAutosync(userId, p, () => {});
              } else {
                // console.log('net123 this.props.connectionStatus !== 1');
                // auto-sync should be disabled. Destroy sync-gateway session
                CouchbaseManager.disableAutosync(userId, p, () => {}, true);
              }
            } else {
              // console.log('net123 this.props.uploadMode !== 0');
              if (this.props.connectionStatus === 2 || this.props.connectionStatus === 1) {
                // console.log('net123 this.props.connectionStatus === 2 || this.props.connectionStatus === 1');
                CouchbaseManager.enableAutosync(userId, p, () => {});
              } else {
                // console.log('net123 reachabilityResult2: ' );
                // connectionType must be none - meaning lte or wifi isn't turned on, then in this case auto-sync should be disabled.
                CouchbaseManager.disableAutosync(userId, p, () => {}, true);
              }
            }

      if (this.checkLoginTimer !== undefined) {
        clearInterval(this.checkLoginTimer);
      }
      this.checkLoginTimer = setInterval(() => {
        console.log('CouchbaseManager.checkLogin(): ' + CouchbaseManager.checkLogin());
        if (CouchbaseManager.checkLogin()) {
          clearInterval(this.checkLoginTimer);
          getPreference('isLoginViaRegistration').then(res => {
            this.props.createLoginSession(userNameOrId, passwordOrName, loginType);
            if (JSON.parse(res)) {
              Actions.intro();
            } else {
              Actions.homePage();
              this.handleNetReachability.remove();
            }
          }).catch(err => {
            Actions.homePage();
            this.handleNetReachability.remove();
          });
        }
      }, 1);

    });
  }

  populateCredentials(response) {
    this.props.updateUPModule({ prop: 'onFirstNameChange', value: response['name'] });
    this.props.updateUPModule({ prop: 'onEmailChange', value: response['email'] });
    this.props.updateUPModule({ prop: 'onBirthdateChange', value: response['birthdate'] });
    this.props.updateUPModule({ prop: 'onOccupationChange', value: response['occupation'] });

    if (response['country'] === "") {
      this.props.updateUPModule({ prop: 'onCountryChange', value: I18n.t('COUNTRY') });
      console.log('tae:', typeof response['country']);
    }
    else {
      this.props.updateUPModule({ prop: 'onCountryChange', value: response['country'] });
      console.log('tae:', typeof response['country']);
    }

    if (response['gender'] === "") {
      this.props.updateUPModule({ prop: 'onGenderChange', value: I18n.t('GENDER') });
    }
    else {
      this.props.updateUPModule({ prop: 'onGenderChange', value: response['gender'] });
    }

    if (response['weight'] === 0) {
      this.props.updateUPModule({ prop: 'onWeightChange', value: this.props.onWeightChange });
    }
    else {
      this.props.updateUPModule({ prop: 'onWeightChange', value: response['weight'].toString() });
    }

    if (response['height'] === 0) {
      this.props.updateUPModule({ prop: 'onHeightChange', value: this.props.onHeightChange });
    }
    else {
      this.props.updateUPModule({ prop: 'onHeightChange', value: response['height'].toString() });
    }

  }

  signInTwitter() {
    RNTwitterSignIn.logIn()
      .then(response => {
        console.log(`Social provider user data twitter: ${JSON.stringify(response)}`);
        //  email is null
        const { authToken, authTokenSecret, name, userName, userID } = response;
        console.log(`Name: ${name}`);
        if (authToken && authTokenSecret) {
          userNameOrId = userID, passwordOrName = name, loginType = 'twitter';
          apiClient.signIn(userNameOrId, passwordOrName, loginType, this.createLoginSession)
            .then(response => {
              console.log('RESPONSE_NI_BAY :', response);
              this.populateCredentials(response);
            })
            .catch(response => {
              console.log(`signIn err response: ${JSON.stringify(response)}`);
              showAlert(I18n.t('ERROR'), response.request._response);
            });
        }
      })
      .catch(error => {
        console.log(`${error}`);
        //showAlert(I18n.t('ERROR'), `${error}`);
      });
  }

  signInFacebook() {
    const { LoginManager } = FBSDK;
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      response => {
        if (response.isCancelled) {
          // showAlert('', 'Login cancelled');
          console.log('signInFacebook', 'Login cancelled');
        } else {
          console.log('signInFacebook', `Login success with permissions: ${response.grantedPermissions.toString()}`);
          console.log(`Social provider user data fb: ${JSON.stringify(response)}`);

          const fbUserInfoCallback = (error, response2) => {
            if (error) {
              console.log(`Error fetching data: ${JSON.stringify(error)}`);
              showAlert(I18n.t('ERROR'), `${JSON.stringify(error)}`);
            } else {
              console.log(`Success fetching data: ${JSON.stringify(response2)}`);
              console.log(`${response2.id} ${response2.name}`);
              userNameOrId = response2.id, passwordOrName = response2.name, loginType = 'facebook';

              apiClient.signIn(userNameOrId, passwordOrName, loginType, this.createLoginSession)
                .then(response => {
                  console.log('RESPONSE_NI_BAY :', response);
                  this.populateCredentials(response);

                })
                .catch(response => {
                  console.log(`signIn err response: ${JSON.stringify(response)}`);
                  showAlert(I18n.t('ERROR'), response.request._response);
                });
            }
          };

          // Start the graph request.
          const infoRequest = new GraphRequest('/me', null, fbUserInfoCallback);

          new GraphRequestManager().addRequest(infoRequest).start();
        }
      },
      error => {
        console.log('signInFacebook', error.message);
        if (error.message === FB_SWITCH_ACCT_ERR) {
          LoginManager.logOut();
          this.signInFacebook();
        } else {
          showAlert(I18n.t('ERROR'), error.message);
        }
      }
    );
  }

  onClickSignup() {
    Actions.signup();
  }

  onClickSignin() {

    if (this.state.nBUserNameOrEmail !== '' && this.state.nBPassword !== '') {
      userNameOrId = this.state.nBUserNameOrEmail, passwordOrName = this.state.nBPassword, loginType = 'neurobeat';
      apiClient.signIn(userNameOrId.trim(), passwordOrName, loginType, this.createLoginSession)
        .then(response => {
          console.log('RESPONSE_NI_BAY :', response);
          this.populateCredentials(response);
        })
        .catch(response => {
          console.log(`signIn err response: ${JSON.stringify(response)}`);
          showAlert(I18n.t('ERROR'), response.request._response);
        });
    } else {
      showAlert(I18n.t('ERROR'), I18n.t('ALL_FIELDS_REQUIRED'));
    }
  }

  signInGoogle() {
    GoogleSignin.hasPlayServices({ autoResolve: true })
      .then(() => {
        // play services are available. can now configure library
        GoogleSignin.configure({
          iosClientId: API_Constants.GOOGLE_CLIENT_ID // I used my jsonized file instead
          //  scopes: ['email', 'profile'], offlineAccess: true
        }).then(() => {
          // you can now call currentUserAsync()
          GoogleSignin.signIn()
            .then(response => {
              console.log('Success login');
              console.log(`Social provider user data google: ${JSON.stringify(response)}`);
              console.log(`User name: ${response.name}`);
              console.log(`User email: ${response.email}`);
              // response.id, response.givenName & response.familyName for android only, response.photo
              userNameOrId = response.id, passwordOrName = response.name, loginType = 'google';
              apiClient.signIn(userNameOrId, passwordOrName, loginType, this.createLoginSession)
                .then(response => {
                  console.log('RESPONSE_NI_BAY :', response);
                  this.populateCredentials(response);

                })
                .catch(response => {
                  console.log(`signIn err response: ${JSON.stringify(response)}`);
                  showAlert(I18n.t('ERROR'), response.request._response);
                });
            })
            .catch(error => {
              console.log('Fail signIn', `${error}`);
              //showAlert(I18n.t('ERROR'), `${error}`);
            })
            .done();
        });
      })
      .catch(err => {
        console.log('Play services error', err.code, err.message);
        showAlert('', 'Play services error');
      });
  }

  async signInKakao() {
    try {
      const res = await KakaoSignin.signIn();
      if (res !== undefined) {
        const response = await res.json();
        const email = response.kaccount_email;
        const nickname = response.properties.nickname;
        const id = response.id;
        console.log(`Social provider user data kakao: ${JSON.stringify(response)}`);
        console.log('signInKakao resBody email', email);
        console.log('signInKakao resBody nickname', nickname);
        userNameOrId = id, passwordOrName = nickname, loginType = 'kakao';
        apiClient.signIn(userNameOrId, passwordOrName, loginType, this.createLoginSession)
          .then(response => {
            console.log('RESPONSE_NI_BAY :', response);
            this.populateCredentials(response);

          })
          .catch(response => {
            console.log(`signIn err response: ${JSON.stringify(response)}`);
            showAlert(I18n.t('ERROR'), response.request._response);
          });
      }
    } catch (error) {
      // Error handle..
      console.log('signInKakao login error!!', `${error}`);
      showAlert(I18n.t('ERROR'), `${error}`);
    }
  }

  signInYahoo() {
    auth0.webAuth
      .authorize({ scope: 'openid profile email', audience: `https://${API_Constants.YAHOO_DOMAIN}/userinfo` })
      .then(response => {
        console.log(`Social provider user data yahoo: ${JSON.stringify(response)}`);
        auth0.auth
          .userInfo({ token: response.accessToken })
          .then(userInfo => {
            // userInfo.sub, userInfo.nickname, userInfo.name, userInfo.picture
            userNameOrId = userInfo.sub, passwordOrName = userInfo.name, loginType = 'yahoo';
            apiClient.signIn(userNameOrId, passwordOrName, loginType, this.createLoginSession)
              .then(response => {
                console.log('RESPONSE_NI_BAY :', response);
                this.populateCredentials(response);

              })
              .catch(response => {
                console.log(`signIn err response: ${JSON.stringify(response)}`);
                showAlert(I18n.t('ERROR'), response.request._response);
              });
          })
          .catch(error => {
            console.log(error);
            //showAlert(I18n.t('ERROR'), error.message);
          });
      })
      .catch(error => {
        console.log(error);
        //showAlert(I18n.t('ERROR'), JSON.stringify(error));
      });
  }

  signInWechat() {
    console.log('signInWechat');

    WeChat.registerApp(API_Constants.WECHAT_APP_ID).then(response => {
      console.log('registerApp response: ' + JSON.stringify(response));

      if (Platform.OS === 'android') {
        console.log('sendAuthRequestWC android');
        this.sendAuthRequestWC();
      } else {
        console.log('isWXAppInstalled checker');
        WeChat.isWXAppInstalled()
          .then(isInstalled => {
            console.log('isInstalled response: ' + JSON.stringify(isInstalled));
            if (isInstalled) {
              this.sendAuthRequestWC();
            } else {
              showAlert(I18n.t('WECHAT_NOT_FOUND'), I18n.t('INSTALL_WECHAT'));
            }
          }).catch(error => {
            console.log('isWXAppInstalled error: ' + JSON.stringify(error));
            showAlert(I18n.t('ERROR'), error.message);
          });
      }
    }).catch(error => {
      console.log('registerApp error: ' + JSON.stringify(error));
      showAlert(I18n.t('ERROR'), error.message);
    });
  }

  sendAuthRequestWC() {
    console.log('sendAuthRequestWC');
    WeChat.sendAuthRequest('snsapi_userinfo')
      .then(response2 => {
        console.log('WeChat.sendAuthRequest response: ' + JSON.stringify(response2));
      
        const accessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + API_Constants.WECHAT_APP_ID +
        "&secret=" + API_Constants.WECHAT_APP_SECRET + "&code=" + response2.code + "&grant_type=authorization_code";
        console.log('accessTokenUrl: ' + accessTokenUrl);
      //  Get the accessToken
        axios.get(accessTokenUrl)
      .then(response3 => {
        console.log(`WeChat accessTokenInfo: ${JSON.stringify(response3)}`);
        
        const userInfoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token="+ response3.data.access_token + "&openid=" + response3.data.openid;
        console.log('userInfoUrl: ' + userInfoUrl);
        //  Get the userInfo
        axios.get(userInfoUrl)
         .then(response4 => {
           console.log(`WeChat userInfo: ${JSON.stringify(response4)}`);
           
           userNameOrId = response4.data.openid, passwordOrName = response4.data.nickname, loginType = 'wechat';
           apiClient.signIn(userNameOrId, passwordOrName, loginType, this.createLoginSession)
             .then(response => {
               console.log('apiClient.signIn response: ' + JSON.stringify(response));
               this.populateCredentials(response);
             })
             .catch(response => {
              console.log('apiClient.signIn catch err: ' + JSON.stringify(response));
             });
         })
         .catch(error4 => {
           console.log('WeChat getUserInfo catch err: ' + JSON.stringify(error4));
         });

      })
      .catch(error3 => {
        console.log('WeChat getAccessToken catch err: ' + JSON.stringify(error3));
      });
        
      }).catch(error2 => {
        console.log('WeChat.sendAuthRequest catch err: ' + JSON.stringify(error2));
      });
  }

  render() {
    return (
      <View style={widgets.commonCon}>
        <View>
          <Header />
        </View>
        <View style={widgets.commonCon2}>
          <View style={widgets.idPassCon}>
            <Icon color={SOCIAL_LOGIN_ICON_COLOR} size={20} type="font-awesome" name="user-o" />
            <TextInput placeholder={I18n.t('EMAIL_ID')} style={widgets.loginTextInput}
              onChangeText={(nBUserNameOrEmail) => this.setState({ nBUserNameOrEmail })}
              value={this.state.nBUserNameOrEmail} />
          </View>
          <View style={widgets.idPassCon}>
            <Icon color={SOCIAL_LOGIN_ICON_COLOR} size={20} type="font-awesome" name="lock" />
            <TextInput style={widgets.loginTextInput} placeholder={I18n.t('PASSWORD')} secureTextEntry={true}
              onChangeText={(nBPassword) => this.setState({ nBPassword })}
              value={this.state.nBPassword} />
          </View>
          <View style={{ marginTop: 10 }}>
            <TouchableOpacity onPress={() => this.onClickSignin()}>
              <Text style={widgets.loginBtn}>{I18n.t('SIGN_IN')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onClickSignup()}>
              <Text style={widgets.loginBtn}>{I18n.t('SIGN_UP')}</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => Actions.recoverEmail()}>
            <Text style={widgets.centerText}>{I18n.t('RECOVER_EMAIL')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => Actions.resetPassword()} style={{ marginTop: 10 }}>
            <Text style={widgets.centerText}>{I18n.t('RESET_PASSWORD')}</Text>
          </TouchableOpacity>
          <View style={widgets.commonMarginTop}>
            <Text style={widgets.centerText}>{I18n.t('SIGN_IN_USING')}</Text>
          </View>
          <View style={widgets.loginSocialBtnCon}>
            {RenderIf(this.state.language !== 'chinese')(
              <View style={widgets.commonConRow}>
                <TouchableOpacity onPress={this.signInFacebook.bind(this)}
                  style={widgets.loginSocialBtnOpacity}>
                  <Icon color={SOCIAL_LOGIN_ICON_COLOR} size={20} type="font-awesome" name="facebook" />
                </TouchableOpacity>
                <View style={widgets.commonMarginLeft2}>
                  <TouchableOpacity onPress={this.signInGoogle.bind(this)}
                    style={widgets.loginSocialBtnOpacity}>
                    <Icon color={SOCIAL_LOGIN_ICON_COLOR} size={20} type="font-awesome" name="google" />
                  </TouchableOpacity>
                </View>
              </View>
            )}
            {RenderIf(this.state.language === 'english')(
              <View style={[widgets.commonConRow, widgets.commonMarginLeft2]}>
                <TouchableOpacity onPress={this.signInTwitter.bind(this)}
                  style={widgets.loginSocialBtnOpacity}>
                  <Icon size={20} color={SOCIAL_LOGIN_ICON_COLOR} type="font-awesome" name="twitter" />
                </TouchableOpacity>
                <View style={widgets.commonMarginLeft2}>
                  <TouchableOpacity
                    style={widgets.loginSocialBtnOpacity}
                    onPress={() => this.refs.instagramLogin.show()}
                  >
                    <Icon size={20} color={SOCIAL_LOGIN_ICON_COLOR} type="font-awesome" name="instagram" />
                  </TouchableOpacity>
                </View>
              </View>
            )}
            {RenderIf(this.state.language === 'korean')(
              <View style={[widgets.commonConRow, widgets.commonMarginLeft2]}>
                <TouchableOpacity
                  onPress={() => {
                    this.signInKakao();
                    console.log('kakao');
                  }}
                  style={widgets.loginSocialBtnOpacity}
                >
                  <Image style={widgets.settingsModuleIcon} source={require('../images/kakao_32.png')} />
                </TouchableOpacity>
              </View>
            )}
            {RenderIf(this.state.language === 'japanese')(
              <View style={[widgets.commonConRow, widgets.commonMarginLeft2]}>
                <TouchableOpacity
                  onPress={() => {
                    this.signInYahoo();
                    console.log('yahoo');
                  }}
                  style={widgets.loginSocialBtnOpacity}
                >
                  <Icon size={20} color={SOCIAL_LOGIN_ICON_COLOR} type="ionicon" name="logo-yahoo" />
                </TouchableOpacity>
              </View>
            )}
            {RenderIf(this.state.language === 'chinese')(
              <View style={[widgets.commonConRow, widgets.commonMarginLeft2]}>
                <TouchableOpacity
                  onPress={() => {
                    this.signInWechat();
                  }}
                  style={widgets.loginSocialBtnOpacity}
                >
                  <Icon size={20} color={SOCIAL_LOGIN_ICON_COLOR} type="font-awesome" name="wechat" />
                </TouchableOpacity>
              </View>
            )}
            <InstagramLogin
              ref="instagramLogin"
              redirectUrl="http://dlogixs.com/"
              clientId={API_Constants.INSTAGRAM_CLIENT_ID}
              onLoginSuccess={response => {
                // get info by response
                console.log(`Social provider user data insta: ${JSON.stringify(response)}`);
                axios
                  .get(`https://api.instagram.com/v1/users/self/?access_token=${response}`)
                  .then(response2 => {
                    console.log(`${JSON.stringify(response2)}`);
                    console.log(`User id: ${response2.data.data.id}`);
                    // response.data.data.username, response.data.data.profile_picture
                    userNameOrId = response2.data.data.id, passwordOrName = response2.data.data.full_name, loginType = 'instagram';
                    apiClient.signIn(userNameOrId, passwordOrName, loginType, this.createLoginSession)
                      .then(response => {
                        console.log('RESPONSE_NI_BAY :', response);
                        this.populateCredentials(response);
                      })
                      .catch(response => {
                        console.log(`signIn err response: ${JSON.stringify(response)}`);
                        showAlert(I18n.t('ERROR'), response.request._response);
                      });
                  })
                  .catch(error => {
                    console.log(`${JSON.stringify(error)}`);
                    showAlert(I18n.t('ERROR'), `${JSON.stringify(error)}`);
                  });
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}


const mapStateToProps = state => {
  const { userId, isSignedIn } = state.loginModule;

  const { uploadMode, wasUploaded, connectionStatus } = state.settingsModule;

  const { onFirstNameChange, onLastNameChange, onEmailChange, onCountryChange, onBirthdateChange, onGenderChange,
    onOccupationChange, onWeightChange, onHeightChange } = state.userProfileModule;

  return {
    userId, isSignedIn, onFirstNameChange, onLastNameChange, onEmailChange, onCountryChange, onBirthdateChange, onGenderChange,
    onOccupationChange, onWeightChange, onHeightChange, uploadMode, wasUploaded, connectionStatus
  };
};

export default connect(mapStateToProps, { createLoginSession, updateUPModule, updateSettingsModule })(Login);
