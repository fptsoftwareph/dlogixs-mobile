import React, { Component } from 'react';
import Header from './Header';
import { widgets } from '../styles/widgets';
import {
  Alert, View, Text, Image,
  TextInput, TouchableOpacity,
  TouchableHighlight, Modal,
  ScrollView, Platform, Linking, NativeEventEmitter,
  NativeModules
} from 'react-native';
import { Icon } from 'react-native-elements';
import DatePicker from 'react-native-datepicker'
import { APIClient } from '../utils/APIClient';
import { Actions } from 'react-native-router-flux';
import I18n from './../translate/i18n/i18n';
import Picker from 'react-native-picker';
import { updateUPModule } from '../redux/actions';
import MenuIconBack from './common/MenuIconBack';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box';
import { DEFAULT_TEXT_COLOR } from '../styles/colors';
import RenderIf from './RenderIf';
import { getPreference } from '../utils/CommonMethods';

var apiClient;
const URL_Constants = require('../config/urls.json');

class Signup extends Component {
  constructor() {
    super();

    apiClient = new APIClient();

    this.state = {
      birthdate: '',
      country: I18n.t('COUNTRY') + ' *',
      countryList: countries,
      dateSet: '',
      email: '',
      firstName: '',
      gender: I18n.t('GENDER') + ' *',
      height: 0,
      lastName: '',
      modalVisible: false,
      occupation: '',
      password: '',
      privacyTermsContent: '',
      privacyTermsHeader: '',
      weight: '',
      isEULAClicked: false,
      agreementTitle: '',
      eulaStatus: false,
      privacyStatus: false,
      localInfoStatus: false,
      agreeAllStatus: false,
      eulaUrl: URL_Constants.EULA_URL,
      privacyUrl: URL_Constants.PRIVACY_POLICY_URL,
      localInfoUrl: URL_Constants.LOCAL_INFO_URL
    };
  }

  componentDidMount() {
    console.log('Signup componentDidMount');

    /*getPreference('language').then(response => {
      console.log(`SelectLang lang: ${response}`);

      if (response === 'korean') {
        this.setState({
          eulaUrl: URL_Constants.EULA_URL_KOR,
          privacyUrl: URL_Constants.PRIVACY_POLICY_URL_KOR,
          localInfoUrl: URL_Constants.LOCAL_INFO_URL_KOR
        });
      } else if (response === 'chinese') {
        this.setState({
          eulaUrl: URL_Constants.EULA_URL_CHI,
          privacyUrl: URL_Constants.PRIVACY_POLICY_URL_CHI,
          localInfoUrl: URL_Constants.LOCAL_INFO_URL_CHI
        });
      } else if (response === 'japanese') {
        this.setState({
          eulaUrl: URL_Constants.EULA_URL_JAP,
          privacyUrl: URL_Constants.PRIVACY_POLICY_URL_JAP,
          localInfoUrl: URL_Constants.LOCAL_INFO_URL_JAP
        });
      } else {
        this.setState({
          eulaUrl: URL_Constants.EULA_URL,
          privacyUrl: URL_Constants.PRIVACY_POLICY_URL,
          localInfoUrl: URL_Constants.LOCAL_INFO_URL
        });
      }
    }).catch(error => console.log(error));*/
  }

  getDateFromCalendar(date) {
    dateSet = MONTH_NAMES[date.month - 1] + " " + date.day + ", " + date.year;

    this.setState({
      dateSet: dateSet,
      zoom: {
        xValue: parseFloat(date.day + ".1"),
        yValue: 0,
        scaleX: 1,
        scaleY: 0,
      },
    });
  }

  setCountryPicker() {
    let country_data = [];

    for (let i = 0; i < countries.length; i++) {
      country_data[i] = countries[i]['name'];
    }
    Picker.init({
      pickerTitleText: I18n.t('PLEASE_SELECT'),
      pickerConfirmBtnText: I18n.t('CONFIRM'),
      pickerCancelBtnText: I18n.t('CANCEL'),
      pickerData: country_data,
      selectedValue: [this.state.country],
      onPickerConfirm: countryData => {
        this.setState({ country: countryData.toString() });
        Picker.hide();
      },
      onPickerCancel: () => {
        Picker.hide();
      },
      onPickerSelect: () => {
      }
    });

    Picker.show();
  }

  setGenderPicker() {
    let gender_data = [I18n.t('MALE'), I18n.t('FEMALE')];
    Picker.init({
      pickerTitleText: I18n.t('PLEASE_SELECT'),
      pickerConfirmBtnText: I18n.t('CONFIRM'),
      pickerCancelBtnText: I18n.t('CANCEL'),
      pickerData: gender_data,
      selectedValue: [this.state.gender],
      onPickerConfirm: genderData => {
        this.setState({ gender: genderData.toString() });
        Picker.hide();
      },
      onPickerCancel: () => {
        Picker.hide();
      },
      onPickerSelect: () => {
      }
    });
    Picker.show();
  }

  onClickCreate() {
    if (this.state.firstName !== '' && this.state.lastName !== '' && this.state.email !== '' && !this.state.password !== ''
      && this.state.country.toLowerCase() !== defaultCountryText.toLowerCase() && this.state.birthdate !== ''
      && this.state.gender.toLowerCase() !== defaultGenderText.toLowerCase() && this.state.weight !== '') {

      if (!this.isEmailValid(this.state.email)) {
        Alert.alert('', I18n.t('INVALID_EMAIL_FORMAT'));
        return;
      }

      else if (this.state.password.length < 3) {
        Alert.alert('', I18n.t('PASSWORD_CHECK'));
        return;
      }

      else if (!this.state.agreeAllStatus) {
        Alert.alert('', I18n.t('CHECK_AGREEMENTS_FIRST'));
        return;
      }


      const userInfo = {
        first_name: this.state.firstName,
        last_name: this.state.lastName,
        email: this.state.email,
        password: this.state.password,
        country: this.state.country,
        birthdate: this.state.birthdate,
        gender: this.state.gender,
        occupation: this.state.occupation,
        weight: this.state.weight,
        height: this.state.height
      };


      this.props.updateUPModule({ prop: 'onFirstNameChange', value: this.state.firstName });
      this.props.updateUPModule({ prop: 'onEmailChange', value: this.state.email });
      this.props.updateUPModule({ prop: 'onCountryChange', value: this.state.country });
      this.props.updateUPModule({ prop: 'onBirthdateChange', value: this.state.birthdate });
      this.props.updateUPModule({ prop: 'onGenderChange', value: this.state.gender });
      this.props.updateUPModule({ prop: 'onOccupationChange', value: this.state.occupation });
      this.props.updateUPModule({ prop: 'onWeightChange', value: this.state.weight });
      this.props.updateUPModule({ prop: 'onHeightChange', value: this.state.height });

      apiClient.signup(userInfo);
    } else {
      Alert.alert('', I18n.t('FILL_UP_REQUIRED'));
    }
  }

  onClickAgreement() {
    if (this.state.agreementTitle === 'eula') {
      if (this.state.eulaStatus === false) {
        this.setState({
          eulaStatus: true
        });
      } else {
        this.setState({
          eulaStatus: false
        });
      }
    } else if (this.state.agreementTitle === 'privacy') {
        if (this.state.privacyStatus === false) {
          this.setState({
            privacyStatus: true
          });
        } else {
          this.setState({
            privacyStatus: false
          });
        }
    } else {
        if (this.state.localInfoStatus === false) {
          this.setState({
            localInfoStatus: true
          });
        } else {
          this.setState({
            localInfoStatus: false
          });
        }
    }

    setTimeout(() => {
      this.checkifAllAgreementsAreChecked();
    }, 1000);
  }

  checkifAllAgreementsAreChecked() {
    console.log('checkifAllAgreementsAreChecked eulaStatus: ' + this.state.eulaStatus);
    console.log('checkifAllAgreementsAreChecked privacyStatus: ' + this.state.privacyStatus);
    console.log('checkifAllAgreementsAreChecked localInfoStatus: ' + this.state.localInfoStatus);
    if (this.state.eulaStatus === true && this.state.privacyStatus === true &&
    this.state.localInfoStatus === true) {
      this.setState({
        agreeAllStatus: true
      });
    } else {
      this.setState({
        agreeAllStatus: false
      });
    }

    console.log('checkifAllAgreementsAreChecked agreeAllStatus: ' + this.state.agreeAllStatus);
  }

  onClickAllAgreement() {
    if (this.state.agreeAllStatus === false) {
      this.setState({
        agreeAllStatus: true,
        localInfoStatus: true,
        privacyStatus: true,
        eulaStatus: true
      });
    } else {
      this.setState({
        agreeAllStatus: false,
        localInfoStatus: false,
        privacyStatus: false,
        eulaStatus: false
      });

      setTimeout(() => {
        console.log('onClickAllAgreement after: ' + this.state.agreeAllStatus);
      }, 1000);
    }
  }

  isEmailValid(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  render() {
    return (
      <View style={[widgets.commonCon, widgets.pagePaddingBottom]}>
        <View>
          <Header />
          <MenuIconBack />
          <Text style={widgets.signupHeader}>{I18n.t('SIGN_UP')}</Text>
        </View>
        <KeyboardAwareScrollView style={widgets.scrollView}>
          <View>
            <View style={widgets.idPassCon}>
              <Icon
                color='#979797'
                size={25}
                type="font-awesome" name="user-o" />
              <TextInput
                maxLength={15}
                placeholder={I18n.t('FIRST_NAME') + '*'}
                style={[widgets.loginTextInput, widgets.commonFont]}
                onChangeText={(firstName) => this.setState({ firstName: firstName })} />
              <TextInput
                maxLength={15}
                placeholder={I18n.t('LAST_NAME') + '*'}
                style={[widgets.loginTextInput, widgets.commonFont]}
                onChangeText={(lastName) => this.setState({ lastName: lastName })} />
            </View>
            <View style={widgets.idPassCon}>
              <Icon
                color='#979797'
                size={25}
                type="font-awesome" name="envelope-o" />
              <TextInput
                maxLength={30}
                placeholder={I18n.t('EMAIL') + '*'}
                keyboardType="email-address"
                style={[widgets.loginTextInput, widgets.commonFont]}
                onChangeText={(email) => this.setState({ email: email })} />
            </View>
            <View style={widgets.idPassCon}>
              <Icon
                color='#979797'
                size={25}
                type="material-community" name="lock-outline" />
              <TextInput
                maxLength={30}
                secureTextEntry={true}
                placeholder={I18n.t('PASSWORD') + ' *'}
                style={[widgets.loginTextInput, widgets.commonFont]}
                onChangeText={(password) => this.setState({ password: password })} />
            </View>

            <View style={[widgets.idPassCon, { marginTop: 20 }]}>
              <Icon
                color='#979797'
                size={25}
                type="font-awesome" name="map-marker" />

              <TouchableOpacity style={{ flex: 1, marginRight: 3 }} onPress={() => {
                this.setCountryPicker();
              }}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: 'gray',
                    marginLeft: 23,
                    ...Platform.select({
                      ios: {
                        borderBottomWidth: 1,
                        borderBottomColor: 'gray',
                        marginLeft: 23,
                      }
                    })
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      backgroundColor: 'transparent',
                      textAlign: 'left',
                      color: 'gray',
                      fontSize: 15,
                    }}>{this.state.country}</Text>
                </View>
              </TouchableOpacity>

            </View>

            <View style={widgets.idPassCon}>
              <Icon
                color='#979797'
                size={25}
                type="octicon" name="gift" />
              <DatePicker
                style={[widgets.birthdateTextInput, widgets.borderNone, this.state.textcolor]}
                date={this.state.birthdate}
                showIcon={false}
                mode="date"
                maxDate={new Date()}
                androidMode="spinner"
                placeholder={I18n.t('BIRTHDAY') + ' *'}
                customStyles={{
                  placeholderText: {
                    color: '#595959',
                    textAlign: 'left',
                    marginBottom: -20
                  },
                  dateInput: {
                    borderColor: 'transparent',
                    borderBottomColor: 'black',
                    justifyContent: 'flex-start',
                    flexDirection: 'row',
                    marginRight: 2,
                    ...Platform.select({
                      ios: {
                        marginLeft: -5,
                        borderBottomWidth: 1,
                        borderBottomColor: 'gray'
                      }
                    })
                  },
                  dateText: {
                    marginBottom: -20,
                    color: this.state.dropdowncolor,
                    justifyContent: 'flex-start',
                    ...Platform.select({
                      ios: {
                        color: 'gray'
                      }
                    })
                  }
                }}
                format="DD/MM/YYYY"
                cancelBtnText="Cancel"
                confirmBtnText="Save"
                onDateChange={value => {
                  this.setState({ birthdate: value });
                }} />
            </View>

            <View style={[widgets.idPassCon, { marginTop: 20 }]}>
              <Icon
                color='#979797'
                size={25}
                type="font-awesome" name="intersex" />

              <TouchableOpacity style={{ flex: 1, marginRight: 3 }} onPress={() => {
                this.setGenderPicker();
              }}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: 'gray',
                    marginLeft: 16,
                    ...Platform.select({
                      ios: {
                        borderBottomWidth: 1,
                        borderBottomColor: 'gray',
                        marginLeft: 16
                      }
                    })
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      backgroundColor: 'transparent',
                      textAlign: 'left',
                      fontSize: 15,
                      color: 'gray'
                    }}>{this.state.gender}</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={widgets.idPassCon}>
              <Icon
                color='#979797'
                size={25}
                type="font-awesome" name="user-secret" />
              <TextInput
                maxLength={30}
                placeholder={I18n.t('OCCUPATION')}
                style={[widgets.loginTextInput, widgets.commonFont]}
                onChangeText={(occupation) => this.setState({ occupation: occupation })} />
            </View>
            <View style={widgets.idPassCon}>
              <Icon
                color='#979797'
                size={25}
                type="material-community" name="scale-bathroom" />
              <TextInput
                maxLength={3}
                placeholder={I18n.t('WEIGHT') + ' ' + I18n.t('KG') + ' *'}
                keyboardType="numeric"
                style={[widgets.loginTextInput, widgets.commonFont]}
                onChangeText={(weight) => this.setState({ weight: weight })} />
              <Icon
                color='#979797'
                size={25}
                type="material-community" name="ruler" />
              <TextInput
                maxLength={3}
                placeholder={I18n.t('HEIGHT') + ' ' + I18n.t('CM')}
                keyboardType="numeric"
                style={[widgets.loginTextInput, widgets.commonFont]}
                onChangeText={(height) => this.setState({ height: height })} />
            </View>
            <Text style={[widgets.centerText, widgets.commonMarginTop2, widgets.policyMargin]}>{I18n.t('BY_SIGNING_UP')}</Text>
            <View style={widgets.privacyPolicyTermsCon}>
              <Text style={[widgets.centerText, widgets.underLinedText]} onPress={() => {
                this.setState({ isEULAClicked: true });
                this.setModalVisibility(true, I18n.t('EULA'), I18n.t('EULA_CONTENT'), 'eula');
              }}>{I18n.t('EULA_LABEL')}</Text>
              <Text> , </Text>
              <Text style={[widgets.centerText, widgets.underLinedText]} onPress={() => {
                this.setState({ isEULAClicked: false });
                this.setModalVisibility(true, I18n.t('PRIVACY_POLICY'), I18n.t('POLICY_CONTENT'), 'privacy');
              }}>{I18n.t('PRIVACY_POLICY_LABEL')}</Text>
              <Text> & </Text>
            </View>
            <Text style={[widgets.centerText, widgets.underLinedText]} onPress={() => {
              this.setModalVisibility(true, I18n.t('LOCAL_INFO'), I18n.t('LOCAL_INFO_CONTENT'), 'local_info');
            }}>{I18n.t('LOCAL_INFO_LABEL')}</Text>
            <View style={widgets.privacyPolicyTermsCon}>
              {RenderIf(this.state.agreeAllStatus)(
              <TouchableOpacity onPress={() => {
                this.onClickAllAgreement();
              }}>
                <Image style={widgets.signUpCheckBox} source={require('../images/check_on.png')} />
              </TouchableOpacity>
              )}
              {RenderIf(!this.state.agreeAllStatus)(
              <TouchableOpacity onPress={() => {
                this.onClickAllAgreement();
              }}>
                <Image style={widgets.signUpCheckBox} source={require('../images/check_off.png')} />
              </TouchableOpacity>
              )}
              <View style={widgets.centerView}>
                <Text>{I18n.t('AGREE_ALL')}</Text>
              </View>
             </View>

            <Modal
              animationType="slide"
              transparent={true}
              style={widgets.modal}
              visible={this.state.modalVisible}
              presentation
              onRequestClose={() => { this.setState({ modalVisible: false }) }}>

              <View style={widgets.privacyPolicyCon}>
                <Image source={require('../images/header_bg.png')} borderRadius={15} style={widgets.modalImageBg}>
                  <View style={widgets.modalCloseCon}>
                    <Text style={widgets.modalClose} onPress={() => this.setModalVisibility(false)}>X</Text>
                  </View>
                  <ScrollView style={widgets.modalScrollView}>
                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                      <View style={widgets.policyHeaderCon}>
                        <Text style={widgets.privacyPolicyHeader}>{this.state.privacyTermsHeader}</Text>
                      </View>
                      <View style={widgets.policyTextCon}>
                        <Text style={widgets.privacyPolicyText}>{this.state.privacyTermsContent}</Text>
                      </View>
                      <Text style={[widgets.privacyPolicyText, widgets.underLinedText, { marginBottom: 15 }]} onPress={() => {
                        Linking.openURL(this.state.agreementTitle === 'eula' ? this.state.eulaUrl :
                        this.state.agreementTitle === 'privacy' ? this.state.privacyUrl : this.state.localInfoUrl);
                      }}>{I18n.t('READ_MORE')}</Text>
                      <View style={widgets.privacyPolicyTermsCon}>
                        <CheckBox checkBoxColor={'white'}
                          onClick={() => this.onClickAgreement()} isChecked={this.state.agreementTitle === 'eula' ? this.state.eulaStatus :
                        this.state.agreementTitle === 'privacy' ? this.state.privacyStatus : this.state.localInfoStatus}/>
                        <View style={widgets.centerView}>
                        <Text style={{ color: 'white', marginLeft: 3 }}>{I18n.t('I_AGREE')}</Text>
                        </View>
                      </View>
                    </View>
                  </ScrollView>
                </Image>
              </View>
            </Modal>

            <TouchableHighlight onPress={() => this.onClickCreate()}>
              <Text style={[widgets.loginBtn, widgets.commonMarginTop2]}>{I18n.t('CREATE')}</Text>
            </TouchableHighlight>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }

  setModalVisibility(isVisible, modalHeader, modalContent, agreementTitle) {
    this.setState({
      modalVisible: isVisible,
      privacyTermsHeader: modalHeader,
      privacyTermsContent: modalContent,
      agreementTitle: agreementTitle
    });
  }
}

const countries = require('../../countries.json')
const PickerItem = Picker.Item;
const defaultGenderText = "GENDER *"
const defaultCountryText = "COUNTRY *"
const LEFT = 'left'
const MONTH_NAMES = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY',
  'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];

const mapStateToProps = (state) => {
  const { onFirstNameChange, onLastNameChange, onEmailChange, onCountryChange, onBirthdateChange, onGenderChange,
    onOccupationChange, onWeightChange, onHeightChange } = state.userProfileModule;

  return {
    onFirstNameChange, onLastNameChange, onEmailChange, onCountryChange, onBirthdateChange, onGenderChange,
    onOccupationChange, onWeightChange, onHeightChange
  };
};

export default connect(mapStateToProps, { updateUPModule })(Signup); 