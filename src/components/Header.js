import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet } from 'react-native';
import { widgets } from '../styles/widgets';
import { containers } from '../styles/containers';

const Header = () => {

  return (
    <ImageBackground style={widgets.header}
      source={require('../images/header_bg.png')}>
      <View style={containers.logoCon}>
        <Image style={widgets.logo}
          source={require('../images/logo.png')} />
      </View>
    </ImageBackground>
  );
}

export default Header;