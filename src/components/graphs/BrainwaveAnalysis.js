import React, {Component} from 'react';
import HeaderMain from '../common/HeaderMain';
import * as DateUtil from '../../utils/DateUtils';
import RNFetchBlob from 'rn-fetch-blob';
import {
  Alert,
  Text,
  View,
  ScrollView,
  Image,
  NativeEventEmitter,
  NativeModules,
  PermissionsAndroid,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ImageBackground,
  processColor,
  Platform
} from 'react-native';

import Modal from 'react-native-modal';
import I18n from '../../translate/i18n/i18n';
import {connect} from 'react-redux';
import {widgets} from '../../styles/widgets';
import {containers} from '../../styles/containers';
import {LineChart, CombinedChart, PieChart} from 'react-native-charts-wrapper';
import {CheckBox} from 'react-native-elements';
const {getMonthNames, getWeekNames} = DateUtil;
import {StationModule} from 'nb_native_modules';
import * as Progress from 'react-native-progress';

import {getPreference, showToast} from '../../utils/CommonMethods';
import getCurrentDate from '../../utils/DateUtils';
import { COLOR_THUMB_TINT } from '../../styles/colors';

const Json2csvParser = require('json2csv').Parser;
const fields = [
  'user_id',
  'name',
  'time',
  'plotPointY',
  'highest',
  'gamma',
  'beta',
  'alpha',
  'theta',
  'delta',
  'rbs_marker_on',
  'rbs_marker_off'
];

var csvData = [{}];
const json2csvParser = new Json2csvParser({fields});
const CONFIG = require('../../config/brainwave.json');
const StationManagerModule = NativeModules.StationModule;

const StationManagerEmitter = new NativeEventEmitter(StationManagerModule);
var brainwaveArr = [];

let attention = 0,
  axisMaximum = 50,
  eegCtr = 0,
  eegCtr2 = 0,
  intervalSet = '',
  meditation = 0,
  xAxisLabelCount = 0,
  isEEGListenerEnabled = false,
  formatterCtr = 0;
let valFormatterSizeIOS;
const xAxisMaxValHistory = 1800;
const xAxisMaxVal = 60;
let userId = '';

let filePath = '';
const currentDate = getCurrentDate();
const pageLimit = 1800;

const mapStateToProps = state => {
  const {isBstationHeadSetEnabled} = state.settingsModule;

  const {onFirstNameChange} = state.userProfileModule;

  return {isBstationHeadSetEnabled, onFirstNameChange};
};

class BrainwaveAnalysis extends Component {

  constructor(props) {
    super(props);

    this.bubbleMarkerOn = [
      {
        x: -5,
        y: -5
      }
    ];
    this.bubbleMarkerOff = [
      {
        x: -5,
        y: -5
      }
    ];

    this.bubbleMarkerOnForHistory = [
      {
        x: -5,
        y: -5
      }
    ];
    this.bubbleMarkerOffForHistory = [
      {
        x: -5,
        y: -5
      }
    ];

    this.bubbleMarkerOnForHistoryDef = [
      {
        x: -5,
        y: -5
      }
    ];
    this.bubbleMarkerOffForHistoryDef = [
      {
        x: -5,
        y: -5
      }
    ];

    this.lineMarkerOn = [];
    this.lineMarkerOff = [];
    this.plotPointY = 0;

    this.state = {
      bubbleMarkerOn: [
        {
          x: -5,
          y: 0,
          size: 0
        }
      ],
      progressModal: false,
      lineColor: processColor('white'),
      labelCount: 3,
      axisMinimum: 0,
      axisMaximum: xAxisMaxVal,
      axisMinimumHistory: 0,
      axisMaximumHistory: xAxisMaxValHistory,
      values: [0],
      historyValues: [0],
      brainwaveFreqDict: {
        delta: 0,
        theta: 0,
        alpha: 0,
        beta: 0,
        gamma: 0
      },
      intervalSet: 30,
      attentionRate: 0,
      meditationRate: 0,
      attentionCounter: 0,
      brainwaveCounter: 0,
      interval5: true,
      interval10: false,
      interval20: false,
      brainwaveInterval: 0,
      modalIntervalVisible: false,
      modalHistoryVisible: false,
      dateSet: this.getCurrentDate(),
      leftArrow: '<',
      rightArrow: '>',
      legend: {
        enabled: true,
        textSize: 0,
        textColor: processColor('transparent'),
        form: 'SQUARE',
        formSize: 0,
        xEntrySpace: 0,
        yEntrySpace: 0,
        formToTextSpace: 0,
        wordWrapEnabled: true,
        maxSizePercent: 0
      },
      marker: {
        enabled: false,
        backgroundTint: processColor('teal'),
        markerColor: processColor('#F0C0FF8C'),
        textColor: processColor('white')
      },
      pieChartData: this.getPieChartData(0, 0),
      zoom: {
        xValue: 1,
        yValue: 0,
        scaleX: CONFIG.OPT1_ZOOM,
        scaleY: 0
      },
      zoomHistory: {
        xValue: 1,
        yValue: 0,
        scaleX: CONFIG.OPT5_ZOOM,
        scaleY: 0
      }
    };

    this.thirtySec = 30 + ' ' + I18n.t('SEC');
    this.oneMin = 1 + ' ' + I18n.t('MIN');
    this.fiveMin = 5 + ' ' + I18n.t('MIN');

    this.fiveSec = 5 + ' ' + I18n.t('SEC');
    this.tenSec = 10 + ' ' + I18n.t('SEC');
    this.twentySec = 20 + ' ' + I18n.t('SEC');

    this.rightCoordinates = [];
    this.handleEEGSpectrumData = this
      .handleEEGSpectrumData
      .bind(this);
    this.handleAttentionData = this
      .handleAttentionData
      .bind(this);
    this.handleMeditationData = this
      .handleMeditationData
      .bind(this);

    getPreference('userId').then(res => {
      console.log('123redux: getPreference userId: ' + res);
      userId = res;
      filePath = `${Platform.OS === 'android' ? RNFetchBlob.fs.dirs.DownloadDir : RNFetchBlob.fs.dirs.DocumentDir}/${currentDate}-neurobeat-brainwave-${userId === undefined ? '' : userId}.csv`;

      RNFetchBlob.fs.unlink(filePath)
      .then(() => {
        console.log('1231event file deleted.');
      })
      .catch(err => {
        console.log('1231event error caught: ' + err);
      });
    });

    valFormatterSizeIOS = 12;
    this.axisCtr = 1;
    this.page = 0;
    this.isLoading = false;
    this.maxPage = 0;
    this.twentyFourHourLimit = 86400;
    this.axisMaxHistory = pageLimit;
  }

  getPieChartData(attention, meditation) {
    return {
      dataSets: [
        {
          values: [
            {
              value: meditation,
              label: ''
            }, {
              value: attention,
              label: ''
            }
          ],
          label: I18n.t('PIE_DATASET'),
          config: {
            colors: [
              processColor('rgb(128, 111, 149)'), processColor('rgb(73, 167, 194)')
            ],
            drawValues: false,
            sliceSpace: 1,
            selectionShift: 13
          }
        }
      ]
    };
  }

  getChartData(values) {
    // console.log('1231bubble: this.bubbleMarkerOn: ' + JSON.stringify(this.bubbleMarkerOn) + '\tthis.bubbleMarkerOff: ' + JSON.stringify(this.bubbleMarkerOff));
    return {
      data: {
        lineData: {
          dataSets: [{
            values: values,
            label: I18n.t('BRAINWAVE_VISUALIZER'),
            config: {
              lineWidth: 2,
              drawValues: false,
              drawCircles: false,
              highlightColor: processColor('white'),
              color: processColor('white'),
              drawFilled: true,
              fillAlpha: 0,
              backgroundColor: 'red',
              valueTextSize: 14
            }
          }]
        },
        scatterData: {
          dataSets: [{
            values: this.bubbleMarkerOn,
            label: 'Marker',
            config: {
              axisDependency: 'left',
              normalizeSizeEnabled: false,
              drawValues: false,
              // please change size when applying markers for ticket 505.
              scatterShapeHoleRadius: 4,
              scatterShapeHoleColor: processColor('red'),
              scatterShape: 'CIRCLE'
            }
          }, {
            values: this.bubbleMarkerOff,
            label: 'Marker',
            config: {
              axisDependency: 'left',
              normalizeSizeEnabled: true,
              drawValues: false,
              scatterShapeHoleRadius: 4,
              scatterShapeHoleColor: processColor('gray'),
              scatterShape: 'CIRCLE'
            }
          }]
        }
      },
      xAxis: {
        position: 'BOTTOM',
        spaceBottom: 10,
        labelCount: this.state.labelCount,
        axisMinimum: this.state.axisMinimum,
        axisMaximum: this.state.axisMaximum,
        labelCountForce: true,
        axisLineWidth: 1,
        granularityEnabled: true,
        granularity: 1,
        textSize: Platform.OS === 'android'
          ? 12
          : valFormatterSizeIOS,
        avoidFirstLastClipping: false,
        textColor: processColor('white')
      }
    };
  }

  getHistoryChartData(values) {
    // console.log('1231bubble: this.bubbleMarkerOnForHistoryDef: ' + JSON.stringify(this.bubbleMarkerOnForHistoryDef) + '\tthis.bubbleMarkerOffForHistoryDef: ' + JSON.stringify(this.bubbleMarkerOffForHistoryDef));
    
    return {
      dataForHistory: {
        lineData: {
          dataSets: [{
            values: values,
            label: I18n.t('BRAINWAVE_VISUALIZER'),
            config: {
              lineWidth: 2,
              drawValues: false,
              drawCircles: false,
              highlightColor: processColor('white'),
              color: processColor('white'),
              drawFilled: true,
              fillAlpha: 0,
              backgroundColor: 'red',
              valueTextSize: 14
            }
          }]
        },
        scatterData: {
          dataSets: [{
            values: this.bubbleMarkerOnForHistoryDef,
            label: 'Marker',
            config: {
              axisDependency: 'left',
              normalizeSizeEnabled: false,
              drawValues: false,
              // please change size when applying markers for ticket 505.
              color: processColor('red'),
              scatterShape: 'CIRCLE',
              scatterShapeSize: Platform.OS === 'ios' ? 6 : 10
            }
          }, {
            values: this.bubbleMarkerOffForHistoryDef,
            label: 'Marker',
            config: {
              axisDependency: 'left',
              normalizeSizeEnabled: true,
              drawValues: false,
              color: processColor('gray'),
              scatterShape: 'CIRCLE',
              scatterShapeSize: Platform.OS === 'ios' ? 6 : 10
            }
          }]
        }
      },
      xAxisHistory: {
        position: 'BOTTOM',
        spaceBottom: 10,
        labelCount: this.state.labelCount,
        axisMinimum: this.state.axisMinimumHistory,
        axisMaximum: this.state.axisMaximumHistory,
        labelCountForce: true,
        axisLineWidth: 1,
        granularityEnabled: true,
        granularity: 1,
        textSize: Platform.OS === 'android'
          ? 12
          : valFormatterSizeIOS,
        avoidFirstLastClipping: false,
        textColor: processColor('white')
      }
    };
  }

  handleEEGSpectrumData(eeg) {
    if (!isEEGListenerEnabled) {
      isEEGListenerEnabled = true;
    }

    let eegArr = [];
    let highest = 0;

    const frequencies = [];
    for (freq in eeg) {
      frequencies.push(freq);
      eegArr.push(eeg[freq]);
    }

    let frequency = '';

    for (let i = 0; i < eegArr.length; i++) {
      if (eegArr[i] > highest) {
        highest = eegArr[i];
        frequency = frequencies[i];
      }
    }

    switch (frequency) {
      case 'delta':
        this.plotPointY = 1.25;
        break;
      case 'theta':
        this.plotPointY = 2.35;
        break;
      case 'alpha':
        this.plotPointY = 3.55;
        break;
      case 'beta':
        this.plotPointY = 4.75;
        break;
      case 'gamma':
        this.plotPointY = 6;
        break;
      default:
        this.plotPointY = 0;
        break;
    }

    //  console.log('handleEEGResponseData: this.plotPointY: ' + this.plotPointY + '\tfrequency: ' + frequency);

    if (highest > axisMaximum) {
      axisMaximum = highest + 10;
    }

    if (eegCtr % 61 === 0) {

      const csv = json2csvParser.parse(csvData);

      RNFetchBlob
        .fs
        .appendFile(filePath, csv, 'utf8')
        .then(() => {
          console.log(`123csv: wrote file ${filePath}`);
        })
        .catch(error => console.error('123csv: ' + error));
      console.log('123csv: csv: ' + JSON.stringify(csv));
      csvData.length = 0;

      this.bubbleMarkerOn = [
        {
          x: -5,
          y: -5
        }
      ];
      this.bubbleMarkerOff = [
        {
          x: -5,
          y: -5
        }
      ];

      this.bubbleMarkerOnForHistoryDef = this.bubbleMarkerOnForHistory;
      this.bubbleMarkerOffForHistoryDef = this.bubbleMarkerOffForHistory;
    };

    if (eegCtr > this.state.axisMaximum) {
      this.axisCtr += (eegCtr);
      eegCtr = 0;
      const lastPlotPointY = this.state.values[this.state.values.length - 1].y
      brainwaveArr.length = 0;
      this.plotPointY = lastPlotPointY;
    }

    //  console.log('1231readcsv axisCtr: ' + this.axisCtr);

    //  console.log('1231readcsv: axisMaximumHistory: ' + this.axisMaxHistory);

    if (this.axisCtr > this.axisMaxHistory) {
      this.axisMaxHistory += pageLimit;
      this.page++;
      this.maxPage = parseInt(this.axisCtr / pageLimit);
    }

    valFormatterSizeIOS = this.state.axisMaximum.toString().length < 4 ? 12 : 8;

    const eegObj = { x: eegCtr, y: this.plotPointY, marker: highest.toString() };
    const timeNow = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // const headers = [user_id, name, time, highest, gamma, beta, alpha, theta,
    // delta];

    csvData.push({
      'user_id': userId,
      'name': this.props.onFirstNameChange,
      'time': timeNow,
      'highest': highest,
      'plotPointY': this.plotPointY,
      'gamma': eeg.gamma,
      'beta': eeg.beta,
      'alpha': eeg.alpha,
      'theta': eeg.theta,
      'delta': eeg.delta,
      'rbs_marker_on': this.bubbleMarkerOnForHistoryDef,
      'rbs_marker_off': this.bubbleMarkerOffForHistoryDef
    });

    eegCtr++;
    eegCtr2++;
    //console.log('EEG ctr: ' + eegCtr2);
    brainwaveArr.push(eegObj);

    let brainwaveSlice = brainwaveArr.slice();
    let config = this.getChartData(brainwaveSlice);

    this.setState({values: brainwaveSlice, data: config.data, xAxis: config.xAxis});

    setTimeout(() => {
      const xVal = eegCtr > 9 ? eegCtr - 5 : 0;
      if (this.refs.brainwaveChart !== undefined) {
        this.refs.brainwaveChart.moveViewToX(xVal);
      }
    }, 1000);
  }

  handleMeditationData(med) {
    //  console.log('handleEEGResponseData() meditation: ' + meditation);
    meditation = med;

    this.setState({
      pieChartData: this.getPieChartData(attention, meditation),
      attentionCounter: this.state.attentionCounter += 1,
      attentionRate: attention,
      meditationRate: meditation
    });
  }

  handleAttentionData(att) {
    //  console.log('handleEEGResponseData() attention: ' + attention);
    attention = att;

    this.setState({
      pieChartData: this.getPieChartData(attention, meditation),
      attentionCounter: this.state.attentionCounter += 1,
      attentionRate: attention,
      meditationRate: meditation
    });
  }

  modifyLineChartZoom(xValue, xZoom) {
    this.setState({
      zoom: {
        xValue: xValue,
        yValue: this.state.zoom.yValue,
        scaleX: xZoom,
        scaleY: 1
      }
    });
  }

  componentDidMount() {
    const defArrVal = [];
    defArrVal.push(0);
    // for (let i = 0; i < 60; i++) {
    //   defArrVal.push(0);
    // }

    let config = this.getChartData(defArrVal);
    this.setState({
      xAxis: config.xAxis,
      data: config.data,
      values: defArrVal,
      yAxis: {
        left: {
          enabled: true,
          valueFormatter: [''],
          drawGridLines: true,
          gridLineWidth: 1,
          spaceBottom: -10,
          axisMinimum: 0,
          axisMaximum: 6,
          labelCount: 6,
          labelCountForce: true,
          textSize: 14,
          textColor: processColor('white')
        },
        right: {
          enabled: false
        }
      }
    });

    console.log('handleEEGResponseData: isBstationHeadSetEnabled: ' + this.props.isBstationHeadSetEnabled);

    if (!this.props.isBstationHeadSetEnabled) {
      Alert.alert('', I18n.t('CONNECT_TO_HEADSET'));
    }

    this.handlerEEGSpectrum = StationManagerEmitter.addListener('EEGSpectrumListener', this.handleEEGSpectrumData);
    this.handleEEGMeditation = StationManagerEmitter.addListener('EEGMeditationListener', this.handleMeditationData);
    this.handleEEGAttention = StationManagerEmitter.addListener('EEGAttentionListener', this.handleAttentionData);

    if (Platform.OS === 'ios') {
      this.setState({
        zoom: {
          xValue: 0,
          yValue: 0,
          scaleX: 1,
          scaleY: 0
        }
      });
      setTimeout(() => {
        this.onCheckBoxPressedIOS(5);
      }, 5);
    }

    if (Platform.OS === 'android') {
      if (Platform.Version >= 23) {
        PermissionsAndroid
          .check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
          .then((result) => {
            if (result) {
              console.log('Permission is OK');
            } else {
              PermissionsAndroid
                .requestMultiple([PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE])
                .then((result2) => {
                  if (result2) {
                    console.log('User accept');
                  } else {
                    console.log('User denied');
                  }
                });
            }
          });
      } else {
        console.log('No permission required in this device.');
      }
    }
  }

  componentWillUnmount() {
    if (this.handlerEEGSpectrum !== undefined) {
      this
        .handlerEEGSpectrum
        .remove();
    }

    if (this.handleEEGMeditation !== undefined) {
      this
        .handleEEGMeditation
        .remove();
    }

    if (this.handleEEGAttention !== undefined) {
      this.handleEEGAttention.remove();
    }

    RNFetchBlob.fs.unlink(filePath)
    .then(() => {
      console.log('1231event file deleted.');
    })
    .catch(err => {
      console.log('1231event error caught: ' + err);
    });

    this.setState({values: [0], historyValues: [0]});
    brainwaveArr = [];
    isEEGListenerEnabled = false;
    eegCtr = 0;
    eegCtr2 = 0;
    formatterCtr = 0;
    csvData = [];
    console.log('eegdata: cleared all values...');
  }

  onCheckboxPressed(value) {
    if (Platform.OS === 'ios') {
      console.log('platform123 -- ios');
      this.onCheckBoxPressedIOS(value);
      // this.onCheckBoxPressedAndroid(value);
    } else {
      console.log('platform123 -- android');
      this.onCheckBoxPressedAndroid(value);
    }
  }

  onCheckBoxPressedAndroid(value) {
    console.log('123selection: 11value: ' + value);
    let i10 = false;
    let i20 = false;
    let i30 = false;

    let xAxisZoom = 0;

    // 2 zoom, 3 label count = 10 interval 2 zoom, 4 label count = 15 interval 3
    // zoom, 3 label count = 20 interval

    switch (value) {
      case 5:
        i10 = true;
        i20 = false;
        i30 = false;

        xAxisLabelCount = CONFIG.LABEL_COUNT3;
        xAxisZoom = CONFIG.OPT1_ZOOM;
        this.setState({intervalSet: 30});
        break;
      case 10:
        i10 = false;
        i20 = true;
        i30 = false;

        xAxisLabelCount = CONFIG.LABEL_COUNT3;
        xAxisZoom = CONFIG.OPT2_ZOOM;
        this.setState({intervalSet: 1});
        break;
      case 20:
        i10 = false;
        i20 = false;
        i30 = true;

        xAxisLabelCount = CONFIG.LABEL_COUNT2;
        xAxisZoom = CONFIG.OPT3_ZOOM;
        this.setState({intervalSet: 5});
        break;
      default:
        break;
    }

    console.log('xAxisZoom: ' + xAxisZoom + '\txAxisLabelCount: ' + xAxisLabelCount);

    this.setState({
      labelCount: xAxisLabelCount,
      interval5: i10,
      interval10: i20,
      interval20: i30,
      modalIntervalVisible: false,
      zoom: {
        xValue: eegCtr,
        yValue: 0,
        scaleX: xAxisZoom,
        scaleY: 0
      }
    });
  }

  setLineChartState(arr, labelCount, xAxisZoom, xValue) {
    this.setState({
      xAxis: {
        valueFormatter: arr,
        position: 'BOTTOM',
        labelCount: labelCount,
        axisMinimum: 0,
        axisMaximum: axisMaximum,
        labelCountForce: true,
        granularityEnabled: true,
        granularity: 1,
        textSize: Platform.OS === 'android'
          ? 12
          : valFormatterSizeIOS,
        avoidFirstLastClipping: true,
        textColor: processColor('white')
      },
      zoom: {
        xValue: xValue,
        yValue: 0,
        scaleX: xAxisZoom,
        scaleY: 0
      }

    });
  }

  onCheckBoxPressedIOS(value) {
    console.log('123selection: value: ' + value);
    let i10 = false;
    let i20 = false;
    let i30 = false;
    let xAxisZoom = 0;
    let arr = [];
    let j = 0;
    let labelCount = 0;

    // 2 zoom, 3 label count = 10 interval 2 zoom, 4 label count = 15 interval 3
    // zoom, 3 label count = 20 interval

    switch (value) {
      case 5:
        this.setState({intervalSet: 30});
        i10 = true;
        i20 = false;
        i30 = false;

        xAxisLabelCount = CONFIG.LABEL_COUNT3;
        xAxisZoom = CONFIG.OPT1_ZOOM;

        arr = this.setChartIntervals();
        labelCount = 11;
        break;
      case 10:
        this.setState({intervalSet: 1});
        i10 = false;
        i20 = true;
        i30 = false;

        xAxisLabelCount = CONFIG.LABEL_COUNT3;
        xAxisZoom = CONFIG.OPT2_ZOOM;
        labelCount = 5;

        arr = this.setChartIntervals();

        if (this.state.zoom.scaleX === 2) {
          xAxisZoom = 1;
        }

        break;
      case 20:
        this.setState({intervalSet: 5});
        i10 = false;
        i20 = false;
        i30 = true;

        xAxisLabelCount = CONFIG.LABEL_COUNT2;
        xAxisZoom = CONFIG.OPT3_ZOOM;
        labelCount = 3;

        arr = this.setChartIntervals();

        if (this.state.zoom.scaleX === 2) {
          xAxisZoom = 1;
        }
        break;
      default:
        break;
    }

    this.setState({interval5: i10, interval10: i20, interval20: i30, labelCount: xAxisLabelCount, modalIntervalVisible: false});

    console.log('xAxisZoom: ' + xAxisZoom + '\txAxisLabelCount: ' + xAxisLabelCount + '\tarr: ' + JSON.stringify(arr));
    this.setLineChartState(arr, xAxisLabelCount, xAxisZoom, eegCtr);
  }

  setChartIntervals() {
    const arr = [];
    let j = 0;

    switch (this.state.intervalSet) {
      case 20:
        for (let i = 0; i < 12; i++) {
          arr[i] = (j += 5).toString();
        }
        break;
      case 10:
        for (let i = 0; i < 6; i++) {
          arr[i] = (j += 10).toString();
        }
        break;
      case 20:
        for (let i = 0; i < 6; i++) {
          arr[i] = (j += 20).toString();
        }
        break;
      default:
        arr.push('0');
        break;
    }

    console.log('handleEEGResponseData: array: ' + arr);

    return arr;
  }

  getCurrentDate() {
    const strDate = new Date();
    return getWeekNames()[strDate.getDay()].toUpperCase() + ', ' + getMonthNames()[strDate.getMonth() + 1].toUpperCase() + ' ' + strDate.getDate();
  }

  handleSelect(event) {
    const entry = event.nativeEvent;
    console.log('1231event: entry: ' + JSON.stringify(entry));
    if (entry === null) {
      this.setState({
        ...this.state,
        selectedEntry: null
      });
    } else {
      this.setState({
        ...this.state,
        selectedEntry: JSON.stringify(entry)
      });
    }
  }

  onSetMarker = (binauralEnabled) => {
    console.log('1231event isEEGListenerEnabled: ' + isEEGListenerEnabled);
    console.log('bubbleMarker binauralEnabled: ' + binauralEnabled);
    let bubbleMarkerOnTemp = [], bubbleMarkerOffTemp = [];
    let bubbleMarkerOnTempForHistory = [], bubbleMarkerOffTempForHistory = [];
    bubbleMarkerOnTempForHistory = this.bubbleMarkerOnForHistory.slice(0);
    bubbleMarkerOffTempForHistory = this.bubbleMarkerOffForHistory.slice(0);

    if (!isEEGListenerEnabled) {
      showToast(I18n.t('START_EEG_TO_ENABLE_MARKER'));
      return;
    }
    if (binauralEnabled) {
      // set bubble values (rbs on)
      bubbleMarkerOnTemp = this
        .bubbleMarkerOn
        .slice();
      bubbleMarkerOnTemp.push({ x: eegCtr - 1, y: this.plotPointY, marker: 'marker' });
      // bubbleMarkers are going to be fully implemented in ticket 505
      bubbleMarkerOnTempForHistory.push({ x: eegCtr2 - 1, y: this.plotPointY, marker: 'marker' });
      this.bubbleMarkerOnForHistory = bubbleMarkerOnTempForHistory;
      this.bubbleMarkerOn = bubbleMarkerOnTemp;
      console.log('bubbleMarkerOn x: ' + eegCtr2 - 1 + ' y: ' + this.plotPointY);
    } else {
      // set bubble values (rbs off)
      bubbleMarkerOffTemp = this
        .bubbleMarkerOff
        .slice();
      bubbleMarkerOffTemp.push({ x: eegCtr - 1, y: this.plotPointY, marker: 'marker' });
      // bubbleMarkers are going to be fully implemented in ticket 505
      bubbleMarkerOffTempForHistory.push({ x: eegCtr2 - 1, y: this.plotPointY, marker: 'marker' });
      this.bubbleMarkerOffForHistory = bubbleMarkerOffTempForHistory;
      this.bubbleMarkerOff = bubbleMarkerOffTemp;
      console.log('bubbleMarkerOff y: ' + eegCtr2 - 1 + ' y: ' + this.plotPointY);
    }

    //  console.log('bubbleMarkerOn arr: ' + JSON.stringify(this.bubbleMarkerOn));
    //  console.log('bubbleMarkerOff arr: ' + JSON.stringify(this.bubbleMarkerOff));
  };

  onChangeListener(event) {
    const left = parseInt(event.left) < 0 ? 0 : parseInt(event.left);
    const right = parseInt(event.right);

    // console.log('1231event event: ' + JSON.stringify(event));
    // console.log("1231event: " + left + "\tright: " + right + '\tpage: ' + this.page);
    // console.log('1231event pageLimit: ' + pageLimit);
    // console.log('1231event rs: ' + ((this.page + 1) * pageLimit) - 3);
    // console.log('1231event re: ' + right <= (((this.page + 1) * pageLimit) + 5));
    let rightBoundaryStart = right >= ((this.page + 1) * pageLimit) - 10;
    let rightBoundaryEnd = right <= (((this.page + 1) * pageLimit) + 10);

    this.rightCoordinates.push(right);
    let direction = '';

    // This code block will determine the direction as to where the user swipes.
    // The only possible values for the direction variable is left and right.

    console.log('1231readcsv: maxPage: ' + this.maxPage + '\tcurrentPage: ' + this.page);
    if (this.rightCoordinates.length > 10) {
      const first = this.rightCoordinates[0];
      const last = this.rightCoordinates[9];

      if (first > last) {
        direction = 'left';
      } else {
        direction = 'right';
      }

      // console.log('1231event: rightBoundaryStart: ' + rightBoundaryStart + '\trightBoundaryEnd: ' + rightBoundaryEnd);
      // console.log("1231event: direction: " + direction);

      if (!this.isLoading) {
        if (rightBoundaryStart && direction === 'right' && this.page < this.maxPage) {
          this.page++;
          this.isLoading = true;
          // console.log('1231event scrolling to next 600 sec dataset');
          // console.log('1231event right scroll: this.page: ' + this.page);
  
          // console.log('1231event: right start: ' + start + '\tend: ' + end + '\tpage: ' + this.page);
          // the reasoning for the condition below is this:
          // to know if we're going to swipe left, we first need to know that the this.page is already greater than 0.
          // this is because 0 is our first this.page, you can't scroll beyond (to the left of) 0.
          // the 2nd criteria here is that the right (x-coordinate) must be less than the current this.page's dataset range.
          // for example, we're at this.page 1 and our right x-coordinate is at 70.
          // the dataset range from this.page 1 is 60 to 120. If we scroll to the left to x-coordinate at 55, the condition below will be satisfied
          // in that this.page 1 is greater than this.page 0 and our current x-coordinate (55) is less than the current this.page's (i.e. this.page 1) dataset range.
          // the current this.page's dataset range will start at: ((1 + 1) * 60) - 60 = 60. Since 55 is less than 60, we know in our part that we are attempting
          // to scroll to the left and that we've already moved to the previous this.page. BOOM! YOU'RE WELCOME! *drops mic*
          const start = this.page * pageLimit;
          const end = (this.page + 1) * pageLimit;
  
          this.readCSV(start, end);
  
          this.setState({
            axisMinimumHistory: start,
            axisMaximumHistory: end
          });
  
          console.log('1231event1: start: ' + start);

          this.setState({ progressModal: true });
  
          setTimeout(() => {
            this.isLoading = false;
            this.setState({ progressModal: false });
          }, 1500);
  
          setTimeout(() => {
            if (this.refs.brainwaveChart !== undefined) {
              // this.refs.brainwaveChart.moveViewToX(start);
              this.refs.brainwaveHistory.moveViewToAnimated(start - 8, 0, 'left', 300);
            }
          }, 200);
        } else if (this.page > 0 && left <= (((this.page + 1) * pageLimit) - (pageLimit - 10)) && direction === 'left') {
          this.page--;
          this.isLoading = true;
          // console.log('1231event scrolling to previous 600 sec dataset');
          const start = this.page * pageLimit;
          const end = (this.page + 1) * pageLimit;
  
          // console.log('1231event: left start: ' + start + '\tend: ' + end + '\tpage: ' + this.page);
  
          this.readCSV(start, end);
  
          this.setState({
            axisMinimumHistory: start,
            axisMaximumHistory: end
          });
  
          this.setState({ progressModal: true });
  
          setTimeout(() => {
            this.isLoading = false;
            this.setState({ progressModal: false });
          }, 1500);
  
          setTimeout(() => {
            if (this.refs.brainwaveChart !== undefined) {
              // this.refs.brainwaveChart.moveViewToX(end);
              this.refs.brainwaveHistory.moveViewToAnimated(end, 0, 'left', 300);
            }
          }, 200);
  
          // console.log('1231event start: ' + start + '\tend: ' + end);
        }
      }

      // we reset this.rightCoordinates
      this.rightCoordinates.length = 0;
    }
  }

  // start - start of the dataset range we're going to render onto the graph end -
  // end of the dataset range we're going to render onto the graph
  readCSV(start, end) {
    console.log('1231readcsv start: ' + start + '\tend: ' + end);
    console.log('1231readcsv filePath: ' + filePath);

    RNFetchBlob.fs.readFile(filePath, 'utf8').then(data => {
      let rows = this.csvToArray(data, start, end + 1);
      console.log('readCSV rows: ' + JSON.stringify(rows));

      let brainwave = [];
      console.log('1231readcsv rows.length: ' + rows.length);

      for (let i = 0, x = start; i < rows.length; i++, x++) {
        // console.log('1231event row: ' + JSON.stringify(rows[i]) + "\t name: " + rows[i]['name'] + "\t plotPointY: " + rows[i].plotPointY + "\tgamma: " + rows[i].gamma);
        if (rows[i].user_id !== undefined) {
          const eegObj = {
            x: x,
            y: parseFloat(rows[i].plotPointY),
            marker: rows[i].highest

          };
          brainwave.push(eegObj);
        }
      }

      console.log('1231readcsv brainwave.length: ' + brainwave.length);

      if (brainwave.length > 0) {
        let brainwaveSlice = brainwave.slice();
        let config = this.getHistoryChartData(brainwaveSlice);

        this.setState({ historyValues: brainwaveSlice, xAxis: config.xAxis });
        this.refs.chart.setDataAndLockIndex(config.data);
      }
    }).catch(error => {
      console.log('readCSV error: ' + JSON.stringify(error));
    });
  }

  csvToArray(csv, start, end) {
    console.log('1231event csvToArray()');
    if (csv.length === 0 && csv.length < end) {
      return [];
    }
    let lines = csv.split('\n');
    //  console.log('csvToArray lines: ' + lines);

    /*  setTimeout(() => {
      var lines2 = JSON.stringify(lines).replace(/,/g, ';');
      console.log('csvToArray lines2: ' + lines2);
    }, 1500);*/

    var result = [];

    var headers = lines[0].split(',');
    headers = headers.slice(0, 11);
    headers.push('rbs_marker_off');

    let initial = 1;

    // Using the condition below, we are guaranteed to display only the latest 24 hour record from the
    // csv file.
    if (lines.length > this.twentyFourHourLimit) {
      initial = lines.length - this.twentyFourHourLimit;
    }

    for (var i = initial; i < lines.length; i++) {

      var obj = {};
      var currentline = lines[i].split(',');
      //  console.log('csvToArray currentline: ' + currentline);

      for (var j = 0; j < headers.length; j++) {
        const header = headers[j].replace('\"', '').replace('\"', '');
        if (currentline[j] !== undefined) {
          obj[header] = currentline[j].replace('\"', '').replace('\"', '');
          //  console.log('csvToArray obj[header]: ' + obj[header]);
        }
      }

      //  console.log('csvToArray obj: ' + JSON.stringify(obj));
      result.push(obj);
    }
    //  console.log('csvToArray result: ' + JSON.stringify(result));
    return result.slice(start, end);
  }

  viewHistory() {
    console.log('1231readcsv: viewHistory()');
    const xAxisZoom = CONFIG.OPT5_ZOOM;
    this.readCSV(this.axisMaxHistory - pageLimit, this.axisMaxHistory);

    this.setState({
      axisMinimumHistory: this.axisMaxHistory - pageLimit,
      axisMaximumHistory: this.axisMaxHistory,
      modalHistoryVisible: true,
      zoomHistory: {
        xValue: this.axisCtr,
        yValue: 0,
        scaleX: xAxisZoom,
        scaleY: 0
      }
    });
  }

  render() {
    let {values} = this.state;
    let {historyValues} = this.state;
    let config = this.getChartData(values);
    let historyConfig = this.getHistoryChartData(historyValues);
    return (
      <View style={widgets.mainBrainwaveView}>
        <HeaderMain hideTopHeader='true' setMarker={this.onSetMarker}/>
        <ScrollView>
          <View style={widgets.brainwaveChartCon}>
            <ImageBackground
              style={widgets.gradientBackground}
              source={require('../../images/bg_image.png')}>
              <View style={widgets.commonFlexRowStyle}>
                <View
                  style={Platform.OS === ''
                  ? widgets.yAxisGraphScale
                  : widgets.yAxisGraphScaleIOS}>
                  <View style={{
                    flexDirection: 'row'
                  }}>
                    <Image
                      style={widgets.frequencyIcons}
                      source={require('../../images/gammaicon.png')}/>
                    <Text style={widgets.frequencyText}>{I18n.t('GAMMA')}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row'
                  }}>
                    <Image
                      style={widgets.frequencyIcons}
                      source={require('../../images/betaicon.png')}/>
                    <Text style={widgets.frequencyText}>{I18n.t('BETA')}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row'
                  }}>
                    <Image
                      style={widgets.frequencyIcons}
                      source={require('../../images/alphaicon.png')}/>
                    <Text style={widgets.frequencyText}>{I18n.t('ALPHA')}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row'
                  }}>
                    <Image
                      style={widgets.frequencyIcons}
                      source={require('../../images/thetaicon.png')}/>
                    <Text style={widgets.frequencyText}>{I18n.t('THETA')}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row'
                  }}>
                    <Image
                      style={widgets.frequencyIcons}
                      source={require('../../images/deltaicon.png')}/>
                    <Text style={widgets.frequencyText}>{I18n.t('DELTA')}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row'
                  }}>
                    <Image
                      style={widgets.frequencyIcons}
                      source={require('../../images/empty-icon.png')}/>
                    <Text style={widgets.frequencyText}/>
                  </View>
                </View>
                <View style={widgets.lineChartView}>
                  <CombinedChart
                    style={{
                    flex: 1,
                    flexDirection: 'row'
                  }}
                    data={config.data}
                    chartDescription={{
                    text: ''
                  }}
                    drawGridBackground={false}
                    legend={this.state.legend}
                    marker={this.state.marker}
                    xAxis={config.xAxis}
                    yAxis={this.state.yAxis}
                    gridBackgroundColor={processColor('#ffffff')}
                    borderColor={processColor('teal')}
                    borderWidth={1}
                    drawBorders={false}
                    touchEnabled={true}
                    dragEnabled={true}
                    dragDecelerationEnabled={true}
                    dragDecelerationFrictionCoef={0.5}
                    scaleEnabled={false}
                    scaleXEnabled={false}
                    scaleYEnabled={false}
                    pinchZoom={false}
                    animation={{
                    durationX: 500,
                    durationY: 500
                  }}
                    drawBarShadow={false}
                    drawValueAboveBar={false}
                    autoScaleMinMaxEnabled={true}
                    zoom={this.state.zoom}
                    doubleTapToZoomEnabled={false}
                    chartBackgroundColor={processColor('transparent')}
                    onSelect={this
                    .handleSelect
                    .bind(this)}
                    ref="brainwaveChart"/>
                </View>
                <Modal
                    animationIn='zoomIn'
                    animationOut='zoomOut'
                    backdropColor='transparent'
                    onBackButtonPress={() => {
                      this.setState({ progressModal: false });
                    }}
                    onRequestClose={() => {
                      this.setState({progressModal: false});
                    }}
                    isVisible={this.state.progressModal}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 200 }}>
                      <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
                    </View>
                  </Modal>

              </View>
              <View style={containers.setIntervalCon}>
                <View style={containers.brainwaveIntervalCon}>
                  <Text
                    style={widgets.textInterval}
                    onPress={() => this.viewHistory() }>{I18n.t('VIEW_HISTORY')}</Text>
                  <Text
                    style={widgets.textInterval}
                    onPress={() => this.setState({modalIntervalVisible: true}) }>{I18n.t('SET_INTERVAL')}</Text>
                </View>
              </View>

            </ImageBackground>
            <View style={widgets.lowerSectionBrainwave}>
              <Modal
                animationType="slide"
                backdropColor={'rgba(52, 52, 52, 0.8)'}
                backdropOpacity={1}
                isVisible={this.state.modalIntervalVisible}
                onBackdropPress={() => {this.setState({modalIntervalVisible: false});}}
                presentationStyle='overFullScreen'
                onRequestClose={() => {
                  this.setState({modalIntervalVisible: false});
                }}>
                <View style={widgets.modalView}>
                  <View style={widgets.intervalView}>
                    <Text
                      style={[
                      widgets.commonFont2, {
                        flex: 0.6,
                        borderBottomColor: 'white',
                        borderBottomWidth: 2,
                        marginTop: 10
                      }
                    ]}>{I18n.t('TIME_INTERVAL')}</Text>
                    <View
                      style={[
                      widgets.wrapper, {
                        flex: 2
                      }
                    ]}>
                      <CheckBox
                        style={widgets.checkbox}
                        title={this.fiveSec}
                        onIconPress={() => this.onCheckboxPressed(5)}
                        onPress={() => this.onCheckboxPressed(5)}
                        textStyle={{ fontSize: 12 }}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.interval5}/>

                      <CheckBox
                        style={widgets.checkbox}
                        title={this.tenSec}
                        onPress={() => this.onCheckboxPressed(10)}
                        textStyle={{ fontSize: 12 }}
                        onIconPress={() => this.onCheckboxPressed(10)}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.interval10}/>

                      <CheckBox
                        style={widgets.checkbox}
                        title={this.twentySec}
                        onPress={() => this.onCheckboxPressed(20)}
                        textStyle={{ fontSize: 12 }}
                        onIconPress={() => this.onCheckboxPressed(20)}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.interval20}/>
                    </View>
                  </View>
                </View>
              </Modal>

              <Modal
                animationType="slide"
                backdropColor={'rgba(52, 52, 52, 0.8)'}
                backdropOpacity={1}
                isVisible={this.state.modalHistoryVisible}
                presentationStyle='overFullScreen'
                onBackdropPress={() => { this.setState({modalHistoryVisible: false}); }}
                onRequestClose={() => {
                  this.setState({modalHistoryVisible: false});
                }}>

                <ImageBackground
                  style={widgets.historyGradientBackground}
                  source={require('../../images/bg_image.png')}>
                  <View style={widgets.historyFlexRowStyle}>
                    <View
                      style={Platform.OS === ''
                      ? widgets.yAxisGraphScale
                      : widgets.yAxisGraphScaleIOS}>
                      <View style={{
                        flexDirection: 'row'
                      }}>
                        <Image
                          style={widgets.frequencyHistoryIcons}
                          source={require('../../images/gammaicon.png')}/>
                        <Text style={widgets.frequencyText}>{I18n.t('GAMMA')}</Text>
                      </View>
                      <View style={{
                        flexDirection: 'row'
                      }}>
                        <Image
                          style={widgets.frequencyHistoryIcons}
                          source={require('../../images/betaicon.png')}/>
                        <Text style={widgets.frequencyText}>{I18n.t('BETA')}</Text>
                      </View>
                      <View style={{
                        flexDirection: 'row'
                      }}>
                        <Image
                          style={widgets.frequencyHistoryIcons}
                          source={require('../../images/alphaicon.png')}/>
                        <Text style={widgets.frequencyText}>{I18n.t('ALPHA')}</Text>
                      </View>
                      <View style={{
                        flexDirection: 'row'
                      }}>
                        <Image
                          style={widgets.frequencyHistoryIcons}
                          source={require('../../images/thetaicon.png')}/>
                        <Text style={widgets.frequencyText}>{I18n.t('THETA')}</Text>
                      </View>
                      <View style={{
                        flexDirection: 'row'
                      }}>
                        <Image
                          style={widgets.frequencyHistoryIcons}
                          source={require('../../images/deltaicon.png')}/>
                        <Text style={widgets.frequencyText}>{I18n.t('DELTA')}</Text>
                      </View>
                      <View style={{
                        flexDirection: 'row'
                      }}>
                        <Image
                          style={widgets.frequencyHistoryIcons}
                          source={require('../../images/empty-icon.png')}/>
                        <Text style={widgets.frequencyText}/>
                      </View>
                    </View>
                    <View style={widgets.lineChartView}>
                        <CombinedChart
                          style={{
                            flex: 1,
                            flexDirection: 'row'
                          }}
                          data={historyConfig.dataForHistory}
                          chartDescription={{
                            text: ''
                          }}
                          drawGridBackground={false}
                          legend={this.state.legend}
                          marker={this.state.marker}
                          xAxis={historyConfig.xAxisHistory}
                          yAxis={this.state.yAxis}
                          gridBackgroundColor={processColor('#ffffff')}
                          borderColor={processColor('teal')}
                          borderWidth={1}
                          drawBorders={false}
                          touchEnabled={true}
                          dragEnabled={true}
                          dragDecelerationEnabled={true}
                          dragDecelerationFrictionCoef={0.5}
                          scaleEnabled={false}
                          onChange={(event) => this.onChangeListener(event.nativeEvent)}
                          scaleXEnabled={false}
                          scaleYEnabled={false}
                          pinchZoom={false}
                          animation={{
                            durationX: 500,
                            durationY: 500
                          }}
                          drawBarShadow={false}
                          drawValueAboveBar={false}
                          autoScaleMinMaxEnabled={true}
                          zoom={this.state.zoomHistory}
                          doubleTapToZoomEnabled={false}
                          chartBackgroundColor={processColor('transparent')}
                          onSelect={this.handleSelect.bind(this)}
                          ref="brainwaveHistory"/>
                    </View>
                  </View>
                </ImageBackground>
              </Modal>

              <View style={widgets.dateAndArrowContainer}>
                <Text style={widgets.commonFont}>{this.state.dateSet}</Text>
              </View>

              <View style={widgets.meditationAttentionCon}>
                <Text
                  onPress={() => {}}
                  style={Platform.OS === 'ios'
                  ? [
                    widgets.brainWaveText,
                    widgets.commonMarginTop2, {
                      marginRight: 5
                    }
                  ]
                  : [widgets.brainWaveText, widgets.commonMarginTop2]}>{I18n.t('BRAIN_CONDITIONS')}</Text>
                <View style={widgets.signupInfoCon}>
                  <View
                    style={{
                    flex: 0.27,
                    height: 150,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}>
                    <Image
                      style={{
                      height: 16,
                      width: 16
                    }}
                      source={require('../../images/blue.png')}/>
                    <Text style={[widgets.centerText, widgets.attentionText]}>{I18n.t('ATTENTION')}{'\n'} {this.state.attentionRate}</Text>
                  </View>

                  <View
                    style={{
                    flex: 0.50,
                    height: Platform.OS === 'android'
                      ? 230
                      : 150,
                    justifyContent: 'center'
                  }}>
                    <PieChart
                      padding={0}
                      style={Platform.OS === 'ios'
                      ? widgets.pieChartIOS
                      : widgets.pieChart}
                      chartBackgroundColor={processColor('transparent')}
                      animation={{
                      durationX: 500,
                      durationY: 500
                    }}
                      logEnabled={true}
                      chartDescription={{
                      text: ''
                    }}
                      data={this.state.pieChartData}
                      legend={{
                      enabled: false
                    }}
                      touchEnabled={false}
                      entryLabelColor={processColor('black')}
                      entryLabelTextSize={20}
                      rotationEnabled={false}
                      drawSliceText={false}
                      usePercentValues={false}
                      holeRadius={85}
                      holeColor={processColor('transparent')}
                      transparentCircleRadius={85}
                      transparentCircleColor={processColor('#f0f0f088')}
                      maxAngle={360}/>
                  </View>
                  <View
                    style={{
                    flex: 0.27,
                    height: 150,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}>
                    <Image
                      style={{
                      height: 16,
                      width: 16
                    }}
                      source={require('../../images/purple.png')}/>
                    <Text
                      style={[
                      widgets.centerText,
                      widgets.meditationText, {
                        marginLeft: 5
                      }
                    ]}>{I18n.t('MEDITATION')}{'\n'}{this.state.meditationRate}</Text>
                  </View>
                </View>
              </View>

            </View>
          </View>
        </ScrollView>

      </View>
    );
  }
}

export default connect(mapStateToProps, {})(BrainwaveAnalysis);