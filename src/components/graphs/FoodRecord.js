import React, { Component } from 'react';
import HeaderMain from '../common/HeaderMain';
import * as DateUtil from '../../utils/DateUtils';
import * as Progress from 'react-native-progress';
import getCurrentDate from '../../utils/DateUtils';
import I18n from '../../translate/i18n/i18n';
import Modal from 'react-native-modal';
import { COLOR_THUMB_TINT } from '../../styles/colors';
import { showToast,savePreference, getPreference } from '../../utils/CommonMethods';

import {
  View,
  ScrollView,
  processColor,
  Text,
  Image,
  Alert,
  AppState,
  Platform,
  RefreshControl,
  PermissionsAndroid,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from '../../styles/widgets';
import { containers } from '../../styles/containers';
import { text } from '../../styles/text';
import { CombinedChart } from 'react-native-charts-wrapper';
import ChartDuration from '../common/ChartDuration';
import { Actions } from 'react-native-router-flux';
import { ChartUtils } from './../../utils/ChartUtils';
import SetTargetModal from '../common/SetTargetModal';
import { updateSettingsModule } from '../../redux/actions';
import { connect } from 'react-redux';
import { TARGET_LINE_COLOR } from '../../styles/colors';
import RenderIf from '../RenderIf';

const { height } = Dimensions.get('window');

const LINE_CHART_VALUES = [
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0
];

var initialDataCalMargin;

switch (height) {
  case 812: {
    initialDataCalMargin = 0;
    break;
  }
  case 740:
  case 736: { //iphone 6-8+
    initialDataCalMargin = 0;
    break;
  }
  case 667: {//iphone6-8
    initialDataCalMargin = 0;
    break;
  }
  case 640: { //common android
    initialDataCalMargin = 0;
    break;
  }
  default: { //iphone5/SE else heigher
    if (height < 640) {
      initialDataCalMargin = 20;
    } else {
      initialDataCalMargin = 0;
    }
    break;
  }
}

const revs = [];
let activity = [0];
let targetCals = [0];
let heartRates = [0];
let timeIntervals = [0];
let kcals = [0];
let enableTarget = true;

let weeklySteps = [0];
let weeklyHeartRates = [0];
let weeklyTimeIntervals = [0];
let weeklyKcals = [0];
let weeklyDividendsForBPM = [0];

let monthlySteps = [0];
let monthlyHeartRates = [0];
let monthlyTimeIntervals = [0];
let monthlyKcals = [0];

let yearlySteps = [0];
let yearlyHeartRates = [0];
let yearlyTimeIntervals = [0];
let yearlyKcals = [0];

var activityTimer;
let ACTIVITY_DATA = [];
let initConnectTime = '';

const uuidv4 = require('uuid/v4');
const ACTIVITY = I18n.t('ACTIVITY');
const SLEEP = I18n.t('SLEEP');

const SLEEP_DATA = [0];
const TARGET_FOOD = [];
let daily_food_value = [];
let justRefreshed = false;

let changedColor = 'transparent';
let changedTarget = 0;
let changedCals = 0;

let foodHighest = 0;
const plusHighest = 50;

const defaultMarker = {
  enabled: true,
  markerColor: processColor('transparent'),
  textColor: processColor('#000000'),
  markerFontSize: 8
};

const invisibleMarker = {
  enabled: false
};


const { getFormattedTimestamp, getMonthNames, getMonthComplete } = DateUtil;
const COUCHBASE_CRED = require('../../config/demo_couchbase_cred.json');

const mapStateToProps = (state) => {
  const { dateAndDocumentMap, targetCals, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins } = state.settingsModule;
  console.log('123watch-- dateAndDocumentMap ' + JSON.stringify(dateAndDocumentMap));

  // This is how we put a value to initConnectTime. initConnectTime should be initialized if its current value is empty
  // and if the dateSmartBandConnected is not empty. this variable will be used to compare the current time to the
  // first time the smartband has been connected to the app.
  if (typeof dateAndDocumentMap !== 'undefined' && !isSmartBandEnabled) {
    initConnectTime = dateAndDocumentMap.time;
    console.log('123redux! state changed: connected!');
  }

  return {
    dateAndDocumentMap, targetCals, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins
  };
};

class FoodRecord extends Component {

  addCalories() {
    let cals = this.props.targetCals;
    let calsLabel = '';

    if (cals !== 99999) {
      cals += 1;
  
    calsLabel = cals <= 1 ? ' ' + I18n.t('CALORIE_DAY') : ' ' + I18n.t('CALORIES_DAY');

    const target = cals + calsLabel;
    const color = cals === 0 ? 'transparent' : 'rgb(233, 186, 0)';
    this.setState({ targetCals: target, lineColor: color });

    this.props.updateSettingsModule({ prop: 'targetCals', value: cals });

    if (cals > foodHighest) {
      this.updateYAxisState(cals + plusHighest);
    }
    else {
      this.updateYAxisState(foodHighest + plusHighest);
    }

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.updateLineChartValues(cals), color);
    }
  }

  componentDidMount() {
    showToast(I18n.t('SWIPE_DOWN_REFRESH'));
    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(res => {
        if (res) {
          console.log('Permission is OK');
          // todo: confirm with team/PM about what we're gonna do with an unconfirmed permission.
        } else {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }
    const currentDate = getCurrentDate();
    const dateNow = new Date(currentDate);
    console.log('123redux! isSmartBandEnabled: ' + this.props.isSmartBandEnabled);
    // we need to clear dateAndDocumentMap if the date in it is not the same as the current date.

    this.getDatabase();
  }

  componentWillMount() {
    if (this.props.targetCals <= 0) {
      this.setState({
        isSetTargetVisible: 'flex', isSetTargetDisplayed: 1,
        targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0
      });
    }
    else {
      this.setState({
        isSetTargetVisible: 'none', isSetTargetDisplayed: 0,
        targetAddSubtractVisible: 'flex', targetAddSubtractDisplayed: 1
      });
    }


    clearInterval(this.activityTimer);

  }

  componentWillReceiveProps() {
    getPreference('isFoodAdded').then(res => {
      if (res==='true') {
        this.getDatabase();
        savePreference('isFoodAdded', 'false');
      } 
    });
  }


  constructor() {
    super();

    this.state = {
      progressModal: false,
      dragEnabled: true,
      highlights: [{ x: -1, y: 0, stackIndex: 27, dataIndex: 0 }],
      initialDataCal: I18n.t('NO_RECORD'),
      display: 'none',
      opacity: 0,
      isTargetTextVisible: 0,
      isTargetDisplayed: 'flex',
      isCalsDisplayed: 'none',
      isCalsTextVisible: 1,
      isSetTargetVisible: 'flex',
      isSetTargetDisplayed: 1,
      targetAddSubtractVisible: 'none',
      targetAddSubtractDisplayed: 0,
      earliestYear: new Date().getFullYear(),
      nextDateTextColor: 'rgb(127, 127, 127)',
      previousDateTextColor: 'rgb(127, 127, 127)',
      lineColor: 'transparent',
      isNoDataAvailable: false,
      chosenYear: 0,
      chosenMonth: 0,
      selectedInterval: '',
      daysInMonth: 0,
      dailyActivities: [],
      rows: {},
      lastDocumentStored: {},
      targetText: I18n.t('SET_TARGET'),
      modalVisible: false,
      targetCals: 0 + ' ' + I18n.t('CALORIE_DAY'),
      isTrophyVisible: true,
      isRefreshing: false,
      activityData: {},
      scanning: false,
      appState: '',
      isDateVisible: true,
      isRightArrowVisible: true,
      isLeftArrowVisible: true,
      leftArrow: '<',
      rightArrow: '>',
      marker: defaultMarker,
      dateSet: this.getCurrentDate(),
      legend: {
        enabled: true,
        textSize: 0,
        textColor: processColor('transparent'),
        form: 'SQUARE',
        formSize: 0,
        xEntrySpace: 0,
        yEntrySpace: 0,
        formToTextSpace: 0,
        wordWrapEnabled: true,
        maxSizePercent: 0
      },
      data: {
        barData: {
          dataSets: [
            {
              values: ACTIVITY_DATA,
              label: I18n.t('FOOD_RECORD'),
              colors: [
                processColor('#77b1c2')
              ],
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                textSize: 15,
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }],
        },
        lineData: {
          dataSets: [
            {
              label: '',
              values: LINE_CHART_VALUES,
              config: {
                colors: [processColor('grey')],
                drawValues: false,
                valueTextSize: 18,
                valueFormatter: '#',
                mode: 'CUBIC_BEZIER',
                drawCircles: false,
                lineWidth: 2,
                drawFilled: false,
                dashedLine: {
                  lineLength: 20,
                  spaceLength: 20
                }
              }
            }
          ]
        }
      },
      yAxis: {
        left: {
          drawGridLines: false,
          textSize: 15,
          axisMinimum: 0,
          axisMaximum: 840
          /*
          **  note: to avoid the y-coordinate from changing its left side grid values whenever scrolling the graph to the right (and vice-versa)
          **  and having this weird "wobbling" effect, always set an axisMaximum. The
          **  axisMaximum limits the highest value to be displayed on the left side of the
          **  y-axis of the graph.
  
          **  What I usually use as the value for the axisMaximum is
          **  the highest value in the Activity array and add it up with a hundred The 100
          **  I add is just for aesthetics, so the highest value for Activity wouldn't
          **  reach the ceiling of the graph. i.e. [100, 800, 950, 400, 370] // the
          **  axisMaximum would be 950 + 100 = 1050.
          */
        },
        right: {
          enabled: false,
        }
      },
      xAxis: {
        valueFormatter: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
        granularityEnabled: true,
        textSize: 15,
        granularity: 1,
        axisMaximum: 31,
        avoidFirstLastClipping: true,
        axisMinimum: 0,
        position: 'BOTTOM'
      },
      zoom: {
        xValue: 0,
        yValue: 0,
        scaleX: 4,
        scaleY: 0
      }
    };
  }

  filterByChosenMonthAndYear() {
    // we need to sort the daily chart by the (chosen or current) month and year.
    // Note: chosenMonth and chosenYear are both initialized to the current month and current year in
    // componentDidUpdate().
    const sortedChartData = [];
    let earliestYear = this.state.earliestYear;
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.rows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.rows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      console.log('123redux!: extractedYear: ' + extractedYear + '\textractedMonth: ' + extractedMonth);

      // Note we add 1 to chosenMonth here because chosenMonth is also used in monthNames array, and monthNames array starts with index 0, hence chosenMonth is by
      // default should start with 0 (for January) and end with 11 (for December).

      console.log('123redux!: this.state.chosenYear: ' + this.state.chosenYear + '\this.state.chosenMonth: ' + (this.state.chosenMonth + 1));
      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          _rev: this.state.rows[i].doc._rev,
          date: this.state.rows[i].doc.date,
          cals_count: this.state.rows[i].doc.cals,
          time: this.state.rows[i].doc.time,
          user_id: this.state.rows[i].doc.user_id,
          target_cals: this.state.rows[i].doc.target_cals,
          food_name: this.state.rows[i].doc.food_name
        };

        sortedChartData[i] = rec;
      }
    }

    this.setState({ earliestYear: earliestYear });
    console.log('123yearmonth: earliestYear: ' + this.state.earliestYear);

    // we remove all undefined elements in allChartData
    for (let i = sortedChartData.length; i > 0; i--) {
      if (typeof sortedChartData[i] === 'undefined') {
        console.log('123redux! sortedChartData is null or undefined!');
        sortedChartData.splice(i, 1);
      }
    }

    console.log('123redux!: sortedChartData: ' + JSON.stringify(sortedChartData));
    return sortedChartData;
  }

  getCurrentDate() {
    const strDate = new Date();
    console.log('MONTHS**', 'months: ' + getMonthComplete());
    return strDate.getDate() + '\n' + getMonthComplete()[strDate.getMonth()].toUpperCase() + ', ' + strDate.getFullYear();
  }


  getActualDaysInAMonth() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    // to get the actual days in the month, we need to also check if the graph is showing the current/previous month.
    const monthToUse = this.state.chosenMonth === month ? month + 1 : this.state.chosenMonth + 1;
    const yearToUse = this.state.chosenYear === year ? year : this.state.chosenYear;
    return this.getDaysInMonth(monthToUse, yearToUse);
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }


  getDatabase() {
    manager.database.get_db({ db: DB_NAME })
      .then(res => {
        this.setupViewAndQuery().then(res => {
          this.scaleChartForDailyInterval(5);
          this.setState({ rows: res.obj.rows });
          setTimeout(() => {
            this.renderDailyChart();
          }, 5);
        });

      }).catch(e => {
        console.log('couchbase: exception: ' + JSON.stringify(e));
      });
  }

  getHighestValue(chartValues) {
    let highest = 0;

    if (typeof chartValues[0] !== 'undefined' && typeof chartValues[0].y !== 'undefined') {

      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i].y > highest) {
          highest = chartValues[i].y;
        }
      }
    }
    else {
      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i] > highest) {
          highest = chartValues[i];
        }
      }
    }
    return highest;
  }

  getSortedMonthlyData() {
    const sortedStepsChartData = [];
    const sortedTimeChartData = [];
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      if (this.state.chosenYear === extractedYear) {
        const recStep = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.cals
        };

        const recTime = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.time
        };

        sortedStepsChartData[i] = recStep;
        sortedTimeChartData[i] = recTime;
      }
    }

    // we remove all undefined elements in allChartData
    for (let i = 0; i < sortedStepsChartData.length; i++) {
      if (typeof sortedStepsChartData[i] === 'undefined') {
        sortedStepsChartData.splice(i, 1);
      }

      if (typeof sortedTimeChartData[i] === 'undefined') {
        sortedTimeChartData.splice(i, 1);
      }
    }

    // this is an array of all the sorted data for activity-sleep analysis.
    const sortedChartData = [sortedStepsChartData, sortedTimeChartData];

    return sortedChartData;
  }

  getWeekRange(limit) {
    var weeks = [];
    let arrCtr = 0;
    let lastRange = 0;

    for (let i = 0; i < limit; i++) {
      if (i === 0) {
        weeks[arrCtr] = '0';
        arrCtr++;
        continue;
      }

      if (i % 7 == 0) {
        weeks[arrCtr] = (i - 6) + '-' + i;
        arrCtr++;
        lastRange = i;
      }
    }

    weeks.push((limit - lastRange) > 1 ? (lastRange + 1) + "-" + limit : (lastRange + 1)) + '';
    return weeks;
  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
    }

    this.setState({ appState: nextAppState });
  }

  handleSelect(event) {
    const entry = event.nativeEvent;
    let dateToShow;
    if (entry === null) {
      this.setState({
        ...this.state,
        selectedEntry: null
      });
    } else {
      this.setState({
        ...this.state,
        selectedEntry: JSON.stringify(entry)
      });

      if (this.state.selectedInterval === I18n.t('DAILY')) {

        if (entry.x < new Date().getDate() || entry.x > new Date().getDate()) {
          this.setState({
            targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0, isTargetDisplayed: 'none',
            isCalsTextVisible: 0, isSetTargetVisible: 'flex', isSetTargetDisplayed: 0, display: 'flex', opacity: 1
          });
        }
        else if (entry.x === new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
          this.setState({
            targetAddSubtractVisible: 'flex', targetAddSubtractDisplayed: 1, isTargetDisplayed: 'flex',
            isCalsTextVisible: 1, isSetTargetVisible: 'none', isSetTargetDisplayed: 0, isCalsDisplayed: 'none',
            isTargetTextVisible: 0, display: 'none', opacity: 0
          });
        }
        if (this.props.targetCals === 0) {
          this.setState({
            isSetTargetVisible: 'flex', isSetTargetDisplayed: 1,
            targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0
          });
        }


      }

      if (typeof entry.x !== 'undefined' && typeof entry.y !== 'undefined') {
        const xVal = Math.round(entry.x);

        if (this.state.selectedInterval === I18n.t('DAILY')) {
          // we show the day and month here
          if (typeof activity[xVal - 1] !== 'undefined') {
            let step = activity[xVal - 1].y;
            let heartRate = heartRates[xVal - 1];
            let time = timeIntervals[xVal - 1];

            let cal = kcals[xVal - 1];
            let hour = Math.floor(parseInt(time) / 60);
            let mins = parseInt(time) % 60;

            if (typeof hour === 'undefined' || isNaN(hour)) {
              hour = 0;
            }

            if (typeof mins === 'undefined' || isNaN(mins)) {
              mins = 0;
            }

            if (typeof step === 'undefined') {
              step = 0;
            }

            if (typeof kcal === 'undefined') {
              cal = 0;
            }

            if (typeof heartRate === 'undefined') {
              heartRate = 0;
            }

            const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');
            dateToShow = xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear;

            if (xVal === this.state.daysInMonth + 1) {
              dateToShow = this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase();
            }

            this.setState({
              dateSet: dateToShow
            });

            if (typeof TARGET_FOOD[xVal] === 'undefined' || isNaN(TARGET_FOOD[xVal])) {
              return;
            }


            if (TARGET_FOOD[xVal] > foodHighest) {
              this.updateYAxisState(TARGET_FOOD[xVal] + plusHighest);
            }
            else {
              this.updateYAxisState(foodHighest + plusHighest);
            }

            this.setState({ initialDataCal: entry.y > 1 ? entry.y +  ' ' + I18n.t('CALS') : entry.y +  ' ' + I18n.t('CAL') });

            let arr = this.updateLineChartValues(TARGET_FOOD[xVal]);
            let remove_elements = arr.splice(xVal, 1, null);
            console.log('WEW: ', arr);

            const color = TARGET_FOOD[xVal] === 0 ? 'transparent' : 'rgb(233, 186, 0)';

            this.updateDataSets(this.state.data.barData.dataSets[0].values,
              arr, color);

            if (TARGET_FOOD[xVal] === 0) {
              this.updateDataSets(this.state.data.barData.dataSets[0].values,
                this.updateLineChartValues(0), 'transparent');
            }

            if (xVal >= new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {


              if (this.props.targetCals === 0) {
                this.updateDataSets(this.state.data.barData.dataSets[0].values,
                  this.updateLineChartValues(0), 'transparent');
              }
              else {
                this.updateDataSets(this.state.data.barData.dataSets[0].values,
                  this.updateLineChartValues(this.props.targetCals), TARGET_LINE_COLOR);
              }

              if (this.props.targetCals === 0 || this.props.targetCals <= foodHighest) {
                this.updateYAxisState(foodHighest + plusHighest);
              }
              else {
                this.updateYAxisState(this.props.targetCals + plusHighest);
              }

              if (TARGET_FOOD[xVal] !== 0) {
                this.updateDataSets(this.state.data.barData.dataSets[0].values,
                  arr, color);
              }

            }
          }
          else {
            if (xVal === 0 || xVal > this.state.daysInMonth) {
              this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
            }
            else {
              this.setState({ dateSet: xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear });
            }
          }

          if (typeof activity[xVal] === 'undefined' || activity[xVal] === 0) {
            this.setState({ initialDataCal: 0 + ' ' + I18n.t('CAL') })
          }

        } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
          let step = weeklySteps[xVal - 1];
          let heartRate = weeklyHeartRates[xVal - 1];
          const time = weeklyTimeIntervals[xVal - 1];

          let kcal = weeklyKcals[xVal - 1];
          let hour = Math.floor(parseInt(time) / 60);
          let mins = parseInt(time) % 60;

          if (typeof hour === 'undefined' || isNaN(hour)) {
            hour = 0;
          }

          if (typeof mins === 'undefined' || isNaN(mins)) {
            mins = 0;
          }

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }

          const dividend = weeklyDividendsForBPM[xVal - 1];

          // compute the average for weekly bpm
          if (dividend > 0) {
            heartRate /= dividend;
            heartRate = heartRate.toFixed(2);
          }

          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');

          if (typeof this.state.xAxis.valueFormatter[xVal] === 'undefined' || xVal === 0) {
            this.setState({
              dateSet: getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear, initialDataCal: entry.y + ' ' + I18n.t('CAL')
            });
          }
          else if (entry.y < 2) {
            this.setState({
              dateSet: this.state.xAxis.valueFormatter[xVal] + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear, initialDataCal: entry.y + ' ' + I18n.t('CAL')
            });
          }
          else {
            this.setState({
              dateSet: this.state.xAxis.valueFormatter[xVal] + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear, initialDataCal: entry.y + ' ' + I18n.t('CALS')
            });
          }

        } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {

          if (xVal === 0) {
            return;
          }

          let step = monthlySteps[xVal - 1];
          let heartRate = monthlyHeartRates[xVal - 1];

          let kcal = isNaN(monthlyKcals[xVal - 1]) ? 0 : monthlyKcals[xVal - 1];
          let time = monthlyTimeIntervals[xVal - 1];

          let hour = Math.floor(parseInt(time) / 60);
          let mins = parseInt(time) % 60;

          if (typeof hour === 'undefined' || isNaN(hour)) {
            hour = 0;
          }

          if (typeof mins === 'undefined' || isNaN(mins)) {
            mins = 0;
          }

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }

          let dividend = 0;


          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');

          if (entry.y < 2) {
            this.setState({
              dateSet: this.state.chosenYear + '\n' + (typeof getMonthComplete()[xVal - 1].toUpperCase() === 'undefined' ? I18n.t('JANUARY') : getMonthComplete()[xVal - 1].toUpperCase()), initialDataCal: entry.y + ' ' + I18n.t('CAL')
            });
          }
          else {
            this.setState({
              dateSet: this.state.chosenYear + '\n' + (typeof getMonthComplete()[xVal - 1].toUpperCase() === 'undefined' ? I18n.t('JANUARY') : getMonthComplete()[xVal - 1].toUpperCase()), initialDataCal: entry.y + ' ' + I18n.t('CALS')
            });
          }



        } else {

          if (this.state.selectedInterval === I18n.t('YEARLY')) {

            if (entry.y < 2) {
              this.setState({ dateSet: this.state.xAxis.valueFormatter[xVal], initialDataCal: entry.y + ' ' + I18n.t('CAL') });
            }
            else {
              this.setState({ dateSet: this.state.xAxis.valueFormatter[xVal], initialDataCal: entry.y + ' ' + I18n.t('CALS') });
            }



          }

          // Note: there's a bug with the graph wherein if you click a bar item, sometimes it returns the zero index,
          // even though it's not the right x-index. We're catching it here for the yearly interval. I haven't observe
          // it occuring in the other intervals, prolly because the way the yearly graph is rendered.
          if (xVal === 0 || typeof (yearlySteps[xVal - 1] === 'undefined')) {
            return;
          }

          // Note: yearlySteps is structured this way: [{year: 0, count: 100}] - only a sample structure
          // the reason this is structured this way is because of the ChartUtils.getYearlyBarYAxisValues()
          // I've made it reusable for the other developers to use for their records tasks.
          let step = yearlySteps[xVal - 1].count;

          let heartRate = yearlyHeartRates[xVal - 1].count;

          let kcal = isNaN(yearlyKcals[xVal - 1].count) ? 0 : yearlyKcals[xVal - 1].count;
          let time = yearlyTimeIntervals[xVal - 1].count;

          let hour = Math.floor(parseInt(time) / 60);
          let mins = parseInt(time) % 60;

          if (typeof hour === 'undefined' || isNaN(hour)) {
            hour = 0;
          }

          if (typeof mins === 'undefined' || isNaN(mins)) {
            mins = 0;
          }

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }


          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');
          this.setState({
            dateSet: this.state.xAxis.valueFormatter[xVal],
          });
        }
      }
    }
  }

  goToAddFoodScreen() {
    if (daily_food_value.length !== 0) {
      Actions.addFood();
    }

    else if (this.props.targetCals === 0) {
      Alert.alert(
        '',
        I18n.t('SET_TARGET_FIRST'),
        [
          { text: I18n.t('LATER'), onPress: () => Actions.addFood(), style: 'cancel' },
          {
            text: I18n.t('OK'), onPress: () => console.log('OK Pressed'), style: 'cancel'
          }
        ],
        { cancelable: false }
      )
    }
    else {
      Alert.alert(
        I18n.t('TARGET_TODAY') + ': ' + this.props.targetCals + ' ' + I18n.t('CALS'),
        I18n.t('PROCEED'),
        [
          { text: I18n.t('CANCEL'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          {
            text: I18n.t('OK'), onPress: () => {
              Actions.addFood();
            }
          }
        ],
        { cancelable: false }
      )
    }

  }


  onChangeTarget(txt) {
    if (!isNaN(txt)) {
      let cals = 0;
      const tmpCals = isNaN(parseInt(txt, 10)) ? Math.round(txt) : parseInt(Math.round(txt), 10);
      if (tmpCals > 0) {
        cals = tmpCals;
      }

      if (cals === 0 && this.props.targetCals > 0) {
        cals = this.props.targetCals;
      }

      const tmpchangedCals = isNaN(parseInt(txt, 10)) ? Math.round(txt) : parseInt(Math.round(txt), 10);
      changedCals = tmpchangedCals > 0 ? tmpchangedCals : 0;

      let calsLabel = '';
      calsLabel = cals <= 1 ? ' ' + I18n.t('CALORIE_DAY') : ' ' + I18n.t('CALORIES_DAY');

      const target = cals + calsLabel;
      changedColor = cals === 0 ? 'transparent' : TARGET_LINE_COLOR;
      changedTarget = cals < 1 ? I18n.t('SET_TARGET') : target;

    }
  }

  updateLineChartValues(steps) {
    const lineChartValues = this.state.data.lineData.dataSets[0].values.slice();

    for (var i = 0; i < 35; i++) {
      lineChartValues[i] = steps;
    }

    return lineChartValues;
  }

  onSetTarget() {
    this.setState({ modalVisible: false });
  }

  setupViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'foodByDocId',
      include_docs: true
    });
  }

  onPressDaily() {
    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getDate() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), isTrophyVisible: true, dragEnabled: true });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase(), isTrophyVisible: false, dragEnabled: true });
    }


    if (this.state.chosenYear === new Date().getFullYear()) {
      if (this.state.chosenMonth === new Date().getMonth()) {
        this.setState({
          selectedInterval: I18n.t('DAILY'),
          isTargetDisplayed: 'flex', isCalsTextVisible: 1, isCalsDisplayed: 'none',
          isTargetTextVisible: 0, display: 'none', opacity: 0
        });

        if (this.props.targetCals === 0) {
          this.setState({
            isSetTargetVisible: 'flex', isSetTargetDisplayed: 1,
            targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0
          });
        }
        else {
          this.setState({
            isSetTargetVisible: 'none', isSetTargetDisplayed: 0,
            targetAddSubtractVisible: 'flex', targetAddSubtractDisplayed: 1
          });
        }

      }
      else {
        this.setState({
          selectedInterval: I18n.t('DAILY'),
          isTargetDisplayed: 'none', isCalsTextVisible: 0, isCalsDisplayed: 'none',
          isTargetTextVisible: 0, display: 'flex', opacity: 1
        });

      }
    }
    else {
      this.setState({
        selectedInterval: I18n.t('DAILY'),
        isTargetDisplayed: 'none', isCalsTextVisible: 0, isCalsDisplayed: 'none',
        isTargetTextVisible: 0, display: 'flex', opacity: 1
      });
    }

    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isTrophyVisible: true, isDateVisible: true, isRightArrowVisible: true, isLeftArrowVisible: true });
      this.renderDailyChart();
    }, 50);
  }

  onPressWeekly() {
    this.setState({
      selectedInterval: I18n.t('WEEKLY'), isRightArrowVisible: true, isLeftArrowVisible: true,
      dragEnabled: true, targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0, isTargetDisplayed: 'none',
      isCalsTextVisible: 0, isSetTargetVisible: 'flex', isSetTargetDisplayed: 0, isCalsDisplayed: 'flex',
      isTargetTextVisible: 1, display: 'none', opacity: 0, marker: defaultMarker
    });
    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });

      const year = new Date().getFullYear();
      const month = new Date().getMonth();
      const daysInMonth = this.getActualDaysInAMonth();
      const valueFormatter = [];
      this.setState({ daysInMonth: daysInMonth });
      const activity = [0];
      const sleepData = [0];
      const food = [];

      const sortedChartData = this.filterByChosenMonthAndYear();

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
        for (let j = 0; j < sortedChartData.length; j++) {
          if (typeof sortedChartData[j] !== 'undefined') {
            const day = parseInt(sortedChartData[j].date.slice(-2));
            if ((day - 1) === i) {
              activity[i] = parseInt(sortedChartData[j].cals_count);
              timeIntervals[i] = parseInt(sortedChartData[j].time);
            } else if (typeof activity[i] === 'undefined') {
              activity[i] = 0;
              timeIntervals[i] = 0;
            }
          }
        }
      }

      // todo: Implement a weekly graph view.
      const cals = ChartUtils.getTotalCaloriesArray(sortedChartData, daysInMonth)[0];

      for (let i = 0; i < cals.length; i++) {
        food[i] = cals[i].y
      }
      const weeklyYAxisValues = ChartUtils.getWeeklyBarYAxisValues(food);
      console.log('weekly value: ', weeklyYAxisValues);
      console.log('cals: ', cals)
      console.log('weekly food: ', food)
      const highest = this.getHighestValue(weeklyYAxisValues);
      weeklyTimeIntervals = ChartUtils.getWeeklyBarYAxisValues(timeIntervals);


      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highest + plusHighest);

      const weekRange = this.getWeekRange(this.state.daysInMonth);
      const dayOfTheMonth = new Date().getDate() / 7;

      if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
        if (this.state.changeWeeklyLabel) {
          if (dayOfTheMonth <= 1) {
            this.setState({ dateSet: weekRange[1] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[0] < 2 ? weeklyYAxisValues[0] + ' ' + I18n.t('CAL') : weeklyYAxisValues[0] + ' ' + I18n.t('CALS') });
          } else if (dayOfTheMonth > 1 && dayOfTheMonth <= 2) {
            this.setState({ dateSet: weekRange[2] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[1] < 2 ? weeklyYAxisValues[1] + ' ' + I18n.t('CAL') : weeklyYAxisValues[1] + ' ' + I18n.t('CALS') });
          } else if (dayOfTheMonth > 2 && dayOfTheMonth <= 3) {
            this.setState({ dateSet: weekRange[3] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[2] < 2 ? weeklyYAxisValues[2] + ' ' + I18n.t('CAL') : weeklyYAxisValues[2] + ' ' + I18n.t('CALS') });
          } else if (dayOfTheMonth > 3 && dayOfTheMonth <= 4) {
            this.setState({ dateSet: weekRange[4] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[3] < 2 ? weeklyYAxisValues[3] + ' ' + I18n.t('CAL') : weeklyYAxisValues[3] + ' ' + I18n.t('CALS') });
          } else if (dayOfTheMonth > 4 && dayOfTheMonth <= 5) {
            this.setState({ dateSet: weekRange[5] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[4] < 2 ? weeklyYAxisValues[4] + ' ' + I18n.t('CAL') : weeklyYAxisValues[4] + ' ' + I18n.t('CALS') });
          }
        }
      } else {
        this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase(), initialDataCal: 0 + ' ' + I18n.t('CAL') });
      }
      for (let i = 0; i < weeklyYAxisValues.length; i++) {
        if (weeklyYAxisValues[i] > 0) {
          this.setState({ isNoDataAvailable: false });
          break;
        } else {
          this.setState({ isNoDataAvailable: true });
        }
      }

      // update x-axis coordinates.
      weeklyYAxisValues.splice(0, 0, 0)
      this.updateXAxisState(weekRange, 5);
      this.updateDataSets(weeklyYAxisValues, [0], 'transparent');

      // update zoom level
      this.setZoomLevel(-5, 0, 1);
    }, 50);
  }

  onPressMonthly() {
    this.setState({
      selectedInterval: I18n.t('MONTHLY'), isRightArrowVisible: true, isLeftArrowVisible: true,
      targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0, isTargetDisplayed: 'none',
      isCalsTextVisible: 0, isSetTargetVisible: 'flex', isSetTargetDisplayed: 1, isCalsDisplayed: 'flex',
      isTargetTextVisible: 1, display: 'none', opacity: 0, marker: defaultMarker, dragEnabled: true
    });

    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getFullYear() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
    }

    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });
      const sortedChartData = this.getSortedMonthlyData();

      // Note: sortedChartData is an array of the different arrays of chart data for activity-sleep analysis,
      // in the following order:
      // sortedChartData = [sortedStepsChartData, sortedHeartRateChartData, sortedKcalChartData, sortedTimeChartData];
      const sortedStepsData = sortedChartData[0];
      const monthlyCount = ChartUtils.getMonthlyBarYAxisValues(sortedStepsData);
      let monthlySleepData = [0];

      for (let i = 0; i < monthlyCount.length; i++) {
        monthlySleepData[i] = monthlyCount[i] > 700 ? monthlyCount[i] + 100 : monthlyCount[i] - 100;
      }

      monthlySteps = monthlyCount;
      monthlyTimeIntervals = ChartUtils.getMonthlyBarYAxisValues(sortedChartData[1]);

      const highest = this.getHighestValue(monthlyCount);
      monthlyCount.splice(0, 0, 0);
      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highest + plusHighest);
      // update x-axis coordinates.
      this.updateXAxisState(getMonthNames(), 13);
      this.updateDataSets(monthlyCount, [0], 'transparent');



      if (monthlyCount[parseInt(new Date().getMonth()) + 1] !== 0 && typeof monthlyCount[parseInt(new Date().getMonth()) + 1] !== 'undefined') {
        this.setState({ initialDataCal: monthlyCount[parseInt(new Date().getMonth()) + 1] < 2 ? monthlyCount[parseInt(new Date().getMonth()) + 1] + ' ' + I18n.t('CAL') : monthlyCount[parseInt(new Date().getMonth()) + 1] + ' ' + I18n.t('CALS') });
      }
      else {
        this.setState({ initialDataCal: 0 + ' ' + I18n.t('CAL') })
      }

      for (let i = 0; i < monthlyCount.length; i++) {
        if (monthlyCount[i] > 0) {
          this.setState({ isNoDataAvailable: false });
          break;
        } else {
          this.setState({ isNoDataAvailable: true });
        }
      }



      let latestMonth = 0;
      for (let i = monthlyCount.length; i > 0; i--) {
        if (monthlyCount[i] > 0) {
          latestMonth = i;
          break;
        }
      }

      // this means the chart in view right now is on the daily interval
      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5) {
        // update zoom level
        this.setZoomLevel(2, 0, latestMonth);
      } else {
        this.setZoomLevel(this.state.zoom.scaleX, 0, latestMonth);
      }

    }, 50);
  }

  onPressYearly() {
    this.setState({
      selectedInterval: I18n.t('YEARLY'), dateSet: new Date().getFullYear(), isTrophyVisible: false,
      isRightArrowVisible: false, isLeftArrowVisible: false, targetAddSubtractVisible: 'none',
      targetAddSubtractDisplayed: 0, isTargetDisplayed: 'none', isCalsTextVisible: 0, isSetTargetVisible: 'flex',
      isSetTargetDisplayed: 1, isCalsDisplayed: 'flex', isTargetTextVisible: 1, display: 'none', opacity: 0,
      marker: defaultMarker
    });
    setTimeout(() => {
      const allChartData = [];
      const allYearTime = [];

      for (let i = 0; i < this.state.rows.length; i++) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.cals
        };
        allChartData[i] = rec;


        const timeRec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.time
        };

        allYearTime[i] = timeRec;
      }

      const yearlyCount = ChartUtils.getYearlyBarYAxisValues(allChartData);
      yearlySteps = yearlyCount;
      yearlyTimeIntervals = ChartUtils.getYearlyBarYAxisValues(allYearTime);

      let counts = [];
      let years = [];
      let yearlySleepData = [];

      for (let i = 0; i < yearlyCount.length; i++) {
        counts[i] = yearlyCount[i].count;
        years[i] = (yearlyCount[i].year) + '';
        yearlySleepData[i] = yearlyCount[i].count > 2000 ? 2000 : yearlyCount[i].count - 200;
      }

      counts.splice(0, 0, 0);

      years.splice(0, 0, '');

      const highest = this.getHighestValue(counts);

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highest + 100);

      // we're gonna manipulate the years array to make the x-coordinates appear longer in the chart 
      // thereby making the bar chart's width thinner. We need to check whether the years are not 0 so that
      // we won't be change the values of the years' elements.
      let xValues = years;

      if (years.length < 12) {
        // 12 here is an arbitrary value
        for (let i = 0; i < 12; i++) {
          if (typeof years[i] === 'undefined') {
            xValues[i] = '';
          }
        }
      }


      for (let i = 0; i < xValues.length; i++) {
        if (xValues[i].toString() === new Date().getFullYear().toString()) {

          if (counts[i] !== 0 && typeof counts[i] !== 'undefined') {
            this.setState({ initialDataCal: counts[i] < 2 ? counts[i] + ' ' + I18n.t('CAL') : counts[i] + ' ' + I18n.t('CALS') });
          }
          else {
            this.setState({ initialDataCal: 0 + ' ' + I18n.t('CAL') });
          }
        }

      }

      console.log('YEARS:', xValues);
      console.log('COUNTS:', counts);

      // update x-axis coordinates.
      this.updateXAxisState(xValues, xValues.length + 1);
      this.updateDataSets(counts, [0], 'transparent');
      let latestYear = 0;
      for (let i = counts.length; i > 0; i--) {
        if (counts[i] !== '') {
          latestYear = i;
          break;
        }
      }

      // this means the chart in view right now is on the daily interval

      if (typeof this.state.zoom.scaleX === 'undefined' || this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5 || this.state.zoom.scaleX === 2) {
        // update zoom level
        this.setZoomLevel(2, 0, 0);
      }


      for (let i = 0; i < counts.length; i++) {
        if (counts[i] > 0) {
          this.setState({ isNoDataAvailable: false });
          break;
        } else {
          this.setState({ isNoDataAvailable: true });
        }
      }


      if (latestYear < 7) {
        this.setState({ dragEnabled: false });
      }
      else {
        this.setState({ dragEnabled: true });
      }

    }, 50);
  }

  onRefresh = () => {
    this.setState({ isRefreshing: true });
    console.log('123targetcals1 doc: ' + JSON.stringify(daily_food_value));
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      this.getDatabase();
      for (let i = 0; i < daily_food_value.length; i++) {
        if (daily_food_value.length !== 0) {
          if (typeof (daily_food_value[i]) === 'undefined') {
            continue;
          }

          let tCals = this.props.targetCals;

          if (this.props.targetCals === 0 && daily_food_value[i].target_cals > 0) {
            tCals = daily_food_value[i].target_cals;
          }

          const doc = {
            _id: daily_food_value[i]._id,
            _rev: revs.pop(),
            user_id: daily_food_value[i].user_id,
            date: daily_food_value[i].date,
            food_name: daily_food_value[i].food_name,
            cals: daily_food_value[i].cals_count,
            time: daily_food_value[i].time,
            type: 'food',
            target_cals: tCals,
            channels: [daily_food_value[i].user_id],
            timestamp: getFormattedTimestamp(),
          };

          console.log('123targetcals1 doc: ' + JSON.stringify(doc));

          manager.document.put({ db: DB_NAME, doc: doc._id, body: doc, rev: doc._rev }).then(res => {
            console.log('123targetcals1: put res: ', res);
            revs.push(res.obj.rev);
            console.log('successfully updated!');
          }).catch(err => {
            console.log('123targetcals1 ERRRROOOORRRR:', err);
          });
        }
      }
    }
    setTimeout(() => {
      this.setState({ isRefreshing: false });
    }, 2000);
  }

  nextDateStyle() {
    return {
      justifyContent: 'flex-end',
      fontSize: 26,
      color: this.state.nextDateTextColor,
      width: 25
    };
  }

  previousDateStyle() {
    return {
      fontSize: 26,
      justifyContent: 'flex-start',
      color: this.state.previousDateTextColor,
      width: 25
    };
  }

  setDailyTarget() {
    ChartDuration.setDailyTargets();
    this.setState({ chosenYear: new Date().getFullYear(), chosenMonth: new Date().getMonth(), dateSet: new Date().getFullYear() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() });
    setTimeout(() => {
      this.onPressDaily();
    }, 5);
  }

  render() {
    return (
      <View style={widgets.mainBrainwaveView}>
        <HeaderMain style={{ flex: 1 }} hideTopHeader='true' />
        <View style={{ flex: 10 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh}
                tintColor='white'
                title={I18n.t('LOADING')}
                titleColor="white"
                colors={['rgb(233, 186, 0)']}
                progressBackgroundColor='white' />
            }>
            <View style={containers.activityChartAndInfoCon}>
              <View style={{ marginTop: 5, marginBottom: 5, flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={text.textTitle}>{I18n.t('FOOD_RECORD')}</Text>
              </View>

              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                zIndex: 20,
                height: 30
              }}>
                {
                  this.state.isDateVisible && this.state.isLeftArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                    <Text style={this.previousDateStyle()} onPress={() => this.setToPreviousInterval()}>{'<'}</Text>
                  </View>
                }
                {
                  this.state.isDateVisible &&
                  <Text style={[widgets.commonFont, { textAlign: 'center', flex: 1 }]}>{this.state.dateSet}</Text>
                }
                {
                  this.state.isDateVisible && this.state.isRightArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                    <Text style={this.nextDateStyle()} onPress={() => this.setToNextInterval()}>{'>'}</Text>
                  </View>
                }
              </View>

              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={[widgets.hoursStepsText, { textAlign: 'left' }]}> {I18n.t('CAL')} </Text>
              </View>
              <CombinedChart
                style={widgets.activityBarChart}
                data={this.state.data}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                animation={{
                  durationX: 0
                }}
                touchEnabled={true}
                legend={this.state.legend}
                marker={this.state.marker}
                highlights={this.state.highlights}
                gridBackgroundColor={processColor('#ffffff')}
                drawBarShadow={false}
                drawValueAboveBar={true}
                autoScaleMinMaxEnabled={true}
                doubleTapToZoomEnabled={false}
                scaleEnabled={false}
                zoom={this.state.zoom}
                chartDescription={{ text: '' }}
                chartDescriptionFontSize={18}
                dragEnabled={this.state.dragEnabled}
                drawBorders={false}
                config={this.state.config}
                drawHighlightArrow={false}
                onSelect={this.handleSelect.bind(this)} />

              {
                this.state.isNoDataAvailable &&
                <Text style={widgets.textNoData}>{I18n.t('NO_DATA_YET')}</Text>
              }

              <SetTargetModal placeholder={I18n.t('SET_TARGET')} buttonTitle={I18n.t('SET_TARGET')} maxLength={5} inputChange={txt => this.onChangeTarget(txt)} closeModal={() => {
                changedCals = -1;
                this.setState({ modalVisible: false });
              }} isModalVisible={this.state.modalVisible} onSetTarget={() => {
                if (changedCals !== -1) {
                  this.setState({ targetText: changedTarget, lineColor: changedColor, targetSteps: changedCals });
                  this.props.updateSettingsModule({ prop: 'targetCals', value: changedCals });
                  this.updateDataSets(this.state.data.barData.dataSets[0].values,
                  this.updateLineChartValues(changedCals), changedColor);
                  if (changedCals > foodHighest) {
                    this.updateYAxisState(changedCals + plusHighest);
                    if (changedCals > 0) {
                      this.setState({ isSetTargetVisible: 'none', isSetTargetDisplayed: 0, targetAddSubtractVisible: 'flex', targetAddSubtractDisplayed: 1 }); 
                    }
                  } else if (changedCals === 0) {
                    this.setState({
                      isSetTargetVisible: 'flex', isSetTargetDisplayed: 1,
                      targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0
                    });
                    this.updateYAxisState(foodHighest + plusHighest);
                  } else {
                    this.updateYAxisState(foodHighest + plusHighest);
                    if (changedCals > 0) {
                      this.setState({ isSetTargetVisible: 'none', isSetTargetDisplayed: 0, targetAddSubtractVisible: 'flex', targetAddSubtractDisplayed: 1 }); 
                    }
                  }
                }
                this.setState({ modalVisible: false });
              }} />


              <View style={widgets.lowerSectionBrainwave}>
                <View style={{ marginTop: initialDataCalMargin }}>
                  <Image
                    style={{
                      display: height < 640 ? 'none' : 'flex',
                      width: 35,
                      height: 35,
                      marginVertical: 5,
                      resizeMode: 'contain',
                      alignSelf: 'center'
                    }}
                    source={require('../../images/manage_items_icons/food_128.png')}
                  />
                  <Text style={[text.textRecordCal, text.textHighlightRecordCal]}>{this.state.initialDataCal}</Text>
                </View>
              </View>
            </View>

            {RenderIf(this.state.display === 'flex' && this.state.opacity === 1)(
              <Card containerStyle={widgets.recordsCardPadding}>
                <TouchableOpacity onPress={() => { Actions.addFood() }}>
                  <View style={widgets.flexRow}>
                    <Image
                      style={{
                        width: 25,
                        height: 25
                      }}
                      source={require('../../images/manage_items_icons/food.png')}
                    />
                    <Text style={text.textRecordCal}> {I18n.t('RECORD')}</Text>
                  </View>
                </TouchableOpacity>
              </Card>
            )}


            {RenderIf(this.state.isTargetDisplayed === 'flex' && this.state.isCalsTextVisible === 1)(
              <Card containerStyle={widgets.recordsCardPadding}>
                <TouchableOpacity onPress={() => { this.goToAddFoodScreen() }}>
                  <View style={widgets.flexRow}>
                    <Image
                      style={{
                        width: 25,
                        height: 25
                      }}
                      source={require('../../images/manage_items_icons/food.png')}
                    />
                    <Text style={text.textRecordCal}>  {I18n.t('RECORD')}</Text>
                  </View>
                </TouchableOpacity>
              </Card>
            )}

            {RenderIf(this.state.isTargetDisplayed === 'flex' && this.state.isCalsTextVisible === 1)(
              <View>
                {RenderIf(this.state.isSetTargetVisible === 'flex' && this.state.isSetTargetDisplayed === 1)(
                  <Card containerStyle={widgets.recordsCardPadding}>
                    <TouchableOpacity onPress={() => { this.setState({modalVisible: true }) }}>
                      <View style={widgets.flexRow}>
                        <Image
                          style={{
                            width: 25,
                            height: 25
                          }}
                          source={require('../../images/target_set32.png')}
                        />
                        <Text style={text.targetVal}>  {I18n.t('SET_TARGET')}</Text>
                      </View>
                    </TouchableOpacity>
                  </Card>
                )}
                {RenderIf(this.state.targetAddSubtractVisible === 'flex' && this.state.targetAddSubtractDisplayed === 1)(
                  <Card containerStyle={widgets.recordsCardPadding}>
                    <View style={containers.foodTrophyContainer}>
                      <TouchableOpacity onPress={() => this.subtractCalories()}>
                        <Image style={{ height: 25, width: 25, marginLeft: 15 }} source={require('../../images/minus32.png')} />
                      </TouchableOpacity>
                      <Image style={{ height: 25, width: 25, marginLeft: 15 }} source={require('../../images/target_set32.png')} />
                      <TouchableOpacity onPress={() => { { this.setState({ modalVisible: true }) } }}>
                        <Text style={text.targetVal}>{this.props.targetCals} {this.props.targetCals <= 1 ? ' ' + I18n.t('CALORIE_DAY') : ' ' + I18n.t('CALORIES_DAY')} </Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => this.addCalories()}>
                        <Image style={{ height: 25, width: 25, marginRight: 15 }} source={require('../../images/plus32.png')} />
                      </TouchableOpacity>
                    </View>
                  </Card>
                )}
              </View>
            )}

            {RenderIf(this.state.isCalsDisplayed === 'flex' && this.state.isTargetTextVisible === 1)(
              <View style={widgets.commonPadding2}>
                <Card containerStyle={widgets.commonPadding}>
                  {/* <TouchableOpacity onPress={() => { this.onPressDaily() }}> */}
                  <TouchableOpacity onPress={() => { this.setDailyTarget() }}>
                    <View style={widgets.flexRow}>
                      <Text style={text.textRecordCal}>{I18n.t('SET_DAILY_TARGET')}</Text>
                    </View>
                  </TouchableOpacity>
                </Card>
              </View>
            )}

          </ScrollView>
        </View>

        <ChartDuration
          onPressDaily={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressDaily();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressWeekly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.setState({ changeWeeklyLabel: true });
              this.onPressWeekly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressMonthly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressMonthly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressYearly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressYearly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          style={{ flex: 1 }} />


        <Modal
          animationIn='zoomIn'
          animationOut='zoomOut'
          backdropColor='transparent'
          onBackButtonPress={() => {
            this.setState({ progressModal: false });
          }}
          isVisible={this.state.progressModal}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
          </View>
        </Modal>

      </View>
    );
  }

  renderDailyChart() {
    TARGET_FOOD = [];
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    const daysInMonth = this.getActualDaysInAMonth();
    const valueFormatter = [];
    const steps = [];
    let xHighlight = 0;
    let yHighlight = 0;
    this.setState({ daysInMonth: daysInMonth, marker: defaultMarker });

    const sortedChartData = this.filterByChosenMonthAndYear();
    console.log('sortedChartData:', sortedChartData);
    let dailySleepData = [0];
    let shouldShowTargetLine = false;
    let greaterThanTarget = false;
    daily_food_value = [];
    for (let i = 0; i < sortedChartData.length; i++) {
      if (typeof sortedChartData[i] === 'undefined') {
        continue;
      }
      const current_day = parseInt(sortedChartData[i].date.slice(-2));
      if (current_day === new Date().getDate()) {
        daily_food_value[i] = sortedChartData[i];
      }
    }

    for (let i = daily_food_value.length; i >= 0; i--) {
      if (typeof daily_food_value[i] === 'undefined') {
        continue;
      }
      revs.push(daily_food_value[i]._rev);
    }

    for (let i = 0; i <= daysInMonth; i++) {
      valueFormatter[i] = (i + 1) + '';
      for (let j = 0; j < sortedChartData.length; j++) {
        if (typeof sortedChartData[j] !== 'undefined') {
          const day = parseInt(sortedChartData[j].date.slice(-2));

          if ((day) === i) {
            TARGET_FOOD[i] = parseInt(sortedChartData[j].target_cals)
            steps[i] = parseInt(sortedChartData[j].cals_count);
            timeIntervals[i] = parseInt(sortedChartData[j].time);

            if (day === new Date().getDate()) {
              console.log('123targetcals1: ' + parseInt(sortedChartData[j].target_cals));
              this.props.updateSettingsModule({ prop: 'targetCals', value: parseInt(sortedChartData[j].target_cals) });

              if (parseInt(sortedChartData[j].target_cals) > 0) {
                this.setState({
                  isSetTargetVisible: 'none', isSetTargetDisplayed: 0,
                  targetAddSubtractVisible: 'flex', targetAddSubtractDisplayed: 1
                });
              }
            }
          } else {
            if (typeof steps[i] === 'undefined') {
              steps[i] = 0;
            }
            if (typeof TARGET_FOOD[i] === 'undefined' || isNaN(TARGET_FOOD[i]) || typeof TARGET_FOOD[i] === null) {
              TARGET_FOOD[i] = 0;
            }
            if (typeof timeIntervals[i] === 'undefined') {
              timeIntervals[i] = 0;
            }
          }
        }
      }
    }
    const cals = ChartUtils.getTotalCaloriesArray(sortedChartData, daysInMonth)[0];
    xHighlight = parseInt(ChartUtils.getTotalCaloriesArray(sortedChartData, daysInMonth)[1]);
    yHighlight = ChartUtils.getTotalCaloriesArray(sortedChartData, daysInMonth)[2];
    activity = steps;

    // this adds a "0" element at the beginning of the valueFormatter array.
    valueFormatter.splice(0, 0, '0');

    if (valueFormatter.length > daysInMonth + 1) {
      // this is a workaround to ensure that the last day on the daily chart is clickable.
      valueFormatter.splice(valueFormatter.length - 1, 1);
    }

    this.setState({ dailyActivities: steps });

    if (typeof cals[parseInt(new Date().getDate())] === 'undefined') {
      this.setState({ initialDataCal: 0 + ' ' + I18n.t('CAL') });
    }
    else if (cals[parseInt(new Date().getDate())].y > 1) {
      this.setState({ initialDataCal: cals[parseInt(new Date().getDate())].y + ' ' + I18n.t('CALS') });
    }
    else {
      this.setState({ initialDataCal: cals[parseInt(new Date().getDate())].y + ' ' + I18n.t('CAL') });
    }


    foodHighest = this.getHighestValue(cals);
    const highest = this.getHighestValue(cals);
    // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
    // it won't reach the ceiling of the graph component.
    console.log('HIGH HIGH :', highest);

    if (this.props.targetCals > highest) {
      this.updateYAxisState(this.props.targetCals + plusHighest);
    }
    else {
      this.updateYAxisState(highest + plusHighest);
    }

    this.updateXAxisState(valueFormatter, daysInMonth);

    if (steps.length === 0 || (steps.length === 1 && steps[0] === 0)) {
      this.setState({ isNoDataAvailable: true });
    } else {
      this.setState({ isNoDataAvailable: false });
    }


    const lineData = [0];
    for (let i = 0; i <= 32; i++) {
      lineData[i] = this.props.targetCals;
    }

    const lineDataZero = [0];
    for (let i = 0; i <= 32; i++) {
      lineDataZero[i] = 0;
    }


    if (this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
      this.updateDataSets(cals, xHighlight === new Date().getDate() ? this.updateLineChartValues(yHighlight) : lineData,
        xHighlight > 0 && xHighlight === new Date().getDate() ? TARGET_LINE_COLOR : this.props.targetCals > 0 ? TARGET_LINE_COLOR : this.state.lineColor);
    }
    else {
      this.updateDataSets(cals, xHighlight === new Date().getDate() ? this.updateLineChartValues(yHighlight) : lineDataZero,
        xHighlight > 0 ? 'transparent' : 'transparent');

      this.setState({
        targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0, isTargetDisplayed: 'none',
        isCalsTextVisible: 0, isSetTargetVisible: 'flex', isSetTargetDisplayed: 0, display: 'flex', opacity: 1
      });

    }

    let latestDay = 0;
    for (let i = activity.length; i > 0; i--) {
      if (activity[i] > 0) {
        latestDay = i;
        break;
      }
    }
    this.scaleChartForDailyInterval(latestDay);

    if (xHighlight > 0 && xHighlight === new Date().getDate()) {
      this.setState({ highlights: [{ x: xHighlight, y: yHighlight, dataIndex: 1 }] });
    }
    console.log('DAYS IN MONTH', daysInMonth)
    console.log('CALORIES: ', cals);
    console.log('TARGET FOOD: ', TARGET_FOOD);
  }

  setToPreviousInterval() {
    this.setState({ nextDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) - 1;
    let year = parseInt(this.state.chosenYear);

    if (month < 0) {
      if (year <= this.state.earliestYear) {
        this.setState({ previousDateTextColor: 'transparent' });
        return;
      }
      month = 11;
      year -= 1;
    }

    this.setState({ chosenMonth: month, chosenYear: year });

    // The reason we're setting this variable is because we need to ensure
    // that when the user presses the previous month, we need to maintain the
    // date text to show the month. Right now it's showing the weekly range.
    if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      this.setState({ changeWeeklyLabel: false });
    } else {
      this.setState({ changeWeeklyLabel: true });
    }
    // note the reason we are adding 1 here is because monthNames[] start with index 0
    // whereas in our document's date field, the month starts with 1 and ends with 12.
    // this.sortDatasetByMonths(month + 1);
    this.sortDatasetByIntervals();
    this.setState({
      dateSet: year + '\n' + getMonthComplete()[month].toUpperCase(),
      targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0, isTargetDisplayed: 'none',
      isCalsTextVisible: 0, isSetTargetVisible: 'none', isSetTargetDisplayed: 0,
    });
  }

  setToNextInterval() {
    this.setState({ previousDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) + 1;
    let year = parseInt(this.state.chosenYear) + 1;

    if (month > new Date().getMonth()) {
      if (year > new Date().getFullYear()) {
        // this is not allowed, we should hide the right arrow key
        this.setState({ nextDateTextColor: 'transparent' });
        return;
      } else {

        if (month > 11) {
          // we reset month to 0
          month = 0;
          this.setState({ chosenYear: year, chosenMonth: month, dateSet: year + '\n' + getMonthComplete()[month].toUpperCase() });
        } else {
          this.setState({ chosenYear: (year - 1), chosenMonth: month, dateSet: (year - 1) + '\n' + getMonthComplete()[month].toUpperCase() });
        }

        setTimeout(() => {
          this.sortDatasetByIntervals();
        }, 5);
        return;
      }
    } else {
      this.setState({ chosenMonth: month });
      // The reason we're setting this variable is because we need to ensure
      // that when the user presses the previous month, we need to maintain the
      // date text to show the month. Right now it's showing the weekly range.
      if (this.state.selectedInterval === I18n.t('WEEKLY')) {
        this.setState({ changeWeeklyLabel: false });
      } else {
        this.setState({ changeWeeklyLabel: true });
      }

      // note the reason we are adding 1 here is because monthNames[] start with index 0
      // whereas in our document's date field, the month starts with 1 and ends with 12.
      // this.sortDatasetByMonths(month + 1);
      // this.sortDatasetByIntervals();
      setTimeout(() => {
        this.sortDatasetByIntervals();
      }, 5);
    }

    this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[month].toUpperCase() });
  }



  setXValuesFormatter(month) {
    const year = this.state.chosenYear;
    const daysInMonth = this.getDaysInMonth(month + 1, year);
    const valueFormatter = [];

    if (this.state.selectedInterval === I18n.t('DAILY')) {

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
      }

      // this adds a "0" element at the beginning of the valueFormatter array.
      valueFormatter.splice(0, 0, '0');
      this.updateXAxisState(valueFormatter, 31);
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // update x-axis coordinates.
      this.updateXAxisState(this.getWeekRange(daysInMonth), 6);
    }
  }

  sortDatasetByIntervals() {
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      // we render the chart with the daily interval, thereby calling onPressDaily()
      this.onPressDaily();
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // we render the chart with the weekly interval, thereby calling onPressWeekly()
      this.onPressWeekly();
    } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {
      // we render the chart with the monthly interval, thereby calling onPressMonthly()
      this.onPressMonthly();
    } else if (this.state.selectedInterval === I18n.t('YEARLY')) {
      // we render the chart with the yearly interval, thereby calling onPressYearly()
      this.onPressYearly();
    }
  }

  setZoomLevel(zoomLevelX, zoomLevelY, moveToX) {
    this.setState({
      isMonthlyView: false,
      zoom: {
        xValue: moveToX,
        yValue: 0,
        scaleX: zoomLevelX,
        scaleY: zoomLevelY
      }
    });
  }

  sortDatasetByMonths(month) {
    let sortedDocs = [];

    for (let i = 0; i < this.state.rows.length; i++) {
      const document = this.state.rows[i].doc;
      const hyphenIndex = document.date.indexOf('-');
      const extractedMonth = parseInt(document.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedMonth === month) {
        sortedDocs[i] = document;
      }
    }

    return sortedDocs;
  }


  subtractCalories() {
    let cals = this.props.targetCals;
    let calsLabel = '';

    if (cals > 1) {
      cals -= 1;
    }
    else {
      this.setState({
        isSetTargetVisible: 'flex', isSetTargetDisplayed: 1,
        targetAddSubtractVisible: 'none', targetAddSubtractDisplayed: 0
      });
    }

    calsLabel = cals <= 1 ? ' ' + I18n.t('CALORIE_DAY') : ' ' + I18n.t('CALORIES_DAY');

    const target = cals + calsLabel;
    const color = cals === 0 ? 'transparent' : 'rgb(233, 186, 0)';
    this.setState({ targetCals: target, lineColor: color });
    this.props.updateSettingsModule({ prop: 'targetCals', value: cals });

    if (cals > foodHighest) {
      this.updateYAxisState(cals + plusHighest);
    }
    else {
      this.updateYAxisState(foodHighest + plusHighest);
    }

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.updateLineChartValues(cals), color);
  }

  updateDataSets(activityCount, lineChartValues, lineColor) {
    console.log('UPDATE DATA SETS: ' + JSON.stringify(activityCount) + '/' + JSON.stringify(lineChartValues) + '/' + lineColor)
    this.setState({
      data: {
        barData: {
          dataSets: [
            {
              values: activityCount,
              label: I18n.t('FOOD'),
              colors: [
                processColor('#77b1c2')
              ],
              drawFilled: true,
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                textSize: 15,
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }],
        },
        lineData: {
          dataSets: [{
            values: lineChartValues,
            label: I18n.t('TARGET'),
            config: {
              colors: [processColor(lineColor)],
              // drawValues: true,
              drawValues: false,
              valueTextSize: 18,
              valueFormatter: '#',
              valueTextColor: processColor('grey'),
              mode: 'CUBIC_BEZIER',
              drawCircles: false,
              lineWidth: 2,
              drawFilled: false,
              dashedLine: {
                lineLength: 40,
                spaceLength: 20
              }
            }
          }]
        }
      }
    });
  }

  scaleChartForDailyInterval(latestDay) {
    // if chart is in monthly view, then we need to re-adjust the scale to suit the daily duration view
    this.setZoomLevel(4, 0, latestDay);
  }


  updateXAxisState(valueFormatter, axisMaximum) {
    console.log('123redux! valueFormatter: ' + JSON.stringify(valueFormatter));
    this.setState({
      xAxis: {
        valueFormatter: valueFormatter,
        granularityEnabled: true,
        granularity: 1,
        drawGridLines: false,
        axisMinimum: 0,
        axisMaximum: axisMaximum + 1,
        textSize: 12,
        position: 'BOTTOM'
      }
    });
  }

  updateYAxisState(maxPoint) {
    this.setState({
      yAxis: {
        left: {
          drawGridLines: true,
          gridLineWidth: 1,
          // labelCount: maxPoint,
          // labelCountForce: false,
          axisMaximum: maxPoint,
          axisMinimum: 0,
          textSize: 14
        },
        right: {
          enabled: false
        }
      }
    });
  }
}

export default connect(mapStateToProps, { updateSettingsModule })(FoodRecord);
