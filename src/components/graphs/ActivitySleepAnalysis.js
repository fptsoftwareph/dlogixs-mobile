import React, { Component } from 'react';
import HeaderMain from '../common/HeaderMain';
import * as DateUtil from '../../utils/DateUtils';
import { SMAModule } from 'nb_native_modules';
import getCurrentDate from '../../utils/DateUtils';
import I18n from '../../translate/i18n/i18n';
import { TARGET_LINE_COLOR } from '../../styles/colors';
import Toast from 'react-native-simple-toast';
import { getPreference, savePreference, showToast } from '../../utils/CommonMethods';
import Modal from 'react-native-modal';
import { COLOR_THUMB_TINT } from '../../styles/colors';
import * as Progress from 'react-native-progress';
import TargetDoc from '../common/TargetDoc';
import CurationActualData from '../common/CurationActualData';

import {
  View,
  ScrollView,
  processColor,
  Text,
  Image,
  Alert,
  AppState,
  Platform,
  RefreshControl,
  PermissionsAndroid
} from 'react-native';

import { widgets } from '../../styles/widgets';
import { containers } from '../../styles/containers';
import { text } from '../../styles/text';
import { CombinedChart } from 'react-native-charts-wrapper';
import { ChartUtils } from './../../utils/ChartUtils';
import ChartDuration from '../common/ChartDuration';
import Target from '../common/Target';
import SetTargetModal from '../common/SetTargetModal';
import { updateSettingsModule } from '../../redux/actions';
import { connect } from 'react-redux';
import normalize from '../../utils/GetPixelSizeForLayoutSize';

async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'Title': I18n.t('LOCATION_PERMISSION'),
        'Message': I18n.t('NEUROBEAT_LOCATION_ACCESS')
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('123perm You can use the gps.');
      getCoordinates();
    } else {
      console.log('123perm GPS permission denied.');
    }
  } catch (err) {
    console.warn('123perm: ' + err);
  }
}

const LINE_CHART_VALUES = [
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0
];

let changedColor = 'transparent';
let changedTarget = 0;
let changedSteps = 0;
let dailyHighestStep = 0;

const COUCHBASE_CRED = require('../../config/demo_couchbase_cred.json');
const BAR_DATA_GAP = 0.2;
let activity = [0];
let targetActivitySteps = [0];
let heartRates = [0];
let timeIntervals = [0];
let kcals = [0];
let enableTarget = true;

let user_id = '';
let shouldUpdateDateSmartBandConnected = false;

let weeklySteps = [0];
let weeklyHeartRates = [0];
let weeklyTimeIntervals = [0];
let weeklyKcals = [0];
let weeklyDividendsForBPM = [0];

let monthlySteps = [0];
let monthlyHeartRates = [0];
let monthlyTimeIntervals = [0];
let monthlyKcals = [0];

let yearlySteps = [0];
let yearlyHeartRates = [0];
let yearlyTimeIntervals = [0];
let yearlyKcals = [0];
var activityTimer;
let ACTIVITY_DATA = [];
let initConnectTime = '';
let sortedRowsByCurrentMonthAndYear = [];
let isTargetButtonClicked = false;
let isGPSAlertShown = false;
let latitude = 0;
let longitude = 0;


const uuidv4 = require('uuid/v4');
const ACTIVITY = I18n.t('ACTIVITY');
const SLEEP = I18n.t('SLEEP');

const SLEEP_DATA = [0];
let justRefreshed = false;
const configWithGroupSpace = {
  barWidth: 0.4,
  group: {
    fromX: 0.5,
    groupSpace: BAR_DATA_GAP,
    barSpace: 0
  }
};

const defaultMarker = {
  enabled: true,
  markerColor: processColor('transparent'),
  textColor: processColor('#000000'),
  markerFontSize: 8
};

const invisibleMarker = {
  enabled: false
};

const configNoGroupSpace = {
  barWidth: 0.4
};

const DAYS_OF_THE_MONTH = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
  '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

const BURNED_CALORIES = I18n.t('BURNED_CALORIES');
const ACTIVITY_TIME = I18n.t('TIME');

const { extractMinsFromTimeStr, extractHoursFromTimeStr, computeTotalMins, getMonthNames, getMonthComplete } = DateUtil;

const mapStateToProps = (state) => {
  const { dateAndDocumentMap, targetSteps, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins, latestSteps, latestCal, latestActivityTime,
    latestBPM } = state.settingsModule;
  console.log('123watch-- dateAndDocumentMap ' + JSON.stringify(dateAndDocumentMap));

  const { onGenderChange, onWeightChange } = state.userProfileModule;

  // This is how we put a value to initConnectTime. initConnectTime should be initialized if its current value is empty
  // and if the dateSmartBandConnected is not empty. this variable will be used to compare the current time to the
  // first time the smartband has been connected to the app.
  if (typeof dateAndDocumentMap !== 'undefined' && !isSmartBandEnabled) {
    initConnectTime = dateAndDocumentMap.time;
    console.log('123redux! state changed: connected!');
  }

  console.log('123redux! isSmartBandEnabled: ' + isSmartBandEnabled);

  console.log('123redux** deductedMinsProps: ' + deductedMins + '\tminsInADay: ' + minsInADay);

  return {
    dateAndDocumentMap, targetSteps, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins, latestSteps, latestCal, latestActivityTime,
    latestBPM, onWeightChange, onGenderChange
  };
};

function getCoordinates() {
  console.log('123perm getCoordinates()');
  navigator.geolocation.getCurrentPosition(
    position => {
      console.log('123perm position: ' + JSON.stringify(position));
      latitude = position.coords.latitude;
      longitude = position.coords.longitude;
    },
    error => {
      console.log('123perm error: ' + JSON.stringify(error));
      if (error.code === 1 && !isGPSAlertShown) {
        isGPSAlertShown = true;
        Alert.alert(I18n.t('ENABLE_LOCATION'), I18n.t('PLEASE_ENABLE_LOCATION'));
      }
    },
    { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
  );
}

class ActivitySleepAnalysis extends Component {

  addSteps() {
    let steps = typeof this.props.targetSteps === 'undefined' ? 0 : this.props.targetSteps;
    let stepsLabel = '';

    if (steps !== 99999) {
    steps += 1;
    stepsLabel = steps <= 1 ? ' ' + I18n.t('STEP') : ' ' + I18n.t('STEPS');

    const target = steps + stepsLabel;
    const color = steps === 0 ? 'transparent' : TARGET_LINE_COLOR;
    this.setState({ targetText: target, lineColor: color });

    this.props.updateSettingsModule({ prop: 'targetSteps', value: steps });

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values, this.updateLineChartValues(steps), color,
      this.state.data.barData.config);

    if (steps > this.state.yAxis.left.axisMaximum) {
      this.updateYAxisState(steps + (Platform.OS === 'android' ? 100 : 150), this.state.yAxis.right.axisMaximum);
    }
    
    }
  }

  componentDidMount() {
    showToast(I18n.t('SWIPE_DOWN_REFRESH'));
    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });
    this.getLocationCoordinates();

    getPreference('shouldUpdateDateSmartBandConnected').then(res => {
      shouldUpdateDateSmartBandConnected = JSON.parse(res);
    });

    const stepsLabel = this.props.targetSteps <= 1 ? I18n.t('STEP') : I18n.t('STEPS');
    this.setState({
      targetText: (typeof this.props.targetSteps === 'undefined' || this.props.targetSteps < 1) ?
        I18n.t('SET_TARGET') : this.props.targetSteps + ' ' + stepsLabel
    });

    console.log('321redux: isSmartBandEnabled: ' + this.props.isSmartBandEnabled + '\tconnectedPeripheralId: ' + this.props.connectedPeripheralId);
    if (this.props.connectedPeripheralId === '') {
      console.log('321redux: alert device not connected to smartwatch.');
      Alert.alert('', I18n.t('CONNECT_WRISTBAND_TO_START'));
    } else {
      console.log('321redux: alert device connected to smartwatch.');
    }

    console.log('123redux: dateAndDocumentMap: ' + JSON.stringify(this.props.dateAndDocumentMap));

    this.getDatabase();

    getPreference('userId').then(res => {
      console.log('123redux: getPreference userId: ' + res);
      user_id = res;
    });

  }

  getActualDaysInAMonth() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    // to get the actual days in the month, we need to also check if the graph is showing the current/previous month.
    const monthToUse = this.state.chosenMonth === month ? month + 1 : this.state.chosenMonth + 1;
    const yearToUse = this.state.chosenYear === year ? year : this.state.chosenYear;
    return this.getDaysInMonth(monthToUse, yearToUse);
  }

  componentWillUnmount() {
    clearInterval(this.activityTimer);
  }

  computeMonthlyAverageHeartRate(heartRate) {
    let dividend = 0;
    let h = this.filterHeartRatesByMonthAndYear(this.getSortedMonthlyData()[1]);

    console.log('yAxisVals: heartRates by month and year: ' + JSON.stringify(h));

    for (let i = 0; i < h.length; i++) {
      if (typeof h[i] !== 'undefined' && h[i].count > 0) {
        dividend++;
      }
    }

    console.log('yAxisVals: dividend: ' + dividend + '\theartRate: ' + JSON.stringify(heartRate));

    if (dividend > 0) {
      heartRate /= dividend;
      heartRate = Math.round(heartRate);
    }

    return heartRate;
  }

  computeTotalTimeAndAverageHeartRate(oldDoc, newDoc) {
    const aveHeartRate = (oldDoc.heart_rate + newDoc.heart_rate) / 2;
    newDoc.heart_rate = Math.round(aveHeartRate);
    return newDoc;
  }

  computeWeeklyAverageHeartRate(index, heartRate) {
    const dividend = weeklyDividendsForBPM[index];

    // compute the average for weekly bpm
    if (dividend > 0) {
      heartRate /= dividend;
      heartRate = Math.round(heartRate);
    }
    return heartRate;
  }

  computeYearlyAverageHeartRate(heartRate) {
    let dividend = 0;
    const h = this.filterHeartRatesByYear(this.getSortedMonthlyData()[1]);
    console.log('yAxisVals: heartRates by month and year: ' + JSON.stringify(h));

    for (let i = 0; i < h.length; i++) {
      if (typeof h[i] !== 'undefined') {
        if (h[i].count > 0) {
          dividend++;
        }
      }
    }

    if (dividend > 0) {
      heartRate /= dividend;
      heartRate = Math.round(heartRate);
    }
    return heartRate;
  }

  constructor() {
    super();

    this.state = {
      sleepRows: {},
      actualWalk: 0,
      progressModal: false,
      highlights: [{ x: -1, y: 0, stackIndex: 27, dataIndex: 0 }],
      dragEnabled: true,
      changeWeeklyLabel: false,
      // we set the default earliest year to the current year.
      earliestYear: new Date().getFullYear(),
      nextDateTextColor: 'rgb(127, 127, 127)',
      previousDateTextColor: 'rgb(127, 127, 127)',
      lineColor: 'transparent',
      isNoDataAvailable: false,
      chosenYear: 0,
      chosenMonth: 0,
      selectedInterval: '',
      daysInMonth: 0,
      dailyActivities: [],
      rows: {},
      lastDocumentStored: {},
      targetText: I18n.t('SET_TARGET'),
      modalVisible: false,
      targetSteps: 0,
      isTrophyVisible: true,
      isRefreshing: false,
      activityData: {},
      scanning: false,
      appState: '',
      heartRate: 0,
      isDateVisible: true,
      isRightArrowVisible: true,
      isLeftArrowVisible: true,
      heartRateText: 'Heart rate at 2:22 PM',
      stepsPerTime: '30 mins',
      numSteps: '0 ' + I18n.t('STEP'),
      calBurnt: '0 ' + I18n.t('KCAL'),
      activityTime: '0 ' + I18n.t('MIN'),
      activityBPM: '0 ' + I18n.t('BPM'),
      leftArrow: '<',
      rightArrow: '>',
      marker: defaultMarker,
      dateSet: this.getCurrentDate(),
      legend: {
        enabled: true,
        textSize: 0,
        textColor: processColor('transparent'),
        form: 'SQUARE',
        formSize: 0,
        xEntrySpace: 0,
        yEntrySpace: 0,
        formToTextSpace: 0,
        wordWrapEnabled: true,
        maxSizePercent: 0
      },
      data: {
        barData: {
          dataSets: [
            {
              values: ACTIVITY_DATA,
              label: I18n.t('ACTIVITY'),
              colors: [
                processColor('#77b1c2'), processColor('#d3bdeb')
              ],
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                textSize: normalize(14),
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }, {
              // 1hr, 2hrs, 3hrs, 4hrs, 8hrs, 6hrs
              values: SLEEP_DATA,
              label: I18n.t('SLEEP'),
              config: {
                textSize: normalize(14),
                drawValues: false,
                colors: [processColor('#d3bdeb')]
              }
            }
          ],
          config: configWithGroupSpace
        },
        lineData: {
          dataSets: [
            {
              label: '',
              values: LINE_CHART_VALUES,
              config: {
                colors: [processColor('grey')],
                drawValues: false,
                valueTextSize: 18,
                valueFormatter: '#',
                mode: 'CUBIC_BEZIER',
                drawCircles: false,
                lineWidth: 2,
                drawFilled: false,
                group: {
                  fromX: 0,
                  groupSpace: 0,
                  barSpace: 15
                },
                dashedLine: {
                  lineLength: 20,
                  spaceLength: 20
                }
              }
            }
          ]
        }
      },
      yAxis: {
        left: {
          drawGridLines: false,
          textSize: normalize(14),
          axisMinimum: 0,
          axisMaximum: 840
          /*
          **  note: to avoid the y-coordinate from changing its left side grid values whenever scrolling the graph to the right (and vice-versa)
          **  and having this weird "wobbling" effect, always set an axisMaximum. The
          **  axisMaximum limits the highest value to be displayed on the left side of the
          **  y-axis of the graph.

          **  What I usually use as the value for the axisMaximum is
          **  the highest value in the Activity array and add it up with a hundred The 100
          **  I add is just for aesthetics, so the highest value for Activity wouldn't
          **  reach the ceiling of the graph. i.e. [100, 800, 950, 400, 370] // the
          **  axisMaximum would be 950 + 100 = 1050.
          */
        },
        right: {
          enabled: true,
          drawGridLines: false
        }
      },
      xAxis: {
        valueFormatter: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
        granularityEnabled: true,
        textSize: normalize(15),
        granularity: 1,
        axisMaximum: 31,
        avoidFirstLastClipping: true,
        axisMinimum: 0,
        position: 'BOTTOM'
      },
      zoom: {
        xValue: 0,
        yValue: 0,
        scaleX: 4,
        scaleY: 0
      }
    };
  }

  filterHeartRatesByYear(dataset) {
    const heartRatesArr = [];
    for (let i = 0; i < dataset.length; i++) {
      if (typeof dataset[i] !== 'undefined') {
        const extractedYear = parseInt(dataset[i].date.substring(0, 4));
        if (extractedYear === this.state.chosenYear) {
          heartRatesArr[i] = dataset[i];
        }
      }
    }

    // we remove all undefined elements in heartRatesArr
    for (let i = 0; i < heartRatesArr.length; i++) {
      if (typeof heartRatesArr[i] === 'undefined') {
        heartRatesArr.splice(i, 1);
      }
    }

    return heartRatesArr;
  }

  filterHeartRatesByMonthAndYear(dataset) {
    const heartRatesArr = [];
    for (let i = 0; i < dataset.length; i++) {
      if (typeof dataset[i] !== 'undefined') {
        const extractedYear = parseInt(dataset[i].date.substring(0, 4));
        const hyphenIndex = dataset[i].date.indexOf('-');
        const extractedMonth = parseInt(dataset[i].date.substring(hyphenIndex + 1).substring(0, 2));

        if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
          heartRatesArr[i] = dataset[i];
        }
      }
    }

    // we remove all undefined elements in heartRatesArr
    for (let i = heartRatesArr.length; i > 0; i--) {
      if (typeof heartRatesArr[i] === 'undefined') {
        heartRatesArr.splice(i, 1);
        console.log('yAxisVals: heartRatesArr one undefined element');
      }
    }

    console.log('yAxisVals: heartRatesArr' + JSON.stringify(heartRatesArr));

    return heartRatesArr;
  }

  filterByChosenMonthAndYear() {
    // we need to sort the daily chart by the (chosen or current) month and year.
    // Note: chosenMonth and chosenYear are both initialized to the current month and current year in
    // componentDidUpdate().
    const sortedChartData = [];
    let earliestYear = this.state.earliestYear;
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.

      if (typeof this.state.rows[i] === 'undefined' || this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.rows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.rows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      console.log('123redux!: extractedYear: ' + extractedYear + '\textractedMonth: ' + extractedMonth);

      // Note we add 1 to chosenMonth here because chosenMonth is also used in monthNames array, and monthNames array starts with index 0, hence chosenMonth is by
      // default should start with 0 (for January) and end with 11 (for December).

      console.log('123redux!: this.state.chosenYear: ' + this.state.chosenYear + '\this.state.chosenMonth: ' + (this.state.chosenMonth + 1));
      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          steps_count: this.state.rows[i].doc.steps,
          time: this.state.rows[i].doc.time,
          kcal: this.state.rows[i].doc.kcal,
          heart_rate: this.state.rows[i].doc.heart_rate,
          user_id: this.state.rows[i].doc.user_id,
          target_steps: this.state.rows[i].doc.target_steps,
          _rev: this.state.rows[i].doc._rev
        };

        sortedChartData[i] = rec;
      }
    }

    const sleepList = [];

    for (let i = 0; i < this.state.sleepRows.length; i++) {
      if (typeof this.state.sleepRows[i].doc.date === 'undefined' || this.state.sleepRows[i].doc.date === '') {
        continue;
      }

      const extractedYear = parseInt(this.state.sleepRows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.sleepRows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.sleepRows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const sleepObj = {
          sleep_count: this.state.sleepRows[i].doc.sleep_analysis_total === undefined ? 0 : this.state.sleepRows[i].doc.sleep_analysis_total,
          date: this.state.sleepRows[i].doc.date
        };

        sleepList.push(sleepObj);
      }
    }

    this.setState({ earliestYear: earliestYear });

    // we remove all undefined elements in allChartData
    for (let i = sortedChartData.length; i > 0; i--) {
      if (typeof sortedChartData[i] === 'undefined') {
        console.log('123redux! sortedChartData is null or undefined!');
        sortedChartData.splice(i, 1);
      }
    }

    console.log('123redux!: sortedChartData: ' + JSON.stringify(sortedChartData));
    return [sortedChartData, sleepList];
  }

  getCurrentDate() {
    const strDate = new Date();
    console.log('MONTHS**', 'months: ' + getMonthComplete());
    return strDate.getDate() + '\n' + getMonthComplete()[strDate.getMonth()].toUpperCase() + ', ' + strDate.getFullYear();
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  getDatabase() {
    manager.database.get_db({ db: DB_NAME })
      .then(res => {
        console.log('couchbase12: res: ' + JSON.stringify(res));
        this.querySleepDocuments();
        this.queryActivityDocuments();
      });
  }

  getHighestValue(chartValues) {
    let highest = 0;

    if (typeof chartValues[0] !== 'undefined' && typeof chartValues[0].y !== 'undefined') {
      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i].y > highest) {
          highest = chartValues[i].y;
        }
      }
    } else {
      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i] > highest) {
          highest = chartValues[i];
        }
      }
    }

    return highest;
  }

  getLocationCoordinates() {
    if (Platform.OS === 'android') {
      if (Platform.Version >= 23) {
        const locationCheck = PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (locationCheck === PermissionsAndroid.RESULTS.GRANTED) {
          getCoordinates();
        } else {
          requestLocationPermission();
        }
      } else {
        getCoordinates();
      }
    } else {
      getCoordinates();
    }
  }

  getSortedMonthlyData() {
    const sortedStepsChartData = [];
    const sortedHeartRateChartData = [];
    const sortedKcalChartData = [];
    const sortedTimeChartData = [];
    const sortedSleepChartData = [];
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (typeof this.state.rows[i] === 'undefined' || this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      if (this.state.chosenYear === extractedYear) {
        const recStep = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.steps
        };

        const recHeartRate = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.heart_rate
        };

        const recTime = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.time
        };

        const recKcal = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.kcal
        };

        sortedStepsChartData[i] = recStep;
        sortedHeartRateChartData[i] = recHeartRate;
        sortedKcalChartData[i] = recKcal;
        sortedTimeChartData[i] = recTime;
      }
    }

    // we remove all undefined elements in allChartData
    for (let i = 0; i < sortedStepsChartData.length; i++) {
      if (typeof sortedStepsChartData[i] === 'undefined') {
        sortedStepsChartData.splice(i, 1);
      }
      if (typeof sortedHeartRateChartData[i] === 'undefined') {
        sortedHeartRateChartData.splice(i, 1);
      }
      if (typeof sortedKcalChartData[i] === 'undefined') {
        sortedKcalChartData.splice(i, 1);
      }
      if (typeof sortedTimeChartData[i] === 'undefined') {
        sortedTimeChartData.splice(i, 1);
      }
    }

    for (let i = 0; i < this.state.sleepRows.length; i++) {
      // we skip docs with empty dates.
      if (typeof this.state.sleepRows[i] === 'undefined' || this.state.sleepRows[i].doc.date === '' || typeof this.state.sleepRows[i].doc.date === 'undefined') {
        continue;
      }

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.sleepRows[i].doc.date.substring(0, 4));
      if (this.state.chosenYear === extractedYear) {
        const recSleep = {
          _id: this.state.sleepRows[i].doc._id,
          date: this.state.sleepRows[i].doc.date,
          count: this.state.sleepRows[i].doc.sleep_analysis_total
        };

        sortedSleepChartData.push(recSleep);
      }
    }

    // this is an array of all the sorted data for activity-sleep analysis.
    const sortedChartData = [sortedStepsChartData, sortedHeartRateChartData, sortedKcalChartData, sortedTimeChartData, sortedSleepChartData];

    return sortedChartData;
  }

  getWeekRange(limit) {
    var weeks = [];
    let arrCtr = 0;
    let lastRange = 0;

    for (let i = 0; i < limit; i++) {
      if (i === 0) {
        weeks[arrCtr] = '0';
        arrCtr++;
        continue;
      }

      if (i % 7 == 0) {
        weeks[arrCtr] = (i - 6) + '-' + i;
        arrCtr++;
        lastRange = i;
      }
    }

    weeks.push((limit - lastRange) > 1 ? (lastRange + 1) + "-" + limit : (lastRange + 1)) + '';
    return weeks;
  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
    }

    this.setState({ appState: nextAppState });
  }

  handleSelect(event) {
    const entry = event.nativeEvent;
    if (entry === null) {
      this.setState({
        ...this.state,
        selectedEntry: null
      });
    } else {
      this.setState({
        ...this.state,
        selectedEntry: JSON.stringify(entry)
      });

      if (typeof entry.x !== 'undefined' && typeof entry.y !== 'undefined') {
        const xVal = Math.round(entry.x);

        if (this.state.selectedInterval === I18n.t('DAILY')) {

          if (xVal === new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
            this.setState({ isTrophyVisible: true });
          } else {
            this.setState({ isTrophyVisible: false });
          }

          if (typeof activity[xVal] !== 'undefined') {
            // we show the day and month here
            let step = activity[xVal].y;
            let heartRate = heartRates[xVal];
            let time = timeIntervals[xVal];

            let cal = kcals[xVal];
            console.log('123xhighlight kcals: ' + JSON.stringify(kcals) + '\tcal: ' + cal);
            const hourMinsArr = DateUtil.formatTime(time);
            const hour = hourMinsArr[0];
            const mins = hourMinsArr[1];

            if (typeof step === 'undefined') {
              step = 0;
            }

            if (typeof cal === 'undefined') {
              cal = 0;
            }

            if (typeof heartRate === 'undefined') {
              heartRate = 0;
            }

            const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : (mins > 1 ? mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MIN'));
            let dateToShow = xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear;

            if (xVal === 0 || xVal > this.state.daysInMonth) {
              dateToShow = this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase();
            }

            console.log('123xhighlight: dateToShow: ' + dateToShow);

            this.setState({
              dateSet: dateToShow, numSteps: step + ' ' + (step <= 1 ? I18n.t('STEP') : I18n.t('STEPS')),
              calBurnt: cal + ' ' + I18n.t('KCAL'), activityTime: recordedTime, activityBPM: heartRate + ' ' + I18n.t('BPM')
            });
            console.log('targetActivitySteps:', targetActivitySteps);

            if (typeof targetActivitySteps[xVal] === 'undefined') {
              return;
            }

            console.log('123redux** xVal: ' + xVal + '\tnewDate().getDate(): ' + new Date().getDate() + '\tchosenMonth: ' + this.state.chosenMonth +
              '\tchosenYear: ' + this.state.chosenYear + '\tmonth: ' + new Date().getMonth() + '\tyear: ' + new Date().getFullYear);

            let tStep = typeof targetActivitySteps[xVal] === 'undefined' || isNaN(targetActivitySteps[xVal]) ? 0 : targetActivitySteps[xVal];

            if (tStep === 0 && this.props.targetSteps > 0 && (xVal >= new Date().getDate() &&
              this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear())) {
              tStep = this.props.targetSteps;
              console.log('123xhighlight: equal!!!!');
            }

            const color = tStep === 0 ? 'transparent' : TARGET_LINE_COLOR;

            console.log('123xhighlight: ' + this.updateLineChartValues(tStep) + '\tcolor: ' + color + '\ttStep: ' +
              tStep + '\tstep: ' + step + '\targetActivitySteps: ' + JSON.stringify(targetActivitySteps) + '\ttarget step: ' + this.props.targetSteps
              + '\tchosenMonth: ' + this.state.chosenMonth + '\tchosenYear: ' + this.state.chosenYear + '\txVal: ' + xVal);

            setTimeout(() => {
              this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
                this.updateLineChartValues(tStep), color, this.state.data.barData.config);
            }, 5);
          } else {
            if (xVal === 0 || xVal > this.state.daysInMonth) {
              this.setState({
                dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase()
              });
            } else {
              this.setState({
                dateSet: xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear
              });
            }

            this.setState({
              numSteps: 0 + ' ' + I18n.t('STEP'),
              calBurnt: 0 + ' ' + I18n.t('KCAL'), activityTime: 0 + ' ' + I18n.t('MINS'), activityBPM: 0 + ' ' + I18n.t('BPM')
            });
          }
        } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
          console.log('yAxisVals: xVal: ' + xVal + '\tactivity length: ' + activity.length);
          let step = weeklySteps[xVal - 1];
          let heartRate = weeklyHeartRates[xVal - 1];
          const time = weeklyTimeIntervals[xVal - 1];

          let kcal = weeklyKcals[xVal - 1];

          const hourMinsArr = DateUtil.formatTime(time);
          const hour = hourMinsArr[0];
          const mins = hourMinsArr[1];

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }

          heartRate = this.computeWeeklyAverageHeartRate(xVal - 1, heartRate);
          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : (mins > 1 ? mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MIN'));

          if (typeof this.state.xAxis.valueFormatter[xVal] === 'undefined' || xVal === 0) {
            this.setState({
              dateSet: getMonthComplete()[this.state.chosenMonth].toUpperCase()
              + ', ' + this.state.chosenYear, numSteps: step + ' ' + (step <= 1 ? I18n.t('STEP') : I18n.t('STEPS')),
              calBurnt: kcal + ' ' + I18n.t('KCAL'), activityTime: recordedTime, activityBPM: heartRate + ' ' + I18n.t('BPM')
            });
          }
          else {
            this.setState({
              dateSet: this.state.xAxis.valueFormatter[xVal] + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase()
              + ', ' + this.state.chosenYear, numSteps: step + ' ' + (step <= 1 ? I18n.t('STEP') : I18n.t('STEPS')),
              calBurnt: kcal + ' ' + I18n.t('KCAL'), activityTime: recordedTime, activityBPM: heartRate + ' ' + I18n.t('BPM')
            });
          }


        } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {

          let step = monthlySteps[xVal - 1];
          let heartRate = monthlyHeartRates[xVal - 1];

          let kcal = isNaN(monthlyKcals[xVal - 1]) ? 0 : monthlyKcals[xVal - 1];
          let time = monthlyTimeIntervals[xVal - 1];

          const hourMinsArr = DateUtil.formatTime(time);
          const hour = hourMinsArr[0];
          const mins = hourMinsArr[1];

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }

          heartRate = this.computeMonthlyAverageHeartRate(heartRate);
          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : (mins > 1 ? mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MIN'));

          this.setState({
            dateSet: this.state.chosenYear + '\n'
            + (typeof getMonthComplete()[xVal - 1] === 'undefined' ? (I18n.t('JANUARY').toUpperCase()) : getMonthComplete()[xVal - 1].toUpperCase()),
            numSteps: step + ' ' + (step <= 1 ? I18n.t('STEP') : I18n.t('STEPS')),
            calBurnt: kcal + ' ' + I18n.t('KCAL'), activityTime: recordedTime, activityBPM: heartRate + ' ' + I18n.t('BPM')
          });
        } else {

          // Note: there's a bug with the graph wherein if you click a bar item, sometimes it returns the zero index,
          // even though it's not the right x-index. We're catching it here for the yearly interval. I haven't observe
          // it occuring in the other intervals, prolly because the way the yearly graph is rendered.
          if (xVal === 0 || typeof yearlySteps[xVal - 1] === 'undefined') {
            return;
          }

          // Note: yearlySteps is structured this way: [{year: 0, count: 100}] - only a sample structure
          // the reason this is structured this way is because of the ChartUtils.getYearlyBarYAxisValues()
          // I've made it reusable for the other developers to use for their records tasks.
          let step = yearlySteps[xVal - 1].count;

          let heartRate = yearlyHeartRates[xVal - 1].count;

          let kcal = isNaN(yearlyKcals[xVal - 1].count) ? 0 : yearlyKcals[xVal - 1].count;
          let time = yearlyTimeIntervals[xVal - 1].count;

          console.log('yearlyView12: yearlySteps: ' + yearlySteps[xVal - 1] + '\tyearlyHeartRates: ' + yearlyHeartRates + '\tyearlyKcals: ' + yearlyKcals);

          const hourMinsArr = DateUtil.formatTime(time);
          const hour = hourMinsArr[0];
          const mins = hourMinsArr[1];

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }

          heartRate = this.computeYearlyAverageHeartRate(heartRate);

          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MIN') : mins + ' ' + I18n.t('MINS');

          this.setState({
            dateSet: this.state.xAxis.valueFormatter[xVal], numSteps: step + ' ' + I18n.t('STEPS').toLowerCase(),
            calBurnt: kcal + ' ' + I18n.t('KCAL'), activityTime: recordedTime, activityBPM: heartRate + ' ' + I18n.t('BPM')
          });
        }
      }
    }
  }

  manageHeartRateData(rate) {
    console.log('dafsasfafa: ', rate);
    this.setState({ heartRate: rate });
  }

  onChangeTarget(txt) {
    if (!isNaN(txt)) {
      let steps = 0;
      const tmpStep = isNaN(parseInt(txt, 10)) ? Math.round(txt) : parseInt(Math.round(txt), 10);

      if (tmpStep > 0) {
        steps = tmpStep;
      }
      if (steps === 0 && this.props.targetSteps > 0) {
        steps = this.props.targetSteps;
      }
      
      const tmpChangedSteps = isNaN(parseInt(txt, 10)) ? Math.round(txt) : parseInt(Math.round(txt), 10);
      changedSteps = tmpChangedSteps > 0 ? tmpChangedSteps : 0;

      let stepsLabel = '';
      stepsLabel = steps <= 1 ? ' ' + I18n.t('STEP') : ' ' + I18n.t('STEPS');
      
      const target = steps + stepsLabel;
      changedColor = steps === 0 ? 'transparent' : TARGET_LINE_COLOR;
      changedTarget = steps < 1 ? I18n.t('SET_TARGET') : target;
    }
  }

  updateLineChartValues(steps) {
    const lineChartValues = [];

    for (var i = 0; i < 35; i++) {
      lineChartValues[i] = steps;
    }

    return lineChartValues;
  }

  onSetTarget() {
    this.setState({ modalVisible: false });
  }

  onPressDaily() {
    this.setState({ selectedInterval: I18n.t('DAILY'), dragEnabled: true });
    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getDate() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), isTrophyVisible: true });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase(), isTrophyVisible: false });
    }

    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({
        isDateVisible: true, isRightArrowVisible: true,
        isLeftArrowVisible: true
      });
      this.renderDailyChart();
    }, 50);
  }

  onPressWeekly() {
    this.setState({
      selectedInterval: I18n.t('WEEKLY'), isRightArrowVisible: true, isLeftArrowVisible: true, dragEnabled: false,
      marker: defaultMarker
    });
    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });

      const daysInMonth = this.getActualDaysInAMonth();
      const valueFormatter = [];
      this.setState({ daysInMonth: daysInMonth });
      const activity = [0];
      const sleepData = [0];
      heartRates = [0];
      timeIntervals = [0];
      kcals = [0];

      const sortedChartData = this.filterByChosenMonthAndYear()[0];
      console.log('123actsleep!: 1sortedChartData: ' + JSON.stringify(sortedChartData));

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
        for (let j = 0; j < sortedChartData.length; j++) {
          if (typeof sortedChartData[j] !== 'undefined') {
            const day = parseInt(sortedChartData[j].date.slice(-2));
            if ((day - 1) === i) {
              activity[i] = parseInt(sortedChartData[j].steps_count);
              sleepData[i] = 0;
              heartRates[i] = parseInt(sortedChartData[j].heart_rate);
              timeIntervals[i] = parseInt(sortedChartData[j].time);
              kcals[i] = typeof sortedChartData[j].kcal === 'undefined' ? 0 : parseInt(sortedChartData[j].kcal);
            } else if (typeof activity[i] === 'undefined') {
              activity[i] = 0;
              sleepData[i] = 0;
              heartRates[i] = 0;
              timeIntervals[i] = 0;
              kcals[i] = 0;
            }
          }
        }
      }

      // hotfix: if there's no activity data, then we still need to compose the dailySleepData array
      if (sortedChartData.length === 0) {
        for (let i = 0; i <= daysInMonth; i++) {
          sleepData[i] = 0;
        }
      }

      const sleepChartData = this.filterByChosenMonthAndYear()[1];

      for (let i = 0; i < sleepChartData.length; i++) {
        for (let j = 0; j < sleepData.length; j++) {
          const sleepDate = parseInt(sleepChartData[i].date.slice(-2));
          if (j === sleepDate - 1) {
            sleepData[j] = sleepChartData[i].sleep_count / 60;
          }
        }
      }

      // todo: Implement a weekly graph view.
      activity.splice(0, 0, 0);
      heartRates.splice(0, 0, 0);
      sleepData.splice(0, 0, 0);
      timeIntervals.splice(0, 0, 0);
      kcals.splice(0, 0, 0);

      const weeklyYAxisValues = ChartUtils.getWeeklyBarYAxisValues(activity);
      console.log('weeklyYAxisValues:', activity);
      weeklySteps = weeklyYAxisValues;
      weeklyHeartRates = ChartUtils.getWeeklyBarYAxisValues(heartRates);

      const weeklySleepData = ChartUtils.getWeeklyBarYAxisValues(sleepData);
      weeklyDividendsForBPM = ChartUtils.getWeeklyDividends(heartRates);

      const weeklySleepArrObj = [];
      for (let i = 0; i < weeklyYAxisValues.length; i++) {
        weeklySleepArrObj.push({ x: i, y: weeklySleepData[i], marker: weeklySleepData[i].toFixed(2) });
      }

      const highestStep = this.getHighestValue(weeklyYAxisValues);
      const highestSleep = this.getHighestValue(weeklySleepData);

      console.log('123redux weeklySleepData: ' + JSON.stringify(weeklySleepData));

      console.log('yAxisVals: weekly dividends: ' + JSON.stringify(weeklyDividendsForBPM));

      weeklyTimeIntervals = ChartUtils.getWeeklyBarYAxisValues(timeIntervals);
      weeklyKcals = ChartUtils.getWeeklyBarYAxisValues(kcals);

      console.log('yAxisValues: weekly hr: ' + JSON.stringify(weeklyHeartRates));
      console.log('yAxisValues: weekly time intervals: ' + JSON.stringify(weeklyTimeIntervals));
      console.log('yAxisValues: weekly kcals: ' + JSON.stringify(weeklyKcals));

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highestStep + (Platform.OS === 'android' ? 100 : 150), highestSleep + 5);

      console.log('yAxisVals: ' + JSON.stringify(weeklyYAxisValues));
      console.log('yAxisVals: heartRates: ' + JSON.stringify(weeklyHeartRates));
      console.log('yAxisVals: timeIntervals: ' + JSON.stringify(weeklyTimeIntervals));
      console.log('yAxisVals: kcals: ' + JSON.stringify(weeklyKcals));

      const weekRange = this.getWeekRange(this.state.daysInMonth);
      const dayOfTheMonth = new Date().getDate() / 7;
      let indexToUse = 0;

      if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
        if (this.state.changeWeeklyLabel) {
          if (dayOfTheMonth <= 1) {
            this.setState({ dateSet: weekRange[1] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 0;
          } else if (dayOfTheMonth > 1 && dayOfTheMonth <= 2) {
            this.setState({ dateSet: weekRange[2] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 1;
          } else if (dayOfTheMonth > 2 && dayOfTheMonth <= 3) {
            this.setState({ dateSet: weekRange[3] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 2;
          } else if (dayOfTheMonth > 3 && dayOfTheMonth <= 4) {
            this.setState({ dateSet: weekRange[4] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 3;
          } else if (dayOfTheMonth > 4 && dayOfTheMonth <= 5) {
            this.setState({ dateSet: weekRange[5] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 4;
          }
        }
      } else {
        this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
      }

      if (highestStep > 0 || highestSleep > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      console.log('12debug!: weekRange: ' + weekRange);
      // update x-axis coordinates.
      this.updateXAxisState(weekRange, 5);

      this.updateDataSets(weeklyYAxisValues, weeklySleepArrObj, [0], 'transparent', configWithGroupSpace);

      if (weeklyYAxisValues.length > 0 && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
        const heartRate = this.computeWeeklyAverageHeartRate(indexToUse, weeklyHeartRates[indexToUse]);
        const hourMinsArr = DateUtil.formatTime(weeklyTimeIntervals[indexToUse]);

        const hour = hourMinsArr[0];
        const mins = hourMinsArr[1];
        const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : (mins > 1 ? mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MIN'));

        // set default values for calories, steps, time, and bpm
        this.setActivityMetrics(weeklyYAxisValues[indexToUse], weeklyKcals[indexToUse], recordedTime, heartRate);
      }

      // update zoom level
      this.setZoomLevel(-5, 0, 1);
    }, 50);
  }

  onPressMonthly() {
    this.setState({
      selectedInterval: I18n.t('MONTHLY'), isRightArrowVisible: true, isLeftArrowVisible: true,
      dragEnabled: true, marker: defaultMarker
    });

    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getFullYear() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
    }

    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });
      const sortedChartData = this.getSortedMonthlyData();
      // Note: sortedChartData is an array of the different arrays of chart data for activity-sleep analysis,
      // in the following order:
      // sortedChartData = [sortedStepsChartData, sortedHeartRateChartData, sortedKcalChartData, sortedTimeChartData];
      const sortedStepsData = sortedChartData[0];

      console.log('12redux: sortedStepsData: ' + JSON.stringify(sortedStepsData));

      const monthlyCount = ChartUtils.getMonthlyBarYAxisValues(sortedStepsData);
      const monthlySleep = ChartUtils.getMonthlyBarYAxisValues(sortedChartData[4]);

      monthlySteps = monthlyCount;
      monthlyHeartRates = ChartUtils.getMonthlyBarYAxisValues(sortedChartData[1]);

      monthlyKcals = ChartUtils.getMonthlyBarYAxisValues(sortedChartData[2]);
      monthlyTimeIntervals = ChartUtils.getMonthlyBarYAxisValues(sortedChartData[3]);

      const monthlySleepArrObj = [];
      for (let i = 0; i < monthlySleep.length; i++) {
        monthlySleep[i] = monthlySleep[i] / 60;
        monthlySleepArrObj.push({ x: i, y: monthlySleep[i], marker: monthlySleep[i].toFixed(2) });
      }

      const highestStep = this.getHighestValue(monthlyCount);
      const highestSleep = this.getHighestValue(monthlySleep);

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highestStep + (Platform.OS === 'android' ? 100 : 150), highestSleep + 5);


      if (highestStep > 0 || highestSleep > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      // update x-axis coordinates.
      this.updateXAxisState(getMonthNames(), 13);
      console.log('yAxisVals: ' + JSON.stringify(monthlyCount));
      this.updateDataSets(monthlyCount, monthlySleepArrObj, [0], 'transparent', configWithGroupSpace);

      if (monthlyCount.length > 0) {
        const currentMonth = new Date().getMonth();
        const hourMinsArr = DateUtil.formatTime(monthlyTimeIntervals[currentMonth]);
        const hour = hourMinsArr[0];
        const mins = hourMinsArr[1];
        const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : (mins > 1 ? mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MIN'));
        // set default values for calories, steps, time, and bpm
        const heartRate = this.computeMonthlyAverageHeartRate(monthlyHeartRates[currentMonth]);
        this.setActivityMetrics(monthlyCount[currentMonth], monthlyKcals[currentMonth], recordedTime, heartRate);
      }

      let latestMonth = 0;
      for (let i = monthlyCount.length; i > 0; i--) {
        if (monthlyCount[i] > 0) {
          latestMonth = i;
          break;
        }
      }

      // this means the chart in view right now is on the daily interval
      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5) {
        // update zoom level
        this.setZoomLevel(2, 0, latestMonth);
      } else {
        this.setZoomLevel(this.state.zoom.scaleX, 0, latestMonth);
      }
    }, 50);
  }

  onPressYearly() {
    this.setState({
      selectedInterval: I18n.t('YEARLY'), isTrophyVisible: false, marker: defaultMarker,
      isRightArrowVisible: false, isLeftArrowVisible: false
    });

    if (this.state.chosenYear === new Date().getFullYear()) {
      this.setState({ dateSet: new Date().getFullYear() });
    } else {
      this.setState({ dateSet: this.state.chosenYear });
    }

    setTimeout(() => {
      const allChartData = [];
      const allYearHeartRate = [];
      const allYearKcal = [];
      const allYearTime = [];
      const allYearSleep = [];

      for (let i = 0; i < this.state.rows.length; i++) {

        if (typeof this.state.rows[i] === 'undefined' || this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
          continue;
        }

        const rec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.steps
        };
        allChartData[i] = rec;

        const heartRateRec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.heart_rate
        };

        allYearHeartRate[i] = heartRateRec;

        const kcalRec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.kcal
        };

        allYearKcal[i] = kcalRec;

        const timeRec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.time
        };

        allYearTime[i] = timeRec;
      }

      for (let i = 0; i < this.state.sleepRows.length; i++) {
        if (typeof this.state.sleepRows[i] === 'undefined' || this.state.sleepRows[i].doc.date === '' || typeof this.state.sleepRows[i].doc.date === 'undefined') {
          continue;
        }

        const sleepRec = {
          _id: this.state.sleepRows[i].doc._id,
          date: this.state.sleepRows[i].doc.date,
          count: this.state.sleepRows[i].doc.sleep_analysis_total
        };

        allYearSleep.push(sleepRec);
      }

      const yearlyCount = ChartUtils.getYearlyBarYAxisValues(allChartData);
      yearlySteps = yearlyCount;
      yearlyHeartRates = ChartUtils.getYearlyBarYAxisValues(allYearHeartRate);
      yearlyKcals = ChartUtils.getYearlyBarYAxisValues(allYearKcal);
      console.log('yearlyKcals: ' + JSON.stringify(yearlyKcals));
      yearlyTimeIntervals = ChartUtils.getYearlyBarYAxisValues(allYearTime);
      console.log('yearlyTimeIntervals: ' + JSON.stringify(yearlyTimeIntervals));

      let counts = [];
      let years = [];
      let yearlySleepData = ChartUtils.getYearlyBarYAxisValues(allYearSleep);

      const yearlySleepArrObj = [];
      for (let i = 0; i < yearlySleepData.length; i++) {
        yearlySleepData[i].count = yearlySleepData[i].count / 60;
        yearlySleepArrObj.push({ x: i, y: yearlySleepData[i].count, marker: yearlySleepData[i].count.toFixed(2) });
      }

      for (let i = 0; i < yearlySleepData.length; i++) {
        years[i] = (yearlySleepData[i].year) + '';
      }

      for (let i = 0; i < yearlyCount.length; i++) {
        counts[i] = yearlyCount[i].count;
        years[i] = (yearlyCount[i].year) + '';
      }

      years.splice(0, 0, '');

      const highestStep = this.getHighestValue(counts);
      const highestSleep = this.getHighestValue(yearlySleepArrObj);

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highestStep + (Platform.OS === 'android' ? 100 : 150), highestSleep + 5);

      // we're gonna manipulate the years array to make the x-coordinates appear longer in the chart 
      // thereby making the bar chart's width thinner. We need to check whether the years are not 0 so that
      // we won't be change the values of the years' elements.
      let xValues = years;

      if (years.length < 12) {
        // 12 here is an arbitrary value
        for (let i = 0; i < 12; i++) {
          if (typeof years[i] === 'undefined') {
            xValues[i] = '';
          }
        }
      }

      if (highestStep > 0 || highestSleep > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      // update x-axis coordinates.
      this.updateXAxisState(xValues, xValues.length + 1);
      this.updateDataSets(counts, yearlySleepArrObj, [0], 'transparent', configWithGroupSpace);

      console.log('yearly years: ' + JSON.stringify(years) + '\tcounts: ' + JSON.stringify(counts) + '\tcount len: ' + counts.length);
      console.log('yearly yearlyHeartRate: ' + JSON.stringify(yearlyHeartRates));
      console.log('yearly yearlyTimeIntervals: ' + JSON.stringify(yearlyTimeIntervals));

      if (yearlyHeartRates.length > 0) {
        const index = counts.length - 1;
        const heartRate = this.computeYearlyAverageHeartRate(yearlyHeartRates[index].count);
        const hourMinsArr = DateUtil.formatTime(yearlyTimeIntervals[index].count);
        console.log('yearly yearlyTimeIntervals[index]: ' + JSON.stringify(yearlyTimeIntervals[index]));
        console.log('yearly hourMinsArr: ' + hourMinsArr);
        const hour = hourMinsArr[0];
        const mins = hourMinsArr[1];
        const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + (mins + ' ' + I18n.t('MINS')) : (mins > 1 ? mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MIN'));
        console.log('yearlyKcals val: ' + JSON.stringify(yearlyKcals[index].count));
        console.log('yearlyRecordedTime val: ' + recordedTime);
        this.setActivityMetrics(counts[index], yearlyKcals[index].count, recordedTime, heartRate);
      }

      let latestYear = 0;
      for (let i = counts.length; i > 0; i--) {
        if (counts[i] !== '') {
          latestYear = i;
          break;
        }
      }

      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5 || this.state.zoom.scaleX === 2) {
        // update zoom level
        this.setZoomLevel(2, 0, 0);
      }


      if (latestYear < 7) {
        this.setState({ dragEnabled: false });
      }
      else {
        this.setState({ dragEnabled: true });
      }

    }, 50);
  }

  onRefresh = () => {

    this.setState({ isRefreshing: true });

    console.log('123redux! dateSmartBandConnected: ' + this.props.dateSmartBandConnected);
    console.log('123redux! dateSmartBandDisconnected: ' + this.props.dateSmartBandDisconnected);
    console.log('123redux! connectedPeripheralId: ' + this.props.connectedPeripheralId);
    console.log('123redux**: onWeightChange: ' + this.props.onWeightChange);

    const gender = this.props.onGenderChange === I18n.t('MALE') ? 1 : 0;
    const currentDate = getCurrentDate();
    const dateNow = new Date(currentDate);

    console.log('123redux**: onGenderChange: ' + this.props.onGenderChange + '\tgender: ' + gender);

    if (this.state.selectedInterval === I18n.t('DAILY')) {
      if ((new Date(this.props.dateAndDocumentMap.date).getTime() < dateNow.getTime())) {
        // we clear initialConnectDate if the existing dateAndDocumentMap data is behind the current date by 1 day.
        console.log('123redux! clearing initConnectTime');
        initConnectTime = '';
      }

      if (this.props.isSmartBandEnabled && this.props.connectedPeripheralId !== '') {

        this.getLocationCoordinates();

        SMAModule.onReadSport(parseFloat(this.props.onWeightChange), gender, sportMap => { // The first two arguments is just an arbitrary value.
          // Note: we're adding this as a safety measure for when the smartphone can't retrieve the proper sportMap object
          // from the smartwatch.

          if (sportMap.date === '') {
            justRefreshed = true;
            this.setState({ isRefreshing: false });
            Toast.show(I18n.t('FAILED_TO_RETRIEVE_ACTIVITY_DATA'), Toast.SHORT);
            return;
          }

          SMAModule.onReadHeartRate((heartRateMap) => {
            // generate a random _id and user_id  for the document
            const _id = uuidv4();

            console.log('123redux** sportMap: ' + JSON.stringify(sportMap));
            console.log('123redux** heartRateMap: ' + JSON.stringify(heartRateMap));
            const time = sportMap.date.substring(sportMap.date.indexOf(' ')).trim();
            const date = sportMap.date.substring(0, sportMap.date.indexOf(' ')).trim();
            console.log('123redux** time: ' + time);

            const mins = extractMinsFromTimeStr(time);
            const hour = extractHoursFromTimeStr(time);
            console.log('123redux** mins' + mins + '\thours: ' + hour);
            const totalMins = computeTotalMins(hour, mins);
            console.log('123redux** mins: ' + mins + '\thour: ' + hour + '\ttotalMins: ' + totalMins);

            const formattedTimestamp = DateUtil.getFormattedTimestamp();

            const doc = {
              _id: _id, user_id: user_id, date: date, heart_rate: heartRateMap.heartRate, kcal: sportMap.calorie,
              steps: sportMap.step, time: time, type: 'activity-sleep', channels: [user_id], timestamp: formattedTimestamp,
              target_steps: this.props.targetSteps === undefined ? 0 : this.props.targetSteps, element_key: 'WALK',
              element: 'target', latitude: latitude, longitude: longitude
            };

            this.setState({ actualWalk: sportMap.step });

            const dateFromConnection = new Date(this.props.dateSmartBandConnected);
            const totalMinsFromConnection = computeTotalMins(dateFromConnection.getHours(), dateFromConnection.getMinutes());

            // By default we set deductedMins with the value of the totalMins - totalMinsFromConnection.
            // Should there be a value for recTime (from dataAndDocumentMap), then we're gonna override deductedMins' value
            // with the result of totalMins - totalRecTimeMins. totalMins is the variable that holds the value
            // of the current time in minutes, totalRecTimeMins is the variable that holds the value of the initial
            // recorded time (the first time the smart watch got connected in a day) in minutes.
            if (totalMins >= totalMinsFromConnection) {
              let deductedMins = totalMins - totalMinsFromConnection;

              console.log('123redux** totalMinsFromConnection: ' + totalMinsFromConnection + '\tdeductedMins: ' + deductedMins);

              doc.time = deductedMins;
              this.props.updateSettingsModule({ prop: 'deductedMins', value: deductedMins });

              console.log('123redux** deductedMinsProps: ' + this.props.deductedMins + '\tminsInADay: ' + this.props.minsInADay);

              this.storeActivityData(doc, totalMins);
              this.upsertTargetDocument();
            } else {
              this.setState({ isRefreshing: false });
            }
          }, error => {
            console.log('123redux** onReadHeartRate error: ' + error);
            justRefreshed = true;
            this.setState({ isRefreshing: false });
          });
        }, error => {
          console.log('123redux** onReadSporterror: ' + error);
          justRefreshed = true;
          this.setState({ isRefreshing: false });
        });
      } else {
        this.setState({ isRefreshing: false });
        justRefreshed = true;
      }
    } else {
      this.setState({ isRefreshing: false });
      justRefreshed = true;
    }
  }

  nextDateStyle() {
    return {
      justifyContent: 'flex-end',
      fontSize: 26,
      color: this.state.nextDateTextColor,
      width: 25
    };
  }

  previousDateStyle() {
    return {
      fontSize: 26,
      justifyContent: 'flex-start',
      color: this.state.previousDateTextColor,
      width: 25
    };
  }

  queryActivityDocuments() {
    this.setupViewAndQuery().then(r => {
      console.log('123redux: query time: ' + JSON.stringify(r.obj.rows));
      this.setState({ rows: r.obj.rows });

      setTimeout(() => {
        this.renderDailyChart();
        justRefreshed = true;
        this.setState({ isRefreshing: false });
      }, 5);
    });
  }

  querySleepDocuments() {
    this.setupSleepViewAndQuery().then(r => {
      this.setState({ sleepRows: r.obj.rows });
    });
  }

  renderDailyChart() {
    // to get the actual days in the month, we need to also check if the graph is showing the current/previous month.
    const daysInMonth = this.getActualDaysInAMonth();
    const valueFormatter = [];
    const steps = [];
    heartRates = [];
    timeIntervals = [];
    targetSteps = [];
    kcals = [];
    let xHighlight = 0;
    let yHighlight = 0;
    let targetSteps = 0;
    this.setState({ daysInMonth: daysInMonth, marker: defaultMarker });

    const sortedChartData = this.filterByChosenMonthAndYear()[0];
    sortedRowsByCurrentMonthAndYear = sortedChartData;

    console.log('123xhighlight: daysInMonth: ' + daysInMonth);
    let dailySleepData = [];

    let shouldShowTargetLine = false;
    let greaterThanTarget = false;

    for (let i = 0; i <= daysInMonth; i++) {
      valueFormatter[i] = (i + 1) + '';
      console.log('123datesets: outer loop: ' + i);
      for (let j = 0; j < sortedChartData.length; j++) {
        if (typeof sortedChartData[j] !== 'undefined') {
          console.log('123datesets: inner loop: ' + j);
          const day = parseInt(sortedChartData[j].date.slice(-2));

          if (day === i) {
            targetSteps = sortedChartData[j].target_steps;
            const stepsCount = sortedChartData[j].steps_count;

            shouldShowTargetLine = targetSteps === stepsCount && stepsCount > 0;

            greaterThanTarget = stepsCount > targetSteps && targetSteps > 0;

            steps[i] = {
              x: (i > 0 ? i - BAR_DATA_GAP : i), y: parseInt(stepsCount),
              marker: shouldShowTargetLine ? '\uD83C\uDFC6' : greaterThanTarget ? '\u2757' : ''
            };

            if (shouldShowTargetLine || greaterThanTarget) {
              xHighlight = day - BAR_DATA_GAP;
              yHighlight = steps[i].y;
            }

            targetActivitySteps[i] = parseInt(targetSteps);
            dailySleepData[i] = {
              x: (i > 0 ? i + BAR_DATA_GAP : i), y: 0, marker: ''
            };

            heartRates[i] = parseInt(sortedChartData[j].heart_rate);
            timeIntervals[i] = parseInt(sortedChartData[j].time);
            kcals[i] = typeof sortedChartData[j].kcal === 'undefined' ? 0 : parseInt(sortedChartData[j].kcal);

            if (day === new Date().getDate()) {
              console.log('123targetcals1: ' + sortedChartData[j].target_steps);
              this.props.updateSettingsModule({ prop: 'targetSteps', value: sortedChartData[j].target_steps });
              const stepsLabel = sortedChartData[j].target_steps <= 1 ? ' ' + I18n.t('STEP') : ' ' + I18n.t('STEPS');
              this.setState({ targetText: sortedChartData[j].target_steps + stepsLabel });
            }
          } else {
            if (typeof steps[i] === 'undefined') {
              steps[i] = { x: (i > 0 ? i - BAR_DATA_GAP : i), y: 0, marker: '' };
            }

            if (typeof targetActivitySteps[i] === 'undefined') {
              targetActivitySteps[i] = 0;
            }

            if (typeof dailySleepData[i] === 'undefined') {
              dailySleepData[i] = { x: (i > 0 ? i + BAR_DATA_GAP : i), y: 0, marker: '' };
            }

            if (typeof heartRates[i] === 'undefined') {
              heartRates[i] = 0;
            }

            if (typeof timeIntervals[i] === 'undefined') {
              timeIntervals[i] = 0;
            }

            if (typeof kcals[i] === 'undefined') {
              kcals[i] = 0;
            }
          }
        }
      }
    }

    // hotfix: if there's no activity data, then we still need to compose the dailySleepData array
    if (sortedChartData.length === 0) {
      for (let i = 0; i <= daysInMonth; i++) {
        dailySleepData[i] = {
          x: (i > 0 ? i + BAR_DATA_GAP : i), y: 0, marker: ''
        };
      }
    }

    console.log('targetSteps: ' + JSON.stringify(targetSteps));

    const sleepChartData = this.filterByChosenMonthAndYear()[1];

    for (let i = 0; i < sleepChartData.length; i++) {
      for (let j = 0; j < dailySleepData.length; j++) {
        const sleepDate = parseInt(sleepChartData[i].date.slice(-2)) + 0.2;
        if (dailySleepData[j].x === sleepDate) {
          dailySleepData[j].y = sleepChartData[i].sleep_count / 60;
          dailySleepData[j].marker = dailySleepData[j].y.toFixed(2);
        }
      }
    }

    activity = steps;

    // this adds a "0" element at the beginning of the valueFormatter array.
    valueFormatter.splice(0, 0, '0');
    console.log('123xhighlight valueFormatter: ' + valueFormatter.length);

    if (valueFormatter.length > daysInMonth + 1) {
      // this is a workaround to ensure that the last day on the daily chart is clickable.
      valueFormatter.splice(valueFormatter.length - 1, 1);
    }

    this.setState({ dailyActivities: steps });

    console.log('123redux**: kcals: ' + JSON.stringify(kcals));

    dailyHighestStep = this.getHighestValue(steps);
    const highestSleep = this.getHighestValue(dailySleepData);

    // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
    // it won't reach the ceiling of the graph component.

    this.updateYAxisState((this.props.targetSteps > dailyHighestStep ? this.props.targetSteps : dailyHighestStep) + (Platform.OS === 'android' ? 100 : 150),
      highestSleep < 23 ? highestSleep + 2 : highestSleep);

    this.updateXAxisState(valueFormatter, daysInMonth);

    if (dailyHighestStep <= 0 && highestSleep <= 0) {
      this.setState({ isNoDataAvailable: true });
    } else {
      this.setState({ isNoDataAvailable: false });
    }

    const lineData = [0];
    let defVal = (typeof this.props.targetSteps === 'undefined' || this.props.targetSteps === null
      || isNaN(this.props.targetSteps)) ? 0 : this.props.targetSteps;

    // this resets the line chart data to zero once the chosen month or chosen year is not equal to the current month and current year.
    if (this.state.chosenMonth !== new Date().getMonth() || this.state.chosenYear !== new Date().getFullYear()) {
      defVal = 0;
    }

    for (let i = 0; i <= 32; i++) {
      lineData[i] = defVal;
    }

    console.log('123xhighlight: ' + JSON.stringify(steps) + '\tsteps.length: ' + steps.length);

    const color = new Date().getMonth() !== this.state.chosenMonth || new Date().getFullYear() !== this.state.chosenYear ? 'transparent' :
      xHighlight > 0 && xHighlight + 0.2 === new Date().getDate() ? TARGET_LINE_COLOR : this.props.targetSteps > 0 ? TARGET_LINE_COLOR : this.state.lineColor;

    this.updateDataSets(steps, dailySleepData, xHighlight + BAR_DATA_GAP === new Date().getDate() ? this.updateLineChartValues(targetSteps) : lineData,
      color, configNoGroupSpace);

    let latestDay = 0;

    for (let i = steps.length; i > 0; i--) {
      if (typeof steps[i] !== 'undefined' && steps[i].y > 0) {
        latestDay = i;
        break;
      }
    }

    // we need to check whether the renderDaily() is rendering chart data from the current month or the previous
    // month before we get to display the metrics. Showing the latest metrics should only be possible when viewing from
    // the current month and year.
    if (steps.length > 0 && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
      const currentDay = new Date().getDate();
      // set default values for calories, steps, time, and bpm
      const hourMinsArr = DateUtil.formatTime(timeIntervals[currentDay]);
      const hour = hourMinsArr[0];
      const mins = hourMinsArr[1];
      const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : (mins > 1 ? mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MIN'));

      this.setActivityMetrics(steps[currentDay].y, kcals[currentDay], recordedTime, heartRates[currentDay]);
    }

    this.scaleChartForDailyInterval(latestDay);

    if (xHighlight > 0 && xHighlight + BAR_DATA_GAP === new Date().getDate()) {
      this.setState({ highlights: [{ x: xHighlight, y: yHighlight, dataIndex: 1 }] });
    }

    console.log('xhighlight', xHighlight);
    console.log('yhighlight', yHighlight);
  }

  scaleChartForDailyInterval(latestDay) {
    // if chart is in monthly view, then we need to re-adjust the scale to suit the daily duration view
    this.setZoomLevel(4, 0, latestDay);
  }

  setActivityMetrics(numSteps, calBurnt, activityTime, activityBPM) {
    this.setState({
      numSteps: typeof numSteps === 'undefined' ? 0 : numSteps + ' ' + (numSteps > 1 ? I18n.t('STEPS') : I18n.t('STEP')),
      calBurnt: (typeof calBurnt === 'undefined' || isNaN(calBurnt) ? 0 : calBurnt) + ' ' + I18n.t('KCAL'),
      activityTime: activityTime,
      activityBPM: activityBPM + ' ' + I18n.t('BPM')
    });

    if (this.state.selectedInterval === I18n.t('DAILY')) {
      this.props.updateSettingsModule({
        prop: 'latestSteps', value: numSteps
      });
      this.props.updateSettingsModule({
        prop: 'latestCal', value: (typeof calBurnt === 'undefined' || isNaN(calBurnt) ? 0 : calBurnt)
      });
      this.props.updateSettingsModule({
        prop: 'latestActivityTime', value: activityTime
      });
      this.props.updateSettingsModule({
        prop: 'latestBPM', value: activityBPM
      });
    }
  }

  setToPreviousInterval() {
    this.setState({ nextDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) - 1;
    let year = parseInt(this.state.chosenYear);

    if (month < 0) {
      if (year <= this.state.earliestYear) {
        this.setState({ previousDateTextColor: 'transparent' });
        return;
      }
      month = 11;
      year -= 1;
    }

    this.setState({ chosenMonth: month, chosenYear: year });

    // The reason we're setting this variable is because we need to ensure
    // that when the user presses the previous month, we need to maintain the
    // date text to show the month. Right now it's showing the weekly range.
    if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      this.setState({ changeWeeklyLabel: false });
    } else {
      this.setState({ changeWeeklyLabel: true });
    }

    // note the reason we are adding 1 here is because monthNames[] start with index 0
    // whereas in our document's date field, the month starts with 1 and ends with 12.
    // this.sortDatasetByMonths(month + 1);
    this.sortDatasetByIntervals();
    this.setState({ dateSet: year + '\n' + getMonthComplete()[month].toUpperCase(), isTrophyVisible: false });
  }

  setToNextInterval() {
    this.setState({ previousDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) + 1;
    let year = parseInt(this.state.chosenYear) + 1;

    if (month > new Date().getMonth()) {
      if (year > new Date().getFullYear()) {
        // this is not allowed, we should hide the right arrow key
        this.setState({ nextDateTextColor: 'transparent' });
        return;
      } else {
        if (month > 11) {
          // we reset month to 0
          month = 0;
          this.setState({ chosenYear: year, chosenMonth: month, dateSet: year + '\n' + getMonthComplete()[month].toUpperCase() });
        } else {
          this.setState({ chosenYear: (year - 1), chosenMonth: month, dateSet: (year - 1) + '\n' + getMonthComplete()[month].toUpperCase() });
        }

        setTimeout(() => {
          this.sortDatasetByIntervals();
        }, 5);
        return;
      }
    } else {
      this.setState({ chosenMonth: month });

      // The reason we're setting this variable is because we need to ensure
      // that when the user presses the previous month, we need to maintain the
      // date text to show the month. Right now it's showing the weekly range.
      if (this.state.selectedInterval === I18n.t('WEEKLY')) {
        this.setState({ changeWeeklyLabel: false });
      } else {
        this.setState({ changeWeeklyLabel: true });
      }

      // note the reason we are adding 1 here is because monthNames[] start with index 0
      // whereas in our document's date field, the month starts with 1 and ends with 12.
      // this.sortDatasetByMonths(month + 1);
      // this.sortDatasetByIntervals();
      setTimeout(() => {
        this.sortDatasetByIntervals();
      }, 5);
    }

    this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[month].toUpperCase() });
  }

  setXValuesFormatter(month) {
    const year = this.state.chosenYear;
    const daysInMonth = this.getDaysInMonth(month + 1, year);
    const valueFormatter = [];

    if (this.state.selectedInterval === I18n.t('DAILY')) {

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
      }

      // this adds a "0" element at the beginning of the valueFormatter array.
      valueFormatter.splice(0, 0, '0');
      this.updateXAxisState(valueFormatter, 31);
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // update x-axis coordinates.
      this.updateXAxisState(this.getWeekRange(daysInMonth), 6);
    }
  }

  sortDatasetByIntervals() {
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      // we render the chart with the daily interval, thereby calling onPressDaily()
      this.onPressDaily();
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // we render the chart with the weekly interval, thereby calling onPressWeekly()
      this.onPressWeekly();
    } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {
      // we render the chart with the monthly interval, thereby calling onPressMonthly()
      this.onPressMonthly();
    } else if (this.state.selectedInterval === I18n.t('YEARLY')) {
      // we render the chart with the yearly interval, thereby calling onPressYearly()
      this.onPressYearly();
    }
  }

  setupViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'activitiesByDocId',
      include_docs: true
    });
  }

  setupSleepViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'sleepByDocId',
      include_docs: true
    });
  }

  setZoomLevel(zoomLevelX, zoomLevelY, moveToX) {
    console.log('123redux!: moveToX: ' + moveToX);
    this.setState({
      isMonthlyView: false,
      zoom: {
        xValue: moveToX,
        yValue: 0,
        scaleX: zoomLevelX,
        scaleY: zoomLevelY
      }
    });
  }

  sortDatasetByMonths(month) {
    let sortedDocs = [];

    for (let i = 0; i < this.state.rows.length; i++) {
      const document = this.state.rows[i].doc;
      const hyphenIndex = document.date.indexOf('-');
      const extractedMonth = parseInt(document.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedMonth === month) {
        sortedDocs[i] = document;
      }
    }

    return sortedDocs;
  }

  storeActivityData(newDoc, totalMins) {
    // before we post the document to couchbase, we must first check our local
    // documents to see if a document with the same date as the one we're going
    // to pass is already there, if it's there, then we just need to use the same
    // document and modify its content
    console.log('123redux document!: ' + JSON.stringify(newDoc));

    const currentDate = getCurrentDate();
    const docDate = new Date(newDoc.date);
    const dateNow = new Date(currentDate);
    const dateFromExistingDoc = new Date(this.props.dateAndDocumentMap.date);

    const dayToUpdate = docDate.getDate();

    if (typeof manager.document !== 'undefined') {
      console.log('123redux document not undefined!');

      // we search through the daily records to see if there's a document with equal current date
      // if there is, we grab that document and update it
      let doc = {};
      for (let i = 0; i < sortedRowsByCurrentMonthAndYear.length; i++) {
        const row = sortedRowsByCurrentMonthAndYear[i];
        if (typeof row === 'undefined') {
          continue;
        }

        if (row !== undefined && row.date !== undefined && row.date !== ''
          && parseInt(row.date.substring(row.date.length - 2)) === new Date().getDate() && row.user_id === newDoc.user_id) {
          doc = row;
          break;
        }
      }

      console.log('123doc: doc: ' + JSON.stringify(doc));

      // there's a match, we need to retrieve that data and update it.
      if (doc._id !== undefined) {
        newDoc._id = doc._id;
        console.log('123updatedoc: oldDoc: ' + JSON.stringify(doc) + '\tnewDoc: ' + JSON.stringify(newDoc));
        newDoc = this.computeTotalTimeAndAverageHeartRate(doc, newDoc);
        console.log('123updatedoc: updated newDoc: ' + JSON.stringify(newDoc));

        if (shouldUpdateDateSmartBandConnected && this.props.dateSmartBandConnected !== '') {
          // first we get the value of time in the activity document.
          const minToSubtract = doc.time;
          const dateSmartBandConnected = this.props.dateSmartBandConnected;
          console.log('123updatedoc: minToSubtract: ' + minToSubtract);
          dateSmartBandConnected.setMinutes(dateSmartBandConnected.getMinutes() - parseInt(minToSubtract));
          console.log('123updatedoc: updated dateSmartBandConnected: ' + dateSmartBandConnected);

          this.props.updateSettingsModule('dateSmartBandConnected', dateSmartBandConnected);
          console.log('123updatedoc: updateSettingsModule - dateSmartBandConnected!!!');
          savePreference('shouldUpdateDateSmartBandConnected', JSON.stringify(false));
          shouldUpdateDateSmartBandConnected = false;
        } else {
          console.log('123updatedoc: should not update smartband init connection time...');
        }

        const dateFromConnection = new Date(this.props.dateSmartBandConnected);
        const totalMinsFromConnection = computeTotalMins(dateFromConnection.getHours(), dateFromConnection.getMinutes());

        const deductedMins = totalMins - totalMinsFromConnection;

        newDoc.time = deductedMins;
        this.props.updateSettingsModule({ prop: 'deductedMins', value: deductedMins });

        if (this.props.targetSteps === 0 && doc.target_steps > 0) {
          newDoc.target_steps = doc.target_steps;
        }

        console.log('123targetcals1 newDoc: ' + JSON.stringify(newDoc));

        manager.document.put({ db: DB_NAME, doc: doc._id, body: newDoc, rev: doc._rev }).then(res => {
          // we then update our dateAndDocMap stored in redux.
          console.log('12redux! res: ' + JSON.stringify(res));
          this.upsertDateAndDocMap(res, newDoc);
          this.queryActivityDocuments();
        }).catch(err => {
          console.log('123watch: error in posting document to couchbase: ' + JSON.stringify(err));
        });

      } else {
        // no match, we create a new document in couchbase.
        manager.document.post({ db: DB_NAME, body: newDoc }).then(res => {
          this.upsertDateAndDocMap(res, newDoc);
          this.queryActivityDocuments();
        }).catch(err => {
          console.log('123watch: error in posting document to couchbase: ' + err);
        });
      }
    }
  }

  // get the latest revision of a document
  getLatestRevision(docId) {
    console.log('123redux: docId: ' + docId);
    const arrData = this.state.rows;
    let rev = '';
    for (let i = 0; i < arrData.length; i++) {
      if (arrData[i].doc._id === docId) {
        rev = arrData[i].doc._rev;
        break;
      }
    }
    return rev;
  }

  subtractSteps() {
    let steps = typeof this.props.targetSteps === 'undefined' ? 0 : this.props.targetSteps;
    let stepsLabel = '';

    if (steps > 0) {
      steps -= 1;
    }
    stepsLabel = steps <= 1 ? ' ' + I18n.t('STEP') : ' ' + I18n.t('STEPS');

    const target = steps + stepsLabel;
    const color = steps === 0 ? 'transparent' : TARGET_LINE_COLOR;
    this.setState({ targetText: steps < 1 ? I18n.t('SET_TARGET') : target, lineColor: color });
    this.props.updateSettingsModule({ prop: 'targetSteps', value: steps });

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
      this.updateLineChartValues(steps), color, this.state.data.barData.config);
  }

  updateDataSets(activityCount, sleepCount, lineChartValues, lineColor, config) {
    console.log('123redux! activityCount: ' + JSON.stringify(activityCount));
    this.setState({
      data: {
        barData: {
          dataSets: [
            {
              values: activityCount,
              label: I18n.t('ACTIVITY'),
              colors: [
                processColor('#77b1c2'), processColor('#d3bdeb')
              ],
              drawFilled: true,
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                textSize: 15,
                drawValues: false,
                colors: [processColor('#77b1c2')],
                axisDependency: 'left'
              }
            }, {
              // Note: for now we'll be making the y-axis values of Sleep Data the same as the Activity Data.
              values: sleepCount,
              label: I18n.t('SLEEP'),
              valueFormatter: ['100', '200'],
              config: {
                drawValues: false,
                colors: [processColor('#d3bdeb')],
                axisDependency: 'right'
              }
            }
          ],
          config: config
        },
        lineData: {
          dataSets: [{
            values: lineChartValues,
            label: I18n.t('TARGET'),
            config: {
              colors: [processColor(lineColor)],
              // drawValues: true,
              drawValues: false,
              valueTextSize: 12,
              valueFormatter: '#',
              valueTextColor: processColor('grey'),
              mode: 'CUBIC_BEZIER',
              drawCircles: false,
              lineWidth: 2,
              drawFilled: false,
              group: {
                fromX: 0,
                groupSpace: 0,
                barSpace: 15
              },
              dashedLine: {
                lineLength: 20,
                spaceLength: Platform.OS === 'ios' ? 15 : 40
              }
            }
          }]
        }
      }
    });
  }

  upsertDateAndDocMap(res, doc) {
    console.log('123watch-- not equal! RESPONSE!!: ' + JSON.stringify(res));
    const docId = res.obj.id;
    const rev = res.obj.rev;

    // timeConsumed field should be in minutes, i.e. 120 for 120 mins or 2 hours.

    // we store the docId, rev, and date of the document's creation in redux:
    const dateAndDocumentMap = {
      rev: rev,
      docId: docId,
      time: doc.time,
      activity: doc,
      date: doc.date // note the date here is taken from the smartwatch.
    };

    this.props.updateSettingsModule({ prop: 'dateAndDocumentMap', value: dateAndDocumentMap });
  }

  updateXAxisState(valueFormatter, axisMaximum) {
    this.setState({
      xAxis: {
        valueFormatter: valueFormatter,
        granularityEnabled: true,
        granularity: 1,
        drawGridLines: false,
        axisMinimum: 0,
        axisMaximum: axisMaximum + 1,
        textSize: 12,
        position: 'BOTTOM'
      }
    });
  }

  updateYAxisState(maxPointStep, maxPointSleep) {
    this.setState({
      yAxis: {
        left: {
          drawGridLines: true,
          gridLineWidth: 1,
          // labelCount: maxPoint,
          // labelCountForce: false,
          axisMaximum: maxPointStep,
          axisMinimum: 0,
          textSize: normalize(15)
        },
        right: {
          enabled: true,
          $set: {
            valueFormatter: ['1', '2', '3', '4', '5']
          },
          drawGridLines: false,
          axisMaximum: maxPointSleep,
          axisMinimum: 0,
          textSize: normalize(15)
        }
      }
    });
  }

  upsertTargetDocument() {
    setTimeout(() => {
      const target = {
        actual_walk: this.state.actualWalk, target_walk: this.props.targetSteps, modified_date: DateUtil.getFormattedTimestamp()
      };

      const actualData = {
        actual_walk: this.state.actualWalk, modified_date: DateUtil.getFormattedTimestamp()
      };

      TargetDoc.upsertTargetDocument('activity', target, () => { });
      CurationActualData.upsertTargetDocument('activity', actualData, () => { });
    }, 5);
  }

  render() {
    return (
      <View style={widgets.mainBrainwaveView}>
        <HeaderMain style={{ flex: 1 }} hideTopHeader='true' />
        <View style={{ flex: 10 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh}
                tintColor='white'
                title={I18n.t('LOADING')}
                titleColor="white"
                colors={['rgb(233, 186, 0)']}
                progressBackgroundColor='white' />
            }>
            <View style={containers.activityChartAndInfoCon}>
              <View style={{ marginTop: 5, marginBottom: 5, flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={text.textTitle}>{I18n.t('ACTIVITY_SLEEP_ANALYSIS')}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                zIndex: 20,
                height: 30
              }}>
                {
                  this.state.isDateVisible && this.state.isLeftArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={ this.previousDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToPreviousInterval()
                    }, 100);
                  }
                  }>{'<'}</Text></View>
                }
                {
                  this.state.isDateVisible &&
                  <Text style={[widgets.commonFont, { textAlign: 'center', flex: 1}]}>{this.state.dateSet}</Text>
                }
                {
                  this.state.isDateVisible && this.state.isRightArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={this.nextDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToNextInterval()
                    }, 100);
                  }
                  }>{'>'}</Text></View>
                }
              </View>

              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={[widgets.hoursStepsText, { textAlign: 'left' }]}> {I18n.t('STEPS')} </Text>
                <Text style={[widgets.hoursStepsText, { textAlign: 'right' }]}> {I18n.t('HOURS')} </Text>
              </View>
              <CombinedChart
                style={widgets.activityBarChart}
                data={this.state.data}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                animation={{
                  durationX: 0
                }}
                touchEnabled={true}
                legend={this.state.legend}
                marker={this.state.marker}
                highlights={this.state.highlights}
                gridBackgroundColor={processColor('#ffffff')}
                drawBarShadow={false}
                drawValueAboveBar={true}
                autoScaleMinMaxEnabled={true}
                doubleTapToZoomEnabled={false}
                scaleEnabled={false}
                zoom={this.state.zoom}
                chartDescription={{ text: '' }}
                chartDescriptionFontSize={18}
                dragEnabled={this.state.dragEnabled}
                drawBorders={false}
                config={this.state.config}
                drawHighlightArrow={false}
                onSelect={this.handleSelect.bind(this)}
                ref="chart" />

              {
                this.state.isNoDataAvailable &&
                <Text style={widgets.textNoData}>{I18n.t('NO_DATA_YET')}</Text>
              }

              <SetTargetModal placeholder={I18n.t('SET_TARGET')} buttonTitle={I18n.t('SET_TARGET')} maxLength={5} inputChange={txt => this.onChangeTarget(txt)} closeModal={() => {
                changedSteps = -1;
                this.setState({ modalVisible: false });
              }} isModalVisible={this.state.modalVisible} onSetTarget={() => {
                if (changedSteps !== -1) {
                  this.setState({ targetText: changedTarget, lineColor: changedColor, targetSteps: changedSteps });
                  this.props.updateSettingsModule({ prop: 'targetSteps', value: changedSteps });
                  this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
                  this.updateLineChartValues(changedSteps), changedColor, this.state.data.barData.config);                
                              
                  let tStep = 0;
                  if (typeof changedTarget === 'string') {
                    tStep = changedTarget.substring(0, changedTarget.indexOf(' '));
                    console.log('tStep: ' + tStep);
                  }

                  if (changedSteps === 0) {
                    this.setState({ targetText: I18n.t('SET_TARGET') });
                  }

                  if (tStep > this.state.yAxis.left.axisMaximum) {
                    this.updateYAxisState(parseInt(tStep) + (Platform.OS === 'android' ? 100 : 150), this.state.yAxis.right.axisMaximum);
                  } else {
                    this.updateYAxisState(dailyHighestStep + (Platform.OS === 'android' ? 100 : 150), this.state.yAxis.right.axisMaximum);
                  }
                }
                  this.setState({ modalVisible: false });
              }} />

              <View style={widgets.lowerSectionBrainwave}>
                {
                  !this.state.isNoDataAvailable &&
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 20 }}>
                    <View style={[widgets.legendMarker, { backgroundColor: '#77b1c2' }]} />
                    <Text style={{ marginLeft: 5, fontSize: normalize(14) }}>{I18n.t('ACTIVITY')}</Text>
                    <View style={[widgets.legendMarker, { backgroundColor: '#d3bdeb', marginLeft: 20 }]} />
                    <Text style={{ marginLeft: 5, fontSize: normalize(14) }}>{I18n.t('SLEEP')}</Text>
                  </View>
                }
                {
                  this.state.isTrophyVisible &&
                  <Target onPressAdd={() => this.addSteps()}
                    onPressSubtract={() => this.subtractSteps()}
                    onTextPress={() => {
                      this.setState({ modalVisible: true });
                    }}
                    targetVal={this.state.targetText} />
                }
                {
                  !this.state.isTrophyVisible &&
                  <View style={{ height: 53 }} />
                }
              </View>
              <View style={containers.activityInfoCon}>
                <View style={containers.activityInfoRowCon}>
                  <View style={{ flex: 0.5, backgroundColor: 'white', marginRight: 2, flexDirection: 'row', justifyContent: 'flex-start', paddingHorizontal: 10, paddingVertical: 5 }}>
                    <Image style={widgets.activityIcons} source={require('../../images/activity/steps.png')} />
                    <View>
                      <Text style={{ fontSize: normalize(18), marginLeft: 15, paddingTop: 2, color: '#7F7F7F' }}>{this.state.numSteps}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 0.5, marginLeft: 2, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'flex-start', paddingHorizontal: 10, paddingVertical: 5 }}>
                    <Image style={widgets.activityIcons} source={require('../../images/activity/calories.png')} />
                    <View>
                      <Text style={{ fontSize: normalize(18), marginLeft: 15, paddingTop: 2, color: '#7F7F7F' }}>{this.state.calBurnt}</Text>
                    </View>
                  </View>
                </View>
                <View style={containers.activityInfoRowCon}>
                  <View style={{ flex: 0.5, backgroundColor: 'white', marginRight: 2, flexDirection: 'row', justifyContent: 'flex-start', paddingHorizontal: 10, paddingVertical: 5 }}>
                    <Image style={[widgets.activityIcons, { marginTop: 0 }]} source={require('../../images/activity/time.png')} />
                    <View>
                      <Text style={{ fontSize: normalize(18), marginLeft: 15, paddingTop: 2, color: '#7F7F7F' }}>{this.state.activityTime}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 0.5, marginLeft: 2, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'flex-start', paddingHorizontal: 10, paddingVertical: 5, }}>
                    <Image style={widgets.activityIcons} source={require('../../images/activity/heartbeat.png')} />
                    <View>
                      <Text style={{ fontSize: normalize(18), marginLeft: 15, paddingTop: 2, color: '#7F7F7F' }}>{this.state.activityBPM}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
        <ChartDuration
          onPressDaily={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressDaily();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressWeekly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.setState({ changeWeeklyLabel: true });
              this.onPressWeekly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressMonthly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressMonthly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressYearly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressYearly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          style={{ flex: 1 }} />


        <Modal
          animationIn='zoomIn'
          animationOut='zoomOut'
          backdropColor='transparent'
          onBackButtonPress={() => {
            this.setState({ progressModal: false });
          }}
          isVisible={this.state.progressModal}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
          </View>
        </Modal>

      </View>
    );
  }
}

export default connect(mapStateToProps, { updateSettingsModule })(ActivitySleepAnalysis);
