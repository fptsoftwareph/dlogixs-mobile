import React, { Component } from 'react';
import HeaderMain from '../common/HeaderMain';
import * as DateUtil from '../../utils/DateUtils';
import getCurrentDate from '../../utils/DateUtils';
import I18n from '../../translate/i18n/i18n';
import { TARGET_LINE_COLOR } from '../../styles/colors';
import Modal from 'react-native-modal';
import { COLOR_THUMB_TINT } from '../../styles/colors';
import * as Progress from 'react-native-progress';
import { getPreference, showToast, savePreference } from '../../utils/CommonMethods';

import {
  View,
  ScrollView,
  processColor,
  Text,
  Image,
  Alert,
  AppState,
  Platform,
  RefreshControl,
  PermissionsAndroid,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from '../../styles/widgets';
import { containers } from '../../styles/containers';
import { text } from '../../styles/text';
import { CombinedChart } from 'react-native-charts-wrapper';
import { ChartUtils } from './../../utils/ChartUtils';
import ChartDuration from '../common/ChartDuration';
import Target from '../common/Target';
import SetTargetModal from '../common/SetTargetModal';
import { updateSettingsModule } from '../../redux/actions';
import { connect } from 'react-redux';
import normalize from '../../utils/GetPixelSizeForLayoutSize';
const { height } = Dimensions.get('window');

const LINE_CHART_VALUES = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

let ctr1 = 0;
let changedColor = 'transparent';
let changedTarget = 0;
let changedHours = 0;
let dailyHighestStep = 0;
let gSleepOnsetLatency = [0];
let gWakeOnsets = [0];
let activity = [0];
let targetSleepHours = [0];
let timeIntervals = [0];
let totalSleep = [0];
let enableTarget = true;

const BAR_DATA_GAP = 0.2;

let weeklySleepData = [0];
let weeklyWakeOnsets = [0];
let weeklyTimeIntervals = [0];
let weeklySleepLatency = [0];

let monthlySleep = [0];
let monthlyWakeOnsets = [0];
let monthlyTimeIntervals = [0];
let monthlyTotalSleep = [0];

let yearlySleep = [0];
let yearlyWakeOnsets = [0];
let yearlyTimeIntervals = [0];
let yearlySleepLatency = [0];

var activityTimer;
let user_id = '';

const ACTIVITY = I18n.t('ACTIVITY');
const SLEEP = I18n.t('SLEEP');

const SLEEP_DATA = [];
const ACTIVITY_DATA = [];

let COLORS = [];
let hypnoColor = processColor('');

const configWithGroupSpace = {
  barWidth: 0.4,
  group: {
    fromX: 0.5,
    groupSpace: BAR_DATA_GAP,
    barSpace: 0
  }
};

const configNoGroupSpace = {
  barWidth: 0.4
};

const defaultMarker = {
  enabled: true,
  markerColor: processColor('transparent'),
  textColor: processColor('#000000'),
  markerFontSize: 8
};

const invisibleMarker = {
  enabled: false
};

const DAYS_OF_THE_MONTH = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
  '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

const BURNED_CALORIES = 'Burned calories';
const ACTIVITY_TIME = I18n.t('TIME');

const { getMonthNames, getMonthComplete } = DateUtil;
const COUCHBASE_CRED = require('../../config/demo_couchbase_cred.json');

const mapStateToProps = (state) => {
  const { dateAndDocumentMap, targetSleepHours, isBstationHeadSetEnabled,
    connectedPeripheralId, minsInADay, deductedMins, latestTotalSleep, latestSleepLatency, latestWakeOnset } = state.settingsModule;
  console.log('123watch-- dateAndDocumentMap ' + JSON.stringify(dateAndDocumentMap));

  console.log('123redux** deductedMinsProps: ' + deductedMins + '\tminsInADay: ' + minsInADay);

  return {
    dateAndDocumentMap, targetSleepHours, isBstationHeadSetEnabled,
    connectedPeripheralId, minsInADay, deductedMins, latestTotalSleep, latestSleepLatency, latestWakeOnset
  };
};

let latestDay = 0;
let noResponseCounter = 0;

class SleepAnalysis extends Component {

  addHours() {
    let hours = typeof this.props.targetSleepHours === 'undefined' ? 0 : this.props.targetSleepHours;

    if (hours < 24) {
      console.log('123redux: hours: ' + hours);
      let hoursLabel = '';

      hours += 1;
      hoursLabel = hours <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');

      const target = hours + hoursLabel;
      const color = hours === 0 ? 'transparent' : TARGET_LINE_COLOR;
      this.setState({ targetText: target, lineColor: color });

      this.props.updateSettingsModule({ prop: 'targetSleepHours', value: hours });
      savePreference('targetSleepHours', JSON.stringify(hours));

      this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values, this.updateLineChartValues(hours), color, this.state.data.barData.config);

      if (hours > this.state.yAxis.left.axisMaximum) {
        this.updateYAxisState(hours + (Platform.OS === 'android' ? 10 : 10), this.state.yAxis.right.axisMaximum);
      }
    }
  }

  calculateSleepInfo(sleepData, latencyData, wakeOnsetData) {
    console.log('123sleep: sleepData: ' + sleepData + '\tlatencyData: ' + latencyData + '\twakeOnsetData: ' + wakeOnsetData);
    // Total Sleep Hours
    const sleep = Math.round(sleepData);
    const sleepHr = Math.floor(parseInt(sleep) / 60);
    const sleepMins = parseInt(sleep) % 60;

    // Sleep Latency
    const sleepLatency = latencyData;
    const sleepLatencyHr = Math.floor((parseInt(sleepLatency) / 60));
    const sleepLatencyMins = parseInt(sleepLatency) % 60;

    // Wake After Sleep Onset
    const wakeOnset = wakeOnsetData;
    const wakeOnsetHr = Math.floor((parseInt(wakeOnset) / 60));
    const wakeOnsetMins = parseInt(wakeOnset) % 60;

    const recordedSleep = (sleepHr > 0) ? sleepHr + ' ' + I18n.t('HR') + ' ' + sleepMins + ' ' + I18n.t('MIN') : sleepMins + ' ' + I18n.t('MINS');
    const recordedSleepLatency = (sleepLatencyHr > 0) ? sleepLatencyHr + ' ' + I18n.t('HR') + ' ' + sleepLatencyMins + ' ' + I18n.t('MIN') : sleepLatencyMins + ' ' + I18n.t('MINS');
    const recordedSleepOnset = (wakeOnsetHr > 0) ? wakeOnsetHr + ' ' + I18n.t('HR') + ' ' + wakeOnsetMins + ' ' + I18n.t('MIN') : wakeOnsetMins + ' ' + I18n.t('MINS');

    this.setSleepInfo(recordedSleep, recordedSleepLatency, recordedSleepOnset);
  }

  componentDidMount() {
    console.log('123redux: rows: --------Component Did Mount ------- ');
    showToast(I18n.t('SWIPE_DOWN_REFRESH'));
    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(res => {
        if (res) {
          console.log('Permission is OK');
          // todo: confirm with team/PM about what we're gonna do with an unconfirmed permission.
        } else {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }

    getPreference('userId').then(res => {
      console.log('123redux: getPreference userId: ' + res);
      user_id = res;
    });

    const hoursLabel = this.props.targetSleepHours <= 1 ? I18n.t('HOUR') : I18n.t('HOURS');
    this.setState({
      targetText: (typeof this.props.targetSleepHours === 'undefined' || this.props.targetSleepHours < 1) ?
        I18n.t('SET_TARGET') : this.props.targetSleepHours + ' ' + hoursLabel
    });

    console.log('123redux: dateAndDocumentMap: ' + JSON.stringify(this.props.dateAndDocumentMap));

    const currentDate = getCurrentDate();
    const dateNow = new Date(currentDate);
    console.log('123redux! isBstationHeadSetEnabled: ' + this.props.isBstationHeadSetEnabled);

    if (this.props.connectedPeripheralId === '') {
      console.log('321redux: alert device not connected to headset.');
      Alert.alert('', I18n.t('CONNECT_HEADSET_TO_START'));
    } else {
      console.log('321redux: alert device connected to headset.');
    }

    this.getDatabase();

  }

  getDatabase() {
    manager.database.get_db({ db: DB_NAME })
      .then(res => {
        console.log('couchbase12: res: ' + JSON.stringify(res));
        this.queryActivityDocuments();
        this.querySleepDocuments();
      });
  }


  componentWillMount() {
    console.log('123redux: rows: --------Component Will Mount ------- ');
    console.log('123redux: rows: ' + JSON.stringify(this.state.rows));
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });
    console.log('123redux: chosenYear: ' + this.state.chosenYear);
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });
  }

  getActualDaysInAMonth() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    // to get the actual days in the month, we need to also check if the graph is showing the current/previous month.
    const monthToUse = this.state.chosenMonth === month ? month + 1 : this.state.chosenMonth + 1;
    const yearToUse = this.state.chosenYear === year ? year : this.state.chosenYear;
    return this.getDaysInMonth(monthToUse, yearToUse);
  }


  componentWillUnmount() {
    console.log('sleep123 sleepAnalysis WillUnmount');
    console.log('123redux: rows: --------Component WillUnmount ------- ');
    if (typeof this.feed !== 'undefined') {
      this.feed.stop();
    }
    COLORS = [];
    clearInterval(this.activityTimer);
  }

  constructor() {
    super();

    this.state = {
      sbw: 0,
      sqiAwaken: 0,
      sqiCycle: 0,
      sqiRM: 0,
      sqiStability: 0,
      sqiTotal: 0,
      actualGTBT: 0,
      activityRows: {},
      rows: {},
      progressModal: false,
      display: 'flex',
      highlights: [{ x: -1, y: 0, stackIndex: 27, dataIndex: 0 }],
      dragEnabled: true,
      changeWeeklyLabel: false,
      // we set the default earliest year to the current year.
      earliestYear: new Date().getFullYear(),
      nextDateTextColor: 'rgb(127, 127, 127)',
      previousDateTextColor: 'rgb(127, 127, 127)',
      modalHypnogramVisible: false,
      lineColor: 'transparent',
      isNoDataAvailable: false,
      chosenYear: 0,
      chosenMonth: 0,
      selectedInterval: '',
      daysInMonth: 0,
      dailyActivities: [],
      totalSleep: '0h 0m',
      sleepLatency: '0h 0m',
      wakeAfterSleepOnset: '0h 0m',
      lastDocumentStored: {},
      targetText: I18n.t('SET_TARGET'),
      modalVisible: false,
      targetSleepHours: 0,
      isTrophyVisible: true,
      isRefreshing: false,
      activityData: {},
      scanning: false,
      appState: '',
      wakeOnset: 0,
      isDateVisible: true,
      wakeOnsetText: '5hr 5mins',
      stepsPerTime: '30 mins',
      numSteps: '765 steps',
      calBurnt: '354 totalSleep',
      activityTime: '30 mins',
      activityBPM: '60 bpm',
      leftArrow: '<',
      rightArrow: '>',
      isRightArrowVisible: true,
      isLeftArrowVisible: true,
      marker: defaultMarker,
      dateSet: this.getCurrentDate(),
      legend: {
        enabled: true,
        textSize: 0,
        textColor: processColor('transparent'),
        form: 'SQUARE',
        formSize: 0,
        xEntrySpace: 0,
        yEntrySpace: 0,
        formToTextSpace: 0,
        wordWrapEnabled: true,
        maxSizePercent: 0
      },
      hypnogramData: {
        barData: {
          dataSets: [{
            values: [],
            label: 'WEAK',
            colors: [],
            drawFilled: true,
            fillAlpha: 90,
            textSize: 15,
            config: {
              textSize: 15,
              drawValues: false,
              colors: []
            }
          }],
          config: {
            barWidth: 0.4,
            group: {
              fromX: 0.5,
              groupSpace: BAR_DATA_GAP,
              barSpace: 0
            }
          }
        }
      },
      data: {
        barData: {
          dataSets: [
            {
              values: SLEEP_DATA,
              label: I18n.t('SLEEP'),
              colors: [
                processColor('#77b1c2'), processColor('#d3bdeb')
              ],
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: normalize(14),
              config: {
                textSize: normalize(14),
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }, {
              // 1hr, 2hrs, 3hrs, 4hrs, 8hrs, 6hrs
              values: ACTIVITY_DATA,
              label: I18n.t('ACTIVITY'),
              config: {
                textSize: normalize(14),
                drawValues: false,
                colors: [processColor('#d3bdeb')]
              }
            }
          ],
          config: configWithGroupSpace
        },
        lineData: {
          dataSets: [
            {
              label: '',
              values: LINE_CHART_VALUES,
              config: {
                colors: [processColor('grey')],
                drawValues: false,
                valueTextSize: 18,
                valueFormatter: '#',
                mode: 'CUBIC_BEZIER',
                drawCircles: false,
                lineWidth: 2,
                drawFilled: false,
                group: {
                  fromX: 0,
                  groupSpace: 0,
                  barSpace: 15
                },
                dashedLine: {
                  lineLength: 20,
                  spaceLength: 20
                }
              }
            }
          ]
        }
      },
      yAxis: {
        left: {
          drawGridLines: false,
          textSize: normalize(14),
          axisMinimum: 0,
          axisMaximum: 24
          /*
          **  note: to avoid the y-coordinate from changing its left side grid values whenever scrolling the graph to the right (and vice-versa)
          **  and having this weird "wobbling" effect, always set an axisMaximum. The
          **  axisMaximum limits the highest value to be displayed on the left side of the
          **  y-axis of the graph.

          **  What I usually use as the value for the axisMaximum is
          **  the highest value in the Activity array and add it up with a hundred The 100
          **  I add is just for aesthetics, so the highest value for Activity wouldn't
          **  reach the ceiling of the graph. i.e. [100, 800, 950, 400, 370] // the
          **  axisMaximum would be 950 + 100 = 1050.
          */
        },
        right: {
          enabled: true,
          drawGridLines: false
        }
      },
      yAxisHypno: {
        left: {
          textColor: processColor('transparent'),
          drawGridLines: true,
          labelCount: 6,
          labelCountForce: true,
          axisMaximum: 5,
          axisMinimum: 0,
          textSize: 14
        },
        right: {
          enabled: false
        }
      },
      xAxis: {
        valueFormatter: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
        granularityEnabled: true,
        textSize: 15,
        granularity: 1,
        axisMaximum: 31,
        avoidFirstLastClipping: true,
        axisMinimum: 0,
        position: 'BOTTOM'

      },
      zoom: {
        xValue: 0,
        yValue: 0,
        scaleX: 4,
        scaleY: 0
      },
      zoomHypno: {
        xValue: 0,
        yValue: 0,
        scaleX: 2,
        scaleY: 0
      }
    };
    console.log('123redux! Constructor ');
  }

  filterWakeOnsetsByYear(dataset) {
    const wakeOnsetsArr = [];

    for (let i = 0; i < dataset.length; i++) {
      if (typeof dataset[i] !== 'undefined') {
        const extractedYear = parseInt(dataset[i].date.substring(0, 4));
        if (extractedYear === this.state.chosenYear) {
          wakeOnsetsArr[i] = dataset[i];
        }
      }
    }

    // we remove all undefined elements in wakeOnsetsArr
    for (let i = 0; i < wakeOnsetsArr.length; i++) {
      if (typeof wakeOnsetsArr[i] === 'undefined') {
        wakeOnsetsArr.splice(i, 1);
      }
    }

    return wakeOnsetsArr;
  }

  filterWakeOnsetsByMonthAndYear(dataset) {
    const wakeOnsetsArr = [];

    for (let i = 0; i < dataset.length; i++) {
      if (typeof dataset[i] !== 'undefined') {
        const extractedYear = parseInt(dataset[i].date.substring(0, 4));
        const hyphenIndex = dataset[i].date.indexOf('-');
        const extractedMonth = parseInt(dataset[i].date.substring(hyphenIndex + 1).substring(0, 2));

        if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
          wakeOnsetsArr[i] = dataset[i];
        }
      }
    }

    // we remove all undefined elements in wakeOnsetsArr
    for (let i = 0; i < wakeOnsetsArr.length; i++) {
      if (typeof wakeOnsetsArr[i] === 'undefined') {
        wakeOnsetsArr.splice(i, 1);
      }
    }

    return wakeOnsetsArr;
  }

  filterByChosenMonthAndYear() {
    console.log('123sleep filterByChosenMonthAndYear()');
    // we need to sort the daily chart by the (chosen or current) month and year.
    // Note: chosenMonth and chosenYear are both initialized to the current month and current year in
    // componentDidUpdate().
    const sortedChartData = [];
    let earliestYear = this.state.earliestYear;

    console.log('123sleep: rows: ' + JSON.stringify(this.state.rows));
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (typeof this.state.rows[i].doc.date === 'undefined' || this.state.rows[i].doc.date === '') {
        continue;
      }

      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.rows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.rows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      console.log('123sleep: extractedYear: ' + extractedYear + '\textractedMonth: ' + extractedMonth);

      // Note we add 1 to chosenMonth here because chosenMonth is also used in monthNames array, and monthNames array starts with index 0, hence chosenMonth is by
      // default should start with 0 (for January) and end with 11 (for December).

      console.log('123sleep: this.state.chosenYear: ' + this.state.chosenYear + '\this.state.chosenMonth: ' + (this.state.chosenMonth + 1));
      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          sleep_sqi_cycle: this.state.rows[i].doc.sleep_sqi_cycle,
          sleep_analysis_points: this.state.rows[i].doc.sleep_analysis_points,
          sleep_sqi_stability: this.state.rows[i].doc.sleep_sqi_stability,
          sleep_analysis_wake: this.state.rows[i].doc.sleep_analysis_wake,
          sleep_hypnogram: this.state.rows[i].doc.sleep_hypnogram,
          sleep_analysis_latency: this.state.rows[i].doc.sleep_analysis_latency,
          sleep_analysis_total: this.state.rows[i].doc.sleep_analysis_total,
          sleep_sqi_awaken: this.state.rows[i].doc.sleep_sqi_awaken,
          steps_count: 0,
          user_id: this.state.rows[i].doc.user_id,
          target_hours: this.state.rows[i].doc.target_hours === undefined ? 0 : this.state.rows[i].doc.target_hours,
          rev: this.state.rows[i].doc._rev,
          timestamp: this.state.rows[i].doc.timestamp,
          type: this.state.rows[i].doc.type
        };
        sortedChartData[i] = rec;
      }
    }

    const stepsList = [];

    for (let i = 0; i < this.state.activityRows.length; i++) {

      if (typeof this.state.activityRows[i].doc.date === 'undefined' || this.state.activityRows[i].doc.date === '') {
        continue;
      }

      const extractedYear = parseInt(this.state.activityRows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.activityRows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.activityRows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const stepsObj = {
          steps_count: this.state.activityRows[i].doc.steps === undefined ? 0 : this.state.activityRows[i].doc.steps,
          date: this.state.activityRows[i].doc.date
        };

        stepsList.push(stepsObj);
      }
    }

    this.setState({ earliestYear: earliestYear });
    console.log('123sleep: earliestYear: ' + this.state.earliest);

    // we remove all undefined elements in allChartData
    for (let i = sortedChartData.length; i > 0; i--) {
      if (typeof sortedChartData[i] === 'undefined') {
        console.log('123sleep sortedChartData is null or undefined!');
        sortedChartData.splice(i, 1);
      }
    }

    console.log('123sleep!: sortedChartData: ' + JSON.stringify(sortedChartData));
    console.log('123sleep stepsList: ' + JSON.stringify(stepsList));
    return [sortedChartData, stepsList];
  }

  getCurrentDate() {
    const strDate = new Date();
    console.log('MONTHS**', 'months: ' + getMonthComplete());
    return strDate.getDate() + '\n' + getMonthComplete()[strDate.getMonth()].toUpperCase() + ', ' + strDate.getFullYear();
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  getHighestValue(chartValues) {
    let highest = 0;

    if (typeof chartValues[0] !== 'undefined' && typeof chartValues[0].y !== 'undefined') {
      for (let i = 0; i < chartValues.length; i++) {
        console.log('123redux! chartValues: ' + JSON.stringify(chartValues[i]));
        if (chartValues[i] !== undefined && chartValues[i].y > highest) {
          highest = chartValues[i].y;
        }
      }
    } else {
      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i] > highest) {
          highest = chartValues[i];
        }
      }
    }

    return highest;
  }

  getSortedMonthlyData() {
    const sortedSleepChartData = [];
    const sortedStepsChartData = [];
    const sortedWakeOnsetChartData = [];
    const sortedSleepLatencyChartData = [];
    const sortedTimeChartData = [];

    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      console.log('123sleep this.state.chosenYear: ' + this.state.chosenYear + '\textractedYear: ' + extractedYear);
      if (this.state.chosenYear === extractedYear) {
        const recSleep = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.sleep_analysis_total
        };

        const recWakeOnset = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.sleep_analysis_wake
        };

        const recTime = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.time
        };

        const recSleepLatency = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.sleep_analysis_latency
        };

        console.log('123sleep monthly - recSleep: ' + JSON.stringify(recSleep));
        console.log('123sleep monthly - recWakeOnset: ' + JSON.stringify(recWakeOnset) + '\recSleepLatency: ' + JSON.stringify(recSleepLatency));

        sortedSleepChartData[i] = recSleep;
        sortedWakeOnsetChartData[i] = recWakeOnset;
        sortedSleepLatencyChartData[i] = recSleepLatency;
        sortedTimeChartData[i] = recTime;
      }
    }

    for (let i = 0; i < this.state.activityRows.length; i++) {

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.activityRows[i].doc.date.substring(0, 4));
      if (this.state.chosenYear === extractedYear) {
        const recStep = {
          _id: this.state.activityRows[i].doc._id,
          date: this.state.activityRows[i].doc.date,
          count: this.state.activityRows[i].doc.steps === undefined ? 0 : this.state.activityRows[i].doc.steps
        };

        sortedStepsChartData[i] = recStep;
      }
    }

    // we remove all undefined elements in allChartData
    for (let i = 0; i < sortedSleepChartData.length; i++) {
      if (typeof sortedSleepChartData[i] === 'undefined') {
        sortedSleepChartData.splice(i, 1);
      }
      if (typeof sortedStepsChartData[i] === 'undefined') {
        sortedStepsChartData.splice(i, 1);
      }
      if (typeof sortedWakeOnsetChartData[i] === 'undefined') {
        sortedWakeOnsetChartData.splice(i, 1);
      }
      if (typeof sortedSleepLatencyChartData[i] === 'undefined') {
        sortedSleepLatencyChartData.splice(i, 1);
      }
      if (typeof sortedTimeChartData[i] === 'undefined') {
        sortedTimeChartData.splice(i, 1);
      }
    }

    // this is an array of all the sorted data for activity-sleep analysis.
    const sortedChartData = [sortedSleepChartData, sortedStepsChartData, sortedWakeOnsetChartData, sortedSleepLatencyChartData, sortedTimeChartData];
    console.log('123sleep monthly sorted data: ' + JSON.stringify(sortedChartData));

    return sortedChartData;
  }

  getWeekRange(limit) {
    var weeks = [];
    let arrCtr = 0;
    let lastRange = 0;

    for (let i = 0; i < limit; i++) {
      if (i === 0) {
        weeks[arrCtr] = '0';
        arrCtr++;
        continue;
      }

      if (i % 7 == 0) {
        weeks[arrCtr] = (i - 6) + '-' + i;
        arrCtr++;
        lastRange = i;
      }
    }

    weeks.push((limit - lastRange) > 1 ? (lastRange + 1) + "-" + limit : (lastRange + 1)) + '';
    return weeks;
  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
    }

    this.setState({ appState: nextAppState });
  }

  handleSelect(event) {
    const entry = event.nativeEvent;
    if (entry === null) {
      this.setState({
        ...this.state,
        selectedEntry: null
      });
    } else {
      this.setState({
        ...this.state,
        selectedEntry: JSON.stringify(entry)
      });

      console.log('123redux! entry.y: ' + JSON.stringify(entry.y));
      console.log('123redux! entry.x: ' + JSON.stringify(entry));

      if (typeof entry.x !== 'undefined' && typeof entry.y !== 'undefined') {
        const xVal = Math.round(entry.x);
        console.log('yAxisVals: xVal: ' + xVal + '\t length: ' + activity.length);
        if (this.state.selectedInterval === I18n.t('DAILY') && typeof activity[xVal] !== 'undefined') {          // we show the day and month here

          if (xVal === new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
            this.setState({ isTrophyVisible: true });
          } else {
            this.setState({ isTrophyVisible: false });
          }
          if (typeof activity[xVal] !== 'undefined') {

            let dateToShow = xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear;

            if (xVal === 0 || xVal > this.state.daysInMonth) {
              dateToShow = this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase();
            }

            this.setState({
              dateSet: dateToShow
            });

            this.calculateSleepInfo(activity[xVal].y * 60, gSleepOnsetLatency[xVal], gWakeOnsets[xVal]);

            if (typeof targetSleepHours[xVal] === 'undefined') {
              return;
            }

            let tHour = typeof targetSleepHours[xVal] === 'undefined' || isNaN(targetSleepHours[xVal]) ? 0 : targetSleepHours[xVal];
            if (tHour === 0 && this.props.targetSleepHours > 0 && (xVal >= new Date().getDate() &&
              this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear())) {
              tHour = this.props.targetSleepHours;
              console.log('123xhighlight: equal!!!!');
            }
            const color = tHour === 0 ? 'transparent' : TARGET_LINE_COLOR;

            setTimeout(() => {
              this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
                this.updateLineChartValues(tHour), color, this.state.data.barData.config);
            }, 5);
          } else {
            if (xVal === 0 || xVal > this.state.daysInMonth) {
              this.setState({
                dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase()
              });
            } else {
              this.setState({
                dateSet: xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear
              });
            }

            this.setState({
              totalSleep: 0 + ' ' + I18n.t('MIN'),
              sleepLatency: 0 + ' ' + I18n.t('MIN'),
              wakeAfterSleepOnset: 0 + ' ' + I18n.t('MIN')
            });
          }

        } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
          console.log('yAxisVals: xVal: ' + xVal + '\tactivity length: ' + activity.length);

          if (typeof this.state.xAxis.valueFormatter[xVal] === 'undefined' || xVal === 0) {
            this.setState({
              dateSet: getMonthComplete()[this.state.chosenMonth] + ', ' + this.state.chosenYear
            });
          } else {
            this.setState({
              dateSet: this.state.xAxis.valueFormatter[xVal] + '\n' + getMonthComplete()[this.state.chosenMonth] + ', ' + this.state.chosenYear
            });
          }

          this.calculateSleepInfo(weeklySleepData[xVal - 1] * 60, weeklySleepLatency[xVal - 1], weeklyWakeOnsets[xVal - 1]);
        } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {

          this.setState({
            dateSet: (typeof getMonthComplete()[xVal - 1] === 'undefined' ? I18n.t('JANUARY') : getMonthComplete()[xVal - 1]) + '\n' + this.state.chosenYear
          });

          this.calculateSleepInfo(monthlySleep[xVal - 1] * 60, monthlyTotalSleep[xVal - 1], monthlyWakeOnsets[xVal - 1]);
        } else {
          // Note: there's a bug with the graph wherein if you click a bar item, sometimes it returns the zero index,
          // even though it's not the right x-index. We're catching it here for the yearly interval. I haven't observe
          // it occuring in the other intervals, prolly because the way the yearly graph is rendered.
          if (xVal === 0 || typeof yearlySleep[xVal - 1] === 'undefined') {
            return;
          }

          // Note: yearlySleep is structured this way: [{year: 0, count: 100}] - only a sample structure
          // the reason this is structured this way is because of the ChartUtils.getYearlyBarYAxisValues()
          // I've made it reusable for the other developers to use for their records tasks.

          this.setState({
            dateSet: this.state.xAxis.valueFormatter[xVal]
          });

          this.calculateSleepInfo(yearlySleep[xVal - 1].count * 60, yearlySleepLatency[xVal - 1].count, yearlyWakeOnsets[xVal - 1].count);
        }
      }
    }
  }

  managewakeOnsetData(rate) {
    console.log('dafsasfafa: ', rate);
    this.setState({ wakeOnset: rate });
  }

  onChangeTarget(txt) {
    if (!isNaN(txt)) {
      let hours = txt === '' || parseInt(txt) < 0 ? 0 : parseInt(txt);
      if (hours === 0 && this.props.targetSleepHours > 0) {
        hours = this.props.targetSleepHours;
      }

      changedHours = txt === '' || parseInt(txt) < 0 ? 0 : parseInt(txt);

      let hoursLabel = '';
      hoursLabel = hours <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');

      const target = hours + hoursLabel;
      changedColor = hours === 0 ? 'transparent' : TARGET_LINE_COLOR;
      changedTarget = hours < 1 ? I18n.t('SET_TARGET') : target;
    }
  }

  updateLineChartValues(hours) {
    const lineChartValues = [];

    for (var i = 0; i < 35; i++) {
      lineChartValues[i] = hours;
    }

    return lineChartValues;
  }

  onSetTarget() {
    this.setState({ modalVisible: false });
  }

  onPressDaily() {
    console.log('123redux!: --------onPresDaily()--------');
    this.setState({ selectedInterval: I18n.t('DAILY'), dragEnabled: true, display: 'flex' });

    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getDate() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), isTrophyVisible: true });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase(), isTrophyVisible: false });
    }

    console.log('123month: B. month: ' + this.state.chosenMonth + '\tyear: ' + this.state.chosenYear);
    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isDateVisible: true, isRightArrowVisible: true, isLeftArrowVisible: true });
      this.renderDailyChart();
    }, 50);
  }

  onPressWeekly() {
    console.log('123redux!: --------onPressWeekly()--------');
    this.setState({ selectedInterval: I18n.t('WEEKLY'), isRightArrowVisible: true, isLeftArrowVisible: true, dragEnabled: false, display: 'none' });
    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });

      const year = new Date().getFullYear();
      const month = new Date().getMonth();
      const daysInMonth = this.getActualDaysInAMonth();
      const valueFormatter = [];
      this.setState({ daysInMonth: daysInMonth });
      const sleepData = [0];
      const activityData = [0];
      const sleepOnsetLatency = [0];
      const wakeOnsets = [0];
      const sortedChartData = this.filterByChosenMonthAndYear()[0];
      console.log('123redux!: 1sortedChartData: ' + JSON.stringify(sortedChartData));

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
        for (let j = 0; j < sortedChartData.length; j++) {
          if (typeof sortedChartData[j] !== 'undefined') {
            const day = parseInt(sortedChartData[j].date.slice(-2));
            if ((day - 1) === i) {
              sleepData[i] = parseInt(sortedChartData[j].sleep_analysis_total) / 60;
              activityData[i] = parseInt(sortedChartData[j].steps_count);
              wakeOnsets[i] = parseInt(sortedChartData[j].sleep_analysis_wake);
              sleepOnsetLatency[i] = parseInt(sortedChartData[j].sleep_analysis_latency);
            } else {
              if (sleepData[i] === undefined) {
                sleepData[i] = 0;
              }
              if (activityData[i] === undefined) {
                activityData[i] = 0;
              }
              if (wakeOnsets[i] === undefined) {
                wakeOnsets[i] = 0;
              }
              if (timeIntervals[i] === undefined) {
                timeIntervals[i] = 0;
              }
              if (sleepOnsetLatency[i] === undefined) {
                sleepOnsetLatency[i] = 0;
              }
            }
          }
        }
      }

      const activityChartData = this.filterByChosenMonthAndYear()[1];
      console.log('123sleep: activityChartData: ' + JSON.stringify(activityChartData) + '\tsize: ' + activityChartData.length);

      console.log('123sleep:111 activityData: ' + JSON.stringify(activityData) + '\tsize: ' + activityData.length);

      for (let i = 0; i < activityChartData.length; i++) {
        for (let j = 0; j < activityData.length; j++) {
          const stepsDate = parseInt(activityChartData[i].date.slice(-2));
          if (j === stepsDate - 1) {
            activityData[j] = activityChartData[i].steps_count;
          }
        }
      }

      activityData.splice(0, 0, 0);
      sleepData.splice(0, 0, 0);
      sleepOnsetLatency.splice(0, 0, 0);
      wakeOnsets.splice(0, 0, 0);
      const weeklyYAxisValues = ChartUtils.getWeeklyBarYAxisValues(sleepData);
      weeklySleepData = weeklyYAxisValues;

      const weeklySleepArrObj = [];
      for (let i = 0; i < weeklyYAxisValues.length; i++) {
        weeklySleepArrObj.push({ x: i, y: weeklyYAxisValues[i], marker: weeklyYAxisValues[i].toFixed(2) });
      }

      weeklyWakeOnsets = ChartUtils.getWeeklyBarYAxisValues(wakeOnsets);

      console.log('123sleep: activityData: ' + JSON.stringify(activityData) + '\tsize: ' + activityData.length);
      console.log('123sleep: sleepData: ' + JSON.stringify(sleepData) + '\tsize: ' + sleepData.length);
      console.log('123sleep: sleepOnsetLatency 1: ' + JSON.stringify(sleepOnsetLatency) + '\tsize: ' + sleepOnsetLatency.length);
      console.log('123sleep: wakeOnsets: ' + JSON.stringify(wakeOnsets) + '\tsize: ' + wakeOnsets.length);
      weeklySleepLatency = ChartUtils.getWeeklyBarYAxisValues(sleepOnsetLatency);
      const weeklyStepsData = ChartUtils.getWeeklyBarYAxisValues(activityData);
      console.log('123sleep: weekly dividends: ' + JSON.stringify(weeklySleepLatency));
      console.log('123sleep weeklyYAxisValues: ' + JSON.stringify(weeklyYAxisValues));
      console.log('123sleep weeklySleepArrObj: ' + JSON.stringify(weeklySleepArrObj));
      console.log('123sleep weeklyStepsData: ' + JSON.stringify(weeklyStepsData));

      console.log('yAxisValues: weekly hr: ' + JSON.stringify(weeklyWakeOnsets));
      console.log('yAxisValues: weekly time intervals: ' + JSON.stringify(weeklyTimeIntervals));

      const highestHour = this.getHighestValue(weeklySleepData);
      const highestStep = this.getHighestValue(weeklyStepsData);
      console.log('123sleep!: weekly highestHour: ' + highestHour);
      console.log('123sleep!: weekly highestStep: ' + highestHour);

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highestHour + 10, highestStep + 50);

      const weekRange = this.getWeekRange(this.state.daysInMonth);
      const dayOfTheMonth = new Date().getDate() / 7;
      let indexToUse = 0;

      console.log('weekly123: chosenYear: ' + this.state.chosenYear + '\tyear now: ' + new Date().getFullYear() + '\chosenMonth: ' + this.state.chosenMonth + '\tmonth now: '
        + new Date().getMonth());

      if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
        if (this.state.changeWeeklyLabel) {
          if (dayOfTheMonth <= 1) {
            this.setState({ dateSet: weekRange[1] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 0;
          } else if (dayOfTheMonth > 1 && dayOfTheMonth <= 2) {
            this.setState({ dateSet: weekRange[2] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 1;
          } else if (dayOfTheMonth > 2 && dayOfTheMonth <= 3) {
            this.setState({ dateSet: weekRange[3] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 2;
          } else if (dayOfTheMonth > 3 && dayOfTheMonth <= 4) {
            this.setState({ dateSet: weekRange[4] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 3;
          } else if (dayOfTheMonth > 4 && dayOfTheMonth <= 5) {
            this.setState({ dateSet: weekRange[5] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 4;
          }
        }
      } else {
        this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
      }


      if (highestHour > 0 || highestStep > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      // update x-axis coordinates.
      this.updateXAxisState(weekRange, 5);
      this.updateDataSets(weeklySleepArrObj, weeklyStepsData, [0], 'transparent', configWithGroupSpace);

      if (weeklySleepData.length > 0 && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
        this.calculateSleepInfo(weeklyYAxisValues[indexToUse] * 60, weeklySleepLatency[indexToUse], weeklyWakeOnsets[indexToUse]);
      } else {
        this.setState({
          totalSleep: 0 + ' ' + I18n.t('MIN'),
          sleepLatency: 0 + ' ' + I18n.t('MIN'),
          wakeAfterSleepOnset: 0 + ' ' + I18n.t('MIN')
        });
      }


      // update zoom level
      this.setZoomLevel(-5, 0, 1);
    }, 50);
  }

  onPressMonthly() {
    this.setState({
      selectedInterval: I18n.t('MONTHLY'), isRightArrowVisible: true, isLeftArrowVisible: true, dragEnabled: true, display: 'none'
    });

    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getFullYear() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
    }

    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });
      const sortedChartData = this.getSortedMonthlyData();
      // Note: sortedChartData is an array of the different arrays of chart data for activity-sleep analysis,
      // in the following order:
      // sortedChartData = [sortedStepsChartData, sortedWakeOnsetChartData, sortedTotalSleepChartData, sortedTimeChartData];
      const sortedSleepChartData = sortedChartData[0];
      const sortedStepsData = sortedChartData[1];
      const sortedWakeOnsetChartData = sortedChartData[2];
      const sortedSleepLatencyChartData = sortedChartData[3];

      monthlySleep = ChartUtils.getMonthlyBarYAxisValues(sortedSleepChartData);
      const monthlySteps = ChartUtils.getMonthlyBarYAxisValues(sortedStepsData);
      monthlyWakeOnsets = ChartUtils.getMonthlyBarYAxisValues(sortedWakeOnsetChartData);
      monthlyTotalSleep = ChartUtils.getMonthlyBarYAxisValues(sortedSleepLatencyChartData);
      monthlyTimeIntervals = ChartUtils.getMonthlyBarYAxisValues(sortedChartData[4]);

      const monthlySleepArrObj = [];
      for (let i = 0; i < monthlySleep.length; i++) {
        monthlySleep[i] = monthlySleep[i] / 60;
        monthlySleepArrObj.push({ x: i, y: monthlySleep[i], marker: monthlySleep[i].toFixed(2) });
      }

      const highestHour = this.getHighestValue(monthlySleep);
      const highestStep = this.getHighestValue(monthlySteps);
      console.log('123redux!: highest: ' + highestHour);
      console.log('123redux!: current month ' + parseInt(new Date().getMonth()) + 1);
      console.log('123redux!: monthlyTotalSleep ' + monthlyTotalSleep[parseInt(new Date().getMonth())]);

      console.log('123sleep!: monthlySleep ' + JSON.stringify(monthlySleep));
      console.log('123sleep!: monthlySleepArrObj ' + JSON.stringify(monthlySleepArrObj));
      console.log('123sleep!: monthlySteps ' + JSON.stringify(monthlySteps));

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highestHour + 5, highestStep + 100);

      if (highestHour > 0 || highestStep > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }



      // update x-axis coordinates.
      this.updateXAxisState(getMonthNames(), 13);
      console.log('yAxisVals: ' + JSON.stringify(monthlySleep));
      this.updateDataSets(monthlySleepArrObj, monthlySteps, [0], 'transparent', configWithGroupSpace);

      if (monthlySleep.length > 0 && monthlySleep[parseInt(new Date().getMonth())] > 0) {
        console.log('123redux!: current month ' + parseInt(new Date().getMonth()) + 1);

        this.calculateSleepInfo(monthlySleep[parseInt(new Date().getMonth())] * 60, monthlyTotalSleep[parseInt(new Date().getMonth())], monthlyWakeOnsets[parseInt(new Date().getMonth())]);
      } else {
        this.setState({
          totalSleep: 0 + ' ' + I18n.t('MIN'),
          sleepLatency: 0 + ' ' + I18n.t('MIN'),
          wakeAfterSleepOnset: 0 + ' ' + I18n.t('MIN')
        });
      }

      let latestMonth = 0;
      for (let i = monthlySleep.length; i > 0; i--) {
        if (monthlySleep[i] > 0) {
          latestMonth = i;
          break;
        }
      }

      // this means the chart in view right now is on the daily interval
      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5) {
        // update zoom level
        this.setZoomLevel(2, 0, latestMonth);
      } else {
        this.setZoomLevel(this.state.zoom.scaleX, 0, latestMonth);
      }
    }, 50);
  }

  onPressYearly() {
    console.log('123redux!: --------onPressYearly()--------');
    this.setState({
      selectedInterval: I18n.t('YEARLY'), isTrophyVisible: false, isRightArrowVisible: false, isLeftArrowVisible: false, display: 'none'
    });


    if (this.state.chosenYear === new Date().getFullYear()) {
      this.setState({ dateSet: new Date().getFullYear() });
    } else {
      this.setState({ dateSet: this.state.chosenYear });
    }

    setTimeout(() => {
      const allChartData = [];
      const allYearWakeOnset = [];
      const allYearSleepLatency = [];
      const allYearTime = [];
      const allYearStepsData = [];

      for (let i = 0; i < this.state.rows.length; i++) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.sleep_analysis_total
        };
        allChartData[i] = rec;

        const wakeOnsetRec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.sleep_analysis_wake
        };

        allYearWakeOnset[i] = wakeOnsetRec;

        const sleepLatencyRec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.sleep_analysis_latency
        };

        allYearSleepLatency[i] = sleepLatencyRec;
      }

      for (let i = 0; i < this.state.activityRows.length; i++) {
        const recSteps = {
          _id: this.state.activityRows[i].doc._id,
          date: this.state.activityRows[i].doc.date,
          count: this.state.activityRows[i].doc.steps === undefined ? 0 : this.state.activityRows[i].doc.steps
        };
        allYearStepsData[i] = recSteps;
      }

      const yearlyCount = ChartUtils.getYearlyBarYAxisValues(allChartData);
      const yearlyActivityData = ChartUtils.getYearlyBarYAxisValues(allYearStepsData);
      yearlySleep = yearlyCount;
      yearlyWakeOnsets = ChartUtils.getYearlyBarYAxisValues(allYearWakeOnset);
      yearlySleepLatency = ChartUtils.getYearlyBarYAxisValues(allYearSleepLatency);
      yearlyTimeIntervals = ChartUtils.getYearlyBarYAxisValues(allYearTime);

      const yearlySleepArrObj = [];
      for (let i = 0; i < yearlySleep.length; i++) {
        yearlySleep[i].count = yearlySleep[i].count / 60;
        yearlySleepArrObj.push({ x: i, y: yearlySleep[i].count, marker: yearlySleep[i].count.toFixed(2) });
      }

      let counts = [];
      let years = [];
      let steps = [];

      for (let i = 0; i < yearlyCount.length; i++) {
        counts[i] = yearlyCount[i].count;
        years[i] = (yearlyCount[i].year) + '';
      }

      for (let i = 0; i < yearlyActivityData.length; i++) {
        const allChartData = [];
        steps[i] = yearlyActivityData[i].count;
        if (yearlyCount.length > 0) {
          years[i] = (yearlyCount[i].year) + '';
        }
      }

      years.splice(0, 0, '');


      const highestHour = this.getHighestValue(counts);
      const highestStep = this.getHighestValue(steps);
      console.log('123redux!: highest: ' + highestHour);
      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highestHour + 5, highestStep + 100);


      // we're gonna manipulate the years array to make the x-coordinates appear longer in the chart 
      // thereby making the bar chart's width thinner. We need to check whether the years are not 0 so that
      // we won't be change the values of the years' elements.
      let xValues = years;

      if (years.length < 12) {
        // 12 here is an arbitrary value
        for (let i = 0; i < 12; i++) {
          if (typeof years[i] === 'undefined') {
            xValues[i] = '';
          }
        }
      }

      console.log('123redux!: xValues: ' + xValues);
      console.log('123redux!: current year: ' + new Date().getFullYear().toString());
      console.log('123redux!: yearlyCount: ' + yearlyCount[0]);
      console.log('123redux!: counts: ' + counts[0]);

      if (yearlySleep.length > 0) {
        // set default values for total sleep, sleep latency, wake onset
        // Total Sleep Hours
        const index = counts.length - 1;
        this.calculateSleepInfo(counts[index] * 60, yearlySleepLatency[index].count, yearlyWakeOnsets[index].count);
      } else {
        this.setState({
          totalSleep: 0 + ' ' + I18n.t('MIN'),
          sleepLatency: 0 + ' ' + I18n.t('MIN'),
          wakeAfterSleepOnset: 0 + ' ' + I18n.t('MIN')
        });
      }

      if (highestHour > 0 || highestStep > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      // update x-axis coordinates.
      this.updateXAxisState(xValues, xValues.length + 1);
      this.updateDataSets(yearlySleepArrObj, steps, [0], 'transparent', configWithGroupSpace);

      let latestYear = 0;
      for (let i = counts.length; i > 0; i--) {
        if (counts[i] !== '') {
          latestYear = i;
          break;
        }
      }

      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5 || this.state.zoom.scaleX === 2) {
        // update zoom level
        this.setZoomLevel(2, 0, 0);
      }

      if (latestYear < 7) {
        this.setState({ dragEnabled: false });
      } else {
        this.setState({ dragEnabled: true });
      }
    }, 50);
  }

  onRefresh = () => {
    this.setState({ isRefreshing: true });
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      this.querySleepDocuments();
    } else {
      setTimeout(() => {
        this.setState({ isRefreshing: false });
      }, 2000);
    }
  }

  nextDateStyle() {
    return {
      justifyContent: 'flex-end',
      fontSize: 26,
      color: this.state.nextDateTextColor,
      width: 25
    };
  }

  previousDateStyle() {
    return {
      fontSize: 26,
      justifyContent: 'flex-start',
      color: this.state.previousDateTextColor,
      width: 25
    };
  }

  querySleepDocuments() {
    this.setupViewAndQuery().then(r => {
      console.log('123couchbase: query time: ' + JSON.stringify(r.obj.rows));
      this.setState({ rows: r.obj.rows });

      setTimeout(() => {
        this.renderDailyChart();
        this.setState({ isRefreshing: false });
      }, 5);
    }).catch(err => {
      this.showSwipeDownAlert(err);
    });
  }

  showSwipeDownAlert(err) {
    console.log('123couchbase err: ' + JSON.stringify(err));
    if (noResponseCounter < 3 && err.status === 0) {
      console.log('123couchbase failed to query for the first time, re-querying sleep documents...');
      this.querySleepDocuments();
      noResponseCounter += 1;
    }
  }

  queryActivityDocuments() {
    this.setupActivitiesViewAndQuery().then(r => {
      console.log('123sleep: activity query time: ' + JSON.stringify(r.obj.rows));
      this.setState({ activityRows: r.obj.rows });
    });
  }

  renderDailyChart() {
    const daysInMonth = this.getActualDaysInAMonth();
    const valueFormatter = [];
    const hours = [];
    const activityData = [];
    let xHighlight = 0;
    let yHighlight = 0;
    const sleepOnsetLatency = [0];
    const wakeOnsets = [0];
    let targetHours = 0;
    const hypnograpms = [0];
    let sleepHypnogram = [];

    this.setState({ daysInMonth: daysInMonth, marker: defaultMarker });

    const sortedChartData = this.filterByChosenMonthAndYear()[0];


    let shouldShowTargetLine = false;
    let greaterThanTarget = false;

    for (let i = 0; i <= daysInMonth; i++) {
      valueFormatter[i] = (i + 1) + '';
      console.log('123datesets: outer loop: ' + i);
      for (let j = 0; j < sortedChartData.length; j++) {
        if (typeof sortedChartData[j] !== 'undefined') {
          console.log('123datesets: inner loop: ' + j);
          const day = parseInt(sortedChartData[j].date.slice(-2));

          if ((day) === i) {
            const sleepTotal = parseInt(sortedChartData[j].sleep_analysis_total) / 60;
            targetHours = parseInt(sortedChartData[j].target_hours);
            shouldShowTargetLine = targetHours === sleepTotal && sleepTotal > 0;
            greaterThanTarget = sleepTotal > targetHours && targetHours > 0;

            hours[i] = {
              // we need to divide the total sleep analysis by 60 because it is in minutes when we fetch the data from the headset.
              // the graph requires the values to be in hours.
              x: (i > 0 ? i - BAR_DATA_GAP : i), y: sleepTotal,
              marker: shouldShowTargetLine ? '\uD83C\uDFC6' : greaterThanTarget ? '\u2757' : ''
            };

            if (shouldShowTargetLine || greaterThanTarget) {
              xHighlight = day - BAR_DATA_GAP;
              yHighlight = hours[i].y;
            }

            targetSleepHours[i] = parseInt(targetHours);
            activityData[i] = {
              x: (i > 0 ? i + BAR_DATA_GAP : i), y: sortedChartData[j].steps_count
            };
            wakeOnsets[i] = parseInt(sortedChartData[j].sleep_analysis_wake);
            sleepOnsetLatency[i] = parseInt(sortedChartData[j].sleep_analysis_latency);
            hypnograpms[i] = sortedChartData[j].sleep_hypnogram;
            sleepHypnogram = hypnograpms[hypnograpms.length - 1];
            if (day === new Date().getDate()) {
              console.log('123targetcals1: ' + targetHours);
              this.setHypnogramData(sleepHypnogram);
              this.props.updateSettingsModule({ prop: 'targetSleepHours', value: targetHours });
              savePreference('targetSleepHours', JSON.stringify(targetHours));
              const hoursLabel = parseInt(targetHours) <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');
              this.setState({ targetText: parseInt(targetHours, 10) < 1 ? I18n.t('SET_TARGET') : parseInt(targetHours, 10) + hoursLabel });
            }

          } else {
            if (typeof hours[i] === 'undefined') {
              hours[i] = { x: (i > 0 ? i - BAR_DATA_GAP : i), y: 0, marker: '' };
            }
            if (typeof targetSleepHours[i] === 'undefined') {
              targetSleepHours[i] = 0;
            }
            if (typeof activityData[i] === 'undefined') {
              activityData[i] = { x: (i > 0 ? i + BAR_DATA_GAP : i), y: 0, marker: '' };
            }
            if (typeof wakeOnsets[i] === 'undefined') {
              wakeOnsets[i] = 0;
            }
            if (typeof sleepOnsetLatency[i] === 'undefined') {
              sleepOnsetLatency[i] = 0;
            }
            if (typeof timeIntervals[i] === 'undefined') {
              timeIntervals[i] = 0;
            }
          }
        }
      }
    }

    const activityChartData = this.filterByChosenMonthAndYear()[1];

    for (let i = 0; i < activityChartData.length; i++) {
      for (let j = 0; j < activityData.length; j++) {
        const stepsDate = parseInt(activityChartData[i].date.slice(-2)) + 0.2;
        if (activityData[j].x === stepsDate) {
          activityData[j].y = activityChartData[i].steps_count;
        }
      }
    }

    activity = hours;
    gSleepOnsetLatency = sleepOnsetLatency;
    gWakeOnsets = wakeOnsets;

    // this adds a "0" element at the beginning of the valueFormatter array.
    valueFormatter.splice(0, 0, '0');

    if (valueFormatter.length > daysInMonth + 1) {
      // this is a workaround to ensure that the last day on the daily chart is clickable.
      valueFormatter.splice(valueFormatter.length - 1, 1);
    }

    this.setState({ dailyActivities: hours });
    console.log('123redux!: highestHour: ' + highestHour);
    console.log('123redux!: highestStep: ' + highestStep);
    const highestHour = this.getHighestValue(hours);
    const highestStep = this.getHighestValue(activityData);

    // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
    // it won't reach the ceiling of the graph component.
    this.updateYAxisState(highestHour < 23 ? highestHour + 2 : highestHour, highestStep + (Platform.OS === 'android' ? 100 : 150));
    this.updateXAxisState(valueFormatter, 31);

    if (this.props.targetSleepHours > highestHour) {
      this.updateYAxisState(this.props.targetSleepHours + (Platform.OS === 'android' ? 10 : 10), this.state.yAxis.right.axisMaximum);
    }

    console.log('123redux!: valueFormatter: ' + valueFormatter);

    if (highestHour > 0 || highestStep > 0) {
      this.setState({ isNoDataAvailable: false });
    } else {
      this.setState({ isNoDataAvailable: true });
    }

    const lineData = [0];
    let defVal = (typeof this.props.targetSleepHours === 'undefined' || this.props.targetSleepHours === null
      || isNaN(this.props.targetSleepHours)) ? 0 : this.props.targetSleepHours;

    // this resets the line chart data to zero once the chosen month or chosen year is not equal to the current month and current year.
    if (this.state.chosenMonth !== new Date().getMonth() || this.state.chosenYear !== new Date().getFullYear()) {
      defVal = 0;
    }

    for (let i = 0; i <= 32; i++) {
      lineData[i] = defVal;
    }

    const color = new Date().getMonth() !== this.state.chosenMonth || new Date().getFullYear() !== this.state.chosenYear ? 'transparent' :
      xHighlight > 0 && xHighlight + 0.2 === new Date().getDate() ? TARGET_LINE_COLOR : this.props.targetSleepHours > 0 ? TARGET_LINE_COLOR : this.state.lineColor;

    this.updateDataSets(hours, activityData, xHighlight + BAR_DATA_GAP === new Date().getDate() ? this.updateLineChartValues(targetHours) : lineData,
      color, configNoGroupSpace);

    for (let i = hours.length; i > 0; i--) {
      if (typeof hours[i] !== 'undefined' && hours[i].y > 0) {
        latestDay = i;
        break;
      }
    }

    if (hours.length > 0 && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
      const currentDay = new Date().getDate();

      this.calculateSleepInfo(hours[currentDay].y * 60, sleepOnsetLatency[currentDay], wakeOnsets[currentDay]);

    } else if (typeof hours.length === 'undefined' || isNaN(hours.length)) {
      this.setSleepInfo(0 + ' ' + I18n.t('MIN'), 0 + ' ' + I18n.t('MIN'), 0 + ' ' + I18n.t('MIN'));
    } else {
      this.setSleepInfo(0 + ' ' + I18n.t('MIN'), 0 + ' ' + I18n.t('MIN'), 0 + ' ' + I18n.t('MIN'));
    }

    this.scaleChartForDailyInterval(latestDay);

    if (xHighlight > 0 && xHighlight + BAR_DATA_GAP === new Date().getDate()) {
      this.setState({ highlights: [{ x: xHighlight, y: yHighlight, dataIndex: 1 }] });
    }

    this.storeSleepProps();
  }

  scaleChartForDailyInterval(latestDay) {
    // if chart is in monthly view, then we need to re-adjust the scale to suit the daily duration view
    this.setZoomLevel(4, 0, latestDay);
  }

  storeSleepProps() {
    this.props.updateSettingsModule({
      prop: 'latestTotalSleep', value: this.state.totalSleep
    });
    this.props.updateSettingsModule({
      prop: 'latestSleepLatency', value: this.state.sleepLatency
    });
    this.props.updateSettingsModule({
      prop: 'latestWakeOnset', value: this.state.wakeAfterSleepOnset
    });
  }

  setHypnogramData(response) {
    let hypnoColors = [];
    let valueFormatter = [];
    for (let i = 0; i < response.length; i++) {
      if (response[i] == 6) {
        hypnoColor = processColor('transparent');
      } else if (response[i] == 5) {
        hypnoColor = processColor('#5897a7');
      } else if (response[i] == 4) {
        hypnoColor = processColor('#77b1c2');
      } else if (response[i] == 3 || response[i] == 2) {
        hypnoColor = processColor('#d3bdeb');
      } else {
        hypnoColor = processColor('#e3d0fc');
      }
      hypnoColors.push(hypnoColor);
      valueFormatter.push(i + '');
    }
    COLORS = hypnoColors;

    this.setState({
      hypnogramData: {
        barData: {
          dataSets: [{
            values: response,
            label: I18n.t('HYPNOGRAM'),
            drawFilled: true,
            fillAlpha: 90,
            textSize: 15,
            config: {
              textSize: 15,
              colors: COLORS,
              drawValues: false
            }
          }
          ],
          config: {
            barWidth: 0.4
          }
        }
      },
      xAxisHypno: {
        valueFormatter: valueFormatter,
        drawGridLines: false,
        granularityEnabled: true,
        textSize: 15,
        granularity: 1,
        labelCountForce: true,
        avoidFirstLastClipping: true,
        axisMaximum: response.length,
        axisMinimum: 0,
        position: 'BOTTOM'
      },
      zoomHypno: {
        xValue: 0,
        yValue: 0,
        scaleX: Math.ceil(response.length / 7),
        scaleY: 0
      }
    });
  }

  setSleepInfo(recordedSleep, recordedSleepLatency, recordedSleepOnset) {
    this.setState({
      totalSleep: recordedSleep,
      sleepLatency: recordedSleepLatency,
      wakeAfterSleepOnset: recordedSleepOnset
    });
  }

  setToPreviousInterval() {
    this.setState({ nextDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) - 1;
    let year = parseInt(this.state.chosenYear);

    console.log('123month: month: ' + month + "\tyear: " + year + '\tearliestYear: ' + this.state.earliestYear);

    if (month < 0) {
      if (year <= this.state.earliestYear) {
        this.setState({ previousDateTextColor: 'transparent' });
        return;
      }
      month = 11;
      year -= 1;
    }

    this.setState({ chosenMonth: month, chosenYear: year });

    // The reason we're setting this variable is because we need to ensure
    // that when the user presses the previous month, we need to maintain the
    // date text to show the month. Right now it's showing the weekly range.
    if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      this.setState({ changeWeeklyLabel: false });
    } else {
      this.setState({ changeWeeklyLabel: true });
    }

    // note the reason we are adding 1 here is because monthNames[] start with index 0
    // whereas in our document's date field, the month starts with 1 and ends with 12.
    // this.sortDatasetByMonths(month + 1);
    setTimeout(() => {
      this.sortDatasetByIntervals();
    }, 5);

    this.setState({ dateSet: year + '\n' + getMonthComplete()[month].toUpperCase(), isTrophyVisible: false });
  }


  setToNextInterval() {
    this.setState({ previousDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) + 1;
    let year = parseInt(this.state.chosenYear) + 1;

    console.log('123month: month: ' + month + '\tyear: ' + year);

    if (month > new Date().getMonth()) {
      if (year > new Date().getFullYear()) {
        // this is not allowed, we should hide the right arrow key
        this.setState({ nextDateTextColor: 'transparent' });
        return;
      } else {
        if (month > 11) {
          // we reset month to 0
          month = 0;
          this.setState({ chosenYear: year, chosenMonth: month, dateSet: year + '\n' + getMonthComplete()[month].toUpperCase() });
        } else {
          this.setState({ chosenYear: (year - 1), chosenMonth: month, dateSet: (year - 1) + '\n' + getMonthComplete()[month].toUpperCase() });
        }

        setTimeout(() => {
          this.sortDatasetByIntervals();
        }, 5);

        console.log('123month: A. month: ' + this.state.chosenMonth + '\tyear: ' + this.state.chosenYear);
        return;
      }
    } else {
      this.setState({ chosenMonth: month });

      // The reason we're setting this variable is because we need to ensure
      // that when the user presses the previous month, we need to maintain the
      // date text to show the month. Right now it's showing the weekly range.
      if (this.state.selectedInterval === I18n.t('WEEKLY')) {
        this.setState({ changeWeeklyLabel: false });
      } else {
        this.setState({ changeWeeklyLabel: true });
      }

      // note the reason we are adding 1 here is because monthNames[] start with index 0
      // whereas in our document's date field, the month starts with 1 and ends with 12.
      // this.sortDatasetByMonths(month + 1);
      // this.sortDatasetByIntervals();
      setTimeout(() => {
        this.sortDatasetByIntervals();
      }, 5);
    }

    console.log('123month: month: ' + this.state.chosenMonth + '\tyear: ' + this.state.chosenYear);

    this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[month].toUpperCase() });
  }

  setXValuesFormatter(month) {
    const year = this.state.chosenYear;
    const daysInMonth = this.getDaysInMonth(month + 1, year);
    const valueFormatter = [];

    if (this.state.selectedInterval === I18n.t('DAILY')) {

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
      }

      // this adds a "0" element at the beginning of the valueFormatter array.
      valueFormatter.splice(0, 0, '0');
      this.updateXAxisState(valueFormatter, 31);
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // update x-axis coordinates.
      this.updateXAxisState(this.getWeekRange(daysInMonth), 6);
    }
  }

  sortDatasetByIntervals() {
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      // we render the chart with the daily interval, thereby calling onPressDaily()
      this.onPressDaily();
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // we render the chart with the weekly interval, thereby calling onPressWeekly()
      this.onPressWeekly();
    } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {
      // we render the chart with the monthly interval, thereby calling onPressMonthly()
      this.onPressMonthly();
    } else if (this.state.selectedInterval === I18n.t('YEARLY')) {
      // we render the chart with the yearly interval, thereby calling onPressYearly()
      this.onPressYearly();
    }
  }

  setupViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'sleepByDocId',
      include_docs: true
    }).catch(err => {
      console.log('123couchbase: setupviewandquery: err: ' + JSON.stringify(err));
      this.showSwipeDownAlert(err);
    });
  }

  setupActivitiesViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'activitiesByDocId',
      include_docs: true
    });
  }

  setZoomLevel(zoomLevelX, zoomLevelY, moveToX) {
    this.setState({
      isMonthlyView: false,
      zoom: {
        xValue: moveToX,
        yValue: 0,
        scaleX: zoomLevelX,
        scaleY: zoomLevelY
      }
    });
  }

  sortDatasetByMonths(month) {
    let sortedDocs = [];

    for (let i = 0; i < this.state.rows.length; i++) {
      const document = this.state.rows[i];
      const hyphenIndex = document.date.indexOf('-');
      const extractedMonth = parseInt(document.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedMonth === month) {
        sortedDocs[i] = document;
      }
    }

    return sortedDocs;
  }

  // get the latest revision of a document
  getLatestRevision(docId) {
    console.log('123redux: docId: ' + docId);
    const arrData = this.state.rows;
    let rev = '';
    for (let i = 0; i < arrData.length; i++) {
      if (arrData[i]._id === docId) {
        rev = arrData[i]._rev;
        break;
      }
    }
    return rev;
  }

  subtractHours() {
    let hours = typeof this.props.targetSleepHours === 'undefined' ? 0 : this.props.targetSleepHours;

    let hoursLabel = '';

    if (hours > 0) {
      hours -= 1;
    }

    hoursLabel = hours <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');

    const target = hours + hoursLabel;
    const color = hours === 0 ? 'transparent' : TARGET_LINE_COLOR;
    //this.setState({ targetText: target, lineColor: color });
    this.setState({ targetText: hours < 1 ? I18n.t('SET_TARGET') : target, lineColor: color });
    this.props.updateSettingsModule({ prop: 'targetSleepHours', value: hours });
    savePreference('targetSleepHours', JSON.stringify(hours));

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
      this.updateLineChartValues(hours), color, this.state.data.barData.config);
  }

  updateDataSets(sleepCount, activityCount, lineChartValues, lineColor, config) {
    console.log('123redux\!:\ steps_count: ' + JSON.stringify(sleepCount) + '\tactivityCount: ' + JSON.stringify(activityCount) + '\tlineChartValues: ' + JSON.stringify(lineChartValues));
    console.log('123redux: config: ' + JSON.stringify(config));
    console.log('123redux: color: ' + lineColor);

    this.setState({
      data: {
        barData: {
          dataSets: [
            {
              values: sleepCount,
              label: I18n.t('SLEEP'),
              colors: [
                processColor('#77b1c2'), processColor('#d3bdeb')
              ],
              drawFilled: true,
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                axisDependency: 'left',
                textSize: 15,
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }, {
              // Note: for now we'll be making the y-axis values of Sleep Data the same as the Activity Data.
              values: activityCount,
              label: I18n.t('ACTIVITY'),
              config: {
                axisDependency: 'right',
                drawValues: false,
                colors: [processColor('#d3bdeb')]
              }
            }
          ],
          config: config
        },
        lineData: {
          dataSets: [{
            values: lineChartValues,
            label: I18n.t('TARGET'),
            config: {
              colors: [processColor(lineColor)],
              // drawValues: true,
              drawValues: false,
              valueTextSize: 12,
              valueFormatter: '#',
              valueTextColor: processColor('grey'),
              mode: 'CUBIC_BEZIER',
              drawCircles: false,
              lineWidth: 2,
              drawFilled: false,
              group: {
                fromX: 0,
                groupSpace: 0,
                barSpace: 15
              },
              dashedLine: {
                lineLength: 20,
                spaceLength: Platform.OS === 'ios' ? 15 : 40
              }
            }
          }]
        }
      }
    });
  }

  upsertDateAndDocMap(res, doc) {
    console.log('123watch-- not equal! RESPONSE!!: ' + JSON.stringify(res));
    const docId = res.obj.id;
    const rev = res.obj.rev;

    // timeConsumed field should be in minutes, i.e. 120 for 120 mins or 2 hours.

    // we store the docId, rev, and date of the document's creation in redux:
    const dateAndDocumentMap = {
      rev: rev,
      docId: docId,
      time: doc.time,
      activity: doc,
      date: doc.date // note the date here is taken from the headset.
    };

    this.props.updateSettingsModule({ prop: 'dateAndDocumentMap', value: dateAndDocumentMap });
  }

  updateXAxisState(valueFormatter, axisMaximum) {
    console.log('123redux! valueFormatter: ' + JSON.stringify(valueFormatter));
    this.setState({
      xAxis: {
        valueFormatter: valueFormatter,
        granularityEnabled: true,
        granularity: 1,
        drawGridLines: false,
        axisMinimum: 0,
        axisMaximum: axisMaximum + 1,
        textSize: 12,
        position: 'BOTTOM'
      }
    });
  }

  updateYAxisState(maxPointHour, maxPointStep) {
    console.log('yAxisPoint: ' + maxPointHour);
    this.setState({
      yAxis: {
        left: {
          drawGridLines: true,
          gridLineWidth: 1,
          // granularityEnabled: true,
          // granularity: 5,
          // labelCountForce: false,
          axisMaximum: maxPointHour,
          axisMinimum: 0,
          textSize: normalize(14)
        },
        right: {
          enabled: true,
          $set: {
            valueFormatter: ['1', '2', '3', '4', '5']
          },
          drawGridLines: false,
          axisMaximum: maxPointStep,
          axisMinimum: 0,
          textSize: normalize(14)
        }
      }
    });
  }

  render() {
    return (
      <View style={widgets.mainBrainwaveView}>
        <HeaderMain style={{ flex: 1 }} hideTopHeader='true' refreshSleepCallback={this.onRefresh} />
        <View style={{ flex: 10 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh}
                tintColor='white'
                title={I18n.t('LOADING')}
                titleColor="white"
                colors={['rgb(233, 186, 0)']}
                progressBackgroundColor='white' />
            }>
            <View style={containers.activityChartAndInfoCon}>
              <View style={{ marginTop: 5, marginBottom: 5, flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={text.textTitle}>{I18n.t('SLEEP_WALK_ANALYSIS')}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                zIndex: 20,
                height: 30
              }}>
                {
                  this.state.isDateVisible && this.state.isLeftArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={ this.previousDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToPreviousInterval()
                    }, 100);
                  }
                  }>{'<'}</Text></View>
                }
                {
                  this.state.isDateVisible &&
                  <Text style={[widgets.commonFont, { textAlign: 'center', flex: 1}]}>{this.state.dateSet}</Text>
                }
                {
                  this.state.isDateVisible && this.state.isRightArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={this.nextDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToNextInterval()
                    }, 100);
                  }
                  }>{'>'}</Text></View>
                }
              </View>

              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={[widgets.hoursStepsText, { textAlign: 'left' }]}> {I18n.t('HOURS')} </Text>
                <Text style={[widgets.hoursStepsText, { textAlign: 'right' }]}> {I18n.t('STEPS')} </Text>
              </View>
              <CombinedChart
                style={widgets.activityBarChart}
                data={this.state.data}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                animation={{
                  durationX: 0
                }}
                maxVisibleValueCount={1}
                touchEnabled={true}
                legend={this.state.legend}
                marker={this.state.marker}
                highlights={this.state.highlights}
                gridBackgroundColor={processColor('#ffffff')}
                drawBarShadow={false}
                drawValueAboveBar={true}
                autoScaleMinMaxEnabled={true}
                doubleTapToZoomEnabled={false}
                scaleEnabled={false}
                zoom={this.state.zoom}
                chartDescription={{ text: '' }}
                chartDescriptionFontSize={18}
                dragEnabled={this.state.dragEnabled}
                drawBorders={false}
                config={this.state.config}
                drawHighlightArrow={false}
                onSelect={this.handleSelect.bind(this)}
                ref="chart" />

              {
                this.state.isNoDataAvailable &&
                <Text style={widgets.textNoData}>{I18n.t('NO_DATA_YET')}</Text>
              }

              <SetTargetModal maxLength={5} buttonTitle={I18n.t('SET_TARGET')} inputChange={txt => this.onChangeTarget(txt)} closeModal={() => {
                this.setState({ modalVisible: false });
              }} isModalVisible={this.state.modalVisible} onSetTarget={() => {
                this.setState({ targetText: changedTarget, lineColor: changedColor, targetSleepHours: changedHours });
                this.props.updateSettingsModule({ prop: 'targetSleepHours', value: changedHours });
                this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
                  this.updateLineChartValues(changedHours), changedColor, this.state.data.barData.config);

                const tHour = changedTarget.substring(0, changedTarget.indexOf(' '));
                console.log('tHour: ' + tHour);

                if (tHour > this.state.yAxis.left.axisMaximum) {
                  this.updateYAxisState(parseInt(tHour) + (Platform.OS === 'android' ? 10 : 10), this.state.yAxis.right.axisMaximum);
                } else {
                  this.updateYAxisState(dailyHighestStep + (Platform.OS === 'android' ? 100 : 150), this.state.yAxis.right.axisMaximum);
                } 10

                this.setState({ modalVisible: false });
              }} />


              <View style={widgets.lowerSectionBrainwave}>
                {
                  !this.state.isNoDataAvailable &&
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 20 }}>
                    <View style={[widgets.legendMarker, { backgroundColor: '#77b1c2' }]} />
                    <Text style={{ marginLeft: 5, fontSize: normalize(14) }}>{I18n.t('SLEEP')}</Text>
                    <View style={[widgets.legendMarker, { backgroundColor: '#d3bdeb', marginLeft: 20 }]} />
                    <Text style={{ marginLeft: 5, fontSize: normalize(14) }}>{I18n.t('ACTIVITY')}</Text>
                  </View>
                }
                {
                  this.state.isTrophyVisible &&
                  <Target onPressAdd={() => this.addHours()}
                    onPressSubtract={() => this.subtractHours()}
                    targetVal={this.state.targetText} />
                }
                {
                  !this.state.isTrophyVisible &&
                  <View style={{ height: 8 }} />
                }
              </View>
            </View>

            <View style={{ display: this.state.display }}>
              <TouchableOpacity onPress={() => {
                if (this.state.hypnogramData.barData.dataSets[0].values.length > 0) {
                  this.setState({ modalHypnogramVisible: true });
                } else {
                  Alert.alert('', I18n.t('NO_HYPNOGRAM_DATA'));
                }
              }}>
                <Text style={[text.targetText, text.viewHynogramStyle]}>{I18n.t('VIEW_HYPNOGRAM')}</Text>
              </TouchableOpacity>
            </View>

            <View style={containers.viewStyle}>

              <Card containerStyle={{ padding: 10, paddingBottom: 5, marginTop: height <= 640 ? 10 : 14 }}>
                <View style={widgets.flexRow}>
                  <Text style={{ flex: 1, textAlign: 'left', fontSize: normalize(14) }}>{I18n.t('TOTAL_SLEEP')}</Text>
                  <Text style={{ textAlign: 'right', fontSize: normalize(14) }}>{this.state.totalSleep}</Text>
                </View>
              </Card>

              <Card containerStyle={{ padding: 10, paddingBottom: 5, marginTop: height <= 640 ? 0 : 14 }}>
                <View style={widgets.flexRow}>
                  <Text style={{ flex: 1, textAlign: 'left', fontSize: normalize(14) }}>{I18n.t('SLEEP_LATENCY')}</Text>
                  <Text style={{ textAlign: 'right', fontSize: normalize(14) }}>{this.state.sleepLatency}</Text>
                </View>
              </Card>

              <Card containerStyle={{ padding: 10, paddingBottom: 5, marginBottom: 10, marginTop: height <= 640 ? 0 : 14 }}>
                <View style={widgets.flexRow}>
                  <Text style={{ flex: 1, textAlign: 'left', fontSize: normalize(14) }}>{I18n.t('WAKE_ONSET')}</Text>
                  <Text style={{ textAlign: 'right', fontSize: normalize(14) }}>{this.state.wakeAfterSleepOnset}</Text>
                </View>
              </Card>
            </View>
          </ScrollView>
        </View>
        <ChartDuration
          onPressDaily={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressDaily();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressWeekly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.setState({ changeWeeklyLabel: true });
              this.onPressWeekly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressMonthly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressMonthly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressYearly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressYearly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          style={{ flex: 1 }} />


        <Modal
          animationIn='zoomIn'
          animationOut='zoomOut'
          backdropColor='transparent'
          onBackButtonPress={() => {
            this.setState({ progressModal: false });
          }}
          isVisible={this.state.progressModal}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
          </View>
        </Modal>

        <Modal
          animationType="slide"
          style={widgets.modalHypnogram}
          visible={this.state.modalHypnogramVisible}
          presentation
          onRequestClose={() => { this.setState({ modalHypnogramVisible: false }) }}>
          <View style={{ alignItems: 'flex-end', marginTop: 30, marginRight: 10 }}>
            <TouchableOpacity onPress={() => { { this.setState({ modalHypnogramVisible: false }) } }}>
              <Image source={require('../../images/x_24f.png')} style={widgets.addFoodModuleCloseIcon} />
            </TouchableOpacity>
          </View>

          <Text style={{ fontWeight: 'bold', fontSize: 20, marginBottom: 20, textAlign: 'center' }}> {I18n.t('HYPNOGRAM')} </Text>
          <View style={widgets.commonFlexRowStyle}>
            <View style={widgets.yAxisGraphScale}>
              <View style={{ flexDirection: 'row' }}>
                <View style={[widgets.legendMarker, { backgroundColor: '#5897a7' }]} />
                <Text> {I18n.t('WAKE')}</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View style={[widgets.legendMarker, { backgroundColor: '#77b1c2' }]} />
                <Text> {I18n.t('REM')}</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View style={[widgets.legendMarker, { backgroundColor: '#d3bdeb' }]} />
                <Text> {I18n.t('LIGHT_2')}</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View style={[widgets.legendMarker, { backgroundColor: '#d3bdeb' }]} />
                <Text> {I18n.t('LIGHT_1')}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                paddingBottom: '125%',
                ...Platform.select({
                  ios: {
                    paddingBottom: '160%',
                  }
                }),
              }}>
                <View style={[widgets.legendMarker, { backgroundColor: '#e3d0fc' }]} />
                <Text> {I18n.t('DEEP')}</Text>
              </View>
            </View>
            <View style={widgets.hypnoChartView}>
              <CombinedChart
                style={widgets.hypnogramChart}
                data={this.state.hypnogramData}
                xAxis={this.state.xAxisHypno}
                yAxis={this.state.yAxisHypno}
                animation={{
                  durationX: 0
                }}
                touchEnabled={true}
                legend={this.state.legend}
                marker={this.state.marker}
                gridBackgroundColor={processColor('#ffffff')}
                drawBarShadow={false}
                drawValueAboveBar={true}
                autoScaleMinMaxEnabled={true}
                doubleTapToZoomEnabled={false}
                scaleEnabled={true}
                zoom={this.state.zoomHypno}
                chartDescription={{ text: '' }}
                chartDescriptionFontSize={18}
                dragEnabled={true}
                drawBorders={false}
                config={this.state.hypnoConfig}
                drawHighlightArrow={true}
                onSelect={this.handleSelect.bind(this)} />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default connect(mapStateToProps, { updateSettingsModule })(SleepAnalysis);
