import React, { Component } from 'react';
import HeaderMain from '../common/HeaderMain';
import * as DateUtil from '../../utils/DateUtils';
import getCurrentDate from '../../utils/DateUtils';
import I18n from '../../translate/i18n/i18n';
import { TARGET_LINE_COLOR } from '../../styles/colors';
import Modal from 'react-native-modal';
import { COLOR_THUMB_TINT } from '../../styles/colors';
import { showToast } from '../../utils/CommonMethods';
import * as Progress from 'react-native-progress';
import { savePreference } from '../../utils/CommonMethods';

import {
  View,
  ScrollView,
  processColor,
  Text,
  AppState,
  Platform,
  RefreshControl,
  PermissionsAndroid,
  Dimensions
} from 'react-native';
import { Card } from 'react-native-elements';
import { widgets } from '../../styles/widgets';
import { containers } from '../../styles/containers';
import { text } from '../../styles/text';
import { CombinedChart } from 'react-native-charts-wrapper';
import { ChartUtils } from './../../utils/ChartUtils';
import ChartDuration from '../common/ChartDuration';
import Target from '../common/Target';
import SetTargetModal from '../common/SetTargetModal';
import { updateSettingsModule } from '../../redux/actions';
import { connect } from 'react-redux';
import normalize from '../../utils/GetPixelSizeForLayoutSize';
const { height } = Dimensions.get('window');

const LINE_CHART_VALUES = [0, 0, 0];
let changedColor = 'transparent';
let changedTarget = 0;
let changedHours = 0;
let dailyHighestStep = 0;

let binaural = [0];
let targetBinauralHours = [0];
let timeIntervals = [0];
let totalBinaural = [0];
let sleepOnsetLatency = [0];
let wakeOnsets = [0];
let enableTarget = true;

const BAR_DATA_GAP = 0.2;

let weeklySleepData = [0];
let weeklyWakeOnsets = [0];
let weeklyTimeIntervals = [0];
let weeklyTotalSleep = [0];
let weeklySleepLatency = [0];
let weeklyBinaural = [0];

let monthlySleep = [0];
let monthlyWakeOnsets = [0];
let monthlyTimeIntervals = [0];
let monthlyTotalSleep = [0];
let monthlyBinaural = [0];

let yearlySleep = [0];
let yearlyWakeOnsets = [0];
let yearlyTimeIntervals = [0];
let yearlySleepLatency = [0];
let yearlyBinaural = [0];
let dailyHighestHour = 0;
let dailyHighestBinaural = 0;

var activityTimer;
let initConnectTime = '';

const uuidv4 = require('uuid/v4');
const ACTIVITY = I18n.t('ACTIVITY');
const SLEEP = I18n.t('SLEEP');
const BINAURAL = I18n.t('BINAURAL');

const SLEEP_DATA = [];
const ACTIVITY_DATA = [];

const RESPONSE = [];
const COLORS = [];
let hypnoColor = processColor('');

const configWithGroupSpace = {
  barWidth: 0.4,
  group: {
    fromX: 0.5,
    groupSpace: BAR_DATA_GAP,
    barSpace: 0
  }
};

const configNoGroupSpace = {
  barWidth: 0.4
};

const defaultMarker = {
  enabled: true,
  markerColor: processColor('transparent'),
  textColor: processColor('#000000'),
  markerFontSize: 8
};

const DAYS_OF_THE_MONTH = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
  '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

const BURNED_CALORIES = 'Burned calories';
const ACTIVITY_TIME = I18n.t('TIME');

const { getMonthNames, getMonthComplete, convertDateToYYYYMMDD } = DateUtil;
const COUCHBASE_CRED = require('../../config/demo_couchbase_cred.json');

const mapStateToProps = (state) => {
  const { dateAndDocumentMap, targetBinauralHours, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins, latestTotalBinauralBeat, latestSleepLatency, latestWakeOnset, sMBbState } = state.settingsModule;
  console.log('123watch-- dateAndDocumentMap ' + JSON.stringify(dateAndDocumentMap));

  // This is how we put a value to initConnectTime. initConnectTime should be initialized if its current value is empty
  // and if the dateSmartBandConnected is not empty. this variable will be used to compare the current time to the
  // first time the smartband has been connected to the app.
  if (typeof dateAndDocumentMap !== 'undefined' && !isSmartBandEnabled) {
    initConnectTime = dateAndDocumentMap.time;
    console.log('123redux! state changed: connected!');
  }

  console.log('123redux! isSmartBandEnabled: ' + isSmartBandEnabled);

  console.log('123redux** deductedMinsProps: ' + deductedMins + '\tminsInADay: ' + minsInADay);

  return {
    dateAndDocumentMap, targetBinauralHours, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins, latestTotalBinauralBeat, latestSleepLatency, latestWakeOnset, sMBbState
  };
};

class BinauralBeat extends Component {

  addHours() {
    let sleepData = typeof this.props.targetBinauralHours === 'undefined' ? 0 : this.props.targetBinauralHours;

    if (sleepData < 24) {
      console.log('123redux: sleepData: ' + sleepData);
      let hoursLabel = '';

      sleepData += 1;
      hoursLabel = sleepData <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');

      const target = sleepData + hoursLabel;
      const color = sleepData === 0 ? 'transparent' : TARGET_LINE_COLOR;
      this.setState({ targetText: target, lineColor: color });

      this.props.updateSettingsModule({ prop: 'targetBinauralHours', value: sleepData });
      savePreference('targetBinauralHours', JSON.stringify(sleepData));
      this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values, this.updateLineChartValues(sleepData), color, this.state.data.barData.config);

      if (sleepData > this.state.yAxis.left.axisMaximum) {
        this.updateYAxisState(sleepData + (Platform.OS === 'android' ? 10 : 10));
      }
    }
  }

  componentDidMount() {
    console.log('123redux: rows: --------Component Did Mount ------- ');
    showToast(I18n.t('SWIPE_DOWN_REFRESH'));
    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(res => {
        if (res) {
          console.log('Permission is OK');
          // todo: confirm with team/PM about what we're gonna do with an unconfirmed permission.
        } else {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }

    const hoursLabel = this.props.targetBinauralHours <= 1 ? I18n.t('HOUR') : I18n.t('HOURS');
    this.setState({
      targetText: (typeof this.props.targetBinauralHours === 'undefined' || this.props.targetBinauralHours < 1) ?
        I18n.t('SET_TARGET') : this.props.targetBinauralHours + ' ' + hoursLabel
    });

    console.log('123redux: dateAndDocumentMap: ' + JSON.stringify(this.props.dateAndDocumentMap));

    const currentDate = getCurrentDate();
    const dateNow = new Date(currentDate);
    console.log('123redux! isSmartBandEnabled: ' + this.props.isSmartBandEnabled);

    this.getDatabase();
  }

  getDatabase() {
    manager.database.get_db({ db: DB_NAME })
      .then(res => {
        this.queryBinauralDocuments();
        this.querySleepDocuments();
      });
  }

  querySleepDocuments() {
    this.setupSleepViewAndQuery().then(r => {
      console.log('123binaural querySleepDocs res: ' + JSON.stringify(r.obj.rows));
      this.setState({ sleepRows: r.obj.rows });
    }).catch(err => {
      console.log('123beat sleep err: ' + JSON.stringify(err));
    });
  }

  queryBinauralDocuments() {
    this.setupBinauralViewAndQuery().then(r => {
      console.log('123binaural queryBinauralDocs res: ' + JSON.stringify(r.obj.rows));
      let binauralArrData = r.obj.rows;
      let binauralMapData = new Map();

      binauralArrData.sort(function(a, b) {
        return new Date(b.doc.timestamp) - new Date(a.doc.timestamp);
      });

      console.log('queryBinauralDocuments sorted: ' + JSON.stringify(binauralArrData));

      for (let binData of binauralArrData) {
        binauralMapData.set(convertDateToYYYYMMDD(binData.doc.timestamp), binData);
      } 

      this.setState({ binauralRows: Array.from(binauralMapData.values()) });

      console.log('queryBinauralDocuments unique values: ' + JSON.stringify(Array.from(binauralMapData.values())));

      setTimeout(() => {
        this.renderDailyChart();
        this.setState({ isRefreshing: false });
      }, 5);
    }).catch(err => {
      console.log('123beat binaural err: ' + JSON.stringify(err));
    });
  }

  setupBinauralViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'binauralByDocId',
      include_docs: true
    });
  }

  setupSleepViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'sleepByDocId',
      include_docs: true
    });
  }

  componentWillMount() {
    console.log('123redux: rows: --------Component Will Mount ------- ');
    console.log('123redux: rows: ' + JSON.stringify(this.state.binauralRows));
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });
    console.log('123redux: chosenYear: ' + this.state.chosenYear);
  }

  getActualDaysInAMonth() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    // to get the actual days in the month, we need to also check if the graph is showing the current/previous month.
    const monthToUse = this.state.chosenMonth === month ? month + 1 : this.state.chosenMonth + 1;
    const yearToUse = this.state.chosenYear === year ? year : this.state.chosenYear;
    return this.getDaysInMonth(monthToUse, yearToUse);
  }


  componentWillUnmount() {
    console.log('123redux: rows: --------Component WillUnmount ------- ');
    if (typeof this.feed !== 'undefined') {
      this.feed.stop();
    }
    COLORS = [];
    clearInterval(this.activityTimer);
  }

  constructor() {
    super();

    this.state = {
      sleepRows: {},
      binauralRows: {},
      progressModal: false,
      display: 'flex',
      highlights: [{ x: -1, y: 0, stackIndex: 27, dataIndex: 0 }],
      dragEnabled: true,
      changeWeeklyLabel: false,
      // we set the default earliest year to the current year.
      earliestYear: new Date().getFullYear(),
      nextDateTextColor: 'rgb(127, 127, 127)',
      previousDateTextColor: 'rgb(127, 127, 127)',
      modalHypnogramVisible: false,
      lineColor: 'transparent',
      isNoDataAvailable: false,
      chosenYear: 0,
      chosenMonth: 0,
      selectedInterval: '',
      daysInMonth: 0,
      dailyActivities: [],
      totalBinaural: '0h 0m',
      sleepLatency: '0h 0m',
      wakeAfterSleepOnset: '0h 0m',
      lastDocumentStored: {},
      targetText: I18n.t('SET_TARGET'),
      modalVisible: false,
      targetBinauralHours: 0,
      isTrophyVisible: true,
      isRefreshing: false,
      binauralData: {},
      scanning: false,
      appState: '',
      wakeOnset: 0,
      isDateVisible: true,
      leftArrow: '<',
      rightArrow: '>',
      isRightArrowVisible: true,
      isLeftArrowVisible: true,
      marker: defaultMarker,
      dateSet: this.getCurrentDate(),
      legend: {
        enabled: true,
        textSize: 0,
        textColor: processColor('transparent'),
        form: 'SQUARE',
        formSize: 0,
        xEntrySpace: 0,
        yEntrySpace: 0,
        formToTextSpace: 0,
        wordWrapEnabled: true,
        maxSizePercent: 0
      },
      data: {
        barData: {
          dataSets: [
            {
              values: SLEEP_DATA,
              label: I18n.t('BINAURAL'),
              colors: [
                processColor('#77b1c2'), processColor('#d3bdeb')
              ],
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: normalize(14),
              config: {
                textSize: normalize(14),
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }, {
              // 1hr, 2hrs, 3hrs, 4hrs, 8hrs, 6hrs
              values: ACTIVITY_DATA,
              label: I18n.t('SLEEP'),
              config: {
                textSize: normalize(14),
                drawValues: false,
                colors: [processColor('#d3bdeb')]
              }
            }
          ],
          config: configWithGroupSpace
        },
        lineData: {
          dataSets: [
            {
              label: '',
              values: LINE_CHART_VALUES,
              config: {
                colors: [processColor('grey')],
                drawValues: false,
                valueTextSize: 18,
                valueFormatter: '#',
                mode: 'CUBIC_BEZIER',
                drawCircles: false,
                lineWidth: 2,
                drawFilled: false,
                group: {
                  fromX: 0,
                  groupSpace: 0,
                  barSpace: 15
                },
                dashedLine: {
                  lineLength: 20,
                  spaceLength: 20
                }
              }
            }
          ]
        }
      },
      yAxis: {
        left: {
          drawGridLines: false,
          textSize: normalize(14),
          axisMinimum: 0,
          axisMaximum: 24
          /*
          **  note: to avoid the y-coordinate from changing its left side grid values whenever scrolling the graph to the right (and vice-versa)
          **  and having this weird "wobbling" effect, always set an axisMaximum. The
          **  axisMaximum limits the highest value to be displayed on the left side of the
          **  y-axis of the graph.

          **  What I usually use as the value for the axisMaximum is
          **  the highest value in the Activity array and add it up with a hundred The 100
          **  I add is just for aesthetics, so the highest value for Activity wouldn't
          **  reach the ceiling of the graph. i.e. [100, 800, 950, 400, 370] // the
          **  axisMaximum would be 950 + 100 = 1050.
          */
        },
        right: {
          enabled: false,
          drawGridLines: false
        }
      },
      xAxis: {
        valueFormatter: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
        granularityEnabled: true,
        textSize: 15,
        granularity: 1,
        axisMaximum: 31,
        avoidFirstLastClipping: true,
        axisMinimum: 0,
        position: 'BOTTOM'

      },
      zoom: {
        xValue: 0,
        yValue: 0,
        scaleX: 4,
        scaleY: 0
      }
    };
    console.log('123redux! Constructor ');
  }

  filterWakeOnsetsByYear(dataset) {
    const wakeOnsetsArr = [];

    for (let i = 0; i < dataset.length; i++) {
      if (typeof dataset[i] !== 'undefined') {
        const extractedYear = parseInt(dataset[i].date.substring(0, 4));
        if (extractedYear === this.state.chosenYear) {
          wakeOnsetsArr[i] = dataset[i];
        }
      }
    }

    // we remove all undefined elements in wakeOnsetsArr
    for (let i = 0; i < wakeOnsetsArr.length; i++) {
      if (typeof wakeOnsetsArr[i] === 'undefined') {
        wakeOnsetsArr.splice(i, 1);
      }
    }

    return wakeOnsetsArr;
  }

  filterWakeOnsetsByMonthAndYear(dataset) {
    const wakeOnsetsArr = [];

    for (let i = 0; i < dataset.length; i++) {
      if (typeof dataset[i] !== 'undefined') {
        const extractedYear = parseInt(dataset[i].date.substring(0, 4));
        const hyphenIndex = dataset[i].date.indexOf('-');
        const extractedMonth = parseInt(dataset[i].date.substring(hyphenIndex + 1).substring(0, 2));

        if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
          wakeOnsetsArr[i] = dataset[i];
        }
      }
    }

    // we remove all undefined elements in wakeOnsetsArr
    for (let i = 0; i < wakeOnsetsArr.length; i++) {
      if (typeof wakeOnsetsArr[i] === 'undefined') {
        wakeOnsetsArr.splice(i, 1);
      }
    }

    return wakeOnsetsArr;
  }

  filterByChosenMonthAndYear() {
    console.log('bb-ops filterByChosenMonthAndYear: ' + JSON.stringify(this.state.binauralRows));

    // we need to sort the daily chart by the (chosen or current) month and year.
    // Note: chosenMonth and chosenYear are both initialized to the current month and current year in
    // componentDidUpdate().
    const sortedChartData = [];
    let earliestYear = this.state.earliestYear;

    console.log('123redux: rows from eeg', JSON.stringify(this.state.binauralRows));
    console.log('bb-ops sleepAnalysisTotal length: ' + this.state.binauralRows.length);
    for (let i = 0; i < this.state.binauralRows.length; i++) {
      console.log('bb-ops sleepAnalysisTotal val: ' + this.state.binauralRows[i].doc.sleep_analysis_total);
      // we skip docs with empty dates.
      if (this.state.binauralRows[i].doc.date === '' || typeof this.state.binauralRows[i].doc.date === 'undefined') {
        continue;
      }

      const extractedYear = parseInt(this.state.binauralRows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.binauralRows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.binauralRows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      console.log('123redux!: extractedYear: ' + extractedYear + '\textractedMonth: ' + extractedMonth);

      // Note we add 1 to chosenMonth here because chosenMonth is also used in monthNames array, and monthNames array starts with index 0, hence chosenMonth is by
      // default should start with 0 (for January) and end with 11 (for December).

      console.log('123redux!: this.state.chosenYear: ' + this.state.chosenYear + '\this.state.chosenMonth: ' + (this.state.chosenMonth + 1));
      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const rec = {
          _id: this.state.binauralRows[i].doc._id,
          date: this.state.binauralRows[i].doc.date,
          binaural_count: this.state.binauralRows[i].doc.binaural === null ? 0 : this.state.binauralRows[i].doc.binaural,
          user_id: this.state.binauralRows[i].doc.user_id,
          target_binaural: this.state.binauralRows[i].doc.target_binaural
        };

        sortedChartData[i] = rec;
      }
      console.log('bb-ops sortedChartData: ' + JSON.stringify(sortedChartData));
    }

    const sleepList = [];
    for (let i = 0; i < this.state.sleepRows.length; i++) {
      if (typeof this.state.sleepRows[i].doc.date === 'undefined' || this.state.sleepRows[i].doc.date === '') {
        continue;
      }

      const extractedYear = parseInt(this.state.sleepRows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.sleepRows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.sleepRows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const sleepObj = {
          sleep_count: this.state.sleepRows[i].doc.sleep_analysis_total === undefined ? 0 : this.state.sleepRows[i].doc.sleep_analysis_total,
          date: this.state.sleepRows[i].doc.date
        };

        sleepList.push(sleepObj);
      }
    }


    this.setState({ earliestYear: earliestYear });
    console.log('123yearmonth: earliestYear: ' + this.state.earliest);

    // we remove all undefined elements in allYearSleepData
    for (let i = sortedChartData.length; i > 0; i--) {
      if (typeof sortedChartData[i] === 'undefined') {
        console.log('123redux! sortedChartData is null or undefined!');
        sortedChartData.splice(i, 1);
      }
    }

    console.log('123binaural!: sortedChartData: ' + JSON.stringify(sortedChartData));
    console.log('123binaural!: sortedChartData: ' + JSON.stringify(sleepList));
    return [sortedChartData, sleepList];
  }

  getCurrentDate() {
    const strDate = new Date();
    console.log('MONTHS**', 'months: ' + getMonthComplete());
    return strDate.getDate() + '\n' + getMonthComplete()[strDate.getMonth()].toUpperCase() + ', ' + strDate.getFullYear();
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  getHighestValue(chartValues) {
    let highest = 0;

    if (typeof chartValues[0] !== 'undefined' && typeof chartValues[0].y !== 'undefined') {
      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i].y > highest) {
          highest = chartValues[i].y;
        }
      }
    } else {
      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i] > highest) {
          highest = chartValues[i];
        }
      }
    }

    return highest;
  }

  getSortedMonthlyData() {
    const sortedSleepChartData = [];
    const sortedBinauralChartData = [];
    for (let i = 0; i < this.state.binauralRows.length; i++) {
      // we skip docs with empty dates.
      if (this.state.binauralRows[i].doc.date === '' || typeof this.state.binauralRows[i].doc.date === 'undefined') {
        continue;
      }

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.binauralRows[i].doc.date.substring(0, 4));
      if (this.state.chosenYear === extractedYear) {
        const recBinaural = {
          _id: this.state.binauralRows[i].doc._id,
          date: this.state.binauralRows[i].doc.date,
          count: this.state.binauralRows[i].doc.binaural
        };

        sortedBinauralChartData[i] = recBinaural;
      }
    }

    for (let i = 0; i < this.state.sleepRows.length; i++) {
      if (this.state.sleepRows[i].doc.date === '' || typeof this.state.sleepRows[i].doc.date === 'undefined') {
        continue;
      }

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.sleepRows[i].doc.date.substring(0, 4));

      if (this.state.chosenYear === extractedYear) {
        const recSleep = {
          _id: this.state.sleepRows[i].doc._id,
          date: this.state.sleepRows[i].doc.date,
          count: this.state.sleepRows[i].doc.sleep_analysis_total
        };

        sortedSleepChartData[i] = recSleep;
      }
    }

    // we remove all undefined elements in allYearSleepData
    for (let i = 0; i < sortedSleepChartData.length; i++) {
      if (typeof sortedSleepChartData[i] === 'undefined') {
        sortedSleepChartData.splice(i, 1);
      }
      if (typeof sortedBinauralChartData[i] === 'undefined') {
        sortedBinauralChartData.splice(i, 1);
      }
    }

    // this is an array of all the sorted data for binaural-recBin analysis.
    const sortedChartData = [sortedBinauralChartData, sortedSleepChartData];

    return sortedChartData;
  }

  getWeekRange(limit) {
    var weeks = [];
    let arrCtr = 0;
    let lastRange = 0;

    for (let i = 0; i < limit; i++) {
      if (i === 0) {
        weeks[arrCtr] = '0';
        arrCtr++;
        continue;
      }

      if (i % 7 == 0) {
        weeks[arrCtr] = (i - 6) + '-' + i;
        arrCtr++;
        lastRange = i;
      }
    }

    weeks.push((limit - lastRange) > 1 ? (lastRange + 1) + "-" + limit : (lastRange + 1)) + '';
    return weeks;
  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
    }

    this.setState({ appState: nextAppState });
  }

  handleSelect(event) {
    const entry = event.nativeEvent;
    if (entry === null) {
      this.setState({
        ...this.state,
        selectedEntry: null
      });
    } else {
      this.setState({
        ...this.state,
        selectedEntry: JSON.stringify(entry)
      });

      console.log('123redux! entry.y: ' + JSON.stringify(entry.y));
      console.log('123redux! entry.x: ' + JSON.stringify(entry));
      if (typeof entry.x !== 'undefined' && typeof entry.y !== 'undefined') {
        const xVal = Math.round(entry.x);
        console.log('yAxisVals: xVal: ' + xVal + '\t length: ' + binaural.length);
        if (this.state.selectedInterval === I18n.t('DAILY') && typeof binaural[xVal] !== 'undefined') { // we show the day and month here
          // Total Binaural Hours

          if (xVal === new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
            this.setState({ isTrophyVisible: true });
          } else {
            this.setState({ isTrophyVisible: false });
          }

          let dateToShow = xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear;

          if (xVal === 0 || xVal > this.state.daysInMonth) {
            dateToShow = this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase();
          }

          this.setState({
            dateSet: dateToShow
          });

          this.calculateBinauralInfo(binaural[xVal].y * 60);
          const recBin = binaural[xVal].y;

          if (typeof targetBinauralHours[xVal] === 'undefined') {
            return;
          }

          this.props.updateSettingsModule({ prop: 'targetBinauralHours', value: targetBinauralHours[xVal] });

          if (xVal === new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
            this.setState({ isTrophyVisible: true });
          } else {
            this.setState({ isTrophyVisible: false });
          }

          console.log('123binauralcount: targetBinauralHours: ' + JSON.stringify(targetBinauralHours));

          let tHour = typeof targetBinauralHours[xVal] === 'undefined' || isNaN(targetBinauralHours[xVal]) ? 0 : targetBinauralHours[xVal];
          if (tHour === 0 && this.props.targetBinauralHours > 0 && (xVal >= new Date().getDate() &&
            this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear())) {
            tHour = this.props.targetBinauralHours;
            console.log('123xhighlight: equal!!!!');
          }
          const color = tHour === 0 ? 'transparent' : TARGET_LINE_COLOR;
          console.log('123binauralcount: tHour: ' + tHour + '\trecBin: ' + recBin);

          this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
            this.updateLineChartValues(tHour), color, this.state.data.barData.config);

        } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
          // Total Binaural Hours
          this.calculateBinauralInfo(weeklyBinaural[xVal - 1] * 60);

          if (typeof this.state.xAxis.valueFormatter[xVal] === 'undefined' || xVal === 0) {
            this.setState({
              dateSet: getMonthComplete()[this.state.chosenMonth].toUpperCase()
              + ', ' + this.state.chosenYear
            });
          } else {
            this.setState({
              dateSet: this.state.xAxis.valueFormatter[xVal] + '\n' + getMonthComplete()[this.state.chosenMonth]
            });
          }
        } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {
          // Total Binaural Hours
          this.calculateBinauralInfo(monthlyBinaural[xVal - 1] * 60);

          this.setState({
            dateSet: (typeof getMonthComplete()[xVal - 1] === 'undefined' ? I18n.t('JANUARY') : getMonthComplete()[xVal - 1]) + '\n' + this.state.chosenYear
          });
        } else {

          // Note: there's a bug with the graph wherein if you click a bar item, sometimes it returns the zero index,
          // even though it's not the right x-index. We're catching it here for the yearly interval. I haven't observe
          // it occuring in the other intervals, prolly because the way the yearly graph is rendered.
          if (xVal === 0 || typeof yearlySleep[xVal - 1] === 'undefined') {
            return;
          }

          // Note: yearlySleep is structured this way: [{year: 0, count: 100}] - only a sample structure
          // the reason this is structured this way is because of the ChartUtils.getYearlyBarYAxisValues()
          // I've made it reusable for the other developers to use for their records tasks.
          // Total Binaural Hours

          console.log('123actsleep: yearlyBinaural: ' + JSON.stringify(yearlyBinaural));

          if (yearlyBinaural[xVal - 1] !== undefined) {
            this.calculateBinauralInfo(yearlyBinaural[xVal - 1].count * 60);
          }

          this.setState({
            dateSet: this.state.xAxis.valueFormatter[xVal]
          });
        }
      }
    }
  }

  managewakeOnsetData(rate) {
    console.log('dafsasfafa: ', rate);
    this.setState({ wakeOnset: rate });
  }

  onChangeTarget(txt) {
    if (!isNaN(txt)) {
      let sleepData = txt === '' || parseInt(txt) < 0 ? 0 : parseInt(txt);
      if (sleepData === 0 && this.props.targetBinauralHours > 0) {
        sleepData = this.props.targetBinauralHours;
      }

      changedHours = txt === '' || parseInt(txt) < 0 ? 0 : parseInt(txt);

      let hoursLabel = '';
      hoursLabel = sleepData <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');

      const target = sleepData + hoursLabel;
      changedColor = sleepData === 0 ? 'transparent' : TARGET_LINE_COLOR;
      changedTarget = sleepData < 1 ? I18n.t('SET_TARGET') : target;
    }
  }

  updateLineChartValues(sleepData) {
    const lineChartValues = [];

    for (var i = 0; i < 35; i++) {
      lineChartValues[i] = sleepData;
    }

    return lineChartValues;
  }

  onSetTarget() {
    this.setState({ modalVisible: false });
  }

  onPressDaily() {
    console.log('123redux!: --------onPresDaily()--------');
    this.setState({ selectedInterval: I18n.t('DAILY'), dragEnabled: true, display: 'flex' });

    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getDate() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), isTrophyVisible: true });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase(), isTrophyVisible: false });
    }

    console.log('123month: B. month: ' + this.state.chosenMonth + '\tyear: ' + this.state.chosenYear);
    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isDateVisible: true, isRightArrowVisible: true, isLeftArrowVisible: true });
      this.renderDailyChart();
    }, 50);
  }

  onPressWeekly() {
    console.log('123redux!: --------onPressWeekly()--------');
    this.setState({ selectedInterval: I18n.t('WEEKLY'), isRightArrowVisible: true, isLeftArrowVisible: true, dragEnabled: false, display: 'none' });
    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });

      const year = new Date().getFullYear();
      const month = new Date().getMonth();
      const daysInMonth = this.getActualDaysInAMonth();
      const valueFormatter = [];
      this.setState({ daysInMonth: daysInMonth });
      const sleepData = [0];
      const binauralData = [0];

      const sortedChartData = this.filterByChosenMonthAndYear()[0];
      console.log('123binaural ======!: 1sortedChartData: ' + JSON.stringify(sortedChartData));

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
        for (let j = 0; j < sortedChartData.length; j++) {
          if (typeof sortedChartData[j] !== 'undefined') {
            const day = parseInt(sortedChartData[j].date.slice(-2));
            if ((day - 1) === i) {
              binauralData[i] = parseInt(sortedChartData[j].binaural_count) / 60;
            } else {
              if (typeof binauralData[i] === 'undefined') {
                binauralData[i] = 0;
              }
            }
          }
        }
        sleepData[i] = 0;
      }

      // hotfix: if there's no activity data, then we still need to compose the dailySleepData array
      if (sortedChartData.length === 0) {
        for (let i = 0; i <= daysInMonth; i++) {
          sleepData[i] = 0;
        }
      }

      const sleepChartData = this.filterByChosenMonthAndYear()[1];

      for (let i = 0; i < sleepChartData.length; i++) {
        for (let j = 0; j < sleepData.length; j++) {
          const sleepDate = parseInt(sleepChartData[i].date.slice(-2));
          if (j === sleepDate - 1) {
            sleepData[j] = sleepChartData[i].sleep_count / 60;
          }
        }
      }

      // todo: Implement a weekly graph view.
      sleepData.splice(0, 0, 0);
      binauralData.splice(0, 0, 0);
      const weeklySleepData = ChartUtils.getWeeklyBarYAxisValues(sleepData);

      const weeklyBinauralData = ChartUtils.getWeeklyBarYAxisValues(binauralData);
      weeklyBinaural = weeklyBinauralData;

      const weeklyBinauralArrObj = [];
      for (let i = 0; i < weeklyBinauralData.length; i++) {
        weeklyBinauralArrObj.push({ x: i, y: weeklyBinauralData[i], marker: weeklyBinauralData[i].toFixed(2) });
      }

      const weeklySleepArrObj = [];
      for (let i = 0; i < weeklySleepData.length; i++) {
        weeklySleepArrObj.push({ x: i, y: weeklySleepData[i], marker: weeklySleepData[i].toFixed(2) });
      }

      const highestHour = this.getHighestValue(weeklySleepData);
      const highestBinaural = this.getHighestValue(weeklyBinauralData);
      console.log('123binauralcount!: weekly highestHour: ' + highestHour);
      console.log('123binauralcount!: weekly highestBinaural: ' + highestHour);

      if (highestHour > 0 || highestBinaural > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      // check which is higher, whichever is higher will be used for the axisMaximum of yAxisState.
      this.updateYAxisState((highestHour > highestBinaural ? highestHour : highestBinaural) + (Platform.OS === 'android' ? 10 : 10));

      const weekRange = this.getWeekRange(this.state.daysInMonth);
      const dayOfTheMonth = new Date().getDate() / 7;
      let indexToUse = 0;

      console.log('weekly123: chosenYear: ' + this.state.chosenYear + '\tyear now: ' + new Date().getFullYear() + '\chosenMonth: ' + this.state.chosenMonth + '\tmonth now: '
        + new Date().getMonth());

      if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
        if (this.state.changeWeeklyLabel) {
          if (dayOfTheMonth <= 1) {
            this.setState({ dateSet: weekRange[1] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 0;
          } else if (dayOfTheMonth > 1 && dayOfTheMonth <= 2) {
            this.setState({ dateSet: weekRange[2] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 1;
          } else if (dayOfTheMonth > 2 && dayOfTheMonth <= 3) {
            this.setState({ dateSet: weekRange[3] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 2;
          } else if (dayOfTheMonth > 3 && dayOfTheMonth <= 4) {
            this.setState({ dateSet: weekRange[4] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 3;
          } else if (dayOfTheMonth > 4 && dayOfTheMonth <= 5) {
            this.setState({ dateSet: weekRange[5] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear() });
            indexToUse = 4;
          }
        }
      } else {
        this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
      }

      // update x-axis coordinates.
      this.updateXAxisState(weekRange, 5);
      this.updateDataSets(weeklyBinauralArrObj, weeklySleepArrObj, [0], 'transparent', configWithGroupSpace);

      if (weeklyBinauralData.length > 0 && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
        this.calculateBinauralInfo(weeklyBinauralData[indexToUse] * 60);
      }

      // update zoom level
      this.setZoomLevel(-5, 0, 1);
    }, 50);
  }

  calculateBinauralInfo(binauralData) {
    // Total Sleep Hours
    let recBin = binauralData;
    let recBinHr = Math.floor(parseInt(recBin / 60));
    let recBinMins = parseInt(recBin % 60);

    if (typeof recBinHr === 'undefined' || isNaN(recBinHr)) {
      recBinHr = 0;
    }

    if (typeof recBinMins === 'undefined' || isNaN(recBinMins)) {
      recBinMins = 0;
    }

    if (typeof recBin === 'undefined') {
      recBin = 0;
    }


    const recordedBinaural = (recBinHr > 0) ? recBinHr + ' ' + I18n.t('HR') + ' ' + recBinMins + ' ' + I18n.t('MIN') : recBinMins + ' ' + I18n.t('MINS');

    this.setBinauralInfo(recordedBinaural);
  }

  setBinauralInfo(recordedBinaural) {
    this.setState({
      totalBinaural: recordedBinaural
    });
  }

  onPressMonthly() {
    console.log('123redux!: --------onPressMonthly()--------');
    // this.setState({ selectedInterval: I18n.t('MONTHLY'), isRightArrowVisible: true, isLeftArrowVisible: true, dragEnabled: true });
    this.setState({
      selectedInterval: I18n.t('MONTHLY'), isRightArrowVisible: true, isLeftArrowVisible: true, dragEnabled: true, display: 'none'
    });

    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getFullYear() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
    }

    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });
      const sortedChartData = this.getSortedMonthlyData();
      // Note: sortedChartData is an array of the different arrays of chart data for binaural-recBin analysis,
      // in the following order:
      // sortedChartData = [sortedBinauralChartData, sortedSleepChartData];
      const sortedBinauralData = sortedChartData[0];
      const sortedSleepChartData = sortedChartData[1];

      monthlySleep = ChartUtils.getMonthlyBarYAxisValues(sortedSleepChartData);
      monthlyBinaural = ChartUtils.getMonthlyBarYAxisValues(sortedBinauralData);

      const monthlyBinauralpArrObj = [];
      for (let i = 0; i < monthlyBinaural.length; i++) {
        monthlyBinaural[i] /= 60;
        monthlyBinauralpArrObj.push({ x: i, y: monthlyBinaural[i], marker: monthlyBinaural[i].toFixed(2) });
      }

      const monthlySleepArrObj = [];
      for (let i = 0; i < monthlySleep.length; i++) {
        monthlySleep[i] /= 60;
        monthlySleepArrObj.push({ x: i, y: monthlySleep[i], marker: monthlySleep[i].toFixed(2) });
      }

      const highestHour = this.getHighestValue(monthlySleep);
      const highestBinaural = this.getHighestValue(monthlyBinaural);

      if (highestHour > 0 || highestBinaural > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      console.log('123redux!: highest: ' + highestHour);
      console.log('123redux!: current month ' + parseInt(new Date().getMonth()) + 1);
      console.log('123redux!: monthlyTotalSleep ' + monthlyTotalSleep[parseInt(new Date().getMonth())]);


      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState((highestHour > highestBinaural ? highestHour : highestBinaural) + (Platform.OS === 'android' ? 50 : 60));

      // update x-axis coordinates.
      this.updateXAxisState(getMonthNames(), 13);
      console.log('yAxisVals: ' + JSON.stringify(monthlySleep));
      console.log('123actsleep: monthlyBinauralpArrObj: ' + JSON.stringify(monthlyBinauralpArrObj) + '\tmonthlySleepArrObj: ' + JSON.stringify(monthlySleepArrObj));
      this.updateDataSets(monthlyBinauralpArrObj, monthlySleepArrObj, [0], 'transparent', configWithGroupSpace);

      if (monthlyBinaural[parseInt(new Date().getMonth())] !== 0) {
        console.log('123redux!: current month ' + parseInt(new Date().getMonth()) + 1);
        this.calculateBinauralInfo(monthlyBinaural[parseInt(new Date().getMonth())] * 60);
      } else {
        this.setState({
          totalBinaural: 0 + ' ' + I18n.t('MIN'),

        });
      }

      let latestMonth = 0;
      for (let i = monthlyBinaural.length; i > 0; i--) {
        if (monthlyBinaural[i] > 0) {
          latestMonth = i;
          break;
        }
      }

      // this means the chart in view right now is on the daily interval
      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5) {
        // update zoom level
        this.setZoomLevel(2, 0, latestMonth);
      } else {
        this.setZoomLevel(this.state.zoom.scaleX, 0, latestMonth);
      }

    }, 50);
  }

  onPressYearly() {
    console.log('123redux!: --------onPressYearly()--------');
    this.setState({
      selectedInterval: I18n.t('YEARLY'), isTrophyVisible: false,
      isRightArrowVisible: false, isLeftArrowVisible: false, display: 'none'
    });

    if (this.state.chosenYear === new Date().getFullYear()) {
      this.setState({ dateSet: new Date().getFullYear() });
    } else {
      this.setState({ dateSet: this.state.chosenYear });
    }

    setTimeout(() => {
      const allYearSleepData = [];
      const allYearBinauralData = [];

      for (let i = 0; i < this.state.sleepRows.length; i++) {
        const rec = {
          _id: this.state.sleepRows[i].doc._id,
          date: this.state.sleepRows[i].doc.date,
          count: this.state.sleepRows[i].doc.sleep_analysis_total
        };
        allYearSleepData[i] = rec;
      }

      for (let i = 0; i < this.state.binauralRows.length; i++) {
        const recBinaural = {
          _id: this.state.binauralRows[i].doc._id,
          date: this.state.binauralRows[i].doc.date,
          count: this.state.binauralRows[i].doc.binaural
        };
        allYearBinauralData[i] = recBinaural;
      }

      const yearlySleepData = ChartUtils.getYearlyBarYAxisValues(allYearSleepData);
      const yearlyBinauralData = ChartUtils.getYearlyBarYAxisValues(allYearBinauralData);
      yearlyBinaural = yearlyBinauralData;
      yearlySleep = yearlySleepData;

      const yearlyBinauralArrObj = [];
      for (let i = 0; i < yearlyBinaural.length; i++) {
        yearlyBinaural[i].count /= 60;
        yearlyBinauralArrObj.push({ x: i, y: yearlyBinaural[i].count, marker: yearlyBinaural[i].count.toFixed(2) });
      }

      const yearlySleepArrObj = [];
      for (let i = 0; i < yearlySleep.length; i++) {
        yearlySleep[i].count /= 60;
        yearlySleepArrObj.push({ x: i, y: yearlySleep[i].count, marker: yearlySleep[i].count.toFixed(2) });
      }

      let sleep = [];
      let years = [];
      let binaural = [];

      console.log('123actsleep: yearlySleepData: ' + JSON.stringify(yearlySleepData));

      for (let i = 0; i < yearlySleepData.length; i++) {
        sleep[i] = yearlySleepData[i].count;
        years[i] = (yearlySleepData[i].year) + '';
      }

      for (let i = 0; i < yearlyBinauralData.length; i++) {
        binaural[i] = yearlyBinauralData[i].count;
        years[i] = (yearlyBinauralData[i].year) + '';
      }

      years.splice(0, 0, '');

      const highestHour = this.getHighestValue(sleep);
      const highestBinaural = this.getHighestValue(binaural);

      if (highestHour > 0 || highestBinaural > 0) {
        this.setState({ isNoDataAvailable: false });
      } else {
        this.setState({ isNoDataAvailable: true });
      }

      console.log('123redux!: highest: ' + highestHour);
      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState((highestHour > highestBinaural ? highestHour : highestBinaural) + (Platform.OS === 'android' ? 100 : 150));


      // we're gonna manipulate the years array to make the x-coordinates appear longer in the chart 
      // thereby making the bar chart's width thinner. We need to check whether the years are not 0 so that
      // we won't be change the values of the years' elements.
      let xValues = years;

      if (years.length < 12) {
        // 12 here is an arbitrary value
        for (let i = 0; i < 12; i++) {
          if (typeof years[i] === 'undefined') {
            xValues[i] = '';
          }
        }
      }

      console.log('123redux!: xValues: ' + xValues);
      console.log('123redux!: current year: ' + new Date().getFullYear().toString());
      console.log('123redux!: yearlySleepData: ' + yearlySleepData[0]);
      console.log('123redux!: sleep: ' + sleep[0]);

      if (yearlyBinaural.length > 0) {
        // set default values for total recBin, recBin latency, wake onset
        // Total Binaural Hours
        const index = binaural.length - 1;
        this.calculateBinauralInfo(binaural[index] * 60);
      } else {
        this.setState({
          totalBinaural: 0 + ' ' + I18n.t('MIN')
        });
      }

      // update x-axis coordinates.
      this.updateXAxisState(xValues, xValues.length + 1);
      this.updateDataSets(yearlyBinauralArrObj, yearlySleepArrObj, [0], 'transparent', configWithGroupSpace);

      let latestYear = 0;
      for (let i = sleep.length; i > 0; i--) {
        if (sleep[i] !== '') {
          latestYear = i;
          break;
        }
      }

      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5 || this.state.zoom.scaleX === 2) {
        // update zoom level
        this.setZoomLevel(2, 0, 0);
      }


      if (latestYear < 7) {
        this.setState({ dragEnabled: false });
      }
      else {
        this.setState({ dragEnabled: true });
      }

    }, 50);
  }

  onRefresh = () => {
    this.setState({ isRefreshing: true });

    if (this.state.selectedInterval === I18n.t('DAILY')) {
      this.querySleepDocuments();
      this.queryBinauralDocuments();
    }
    else {
      setTimeout(() => {
        this.setState({ isRefreshing: false });
      }, 2000);
    }
  }

  nextDateStyle() {
    return {
      justifyContent: 'flex-end',
      fontSize: 26,
      color: this.state.nextDateTextColor,
      width: 25
    };
  }

  previousDateStyle() {
    return {
      fontSize: 26,
      justifyContent: 'flex-start',
      color: this.state.previousDateTextColor,
      width: 25
    };
  }

  renderDailyChart() {
    const daysInMonth = this.getActualDaysInAMonth();
    const valueFormatter = [];
    const sleepData = [];
    const binauralData = [];
    let xHighlight = 0;
    let yHighlight = 0;
    let targetBinaural = 0;

    this.setState({ daysInMonth: daysInMonth, marker: defaultMarker });

    const sortedChartData = this.filterByChosenMonthAndYear()[0];
    console.log('123binaural!: --------renderDailyChart--------');
    console.log('123redux!: daysInMonth: ' + daysInMonth);

    let shouldShowTargetLine = false;
    let greaterThanTarget = false;

    for (let i = 0; i <= daysInMonth; i++) {
      valueFormatter[i] = (i + 1) + '';
      console.log('123datesets: outer loop: ' + i);
      for (let j = 0; j < sortedChartData.length; j++) {
        if (typeof sortedChartData[j] !== 'undefined') {
          console.log('123datesets: inner loop: ' + j);
          const day = parseInt(sortedChartData[j].date.slice(-2));

          if ((day) === i) {
            const binauralCount = parseInt(sortedChartData[j].binaural_count) / 60;
            targetBinaural = (sortedChartData[j].target_binaural === null || sortedChartData[j].target_binaural === undefined) ? 0 : parseInt(sortedChartData[j].target_binaural);

            shouldShowTargetLine = targetBinaural === binauralCount
              && binauralCount > 0;

            greaterThanTarget = binauralCount > targetBinaural && targetBinaural > 0;
            console.log('123greatertarget: greaterThanTarget: ' + greaterThanTarget + '\tbinauralCount: ' + binauralCount + '\ttargetBinaural: ' + targetBinaural);
            console.log('123greatertarget: shouldShowTargetLine: ' + shouldShowTargetLine);

            binauralData[i] = {
              x: (i > 0 ? i - BAR_DATA_GAP : i), y: binauralCount,
              marker: shouldShowTargetLine ? '\uD83C\uDFC6' : greaterThanTarget ? '\u2757' : ''
            };

            if (shouldShowTargetLine || greaterThanTarget) {
              xHighlight = day - BAR_DATA_GAP;
              yHighlight = binauralData[i].y;
            }

            targetBinauralHours[i] = targetBinaural;
            sleepData[i] = {
              x: (i > 0 ? i + BAR_DATA_GAP : i), y: 0
            };

            wakeOnsets[i] = parseInt(sortedChartData[j].wake_onset);
            sleepOnsetLatency[i] = parseInt(sortedChartData[j].sleep_latency);

            if (day === new Date().getDate()) {
              this.props.updateSettingsModule({ prop: 'targetBinauralHours', value: targetBinaural });
              savePreference('targetBinauralHours', JSON.stringify(targetBinaural));
              const hoursLabel = targetBinaural <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');

              const target = targetBinaural + hoursLabel;
              const color = targetBinaural === 0 ? 'transparent' : TARGET_LINE_COLOR;
              this.setState({ targetText: targetBinaural < 1 ? I18n.t('SET_TARGET') : target, lineColor: color });
            }

          } else {
            if (typeof targetBinauralHours[i] === 'undefined') {
              targetBinauralHours[i] = 0;
            }

            if (typeof binauralData[i] === 'undefined') {
              binauralData[i] = { x: (i > 0 ? i - BAR_DATA_GAP : i), y: 0, marker: '' };
            }
            if (typeof sleepData[i] === 'undefined') {
              sleepData[i] = { x: (i > 0 ? i + BAR_DATA_GAP : i), y: 0, marker: '' };
            }
            if (typeof wakeOnsets[i] === 'undefined') {
              wakeOnsets[i] = 0;
            }
            if (typeof sleepOnsetLatency[i] === 'undefined') {
              sleepOnsetLatency[i] = 0;
            }
            if (typeof timeIntervals[i] === 'undefined') {
              timeIntervals[i] = 0;
            }
          }
        }
      }
    }

    // hotfix: if there's no activity data, then we still need to compose the dailySleepData array
    if (sortedChartData.length === 0) {
      for (let i = 0; i <= daysInMonth; i++) {
        sleepData[i] = {
          x: (i > 0 ? i + BAR_DATA_GAP : i), y: 0, marker: ''
        };
      }
    }

    const sleepChartData = this.filterByChosenMonthAndYear()[1];

    for (let i = 0; i < sleepChartData.length; i++) {
      for (let j = 0; j < sleepData.length; j++) {
        const sleepDate = parseInt(sleepChartData[i].date.slice(-2)) + 0.2;
        if (sleepData[j].x === sleepDate) {
          sleepData[j].y = sleepChartData[i].sleep_count / 60;
        }
      }
    }

    binaural = binauralData;
    console.log('123xz targetBinaural: ' + targetBinaural);
    console.log('123redux!: targetBinauralHours: ' + JSON.stringify(targetBinauralHours));
    console.log('123redux!: binauralData: ' + JSON.stringify(binauralData));

    // this adds a "0" element at the beginning of the valueFormatter array.
    valueFormatter.splice(0, 0, '0');

    if (valueFormatter.length > daysInMonth + 1) {
      // this is a workaround to ensure that the last day on the daily chart is clickable.
      valueFormatter.splice(valueFormatter.length - 1, 1);
    }

    this.setState({ dailyActivities: binauralData });

    const highestHour = this.getHighestValue(sleepData);
    const highestBinaural = this.getHighestValue(binauralData);
    dailyHighestHour = highestHour;
    dailyHighestBinaural = highestBinaural;

    if (highestHour > 0 || highestBinaural > 0) {
      this.setState({ isNoDataAvailable: false });
    } else {
      this.setState({ isNoDataAvailable: true });
    }

    console.log('123binaural!: highestHour: ' + highestHour);
    console.log('123binaural!: highestBinaural: ' + highestBinaural);
    // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
    // it won't reach the ceiling of the graph component.
    this.updateYAxisState(highestBinaural > highestHour ? (highestBinaural < 23 ? highestBinaural + 5 : highestBinaural) : highestHour + 2);
    this.updateXAxisState(valueFormatter, 31);

    if (this.props.targetBinauralHours > highestHour) {
      this.updateYAxisState(this.props.targetBinauralHours + (Platform.OS === 'android' ? 10 : 10));
    }

    console.log('123redux!: valueFormatter: ' + valueFormatter);

    const lineData = [0];
    let defVal = (typeof this.props.targetBinauralHours === 'undefined' || this.props.targetBinauralHours === null
      || isNaN(this.props.targetBinauralHours)) ? 0 : this.props.targetBinauralHours;

    // this resets the line chart data to zero once the chosen month or chosen year is not equal to the current month and current year.
    if (this.state.chosenMonth !== new Date().getMonth() || this.state.chosenYear !== new Date().getFullYear()) {
      defVal = 0;
    }

    for (let i = 0; i <= 32; i++) {
      lineData[i] = defVal;
    }

    const color = new Date().getMonth() !== this.state.chosenMonth || new Date().getFullYear() !== this.state.chosenYear ? 'transparent' :
      xHighlight > 0 && xHighlight + 0.2 === new Date().getDate() ? TARGET_LINE_COLOR : this.props.targetBinauralHours > 0 ? TARGET_LINE_COLOR : this.state.lineColor;
    this.updateDataSets(binauralData, sleepData, xHighlight + BAR_DATA_GAP === new Date().getDate() ? this.updateLineChartValues(targetBinaural) : lineData,
      color, configNoGroupSpace);

    console.log('bb-ops updateDataSets binauralData: ' + JSON.stringify(binauralData));
    console.log('bb-ops updateDataSets sleepData: ' + JSON.stringify(sleepData));

    let latestDay = 0;

    for (let i = binauralData.length; i > 0; i--) {
      if (typeof binauralData[i] !== 'undefined' && binauralData[i].y > 0) {
        latestDay = i;
        break;
      }
    }

    if (binauralData.length > 0) {
      const currentDay = new Date().getDate();
      // set default values for total recBin, recBin latency, wake onset
      // Total Sleep Hours
      let recBin = binauralData[currentDay].y;
      let recBinHr = Math.floor((parseInt(recBin) * 60) / 60);
      let recBinMins = parseInt(recBin * 60) % 60;

      // Sleep Latency
      let sleepLatency = sleepOnsetLatency[currentDay];
      let sleepLatencyHr = Math.floor((parseInt(sleepLatency) * 60) / 60);
      let sleepLatencyMins = parseInt(sleepLatency * 60) % 60;

      // Wake After Sleep Onset
      let wakeOnset = wakeOnsets[currentDay];
      let wakeOnsetHr = Math.floor((parseInt(wakeOnset) * 60) / 60);
      let wakeOnsetMins = parseInt(wakeOnset * 60) % 60;

      const recordedBinaural = (recBinHr > 0) ? recBinHr + ' hr ' + recBinMins + ' ' + I18n.t('MINS') : recBinMins + ' ' + I18n.t('MIN');
      const recordedSleepLatency = (sleepLatencyHr > 0) ? sleepLatencyHr + ' hr ' + sleepLatencyMins + ' ' + I18n.t('MINS') : sleepLatencyMins + ' ' + I18n.t('MIN');
      const recordedSleepOnset = (wakeOnsetHr > 0) ? wakeOnsetHr + ' hr ' + wakeOnsetMins + ' ' + I18n.t('MINS') : wakeOnsetMins + ' ' + I18n.t('MIN');

      this.setState({
        totalBinaural: recordedBinaural,
        sleepLatency: recordedSleepLatency,
        wakeAfterSleepOnset: recordedSleepOnset
      });

      this.props.updateSettingsModule({
        prop: 'latestTotalBinauralBeat', value: recordedBinaural
      });
    } else {
      this.setState({
        totalBinaural: 0 + ' ' + I18n.t('MIN'),
        sleepLatency: 0 + ' ' + I18n.t('MIN'),
        wakeAfterSleepOnset: 0 + ' ' + I18n.t('MIN')
      });
    }


    this.scaleChartForDailyInterval(latestDay);

    if (xHighlight > 0 && xHighlight + BAR_DATA_GAP === new Date().getDate()) {
      this.setState({ highlights: [{ x: xHighlight, y: yHighlight, dataIndex: 1 }] });
    }
  }

  scaleChartForDailyInterval(latestDay) {
    // if chart is in monthly view, then we need to re-adjust the scale to suit the daily duration view
    this.setZoomLevel(4, 0, latestDay);
  }

  setToPreviousInterval() {
    this.setState({ nextDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) - 1;
    let year = parseInt(this.state.chosenYear);

    console.log('123month: month: ' + month + "\tyear: " + year + '\tearliestYear: ' + this.state.earliestYear);

    if (month < 0) {
      if (year <= this.state.earliestYear) {
        this.setState({ previousDateTextColor: 'transparent' });
        return;
      }
      month = 11;
      year -= 1;
    }

    this.setState({ chosenMonth: month, chosenYear: year });

    // The reason we're setting this variable is because we need to ensure
    // that when the user presses the previous month, we need to maintain the
    // date text to show the month. Right now it's showing the weekly range.
    if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      this.setState({ changeWeeklyLabel: false });
    } else {
      this.setState({ changeWeeklyLabel: true });
    }

    // note the reason we are adding 1 here is because monthNames[] start with index 0
    // whereas in our document's date field, the month starts with 1 and ends with 12.
    // this.sortDatasetByMonths(month + 1);
    this.sortDatasetByIntervals();
    this.setState({ dateSet: year + '\n' + getMonthComplete()[month].toUpperCase(), isTrophyVisible: false });
  }


  setToNextInterval() {
    this.setState({ previousDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) + 1;
    let year = parseInt(this.state.chosenYear) + 1;

    console.log('123month: month: ' + month + '\tyear: ' + year);

    if (month > new Date().getMonth()) {
      if (year > new Date().getFullYear()) {
        // this is not allowed, we should hide the right arrow key
        this.setState({ nextDateTextColor: 'transparent' });
        return;
      } else {
        if (month > 11) {
          // we reset month to 0
          month = 0;
          this.setState({ chosenYear: year, chosenMonth: month, dateSet: year + '\n' + getMonthComplete()[month].toUpperCase() });
        } else {
          this.setState({ chosenYear: (year - 1), chosenMonth: month, dateSet: (year - 1) + '\n' + getMonthComplete()[month].toUpperCase() });
        }

        setTimeout(() => {
          this.sortDatasetByIntervals();
        }, 5);

        console.log('123month: A. month: ' + this.state.chosenMonth + '\tyear: ' + this.state.chosenYear);
        return;
      }
    } else {
      this.setState({ chosenMonth: month });

      // The reason we're setting this variable is because we need to ensure
      // that when the user presses the previous month, we need to maintain the
      // date text to show the month. Right now it's showing the weekly range.
      if (this.state.selectedInterval === I18n.t('WEEKLY')) {
        this.setState({ changeWeeklyLabel: false });
      } else {
        this.setState({ changeWeeklyLabel: true });
      }
      // note the reason we are adding 1 here is because monthNames[] start with index 0
      // whereas in our document's date field, the month starts with 1 and ends with 12.
      // this.sortDatasetByMonths(month + 1);
      // this.sortDatasetByIntervals();
      this.sortDatasetByIntervals();
    }

    console.log('123month: month: ' + this.state.chosenMonth + '\tyear: ' + this.state.chosenYear);

    this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[month].toUpperCase() });
  }

  setXValuesFormatter(month) {
    const year = this.state.chosenYear;
    const daysInMonth = this.getDaysInMonth(month + 1, year);
    const valueFormatter = [];

    if (this.state.selectedInterval === I18n.t('DAILY')) {

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
      }

      // this adds a "0" element at the beginning of the valueFormatter array.
      valueFormatter.splice(0, 0, '0');
      this.updateXAxisState(valueFormatter, 31);
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // update x-axis coordinates.
      this.updateXAxisState(this.getWeekRange(daysInMonth), 6);
    }
  }

  sortDatasetByIntervals() {
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      // we render the chart with the daily interval, thereby calling onPressDaily()
      this.onPressDaily();
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // we render the chart with the weekly interval, thereby calling onPressWeekly()
      this.onPressWeekly();
    } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {
      // we render the chart with the monthly interval, thereby calling onPressMonthly()
      this.onPressMonthly();
    } else if (this.state.selectedInterval === I18n.t('YEARLY')) {
      // we render the chart with the yearly interval, thereby calling onPressYearly()
      this.onPressYearly();
    }
  }

  setZoomLevel(zoomLevelX, zoomLevelY, moveToX) {
    this.setState({
      isMonthlyView: false,
      zoom: {
        xValue: moveToX,
        yValue: 0,
        scaleX: zoomLevelX,
        scaleY: zoomLevelY
      }
    });
  }

  sortDatasetByMonths(month) {
    let sortedDocs = [];

    for (let i = 0; i < this.state.binauralRows.length; i++) {
      const document = this.state.binauralRows[i];
      const hyphenIndex = document.date.indexOf('-');
      const extractedMonth = parseInt(document.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedMonth === month) {
        sortedDocs[i] = document;
      }
    }

    return sortedDocs;
  }

  subtractHours() {
    let sleepData = typeof this.props.targetBinauralHours === 'undefined' ? 0 : this.props.targetBinauralHours;

    let hoursLabel = '';

    if (sleepData > 0) {
      sleepData -= 1;
    }

    hoursLabel = sleepData <= 1 ? ' ' + I18n.t('HOUR') : ' ' + I18n.t('HOURS');

    const target = sleepData + hoursLabel;
    const color = sleepData === 0 ? 'transparent' : TARGET_LINE_COLOR;
    //this.setState({ targetText: target, lineColor: color });
    this.setState({ targetText: sleepData < 1 ? I18n.t('SET_TARGET') : target, lineColor: color });
    this.props.updateSettingsModule({ prop: 'targetBinauralHours', value: sleepData });
    savePreference('targetBinauralHours', JSON.stringify(sleepData));

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
      this.updateLineChartValues(sleepData), color, this.state.data.barData.config);
  }

  updateDataSets(binauralCount, sleepCount, lineChartValues, lineColor, config) {
    console.log('123binauralcount: binaural_count: ' + JSON.stringify(binauralCount) + '\tsleepCount: ' + JSON.stringify(sleepCount) + '\tlineChartValues: ' + JSON.stringify(lineChartValues));
    console.log('123binauralcount: config: ' + JSON.stringify(config));
    console.log('123binauralcount: color: ' + lineColor);
    this.setState({
      data: {
        barData: {
          dataSets: [
            {
              values: binauralCount,
              label: I18n.t('BINAURAL'),
              colors: [
                processColor('#77b1c2'), processColor('#d3bdeb')
              ],
              drawFilled: true,
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                axisDependency: 'left',
                textSize: 15,
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }, {
              // Note: for now we'll be making the y-axis values of Sleep Data the same as the Activity Data.
              values: sleepCount,
              label: I18n.t('SLEEP'),
              config: {
                axisDependency: 'right',
                drawValues: false,
                colors: [processColor('#d3bdeb')]
              }
            }
          ],
          config: config
        },
        lineData: {
          dataSets: [{
            values: lineChartValues,
            label: I18n.t('TARGET'),
            config: {
              colors: [processColor(lineColor)],
              // drawValues: true,
              drawValues: false,
              valueTextSize: 12,
              valueFormatter: '#',
              valueTextColor: processColor('grey'),
              mode: 'CUBIC_BEZIER',
              drawCircles: false,
              lineWidth: 2,
              drawFilled: false,
              group: {
                fromX: 0,
                groupSpace: 0,
                barSpace: 15
              },
              dashedLine: {
                lineLength: 20,
                spaceLength: Platform.OS === 'ios' ? 15 : 40
              }
            }
          }]
        }
      }
    });

    console.log('123redux done with updatedatasets...');
  }

  upsertDateAndDocMap(res, doc) {
    console.log('123watch-- not equal! RESPONSE!!: ' + JSON.stringify(res));
    const docId = res.obj.id;
    const rev = res.obj.rev;

    // timeConsumed field should be in minutes, i.e. 120 for 120 mins or 2 sleepData.

    // we store the docId, rev, and date of the document's creation in redux:
    const dateAndDocumentMap = {
      rev: rev,
      docId: docId,
      time: doc.time,
      binaural: doc,
      date: doc.date // note the date here is taken from the smartwatch.
    };

    this.props.updateSettingsModule({ prop: 'dateAndDocumentMap', value: dateAndDocumentMap });
  }

  updateXAxisState(valueFormatter, axisMaximum) {
    console.log('123redux! valueFormatter: ' + JSON.stringify(valueFormatter));
    this.setState({
      xAxis: {
        valueFormatter: valueFormatter,
        granularityEnabled: true,
        granularity: 1,
        drawGridLines: false,
        axisMinimum: 0,
        axisMaximum: axisMaximum + 1,
        textSize: 12,
        position: 'BOTTOM'
      }
    });
  }

  updateYAxisState(maxPointHour) {
    console.log('yAxisPoint: ' + maxPointHour);
    this.setState({
      yAxis: {
        left: {
          drawGridLines: true,
          gridLineWidth: 1,
          // granularityEnabled: true,
          // granularity: 5,
          // labelCountForce: false,
          axisMaximum: maxPointHour,
          axisMinimum: 0,
          textSize: normalize(14)
        },
        right: {
          enabled: false,
          $set: {
            valueFormatter: ['1', '2', '3', '4', '5']
          },
          drawGridLines: false,
          axisMaximum: maxPointHour,
          axisMinimum: 0,
          textSize: normalize(14)
        }
      }
    });
  }

  render() {
    return (
      <View style={widgets.mainBrainwaveView}>
        <HeaderMain style={{ flex: 1 }} hideTopHeader='true' refreshCallback={this.onRefresh} />
        <View style={{ flex: 10 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh}
                tintColor='white'
                title={I18n.t('LOADING')}
                titleColor="white"
                colors={['rgb(233, 186, 0)']}
                progressBackgroundColor='white' />
            }>
            <View style={containers.activityChartAndInfoCon}>
              <View style={{ marginTop: 5, marginBottom: 5, flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={text.textTitle}> {I18n.t('BINAURAL_SCREEN_TITLE')}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                zIndex: 20,
                height: 30
              }}>
                {
                  this.state.isDateVisible && this.state.isLeftArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={ this.previousDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToPreviousInterval()
                    }, 100);
                  }
                  }>{'<'}</Text></View>
                }
                {
                  this.state.isDateVisible &&
                  <Text style={[widgets.commonFont, { textAlign: 'center', flex: 1}]}>{this.state.dateSet}</Text>
                }
                {
                  this.state.isDateVisible && this.state.isRightArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={this.nextDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToNextInterval()
                    }, 100);
                  }
                  }>{'>'}</Text></View>
                }
              </View>

              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={[widgets.hoursStepsText, { textAlign: 'left' }]}> {I18n.t('HOURS')} </Text>
              </View>
              <CombinedChart
                style={widgets.activityBarChart}
                data={this.state.data}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                animation={{
                  durationX: 0
                }}
                touchEnabled={true}
                legend={this.state.legend}
                marker={this.state.marker}
                highlights={this.state.highlights}
                gridBackgroundColor={processColor('#ffffff')}
                drawBarShadow={false}
                drawValueAboveBar={true}
                autoScaleMinMaxEnabled={true}
                doubleTapToZoomEnabled={false}
                scaleEnabled={false}
                zoom={this.state.zoom}
                chartDescription={{ text: '' }}
                chartDescriptionFontSize={18}
                dragEnabled={this.state.dragEnabled}
                drawBorders={false}
                config={this.state.config}
                drawHighlightArrow={false}
                onSelect={this.handleSelect.bind(this)}
                ref="chart" />

              {
                this.state.isNoDataAvailable &&
                <Text style={widgets.textNoData}>{I18n.t('NO_DATA_YET')}</Text>
              }

              <SetTargetModal inputChange={txt => this.onChangeTarget(txt)} closeModal={() => {
                this.setState({ modalVisible: false });
              }} isModalVisible={this.state.modalVisible} maxLength={2} />

              <SetTargetModal maxLength={5} inputChange={txt => this.onChangeTarget(txt)} closeModal={() => {
                this.setState({ modalVisible: false });
              }} isModalVisible={this.state.modalVisible} onSetTarget={() => {
                this.setState({ targetText: changedTarget, lineColor: changedColor, targetBinauralHours: changedHours });
                this.props.updateSettingsModule({ prop: 'targetBinauralHours', value: changedHours });
                this.updateDataSets(this.state.data.barData.dataSets[0].values, this.state.data.barData.dataSets[1].values,
                  this.updateLineChartValues(changedHours), changedColor, this.state.data.barData.config);

                const tHour = changedTarget.substring(0, changedTarget.indexOf(' '));
                console.log('tHour: ' + tHour);

                if (tHour > this.state.yAxis.left.axisMaximum) {
                  this.updateYAxisState(parseInt(tHour) + (Platform.OS === 'android' ? 10 : 10));
                } else {
                  this.updateYAxisState((highestHour > highestBinaural ? highestHour : highestBinaural) + (Platform.OS === 'android' ? 10 : 10));
                } 10

                this.setState({ modalVisible: false });
              }} />


              <View style={widgets.lowerSectionBrainwave}>
                {
                  !this.state.isNoDataAvailable &&
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 20 }}>
                    <View style={[widgets.legendMarker, { backgroundColor: '#77b1c2' }]} />
                    <Text style={{ marginLeft: 5, fontSize: normalize(14) }}>{I18n.t('BINAURAL')}</Text>
                    <View style={[widgets.legendMarker, { backgroundColor: '#d3bdeb', marginLeft: 20 }]} />
                    <Text style={{ marginLeft: 5, fontSize: normalize(14) }}>{I18n.t('SLEEP')}</Text>
                  </View>
                }
                {
                  this.state.isTrophyVisible &&
                  <Target onPressAdd={() => this.addHours()}
                    onPressSubtract={() => this.subtractHours()}
                    targetVal={this.state.targetText} />
                }
                {
                  !this.state.isTrophyVisible &&
                  <View style={{ height: 8 }} />
                }
              </View>
            </View>

            <View style={containers.viewStyle}>

              <Card containerStyle={{ padding: 10, paddingBottom: 5, marginTop: height <= 640 ? 10 : 14 }}>
                <View style={[widgets.flexRow, { paddingBottom: 5 }]}>
                  <Text style={{ flex: 1, textAlign: 'left', fontSize: normalize(14) }}>{I18n.t('TOTAL_BINAURAL_BEAT_USAGE')}</Text>
                  <Text style={{ textAlign: 'right', fontSize: normalize(14) }}>{this.state.totalBinaural}</Text>
                </View>
              </Card>

            </View>
          </ScrollView>
        </View>
        <ChartDuration
          onPressDaily={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressDaily();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressWeekly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.setState({ changeWeeklyLabel: true });
              this.onPressWeekly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressMonthly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressMonthly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressYearly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressYearly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          style={{ flex: 1 }} />


        <Modal
          animationIn='zoomIn'
          animationOut='zoomOut'
          backdropColor='transparent'
          onBackButtonPress={() => {
            this.setState({ progressModal: false });
          }}
          isVisible={this.state.progressModal}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
          </View>
        </Modal>
      </View>
    );
  }
}

export default connect(mapStateToProps, { updateSettingsModule })(BinauralBeat);
