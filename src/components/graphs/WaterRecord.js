import React, { Component } from 'react';
import HeaderMain from '../common/HeaderMain';
import * as DateUtil from '../../utils/DateUtils';
import getCurrentDate from '../../utils/DateUtils';
import I18n from '../../translate/i18n/i18n';
import ReactNativeModal from 'react-native-modal';
import { COLOR_THUMB_TINT } from '../../styles/colors';
import { getPreference, showToast } from '../../utils/CommonMethods';

import {
  View,
  ScrollView,
  processColor,
  Text,
  Image,
  Modal,
  AppState,
  Platform,
  RefreshControl,
  PermissionsAndroid,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { Card, normalize } from 'react-native-elements';
import { widgets } from '../../styles/widgets';
import { containers } from '../../styles/containers';
import { text } from '../../styles/text';
import { CombinedChart } from 'react-native-charts-wrapper';
import ChartDuration from '../common/ChartDuration';
import { ChartUtils } from './../../utils/ChartUtils';
import { updateSettingsModule } from '../../redux/actions';
import { connect } from 'react-redux';
import { TARGET_LINE_COLOR } from '../../styles/colors';
import { PASSIVE_INTERVAL_COLOR } from '../../styles/colors';
import { CommonDialog } from './../common/CommonDialog'
import * as Progress from 'react-native-progress';
import RenderIf from '../RenderIf';

const { height } = Dimensions.get('window');


const LINE_CHART_VALUES = [
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0
];

var initialDataCalMargin;

switch (height) {
  case 812: {
    initialDataCalMargin = 0;
    break;
  }
  case 740:
  case 736: { //iphone 6-8+
    initialDataCalMargin = 0;
    break;
  }
  case 667: {//iphone6-8
    initialDataCalMargin = 0;
    break;
  }
  case 640: { //common android
    initialDataCalMargin = 0;
    break;
  }
  default: { //iphone5/SE else heigher
    if (height < 640) {
      initialDataCalMargin = 20;
    } else {
      initialDataCalMargin = 0;
    }
    break;
  }
}

let counter = 0;
const revs = [];
let user_id = '';
let activity = [0];
let targetGlassWater = [0];
let heartRates = [0];
let timeIntervals = [0];
let kcals = [0];
let enableTarget = true;

let weeklySteps = [0];
let weeklyHeartRates = [0];
let weeklyTimeIntervals = [0];
let weeklyKcals = [0];
let weeklyDividendsForBPM = [0];

let monthlySteps = [0];
let monthlyHeartRates = [0];
let monthlyTimeIntervals = [0];
let monthlyKcals = [0];

let yearlySteps = [0];
let yearlyHeartRates = [0];
let yearlyTimeIntervals = [0];
let yearlyKcals = [0];

var activityTimer;
let ACTIVITY_DATA = [];
let initConnectTime = '';

const uuidv4 = require('uuid/v4');
const ACTIVITY = I18n.t('ACTIVITY');
const SLEEP = I18n.t('SLEEP');

const SLEEP_DATA = [0];
const TARGET_WATER = [];
let daily_water_value = [];
let daily_water = [];
let justRefreshed = false;

let changedColor = 'transparent';
let changedTarget = 0;
let changedGlass = 0;

let glassHighest = 0;
const plusHighest = 50;

const defaultMarker = {
  enabled: true,
  markerColor: processColor('transparent'),
  textColor: processColor('#000000'),
  markerFontSize: 8
};

const invisibleMarker = {
  enabled: false
};


const { getFormattedTimestamp, getMonthNames, getMonthComplete } = DateUtil;
const COUCHBASE_CRED = require('../../config/demo_couchbase_cred.json');

const mapStateToProps = (state) => {
  const { dateAndDocumentMap, targetGlassWater, recordGlassWater, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins } = state.settingsModule;
  console.log('123watch-- dateAndDocumentMap ' + JSON.stringify(dateAndDocumentMap));

  // This is how we put a value to initConnectTime. initConnectTime should be initialized if its current value is empty
  // and if the dateSmartBandConnected is not empty. this variable will be used to compare the current time to the
  // first time the smartband has been connected to the app.
  if (typeof dateAndDocumentMap !== 'undefined' && !isSmartBandEnabled) {
    initConnectTime = dateAndDocumentMap.time;
    console.log('123redux! state changed: connected!');
  }

  return {
    dateAndDocumentMap, targetGlassWater, recordGlassWater, isSmartBandEnabled, dateSmartBandConnected, dateSmartBandDisconnected,
    connectedPeripheralId, minsInADay, deductedMins
  };
};

class WaterRecord extends Component {

  addGlass() {
    let glass = this.props.targetGlassWater;
    let glassLabel = '';

    if (glass < 20) {
      glass += 1;
    }

    glassLabel = glass <= 1 ? ' ' + I18n.t('GLASS_DAY') : ' ' + I18n.t('GLASSES_DAY');

    const target = glass + glassLabel;
    const color = glass === 0 ? 'transparent' : 'rgb(233, 186, 0)';
    this.setState({ targetGlassWater: target, lineColor: color });

    this.props.updateSettingsModule({ prop: 'targetGlassWater', value: glass });

    if (glass > glassHighest) {
      this.updateYAxisState(glass + plusHighest);
    }
    else {
      this.updateYAxisState(glassHighest + plusHighest);
    }

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.updateLineChartValues(glass), color);
  }

  componentDidMount() {
    showToast(I18n.t('SWIPE_DOWN_REFRESH'));
    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
    this.setState({ chosenMonth: new Date().getMonth(), chosenYear: new Date().getFullYear(), selectedInterval: I18n.t('DAILY') });


    getPreference('userId').then(res => {
      console.log('123redux: getPreference userId: ' + res);
      user_id = res;
    });

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(res => {
        if (res) {
          console.log('Permission is OK');
          // todo: confirm with team/PM about what we're gonna do with an unconfirmed permission.
        } else {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }
    const currentDate = getCurrentDate();
    const dateNow = new Date(currentDate);
    console.log('123redux! isSmartBandEnabled: ' + this.props.isSmartBandEnabled);
    // we need to clear dateAndDocumentMap if the date in it is not the same as the current date.

    this.getDatabase();

  }


  componentWillMount() {
    if (this.props.targetGlassWater === 0) {
      this.setState({
        setTargetDisplay: 'flex', setTargetVisible: 1,
        targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0
      });
    }
    else {
      this.setState({
        setTargetDisplay: 'none', setTargetVisible: 0,
        targetAddSubtractDisplay: 'flex', targetAddSubtractVisible: 1
      });
    }

    clearInterval(this.activityTimer);

  }

  constructor() {
    super();
    this.state = {
      disabled: false,
      progressModal: false,
      dragEnabled: true,
      isUpdateSuccessful: false,
      isSuccessful: false,
      daysModalInMonth: new Date(new Date().getFullYear(), parseInt(new Date().getMonth() + 1), 0).getDate(),
      monthChosen: parseInt(new Date().getMonth() + 1),
      chosenPrevMonth: this.getModalMonth(),
      isDateVisible: true,
      dateModalSet: this.getModalCurrentDate(),
      currentYear: this.getModalCurrentYear(),
      currentMonth: this.getModalCurrentMonth(),
      currentDay: this.getModalCurrentDay(),
      timeSet: '',
      highlights: [{ x: -1, y: 0, stackIndex: 27, dataIndex: 0 }],
      initialDataCal: I18n.t('NO_RECORD'),
      recordDisplay: 'flex',
      recordVisible: 1,
      recordUpdateDisplay: 'none',
      recordUpdateVisible: 0,
      setTargetDisplay: 'flex',
      setTargetVisible: 1,
      setDailyTargetDisplay: 'none',
      setDailyTargetVisible: 0,
      targetAddSubtractDisplay: 'none',
      targetAddSubtractVisible: 0,
      recordGlass: I18n.t('RECORD'),
      glassLabel: '',
      earliestYear: new Date().getFullYear(),
      nextDateTextColor: 'rgb(127, 127, 127)',
      previousDateTextColor: 'rgb(127, 127, 127)',
      lineColor: 'transparent',
      isNoDataAvailable: false,
      chosenYear: 0,
      chosenMonth: 0,
      selectedInterval: '',
      daysInMonth: 0,
      dailyActivities: [],
      rows: {},
      lastDocumentStored: {},
      targetText: I18n.t('SET_TARGET'),
      modalVisible: false,
      targetGlassWater: 0 + ' ' + I18n.t('GLASS_DAY'),
      isTrophyVisible: true,
      isRefreshing: false,
      activityData: {},
      scanning: false,
      appState: '',
      isDateVisible: true,
      isRightArrowVisible: true,
      isLeftArrowVisible: true,
      leftArrow: '<',
      rightArrow: '>',
      arrowLeft: '<',
      arrowRight: '>',
      marker: defaultMarker,
      dateSet: this.getCurrentDate(),
      legend: {
        enabled: true,
        textSize: 0,
        textColor: processColor('transparent'),
        form: 'SQUARE',
        formSize: 0,
        xEntrySpace: 0,
        yEntrySpace: 0,
        formToTextSpace: 0,
        wordWrapEnabled: true,
        maxSizePercent: 0
      },
      data: {
        barData: {
          dataSets: [
            {
              values: ACTIVITY_DATA,
              label: I18n.t('FOOD_RECORD'),
              colors: [
                processColor('#77b1c2')
              ],
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                textSize: 15,
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }],
        },
        lineData: {
          dataSets: [
            {
              label: '',
              values: LINE_CHART_VALUES,
              config: {
                colors: [processColor('grey')],
                drawValues: false,
                valueTextSize: 18,
                valueFormatter: '#',
                mode: 'CUBIC_BEZIER',
                drawCircles: false,
                lineWidth: 2,
                drawFilled: false,
                dashedLine: {
                  lineLength: 20,
                  spaceLength: 20
                }
              }
            }
          ]
        }
      },
      yAxis: {
        left: {
          drawGridLines: false,
          textSize: normalize(14),
          axisMinimum: 0,
          axisMaximum: 840
          /*
          **  note: to avoid the y-coordinate from changing its left side grid values whenever scrolling the graph to the right (and vice-versa)
          **  and having this weird "wobbling" effect, always set an axisMaximum. The
          **  axisMaximum limits the highest value to be displayed on the left side of the
          **  y-axis of the graph.
  
          **  What I usually use as the value for the axisMaximum is
          **  the highest value in the Activity array and add it up with a hundred The 100
          **  I add is just for aesthetics, so the highest value for Activity wouldn't
          **  reach the ceiling of the graph. i.e. [100, 800, 950, 400, 370] // the
          **  axisMaximum would be 950 + 100 = 1050.
          */
        },
        right: {
          enabled: false,
        }
      },
      xAxis: {
        valueFormatter: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
        granularityEnabled: true,
        textSize: normalize(14),
        granularity: 1,
        axisMaximum: 31,
        avoidFirstLastClipping: true,
        axisMinimum: 0,
        position: 'BOTTOM'
      },
      zoom: {
        xValue: 0,
        yValue: 0,
        scaleX: 4,
        scaleY: 0
      }
    };
  }

  filterByChosenMonthAndYear() {
    // we need to sort the daily chart by the (chosen or current) month and year.
    // Note: chosenMonth and chosenYear are both initialized to the current month and current year in
    // componentDidUpdate().
    const sortedChartData = [];
    let earliestYear = this.state.earliestYear;
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.rows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.rows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      console.log('123redux!: extractedYear: ' + extractedYear + '\textractedMonth: ' + extractedMonth);

      // Note we add 1 to chosenMonth here because chosenMonth is also used in monthNames array, and monthNames array starts with index 0, hence chosenMonth is by
      // default should start with 0 (for January) and end with 11 (for December).

      console.log('123redux!: this.state.chosenYear: ' + this.state.chosenYear + '\this.state.chosenMonth: ' + (this.state.chosenMonth + 1));
      if (this.state.chosenYear === extractedYear && (this.state.chosenMonth + 1) === extractedMonth) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          _rev: this.state.rows[i].doc._rev,
          date: this.state.rows[i].doc.date,
          glass_count: this.state.rows[i].doc.glass,
          time: this.state.rows[i].doc.time,
          user_id: this.state.rows[i].doc.user_id,
          target_glass: this.state.rows[i].doc.target_glass
        };

        sortedChartData[i] = rec;
      }
    }

    this.setState({ earliestYear: earliestYear });
    console.log('123yearmonth: earliestYear: ' + this.state.earliestYear);

    // we remove all undefined elements in allChartData
    for (let i = sortedChartData.length; i > 0; i--) {
      if (typeof sortedChartData[i] === 'undefined') {
        console.log('123redux! sortedChartData is null or undefined!');
        sortedChartData.splice(i, 1);
      }
    }

    console.log('123redux!: sortedChartData: ' + JSON.stringify(sortedChartData));
    return sortedChartData;
  }

  getCurrentDate() {
    const strDate = new Date();
    console.log('MONTHS**', 'months: ' + getMonthComplete());
    return strDate.getDate() + '\n' + getMonthComplete()[strDate.getMonth()].toUpperCase() + ', ' + strDate.getFullYear();
  }

  getActualDaysInAMonth() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    // to get the actual days in the month, we need to also check if the graph is showing the current/previous month.
    const monthToUse = this.state.chosenMonth === month ? month + 1 : this.state.chosenMonth + 1;
    const yearToUse = this.state.chosenYear === year ? year : this.state.chosenYear;
    return this.getDaysInMonth(monthToUse, yearToUse);
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  getDatabase() {
    manager.database.get_db({ db: DB_NAME })
      .then(res => {
        this.setupViewAndQuery().then(res => {
          this.scaleChartForDailyInterval(5);
          this.setState({ rows: res.obj.rows });
          setTimeout(() => {
            this.renderDailyChart();
            this.getCurrentData();
          }, 5);
        });
      }).catch(e => {
        console.log('couchbase: exception: ' + JSON.stringify(e));
      });
  }

  getHighestValue(chartValues) {
    let highest = 0;

    if (typeof chartValues[0] !== 'undefined' && typeof chartValues[0].y !== 'undefined') {

      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i].y > highest) {
          highest = chartValues[i].y;
        }
      }
    }
    else {
      for (let i = 0; i < chartValues.length; i++) {
        if (chartValues[i] > highest) {
          highest = chartValues[i];
        }
      }
    }
    return highest;
  }

  getSortedMonthlyData() {
    const sortedStepsChartData = [];
    const sortedTimeChartData = [];
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      // only give getMonthlyBarYAxisValues() the chosenYear since this view is already just pertaining to months and not specifically days or weeks.
      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      if (this.state.chosenYear === extractedYear) {
        const recStep = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.glass
        };

        const recTime = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.time
        };

        sortedStepsChartData[i] = recStep;
        sortedTimeChartData[i] = recTime;
      }
    }

    // we remove all undefined elements in allChartData
    for (let i = 0; i < sortedStepsChartData.length; i++) {
      if (typeof sortedStepsChartData[i] === 'undefined') {
        sortedStepsChartData.splice(i, 1);
      }

      if (typeof sortedTimeChartData[i] === 'undefined') {
        sortedTimeChartData.splice(i, 1);
      }
    }

    // this is an array of all the sorted data for activity-sleep analysis.
    const sortedChartData = [sortedStepsChartData, sortedTimeChartData];

    return sortedChartData;
  }

  getWeekRange(limit) {
    var weeks = [];
    let arrCtr = 0;
    let lastRange = 0;

    for (let i = 0; i < limit; i++) {
      if (i === 0) {
        weeks[arrCtr] = '0';
        arrCtr++;
        continue;
      }

      if (i % 7 == 0) {
        weeks[arrCtr] = (i - 6) + '-' + i;
        arrCtr++;
        lastRange = i;
      }
    }

    weeks.push((limit - lastRange) > 1 ? (lastRange + 1) + "-" + limit : (lastRange + 1)) + '';
    return weeks;
  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
    }

    this.setState({ appState: nextAppState });
  }

  handleSelect(event) {
    const entry = event.nativeEvent;
    let dateToShow;
    if (entry === null) {
      this.setState({
        ...this.state,
        selectedEntry: null
      });
    } else {
      this.setState({
        ...this.state,
        selectedEntry: JSON.stringify(entry)
      });

      if (this.state.selectedInterval === I18n.t('DAILY')) {
        if (entry.x < new Date().getDate() || entry.x > new Date().getDate()) {
          this.setState({
            recordDisplay: 'flex', recordVisible: 1, setTargetDisplay: 'none', setTargetVisible: 0,
            targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0
          });
        }
        else if (entry.x === new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
          this.setState({
            recordDisplay: 'flex', recordVisible: 1, setTargetDisplay: 'flex', setTargetVisible: 1
          });

          if (this.props.targetGlassWater === 0) {
            this.setState({
              setTargetDisplay: 'flex', setTargetVisible: 1, targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0
            });
          }
          else {
            this.setState({
              setTargetDisplay: 'none', setTargetVisible: 0, targetAddSubtractDisplay: 'flex', targetAddSubtractVisible: 1
            });
          }

        }
      }

      if (typeof entry.x !== 'undefined' && typeof entry.y !== 'undefined') {
        const xVal = Math.round(entry.x);

        if (this.state.selectedInterval === I18n.t('DAILY')) {
          // we show the day and month here

          if (typeof activity[xVal - 1] !== 'undefined') {
            let step = activity[xVal - 1].y;
            let heartRate = heartRates[xVal - 1];
            let time = timeIntervals[xVal - 1];

            let cal = kcals[xVal - 1];
            let hour = Math.floor(parseInt(time) / 60);
            let mins = parseInt(time) % 60;

            if (typeof hour === 'undefined' || isNaN(hour)) {
              hour = 0;
            }

            if (typeof mins === 'undefined' || isNaN(mins)) {
              mins = 0;
            }

            if (typeof step === 'undefined') {
              step = 0;
            }

            if (typeof kcal === 'undefined') {
              cal = 0;
            }

            if (typeof heartRate === 'undefined') {
              heartRate = 0;
            }

            const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');
            dateToShow = xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear;

            if (xVal === this.state.daysInMonth + 1) {
              dateToShow = this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase();
            }

            this.setState({
              dateSet: dateToShow
            });

            if (typeof TARGET_WATER[xVal] === 'undefined' || isNaN(TARGET_WATER[xVal])) {
              return;
            }


            if (TARGET_WATER[xVal] > glassHighest) {
              this.updateYAxisState(TARGET_WATER[xVal] + plusHighest);
            }
            else {
              this.updateYAxisState(glassHighest + plusHighest);
            }

            // if (typeof entry.data === 'undefined') {
            //   alert(entry.data + ' ' + entry.y)
            //   this.setState({ initialDataCal: entry.y + ' ' + I18n.t('GLASS') });
            // }
            
            if (entry.y > 1) {
              this.setState({ initialDataCal: entry.y + ' ' + I18n.t('GLASSES') });
            }
            else{
              this.setState({ initialDataCal: entry.y + ' ' + I18n.t('GLASS') });
            }

            let arr = this.updateLineChartValues(TARGET_WATER[xVal]);
            let remove_elements = arr.splice(xVal, 1, null);
            console.log('WEW: ', arr);

            const color = TARGET_WATER[xVal] === 0 ? 'transparent' : 'rgb(233, 186, 0)';
            this.updateDataSets(this.state.data.barData.dataSets[0].values,
              arr, color);


            if (TARGET_WATER[xVal] === 0) {
              this.updateDataSets(this.state.data.barData.dataSets[0].values,
                this.updateLineChartValues(0), 'transparent');
            }

            if (xVal >= new Date().getDate() && this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {

              if (this.props.targetGlassWater === 0) {
                this.updateDataSets(this.state.data.barData.dataSets[0].values,
                  this.updateLineChartValues(0), 'transparent');
              }
              else {
                this.updateDataSets(this.state.data.barData.dataSets[0].values,
                  this.updateLineChartValues(this.props.targetGlassWater), TARGET_LINE_COLOR);
              }


              if (this.props.targetGlassWater === 0 || this.props.targetGlassWater <= glassHighest) {
                this.updateYAxisState(glassHighest + plusHighest);
              }
              else {
                this.updateYAxisState(this.props.targetGlassWater + plusHighest);
              }

              if (TARGET_WATER[xVal] !== 0) {
                this.updateDataSets(this.state.data.barData.dataSets[0].values,
                  arr, color);
              }
            }

          }
          else {
            if (xVal === 0 || xVal > this.state.daysInMonth) {
              this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
            }
            else {
              this.setState({ dateSet: xVal + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear });
            }
          }

          if (typeof activity[xVal] === 'undefined' || typeof activity[xVal] === 'undefined' || activity[xVal].y === 0) {
            this.setState({ initialDataCal: 0 + ' ' + I18n.t('GLASS') })
          }


        } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
          let step = weeklySteps[xVal - 1];
          let heartRate = weeklyHeartRates[xVal - 1];
          const time = weeklyTimeIntervals[xVal - 1];

          let kcal = weeklyKcals[xVal - 1];
          let hour = Math.floor(parseInt(time) / 60);
          let mins = parseInt(time) % 60;

          if (typeof hour === 'undefined' || isNaN(hour)) {
            hour = 0;
          }

          if (typeof mins === 'undefined' || isNaN(mins)) {
            mins = 0;
          }

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }

          const dividend = weeklyDividendsForBPM[xVal - 1];

          // compute the average for weekly bpm
          if (dividend > 0) {
            heartRate /= dividend;
            heartRate = heartRate.toFixed(2);
          }

          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');

          if (typeof this.state.xAxis.valueFormatter[xVal] === 'undefined' || xVal === 0) {
            this.setState({
              dateSet: getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear, initialDataCal: entry.y + ' ' + I18n.t('GLASS')
            });
          }
          else if (entry.y < 2) {
            this.setState({
              dateSet: this.state.xAxis.valueFormatter[xVal] + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear, initialDataCal: entry.y + ' ' + I18n.t('GLASS')
            });
          }
          else {
            this.setState({
              dateSet: this.state.xAxis.valueFormatter[xVal] + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() + ', ' + this.state.chosenYear, initialDataCal: entry.y + ' ' + I18n.t('GLASSES')
            });
          }

        } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {

          if (xVal === 0) {
            return;
          }

          let step = monthlySteps[xVal - 1];
          let heartRate = monthlyHeartRates[xVal - 1];

          let kcal = isNaN(monthlyKcals[xVal - 1]) ? 0 : monthlyKcals[xVal - 1];
          let time = monthlyTimeIntervals[xVal - 1];

          let hour = Math.floor(parseInt(time) / 60);
          let mins = parseInt(time) % 60;

          if (typeof hour === 'undefined' || isNaN(hour)) {
            hour = 0;
          }

          if (typeof mins === 'undefined' || isNaN(mins)) {
            mins = 0;
          }

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }

          let dividend = 0;


          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');

          if (entry < 2) {
            this.setState({
              dateSet: this.state.chosenYear + '\n' + (typeof getMonthComplete()[xVal - 1].toUpperCase() === 'undefined' ? I18n.t('JANUARY') : getMonthComplete()[xVal - 1].toUpperCase()), initialDataCal: entry.y + ' ' + I18n.t('GLASS')
            });
          }
          else {
            this.setState({
              dateSet: this.state.chosenYear + '\n' + (typeof getMonthComplete()[xVal - 1].toUpperCase() === 'undefined' ? I18n.t('JANUARY') : getMonthComplete()[xVal - 1].toUpperCase()), initialDataCal: entry.y + ' ' + I18n.t('GLASSES')
            });
          }


        } else {

          if (this.state.selectedInterval === I18n.t('YEARLY')) {
            if (entry < 2) {
              this.setState({ dateSet: this.state.xAxis.valueFormatter[xVal], initialDataCal: entry.y + ' ' + I18n.t('GLASS') });
            }
            else {
              this.setState({ dateSet: this.state.xAxis.valueFormatter[xVal], initialDataCal: entry.y + ' ' + I18n.t('GLASSES') });
            }
          }

          // Note: there's a bug with the graph wherein if you click a bar item, sometimes it returns the zero index,
          // even though it's not the right x-index. We're catching it here for the yearly interval. I haven't observe
          // it occuring in the other intervals, prolly because the way the yearly graph is rendered.
          if (xVal === 0 || typeof (yearlySteps[xVal - 1] === 'undefined')) {
            return;
          }

          // Note: yearlySteps is structured this way: [{year: 0, count: 100}] - only a sample structure
          // the reason this is structured this way is because of the ChartUtils.getYearlyBarYAxisValues()
          // I've made it reusable for the other developers to use for their records tasks.
          let step = yearlySteps[xVal - 1].count;

          let heartRate = yearlyHeartRates[xVal - 1].count;

          let kcal = isNaN(yearlyKcals[xVal - 1].count) ? 0 : yearlyKcals[xVal - 1].count;
          let time = yearlyTimeIntervals[xVal - 1].count;

          let hour = Math.floor(parseInt(time) / 60);
          let mins = parseInt(time) % 60;

          if (typeof hour === 'undefined' || isNaN(hour)) {
            hour = 0;
          }

          if (typeof mins === 'undefined' || isNaN(mins)) {
            mins = 0;
          }

          if (typeof step === 'undefined') {
            step = 0;
          }

          if (typeof kcal === 'undefined') {
            kcal = 0;
          }

          if (typeof heartRate === 'undefined') {
            heartRate = 0;
          }


          const recordedTime = (hour > 0) ? hour + ' ' + I18n.t('HR') + ' ' + mins + ' ' + I18n.t('MINS') : mins + ' ' + I18n.t('MINS');

          this.setState({
            dateSet: this.state.xAxis.valueFormatter[xVal],
          });
        }
      }
    }
  }

  doSave() {
    if (daily_water.length !== 0) {
      this.updateGlass();
    }
    else {
      this.saveGlass();
    }
    this.setState({ modalVisible: false })
  }

  saveGlass() {
    setTimeout(() => {
      const month = parseInt(new Date().getMonth() + 1);
      const strDate = new Date();
      const date = strDate.getFullYear() + '-' + month.toString() + '-' + strDate.getDate();
      const time = strDate.getHours();
      const _id = uuidv4();
      const foodArrayObj = this.props.arr;
      const dateCurrent = this.state.currentYear + '-' + this.state.currentMonth + '-' + this.state.currentDay;
      let targetGlass;

      if (parseInt(this.state.currentMonth) === parseInt(new Date().getMonth() + 1) && parseInt(this.state.currentYear) === new Date().getFullYear()) {
        if (parseInt(this.state.currentDay) === new Date().getDate()) {
          targetGlass = this.props.targetGlassWater;
        } else {
          targetGlass = 0;
        }
      } else {
        targetGlass = 0
      }

      const doc = {
        _id: _id, user_id: user_id, date: dateCurrent,
        glass: this.props.recordGlassWater, time: time,
        type: 'water', target_glass: targetGlass,
        channels: [user_id], timestamp: getFormattedTimestamp()
      };

      manager.document.post({ db: DB_NAME, body: doc }).then(res => {
        console.log('DOC:', doc);
        console.log('res_save:', res);
        this.setState({ isSuccessful: true });
      }).catch(err => {
        console.log('Something went wrong');
      });

      this.setState({ modalVisible: false });
    }, 50);
  }

  updateGlass() {
    setTimeout(() => {
      for (let i = 0; i < daily_water.length; i++) {
        if (typeof (daily_water[i]) === 'undefined') {
          continue;
        }

        let tGlass = this.props.targetGlassWater;

        if (this.props.targetGlassWater === 0 && daily_water[i].target_glass > 0) {
          tGlass = daily_water[i].target_glass;
        }

        const doc = {
          _id: daily_water[i]._id,
          _rev: revs.pop(),
          user_id: daily_water[i].user_id,
          date: daily_water[i].date,
          glass: this.props.recordGlassWater,
          time: daily_water[i].time,
          type: 'water',
          target_glass: tGlass,
          channels: [daily_water[i].user_id],
          timestamp: getFormattedTimestamp(),
        };

        manager.document.put({ db: DB_NAME, doc: doc._id, body: doc, rev: doc._rev }).then(res => {
          console.log('res_update:', res);
          revs.push(res.obj.rev);
          console.log('successfully updated!');
        }).catch(err => {
          console.log('ERRRROOOORRRR:', err);
        });
      }

      this.setState({ isUpdateSuccessful: true });

    }, 50);
  }

  onChangeTarget(txt) {
    if (!isNaN(txt)) {
      let glass = txt === '' || parseInt(txt) < 0 ? 0 : parseInt(txt);

      if (glass === 0 && this.props.recordGlassWater > 0) {
        glass = this.props.recordGlassWater;
      }

      changedGlass = txt === '' || parseInt(txt) < 0 ? 0 : parseInt(txt);

      let glassLabel = '';
      glassLabel = glass <= 1 ? ' ' + I18n.t('GLASS_DAY') : ' ' + I18n.t('GLASSES_DAY');

      const target = glass + glassLabel;
      changedColor = glass === 0 ? 'transparent' : TARGET_LINE_COLOR;
      changedTarget = glass < 1 ? I18n.t('SET_TARGET') : target;

    }
  }

  updateLineChartValues(steps) {
    const lineChartValues = this.state.data.lineData.dataSets[0].values.slice();

    for (var i = 0; i < 35; i++) {
      lineChartValues[i] = steps;
    }

    return lineChartValues;
  }

  onSetTarget() {
    this.setState({ modalVisible: false });
  }

  setupViewAndQuery() {
    return manager.query.get_db_design_ddoc_view_view({
      db: DB_NAME,
      ddoc: 'main',
      view: 'waterByDocId',
      include_docs: true
    });
  }

  onPressDaily() {
    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getDate() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), isTrophyVisible: true, dragEnabled: true });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase(), isTrophyVisible: false, dragEnabled: true });
    }

    if (this.state.chosenYear === new Date().getFullYear()) {
      if (this.state.chosenMonth === new Date().getMonth()) {
        this.setState({
          selectedInterval: I18n.t('DAILY'), setDailyTargetDisplay: 'none', setDailyTargetVisible: 0,
          recordDisplay: 'flex', recordVisible: 1, setTargetDisplay: 'flex', setTargetVisible: 1
        });

        if (this.props.targetGlassWater === 0) {
          this.setState({
            setTargetDisplay: 'flex', setTargetVisible: 1,
            targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0
          });
        }
        else {
          this.setState({
            setTargetDisplay: 'none', setTargetVisible: 0,
            targetAddSubtractDisplay: 'flex', targetAddSubtractVisible: 1
          });
        }

      }
      else {
        this.setState({
          selectedInterval: I18n.t('DAILY'), setDailyTargetDisplay: 'none', setDailyTargetVisible: 0,
          setTargetDisplay: 'none', setTargetVisible: 0, recordDisplay: 'flex', recordVisible: 1
        });
      }
    }
    else {
      this.setState({
        selectedInterval: I18n.t('DAILY'), setDailyTargetDisplay: 'none', setDailyTargetVisible: 0,
        recordDisplay: 'flex', recordVisible: 1, setTargetDisplay: 'none', setTargetVisible: 0
      });
    }

    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isTrophyVisible: true, isDateVisible: true, isRightArrowVisible: true, isLeftArrowVisible: true });
      this.renderDailyChart();
    }, 50);
  }

  onPressWeekly() {
    this.setState({
      selectedInterval: I18n.t('WEEKLY'), isRightArrowVisible: true, isLeftArrowVisible: true,
      dragEnabled: false, marker: defaultMarker, setDailyTargetDisplay: 'flex', setDailyTargetVisible: 1,
      recordDisplay: 'none', recordVisible: 0, setTargetDisplay: 'none', setTargetVisible: 0,
      targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0
    });
    // we need this delay so that the chart wouldn't mess up the zoom levels when re-rendering the datasets.
    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });

      const year = new Date().getFullYear();
      const month = new Date().getMonth();
      const daysInMonth = this.getActualDaysInAMonth();
      const valueFormatter = [];
      this.setState({ daysInMonth: daysInMonth });
      const glass = [0];
      const sleepData = [0];

      const sortedChartData = this.filterByChosenMonthAndYear();

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
        for (let j = 0; j < sortedChartData.length; j++) {
          if (typeof sortedChartData[j] !== 'undefined') {
            const day = parseInt(sortedChartData[j].date.slice(-2));
            if ((day - 1) === i) {
              glass[i] = parseInt(sortedChartData[j].glass_count);
              timeIntervals[i] = parseInt(sortedChartData[j].time);
            } else if (typeof glass[i] === 'undefined') {
              glass[i] = 0;
              timeIntervals[i] = 0;
            }
          }
        }
      }

      glass.splice(0, 0, 0);
      const weeklyYAxisValues = ChartUtils.getWeeklyBarYAxisValues(glass);
      console.log('weeklyYAxisValues:', glass);
      const highest = this.getHighestValue(weeklyYAxisValues);
      weeklyTimeIntervals = ChartUtils.getWeeklyBarYAxisValues(timeIntervals);

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highest + plusHighest);

      const weekRange = this.getWeekRange(this.state.daysInMonth);
      const dayOfTheMonth = new Date().getDate() / 7;

      if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
        if (this.state.changeWeeklyLabel) {
          if (dayOfTheMonth <= 1) {
            this.setState({ dateSet: weekRange[1] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[0] < 2 ? weeklyYAxisValues[0] + ' ' + I18n.t('GLASS') : weeklyYAxisValues[0] + ' ' + I18n.t('GLASSES') });
          } else if (dayOfTheMonth > 1 && dayOfTheMonth <= 2) {
            this.setState({ dateSet: weekRange[2] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[1] < 2 ? weeklyYAxisValues[1] + ' ' + I18n.t('GLASS') : weeklyYAxisValues[1] + ' ' + I18n.t('GLASSES') });
          } else if (dayOfTheMonth > 2 && dayOfTheMonth <= 3) {
            this.setState({ dateSet: weekRange[3] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[2] < 2 ? weeklyYAxisValues[2] + ' ' + I18n.t('GLASS') : weeklyYAxisValues[2] + ' ' + I18n.t('GLASSES') });
          } else if (dayOfTheMonth > 3 && dayOfTheMonth <= 4) {
            this.setState({ dateSet: weekRange[4] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[3] < 2 ? weeklyYAxisValues[3] + ' ' + I18n.t('GLASS') : weeklyYAxisValues[3] + ' ' + I18n.t('GLASSES') });
          } else if (dayOfTheMonth > 4 && dayOfTheMonth <= 5) {
            this.setState({ dateSet: weekRange[5] + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() + ', ' + new Date().getFullYear(), initialDataCal: weeklyYAxisValues[4] < 2 ? weeklyYAxisValues[4] + ' ' + I18n.t('GLASS') : weeklyYAxisValues[4] + ' ' + I18n.t('GLASSES') });
          }
        }
      } else {
        this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase(), initialDataCal: 0 + ' ' + I18n.t('GLASS') });
      }
      for (let i = 0; i < weeklyYAxisValues.length; i++) {
        if (weeklyYAxisValues[i] > 0) {
          this.setState({ isNoDataAvailable: false });
          break;
        } else {
          this.setState({ isNoDataAvailable: true });
        }
      }

      // update x-axis coordinates.
      weeklyYAxisValues.splice(0, 0, 0)
      this.updateXAxisState(weekRange, 5);
      this.updateDataSets(weeklyYAxisValues, [0], 'transparent');

      // update zoom level
      this.setZoomLevel(-5, 0, 1);
    }, 50);
  }

  onPressMonthly() {
    this.setState({
      selectedInterval: I18n.t('MONTHLY'), isRightArrowVisible: true, isLeftArrowVisible: true,
      marker: defaultMarker, setDailyTargetDisplay: 'flex', setDailyTargetVisible: 1,
      recordDisplay: 'none', recordVisible: 0, setTargetDisplay: 'none', setTargetVisible: 0,
      targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0, dragEnabled: true
    });

    if (this.state.chosenYear === new Date().getFullYear() && this.state.chosenMonth === new Date().getMonth()) {
      this.setState({ dateSet: new Date().getFullYear() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() });
    } else {
      this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[this.state.chosenMonth].toUpperCase() });
    }

    setTimeout(() => {
      this.setState({ isTrophyVisible: false, isDateVisible: true });
      const sortedChartData = this.getSortedMonthlyData();

      // Note: sortedChartData is an array of the different arrays of chart data for activity-sleep analysis,
      // in the following order:
      // sortedChartData = [sortedStepsChartData, sortedHeartRateChartData, sortedKcalChartData, sortedTimeChartData];
      const sortedStepsData = sortedChartData[0];
      const monthlyCount = ChartUtils.getMonthlyBarYAxisValues(sortedStepsData);
      let monthlySleepData = [0];
      console.log('MONTHLYCOUNT:', sortedStepsData);
      console.log('SCHARTDATA:', sortedChartData);
      for (let i = 0; i < monthlyCount.length; i++) {
        monthlySleepData[i] = monthlyCount[i] > 700 ? monthlyCount[i] + 100 : monthlyCount[i] - 100;
      }

      monthlySteps = monthlyCount;
      monthlyTimeIntervals = ChartUtils.getMonthlyBarYAxisValues(sortedChartData[1]);

      const highest = this.getHighestValue(monthlyCount);
      monthlyCount.splice(0, 0, 0);
      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highest + plusHighest);
      // update x-axis coordinates.
      this.updateXAxisState(getMonthNames(), 13);
      this.updateDataSets(monthlyCount, [0], 'transparent');


      if (monthlyCount[parseInt(new Date().getMonth()) + 1] !== 0 && typeof monthlyCount[parseInt(new Date().getMonth()) + 1] !== 'undefined') {
        this.setState({ initialDataCal: monthlyCount[parseInt(new Date().getMonth()) + 1] < 2 ? monthlyCount[parseInt(new Date().getMonth()) + 1] + ' ' + I18n.t('GLASS') : monthlyCount[parseInt(new Date().getMonth()) + 1] + ' ' + I18n.t('GLASSES') });
      }
      else {
        this.setState({ initialDataCal: 0 + ' ' + I18n.t('GLASS') })
      }

      for (let i = 0; i < monthlyCount.length; i++) {
        if (monthlyCount[i] > 0) {
          this.setState({ isNoDataAvailable: false });
          break;
        } else {
          this.setState({ isNoDataAvailable: true });
        }
      }



      let latestMonth = 0;
      for (let i = monthlyCount.length; i > 0; i--) {
        if (monthlyCount[i] > 0) {
          latestMonth = i;
          break;
        }
      }

      // this means the chart in view right now is on the daily interval
      if (this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5) {
        // update zoom level
        this.setZoomLevel(2, 0, latestMonth);
      } else {
        this.setZoomLevel(this.state.zoom.scaleX, 0, latestMonth);
      }

    }, 50);
  }

  onPressYearly() {
    this.setState({
      selectedInterval: I18n.t('YEARLY'), dateSet: new Date().getFullYear(), isTrophyVisible: false,
      isRightArrowVisible: false, isLeftArrowVisible: false, marker: defaultMarker,
      setDailyTargetDisplay: 'flex', setDailyTargetVisible: 1, recordDisplay: 'none',
      recordVisible: 0, setTargetDisplay: 'none', setTargetVisible: 0,
      targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0
    });
    setTimeout(() => {
      const allChartData = [];
      const allYearTime = [];

      for (let i = 0; i < this.state.rows.length; i++) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.glass
        };
        allChartData[i] = rec;


        const timeRec = {
          _id: this.state.rows[i].doc._id,
          date: this.state.rows[i].doc.date,
          count: this.state.rows[i].doc.time
        };

        allYearTime[i] = timeRec;
      }

      const yearlyCount = ChartUtils.getYearlyBarYAxisValues(allChartData);
      yearlySteps = yearlyCount;
      yearlyTimeIntervals = ChartUtils.getYearlyBarYAxisValues(allYearTime);

      let counts = [];
      let years = [];
      let yearlySleepData = [];

      for (let i = 0; i < yearlyCount.length; i++) {
        counts[i] = yearlyCount[i].count;
        years[i] = (yearlyCount[i].year) + '';
        yearlySleepData[i] = yearlyCount[i].count > 2000 ? 2000 : yearlyCount[i].count - 200;
        if (typeof counts[i] === 'undefined') {
          counts[i] = 0;
        }
      }

      counts.splice(0, 0, 0);

      years.splice(0, 0, '');

      const highest = this.getHighestValue(counts);

      // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
      // it won't reach the ceiling of the graph component.
      this.updateYAxisState(highest + 100);

      // we're gonna manipulate the years array to make the x-coordinates appear longer in the chart 
      // thereby making the bar chart's width thinner. We need to check whether the years are not 0 so that
      // we won't be change the values of the years' elements.
      let xValues = years;

      if (years.length < 12) {
        // 12 here is an arbitrary value
        for (let i = 0; i < 12; i++) {
          if (typeof years[i] === 'undefined') {
            xValues[i] = '';
          }
        }
      }


      for (let i = 0; i < xValues.length; i++) {
        if (xValues[i].toString() === new Date().getFullYear().toString()) {

          if (counts[i] !== 0 && typeof counts[i] !== 'undefined') {
            this.setState({ initialDataCal: counts[i] < 2 ? counts[i] + ' ' + I18n.t('GLASS') : counts[i] + ' ' + I18n.t('GLASSES') });
          }
          else {
            this.setState({ initialDataCal: 0 + ' ' + I18n.t('GLASS') });
          }
        }

      }

      console.log('YEARS:', xValues);
      console.log('COUNTS:', counts);

      // update x-axis coordinates.
      this.updateXAxisState(xValues, xValues.length + 1);
      this.updateDataSets(counts, [0], 'transparent');
      let latestYear = 0;
      for (let i = counts.length; i > 0; i--) {
        if (counts[i] !== '') {
          latestYear = i;
          break;
        }
      }

      if (typeof this.state.zoom.scaleX === 'undefined' || this.state.zoom.scaleX === 4 || this.state.zoom.scaleX === -5 || this.state.zoom.scaleX === 2) {
        // update zoom level
        this.setZoomLevel(2, 0, 0);
      }

      for (let i = 0; i < counts.length; i++) {
        if (counts[i] > 0) {
          this.setState({ isNoDataAvailable: false });
          break;
        } else {
          this.setState({ isNoDataAvailable: true });
        }
      }


      if (latestYear < 7) {
        this.setState({ dragEnabled: false });
      }
      else {
        this.setState({ dragEnabled: true });
      }

    }, 50);
  }

  onRefresh = () => {
    this.setState({ isRefreshing: true });
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      for (let i = 0; i < daily_water_value.length; i++) {
        if (daily_water_value.length !== 0) {
          if (typeof (daily_water_value[i]) === 'undefined') {
            continue;
          }

          let tGlass = this.props.targetGlassWater;

          if (this.props.targetGlassWater === 0 && daily_water_value[i].target_glass > 0) {
            tGlass = daily_water_value[i].target_glass;
          }

          const doc = {
            _id: daily_water_value[i]._id,
            _rev: revs.pop(),
            user_id: daily_water_value[i].user_id,
            date: daily_water_value[i].date,
            glass: daily_water_value[i].glass_count,
            time: daily_water_value[i].time,
            type: 'water',
            target_glass: tGlass,
            channels: [daily_water_value[i].user_id],
            timestamp: getFormattedTimestamp(),
          };

          manager.document.put({ db: DB_NAME, doc: doc._id, body: doc, rev: doc._rev }).then(res => {
            console.log('res:', res);
            revs.push(res.obj.rev);
            console.log('successfully updated!');
          }).catch(err => {
            console.log('ERRRROOOORRRR:', err);
          });
        }
      }
      this.getDatabase();
    }
    setTimeout(() => {
      this.setState({ isRefreshing: false });
    }, 2000);
  }

  nextDateStyle() {
    return {
      justifyContent: 'flex-end',
      fontSize: 26,
      color: this.state.nextDateTextColor,
      width: 25
    };
  }

  previousDateStyle() {
    return {
      fontSize: 26,
      justifyContent: 'flex-start',
      color: this.state.previousDateTextColor,
      width: 25
    };
  }

  setDailyTarget() {
    ChartDuration.setDailyTargets();
    this.setState({ chosenYear: new Date().getFullYear(), chosenMonth: new Date().getMonth(), dateSet: new Date().getFullYear() + '\n' + getMonthComplete()[new Date().getMonth()].toUpperCase() });
    setTimeout(() => {
      this.onPressDaily();
    }, 5);
  }


  render() {
    return (
      <View style={widgets.mainBrainwaveView}>
        <HeaderMain style={{ flex: 1 }} hideTopHeader='true' />
        <View style={{ flex: 10 }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh}
                tintColor='white'
                title='Loading...'
                titleColor="white"
                colors={['rgb(233, 186, 0)']}
                progressBackgroundColor='white' />
            }>
            <View style={containers.activityChartAndInfoCon}>
              <View style={{ marginTop: 5, marginBottom: 5, flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={text.textTitle}>{I18n.t('WATER_RECORD')}</Text>
              </View>

              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                zIndex: 20,
                height: 30
              }}>
                {
                  this.state.isDateVisible && this.state.isLeftArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={ this.previousDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToPreviousInterval()
                    }, 100);
                  }
                  }>{'<'}</Text></View>
                }
                {
                  this.state.isDateVisible &&
                  <Text style={[widgets.commonFont, { textAlign: 'center', flex: 1}]}>{this.state.dateSet}</Text>
                }
                {
                  this.state.isDateVisible && this.state.isRightArrowVisible &&
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={this.nextDateStyle()} onPress={() => {
                    setTimeout(() => {
                      this.setToNextInterval()
                    }, 100);
                  }
                  }>{'>'}</Text></View>
                }
              </View>

              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={[widgets.hoursStepsText, { textAlign: 'left' }]}> {I18n.t('GLASS')} </Text>
              </View>
              <CombinedChart
                style={widgets.activityBarChart}
                data={this.state.data}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                animation={{
                  durationX: 0
                }}
                touchEnabled={true}
                legend={this.state.legend}
                marker={this.state.marker}
                highlights={this.state.highlights}
                gridBackgroundColor={processColor('#ffffff')}
                drawBarShadow={false}
                drawValueAboveBar={true}
                autoScaleMinMaxEnabled={true}
                doubleTapToZoomEnabled={false}
                scaleEnabled={false}
                zoom={this.state.zoom}
                chartDescription={{ text: '' }}
                chartDescriptionFontSize={18}
                dragEnabled={this.state.dragEnabled}
                drawBorders={false}
                config={this.state.config}
                drawHighlightArrow={false}
                onSelect={this.handleSelect.bind(this)} />

              {
                this.state.isNoDataAvailable &&
                <Text style={widgets.textNoData}>{I18n.t('NO_DATA_YET')}</Text>
              }

              <View style={widgets.lowerSectionBrainwave}>
                <View style={{ marginTop: initialDataCalMargin }}>
                  <Image
                    style={{
                      display: height < 640 ? 'none' : 'flex',
                      width: 35,
                      height: 35,
                      marginVertical: 5,
                      resizeMode: 'contain',
                      alignSelf: 'center'
                    }}
                    source={require('../../images/manage_items_icons/water_128.png')}
                  />
                  <Text style={[text.textRecordCal, text.textHighlightRecordCal]}>{this.state.initialDataCal}</Text>
                </View>
              </View>
            </View>

            {RenderIf(this.state.recordDisplay === 'flex' && this.state.recordVisible === 1)(
              <Card containerStyle={widgets.recordsCardPadding}>
                <View style={widgets.flexRow}>
                  <Image
                    style={{
                      width: 25,
                      height: 25
                    }}
                    source={require('../../images/manage_items_icons/water.png')}
                  />
                  <TouchableOpacity onPress={() => { { this.setState({ modalVisible: true }) } }}>
                    <Text style={text.textRecordCal}> {this.state.recordGlass} {this.state.glassLabel}</Text>
                  </TouchableOpacity>
                </View>
              </Card>
            )}

            {RenderIf(this.state.setTargetDisplay === 'flex' && this.state.setTargetVisible === 1)(
              <Card containerStyle={widgets.recordsCardPadding}>
                <TouchableOpacity onPress={() => { this.setState({ setTargetDisplay: 'none', setTargetVisible: 0, targetAddSubtractDisplay: 'flex', targetAddSubtractVisible: 1 }) }}>
                  <View style={widgets.flexRow}>
                    <Image
                      style={{
                        width: 25,
                        height: 25,
                      }}
                      source={require('../../images/target_set32.png')}
                    />
                    <Text style={text.targetVal}>  {I18n.t('SET_TARGET')}</Text>
                  </View>
                </TouchableOpacity>
              </Card>
            )}


            {RenderIf(this.state.targetAddSubtractDisplay === 'flex' && this.state.targetAddSubtractVisible === 1)(
              <Card containerStyle={widgets.recordsCardPadding}>
                <View style={containers.foodTrophyContainer}>
                  <TouchableOpacity onPress={() => this.subtractGlass()}>
                    <Image style={{ height: 25, width: 25, marginLeft: 15 }} source={require('../../images/minus32.png')} />
                  </TouchableOpacity>
                  <Image style={{ height: 25, width: 25, marginLeft: 15 }} source={require('../../images/target_set32.png')} />
                  <Text style={text.targetVal}>{this.props.targetGlassWater} {this.props.targetGlassWater <= 1 ? ' ' + I18n.t('GLASS_DAY') : ' ' + I18n.t('GLASSES_DAY')} </Text>
                  <TouchableOpacity onPress={() => this.addGlass()}>
                    <Image style={{ height: 25, width: 25, marginRight: 15 }} source={require('../../images/plus32.png')} />
                  </TouchableOpacity>
                </View>
              </Card>
            )}

            {RenderIf(this.state.setDailyTargetDisplay === 'flex' && this.state.setDailyTargetVisible === 1)(
              <Card containerStyle={widgets.recordsCardPadding}>
                <TouchableOpacity onPress={() => { this.setDailyTarget() }}>
                  <View style={widgets.flexRow}>
                    <Text style={text.textRecordCal}>{I18n.t('SET_DAILY_TARGET')}</Text>
                  </View>
                </TouchableOpacity>
              </Card>
            )}


          </ScrollView>
        </View>


        <Modal
          animationType="slide"
          transparent={true}
          style={widgets.modal}
          visible={this.state.modalVisible}
          presentation
          onRequestClose={() => { this.setState({ modalVisible: false }) }}>

          <View>
            <View style={{ marginTop: '50%', padding: 10, backgroundColor: 'white', marginLeft: 20, marginRight: 20, borderStyle: 'solid', borderWidth: 1 }}>
              <ScrollView style={widgets.modalScrollView}>
                <View style={{ marginTop: 20 }}>

                  <View style={widgets.dateAndArrowContainer}>
                    {
                      this.state.isDateVisible &&
                      <Text style={widgets.textLeftArrow} disabled={this.state.disabled} onPress={() => {
                        counter += 1;
                        this.setState({
                          ...Platform.select({
                            ios: {
                              modalVisible: false
                            }
                          }),
                          progressModal: true, disabled: true
                        }, function () {
                          if (counter === 1) {
                            setTimeout(() => {
                              this.gotoPreviousDate()
                              this.setState({ progressModal: false, disabled: false })
                              counter = 0;

                              if (Platform.OS === 'ios') {
                                setTimeout(() => { this.setState({ modalVisible: true }) }, 500)
                              }
                            }, 1500)
                          }
                          else {
                            counter = 0;
                          }
                        });
                      }}>{this.state.arrowLeft}</Text>
                    }
                    {
                      this.state.isDateVisible &&
                      <Text style={widgets.commonFont}>{this.state.dateModalSet}</Text>
                    }
                    {
                      this.state.isDateVisible &&
                      <Text style={widgets.textRightArrow} disabled={this.state.disabled} onPress={() => {
                        counter += 1;
                        this.setState({
                          ...Platform.select({
                            ios: {
                              modalVisible: false
                            }
                          }),
                          progressModal: true, disabled: true
                        }, function () {
                          if (counter === 1) {
                            setTimeout(() => {
                              this.gotoNextDate()
                              this.setState({ progressModal: false, disabled: false })
                              counter = 0;

                              if (Platform.OS === 'ios') {
                                setTimeout(() => { this.setState({ modalVisible: true }) }, 500)
                              }
                            }, 1500)
                          }
                          else {
                            counter = 0;
                          }
                        });
                      }}>{this.state.arrowRight}</Text>
                    }
                  </View>

                  <View>
                    <Card containerStyle={widgets.commonPadding}>
                      <View style={containers.foodTrophyContainer}>
                        <TouchableOpacity onPress={() => this.subtractRecord()}>
                          <Image style={{ height: 20, width: 20 }} source={require('../../images/minus32.png')} />
                        </TouchableOpacity>
                        <Text style={{ marginLeft: 15 }}> </Text>
                        <Text style={text.targetVal}>{this.props.recordGlassWater} {this.props.recordGlassWater <= 1 ? ' ' + I18n.t('GLASS') : ' ' + I18n.t('GLASSES')} </Text>
                        <Text style={{ marginRight: 15 }}> </Text>
                        <TouchableOpacity onPress={() => this.addRecord()}>
                          <Image style={{ height: 20, width: 20 }} source={require('../../images/plus32.png')} />
                        </TouchableOpacity>
                      </View>
                    </Card>
                  </View>


                  <View style={widgets.buttonItemContainer}>
                    <TouchableOpacity onPress={() => { this.setState({ modalVisible: false }) }}>
                      <Card containerStyle={widgets.FoodModuleWithNoDescPad}>
                        <View style={widgets.commonConRow}>
                          <View style={widgets.settingsModuleTitleCon}>
                            <Text style={[text.title]}>
                              {I18n.t('CANCEL')}
                            </Text>
                          </View>
                        </View>
                      </Card>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.doSave() }}>
                      <Card containerStyle={widgets.FoodModuleWithNoDescPad}>
                        <View style={widgets.commonConRow}>
                          <View style={widgets.settingsModuleTitleCon}>
                            <Text style={[text.title]}>
                              {I18n.t('SAVE')}
                            </Text>
                          </View>
                        </View>
                      </Card>
                    </TouchableOpacity>
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>

        <CommonDialog visibility={this.state.isSuccessful}
          message={I18n.t('SAVE_SUCCESSFUL')}
          rightBtnLabel={I18n.t('OK')}
          dismissDialog={() => {
            this.setState({ isSuccessful: false });
            this.getDatabase();
          }} />

        <CommonDialog visibility={this.state.isUpdateSuccessful}
          message={I18n.t('UPDATE_SUCCESSFUL')}
          rightBtnLabel={I18n.t('OK')}
          dismissDialog={() => {
            this.setState({ isUpdateSuccessful: false });
            this.getDatabase();
          }} />

        <ChartDuration
          onPressDaily={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressDaily();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressWeekly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.setState({ changeWeeklyLabel: true });
              this.onPressWeekly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressMonthly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressMonthly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          onPressYearly={() => {
            this.setState({ progressModal: true });
            setTimeout(() => {
              this.onPressYearly();
              this.setState({ progressModal: false });
            }, 1500);
          }}
          style={{ flex: 1 }} />


        <ReactNativeModal
          animationIn='zoomIn'
          animationOut='zoomOut'
          backdropColor='transparent'
          onBackButtonPress={() => {
            this.setState({ progressModal: false });
          }}
          isVisible={this.state.progressModal}>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 50 }}>
            <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
          </View>
        </ReactNativeModal>

      </View>
    );
  }

  renderDailyChart() {
    TARGET_WATER = [];
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    const daysInMonth = this.getActualDaysInAMonth();
    const valueFormatter = [];
    const glass = [];
    const latestDayCount = [];
    let xHighlight = 0;
    let yHighlight = 0;
    this.setState({ daysInMonth: daysInMonth, marker: defaultMarker });
    const sortedChartData = this.filterByChosenMonthAndYear();
    console.log('sortedChartData:', sortedChartData);
    let dailySleepData = [0];
    let shouldShowTargetLine = false;
    let greaterThanTarget = false;
    daily_water_value = [];

    for (let i = 0; i < sortedChartData.length; i++) {
      if (typeof sortedChartData[i] === 'undefined') {
        continue;
      }
      const current_day = parseInt(sortedChartData[i].date.slice(-2));
      if (current_day === new Date().getDate()) {
        daily_water_value[i] = sortedChartData[i];
      }
    }

    for (let i = daily_water_value.length; i >= 0; i--) {
      if (typeof daily_water_value[i] === 'undefined') {
        continue;
      }
      revs.push(daily_water_value[i]._rev);
    }

    for (let i = 0; i <= daysInMonth; i++) {
      valueFormatter[i] = (i + 1) + '';
      console.log('123datesets: outer loop: ' + i);
      for (let j = 0; j < sortedChartData.length; j++) {
        if (typeof sortedChartData[j] !== 'undefined') {
          console.log('123datesets: inner loop: ' + j);
          const day = parseInt(sortedChartData[j].date.slice(-2));

          if (day === i) {
            latestDayCount[i] = parseInt(sortedChartData[j].glass_count);
            shouldShowTargetLine = sortedChartData[j].target_glass === sortedChartData[j].glass_count
              && sortedChartData[j].glass_count > 0;

            greaterThanTarget = sortedChartData[j].glass_count > sortedChartData[j].target_glass && sortedChartData[j].target_glass > 0;

            glass[i] = {
              x: i, y: parseInt(sortedChartData[j].glass_count),
              marker: shouldShowTargetLine ? '\uD83C\uDFC6' : greaterThanTarget ? '\u2757' : ''
            };

            if (shouldShowTargetLine) {
              xHighlight = shouldShowTargetLine ? day : 0;
              yHighlight = glass[i].y;
            }

            TARGET_WATER[i] = parseInt(sortedChartData[j].target_glass);
            timeIntervals[i] = parseInt(sortedChartData[j].time);
            if (day === new Date().getDate()) {
              this.props.updateSettingsModule({ prop: 'targetGlassWater', value: parseInt(sortedChartData[j].target_glass) });

              if (parseInt(sortedChartData[j].target_glass) > 0) {
                this.setState({
                  setTargetDisplay: 'none', setTargetVisible: 0,
                  targetAddSubtractDisplay: 'flex', targetAddSubtractVisible: 1
                });
              }
            }
          } else {
            if (typeof glass[i] === 'undefined') {
              glass[i] = { x: (i > 0 ? i : i), y: 0, marker: '' };
            }

            if (typeof TARGET_WATER[i] === 'undefined') {
              TARGET_WATER[i] = 0;
            }

            if (typeof timeIntervals[i] === 'undefined') {
              timeIntervals[i] = 0;
            }

            if (typeof latestDayCount[i] === 'undefined') {
              latestDayCount[i] = 0;
            }
          }
        }
      }
    }

    activity = glass;
    // this adds a "0" element at the beginning of the valueFormatter array.
    valueFormatter.splice(0, 0, '0');

    if (valueFormatter.length > daysInMonth + 1) {
      // this is a workaround to ensure that the last day on the daily chart is clickable.
      valueFormatter.splice(valueFormatter.length - 1, 1);
    }

    this.setState({ dailyActivities: glass });

    if (typeof glass[parseInt(new Date().getDate())] === 'undefined') {
      this.setState({ initialDataCal: 0 + ' ' + I18n.t('GLASS') });
    }
    else if (glass[parseInt(new Date().getDate())].y > 1) {
      this.setState({ initialDataCal: glass[parseInt(new Date().getDate())].y + ' ' + I18n.t('GLASSES') });
    }
    else {
      this.setState({ initialDataCal: glass[parseInt(new Date().getDate())].y + ' ' + I18n.t('GLASS') });
    }

    glassHighest = this.getHighestValue(glass);
    const highest = this.getHighestValue(glass);
    // this is just an arbitrary number, to make sure whatever the y-value of a bar data,
    // it won't reach the ceiling of the graph component.
    console.log('HIGH HIGH :', highest);

    if (this.props.targetGlassWater > highest) {
      this.updateYAxisState(this.props.targetGlassWater + plusHighest);
    }
    else {
      this.updateYAxisState(highest + plusHighest);
    }

    this.updateXAxisState(valueFormatter, daysInMonth);

    if (glass.length === 0 || (glass.length === 1 && glass[0] === 0)) {
      this.setState({ isNoDataAvailable: true });
    } else {
      this.setState({ isNoDataAvailable: false });
    }

    const lineData = [0];
    for (let i = 0; i <= 32; i++) {
      lineData[i] = this.props.targetGlassWater;
    }

    const lineDataZero = [0];
    for (let i = 0; i <= 32; i++) {
      lineDataZero[i] = 0;
    }


    if (this.state.chosenMonth === new Date().getMonth() && this.state.chosenYear === new Date().getFullYear()) {
      this.updateDataSets(glass, xHighlight === new Date().getDate() ? this.updateLineChartValues(yHighlight) : lineData,
        xHighlight > 0 && xHighlight === new Date().getDate() ? TARGET_LINE_COLOR : this.props.targetGlassWater > 0 ? TARGET_LINE_COLOR : this.state.lineColor);
    }
    else {
      this.updateDataSets(glass, xHighlight === new Date().getDate() ? this.updateLineChartValues(yHighlight) : lineDataZero,
        xHighlight > 0 ? 'transparent' : 'transparent')

      this.setState({
        targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0, setTargetDisplay: 'none', setTargetVisible: 0
      });

    }

    let latestDay = 0;
    for (let i = latestDayCount.length; i > 0; i--) {
      if (latestDayCount[i] > 0) {
        latestDay = i;
        break;
      }
    }
    console.log('latest day: ', latestDay);
    this.scaleChartForDailyInterval(latestDay);

    if (xHighlight > 0 && xHighlight === new Date().getDate()) {
      this.setState({ highlights: [{ x: xHighlight, y: yHighlight, dataIndex: 1 }] });
    }
    console.log('showtargetline:', shouldShowTargetLine);
    console.log('xhighlight:', xHighlight);
    console.log('yhighlight:', yHighlight);
    console.log('daily_food_record:', daily_water_value);
    console.log('DAYS IN MONTH', daysInMonth)
    console.log('GLASS: ', glass);
    console.log('TARGET WATER: ', TARGET_WATER);
  }

  setToPreviousInterval() {
    this.setState({ nextDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) - 1;
    let year = parseInt(this.state.chosenYear);

    if (month < 0) {
      if (year <= this.state.earliestYear) {
        this.setState({ previousDateTextColor: 'transparent' });
        return;
      }
      month = 11;
      year -= 1;
    }

    this.setState({ chosenMonth: month, chosenYear: year });

    // The reason we're setting this variable is because we need to ensure
    // that when the user presses the previous month, we need to maintain the
    // date text to show the month. Right now it's showing the weekly range.
    if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      this.setState({ changeWeeklyLabel: false });
    } else {
      this.setState({ changeWeeklyLabel: true });
    }

    // note the reason we are adding 1 here is because monthNames[] start with index 0
    // whereas in our document's date field, the month starts with 1 and ends with 12.
    // this.sortDatasetByMonths(month + 1);
    this.sortDatasetByIntervals();
    this.setState({
      dateSet: year + '\n' + getMonthComplete()[month].toUpperCase(), targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0,
      setTargetDisplay: 'none', setTargetVisible: 0
    });
  }

  setToNextInterval() {
    this.setState({ previousDateTextColor: 'rgb(127, 127, 127)' });
    let month = parseInt(this.state.chosenMonth) + 1;
    let year = parseInt(this.state.chosenYear) + 1;

    if (month > new Date().getMonth()) {
      if (year > new Date().getFullYear()) {
        // this is not allowed, we should hide the right arrow key
        this.setState({ nextDateTextColor: 'transparent' });
        return;
      } else {

        if (month > 11) {
          // we reset month to 0
          month = 0;
          this.setState({ chosenYear: year, chosenMonth: month, dateSet: year + '\n' + getMonthComplete()[month].toUpperCase() });
        } else {
          this.setState({ chosenYear: (year - 1), chosenMonth: month, dateSet: (year - 1) + '\n' + getMonthComplete()[month].toUpperCase() });
        }

        setTimeout(() => {
          this.sortDatasetByIntervals();
        }, 5);
        return;
      }
    } else {
      this.setState({ chosenMonth: month });
      // The reason we're setting this variable is because we need to ensure
      // that when the user presses the previous month, we need to maintain the
      // date text to show the month. Right now it's showing the weekly range.
      if (this.state.selectedInterval === I18n.t('WEEKLY')) {
        this.setState({ changeWeeklyLabel: false });
      } else {
        this.setState({ changeWeeklyLabel: true });
      }

      // note the reason we are adding 1 here is because monthNames[] start with index 0
      // whereas in our document's date field, the month starts with 1 and ends with 12.
      // this.sortDatasetByMonths(month + 1);
      // this.sortDatasetByIntervals();
      setTimeout(() => {
        this.sortDatasetByIntervals();
      }, 5);
    }

    this.setState({ dateSet: this.state.chosenYear + '\n' + getMonthComplete()[month].toUpperCase() });
  }

  setXValuesFormatter(month) {
    const year = this.state.chosenYear;
    const daysInMonth = this.getDaysInMonth(month + 1, year);
    const valueFormatter = [];

    if (this.state.selectedInterval === I18n.t('DAILY')) {

      for (let i = 0; i < daysInMonth; i++) {
        valueFormatter[i] = (i + 1) + '';
      }

      // this adds a "0" element at the beginning of the valueFormatter array.
      valueFormatter.splice(0, 0, '0');
      this.updateXAxisState(valueFormatter, 31);
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // update x-axis coordinates.
      this.updateXAxisState(this.getWeekRange(daysInMonth), 6);
    }
  }

  sortDatasetByIntervals() {
    if (this.state.selectedInterval === I18n.t('DAILY')) {
      // we render the chart with the daily interval, thereby calling onPressDaily()
      this.onPressDaily();
    } else if (this.state.selectedInterval === I18n.t('WEEKLY')) {
      // we render the chart with the weekly interval, thereby calling onPressWeekly()
      this.onPressWeekly();
    } else if (this.state.selectedInterval === I18n.t('MONTHLY')) {
      // we render the chart with the monthly interval, thereby calling onPressMonthly()
      this.onPressMonthly();
    } else if (this.state.selectedInterval === I18n.t('YEARLY')) {
      // we render the chart with the yearly interval, thereby calling onPressYearly()
      this.onPressYearly();
    }
  }

  setZoomLevel(zoomLevelX, zoomLevelY, moveToX) {
    this.setState({
      isMonthlyView: false,
      zoom: {
        xValue: moveToX,
        yValue: 0,
        scaleX: zoomLevelX,
        scaleY: zoomLevelY
      }
    });
  }

  sortDatasetByMonths(month) {
    let sortedDocs = [];

    for (let i = 0; i < this.state.rows.length; i++) {
      const document = this.state.rows[i].doc;
      const hyphenIndex = document.date.indexOf('-');
      const extractedMonth = parseInt(document.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedMonth === month) {
        sortedDocs[i] = document;
      }
    }

    return sortedDocs;
  }

  subtractGlass() {
    let glass = this.props.targetGlassWater;
    let glassLabel = '';

    if (glass > 0) {
      glass -= 1;
    }
    else {
      this.setState({
        targetAddSubtractDisplay: 'none', targetAddSubtractVisible: 0,
        setTargetDisplay: 'flex', setTargetVisible: 1
      });
    }

    glassLabel = glass <= 1 ? ' ' + I18n.t('GLASS_DAY') : ' ' + I18n.t('GLASSES_DAY');

    const target = glass + glassLabel;
    const color = glass === 0 ? 'transparent' : 'rgb(233, 186, 0)';
    this.setState({ targetGlassWater: target, lineColor: color });
    this.props.updateSettingsModule({ prop: 'targetGlassWater', value: glass });

    if (glass > glassHighest) {
      this.updateYAxisState(glass + plusHighest);
    }
    else {
      this.updateYAxisState(glassHighest + plusHighest);
    }

    this.updateDataSets(this.state.data.barData.dataSets[0].values, this.updateLineChartValues(glass), color);
  }

  updateDataSets(activityCount, lineChartValues, lineColor) {
    console.log('UPDATE DATA SETS: ' + JSON.stringify(activityCount) + '/' + JSON.stringify(lineChartValues) + '/' + lineColor)
    this.setState({
      data: {
        barData: {
          dataSets: [
            {
              values: activityCount,
              label: I18n.t('WATER'),
              colors: [
                processColor('#77b1c2')
              ],
              drawFilled: true,
              fillColor: '#8cbf52',
              fillAlpha: 90,
              textSize: 15,
              config: {
                textSize: 15,
                drawValues: false,
                colors: [processColor('#77b1c2')]
              }
            }],
        },
        lineData: {
          dataSets: [{
            values: lineChartValues,
            label: I18n.t('TARGET'),
            config: {
              colors: [processColor(lineColor)],
              // drawValues: true,
              drawValues: false,
              valueTextSize: 18,
              valueFormatter: '#',
              valueTextColor: processColor('grey'),
              mode: 'CUBIC_BEZIER',
              drawCircles: false,
              lineWidth: 2,
              drawFilled: false,
              dashedLine: {
                lineLength: 40,
                spaceLength: 20
              }
            }
          }]
        }
      }
    });
  }

  scaleChartForDailyInterval(latestDay) {
    // if chart is in monthly view, then we need to re-adjust the scale to suit the daily duration view
    this.setZoomLevel(4, 0, latestDay);
  }

  updateXAxisState(valueFormatter, axisMaximum) {
    console.log('123redux! valueFormatter: ' + JSON.stringify(valueFormatter));
    this.setState({
      xAxis: {
        valueFormatter: valueFormatter,
        granularityEnabled: true,
        granularity: 1,
        drawGridLines: false,
        axisMinimum: 0,
        axisMaximum: axisMaximum + 1,
        textSize: normalize(12),
        position: 'BOTTOM'
      }
    });
  }

  updateYAxisState(maxPoint) {
    this.setState({
      yAxis: {
        left: {
          drawGridLines: true,
          gridLineWidth: 1,
          // labelCount: maxPoint,
          // labelCountForce: false,
          axisMaximum: maxPoint,
          axisMinimum: 0,
          textSize: normalize(14)
        },
        right: {
          enabled: false
        }
      }
    });
  }

  getModalCurrentDate() {
    const strDate = new Date();
    if (parseInt(strDate.getDate()) < 10) {
      return getMonthComplete()[strDate.getMonth()].toUpperCase() + ' ' + '0' + strDate.getDate() + ', ' + strDate.getFullYear();
    }
    else {
      return getMonthComplete()[strDate.getMonth()].toUpperCase() + ' ' + strDate.getDate() + ', ' + strDate.getFullYear();
    }
  }

  getModalCurrentDay() {
    const strDate = new Date();
    if (strDate.getDate() < 10) {
      return "0" + strDate.getDate();
    }
    else {
      return strDate.getDate();
    }
  }

  getModalCurrentMonth() {
    const strDate = new Date();
    const month = strDate.getMonth() + 1;
    if (month < 10) {
      return "0" + month;
    }
    else {
      return month;
    }
  }

  getModalMonth() {
    const strDate = new Date();
    const month = strDate.getMonth();
    return month;

  }

  getModalCurrentYear() {
    const strDate = new Date();
    return strDate.getFullYear();

  }

  getCurrentData() {
    const sortedChartData = this.filterByMonthAndYear();
    daily_water = [];

    for (let i = 0; i < sortedChartData.length; i++) {
      if (typeof sortedChartData[i] === 'undefined') {
        continue;
      }
      const current_day = parseInt(sortedChartData[i].date.slice(-2));
      if (current_day === parseInt(this.state.currentDay)) {
        daily_water[i] = sortedChartData[i];
      }
    }

    for (let i = daily_water.length; i >= 0; i--) {
      if (typeof daily_water[i] === 'undefined') {
        continue;
      }
      revs.push(daily_water[i]._rev);
    }
    console.log('DAILY WATER MOUNT: ', daily_water)
  }

  filterByMonthAndYear() {
    const sortedChartData = [];
    let earliestYear = this.state.earliestYear;
    for (let i = 0; i < this.state.rows.length; i++) {
      // we skip docs with empty dates.
      if (this.state.rows[i].doc.date === '' || typeof this.state.rows[i].doc.date === 'undefined') {
        continue;
      }

      const extractedYear = parseInt(this.state.rows[i].doc.date.substring(0, 4));
      const hyphenIndex = this.state.rows[i].doc.date.indexOf('-');
      const extractedMonth = parseInt(this.state.rows[i].doc.date.substring(hyphenIndex + 1).substring(0, 2));

      if (extractedYear < earliestYear) {
        earliestYear = extractedYear;
      }

      console.log('123redux!: extractedYear: ' + extractedYear + '\textractedMonth: ' + extractedMonth);

      // Note we add 1 to chosenMonth here because chosenMonth is also used in monthNames array, and monthNames array starts with index 0, hence chosenMonth is by
      // default should start with 0 (for January) and end with 11 (for December).

      console.log('123redux!: this.state.chosenYear: ' + this.state.chosenYear + '\this.state.chosenMonth: ' + (this.state.chosenMonth + 1));
      if (this.state.currentYear === extractedYear && (this.state.chosenPrevMonth + 1) === extractedMonth) {
        const rec = {
          _id: this.state.rows[i].doc._id,
          _rev: this.state.rows[i].doc._rev,
          date: this.state.rows[i].doc.date,
          glass_count: this.state.rows[i].doc.glass,
          time: this.state.rows[i].doc.time,
          user_id: this.state.rows[i].doc.user_id,
          target_glass: this.state.rows[i].doc.target_glass
        };

        sortedChartData[i] = rec;
      }
    }

    this.setState({ earliestYear: earliestYear });
    console.log('123yearmonth: earliestYear: ' + this.state.earliestYear);

    // we remove all undefined elements in allChartData
    for (let i = sortedChartData.length; i > 0; i--) {
      if (typeof sortedChartData[i] === 'undefined') {
        console.log('123redux! sortedChartData is null or undefined!');
        sortedChartData.splice(i, 1);
      }
    }

    console.log('123redux!: sortedChartData: ' + JSON.stringify(sortedChartData));
    return sortedChartData;
  }


  gotoPreviousDate() {
    const strDate = new Date();
    let month = this.state.chosenPrevMonth;
    let month_name = getMonthComplete()[month].toUpperCase();
    let day = parseInt(this.state.currentDay);
    let year = strDate.getFullYear();
    let counterMonth = this.state.monthChosen;
    let sortedChartData = this.filterByMonthAndYear();
    daily_water = [];
    console.log('SORTED: ', sortedChartData)
    day -= 1;

    if (day < 10) {
      day = "0" + day;
    }

    if (day < 1) {


      this.setState({ chosenPrevMonth: parseInt(month -= 1), monthChosen: parseInt(counterMonth -= 1) });
      setTimeout(() => {
        this.setState({ daysModalInMonth: new Date(this.state.currentYear, this.state.monthChosen, 0).getDate() });
      }, 60);

      day = this.state.daysModalInMonth + 1;
      if (day === this.state.daysModalInMonth + 1) {
        day = "";
        setTimeout(() => {
          day = this.state.daysModalInMonth;
          month = parseInt(this.state.chosenPrevMonth);
          month_name = getMonthComplete()[month].toUpperCase();
          this.setToPreviousInterval();
          this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: day, dateModalSet: month_name + " " + day + ", " + this.state.currentYear, currentMonth: month += 1 });
          sortedChartData = this.filterByMonthAndYear();
        }, 100);

        setTimeout(() => {

          for (let i = 0; i < sortedChartData.length; i++) {
            if (typeof sortedChartData[i] === 'undefined') {
              continue;
            }
            const current_day = parseInt(sortedChartData[i].date.slice(-2));
            if (current_day === parseInt(this.state.currentDay)) {
              daily_water[i] = sortedChartData[i];
            }
          }

          for (let i = daily_water.length; i >= 0; i--) {
            if (typeof daily_water[i] === 'undefined') {
              continue;
            }
            revs.push(daily_water[i]._rev);
          }
          console.log('DAILY_WATER_PREV: ', daily_water);
        }, 100);

      }

      if (month < 0) {
        this.setState({ chosenPrevMonth: 11, currentMonth: 12, currentYear: parseInt(year - 1) });
      }
    }
    else {
      setTimeout(() => {
        this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: day, dateModalSet: month_name + " " + day + ", " + this.state.currentYear });
      }, 100);
    }

    if (this.state.currentMonth < 10) {
      this.setState({ currentMonth: "0" + parseInt(month + 1) });
    }
    else {
      this.setState({ currentMonth: parseInt(month + 1) });
    }


    setTimeout(() => {

      for (let i = 0; i < sortedChartData.length; i++) {
        if (typeof sortedChartData[i] === 'undefined') {
          continue;
        }
        const current_day = parseInt(sortedChartData[i].date.slice(-2));
        if (current_day === parseInt(this.state.currentDay)) {
          daily_water[i] = sortedChartData[i];
        }
      }

      for (let i = daily_water.length; i >= 0; i--) {
        if (typeof daily_water[i] === 'undefined') {
          continue;
        }
        revs.push(daily_water[i]._rev);
      }
      console.log(daily_water);
    }, 100);
  }

  gotoNextDate() {
    const strDate = new Date();
    let month = this.state.chosenPrevMonth;
    let month_name = getMonthComplete()[month].toUpperCase();
    let day = parseInt(this.state.currentDay);
    let year = this.state.currentYear;
    let counterMonth = this.state.monthChosen;

    let sortedChartData = this.filterByMonthAndYear();
    daily_water = [];

    if (parseInt(this.state.currentDay) < new Date().getDate() || parseInt(this.state.chosenPrevMonth) < new Date().getMonth() || parseInt(this.state.currentYear) < new Date().getFullYear()) {
      if (parseInt(day) < parseInt(new Date().getDate()) || parseInt(this.state.currentMonth) < parseInt(new Date().getMonth() + 1) || parseInt(year) < parseInt(new Date().getFullYear())) {
        day += 1;

        if (day < 10) {
          day = "0" + day;
        }

        if (day > this.state.daysModalInMonth) {

          this.setState({ chosenPrevMonth: parseInt(month += 1), monthChosen: parseInt(counterMonth += 1) });
          setTimeout(() => {
            this.setState({ daysModalInMonth: new Date(this.state.currentYear, this.state.monthChosen, 0).getDate() });
          }, 60);

          day = 0;
          if (day === 0) {
            day = "";
            setTimeout(() => {
              day = 1;
              month = parseInt(this.state.chosenPrevMonth);
              month_name = getMonthComplete()[month].toUpperCase();
              this.setToNextInterval();
              this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: '0' + day, dateModalSet: month_name + " " + '0' + day + ", " + this.state.currentYear, currentMonth: '0' + (month += 1) });
              sortedChartData = this.filterByMonthAndYear();
            }, 100);

            setTimeout(() => {

              for (let i = 0; i < sortedChartData.length; i++) {
                if (typeof sortedChartData[i] === 'undefined') {
                  continue;
                }
                const current_day = parseInt(sortedChartData[i].date.slice(-2));
                if (current_day === parseInt(this.state.currentDay)) {
                  daily_water[i] = sortedChartData[i];
                }
              }

              for (let i = daily_water.length; i >= 0; i--) {
                if (typeof daily_water[i] === 'undefined') {
                  continue;
                }
                revs.push(daily_water[i]._rev);
              }
              console.log('DAILY_WATER_PREV: ', daily_water);
            }, 100);


          }

          if (month > 11) {
            this.setState({ chosenPrevMonth: 0, currentMonth: 1, currentYear: parseInt(year + 1) });
          }
        }
        else {
          setTimeout(() => {
            this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: day, dateModalSet: month_name + " " + day + ", " + this.state.currentYear });
          }, 100);
        }
      }
      else {
        if (this.state.currentMonth < 10) {
          this.setState({ currentMonth: "0" + parseInt(month + 1) });
        }
        else {
          this.setState({ currentMonth: parseInt(month + 1) });
        }
      }

    }
    setTimeout(() => {
      for (let i = 0; i < sortedChartData.length; i++) {
        if (typeof sortedChartData[i] === 'undefined') {
          continue;
        }
        const current_day = parseInt(sortedChartData[i].date.slice(-2));
        if (current_day === parseInt(this.state.currentDay)) {
          daily_water[i] = sortedChartData[i];
        }
      }

      for (let i = daily_water.length; i >= 0; i--) {
        if (typeof daily_water[i] === 'undefined') {
          continue;
        }
        revs.push(daily_water[i]._rev);
      }

      console.log(daily_water);
    }, 100);
  }

  addRecord() {
    let glass = this.props.recordGlassWater;
    let glassLabel = '';

    if (glass < 99) {
      glass += 1;
    }
    glassLabel = glass <= 1 ? ' ' + I18n.t('GLASS') : ' ' + I18n.t('GLASSES');
    const target = glass + glassLabel;
    this.setState({ recordGlassWater: target });
    this.props.updateSettingsModule({ prop: 'recordGlassWater', value: glass });
  }

  subtractRecord() {
    let glass = this.props.recordGlassWater;
    let glassLabel = '';

    if (glass > 0) {
      glass -= 1;
    }
    glassLabel = glass <= 1 ? ' ' + I18n.t('GLASS') : ' ' + I18n.t('GLASSES');
    const target = glass + glassLabel;
    this.setState({ recordGlassWater: target });
    this.props.updateSettingsModule({ prop: 'recordGlassWater', value: glass });
  }
}

export default connect(mapStateToProps, { updateSettingsModule })(WaterRecord);
