import React, { Component } from 'react';
import * as DateUtil from '../utils/DateUtils';
import {
    Text, View, Image, TouchableOpacity, Modal, ScrollView, TextInput, Alert, ListView, Platform
} from 'react-native';
import HeaderMain from './common/HeaderMain';
import { text } from './../styles/text';
import { widgets } from './../styles/widgets';
import { Card } from 'react-native-elements';
import { connect } from 'react-redux';
import { updateSettingsModule } from '../redux/actions';
import getCurrentDate from '../utils/DateUtils';
import I18n from '../translate/i18n/i18n';
import ReactNativeModal from 'react-native-modal';
import { PASSIVE_INTERVAL_COLOR } from '../styles/colors';
import { CommonDialog } from './common/CommonDialog'
import { getPreference, savePreference, showAlert } from '../utils/CommonMethods';
import * as Progress from 'react-native-progress';
import { COLOR_THUMB_TINT } from '../styles/colors';

let counter = 0;
let user_id = '';
const FOOD = [];
const FOOD_SEARCH = [];
const FOOD_SEARCH_MAP = new Map();
const CAL = [];
const uuidv4 = require('uuid/v4');
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const ds_search = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const { getFormattedTimestamp, getMonthComplete } = DateUtil;

class AddFood extends Component {

    constructor() {
        super();

        this.state = {
            disabled: false,
            progressModal: false,
            isSearchRecentlyOpened: false,
            isEmpty: false,
            isSuccessful: false,
            nextDateTextColor: PASSIVE_INTERVAL_COLOR,
            previousDateTextColor: PASSIVE_INTERVAL_COLOR,
            daysInMonth: new Date(new Date().getFullYear(), parseInt(new Date().getMonth() + 1), 0).getDate(),
            chosenYear: 0,
            chosenMonth: 0,
            monthChosen: parseInt(new Date().getMonth() + 1),
            chosenPrevMonth: this.getMonth(),
            selectedInterval: '',
            checked: false,
            dataSource: ds,
            dataSourceSearch: ds_search,
            leftArrow: '<',
            rightArrow: '>',
            foodname: '',
            foodcal: '',
            modalVisible: false,
            modalSearchVisible: false,
            isDateVisible: true,
            totalcal: 0,
            dateSet: this.getCurrentDate(),
            currentYear: this.getCurrentYear(),
            currentMonth: this.getCurrentMonth(),
            currentDay: this.getCurrentDay(),
            timeSet: '',
        }

    }

    componentWillUnmount() {
        FOOD = [];
        FOOD_SEARCH = [];
        CAL = [];
        this.setState({ totalcal: 0, dataSourceSearch: ds_search.cloneWithRows(FOOD_SEARCH) })
    }

    clearData() {
        FOOD = [];
        CAL = [];
    }

    componentDidMount() {
        FOOD = [];
        CAL = [];
        FOOD_SEARCH = [];
        this.populateSearchFood();
        this.setState({ dataSource: ds.cloneWithRows(FOOD), totalcal: 0 });
        this.setupViewAndQuery().then(res => {
            for (let i = 0; i < res.obj.rows.length; i++) {
                if (parseInt(res.obj.rows[i].doc.date.split('-')[2]) === parseInt(this.state.currentDay) && parseInt(res.obj.rows[i].doc.date.split('-')[1]) === parseInt(this.state.currentMonth) && parseInt(res.obj.rows[i].doc.date.split('-')[0]) === parseInt(this.state.currentYear)) {
                    FOOD[i] = res.obj.rows[i].doc;
                    CAL[i] = parseInt(res.obj.rows[i].doc.cals);
                    console.log('FOOD MOUNT :', FOOD);
                }
            }
            this.setState({ dataSource: ds.cloneWithRows(FOOD), totalcal: CAL.reduce((x, y) => x + y) })
        });


        getPreference('userId').then(res => {
            console.log('123redux: getPreference userId: ' + res);
            user_id = res;
        });

        savePreference('isFoodAdded', 'true');

    }

    populateSearchFood() {
        this.setupViewAndQuery().then(res => {
            for (let i = 0; i < res.obj.rows.length; i++) {
                FOOD_SEARCH_MAP.set(res.obj.rows[i].doc.food_name, res.obj.rows[i].doc);
            }
            const list = Array.from(FOOD_SEARCH_MAP.values());
            this.setState({ dataSourceSearch: ds_search.cloneWithRows(list) })
        });
    }


    getCurrentDate() {
        const strDate = new Date();
        if (parseInt(strDate.getDate()) < 10) {
            return getMonthComplete()[strDate.getMonth()].toUpperCase() + ' ' + '0' + strDate.getDate() + ', ' + strDate.getFullYear();
        }
        else {
            return getMonthComplete()[strDate.getMonth()].toUpperCase() + ' ' + strDate.getDate() + ', ' + strDate.getFullYear();
        }
    }

    getCurrentDay() {
        const strDate = new Date();
        if (strDate.getDate() < 10) {
            return "0" + strDate.getDate();
        }
        else {
            return strDate.getDate();
        }
    }

    getCurrentMonth() {
        const strDate = new Date();
        const month = strDate.getMonth() + 1;
        if (month < 10) {
            return "0" + month;
        }
        else {
            return month;
        }
    }

    getMonth() {
        const strDate = new Date();
        const month = strDate.getMonth();
        return month;

    }

    getCurrentYear() {
        const strDate = new Date();
        return strDate.getFullYear();

    }

    setModalVisibility(isVisible) {
        this.setState({
            modalVisible: isVisible,
        });
    }

    setSearchModalVisibility(isVisible) {

        console.log('Data Source:', this.state.dataSourceSearch);

        if (FOOD_SEARCH_MAP.length === 0) {
            Alert.alert(
                '',
                I18n.t('NO_DATA_YET'),
                [
                    { text: I18n.t('OK'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                ],
                { cancelable: false }
            )
        }
        else {
            this.setState({
                modalSearchVisible: isVisible,
            });
        }
    }


    setupViewAndQuery() {
        return manager.query.get_db_design_ddoc_view_view({
            db: DB_NAME,
            ddoc: 'main',
            view: 'foodByDocId',
            include_docs: true
        });
    }

    cancelFood() {

        this.setState({ foodname: '', foodcal: 0 });

        this.setModalVisibility(false)
    }

    saveFood() {
        console.log('food123: foodcal: ' + this.state.foodcal);
        const month = parseInt(new Date().getMonth() + 1);
        const strDate = new Date();
        const date = strDate.getFullYear() + '-' + month.toString() + '-' + strDate.getDate();
        const time = strDate.getHours();
        const _id = uuidv4();
        const foodArrayObj = this.props.arr;
        const dateCurrent = this.state.currentYear + '-' + this.state.currentMonth + '-' + this.state.currentDay;

        let targetCals;

        if (parseInt(this.state.currentMonth) === parseInt(new Date().getMonth() + 1) && parseInt(this.state.currentYear) === new Date().getFullYear()) {
            if (parseInt(this.state.currentDay) === new Date().getDate()) {
                targetCals = this.props.targetCals;
            } else {
                targetCals = 0;
            }
        } else {
            targetCals = 0
        }

        let cal = 0;
        const tmpCal = isNaN(parseInt(this.state.foodcal, 10)) ? Math.round(this.state.foodcal) : parseInt(Math.round(this.state.foodcal), 10);
        if (tmpCal > 0) {
            cal = tmpCal;
        }

        const doc = {
            _id: _id, user_id: user_id, date: dateCurrent,
            food_name: this.state.foodname, cals: cal,
            time: time, type: 'food', target_cals: targetCals,
            channels: [user_id], timestamp: getFormattedTimestamp()
        };


        if (this.state.foodname === "" || this.state.foodcal === "") {
            this.setState({ isEmpty: true });
        } else {
            if (cal > 0) {
                manager.document.post({ db: DB_NAME, body: doc }).then(res => {
                    this.setState({ isSuccessful: true });
                }).catch(err => {
                    console.log('failed:', JSON.stringify(err));
                });
                this.setState({ foodname: '', foodcal: 0 });
                console.log(doc);
                this.setModalVisibility(false);
            } else {
                showAlert('', I18n.t('VALID_CALORIE'));
            }
        }
    }

    setToPreviousInterval() {
        const strDate = new Date();
        let month = this.state.chosenPrevMonth;
        let month_name = getMonthComplete()[month].toUpperCase();
        let day = parseInt(this.state.currentDay);
        let year = strDate.getFullYear();
        let counterMonth = this.state.monthChosen;

        day -= 1;

        if (day < 10) {
            day = "0" + day;
        }

        if (day < 1) {

            this.setState({ chosenPrevMonth: parseInt(month -= 1), monthChosen: parseInt(counterMonth -= 1) });
            setTimeout(() => {
                this.setState({ daysInMonth: new Date(this.state.currentYear, this.state.monthChosen, 0).getDate() });
            }, 60);

            day = this.state.daysInMonth + 1;
            if (day === this.state.daysInMonth + 1) {
                day = "";
                setTimeout(() => {
                    day = this.state.daysInMonth;
                    month = parseInt(this.state.chosenPrevMonth);
                    month_name = getMonthComplete()[month].toUpperCase();
                    this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: day, dateSet: month_name + " " + day + ", " + this.state.currentYear, currentMonth: month += 1 });
                    this.componentDidMount();
                }, 100);
            }

            if (month < 0) {
                this.setState({ chosenPrevMonth: 11, currentMonth: 12, currentYear: parseInt(year - 1) });
            }
        }
        else {
            setTimeout(() => {
                this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: day, dateSet: month_name + " " + day + ", " + this.state.currentYear });
                this.componentDidMount();
            }, 100);
        }

        if (this.state.currentMonth < 10) {
            this.setState({ currentMonth: "0" + parseInt(month + 1) });
        }
        else {
            this.setState({ currentMonth: parseInt(month + 1) });
        }


        FOOD = [];
        CAL = [];
        this.setState({ dataSource: ds.cloneWithRows(FOOD), totalcal: 0 })
    }

    setToNextInterval() {
        const strDate = new Date();
        let month = this.state.chosenPrevMonth;
        let month_name = getMonthComplete()[month].toUpperCase();
        let day = parseInt(this.state.currentDay);
        let year = this.state.currentYear;
        let counterMonth = this.state.monthChosen;

        if (parseInt(this.state.currentDay) < new Date().getDate() || parseInt(this.state.chosenPrevMonth) < new Date().getMonth() || parseInt(this.state.currentYear) < new Date().getFullYear()) {
            if (parseInt(day) < parseInt(new Date().getDate()) || parseInt(this.state.currentMonth) < parseInt(new Date().getMonth() + 1) || parseInt(year) < parseInt(new Date().getFullYear())) {
                day += 1;


                if (day < 10) {
                    day = "0" + day;
                }

                if (day > this.state.daysInMonth) {
                    this.setState({ chosenPrevMonth: parseInt(month += 1), monthChosen: parseInt(counterMonth += 1) });
                    setTimeout(() => {
                        this.setState({ daysInMonth: new Date(this.state.currentYear, this.state.monthChosen, 0).getDate() });
                    }, 60);
                    day = 0;
                    if (day === 0) {
                        day = "";
                        setTimeout(() => {
                            day = 1;
                            month = parseInt(this.state.chosenPrevMonth);
                            month_name = getMonthComplete()[month].toUpperCase();
                            this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: '0' + day, dateSet: month_name + " " + '0' + day + ", " + this.state.currentYear, currentMonth: '0' + (month += 1) });
                            this.componentDidMount();
                        }, 100);
                    }

                    if (month > 11) {
                        this.setState({ chosenPrevMonth: 0, currentMonth: 1, currentYear: parseInt(year + 1) });
                    }
                }
                else {
                    setTimeout(() => {
                        this.setState({ previousDateTextColor: PASSIVE_INTERVAL_COLOR, currentDay: day, dateSet: month_name + " " + day + ", " + this.state.currentYear });
                        this.componentDidMount();
                    }, 100);
                }
            }
            else {
                if (this.state.currentMonth < 10) {
                    this.setState({ currentMonth: "0" + parseInt(month + 1) });
                }
                else {
                    this.setState({ currentMonth: parseInt(month + 1) });
                }
            }
            FOOD = [];
            CAL = [];
            this.setState({ dataSource: ds.cloneWithRows(FOOD), totalcal: 0 })
        }
    }

    getActualDaysInAMonth() {
        return new Date(new Date().getFullYear(), new Date().getMonth(), 0).getDate();
    }


    render() {

        return (
            <View style={{ flex: 1 }}>
                <HeaderMain style={{ flex: 1 }} hideTopHeader='true' />
                <ScrollView>

                    <View style={widgets.dateAndArrowContainer}>
                        {
                            this.state.isDateVisible &&
                            <Text style={widgets.textLeftArrow} disabled={this.state.disabled} onPress={() => {
                                counter += 1;
                                this.setState({ progressModal: true, disabled: true }, function () {
                                    if (counter === 1) {
                                        setTimeout(() => {
                                            this.setToPreviousInterval()
                                            this.setState({ progressModal: false, disabled: false })
                                            counter = 0;
                                        }, 1500)
                                    }
                                    else {
                                        counter = 0;
                                    }
                                });
                            }}>{this.state.leftArrow}</Text>
                        }
                        {
                            this.state.isDateVisible &&
                            <Text style={widgets.commonFont}>{this.state.dateSet}</Text>
                        }
                        {
                            this.state.isDateVisible &&
                            <Text style={widgets.textRightArrow} disabled={this.state.disabled} onPress={() => {
                                counter += 1;
                                this.setState({ progressModal: true, disabled: true }, function () {
                                    if (counter === 1) {
                                        setTimeout(() => {
                                            this.setToNextInterval()
                                            this.setState({ progressModal: false, disabled: false })
                                            counter = 0;
                                        }, 1500)
                                    }
                                    else {
                                        counter = 0;
                                    }
                                });
                            }}>{this.state.rightArrow}</Text>
                        }
                    </View>

                    <Card containerStyle={widgets.settingModuleWithNoDescPad}>
                        <View style={widgets.commonConRow}>
                            <View style={widgets.settingsModuleTitleCon}>
                                <TouchableOpacity onPress={() => { this.setModalVisibility(true) }}>
                                    <Text style={[text.title, text.textRecordCal]}>
                                        {I18n.t('ADD_FOOD')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={widgets.alignRightView}>
                                <TouchableOpacity onPress={() => { this.setSearchModalVisibility(true) }}>
                                    <Image style={widgets.settingsModuleIcon} source={require('../images/search32.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>

                    <Card containerStyle={widgets.addFoodModuleWithNoDescPad}>
                        <View style={widgets.totalCalUndrline}>
                            <View style={widgets.commonFlex}>
                                <Text style={[text.title]}>
                                    {I18n.t('TOTAL_CALORIES')}
                                </Text>

                            </View>

                            <View style={widgets.alignRightView}>
                                <Text style={[text.textTotalCal]}>
                                    {this.state.totalcal}
                                </Text>
                            </View>
                        </View>




                        <ListView
                            showsVerticalScrollIndicator={true}
                            dataSource={this.state.dataSource}
                            renderRow={(rowData) =>
                                <View style={widgets.addFoodCon}>
                                    <TouchableOpacity onPress={() => {
                                        {
                                            Alert.alert(
                                                '',
                                                I18n.t('REMOVE_FOOD'),
                                                [
                                                    { text: I18n.t('CANCEL'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                                    {
                                                        text: I18n.t('OK'), onPress: () => {
                                                            manager.document.delete({ db: DB_NAME, doc: rowData._id, rev: rowData._rev });
                                                            setTimeout(() => {
                                                                this.componentDidMount();
                                                                console.log('rowdata: ', rowData);
                                                            }, 60);
                                                        }
                                                    }
                                                ],
                                                { cancelable: false }
                                            )

                                        }
                                    }}>
                                        <Image style={widgets.addFoodModuleCloseIcon}
                                            source={require('../images/x_24f.png')} />
                                    </TouchableOpacity>
                                    <Text> {rowData.food_name}</Text>

                                    <View style={widgets.alignRightView}>
                                        <Text style={[text.textTotalCal]}>
                                            {rowData.cals}
                                        </Text>
                                    </View>
                                </View>
                            }
                        />



                    </Card>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        style={widgets.modal}
                        visible={this.state.modalVisible}
                        presentation
                        onRequestClose={() => { this.setState({ modalVisible: false }) }}>

                        <View style={widgets.modalAddFood}>
                            <View style={{ marginTop: '50%', padding: 10, backgroundColor: 'white', marginLeft: 35, marginRight: 35, borderStyle: 'solid', borderWidth: 1 }}>
                                <ScrollView style={widgets.modalScrollView}>
                                    <View style={{ marginTop: 20 }}>
                                        <View style={widgets.addFoodCon}>
                                            <Text>
                                                {I18n.t('NAME_OF_FOOD')}
                                            </Text>
                                            <TextInput style={[widgets.loginTextInput, widgets.commonFont]}
                                                onChangeText={(foodname) => {
                                                    this.setState({ foodname: foodname });
                                                }}
                                                maxLength={20}
                                            />
                                        </View>

                                        <View style={widgets.addFoodCon}>
                                            <Text style={{ paddingRight: 32 }}>
                                                {I18n.t('CALORIES')}
                                            </Text>
                                            <TextInput style={[widgets.loginTextInput, widgets.commonFont]}
                                                keyboardType="numeric"
                                                maxLength={4}
                                                onChangeText={(foodcal) => {                                                    
                                                    this.setState({ foodcal: foodcal });
                                                }} />
                                        </View>

                                        <View style={widgets.buttonItemContainer}>
                                            <TouchableOpacity onPress={() => { this.cancelFood() }}>
                                                <Card containerStyle={widgets.FoodModuleWithNoDescPad}>
                                                    <View style={widgets.commonConRow}>
                                                        <View style={widgets.settingsModuleTitleCon}>
                                                            <Text style={[text.title]}>
                                                                {I18n.t('CANCEL')}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </Card>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => { this.saveFood() }}>
                                                <Card containerStyle={widgets.FoodModuleWithNoDescPad}>
                                                    <View style={widgets.commonConRow}>
                                                        <View style={widgets.settingsModuleTitleCon}>
                                                            <Text style={[text.title]}>
                                                                {I18n.t('SAVE')}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </Card>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>

                    </Modal>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        style={widgets.modal}
                        visible={this.state.modalSearchVisible}
                        presentation
                        onRequestClose={() => { this.setState({ modalSearchVisible: false }) }}>

                        <View style={widgets.modalAddFood}>


                            <View style={{ marginTop: 100, marginBottom: 100, padding: 10, backgroundColor: 'white', marginLeft: 35, marginRight: 35, borderStyle: 'solid', borderWidth: 1 }}>

                                <View style={{ alignItems: 'flex-end' }}>
                                    <TouchableOpacity onPress={() => { { this.setState({ modalSearchVisible: false }) } }}>
                                        <Image source={require('../images/x_24f.png')}
                                            style={[widgets.addFoodModuleCloseIcon, {
                                                marginRight: 10,
                                            }]} />
                                    </TouchableOpacity>
                                </View>

                                <Text style={{ marginLeft: 20, fontWeight: 'bold' }}>{I18n.t('LIST_OF_FOOD')}</Text>

                                <ListView
                                    style={{ marginTop: 5, marginBottom: 8 }}
                                    showsVerticalScrollIndicator={true}
                                    dataSource={this.state.dataSourceSearch}
                                    renderRow={(rowData) =>
                                        <TouchableOpacity onPress={() => {

                                            const strDate = new Date();
                                            const time = strDate.getHours();
                                            const _id = uuidv4();
                                            const dateCurrent = this.state.currentYear + '-' + this.state.currentMonth + '-' + this.state.currentDay;

                                            let targetCals;

                                            if (parseInt(this.state.currentMonth) === parseInt(new Date().getMonth() + 1) && parseInt(this.state.currentYear) === new Date().getFullYear()) {
                                                if (parseInt(this.state.currentDay) === new Date().getDate()) {
                                                    targetCals = this.props.targetCals;
                                                } else {
                                                    targetCals = 0;
                                                }
                                            } else {
                                                targetCals = 0
                                            }

                                            const doc = {
                                                _id: _id, user_id: user_id, date: dateCurrent,
                                                food_name: rowData.food_name, cals: parseInt(rowData.cals),
                                                time: time, type: 'food', target_cals: targetCals,
                                                channels: [user_id], timestamp: getFormattedTimestamp()
                                            };

                                            Alert.alert(
                                                '',
                                                I18n.t('ADD_FOOD_ALERT'),
                                                [
                                                    { text: I18n.t('CANCEL'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                                    {
                                                        text: I18n.t('OK'), onPress: () => {
                                                            manager.document.post({ db: DB_NAME, body: doc }).then(res => {
                                                                if (Platform.OS === 'android') {
                                                                    this.setState({ isSuccessful: true });
                                                                } else {
                                                                    this.setState({ modalSearchVisible: false, isSuccessful: true, isSearchRecentlyOpened: true });
                                                                }
                                                            }).catch(err => {
                                                                console.log('Something went wrong when trying to add food: ' + JSON.stringify(res));
                                                            });

                                                        }
                                                    }
                                                ],
                                                { cancelable: false }
                                            )



                                        }}>
                                            <Card containerStyle={widgets.listFoodModuleWithNoDescPad}>
                                                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>
                                                    {'+ ' + rowData.food_name + ' ( ' + rowData.cals + I18n.t('CALS') + ')'}
                                                </Text>
                                            </Card>
                                        </TouchableOpacity>
                                    }
                                />

                            </View>

                        </View>
                    </Modal >
                </ScrollView>

                <CommonDialog visibility={this.state.isSuccessful}
                    message={I18n.t('SAVE_SUCCESSFUL')}
                    rightBtnLabel={I18n.t('OK')}
                    dismissDialog={() => {
                        this.setState({ isSuccessful: false });
                        if (this.state.isSearchRecentlyOpened) {
                            this.setState({ isSearchRecentlyOpened: false, modalSearchVisible: true });
                        }
                        this.componentDidMount();
                    }} />

                <CommonDialog visibility={this.state.isEmpty}
                    message={I18n.t('MUST_NOT_BE_EMPTY')}
                    rightBtnLabel={I18n.t('OK')}
                    dismissDialog={() => {
                        this.setState({ isEmpty: false });
                    }} />

                <ReactNativeModal
                    animationIn='zoomIn'
                    animationOut='zoomOut'
                    backdropColor='transparent'
                    onBackButtonPress={() => {
                        this.setState({ progressModal: false });
                    }}
                    isVisible={this.state.progressModal}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Progress.Circle size={60} indeterminate={true} color={COLOR_THUMB_TINT} borderWidth={5} />
                    </View>
                </ReactNativeModal>

            </View >
        );
    }
}

const mapStateToProps = (state) => {
    const { targetCals } = state.settingsModule;

    return {
        targetCals
    };
};

export default connect(mapStateToProps, { updateSettingsModule })(AddFood); 
