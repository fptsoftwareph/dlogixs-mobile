import React, { Component } from 'react';
import {
  View, Text, TextInput, TouchableOpacity, ScrollView, Alert, NativeEventEmitter,
  NativeModules, Platform
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import I18n from './../translate/i18n/i18n';
import { widgets } from '../styles/widgets';
import MenuIconBack from './common/MenuIconBack';
import { text } from '../styles/text';
import Header from './Header';
import { Icon } from 'react-native-elements';
import { DEFAULT_RESET_HEADER_COLOR } from '../styles/colors';
import { APIClient } from '../utils/APIClient';
import { Actions } from 'react-native-router-flux';

let apiClient;

class RecoverEmail extends Component {
  constructor() {
    super();

    apiClient = new APIClient();

    this.state = {
      isEmailConfirmedVisible: '',
      firstName: '',
      lastName: '',
      email: '',
      birthdate: I18n.t('BIRTHDAY') + ' *'
    };
  }

  onClickConfirm() {
    const name = (this.state.firstName + ' ' + this.state.lastName).trim();
    if (this.state.firstName === '' || this.state.lastName === '' || this.state.birthdate === I18n.t('BIRTHDAY') + ' *') {
      Alert.alert('', I18n.t('FILL_UP_REQUIRED'));
      return;
    }
    apiClient.recoverEmail(name, this.state.birthdate, (recovered, e) => {
      if (recovered) {
        this.setState({ isEmailConfirmedVisible: true, email: e });
      } else {
        this.setState({ isEmailConfirmedVisible: false });
        Alert.alert('', I18n.t('FAILED_TO_RECOVER_EMAIL'));
      }
    });
  }

  onClickSignin() {
    Actions.login();
  }

  render() {
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={widgets.commonCon}>
          <View>
            <Header />
            <MenuIconBack />
          </View>
          <Text style={[
            widgets.signupHeader, {
              marginTop: 15,
              color: DEFAULT_RESET_HEADER_COLOR
            }
          ]}>{I18n.t('RECOVER_EMAIL')}</Text>
          {!this.state.isEmailConfirmedVisible ?
            <View style={widgets.commonCon2}>
              <View style={widgets.idPassCon}>
                <Icon color='#979797' size={25} type="font-awesome" name="user-o" />
                <TextInput
                  placeholder={I18n.t('FIRST_NAME') + ' *'}
                  keyboardType='default'
                  autoCorrect={false}
                  autoCapitalize='words'
                  style={[widgets.loginTextInput, widgets.commonFont]}
                  onChangeText={(firstName) => this.setState({ firstName: firstName })} />
                <TextInput
                  placeholder={I18n.t('LAST_NAME') + '*'}
                  keyboardType='default'
                  autoCorrect={false}
                  autoCapitalize='words'
                  style={[widgets.loginTextInput, widgets.commonFont]}
                  onChangeText={(lastName) => this.setState({ lastName: lastName })} />
              </View>
              <View style={widgets.idPassCon}>
                <Icon color='#979797' size={25} type="octicon" name="gift" />
                <DatePicker
                  style={[widgets.loginTextInput, widgets.borderNone]}
                  date={this.state.date}
                  showIcon={false}
                  mode="date"
                  androidMode="spinner"
                  placeholder={this.state.birthdate}
                  customStyles={{
                    placeholderText: {
                      color: '#595959',
                      textAlign: 'left'
                    },
                    dateInput: {
                      borderColor: '#e8e8ed',
                      borderBottomColor: 'black',
                      justifyContent: 'flex-start',
                      flexDirection: 'row'
                    }
                  }}
                  format="DD/MM/YYYY"
                  confirmBtnText={I18n.t('SAVE')}
                  onDateChange={(date) => {
                    this.setState({ birthdate: date });
                  }} />
              </View>
              <View style={widgets.confirmButton}>
                <TouchableOpacity
                  onPress={() => this.onClickConfirm()}
                  style={{ flex: 1, height: 64 }}>
                  <Text
                    style={[widgets.loginBtn, widgets.commonMarginTop2, { flex: 1 }]}>{I18n.t('CONFIRM')}</Text>
                </TouchableOpacity>
              </View>
            </View>
            :
            <View style={[widgets.commonCon2, { marginTop: 20 }]}>
              <Icon color='#979797' size={25} type="material-community" name="email-outline" />
              <Text style={[text.yourEmailId, widgets.commonFont]}>{I18n.t('YOUR_EMAIL_ID')}</Text>
              <Text style={[text.yourEmailId, widgets.commonFont, { fontWeight: 'bold', marginTop: 10 }]}>{this.state.email}</Text>
              <View style={widgets.confirmButton}>
                <TouchableOpacity
                  onPress={() => this.onClickSignin()}
                  style={{ flex: 1, height: 64 }}>
                  <Text
                    style={[widgets.loginBtn, widgets.commonMarginTop2, { flex: 1 }]}>{I18n.t('SIGN_IN')}</Text>
                </TouchableOpacity>
              </View>
            </View>
          }
        </View>
      </ScrollView>
    );
  }
}

export default RecoverEmail;
