import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

function commonModalMarginSizer(screenWidth) {
  if (screenWidth > 400) {
    return 280;
  } else if (screenWidth > 360) {
    return 250;
  } else if (screenWidth > 250) {
    return 220;
  } else {
    return 180;
  }
}

const containers = StyleSheet.create({
  setIntervalCon: {
    flex: 0.1,
    justifyContent: 'flex-start'
  },
  brainwaveIntervalCon: {
    paddingLeft: 5,
    paddingRight: 5,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  setTargetCon: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  targetTextInputCon: {
    height: 200,
    width: 280,
    padding: 20,
    justifyContent: 'center',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 0.7,
    borderRadius: 14,
  },
  legendCon: {
    flex: 1,
    marginRight: 40,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  SleepInfoContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 12
  },
  durationContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'rgb(73,167,194)',
    height: 100
  },
  logoCon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  introLogCon: {
    height: 250,
    justifyContent: 'center',
    alignItems: 'center'
  },
  activityInfoCon: {
    flex: 1,
    paddingHorizontal: 10,
    // marginLeft: 10,
    // marginRight: 10,
    //backgroundColor: '#e6e6e6',
    // marginBottom: 40
    paddingTop: 5,
    paddingBottom: 30
  },
  recordGraphCon: {
    flex: 1,
    backgroundColor: '#e6e6e6',
    paddingBottom: 20,
  },
  activityInfoRowCon: {
    marginTop: 5,
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    justifyContent: 'space-around',
    flex: 1,
    alignItems: 'center'
  },
  alarmInfoRowCon: {
    marginTop: 5,
    flexDirection: 'row',
    height: 50,
    marginBottom: 5
  },
  alarmInfoTimeRowCon: {
    marginTop: 5,
    flexDirection: 'row',
    height: 50,
    backgroundColor: 'white'
  },
  activityChartAndInfoCon: {
    backgroundColor: '#f1f0f0',
    paddingBottom: 5
  },
  chart: {
    flex: 1,
    height: 290,
    backgroundColor: 'transparent',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3,
    marginBottom: 10
  },
  trophyContainer: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15,
    marginLeft: 20,
    marginRight: 20
  },
  foodTrophyContainer: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20
  }
  ,
  horizontalLineStyle: {
    backgroundColor: '#e6e6e6',
    justifyContent: 'center',
    alignItems: 'center',
    height: 10,
    shadowColor: '#bbb',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 1.3,
    elevation: 3,
    position: 'relative'
  },
  AlarmIconsCon: {
    margin: '1%',
    alignItems: 'center'
  },
  addAlarmContainer: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    margin: 3
  },
  commonMargin: {
    margin: 5
  },
  commonModalCon: {
    marginTop: commonModalMarginSizer(width),
    marginBottom: commonModalMarginSizer(width),
    justifyContent: 'center',
    alignItems: 'center'
  },
  sendWifiDetailsCon: {
    marginTop: commonModalMarginSizer(width),
    marginBottom: commonModalMarginSizer(width),
    justifyContent: 'center'
  }
});

export { containers };
