import { Platform, StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import normalize from '../utils/GetPixelSizeForLayoutSize';
const { width, height } = Dimensions.get('window');

function widgetSizerForSwitches(screenWidth) {
  if (screenWidth > 400) {
    return .9;
  } else if (screenWidth > 250) {
    return .8;
  } else {
    return .6;
  }
}

function commonMiniMarginSizer(screenWidth) {
  if (screenWidth > 400) {
    return 10;
  } else if (screenWidth > 250) {
    return 5;
  } else {
    return 3;
  }
}

function commonSmallMarginSizer(screenWidth) {
  if (screenWidth > 400) {
    return 15;
  } else if (screenWidth > 250) {
    return 10;
  } else {
    return 5;
  }
}

function commonMediumMarginSizer(screenWidth) {
  if (screenWidth > 400) {
    return 20;
  } else if (screenWidth > 250) {
    return 15;
  } else {
    return 10;
  }
}


//ill transfer it later in 1 file.
var itemIconSize;
var itemIconMarginTop;
var circlePointsSize;
var circleBedHeight;
var circleBedWidth;
var barChartMarginBottom;
var barChartHeight;
var minusOnIconSize;

switch (height) {
  case 740: //s8
  case 736: { //iphone 6-8+    
    itemIconSize = 65;
    itemIconMarginTop = 20;
    circlePointsSize = 75;
    circleBedHeight = 30;
    circleBedWidth = 48;
    barChartMarginBottom = 60;
    barChartHeight = 250;
    minusOnIconSize = 25;

    break;
  }
  case 667: {//iphone6-8
    itemIconSize = 45;
    itemIconMarginTop = 20;
    circlePointsSize = 42;
    circleBedHeight = 22;
    circleBedWidth = 36;
    minusOnIconSize = 10;
    barChartMarginBottom = 38;
    barChartHeight = 246;
    break;
  }
  case 640: { //common android    
    itemIconSize = 40;
    itemIconMarginTop = 15;
    circlePointsSize = 42;
    circleBedHeight = 22;
    circleBedWidth = 36;
    minusOnIconSize = 0;
    barChartMarginBottom = 30;
    barChartHeight = 190;
    break;
  }
  default: { //iphone5/SE else heigher
    if (height < 640) {
      itemIconSize = 38;
      // itemIconSize = 20;
      itemIconMarginTop = 8;
      circlePointsSize = 42;
      circleBedHeight = 18;
      circleBedWidth = 28;
      barChartMarginBottom = 30;
      barChartHeight = 190;
      minusOnIconSize = 4;
    } else {
      iitemIconSize = 65;
      itemIconMarginTop = 20;
      circlePointsSize = 75;
      circleBedHeight = 30;
      circleBedWidth = 48;
      barChartMarginBottom = 60;
      barChartHeight = 250;
      minusOnIconSize = 25;
    }
    break;
  }
}

const widgets = StyleSheet.create({
  arrowBack: {
    position: 'absolute',
    height: 50,
    width: 50
  },
  textNoData: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  textLeftArrow: {
    flex: 1,
    fontSize: 26,
    textAlign: 'right',
    marginRight: 50
  },
  textRightArrow: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 50,
    justifyContent: 'flex-end',
    fontSize: 26
  },
  buttonDuration: {
    justifyContent: 'center'
  },
  buttonDurationSelect: {
    justifyContent: 'center',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 7
  },
  legendMarker: {
    height: height === 568 ? 12 : 16,
    width: height === 568 ? 12 : 16
  },
  activityBarChart: {
    // flex: 1,
    // height: 300,
    // width: null,
    // marginLeft: 10,
    // marginRight: 15,
    // backgroundColor: 'transparent'
    height: barChartHeight,
    marginTop: -42,
    marginBottom: barChartMarginBottom,
    ...Platform.select({
      android: {
        marginTop: 0,
        marginBottom: 0,
      }
    }),
    width: null,
    marginLeft: 5,
    marginRight: 15,
    backgroundColor: 'transparent'
  },
  hypnogramChart: {
    flex: 1,
    height: 300,
    width: null,
    marginLeft: 0,
    marginRight: 15,
    backgroundColor: 'transparent'
  },
  freqTextsCon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(220, 219, 219, 0.35)'
  },
  freqViewCon: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    width: 200,
    height: 180
  },
  frequencyIcons: {
    height: 20,
    width: 20
  },
  frequencyHistoryIcons: {
    height: 15,
    width: 15
  },
  activityIcons: {
    height: 30,
    width: 30,
  },
  hoursStepsText: {
    color: 'rgb(137, 137, 137)',
    backgroundColor: 'transparent',
    fontSize: 14,
    marginLeft: 5,
    marginRight: 5,
    flex: 0.5
  },
  frequencyText: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize: 14,
    marginLeft: 5
  },
  checkbox: {
    marginTop: 10,
    marginLeft: 20,
    flex: 1
  },
  intervalView: {
    height: 180,
    width: 200,
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgb(220, 219, 219)',
    marginLeft: 40,
    marginRight: 40
  },
  textInterval: {
    textAlign: 'right',
    fontSize: 14,
    color: 'white',
    marginTop: 15,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    backgroundColor: 'transparent'
  },
  brainwaveChartCon: {
    flex: 1,
    marginLeft: -10,
    marginRight: -10
  },
  pieChart: {
    flex: 0.5,
    height: 200,
    width: null
  },
  pieChartIOS: {
    flex: 1,
    height: 200,
    width: null,
    backgroundColor: 'transparent',
    padding: 0
  },
  attentionText: {
    color: '#50a1c7',
    fontSize: 14,
    backgroundColor: 'transparent',
    ...Platform.select({
      android: {
        fontFamily: 'Helvetica'
      }
    })
  },
  meditationText: {
    color: 'rgb(128, 111, 149)',
    fontSize: 14,
    backgroundColor: 'transparent',
    ...Platform.select({
      android: {
        fontFamily: 'Helvetica'
      }
    })
  },
  meditationAttentionCon: {
    flex: 0.4,
    height: 200,
    width: null,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: 'white',
    marginBottom: 60
  },
  lowerSectionBrainwave: {
    flex: 0.9,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  mainBrainwaveView: {
    flex: 12,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  yAxisGraphScale: {
    flex: 0.2,
    flexDirection: 'column',
    marginBottom: 5,
    marginTop: 10,
    justifyContent: 'space-between',
    paddingLeft: 10,
    alignItems: 'flex-start'
  },
  yAxisGraphScaleIOS: {
    flex: 0.2,
    flexDirection: 'column',
    marginBottom: 15,
    marginTop: 5,
    justifyContent: 'space-between',
    paddingLeft: 10,
    alignItems: 'flex-start'
  },
  lineChartIOS: {
    flex: 0.65,
    marginLeft: -40
  },
  lineChartView: {
    height: 180,
    marginLeft: 5,
    flex: 0.8,
    backgroundColor: 'transparent'
  },
  hypnoChartView: {
    paddingLeft: -100,
    flex: 0.8,
    backgroundColor: 'transparent'
  },
  commonFlexRowStyle: {
    flex: 0.8,
    flexDirection: 'row'
  },
  historyFlexRowStyle: {
    flex: 0.8,
    flexDirection: 'row'
  },
  gradientBackground: {
    height: 210,
    marginLeft: 10,
    marginRight: 10,
    width: null
  },
  historyGradientBackground: {
    height: 210,
    width: null
  },
  dateAndArrowContainer: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 5
  },
  scrollViewContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  pageHeaderContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  menuIconsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  LanguageContainer: {
    paddingTop: 40,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  modalScrollView: {
    marginTop: -20
  },
  privacyPolicyCon: {
    marginTop: 10,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  modal: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flexDirection: 'column'
  },
  modalCloseCon: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  policyHeaderCon: {
    flex: 0.2,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  policyTextCon: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  modalClose: {
    fontSize: 30,
    color: 'white',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Medium'
      }
    }),
    textAlign: 'right',
    marginRight: 15
  },
  privacyPolicyHeader: {
    color: 'white',
    fontSize: 20,
    flex: 1,
    textAlign: 'center',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Medium'
      }
    })
  },
  LanguageHeader: {
    color: 'black',
    fontSize: 22,
    flex: 1,
    textAlign: 'center',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Medium'
      }
    })
  },
  privacyPolicyText: {
    color: 'white',
    fontSize: 16,
    flex: 2,
    marginTop: 15,
    textAlign: 'center',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Light'
      }
    })
  },
  modalImageBg: {
    borderWidth: 1,
    width: null,
    height: null,
    borderRadius: 10,
    borderColor: 'white',
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    marginBottom: 60
  },
  modalSelectLang: {
    borderWidth: 1,
    width: null,
    height: null,
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: 'black',
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 200,
    marginBottom: 200
  },

  modalAlarmTone: {
    borderWidth: 1,
    borderColor: 'black',
    width: null,
    height: null,
    backgroundColor: 'white',
    flex: 1,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 100,
    marginBottom: 150
  },

  modalSnooze: {
    borderWidth: 1,
    borderColor: 'black',
    width: null,
    height: null,
    backgroundColor: 'white',
    flex: 1,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 100,
    marginBottom: 200,
    ...Platform.select({
      ios: {
        marginTop: 50,
        marginBottom: 80,
      }
    }),
  },

  modalHypnogram: {
    paddingTop: 50,
    paddingBottom: 50,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  bluetoothScanPrompt: {
    borderWidth: 1,
    width: null,
    height: null,
    backgroundColor: 'white',
    borderColor: 'black',
    flex: 1,
    marginLeft: 30,
    marginRight: 30
  },
  bluetoothScanPromptCon: {
    width: null,
    height: null,
    flex: 1
  },
  scrollView: {
    marginTop: 30
  },
  confirmButton: {
    marginBottom: 10,
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  signupHeader: {
    color: 'white',
    fontSize: 18,
    marginTop: -40,
    textAlign: 'center',
    ...Platform.select({
      ios: {
        backgroundColor: 'transparent',
      }
    }),
  },
  dotStyle: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 13,
    height: 13,
    borderRadius: 7,
    marginBottom: 100,
    marginLeft: 7,
    marginRight: 7
  },
  activeDotStyle: {
    backgroundColor: '#fff',
    width: 13,
    height: 13,
    borderRadius: 7,
    marginBottom: 100,
    marginLeft: 7,
    marginRight: 7
  },
  buttonSkip: {
    marginLeft: 15,
    marginRight: 15,
    height: 50,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'rgba(255, 255, 255, 0.3)'
  },
  skipText: {
    fontSize: 18,
    textAlign: 'center',
    backgroundColor: 'transparent',
    color: 'white'
  },
  footer: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    marginBottom: 20
  },
  commonCon3: {
    flex: 1,
    justifyContent: 'center',
    height: 250,
    width: null
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  slide: {
    flex: 1,
    flexDirection: 'column'
  },
  congratsText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 28,
    fontWeight: 'bold'
  },
  activatedText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18
  },
  slidetitle: {
    paddingBottom: 100,
    fontSize: 50
  },
  datePickerColor: {
    color: '#595959'
  },
  datePickerTextAlign: {
    textAlign: 'left'
  },
  datePickerBorderColor: {
    borderColor: '#e8e8ed',
  },
  datePickerBorderBottomColor: {
    borderBottomColor: 'black',
  },
  datePickerJC: {
    justifyContent: 'flex-start',
  },
  datePickerFD: {
    flexDirection: 'row'
  },
  commonFont: {
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular',
      }
    }),
    color: 'gray'
  },
  brainWaveText: {
    flex: 0.3,
    textAlign: 'center',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    fontSize: 20,
    color: 'gray',
  },
  commonFont2: {
    flex: 1,
    textAlign: 'center',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    fontSize: 20,
    backgroundColor: 'rgba(220, 219, 219, 0.35)',
  },
  borderNone: {
    borderWidth: 2,
    borderColor: '#e8e8ed',
  },
  birthdayTouchableCon: {
    flex: 1,
    marginLeft: 10,
    borderBottomWidth: 1,
  },
  modalView: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  historyView: {
    flex: 0.5,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center'
  },
  privacyPolicyTermsCon: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  calendar: {
    flex: 1,
    height: 300,
    width: 300,
  },
  countryCon: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    height: 40,
    flex: 1,
    marginLeft: 15,
  },
  signupInfoCon: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'rgb(187, 187, 187)'
  },
  loginTextInput: {
    marginLeft: 10,
    flex: 1,
    ...Platform.select({
      ios: {
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        color: 'gray',
      }
    })
  },
  birthdateTextInput: {
    marginLeft: 10,
    flex: 1,
  },
  loginBtn: {
    backgroundColor: '#ffffff',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    marginBottom: 10,
    textAlign: 'center'
  },

  updateBtn: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: '#3bafda',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 100,
    marginRight: 100,
    marginBottom: 10,
    textAlign: 'center',
    borderRadius: 10
  },
  loginSocialBtnCon: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginSocialBtnOpacity: {
    borderWidth: 2,
    borderColor: '#979797',
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
    width: 35,
    height: 35,
    backgroundColor: 'transparent',
    borderRadius: 50
  },
  loginSocialBtn: {

  },
  centerText: {
    textAlign: 'center'
  },

  leftText: {
    textAlign: 'left'
  },
  commonMarginTop2: {
    marginTop: 15,
  },
  header: {
    height: 230,
    width: null
  },
  underLinedText: {
    textDecorationLine: 'underline'
  },
  logo: {
    height: 110,
    width: 190
  },
  settingsModuleIcon: {
    height: 25,
    width: 25
  },
  addFoodModuleCloseIcon: {
    height: 20,
    width: 20
  },
  alarmModuleCloseIcon: {
    height: 30,
    width: 30
  },
  audioSelectorOptionIcon: {
    height: 12,
    width: 12
  },
  settingsClockIcon: {
    height: 16,
    width: 16
  },
  selectLangTitle: {
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: 'center',
  },
  dropdownPadding: {
    marginLeft: 30,
    marginRight: 30,
  },
  continueBtn: {
    backgroundColor: '#ffffff',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    marginBottom: 10,
    textAlign: 'center',
    alignItems: 'flex-end',
  },
  pagePaddingBottom: {
    paddingBottom: 20,
  },
  commonCon: {
    flex: 1,
    flexDirection: 'column'
  },
  commonConColumn: {
    flexDirection: 'column'
  },
  commonConRow: {
    flexDirection: 'row'
  },
  alignBottom: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  idPassCon: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addFoodCon: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  idDoctor: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 200,
    marginBottom: 100,
    flexDirection: 'row'
  },
  totalCalUndrline: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
  },
  settingsModuleTitleCon: {
    marginLeft: commonSmallMarginSizer(width),
    marginRight: commonMiniMarginSizer(width)
  },
  settingsModuleTitleWithNoDescCon: {
    justifyContent: 'center',
    marginLeft: commonSmallMarginSizer(width),
  },
  commonCon2: {
    flex: 1,
    marginLeft: -1
  },
  commonMarginTop: {
    marginTop: 20
  },
  centerView: {
    justifyContent: 'center'
  },
  flexEndView: {
    justifyContent: 'flex-end'
  },
  settingsCon: {
    flex: 1,
    flexDirection: 'column',
    paddingBottom: 15,
    backgroundColor: '#dcdddc'
  },
  alignRightView: {
    flex: 1,
    alignItems: 'flex-end',
    marginRight: commonMiniMarginSizer(width)
  },
  marginAddFood: {
    padding: 10,
  },
  alignRightViewWithNoMargin: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  alignLeftView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 10
  },
  textAlignRight: {
    flex: 1,
    textAlign: 'right'
  },
  textAlignLeft: {
    flex: 1,
    textAlign: 'left'
  },
  commonMarginRight: {
    marginRight: 10
  },
  commonMarginLeft: {
    marginLeft: 10
  },
  commonMarginLeft2: {
    marginLeft: commonSmallMarginSizer(width)
  },
  commonFlex: {
    flex: 1
  },
  commonMarginLeftRight: {
    marginLeft: 10,
    marginRight: 10
  },
  audioSelectorCon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 10
  },
  alignRightViewIcon: {
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  analysisTimeCon: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  sleepAnalysisChart: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 30,
    marginRight: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  sleepAnalysisDate: {
    flex: 0.7,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    backgroundColor: 'transparent'
  },
  menuIconsContainerSleep: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderColor: 'black',
    borderWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  menuIconsContainerQRation: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    padding: 10
  },
  qrationIconsWrapper: {
    alignContent: 'center',
    justifyContent: 'center',
    //padding: height <= 640 ? 6 : 15, 
    padding: 6,
    width: '48%',
    backgroundColor: '#fff'
  },
  spaceAround: {
    justifyContent: 'space-around'
  },
  commonMarginBottom: {
    marginBottom: 10
  },
  commonPadding: {
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10
  },

  alarmPadding: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 5,
    paddingBottom: 5
  },

  commonPaddingTopAndBottom: {
    paddingTop: 5,
    paddingBottom: 5
  },
  recordsCardPadding: {
    padding: 5,
    marginTop: height <= 640 ? 10 : 14
  },
  textLanguagePosition: {
    textAlign: 'center'
  },
  languagesIcons: {
    height: 50,
    width: 50
  },
  settingModuleWithNoDescPad: {
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 20
  },
  addFoodModuleWithNoDescPad: {
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 100
  },
  FoodModuleWithNoDescPad: {
    padding: 5,
  },
  listFoodModuleWithNoDescPad: {
    padding: 5,
    backgroundColor: 'white'
  },
  appHeaderBg: {
    backgroundColor: '#E9E8EF',
    borderBottomColor: 'transparent',
    borderBottomWidth: 0
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
  menuIconsSleepAnalysis: {
    height: 50,
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'center'
  },
  buttonSelectedDuration: {
    flex: 1,
    borderColor: 'white',
    borderWidth: 3,
    borderRadius: 3,
    marginTop: 2,
    marginBottom: 2
  },
  sleepDetailsArea: {
    marginTop: 8,
    marginHorizontal: 4,
    marginBottom: 5,
    backgroundColor: '#77b1c2'
  },
  sleepReadsContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between'
  },
  reads: {
    fontSize: normalize(12),
    fontWeight: 'bold',
    color: '#fff'
  },
  readsTitle: {
    fontSize: normalize(10),
    color: '#fff'
  },
  boxContainer: {
    margin: '1%',
    backgroundColor: '#fff'
  },
  activityBoxContainer: {
    margin: '1%',
    backgroundColor: '#B8B8B8'
  },
  activityContentWrapper: {
    width: '48%',
    margin: '1%'
  },
  activityIcon: {
    width: height <= 640 ? 30 : 35,
    height: height <= 640 ? 30 : 35,
    marginLeft: '12%',
    alignItems: 'flex-start',
    alignSelf: 'center'
  },
  qrationWrapper: {
    margin: 2,
    marginTop: 0,
    marginBottom: 0
  },
  qrationIconWrapper: {
    width: '31.3%',
    margin: '1%'
  },
  qrationIcon: {
    width: 30,
    height: 30,
    marginTop: 8,
    marginBottom: 10,
    alignItems: 'center',
    alignSelf: 'center'
  },
  itemContainerWrapper: {
    margin: 10,
    marginTop: 0,
    marginBottom: 0
  },
  itemContainer: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  buttonItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center'
  },
  itemWrapper: {
    width: '48%',
    margin: '1%',
    alignItems: 'center'
  },
  itemIcon: {
    height: itemIconSize,
    width: itemIconSize,
    marginBottom: 8,
    marginTop: itemIconMarginTop,
    padding: 10
  },
  manageItemIcon: {
    height: itemIconSize - minusOnIconSize,
    width: itemIconSize - minusOnIconSize,
    marginBottom: 8,
    marginTop: itemIconMarginTop,
    padding: 10
  },
  itemText: {
    color: '#979797',
    fontSize: 14,
    marginBottom: 10,
    alignSelf: 'center'
  },
  circleShape: {
    width: 52,
    height: 52,
    borderRadius: 100 / 2,
    backgroundColor: 'white',
  },
  gearIcon: {
    width: 80,
    height: 40,
    marginTop: 27,
    marginBottom: 8,
    marginLeft: 10,
    marginRight: 25,
    paddingTop: 10,
    paddingBottom: 5
  },
  stationIcon: {
    width: 50,
    height: 47,
    marginTop: 25,
    marginBottom: 5,
    marginLeft: 25,
    marginRight: 10,
    paddingBottom: 5
  },
  actionIcon: {
    width: 50,
    height: 50,
    marginTop: 10,
    marginBottom: 2,
    marginLeft: 10,
    marginRight: 10,
    paddingBottom: 2,
  },
  moreCircleIcon: {
    width: 25,
    height: 25,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 25,
    marginRight: 10
  },
  cardContainerWrapper: {
    margin: 0.5,
    marginTop: 3.5,
    marginBottom: 0
  },
  modalContainer: {
    flexDirection: 'row',
    color: '#979797',
    fontSize: 14
  },
  activityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 10
  },
  shortcutWrapper: {
    marginTop: 5,
    borderBottomColor: '#bbb',
    borderBottomWidth: 0.5,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#bbb',
    borderBottomWidth: 0.5,
    //backgroundColor: '#EEEEEE'
  },
  shortcutContainer: {
    flex: 1,
    height: 30
  },
  touchWrapper: {
    alignItems: 'center'
  },
  iconContainerStyle: {
    alignSelf: 'center'
  },
  iconStyle: {
    alignSelf: 'center',
    margin: 10,
    padding: 5
  },
  scanBg: {
    width: 75,
    height: 30,
    backgroundColor: '#3bafda',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelBg: {
    width: 75,
    height: 30,
    backgroundColor: 'white',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
  },
  disabledCard: {
    backgroundColor: '#dbdbdb',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'gray'
  },
  commonPaddingSmall: {
    padding: 5
  },
  pickerSettings: {
    backgroundColor: 'transparent',
    textAlign: 'left',
    borderBottomWidth: 1,
    borderBottomColor: '#e0e1e2',
    color: 'gray',
  },
  connectTextInput: {
    marginTop: 10,
    marginLeft: 30,
    marginRight: 30,
    ...Platform.select({
      ios: {
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        color: 'gray'
      }
    })
  },
  centerItemsInside: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  progressCircleBed: {
    height: circleBedHeight,
    width: circleBedWidth,

    ...Platform.select({
      android: {
        marginBottom: -10
      }
    })
  },
  progressCirclePoints: {
    color: '#ffffff',
    fontSize: circlePointsSize,
    ...Platform.select({
      android: {
        marginBottom: -10
      },
      ios: {
        marginBottom: -5
      }
    })
  },
  switch: {
    transform: [{ scaleX: widgetSizerForSwitches(width) }, { scaleY: widgetSizerForSwitches(width) }]
  },
  swipe: {
    flex: 1
  },
  bTDevListingViewMargin: {
    marginTop: 100,
    marginBottom: 100
  },
  signUpCheckBox: {
    alignItems: 'center',
    height: 30,
    width: 30,
    marginTop: 3,
    marginRight: 3
  },
  policyMargin: {
    marginLeft: 25,
    marginRight: 25
  }
});

export { widgets };
