import { Platform, StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import normalize from '../utils/GetPixelSizeForLayoutSize';

const { width, height } = Dimensions.get('window');

function fontSizerForTitle(screenWidth) {
  if (screenWidth > 400) {
    return 18;
  } else if (screenWidth >= 360) {
    return 16;
  } else if (screenWidth > 250) {
    return 12;
  } else {
    return 10;
  }
}

function fontSizerForSubTitle(screenWidth) {
  if (screenWidth > 400) {
    return 16;
  } else if (screenWidth >= 360) {
    return 12;
  } else if (screenWidth > 250) {
    return 10;
  } else {
    return 8;
  }
}

function fontSizerForModalLabel(screenWidth) {
  if (screenWidth > 400) {
    return 20;
  } else if (screenWidth >= 360) {
    return 18;
  } else if (screenWidth > 250) {
    return 14;
  } else {
    return 12;
  }
}

const text = StyleSheet.create({
  p: {
    color: 'black',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 14
  },
  title: {
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: fontSizerForTitle(width)
  },
  textTotalCal: {
    textAlign: 'right',
    justifyContent: 'flex-end'
  },
  sleepAnalysisDateText: {
    flex: 0.3,
    marginTop: 5,
    textAlign: 'center',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    fontSize: 20,
    color: 'white'
  },
  sleepAnalysisDateTextDay: {
    flex: 0.3,
    textAlign: 'center',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    fontSize: 20,
    color: 'white'
  },
  sleepDataText: {
    flex: 0.3,
    marginLeft: 10,
    marginBottom: 5,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    fontSize: 14
  },
  textTitle: {
    marginTop: 5,
    marginBottom: 5,
    alignItems: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: normalize(16),
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    color: 'black'
  },
  targetText: {
    marginTop: 5,
    marginBottom: 5,
    textAlign: 'center',
    fontSize: normalize(15),
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    color: '#4bacc6'
  },
  SleepInfoLabel: {
    fontSize: 15,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    color: 'black',
    textAlign: 'left',
    justifyContent: 'flex-start'
  },
  SleepInfoData: {
    fontSize: 15,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    color: 'black',
    justifyContent: 'flex-end',
    textAlign: 'right'
  },
  gradientBackground: {
    flex: 1,
    height: 400,
    marginTop: 30,
    width: null
  },
  durationText: {
    fontSize: 15,
    color: 'white',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    })
  },
  durationSelectedText: {
    padding: 5.7,
    fontSize: 15,
    color: 'white',
    borderColor: 'white',
    borderWidth: 0.7,
    borderRadius: 14,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    })
  },
  subTitle: {
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: fontSizerForSubTitle(width)
  },
  cal: {
    color: '#8cc152',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  textRecordCal: {
    color: '#4bacc6',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 20,
    textAlign: 'center'
  },

  textAlarmTitle: {
    color: '#4bacc6',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 30,
    textAlign: 'center'
  },

  textAlarmDetailTitle: {
    color: '#4bacc6',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 50,
    textAlign: 'center'
  },

  textAlarmSubtitle: {
    color: '#4bacc6',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 15,
    textAlign: 'center'
  },

  textHighlightRecordCal: {
    fontSize: normalize(20),
    fontWeight: 'bold'
  },

  titleFirstname: {
    fontSize: 22,
    paddingTop: 10,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Medium'
      }
    }),
    width: 200
  },
  subTitleLastname: {
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Medium'
      }
    }),
    fontSize: 12,
    width: 200
  },
  data: {
    color: 'black',
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 25
  },
  alarmActiveText: {
    marginTop: 5,
    marginBottom: 5,
    textAlign: 'center',
    fontSize: 15,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    color: '#8e75c9'
  },
  alarmInactiveText: {
    marginTop: 5,
    marginBottom: 5,
    textAlign: 'center',
    fontSize: 15,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    color: '#828282'
  },
  alarmTitle: {
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    fontSize: 16,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#8e75c9'
  },
  bluetoothTitleTask: {
    color: 'black',
    fontSize: fontSizerForModalLabel(width),
    paddingTop: 10,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    textAlign: 'center'
  },
  sleepHypnogram: {
    color: 'black',
    fontSize: 15,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto'
      }
    }),
    textAlign: 'center'
  },
  scanBg: {
    width: 75,
    height: 29,
    backgroundColor: '#3bafda',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'black',
    textAlign: 'center',
    color: 'white'
  },
  titleColorBlue: {
    color: '#3bafda',
    fontSize: 15
  },
  titleColorGray: {
    color: 'gray',
    fontSize: 15
  },
  targetVal: {
    color: '#604a7b',
    fontSize: normalize(15)
  },
  marginRight: {
    fontSize: 20,
    marginRight: 15
  },
  yourEmailId: {
    color: '#404040',
    fontSize: 16,
    marginTop: 15,
    textAlign: 'center'
  },
  viewHynogramStyle: {
    marginRight: 15,
    fontSize: height <= 640 ? normalize(18) : normalize(20),
    textDecorationLine: 'underline',
    textAlign: 'right',
    justifyContent: 'flex-end',
    marginTop: 5,
    ...Platform.select({
      android: {
        fontFamily: 'Roboto-Regular'
      }
    }),
    color: '#4bacc6'
  },
  qrationElemNoData: {
    textAlign: 'center',
    justifyContent: 'center'
  },
  centerText: {
    textAlign: 'center',
    textAlignVertical: 'center'
  }
});

export { text };
