'use strict';

import Couchbase from 'react-native-couchbase-lite';

export default class Feed {
  constructor(poll, seq, callback) {
    this.getChanges(poll, seq, callback);
  }
  getChanges(poll, seq, callback) {
    Couchbase.initRESTClient(manager => {
      manager.query.get_db_changes({ db: DB_NAME, include_docs: true,
        feed: poll, since: seq })
        .then(res => {
          if (!this.stopped) {
            callback();
            this.getChanges(res.obj.last_seq, callback);
          }
        })
        .catch(e => console.log('123couchbase Feed error: ', e));
    });
  }
  stop() {
    this.stopped = true;
  }
}
