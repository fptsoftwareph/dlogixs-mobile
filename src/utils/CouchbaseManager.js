'use strict';

import Couchbase from 'react-native-couchbase-lite';
import Session from './Session';
import { savePreference, getPreference } from '../utils/CommonMethods';

const COUCHBASE_CRED = require('../config/demo_couchbase_cred.json');
global.LOGIN_FLOW_ENABLED = false;
const SYNC_ENABLED = true;

global.DB_NAME = 'dlogixs';
global.manager = undefined;

global.SG_HOST = COUCHBASE_CRED.SG_HOST;
const USE_PREBUILT_DB = false;
var sessionRes;

const VIEWS = {
  views: {
    activitiesByUserId: {
      map: function (doc) {
        if (doc.type === 'activity-sleep') {
          emit(doc.user_id, null);
        }
      }.toString()
    },
    activitiesByRevision: {
      map: function (doc) {
        if (doc.type === 'activity-sleep') {
          emit(doc._rev, null);
        }
      }.toString()
    },
    activityWithinToday: {
      map: function (doc) {
        if (doc.type === 'activity-sleep' &&
        Math.abs(new Date().getTime() - new Date(doc.timestamp).getTime()) / (1000 * 60 * 60 * 24) < 1) {
          emit(doc._id, null);
        }
      }.toString()
    },
    sleepWithinToday: {
      map: function (doc) {
        if (doc.type === 'sleep-activity' &&
        Math.abs(new Date().getTime() - new Date(doc.timestamp).getTime()) / (1000 * 60 * 60 * 24) < 1) {
          emit(doc._id, null);
        }
      }.toString()
    },
    activitiesByDocId: {
      map: function (doc) {
        if (doc.type === 'activity-sleep') {
          emit(doc._id, null);
        }
      }.toString()
    },
    foodByDocId: {
      map: function (doc) {
        if (doc.type === 'food') {
          emit(doc._id, null);
        }
      }.toString()
    },
    waterByDocId: {
      map: function (doc) {
        if (doc.type === 'water') {
          emit(doc._id, null);
        }
      }.toString()
    },
    caffeineByDocId: {
      map: function (doc) {
        if (doc.type === 'caffeine') {
          emit(doc._id, null);
        }
      }.toString()
    },
    alcoholByDocId: {
      map: function (doc) {
        if (doc.type === 'alcohol') {
          emit(doc._id, null);
        }
      }.toString()
    },
    cigaretteByDocId: {
      map: function (doc) {
        if (doc.type === 'cigarette') {
          emit(doc._id, null);
        }
      }.toString()
    },
    curationByDocId: {
      map: function (doc) {
        if (doc.type === 'curation') {
          emit(doc._id, null);
        }
      }.toString()
    },
    last2MosDataByDocId: {
      map: function (doc) {
        if (doc.type === 'curation' &&
        Math.abs(new Date().getTime() - new Date(doc.modified_date).getTime()) / (1000 * 60 * 60 * 24) <= 60) {
          emit(doc._id, null);
        }
      }.toString()
    },
    targetDataByDocId: {
      map: function (doc) {
        if (doc.type === 'curation' &&
        Math.abs(new Date().getTime() - new Date(doc.modified_date).getTime()) / (1000 * 60 * 60 * 24) < 1) {
          emit(doc._id, null);
        }
      }.toString()
    },
    curationActualDataByDocId: {
      map: function (doc) {
        if (doc.type === 'curation_actual') {
          emit(doc._id, null);
        }
      }.toString()
    },
    alarmByDocId: {
      map: function (doc) {
        if (doc.type === 'alarm') {
          emit(doc._id, null);
        }
      }.toString()
    },
    sleepByDocId: {
      map: function (doc) {
        if (doc.type === 'sleep-activity') {
          emit(doc._id, null);
        }
      }.toString()
    },
    binauralByDocId: {
      map: function (doc) {
        if (doc.type === 'binaural') {
          emit(doc._id, null);
        }
      }.toString()
    },
    binauralWithinToday: {
      map: function (doc) {
        if (doc.type === 'binaural' &&
        Math.abs(new Date().getTime() - new Date(doc.timestamp).getTime()) / (1000 * 60 * 60 * 24) < 1) {
          emit(doc._id, null);
        }
      }.toString()
    }
  }
};


let username = '';
let password = '';
let isSuccess = false;

module.exports = {
  init(client) {
    manager = client;
  },

  checkLogin() {
    console.log('getDatabase(): isSuccess: ' + isSuccess);
    return isSuccess;
  },

  disableAutosync(userId, pass, getDatabase, isLogin) {
    if (isLogin) {
      console.log('disableAutosync islOGIN');
      this.enableAutosync(userId, pass, getDatabase);
    } else {
      this.stopReplications()
      .then(res => {
        console.log('1231 disableSync stopReplications() res: ' + JSON.stringify(res));
        Session.deleteSyncGatewaySession();
      });

      console.log('1231sync disable sync...');
      getDatabase();
    }
  },

  enableAutosync(userId, pass, getDatabase) {
    username = userId;
    password = pass;
    Session.createSyncGatewaySession(userId, pass)
    .then(res => {
      console.log('123couchbase: initCouchbase res: ' + JSON.stringify(res));
      console.log('1231sync enable sync...');
      this.startSession(userId, pass);
      getDatabase();
    });
  },

  login(user, pass) {
    username = user;
    password = pass;
    this.startSession(username, password);
  },

  startSession(user, pass) {
    this.installPrebuiltDb(() => {
      this.startDatabaseOperations()
        .then(() => {
          this.setupReplications(user, pass);
        })
        .then(res => {
          this.sessionRes = res;
        });
    });
  },

  setupDatabase() {
    console.log('123couchbase-- setupDatabase()');
    manager.database.put_db({ db: DB_NAME })
      .then(() => this.startDatabaseOperations())
      .catch(e => {
        if (e.status === 412) {
          this.startDatabaseOperations();
        }
      }
      );
  },

  installPrebuiltDb(callback) {
    console.log('123couchbase-- installPrebuiltDb()');

    if (USE_PREBUILT_DB) {
      console.log('123couchbase-- A installPrebuiltDb() -- USE_PREBUILT_DB');
      Couchbase.installPrebuiltDatabase(DB_NAME, callback);
    } else {
      console.log('123couchbase-- B installPrebuiltDb() -- USE_PREBUILT_DB');
      callback();
    }
  },

  startDatabaseOperations() {
    console.log('123couchbase-- startDatabaseOperations()');
    return manager.database.get_db({ db: DB_NAME })
      .then(() => {
        this.setupViews();
      })
      .catch(e => {
        if (e.status === 404) {
          this.setupDatabase();
        }
      });
  },

  setupViews() {
    console.log('123couchbase-- setupViews()');
    manager.query.get_db_design_ddoc({ db: DB_NAME, ddoc: 'main' })
      .catch(e => {
        if (e.status === 404) {
          manager.query.put_db_design_ddoc({ ddoc: 'main', db: DB_NAME, body: VIEWS })
            .catch(e => console.log('getdatabase: error in put_db_design_ddoc: ' + JSON.stringify(e)));
        }
      });
  },

  setupReplications(username, password) {
    if (SYNC_ENABLED) {
      const sgUrl = `http://${username}:${password}@${SG_HOST}`;
      console.log('123couchbase--', 'sgUrl: ' + sgUrl);

      return manager.server.post_replicate({ body: { source: sgUrl, target: DB_NAME, continuous: true } })
        .then(res => {
          manager.server.post_replicate({ body: { source: DB_NAME, target: sgUrl, continuous: true } });
          isSuccess = true;
        })
        .catch(e => console.log('123couchbase: getdatabase: error in setup replications: ' + JSON.stringify(e)));
    }
  },

  stopReplications() {
    const sgUrl = `http://${username}:${password}@${SG_HOST}`;
    return manager.server.post_replicate({ body: { source: sgUrl, target: DB_NAME, continuous: true, cancel: true } })
      .then(res => manager.server.post_replicate({ body: { source: DB_NAME, target: sgUrl, continuous: true, cancel: true } }))
      .catch(e => console.log('123couchbase--', 'error in stopReplications: ' + JSON.stringify(e)));
  },

  logout() {
    getPreference('userId').then(res => {
      username = username === '' ? res : username;
      console.log('123couchbase userId: ' + username);
    });

    getPreference('password').then(res => {
      password = password === '' ? (getPreference('loginType') === 'neurobeat' ? res : username) : password;
    });

    // if loginType === neurobeat, we use the password stored in the preference, else we use the user's id
    // because we use the user id as password for 3rd party social media logins (since they don't enter a password
    // when logging-in this way).

    if (manager !== undefined) {
      this.stopReplications()
        .then(res => {
          Session.deleteSyncGatewaySession();
          manager.database.delete_db({ db: DB_NAME })
            .then(res => console.log('123couchbase delete db: ' + JSON.stringify(res)))
            .catch(e => {
              if (e.status === 412) {
                this.startDatabaseOperations();
              }
            }
            );
          savePreference('userId', '');
          savePreference('password', '');
        });
      username = '';
      password = '';
    }
  }
};


