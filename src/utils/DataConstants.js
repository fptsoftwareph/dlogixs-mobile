export const IS_WIFI_STATION_ENABLED = 'isWstationHeadSetEnabled';
export const IS_RBS_ENABLED = 'isRbsEnabled';
export const DEVICE_NAME = 'Device Name';
export const NETWORK_NAME = 'Network Name';
export const CURATION_COUNTER_1 = 1;
export const CURATION_COUNTER_2 = 2;
export const CURATION_COUNTER_3 = 3;
export const CURATION_COUNTER_4 = 4;
export const CURATION_COUNTER_5 = 5;
export const CURATION_COUNTER_6 = 6;
export const BSTATION = 'bStation';
export const WRISTBAND = 'wristband';
export const WIFI_STATION = 'wifiStation';
export const STATUS_200 = 200;
export const BT_ADAPTER_NOT_INIT = "BluetoothAdapter not initialized, please connect again to the peripheral.";
export const DEVICE_DOESNT_SUPPORT_UART = "Device doesn't support UART. Disconnecting";
export const FB_SWITCH_ACCT_ERR = 'User logged in as different Facebook user.';

