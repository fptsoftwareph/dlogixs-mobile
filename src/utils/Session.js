'use strict';

import Swagger from 'swagger-client';
import spec from '../config/sync-gateway-spec.json';

let manager;

module.exports = {
  createSyncGatewaySession(user, pass) {
    console.log('123couchbase-- user: ' + user + "\tpass: " + pass + '\tDB_NAME: ' + DB_NAME);
    spec.host = SG_HOST.split('/')[0];
    console.log('123couchbase: create session spec.host: ' + spec.host);
    return new Swagger({ spec: spec, usePromise: true })
      .then(client => {
        manager = client;
        return manager;
      })
      .then(client => {
        return client.session.post_db_session({ db: DB_NAME, SessionBody: { name: user, password: pass } });
      }).then(res => {
        console.log('1231 res: ' + JSON.stringify(res));
      });
  },

  deleteSyncGatewaySession() {
    spec.host = SG_HOST.split('/')[0];
    if (manager !== undefined) {
      return manager.session.delete_db_session({ db: DB_NAME })
      .then(res => console.log('123couchbase: res (delete session): ' + JSON.stringify(res)))
      .catch(e => console.warn('123couchbase: error (delete session): ' + JSON.stringify(e)));
    }
    return undefined;
  }
};
