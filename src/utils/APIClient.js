import axios from 'axios';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { showAlert, showAlertWithActions } from './CommonMethods';
import I18n from '../translate/i18n/i18n';
import { STATUS_200 } from './DataConstants';
import FormData from 'form-data';
import { getPreference, savePreference } from './CommonMethods';

const URL_Constants = require('../config/urls.json');
var axiosClient;
let user_id = '';

export class APIClient extends Component {

  createInstance() {
    return axios.create({
      baseURL: URL_Constants.SERVER_BASE_URL,
      timeout: 5000,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest',
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  constructor() {
    super();

    if (axiosClient === undefined) {
      this.axiosClient = this.createInstance();
    }

    getPreference('userId').then(res => {
      console.log('123redux: getPreference userId: ' + res);
      user_id = res;
    });

  }

  updateProfile(userInfo) {
    console.log('axios_: userInfo: ' + JSON.stringify(userInfo));
    const body = new FormData();
    body.append('weight', userInfo.weight);
    body.append('height', userInfo.height);
    body.append('country', userInfo.country);
    body.append('birthdate', userInfo.birthdate);
    body.append('gender', userInfo.gender);
    body.append('occupation', userInfo.occupation);
    body.append('user_id', user_id);

    console.log('body:', JSON.stringify(body));

    this.axiosClient.put("/accounts/update-user",
      body).then(function (response) {
        console.log('axios_response: ' + JSON.stringify(response));
      }).catch(function (error) {
        console.log('axios_error: ' + JSON.stringify(error));
      });
  }

  recoverEmail(name, birthdate, setEmailRecoveredCallback) {
    console.log('123email recoverEmail()');
    const body = new FormData();
    body.append('name', name);
    body.append('birthdate', birthdate);
    this.axiosClient.post('/accounts/recover-email', body).then(response => {
      if (response.data.status === 200) {
        setEmailRecoveredCallback(true, response.data.email);
      } else {
        setEmailRecoveredCallback(false, '');
      }
    });
  }

  resetPassword(email, email_greeting, email_body, email_end, language, resetPassCallback) {
    const body = new FormData();
    body.append('email', email);
    body.append('email_greeting', email_greeting);
    body.append('email_body', email_body);
    body.append('email_end', email_end);
    body.append('language', language);
    this.axiosClient.post('/accounts/pass-reset-by-email/', body).then(response => {
      if (response.data.status === 200) {
        resetPassCallback(true);
      } else {
        resetPassCallback(false);
      }
    }).catch(err => {
      console.log('123password error: ' + JSON.stringify(err));
      resetPassCallback(false);
    });
  }

  signup(userInfo) {
    console.log('axios_: userInfo: ' + JSON.stringify(userInfo));
    const body = new FormData();
    body.append('email', userInfo.email);
    body.append('password', userInfo.password);
    body.append('name', userInfo.first_name + ' ' + userInfo.last_name);
    body.append('login_type', 'neurobeat');
    body.append('weight', userInfo.weight);
    body.append('height', userInfo.height);
    body.append('country', userInfo.country);
    body.append('birthdate', userInfo.birthdate);
    body.append('gender', userInfo.gender);
    body.append('occupation', userInfo.occupation);

    this.axiosClient.post("/accounts/register",
      body).then(function (response) {
        console.log('axios_response: ' + JSON.stringify(response));

        if (response['data']['is_success']) {
          // store _id to couchbase lite.
          // to access it, you need to: response['data']['_id']
          savePreference('isLoginViaRegistration', JSON.stringify(true));
          showAlertWithActions('', I18n.t('REGISTRATION_SUCCESSFUL'), () => {
            Actions.login();
          }, true);

        } else {
          showAlert('', response['data']['message']);
        }
      }).catch(function (error) {
        console.log('axios_error: ' + JSON.stringify(error));
      });
  }

  signIn(userNameOrId, passwordOrName, loginType, createLoginSession) {

    return new Promise((fulfill, reject) => {

      const params = new FormData();
      let paramKey1, paramKey2;

      if (loginType === 'neurobeat') {
        paramKey1 = 'username';
        paramKey2 = 'password';
      } else {
        paramKey1 = 'user_id';
        paramKey2 = 'name';
      }

      params.append("login_type", loginType);
      params.append(paramKey1, userNameOrId);
      params.append(paramKey2, passwordOrName);

      console.log('Request params: ' + JSON.stringify(params));

      this.axiosClient.post("/accounts/signin", params).then(response => {
        console.log('RESPONSE:', response);
        if (STATUS_200 === response['data']['status']) {
          fulfill(response['data']['user']);
          createLoginSession(response['data']['user_id']);
        } else {
          reject(response['data']['user']);
          showAlert(I18n.t('ERROR'), response['data']['message']);
        }
      }).catch(error => {
        reject(error);
      });

    });
  }
}