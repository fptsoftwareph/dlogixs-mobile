import I18n from '../translate/i18n/i18n';

export let daysOfTheMonth = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
  '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

  export let weeksOfTheMonth = ['0', '1-7', '8-14', '15-21', '22-28', '29-31'];

export function extractMinsFromTimeStr(timeStr) {
  return timeStr.substring(timeStr.indexOf(':') + 1).substring(0, 2);
}

export function extractHoursFromTimeStr(timeStr) {
  console.log('123redux** timeStr: ' + timeStr);
  return timeStr.substring(0, timeStr.indexOf(':'));
}

export function getMonthNames() {
  let monthShortNames = ["", I18n.t('JAN'), I18n.t('FEB'), I18n.t('MAR'), I18n.t('APR'),
    I18n.t('MAY'), I18n.t('JUN'), I18n.t('JUL'), I18n.t('AUG'), I18n.t('SEP'), I18n.t('OCT'), I18n.t('NOV'), I18n.t('DEC')];
  let newMonthNames = [];

  for (let i = 0; i < monthShortNames.length; i++) {
    newMonthNames[i] = monthShortNames[i];
  }

  return newMonthNames;
}

export function getWeekNames() {
  let weekNames = [I18n.t('SUNDAY'), I18n.t('MONDAY'), I18n.t('TUESDAY'), I18n.t('WEDNESDAY'),
  I18n.t('THURSDAY'), I18n.t('FRIDAY'), I18n.t('SATURDAY')];
  let newWeekNames = [];

  for (let i = 0; i < weekNames.length; i++) {
    newWeekNames[i] = weekNames[i];
  }

  return newWeekNames;
}

export function formatTime(time) {
  let hour = Math.floor(parseInt(time) / 60);
  let mins = parseInt(time) % 60;

  if (typeof hour === 'undefined' || isNaN(hour)) {
    hour = 0;
  }

  if (typeof mins === 'undefined' || isNaN(mins)) {
    mins = 0;
  }
  return [hour, mins];
}

export function getMonthComplete() {
  let monthComplete = [
    I18n.t('JANUARY'), I18n.t('FEBRUARY'), I18n.t('MARCH'), I18n.t('APRIL'), I18n.t('MAY'), I18n.t('JUNE'), I18n.t('JULY'),
    I18n.t('AUGUST'), I18n.t('SEPTEMBER'), I18n.t('OCTOBER'), I18n.t('NOVEMBER'), I18n.t('DECEMBER')
  ];
  let newMonthComplete = [];

  for (let i = 0; i < monthComplete.length; i++) {
    newMonthComplete[i] = monthComplete[i];
  }

  return newMonthComplete;
}

// this function computes the total minutes from a given hour & string params
// i.e. 120 for 2 hours 0 mins.
// Note we deduct 1 from the hourStr because time in js starts with 0-23 (24 hour format)
// so 12 pm is 11 on the 24-hour format of the Date library.
export function computeTotalMins(hourStr, minStr) {
  return ((parseInt(hourStr) - 1) * 60) + parseInt(minStr);
}

export function getFormattedTimestamp() {
  var month = new Date().getMonth() + 1 < 10 ? "0" + (new Date().getMonth() + 1) : new Date().getMonth() + 1;
  var day = new Date().getDate() < 10 ? "0" + (new Date().getDate()) : new Date().getDate();
  var sec = new Date().getSeconds() < 10 ? "0" + new Date().getSeconds() : new Date().getSeconds();
  var hour = new Date().getHours();

  var min = new Date().getMinutes() < 10 ? "0" + new Date().getMinutes() : new Date().getMinutes();
  var timeNow = (hour < 10 ? "0" + hour : hour) + ":" + min + ":" + sec;
  var dt = month + "/" + day + '/' + new Date().getFullYear() + ' ' + timeNow;
  // returned format would be something like this: 02/22/2018 16:40:58
  return dt;
}

// current date formatted in yyyy-mm-dd
export default function getCurrentDate() {
  var d = new Date();
  let month = '' + (d.getMonth() + 1);
  let day = '' + d.getDate();
  let year = d.getFullYear();

  month = month.length < 2 ? '0' + month : month;
  day = day.length < 2 ? '0' + day : day;

  let currentDate = [year, month, day].join('-');
  return currentDate;
}

// passed date formatted in yyyy-mm-dd
export function convertDateToYYYYMMDD(date) {
  var d = new Date(date);
  let month = '' + (d.getMonth() + 1);
  let day = '' + d.getDate();
  let year = d.getFullYear();

  month = month.length < 2 ? '0' + month : month;
  day = day.length < 2 ? '0' + day : day;

  return [year, month, day].join('-');
}
