

export class ChartUtils {

  static getWeeklyBarYAxisValues(barChartValues) {
    console.log('123debug: barChartValues: ' + JSON.stringify(barChartValues));

    let week1Sum = 0;
    let week2Sum = 0;
    let week3Sum = 0;
    let week4Sum = 0;
    let excessWeekSum = 0;
    const chartLength = barChartValues.length;

    for (let i = 0; i < chartLength; i++) {
      // get the sum of the y values in week 1
      if (i == 7) {
        for (let j = 1; j <= 7; j++) {
          week1Sum += barChartValues[j];
        }
      } else if (i == 14) {
        for (let j = 8; j <= 14; j++) {
          week2Sum += barChartValues[j];
        }
      } else if (i == 21) {
        for (let j = 15; j <= 21; j++) {
          week3Sum += barChartValues[j];
        }
      } else if (i == 28) {
        for (let j = 22; j <= 28; j++) {
          week4Sum += barChartValues[j];
        }
      } else if (i == chartLength - 1) {
        // excess days
        for (let j = 29; j <= chartLength - 1; j++) {
          excessWeekSum += barChartValues[j];
        }
      }
    }

    return [week1Sum, week2Sum, week3Sum, week4Sum, excessWeekSum];
  }

  // this will return the dividends (for the averaging computation for bpm) per week
  static getWeeklyDividends(barChartValues) {
    console.log('123debug: barChartValues: ' + JSON.stringify(barChartValues));

    let week1Dividend = 0;
    let week2Dividend = 0;
    let week3Dividend = 0;
    let week4Dividend = 0;
    let excessWeekDividend = 0;

    const chartLength = barChartValues.length;

    for (let i = 0; i < chartLength; i++) {
      // get the sum of the y values in week 1
      if (i == 7) {
        for (let j = 1; j <= 7; j++) {
          if (barChartValues[j] > 0) {
            week1Dividend++;
          }
        }
      } else if (i == 14) {
        for (let j = 8; j <= 14; j++) {
          if (barChartValues[j] > 0) {
            week2Dividend++;
          }
        }
      } else if (i == 21) {
        for (let j = 15; j <= 21; j++) {
          if (barChartValues[j] > 0) {
            week3Dividend++;
          }
        }
      } else if (i == 28) {
        for (let j = 22; j <= 28; j++) {
          if (barChartValues[j] > 0) {
            week4Dividend++;
          }
        }
      } else if (i == chartLength - 1) {
        // excess days
        for (let j = 29; j <= chartLength - 1; j++) {
          if (barChartValues[j] > 0) {
            excessWeekDividend++;
          }
        }
      }
    }

    return [week1Dividend, week2Dividend, week3Dividend, week4Dividend, excessWeekDividend];
  }

  // Note: allChartData is an array of elements with this format: [{_id: '', date: '', count: 0}]
  static getMonthlyBarYAxisValues(allChartData) {
    console.log('123debug: allChartData: ' + JSON.stringify(allChartData) + '\tlength: ' + allChartData.length);
    let months = [];

    for (let i = 1; i < 13; i++) {
      let count = 0;
      for (let j = 0; j < allChartData.length; j++) {
        if (typeof allChartData[j] !== 'undefined') {
          const currentData = allChartData[j];
          // console.log('123debug currentData: ' + JSON.stringify(currentData));
          // console.log('123debug currentData doc: ' + JSON.stringify(currentData.doc));
          const hyphenIndex = currentData.date.indexOf('-');
          const extractedMonth = currentData.date.substring(hyphenIndex + 1).substring(0, 2);
          if (i === parseInt(extractedMonth)) {
            count += currentData.count;
            months[i] = count;
          } else {
            if (typeof months[i] === 'undefined') {
              months[i] = 0;
            }
          }
        }
      }
    }

    // this removes the first 'empty' index created when we initialize months.
    months.splice(0, 1);
    return months;
  }

  static getYearlyBarYAxisValues(allChartData) {
    console.log('123debug: allChartData: ' + JSON.stringify(allChartData) + '\tlength: ' + allChartData.length);
    let highest = 0;
    let lowest = 10000000;
    const years = [];
    let index = 0;

    for (let i = 0; i < allChartData.length; i++) {
      if (typeof allChartData[i] === 'undefined' || allChartData[i].date === '') {
        continue;
      }

      const year = allChartData[i].date.substring(0, 4);
      if (year > highest) {
        highest = year;
      }

      console.log('123debug year: ' + year);

      if (year < lowest) {
        lowest = year;
      }
    }

    console.log('123debug: highest: ' + highest + '\tlowest: ' + lowest);

    for (let i = lowest; i <= highest; i++) {
      let count = 0;
      for (let j = 0; j < allChartData.length; j++) {
        if (typeof allChartData[j] === 'undefined' || allChartData[j].date === '') {
          continue;
        }
        const year = parseInt(allChartData[j].date.substring(0, 4));
        // document.writeln("i: " + i + "\tyear: " + year );
        if (i == parseInt(year)) {
          count += allChartData[j].count;
          years[index] = { year: year, count: count };
        }
      }
      index++;
    }

    console.log('123debug: years: ' + JSON.stringify(years));
    return years;
  }

  // create an array of documents with type === 'food'
  static getTotalCaloriesArray(foodRecords, totalDaysInAMonth) {
    let calories = [];
    let months = [];
    let xHighlight = 0;
    let yHighlight = 0;
    let shouldShowTargetLine = false;
    let greaterThanTarget = false;

    console.log('FOOD RECORDS ', foodRecords);
    for (let i = 0; i <= totalDaysInAMonth; i++) {
      let totalCal = 0;
      for (let j = 0; j < foodRecords.length; j++) {
        if (typeof foodRecords[j] !== 'undefined') {
          let day = foodRecords[j].date.substring(foodRecords[j].date.length - 2);
          if ((parseInt(day)) === i) {
            console.log('DAY:', day)
            totalCal += foodRecords[j].cals_count;

            greaterThanTarget = totalCal > foodRecords[j].target_cals && foodRecords[j].target_cals > 0;

            shouldShowTargetLine = totalCal === foodRecords[j].target_cals && foodRecords[j].cals_count > 0;

            console.log('TOTAL CALSSS:', totalCal);
            console.log('CALSSS COUNT:', foodRecords[j].cals_count);
            console.log('TARGET CALSSS COUNT:', foodRecords[j].target_cals);

            calories[i] = {
              x: i, y: totalCal,
              marker: shouldShowTargetLine ? '\uD83C\uDFC6' : greaterThanTarget ? '\u2757' : ''
            };

            console.log('SHOULD SHOW', shouldShowTargetLine);
            console.log('GREATER THAN:', greaterThanTarget);

            if (shouldShowTargetLine) {
              xHighlight = shouldShowTargetLine ? day : 0;
              yHighlight = calories[i].y;
            }

            console.log('CALORIES:', calories[i])
          } else if (typeof calories[i] === 'undefined') {
            calories[i] = {
              x: i, y: 0,
              marker: ''
            };
          }
        }
      }
    }

    return [calories, xHighlight, yHighlight];
  }
}
