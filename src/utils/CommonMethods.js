import { AsyncStorage, NetInfo, Alert } from 'react-native';
import I18n from './../translate/i18n/i18n';
import Toast from 'react-native-simple-toast';

const savePreference = async (keyName, value) => {
  try {
    await AsyncStorage.setItem(keyName, value);
  } catch (error) {
    // Error saving data
    setLog2('savePreference', `Error:  ${error}`);
  }
};

const getPreference = async keyName => {
  let itemValue = '';

  if (keyName === 'analysisTime') {
    itemValue = '18:00';
  } else {
    itemValue = 'english';
  }

  try {
    const item = await AsyncStorage.getItem(keyName);
    if (item !== null) {
      // We have data!!
      setLog2('getPreference', item);
      itemValue = item;
    }
  } catch (error) {
    // Error retrieving data
    setLog2('getPreference', `Error:  ${error}`);
  }
  return itemValue;
};

const removePreferenceKeys = keys => {
  AsyncStorage.multiRemove(keys)
  .then(res => {
    setLog2('sleep123 removePreferenceKeys', res);
  })
  .catch(err => {
    setLog2('sleep123 removePreferenceKeys', `Error: ${err}`);
  });
};

const getBooleanPreference = async keyName => {
  let itemValue = false;
  try {
    const item = await AsyncStorage.getItem(keyName);
    if (item !== null) {
      // We have data!!
      setLog2('getBooleanPreference', item);
      itemValue = item;
    }
  } catch (error) {
    // Error retrieving data
    setLog2('getBooleanPreference', `Error:  ${error}`);
  }
  return itemValue;
};

const setLog = message => {
  console.log(message);
};

const setLog2 = (className, message) => {
  console.log(className, message);
};

const convert24To12Hr = time => {
  // Check correct time format and split into components
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) {
    // If time format correct
    time = time.slice(1); // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join(''); // return adjusted time or original string
};

const isConnectedToInternet = () => {
  NetInfo.isConnected
    .fetch()
    .then(isConnected => isConnected)
    .catch(error => error);
};

const showAlert = (title, message) => {
  Alert.alert(title, message, [{ text: I18n.t('OK'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' }], {
    cancelable: false
  });
};

const showToast = (message) => {
  Toast.show(message, Toast.LONG);
};

const showAlertWithActions = (title, message, rightAction, noCancel) => {
  if (noCancel) {
    Alert.alert(
      title,
      message,
      [
        { text: I18n.t('OK'), onPress: () => rightAction() }
      ],
      { cancelable: false }
    );
  } else {
    Alert.alert(
      title,
      message,
      [
        { text: I18n.t('CANCEL'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: I18n.t('OK'), onPress: () => rightAction() }
      ],
      { cancelable: false }
    );
  }
};

const setLocalization = lang => {
  console.log(`setLocalization: ${lang}`);
  if (lang === 'korean') {
    I18n.locale = 'kor';
  } else if (lang === 'chinese') {
    I18n.locale = 'chi';
  } else if (lang === 'japanese') {
    I18n.locale = 'jap';
  } else {
    I18n.locale = 'en';
  }
};

export {
  savePreference, getPreference, setLog, setLog2, convert24To12Hr, isConnectedToInternet, showAlert, showToast,
  getBooleanPreference, setLocalization, showAlertWithActions, removePreferenceKeys
};
