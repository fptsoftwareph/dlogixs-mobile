import React, { Component } from 'react';
import { TouchableOpacity, Image, Platform, Dimensions } from 'react-native';
import {
  ActionConst,
  Actions,
  Stack,
  Scene,
  Router
} from 'react-native-router-flux';
import { widgets } from './styles/widgets';
import Login from './components/Login';
import Splash from './components/Splash';
import Signup from './components/Signup';
import { Icon } from 'react-native-elements';
import RecoverEmail from './components/RecoverEmail';
import ResetPassword from './components/ResetPassword';
import SelectLanguage from './components/SelectLanguage';
import Intro from './components/Intro';
import Home from './components/Home';
import Settings from './components/Settings';
import Qration from './components/Qration';
import Alarm from './components/alarm/Alarm';
import AlarmDetail from './components/alarm/AlarmDetail';
import UserProfile from './components/UserProfile';
import BrainwaveAnalysis from './components/graphs/BrainwaveAnalysis';
import FoodRecord from './components/graphs/FoodRecord';
import AddFood from './components/AddFood';
import SleepAnalysis from './components/graphs/SleepAnalysis';
import ActivitySleepAnalysis from './components/graphs/ActivitySleepAnalysis';
import BinauralBeat from './components/graphs/BinauralBeat';
import WaterRecord from './components/graphs/WaterRecord';
import AlcoholRecord from './components/graphs/AlcoholRecord';
import CaffeineRecord from './components/graphs/CaffeineRecord';
import CigaretteRecord from './components/graphs/CigaretteRecord';
import AlarmActivity from './components/alarm/AlarmActivity';
const { width, height } = Dimensions.get('window');
import { SOCIAL_LOGIN_ICON_COLOR } from './styles/colors';
import { getPreference, savePreference } from './utils/CommonMethods';

//let logo = require("./images/Logo-header.png");
//let rightIcon = require("./images/shortcut_icons/settings.png");

class AppRouter extends Component {
  renderRightComponent() {
    return (
      <TouchableOpacity onPress={() => { Actions.settings() }} >
        <Image style={{ height: 15, width: 15, padding: 10, marginRight: 15 }} source={require('./images/shortcut_icons/settings.png')} />
      </TouchableOpacity>
    );
  }

  renderLeftComponent() {
    return (
      <TouchableOpacity onPress={() => {
        this.callPop();
      }} >
        <Icon
          iconStyle={{ marginLeft: 10 }}
          color={SOCIAL_LOGIN_ICON_COLOR}
          size={40}
          type='ionicon'
          name='ios-arrow-round-back'
        />
      </TouchableOpacity>
    );
  }

  renderLogo() {
    return (
      <TouchableOpacity onPress={() => {
        Actions.homePage()
      }}>
        <Image style=
          {{
            height: height <= 640 ? 18 : 20,
            width: height <= 640 ? 135 : 145,
            ...Platform.select({
              android: {
                marginLeft: '20%',
              }
            })
          }}
          source={require('./images/Logo-header.png')} />
      </TouchableOpacity>
    );
  }

  callPop() {
    // There's a weird bug wherein if we call Actions.pop({refresh: {<props>}}), the scene from where we invoked the pop
    // will have its componentDidMount callback invoked again.
    // i.e. if we pressed the left arrow in ActivitySleepAnalysis page, when we get
    // to the home page, the ActivityAnalysis' componentDidMount gets called again. We can bypass this issue by
    // setting a condition wherein we'll only use the .pop({refresh: {<props>}}) when the user profile has not been
    // updated yet. Note: we need pop({refresh: {<props>}}) to ensure that our componentWillReceiveProps()
    // gets invoked every time we press the back button or left arrow.
    getPreference('isProfileLacking').then(res => {
      if (JSON.parse(res)) {
        Actions.pop({ refresh: { backPressProp: Math.random() * new Date().getTime() } });
      } else {
        // Note the reason we're using random and not backPressProp is because if we use backPressProp, we'd be invoking
        // the condition I've created in Home.js for checking the user profile.
        getPreference('isFoodAdded').then(res => {
          if (res === "true") {
            Actions.pop({ refresh: { isFoodAdded: Math.floor(Math.random() * 100000) } });
          } else {
            // Note the reason we're using random and not backPressProp is because if we use backPressProp, we'd be invoking
            // the condition I've created in Home.js for checking the user profile.
            Actions.pop();
          }
        });
      }
    });
  }

  onBackPress = () => {
    if (Actions.state.index === 0) {
      return false;
    }

    this.callPop();
    return true;
  }

  render() {
    return (
      <Router
        navBarButtonColor='#9b9b9b'
        navigationBarStyle={widgets.appHeaderBg}
        //navigationBarTitleImage={logo}
        //navigationBarTitleImageStyle={{alignSelf: 'center', marginLeft: -5, height: 26, width:185}}
        // renderTitle={this.test()}
        // rightButtonImage={rightIcon}
        // onRight={() => Actions.settings()}
        renderLeftButton={() => this.renderLeftComponent()}
        renderRightButton={() => this.renderRightComponent()}
        backAndroidHandler={this.onBackPress}
      >
        <Stack
          key="root">
          <Scene
            type={ActionConst.REPLACE}
            hideNavBar
            key="splash"
            component={Splash}
            initial
          />
          <Scene
            hideNavBar
            key="selectLang"
            component={SelectLanguage}
            type={ActionConst.RESET}
          />
          <Scene
            hideNavBar
            key="login"
            component={Login}
            type={ActionConst.RESET}
          />
          <Scene
            hideNavBar
            key="signup"
            component={Signup}
          />
          <Scene
            hideNavBar
            key="recoverEmail"
            component={RecoverEmail}
          />
          <Scene
            hideNavBar
            key="resetPassword"
            component={ResetPassword}
          />
          <Scene
            hideNavBar
            key="intro"
            component={Intro}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="settings"
            component={Settings}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="userProfile"
            onLeft={() => {
              this.callPop();
            }}
            component={UserProfile} />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="brainwaveAnalysis"
            component={BrainwaveAnalysis}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="foodRecord"
            component={FoodRecord}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="activitySleepAnalysis"
            component={ActivitySleepAnalysis}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="sleepAnalysis"
            component={SleepAnalysis}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="addFood"
            component={AddFood}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="binauralBeat"
            component={BinauralBeat}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="alarm"
            component={Alarm}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="alarmDetail"
            component={AlarmDetail}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="qration"
            component={Qration}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="waterRecord"
            component={WaterRecord}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="alcoholRecord"
            component={AlcoholRecord}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="caffeineRecord"
            component={CaffeineRecord}
          />
          <Scene
            renderTitle={() => this.renderLogo()}
            key="cigaretteRecord"
            component={CigaretteRecord}
          />
          <Scene
            hideNavBar
            key="homePage"
            component={Home}
            //navigationBarTitleImage={logo}
            navigationBarTitleImageStyle={{ alignSelf: 'center', marginLeft: 5 }}
            // rightButtonImage={rightIcon}
            // onRight={() => { Actions.settings() }}
            type={ActionConst.RESET}
          />
          <Scene
            hideNavBar
            key="alarmActivity"
            component={AlarmActivity}
            type={ActionConst.JUMP}
          />
        </Stack>
      </Router>
    );
  }
}

export default AppRouter;