import React, { Component } from 'react';
import AppRouter from  './src/AppRouter';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './src/redux/reducers';
import ReduxThunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/es/storage';
import { PersistGate } from 'redux-persist/es/integration/react';
import { View } from 'react-native';
//import configureStore from './store/configureStore'

export default class App extends Component {

/*configureStore () {
    let store = createStore(reducer)
    let persistor = persistStore(store)
    
    return { persistor, store }
}*/

  render() {
    return (
      <Provider store={store}>
        <PersistGate 
          loading={<View/>}
          onBeforeLift={onBeforeLift}
          persistor={persistor}>
            <AppRouter/>
        </PersistGate>
      </Provider>
    );
  }
}

//const { persistor, store } = configureStore();

const onBeforeLift = () => {
  // take some action before the gate lifts
  console.log("onBeforeLift");
}

const config = {
  key: 'root', // key is required
  storage, // storage is now required
}
const reducer = persistReducer(config, reducers)
const store = createStore(reducer, {}, applyMiddleware(ReduxThunk));
const persistor = persistStore(store);